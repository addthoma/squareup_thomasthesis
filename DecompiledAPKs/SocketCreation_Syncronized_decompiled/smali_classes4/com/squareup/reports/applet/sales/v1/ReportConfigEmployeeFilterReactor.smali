.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Reactor<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportConfigEmployeeFilterReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportConfigEmployeeFilterReactor.kt\ncom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Singles\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,177:1\n1452#2:178\n1360#3:179\n1429#3,3:180\n*E\n*S KotlinDebug\n*F\n+ 1 ReportConfigEmployeeFilterReactor.kt\ncom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor\n*L\n102#1:178\n129#1:179\n129#1,3:180\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0004\u0018\u0019\u001a\u001bB\u0017\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u000bH\u0002J2\u0010\u000c\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u00022\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0011H\u0016J\u001e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u000f\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;",
        "Lcom/squareup/workflow/rx1/Reactor;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;",
        "reportConfigRunner",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;",
        "employees",
        "Lcom/squareup/permissions/Employees;",
        "(Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lcom/squareup/permissions/Employees;)V",
        "loadInitialState",
        "Lio/reactivex/Single;",
        "onReact",
        "Lrx/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "selectEmployee",
        "",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;",
        "employeeToken",
        "",
        "Employee",
        "EmployeeFilterEvent",
        "EmployeeFilterResult",
        "EmployeeFilterState",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final employees:Lcom/squareup/permissions/Employees;

.field private final reportConfigRunner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lcom/squareup/permissions/Employees;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reportConfigRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employees"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->reportConfigRunner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->employees:Lcom/squareup/permissions/Employees;

    return-void
.end method

.method public static final synthetic access$loadInitialState(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;)Lio/reactivex/Single;
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->loadInitialState()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$selectEmployee(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;Ljava/lang/String;)Ljava/util/List;
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->selectEmployee(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final loadInitialState()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->employees:Lcom/squareup/permissions/Employees;

    .line 93
    invoke-virtual {v0}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->reportConfigRunner:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    .line 97
    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEdit()Lrx/Observable;

    move-result-object v1

    const-string v2, "reportConfigRunner\n        .configInEdit()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    .line 99
    sget-object v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$loadInitialState$previouslySelectedEmployees$1;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$loadInitialState$previouslySelectedEmployees$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    .line 102
    sget-object v2, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    const-string v2, "activeEmployees"

    .line 103
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/SingleSource;

    const-string v2, "previouslySelectedEmployees"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/SingleSource;

    .line 178
    new-instance v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$loadInitialState$$inlined$zip$1;

    invoke-direct {v2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$loadInitialState$$inlined$zip$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    invoke-static {v0, v1, v2}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.zip(s1, s2, BiFun\u2026-> zipper.invoke(t, u) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final selectEmployee(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;",
            ">;"
        }
    .end annotation

    .line 128
    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;->getListedEmployees()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 180
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 181
    move-object v2, v1

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    .line 130
    invoke-virtual {v2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 131
    invoke-virtual {v2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->isSelected()Z

    move-result v1

    xor-int/lit8 v6, v1, 0x1

    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->copy$default(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    move-result-object v2

    .line 130
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 182
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public onAbandoned(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/Reactor$DefaultImpls;->onAbandoned(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onAbandoned(Ljava/lang/Object;)V
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->onAbandoned(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)V

    return-void
.end method

.method public onReact(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent;",
            ">;)",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$InitialState;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$1;

    invoke-direct {p1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$1;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 59
    :cond_0
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$AllEmployeesSelected;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$2;

    invoke-direct {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$2;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 69
    :cond_1
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->onReact(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method
