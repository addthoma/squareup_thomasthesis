.class public Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;
.super Ljava/lang/Object;
.source "SalesReportPayloadFactory.java"


# instance fields
.field private final printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/salesreport/print/SalesPrintFormatter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->res:Lcom/squareup/util/Res;

    .line 35
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    return-void
.end method

.method private createItemRowWithVariants(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;Ljava/util/List;)Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;)",
            "Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;"
        }
    .end annotation

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 125
    new-instance v2, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    iget-object v4, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedName:Ljava/lang/String;

    iget-object v5, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->quantity:Ljava/math/BigDecimal;

    iget-object v6, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMeasurementUnit:Ljava/lang/String;

    .line 126
    invoke-virtual {v3, v4, v5, v6}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantityWithMeasurmentUnit(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMoney:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 125
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :cond_0
    new-instance p2, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedName:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->quantity:Ljava/math/BigDecimal;

    iget-object v4, p1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMeasurementUnit:Ljava/lang/String;

    .line 131
    invoke-virtual {v1, v2, v3, v4}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantityWithMeasurmentUnit(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMoney:Ljava/lang/String;

    invoke-direct {p2, v1, p1, v0}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object p2
.end method


# virtual methods
.method public buildPayload(Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;)Lcom/squareup/salesreport/print/SalesReportPayload;
    .locals 8

    .line 39
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->toHeaderSection(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    move-result-object v2

    .line 40
    new-instance v3, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_sales_summary_sales_table:I

    .line 41
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->salesRows:Ljava/util/List;

    .line 42
    invoke-virtual {p0, v1}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->toSalesSectionRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 44
    new-instance v4, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_sales_summary_payments_table:I

    .line 45
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->paymentsRows:Ljava/util/List;

    .line 46
    invoke-virtual {p0, v1}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->toPaymentsSectionRow(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 48
    new-instance v5, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_discounts_applied:I

    .line 49
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->discountRows:Ljava/util/List;

    .line 50
    invoke-virtual {p0, v1}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->toDiscountSectionRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v5, v0, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 52
    new-instance v6, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_category_sales:I

    .line 53
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->categoryRows:Ljava/util/List;

    .line 54
    invoke-virtual {p0, v1}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->toCategorySectionRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 57
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-boolean v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_items_sales:I

    .line 59
    invoke-interface {v1, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->categoryRows:Ljava/util/List;

    .line 60
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->toItemSectionRows(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;-><init>(Ljava/lang/String;Ljava/util/List;)V

    move-object v7, v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    move-object v7, p1

    .line 63
    :goto_0
    new-instance p1, Lcom/squareup/salesreport/print/SalesReportPayload;

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/print/SalesReportPayload;-><init>(Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)V

    return-object p1
.end method

.method toCategorySectionRows(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;",
            ">;"
        }
    .end annotation

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 82
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 83
    iget-object v2, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    sget-object v3, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    if-ne v2, v3, :cond_0

    .line 84
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    iget-object v3, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedName:Ljava/lang/String;

    iget-object v4, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->quantity:Ljava/math/BigDecimal;

    iget-object v5, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMeasurementUnit:Ljava/lang/String;

    .line 85
    invoke-virtual {v2, v3, v4, v5}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantityWithMeasurmentUnit(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    new-instance v3, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;

    iget-object v1, v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMoney:Ljava/lang/String;

    invoke-direct {v3, v2, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method toDiscountSectionRows(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;",
            ">;"
        }
    .end annotation

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 140
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;

    .line 141
    new-instance v2, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    iget-object v4, v1, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->discountName:Ljava/lang/String;

    iget-object v5, v1, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->formattedQuantity:Ljava/lang/String;

    .line 142
    invoke-virtual {v3, v4, v5}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->nameAndQuantity(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->formattedDiscountMoney:Ljava/lang/String;

    invoke-direct {v2, v3, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method toHeaderSection(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;
    .locals 7

    .line 69
    new-instance v6, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report:I

    .line 70
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->printFormatter:Lcom/squareup/salesreport/print/SalesPrintFormatter;

    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    .line 72
    invoke-virtual {v0, v2, p1}, Lcom/squareup/salesreport/print/SalesPrintFormatter;->reportDateRange(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    const-string v2, ""

    const-string v4, ""

    const-string v5, ""

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method

.method toItemSectionRows(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;",
            ">;"
        }
    .end annotation

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 100
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 101
    iget-object v4, v3, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    sget-object v5, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    if-ne v4, v5, :cond_2

    if-eqz v2, :cond_1

    .line 104
    invoke-direct {p0, v2, v1}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->createItemRowWithVariants(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;Ljava/util/List;)Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v3

    goto :goto_0

    .line 108
    :cond_2
    iget-object v4, v3, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    sget-object v5, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM_VARIATION:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    if-ne v4, v5, :cond_0

    .line 109
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    .line 114
    invoke-direct {p0, v2, v1}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->createItemRowWithVariants(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;Ljava/util/List;)Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method

.method toPaymentsSectionRow(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;",
            ">;"
        }
    .end annotation

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 166
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    .line 167
    new-instance v2, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;

    iget-object v3, v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->rowText:Ljava/lang/String;

    iget-object v4, v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->value:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 170
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method toSalesSectionRows(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;",
            ">;"
        }
    .end annotation

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 153
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    .line 154
    new-instance v2, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;

    iget-object v3, v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->rowText:Ljava/lang/String;

    iget-object v4, v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->value:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v2, v3, v4, v1}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 157
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method
