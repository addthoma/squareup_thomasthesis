.class public Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent;
.super Lcom/squareup/analytics/event/v1/ErrorEvent;
.source "LoadingReportFailedEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;
    }
.end annotation


# instance fields
.field public final error_type:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;)V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/analytics/RegisterErrorName;->REPORTS_LOADING_REPORT_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ErrorEvent;-><init>(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 17
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent;->error_type:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-void
.end method
