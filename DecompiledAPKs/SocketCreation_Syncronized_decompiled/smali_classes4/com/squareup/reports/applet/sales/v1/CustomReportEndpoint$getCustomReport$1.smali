.class final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$getCustomReport$1;
.super Ljava/lang/Object;
.source "CustomReportEndpoint.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;Lkotlin/jvm/functions/Function1;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $isSuccessful:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$getCustomReport$1;->$isSuccessful:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    sget-object v0, Lcom/squareup/receiving/StandardReceiver;->Companion:Lcom/squareup/receiving/StandardReceiver$Companion;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$getCustomReport$1;->$isSuccessful:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/receiving/StandardReceiver$Companion;->rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$getCustomReport$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    return-object p1
.end method
