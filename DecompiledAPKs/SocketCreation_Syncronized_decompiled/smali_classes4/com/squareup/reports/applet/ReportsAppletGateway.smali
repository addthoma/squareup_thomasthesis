.class public interface abstract Lcom/squareup/reports/applet/ReportsAppletGateway;
.super Ljava/lang/Object;
.source "ReportsAppletGateway.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/ReportsAppletGateway$ParentComponent;,
        Lcom/squareup/reports/applet/ReportsAppletGateway$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008f\u0018\u0000 \u00062\u00020\u0001:\u0002\u0006\u0007J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsAppletGateway;",
        "",
        "activate",
        "",
        "activateCurrentDrawerReport",
        "activateDisputesReport",
        "Companion",
        "ParentComponent",
        "reports-applet"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/reports/applet/ReportsAppletGateway$Companion;

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "REPORTS"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/reports/applet/ReportsAppletGateway$Companion;->$$INSTANCE:Lcom/squareup/reports/applet/ReportsAppletGateway$Companion;

    sput-object v0, Lcom/squareup/reports/applet/ReportsAppletGateway;->Companion:Lcom/squareup/reports/applet/ReportsAppletGateway$Companion;

    return-void
.end method


# virtual methods
.method public abstract activate()V
.end method

.method public abstract activateCurrentDrawerReport()V
.end method

.method public abstract activateDisputesReport()V
.end method
