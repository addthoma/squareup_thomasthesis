.class public final Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;
.super Ljava/lang/Object;
.source "RealReportsAppletEntryPoint_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;",
        ">;"
    }
.end annotation


# instance fields
.field private final currentDrawerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;"
        }
    .end annotation
.end field

.field private final disputesSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;"
        }
    .end annotation
.end field

.field private final drawerHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
            ">;"
        }
    .end annotation
.end field

.field private final gatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyReportSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final salesReportSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/SalesReportSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/SalesReportSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->preferencesProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->currentDrawerProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->drawerHistoryProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->salesReportSectionProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->disputesSectionProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->loyaltyReportSectionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/SalesReportSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
            ">;)",
            "Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;"
        }
    .end annotation

    .line 66
    new-instance v8, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;)Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;
    .locals 9

    .line 73
    new-instance v8, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;-><init>(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;
    .locals 8

    .line 55
    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->currentDrawerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->drawerHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->salesReportSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/reports/applet/sales/SalesReportSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->disputesSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/reports/applet/disputes/DisputesSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->loyaltyReportSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    invoke-static/range {v1 .. v7}, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->newInstance(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;)Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/reports/applet/RealReportsAppletEntryPoint_Factory;->get()Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;

    move-result-object v0

    return-object v0
.end method
