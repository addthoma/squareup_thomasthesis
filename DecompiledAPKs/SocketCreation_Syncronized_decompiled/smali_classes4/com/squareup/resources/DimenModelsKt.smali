.class public final Lcom/squareup/resources/DimenModelsKt;
.super Ljava/lang/Object;
.source "DimenModels.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0015\u0010\u0002\u001a\u00020\u0001*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\"\u0015\u0010\u0006\u001a\u00020\u0001*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005\u00a8\u0006\u0008"
    }
    d2 = {
        "ZERO_DIMEN",
        "Lcom/squareup/resources/FixedDimen;",
        "dp",
        "",
        "getDp",
        "(I)Lcom/squareup/resources/FixedDimen;",
        "px",
        "getPx",
        "resources_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ZERO_DIMEN:Lcom/squareup/resources/FixedDimen;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 36
    new-instance v0, Lcom/squareup/resources/FixedDimen;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v2, v1, v3, v1}, Lcom/squareup/resources/FixedDimen;-><init>(ILcom/squareup/resources/FixedDimen$Unit;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/resources/DimenModelsKt;->ZERO_DIMEN:Lcom/squareup/resources/FixedDimen;

    return-void
.end method

.method public static final getDp(I)Lcom/squareup/resources/FixedDimen;
    .locals 2

    if-nez p0, :cond_0

    .line 40
    sget-object p0, Lcom/squareup/resources/DimenModelsKt;->ZERO_DIMEN:Lcom/squareup/resources/FixedDimen;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/resources/FixedDimen;

    sget-object v1, Lcom/squareup/resources/FixedDimen$Unit;->DP:Lcom/squareup/resources/FixedDimen$Unit;

    invoke-direct {v0, p0, v1}, Lcom/squareup/resources/FixedDimen;-><init>(ILcom/squareup/resources/FixedDimen$Unit;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static final getPx(I)Lcom/squareup/resources/FixedDimen;
    .locals 2

    if-nez p0, :cond_0

    .line 38
    sget-object p0, Lcom/squareup/resources/DimenModelsKt;->ZERO_DIMEN:Lcom/squareup/resources/FixedDimen;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/resources/FixedDimen;

    sget-object v1, Lcom/squareup/resources/FixedDimen$Unit;->PX:Lcom/squareup/resources/FixedDimen$Unit;

    invoke-direct {v0, p0, v1}, Lcom/squareup/resources/FixedDimen;-><init>(ILcom/squareup/resources/FixedDimen$Unit;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method
