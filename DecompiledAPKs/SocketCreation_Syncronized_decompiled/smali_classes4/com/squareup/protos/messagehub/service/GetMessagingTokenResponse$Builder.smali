.class public final Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMessagingTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public api_key:Ljava/lang/String;

.field public app_id:Ljava/lang/String;

.field public device_look_up_token:Ljava/lang/String;

.field public domain_name:Ljava/lang/String;

.field public hmac_token:Ljava/lang/String;

.field public recent_active:Ljava/lang/Boolean;

.field public status:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public api_key(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->api_key:Ljava/lang/String;

    return-object p0
.end method

.method public app_id(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->app_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;
    .locals 10

    .line 259
    new-instance v9, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->status:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->domain_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->api_key:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->app_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->hmac_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->recent_active:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->device_look_up_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->build()Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public device_look_up_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->device_look_up_token:Ljava/lang/String;

    return-object p0
.end method

.method public domain_name(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->domain_name:Ljava/lang/String;

    return-object p0
.end method

.method public hmac_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->hmac_token:Ljava/lang/String;

    return-object p0
.end method

.method public recent_active(Ljava/lang/Boolean;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->recent_active:Ljava/lang/Boolean;

    return-object p0
.end method

.method public status(Ljava/lang/Boolean;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->status:Ljava/lang/Boolean;

    return-object p0
.end method
