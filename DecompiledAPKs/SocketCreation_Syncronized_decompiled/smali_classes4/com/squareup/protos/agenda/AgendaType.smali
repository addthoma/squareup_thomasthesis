.class public final enum Lcom/squareup/protos/agenda/AgendaType;
.super Ljava/lang/Enum;
.source "AgendaType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/AgendaType$ProtoAdapter_AgendaType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/agenda/AgendaType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/agenda/AgendaType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/AgendaType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENT:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum AVAILABILITY_OVERRIDE:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum BOOKED_SERVICE:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum BUSINESS:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum CLIENT:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum CONFIRMATION:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum CONFIRMATION_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum HOURS:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum HOURS_COLLECTION:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum INSTRUMENT:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum PAYMENT:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum PLACEHOLDER_EVENT:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum REMINDER_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum SERVICE:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum STAFF:Lcom/squareup/protos/agenda/AgendaType;

.field public static final enum WIDGET:Lcom/squareup/protos/agenda/AgendaType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v1, 0x0

    const-string v2, "SERVICE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->SERVICE:Lcom/squareup/protos/agenda/AgendaType;

    .line 13
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v2, 0x1

    const-string v3, "APPOINTMENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->APPOINTMENT:Lcom/squareup/protos/agenda/AgendaType;

    .line 15
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v3, 0x2

    const-string v4, "CLIENT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->CLIENT:Lcom/squareup/protos/agenda/AgendaType;

    .line 17
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v4, 0x3

    const-string v5, "STAFF"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->STAFF:Lcom/squareup/protos/agenda/AgendaType;

    .line 19
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v5, 0x4

    const-string v6, "PLACEHOLDER_EVENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->PLACEHOLDER_EVENT:Lcom/squareup/protos/agenda/AgendaType;

    .line 21
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v6, 0x5

    const-string v7, "BUSINESS"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->BUSINESS:Lcom/squareup/protos/agenda/AgendaType;

    .line 23
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v7, 0x6

    const-string v8, "BOOKED_SERVICE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->BOOKED_SERVICE:Lcom/squareup/protos/agenda/AgendaType;

    .line 25
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/4 v8, 0x7

    const-string v9, "WIDGET"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->WIDGET:Lcom/squareup/protos/agenda/AgendaType;

    .line 27
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/16 v9, 0x8

    const-string v10, "HOURS"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->HOURS:Lcom/squareup/protos/agenda/AgendaType;

    .line 29
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/16 v10, 0x9

    const-string v11, "HOURS_COLLECTION"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->HOURS_COLLECTION:Lcom/squareup/protos/agenda/AgendaType;

    .line 31
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/16 v11, 0xa

    const-string v12, "INSTRUMENT"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->INSTRUMENT:Lcom/squareup/protos/agenda/AgendaType;

    .line 33
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/16 v12, 0xb

    const-string v13, "PAYMENT"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->PAYMENT:Lcom/squareup/protos/agenda/AgendaType;

    .line 35
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/16 v13, 0xc

    const-string v14, "CONFIRMATION"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->CONFIRMATION:Lcom/squareup/protos/agenda/AgendaType;

    .line 37
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/16 v14, 0xd

    const-string v15, "CONFIRMATION_MESSAGE"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->CONFIRMATION_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

    .line 39
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const/16 v15, 0xe

    const-string v14, "REMINDER_MESSAGE"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->REMINDER_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

    .line 41
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType;

    const-string v14, "AVAILABILITY_OVERRIDE"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/agenda/AgendaType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->AVAILABILITY_OVERRIDE:Lcom/squareup/protos/agenda/AgendaType;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/protos/agenda/AgendaType;

    .line 10
    sget-object v13, Lcom/squareup/protos/agenda/AgendaType;->SERVICE:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->APPOINTMENT:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->CLIENT:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->STAFF:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->PLACEHOLDER_EVENT:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->BUSINESS:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->BOOKED_SERVICE:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->WIDGET:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->HOURS:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->HOURS_COLLECTION:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->INSTRUMENT:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->PAYMENT:Lcom/squareup/protos/agenda/AgendaType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->CONFIRMATION:Lcom/squareup/protos/agenda/AgendaType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->CONFIRMATION_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->REMINDER_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/agenda/AgendaType;->AVAILABILITY_OVERRIDE:Lcom/squareup/protos/agenda/AgendaType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->$VALUES:[Lcom/squareup/protos/agenda/AgendaType;

    .line 43
    new-instance v0, Lcom/squareup/protos/agenda/AgendaType$ProtoAdapter_AgendaType;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/AgendaType$ProtoAdapter_AgendaType;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/AgendaType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lcom/squareup/protos/agenda/AgendaType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/agenda/AgendaType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 71
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->AVAILABILITY_OVERRIDE:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 70
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->REMINDER_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 69
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->CONFIRMATION_MESSAGE:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 68
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->CONFIRMATION:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 67
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->PAYMENT:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 66
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->INSTRUMENT:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 65
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->HOURS_COLLECTION:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 64
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->HOURS:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 63
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->WIDGET:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 62
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->BOOKED_SERVICE:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 61
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->BUSINESS:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 60
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->PLACEHOLDER_EVENT:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 59
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->STAFF:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 58
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->CLIENT:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 57
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->APPOINTMENT:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    .line 56
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/agenda/AgendaType;->SERVICE:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/agenda/AgendaType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/agenda/AgendaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/agenda/AgendaType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/agenda/AgendaType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/agenda/AgendaType;->$VALUES:[Lcom/squareup/protos/agenda/AgendaType;

    invoke-virtual {v0}, [Lcom/squareup/protos/agenda/AgendaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/agenda/AgendaType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/squareup/protos/agenda/AgendaType;->value:I

    return v0
.end method
