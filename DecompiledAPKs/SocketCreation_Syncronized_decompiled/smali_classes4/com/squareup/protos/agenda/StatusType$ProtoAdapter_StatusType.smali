.class final Lcom/squareup/protos/agenda/StatusType$ProtoAdapter_StatusType;
.super Lcom/squareup/wire/EnumAdapter;
.source "StatusType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/StatusType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_StatusType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/agenda/StatusType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 56
    const-class v0, Lcom/squareup/protos/agenda/StatusType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/agenda/StatusType;
    .locals 0

    .line 61
    invoke-static {p1}, Lcom/squareup/protos/agenda/StatusType;->fromValue(I)Lcom/squareup/protos/agenda/StatusType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 54
    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/StatusType$ProtoAdapter_StatusType;->fromValue(I)Lcom/squareup/protos/agenda/StatusType;

    move-result-object p1

    return-object p1
.end method
