.class final Lcom/squareup/protos/agenda/ApplicationError$ErrorType$ProtoAdapter_ErrorType;
.super Lcom/squareup/wire/EnumAdapter;
.source "ApplicationError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ApplicationError$ErrorType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/agenda/ApplicationError$ErrorType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 158
    const-class v0, Lcom/squareup/protos/agenda/ApplicationError$ErrorType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/agenda/ApplicationError$ErrorType;
    .locals 0

    .line 163
    invoke-static {p1}, Lcom/squareup/protos/agenda/ApplicationError$ErrorType;->fromValue(I)Lcom/squareup/protos/agenda/ApplicationError$ErrorType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/ApplicationError$ErrorType$ProtoAdapter_ErrorType;->fromValue(I)Lcom/squareup/protos/agenda/ApplicationError$ErrorType;

    move-result-object p1

    return-object p1
.end method
