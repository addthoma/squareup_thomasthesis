.class final Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateMerchantFeedbackRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreateMerchantFeedbackRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 232
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 259
    new-instance v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;-><init>()V

    .line 260
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 261
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 270
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 268
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    goto :goto_0

    .line 267
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->installation_identity(Lcom/squareup/protos/agenda/ClientInstallationIdentity;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    goto :goto_0

    .line 266
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->rating(Ljava/lang/Integer;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    goto :goto_0

    .line 265
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->feedback(Ljava/lang/String;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    goto :goto_0

    .line 264
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/agenda/UnitIdentifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->current_unit(Lcom/squareup/protos/agenda/UnitIdentifier;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    goto :goto_0

    .line 263
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    goto :goto_0

    .line 274
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 275
    invoke-virtual {v0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->build()Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 230
    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 248
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 249
    sget-object v0, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 250
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 251
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 252
    sget-object v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 253
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 254
    invoke-virtual {p2}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 230
    check-cast p2, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;)I
    .locals 4

    .line 237
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    const/4 v3, 0x2

    .line 238
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    const/4 v3, 0x3

    .line 239
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 240
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    const/4 v3, 0x5

    .line 241
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x6

    .line 242
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 230
    check-cast p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;->encodedSize(Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;
    .locals 2

    .line 280
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->newBuilder()Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    move-result-object p1

    .line 281
    iget-object v0, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/agenda/UnitIdentifier;

    iput-object v0, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 282
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    iput-object v0, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    .line 283
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 284
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 285
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->build()Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 230
    check-cast p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;->redact(Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    move-result-object p1

    return-object p1
.end method
