.class public final Lcom/squareup/protos/agenda/GetPersonInfoResponse;
.super Lcom/squareup/wire/Message;
.source "GetPersonInfoResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/GetPersonInfoResponse$ProtoAdapter_GetPersonInfoResponse;,
        Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/GetPersonInfoResponse;",
        "Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/GetPersonInfoResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_PERSON_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final employments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.Employment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/Employment;",
            ">;"
        }
    .end annotation
.end field

.field public final person_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$ProtoAdapter_GetPersonInfoResponse;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse$ProtoAdapter_GetPersonInfoResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/Employment;",
            ">;)V"
        }
    .end annotation

    .line 56
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/Employment;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 61
    sget-object v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 62
    iput-object p1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->email:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->person_token:Ljava/lang/String;

    const-string p1, "employments"

    .line 64
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->employments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 80
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/GetPersonInfoResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 81
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/GetPersonInfoResponse;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->email:Ljava/lang/String;

    .line 83
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->person_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->person_token:Ljava/lang/String;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->employments:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->employments:Ljava/util/List;

    .line 85
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 90
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->email:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->employments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;
    .locals 2

    .line 69
    new-instance v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;-><init>()V

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->email:Ljava/lang/String;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->person_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->person_token:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->employments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->employments:Ljava/util/List;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->newBuilder()Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->email:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", person_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->person_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->employments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", employments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;->employments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetPersonInfoResponse{"

    .line 107
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
