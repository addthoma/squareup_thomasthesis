.class public final Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateMerchantFeedbackResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse;",
        "Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse$Builder;->build()Lcom/squareup/protos/agenda/CreateMerchantFeedbackResponse;

    move-result-object v0

    return-object v0
.end method
