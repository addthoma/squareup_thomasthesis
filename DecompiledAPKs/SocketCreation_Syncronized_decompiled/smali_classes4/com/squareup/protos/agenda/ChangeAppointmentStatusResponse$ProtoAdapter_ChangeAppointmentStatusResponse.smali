.class final Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$ProtoAdapter_ChangeAppointmentStatusResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ChangeAppointmentStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ChangeAppointmentStatusResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 93
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 111
    new-instance v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;-><init>()V

    .line 112
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 113
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 117
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 115
    :cond_0
    sget-object v3, Lcom/squareup/protos/agenda/ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/agenda/ResponseStatus;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->status(Lcom/squareup/protos/agenda/ResponseStatus;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 122
    invoke-virtual {v0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->build()Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 91
    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$ProtoAdapter_ChangeAppointmentStatusResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 105
    sget-object v0, Lcom/squareup/protos/agenda/ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;->status:Lcom/squareup/protos/agenda/ResponseStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 106
    invoke-virtual {p2}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 91
    check-cast p2, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$ProtoAdapter_ChangeAppointmentStatusResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;)I
    .locals 3

    .line 98
    sget-object v0, Lcom/squareup/protos/agenda/ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;->status:Lcom/squareup/protos/agenda/ResponseStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 99
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 91
    check-cast p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$ProtoAdapter_ChangeAppointmentStatusResponse;->encodedSize(Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;
    .locals 2

    .line 127
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;->newBuilder()Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;

    move-result-object p1

    .line 128
    iget-object v0, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->status:Lcom/squareup/protos/agenda/ResponseStatus;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/agenda/ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->status:Lcom/squareup/protos/agenda/ResponseStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/agenda/ResponseStatus;

    iput-object v0, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->status:Lcom/squareup/protos/agenda/ResponseStatus;

    .line 129
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$Builder;->build()Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 91
    check-cast p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse$ProtoAdapter_ChangeAppointmentStatusResponse;->redact(Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusResponse;

    move-result-object p1

    return-object p1
.end method
