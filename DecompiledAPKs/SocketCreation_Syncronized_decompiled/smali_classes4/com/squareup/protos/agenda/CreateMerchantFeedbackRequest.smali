.class public final Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;
.super Lcom/squareup/wire/Message;
.source "CreateMerchantFeedbackRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;,
        Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;",
        "Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FEEDBACK:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_RATING:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.UnitIdentifier#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final feedback:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.ClientInstallationIdentity#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final rating:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final timestamp:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$ProtoAdapter_CreateMerchantFeedbackRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->DEFAULT_RATING:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/agenda/ClientInstallationIdentity;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 8

    .line 89
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/agenda/ClientInstallationIdentity;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/agenda/ClientInstallationIdentity;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    .line 97
    iput-object p2, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 98
    iput-object p3, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    .line 99
    iput-object p4, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    .line 100
    iput-object p5, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    .line 101
    iput-object p6, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 120
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 121
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    iget-object v3, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    iget-object v3, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 128
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 133
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/UnitIdentifier;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 142
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    .locals 2

    .line 106
    new-instance v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->id:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    iput-object v1, v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->feedback:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->rating:Ljava/lang/Integer;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    iput-object v1, v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->newBuilder()Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    if-eqz v1, :cond_1

    const-string v1, ", current_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", feedback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->feedback:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", rating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->rating:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    if-eqz v1, :cond_4

    const-string v1, ", installation_identity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_5

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateMerchantFeedbackRequest{"

    .line 156
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
