.class public final Lcom/squareup/protos/agenda/Unit$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Unit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/Unit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/Unit;",
        "Lcom/squareup/protos/agenda/Unit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public display_name:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public time_zone:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 147
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/agenda/Unit$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/agenda/Unit$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/agenda/Unit;
    .locals 8

    .line 183
    new-instance v7, Lcom/squareup/protos/agenda/Unit;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/agenda/Unit$Builder;->display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/agenda/Unit$Builder;->phone_number:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/agenda/Unit$Builder;->time_zone:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/agenda/Unit$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/agenda/Unit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Unit$Builder;->build()Lcom/squareup/protos/agenda/Unit;

    move-result-object v0

    return-object v0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/agenda/Unit$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/agenda/Unit$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/agenda/Unit$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/agenda/Unit$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public time_zone(Ljava/lang/String;)Lcom/squareup/protos/agenda/Unit$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/agenda/Unit$Builder;->time_zone:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/Unit$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/agenda/Unit$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
