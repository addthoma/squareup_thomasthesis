.class final Lcom/squareup/protos/agenda/Business$LocationType$ProtoAdapter_LocationType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Business.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/Business$LocationType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LocationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/agenda/Business$LocationType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 747
    const-class v0, Lcom/squareup/protos/agenda/Business$LocationType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/agenda/Business$LocationType;
    .locals 0

    .line 752
    invoke-static {p1}, Lcom/squareup/protos/agenda/Business$LocationType;->fromValue(I)Lcom/squareup/protos/agenda/Business$LocationType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 745
    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/Business$LocationType$ProtoAdapter_LocationType;->fromValue(I)Lcom/squareup/protos/agenda/Business$LocationType;

    move-result-object p1

    return-object p1
.end method
