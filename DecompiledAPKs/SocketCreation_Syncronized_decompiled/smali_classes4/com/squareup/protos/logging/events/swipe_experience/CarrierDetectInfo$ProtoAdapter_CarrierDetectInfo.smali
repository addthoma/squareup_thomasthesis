.class final Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CarrierDetectInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CarrierDetectInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 457
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 498
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;-><init>()V

    .line 499
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 500
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 516
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 514
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->in_packet_runtime_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 513
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 512
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 511
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->allow_restart(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 510
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_avg(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 509
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_threshold(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 508
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_avg(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 507
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_threshold(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 506
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_time_in_us(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto :goto_0

    .line 505
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_runtime_in_us(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto/16 :goto_0

    .line 504
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->is_early_packet(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto/16 :goto_0

    .line 503
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->num_samples(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto/16 :goto_0

    .line 502
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_sample(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    goto/16 :goto_0

    .line 520
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 521
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 455
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 480
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 481
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 482
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 483
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 484
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 485
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 486
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 487
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 488
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 489
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 490
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 491
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 492
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 493
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 455
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)I
    .locals 4

    .line 462
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 463
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 464
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 465
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    const/4 v3, 0x5

    .line 466
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 467
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 468
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    const/16 v3, 0x8

    .line 469
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    const/16 v3, 0x9

    .line 470
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 471
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    const/16 v3, 0xb

    .line 472
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    const/16 v3, 0xc

    .line 473
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    const/16 v3, 0xd

    .line 474
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 455
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
    .locals 2

    .line 526
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    move-result-object p1

    .line 527
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 528
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 529
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 530
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 455
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;->redact(Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    move-result-object p1

    return-object p1
.end method
