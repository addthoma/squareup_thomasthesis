.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;
.super Ljava/lang/Enum;
.source "Gen2DemodInfo.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DemodResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult$ProtoAdapter_DemodResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BAD_CHARACTER:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum DECODE_FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum LRC_SELF_PARITY:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum MISSING_LRC:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

.field public static final enum UNKNOWN_ERROR_CODE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 108
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 114
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v2, 0x1

    const-string v3, "FAILURE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 120
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v3, 0x2

    const-string v4, "START_SENTINEL_NOT_FOUND"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 126
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v4, 0x3

    const-string v5, "END_SENTINEL_NOT_FOUND"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 135
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v5, 0x4

    const-string v6, "DECODE_FAILURE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->DECODE_FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 142
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v6, 0x5

    const-string v7, "BAD_CHARACTER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->BAD_CHARACTER:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 148
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v7, 0x6

    const-string v8, "MISSING_LRC"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->MISSING_LRC:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 154
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/4 v8, 0x7

    const-string v9, "LRC_SELF_PARITY"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->LRC_SELF_PARITY:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 161
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/16 v9, 0x8

    const-string v10, "CARD_DATA_FAILED_LRC_CHECK"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 167
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/16 v10, 0x9

    const-string v11, "UNKNOWN_ERROR_CODE"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->UNKNOWN_ERROR_CODE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 103
    sget-object v11, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->DECODE_FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->BAD_CHARACTER:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->MISSING_LRC:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->LRC_SELF_PARITY:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->UNKNOWN_ERROR_CODE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    .line 169
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult$ProtoAdapter_DemodResult;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult$ProtoAdapter_DemodResult;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 174
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 191
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->UNKNOWN_ERROR_CODE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 190
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->CARD_DATA_FAILED_LRC_CHECK:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 189
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->LRC_SELF_PARITY:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 188
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->MISSING_LRC:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 187
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->BAD_CHARACTER:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 186
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->DECODE_FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 185
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->END_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 184
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 183
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->FAILURE:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    .line 182
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;
    .locals 1

    .line 103
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;
    .locals 1

    .line 103
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 198
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->value:I

    return v0
.end method
