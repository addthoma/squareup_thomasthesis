.class public final Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;
.super Lcom/squareup/wire/Message;
.source "CompleteActionItemRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;,
        Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_NAME:Lcom/squareup/protos/checklist/common/ActionItemName;

.field private static final serialVersionUID:J


# instance fields
.field public final name:Lcom/squareup/protos/checklist/common/ActionItemName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.common.ActionItemName#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$ProtoAdapter_CompleteActionItemRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->UNKNOWN:Lcom/squareup/protos/checklist/common/ActionItemName;

    sput-object v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->DEFAULT_NAME:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/checklist/common/ActionItemName;)V
    .locals 1

    .line 34
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;-><init>(Lcom/squareup/protos/checklist/common/ActionItemName;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/checklist/common/ActionItemName;Lokio/ByteString;)V
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 39
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 53
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 54
    :cond_1
    check-cast p1, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    .line 55
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    iget-object p1, p1, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 56
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 61
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 63
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/common/ActionItemName;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 65
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;
    .locals 2

    .line 44
    new-instance v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;-><init>()V

    .line 45
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    iput-object v1, v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->newBuilder()Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CompleteActionItemRequest{"

    .line 74
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
