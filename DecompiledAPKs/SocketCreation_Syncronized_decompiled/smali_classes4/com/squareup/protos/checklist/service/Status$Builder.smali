.class public final Lcom/squareup/protos/checklist/service/Status$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Status.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/service/Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/checklist/service/Status;",
        "Lcom/squareup/protos/checklist/service/Status$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/checklist/service/Status;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/checklist/service/Status;

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/checklist/service/Status$Builder;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/checklist/service/Status;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/checklist/service/Status$StatusCode;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/Status$Builder;->build()Lcom/squareup/protos/checklist/service/Status;

    move-result-object v0

    return-object v0
.end method

.method public statusCode(Lcom/squareup/protos/checklist/service/Status$StatusCode;)Lcom/squareup/protos/checklist/service/Status$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/Status$Builder;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/service/Status$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/Status$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
