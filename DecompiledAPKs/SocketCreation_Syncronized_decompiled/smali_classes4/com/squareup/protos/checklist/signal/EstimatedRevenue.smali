.class public final enum Lcom/squareup/protos/checklist/signal/EstimatedRevenue;
.super Ljava/lang/Enum;
.source "EstimatedRevenue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/signal/EstimatedRevenue$ProtoAdapter_EstimatedRevenue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/signal/EstimatedRevenue;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/signal/EstimatedRevenue;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum REVENUE_0_TO_10000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_10000000_TO_20000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_1000000_TO_5000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_100000_TO_250000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_10000_TO_50000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_20000000_PLUS:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_250000_TO_500000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_5000000_TO_10000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_500000_TO_1000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_50000_TO_100000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final enum REVENUE_DO_NOT_USE:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 11
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v1, 0x0

    const-string v2, "REVENUE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_DO_NOT_USE:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 13
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v2, 0x1

    const-string v3, "REVENUE_0_TO_10000"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_0_TO_10000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 15
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v3, 0x2

    const-string v4, "REVENUE_10000_TO_50000"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_10000_TO_50000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 17
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v4, 0x3

    const-string v5, "REVENUE_50000_TO_100000"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_50000_TO_100000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 19
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v5, 0x4

    const-string v6, "REVENUE_100000_TO_250000"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_100000_TO_250000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 21
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v6, 0x5

    const-string v7, "REVENUE_250000_TO_500000"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_250000_TO_500000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 23
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v7, 0x6

    const-string v8, "REVENUE_500000_TO_1000000"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_500000_TO_1000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 25
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/4 v8, 0x7

    const-string v9, "REVENUE_1000000_TO_5000000"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_1000000_TO_5000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 27
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/16 v9, 0x8

    const-string v10, "REVENUE_5000000_TO_10000000"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_5000000_TO_10000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 29
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/16 v10, 0x9

    const-string v11, "REVENUE_10000000_TO_20000000"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_10000000_TO_20000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 31
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/16 v11, 0xa

    const-string v12, "REVENUE_20000000_PLUS"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_20000000_PLUS:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 10
    sget-object v12, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_DO_NOT_USE:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_0_TO_10000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_10000_TO_50000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_50000_TO_100000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_100000_TO_250000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_250000_TO_500000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_500000_TO_1000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_1000000_TO_5000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_5000000_TO_10000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_10000000_TO_20000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_20000000_PLUS:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->$VALUES:[Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 33
    new-instance v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue$ProtoAdapter_EstimatedRevenue;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue$ProtoAdapter_EstimatedRevenue;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/signal/EstimatedRevenue;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 56
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_20000000_PLUS:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 55
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_10000000_TO_20000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 54
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_5000000_TO_10000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 53
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_1000000_TO_5000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 52
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_500000_TO_1000000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 51
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_250000_TO_500000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 50
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_100000_TO_250000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 49
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_50000_TO_100000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 48
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_10000_TO_50000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 47
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_0_TO_10000:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    .line 46
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_DO_NOT_USE:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/EstimatedRevenue;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/signal/EstimatedRevenue;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->$VALUES:[Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->value:I

    return v0
.end method
