.class final Lcom/squareup/protos/checklist/signal/EstimatedUsageType$ProtoAdapter_EstimatedUsageType;
.super Lcom/squareup/wire/EnumAdapter;
.source "EstimatedUsageType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/signal/EstimatedUsageType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EstimatedUsageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/checklist/signal/EstimatedUsageType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 56
    const-class v0, Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/checklist/signal/EstimatedUsageType;
    .locals 0

    .line 61
    invoke-static {p1}, Lcom/squareup/protos/checklist/signal/EstimatedUsageType;->fromValue(I)Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 54
    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/signal/EstimatedUsageType$ProtoAdapter_EstimatedUsageType;->fromValue(I)Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    move-result-object p1

    return-object p1
.end method
