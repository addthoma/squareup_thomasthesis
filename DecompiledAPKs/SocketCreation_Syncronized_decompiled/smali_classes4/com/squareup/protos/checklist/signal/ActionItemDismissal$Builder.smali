.class public final Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ActionItemDismissal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/signal/ActionItemDismissal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/checklist/signal/ActionItemDismissal;",
        "Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public action_item:Ljava/lang/String;

.field public dismissed_at:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public action_item(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;->action_item:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/checklist/signal/ActionItemDismissal;
    .locals 4

    .line 108
    new-instance v0, Lcom/squareup/protos/checklist/signal/ActionItemDismissal;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;->action_item:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/checklist/signal/ActionItemDismissal;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;->build()Lcom/squareup/protos/checklist/signal/ActionItemDismissal;

    move-result-object v0

    return-object v0
.end method

.method public dismissed_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/ActionItemDismissal$Builder;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
