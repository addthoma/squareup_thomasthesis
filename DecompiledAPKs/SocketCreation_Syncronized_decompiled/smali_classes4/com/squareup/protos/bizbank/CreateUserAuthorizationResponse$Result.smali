.class public final enum Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;
.super Ljava/lang/Enum;
.source "CreateUserAuthorizationResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result$ProtoAdapter_Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

.field public static final enum ERROR:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

.field public static final enum EXPIRED_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

.field public static final enum INVALID_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

.field public static final enum SUCCESS:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 99
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->DO_NOT_USE:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 101
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->SUCCESS:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 103
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    const/4 v3, 0x2

    const-string v4, "ERROR"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->ERROR:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 105
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    const/4 v4, 0x3

    const-string v5, "INVALID_TOKEN"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->INVALID_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 107
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    const/4 v5, 0x4

    const-string v6, "EXPIRED_TOKEN"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->EXPIRED_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 98
    sget-object v6, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->DO_NOT_USE:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->SUCCESS:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->ERROR:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->INVALID_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->EXPIRED_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->$VALUES:[Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 109
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result$ProtoAdapter_Result;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result$ProtoAdapter_Result;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 114
    iput p3, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 126
    :cond_0
    sget-object p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->EXPIRED_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object p0

    .line 125
    :cond_1
    sget-object p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->INVALID_TOKEN:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object p0

    .line 124
    :cond_2
    sget-object p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->ERROR:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object p0

    .line 123
    :cond_3
    sget-object p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->SUCCESS:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object p0

    .line 122
    :cond_4
    sget-object p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->DO_NOT_USE:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;
    .locals 1

    .line 98
    const-class v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;
    .locals 1

    .line 98
    sget-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->$VALUES:[Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    invoke-virtual {v0}, [Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 133
    iget v0, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->value:I

    return v0
.end method
