.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnifiedActivitiesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_activity_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

.field public min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 328
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 329
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;
    .locals 5

    .line 360
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;-><init>(Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 321
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    move-result-object v0

    return-object v0
.end method

.method public include_activity_types(Ljava/util/List;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;)",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;"
        }
    .end annotation

    .line 337
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    return-object p0
.end method

.method public max_latest_event_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public min_latest_event_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
