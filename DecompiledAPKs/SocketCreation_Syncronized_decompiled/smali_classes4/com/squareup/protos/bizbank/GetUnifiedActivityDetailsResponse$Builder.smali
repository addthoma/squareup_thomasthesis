.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnifiedActivityDetailsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;
    .locals 4

    .line 113
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;-><init>(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->error_message:Ljava/lang/String;

    const/4 p1, 0x0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    return-object p0
.end method

.method public response(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    const/4 p1, 0x0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method
