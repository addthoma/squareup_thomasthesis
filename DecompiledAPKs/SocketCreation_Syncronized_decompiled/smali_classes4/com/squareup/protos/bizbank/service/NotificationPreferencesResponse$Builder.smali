.class public final Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationPreferencesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
        "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public email:Ljava/lang/String;

.field public send_card_declined_notifications:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->send_card_declined_notifications:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->email:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->build()Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;

    move-result-object v0

    return-object v0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public send_card_declined_notifications(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->send_card_declined_notifications:Ljava/lang/Boolean;

    return-object p0
.end method
