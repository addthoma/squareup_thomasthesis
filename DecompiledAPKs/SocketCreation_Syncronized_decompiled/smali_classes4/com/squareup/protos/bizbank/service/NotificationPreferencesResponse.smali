.class public final Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;
.super Lcom/squareup/wire/Message;
.source "NotificationPreferencesResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$ProtoAdapter_NotificationPreferencesResponse;,
        Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
        "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_SEND_CARD_DECLINED_NOTIFICATIONS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final send_card_declined_notifications:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$ProtoAdapter_NotificationPreferencesResponse;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$ProtoAdapter_NotificationPreferencesResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->DEFAULT_SEND_CARD_DECLINED_NOTIFICATIONS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 1

    .line 49
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->send_card_declined_notifications:Ljava/lang/Boolean;

    .line 56
    iput-object p2, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->email:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 71
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 72
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->send_card_declined_notifications:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->send_card_declined_notifications:Ljava/lang/Boolean;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->email:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->email:Ljava/lang/String;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->send_card_declined_notifications:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->email:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 85
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->send_card_declined_notifications:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->send_card_declined_notifications:Ljava/lang/Boolean;

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->email:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->newBuilder()Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->send_card_declined_notifications:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", send_card_declined_notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->send_card_declined_notifications:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;->email:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "NotificationPreferencesResponse{"

    .line 95
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
