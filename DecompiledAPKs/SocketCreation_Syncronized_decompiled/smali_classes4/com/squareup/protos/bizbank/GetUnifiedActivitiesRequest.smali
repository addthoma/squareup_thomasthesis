.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;
.super Lcom/squareup/wire/Message;
.source "GetUnifiedActivitiesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ProtoAdapter_GetUnifiedActivitiesRequest;,
        Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;,
        Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;,
        Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVITY_STATE_CATEGORY:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.GetUnifiedActivitiesRequest$ActivityStateCategory#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final batch_request:Lcom/squareup/protos/bizbank/BatchRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.BatchRequest#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.GetUnifiedActivitiesRequest$Filters#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ProtoAdapter_GetUnifiedActivitiesRequest;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ProtoAdapter_GetUnifiedActivitiesRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->PENDING:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->DEFAULT_ACTIVITY_STATE_CATEGORY:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;Lcom/squareup/protos/bizbank/BatchRequest;)V
    .locals 6

    .line 71
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;Lcom/squareup/protos/bizbank/BatchRequest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;Lcom/squareup/protos/bizbank/BatchRequest;Lokio/ByteString;)V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unit_token:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    .line 80
    iput-object p3, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    .line 81
    iput-object p4, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 98
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 99
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unit_token:Ljava/lang/String;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unit_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/BatchRequest;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 116
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->unit_token:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    if-eqz v1, :cond_1

    const-string v1, ", activity_state_category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->activity_state_category:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$ActivityStateCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    if-eqz v1, :cond_2

    const-string v1, ", filters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->filters:Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    if-eqz v1, :cond_3

    const-string v1, ", batch_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;->batch_request:Lcom/squareup/protos/bizbank/BatchRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetUnifiedActivitiesRequest{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
