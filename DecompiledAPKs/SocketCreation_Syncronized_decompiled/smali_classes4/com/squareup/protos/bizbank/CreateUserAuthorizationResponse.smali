.class public final Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;
.super Lcom/squareup/wire/Message;
.source "CreateUserAuthorizationResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$ProtoAdapter_CreateUserAuthorizationResponse;,
        Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;,
        Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RESULT:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

.field private static final serialVersionUID:J


# instance fields
.field public final result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.CreateUserAuthorizationResponse$Result#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$ProtoAdapter_CreateUserAuthorizationResponse;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$ProtoAdapter_CreateUserAuthorizationResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->DO_NOT_USE:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    sput-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->DEFAULT_RESULT:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;)V
    .locals 1

    .line 35
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;-><init>(Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;Lokio/ByteString;)V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 40
    iput-object p1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 54
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 55
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 57
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 62
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 66
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;-><init>()V

    .line 46
    iget-object v1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->newBuilder()Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    if-eqz v1, :cond_0

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateUserAuthorizationResponse{"

    .line 75
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
