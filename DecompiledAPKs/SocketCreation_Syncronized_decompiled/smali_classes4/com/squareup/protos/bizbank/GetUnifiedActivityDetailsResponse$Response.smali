.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;
.super Lcom/squareup/wire/Message;
.source "GetUnifiedActivityDetailsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Response"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;,
        Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivity#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivityDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 118
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V
    .locals 1

    .line 142
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lokio/ByteString;)V
    .locals 1

    .line 147
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    .line 149
    iput-object p2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 164
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 165
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    .line 167
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    .line 168
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 173
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 175
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 178
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;
    .locals 2

    .line 154
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;-><init>()V

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    if-eqz v1, :cond_0

    const-string v1, ", unified_activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    if-eqz v1, :cond_1

    const-string v1, ", unified_activity_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Response{"

    .line 188
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
