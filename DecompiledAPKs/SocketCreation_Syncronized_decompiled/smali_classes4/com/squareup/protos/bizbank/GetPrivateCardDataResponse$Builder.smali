.class public final Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetPrivateCardDataResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

.field public private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;
    .locals 4

    .line 115
    new-instance v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;-><init>(Lcom/squareup/protos/bizbank/PrivateCardData;Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->build()Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;)Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    const/4 p1, 0x0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    return-object p0
.end method

.method public private_card_info(Lcom/squareup/protos/bizbank/PrivateCardData;)Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    const/4 p1, 0x0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    return-object p0
.end method
