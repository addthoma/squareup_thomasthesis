.class public final Lcom/squareup/protos/eventstream/v1/Source$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Source.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Source;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Source;",
        "Lcom/squareup/protos/eventstream/v1/Source$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application:Lcom/squareup/protos/eventstream/v1/Application;

.field public city:Ljava/lang/String;

.field public coordinates:Lcom/squareup/protos/common/location/Coordinates;

.field public device:Lcom/squareup/protos/eventstream/v1/Device;

.field public ip_address:Ljava/lang/String;

.field public os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

.field public reader:Lcom/squareup/protos/eventstream/v1/Reader;

.field public region:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public user_agent:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 226
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public application(Lcom/squareup/protos/eventstream/v1/Application;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application:Lcom/squareup/protos/eventstream/v1/Application;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/eventstream/v1/Source;
    .locals 13

    .line 292
    new-instance v12, Lcom/squareup/protos/eventstream/v1/Source;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->type:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application:Lcom/squareup/protos/eventstream/v1/Application;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    iget-object v4, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device:Lcom/squareup/protos/eventstream/v1/Device;

    iget-object v5, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    iget-object v6, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->user_agent:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->ip_address:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    iget-object v9, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->city:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->region:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/eventstream/v1/Source;-><init>(Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Application;Lcom/squareup/protos/eventstream/v1/OperatingSystem;Lcom/squareup/protos/eventstream/v1/Device;Lcom/squareup/protos/eventstream/v1/Reader;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Coordinates;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 205
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->build()Lcom/squareup/protos/eventstream/v1/Source;

    move-result-object v0

    return-object v0
.end method

.method public city(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->city:Ljava/lang/String;

    return-object p0
.end method

.method public coordinates(Lcom/squareup/protos/common/location/Coordinates;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    return-object p0
.end method

.method public device(Lcom/squareup/protos/eventstream/v1/Device;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device:Lcom/squareup/protos/eventstream/v1/Device;

    return-object p0
.end method

.method public ip_address(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->ip_address:Ljava/lang/String;

    return-object p0
.end method

.method public os(Lcom/squareup/protos/eventstream/v1/OperatingSystem;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    return-object p0
.end method

.method public reader(Lcom/squareup/protos/eventstream/v1/Reader;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    return-object p0
.end method

.method public region(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->region:Ljava/lang/String;

    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 235
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->type:Ljava/lang/String;

    return-object p0
.end method

.method public user_agent(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->user_agent:Ljava/lang/String;

    return-object p0
.end method
