.class public final Lcom/squareup/protos/inventory/InventoryAdjustment;
.super Lcom/squareup/wire/Message;
.source "InventoryAdjustment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/inventory/InventoryAdjustment$ProtoAdapter_InventoryAdjustment;,
        Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/inventory/InventoryAdjustment;",
        "Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/inventory/InventoryAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADJUSTMENT_ID:Ljava/lang/Integer;

.field public static final DEFAULT_ADJUSTMENT_TYPE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public static final DEFAULT_MEMO:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_CREATE_UNIQUE_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adjustment_id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.inventory.InventoryAdjustmentType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final inventory_adjustment_line:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.inventory.InventoryAdjustmentLine#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
            ">;"
        }
    .end annotation
.end field

.field public final memo:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final payment_create_unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustment$ProtoAdapter_InventoryAdjustment;

    invoke-direct {v0}, Lcom/squareup/protos/inventory/InventoryAdjustment$ProtoAdapter_InventoryAdjustment;-><init>()V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustment;->DEFAULT_ADJUSTMENT_ID:Ljava/lang/Integer;

    .line 34
    sget-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustment;->DEFAULT_ADJUSTMENT_TYPE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/inventory/InventoryAdjustmentType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
            ">;",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            ")V"
        }
    .end annotation

    .line 105
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/inventory/InventoryAdjustment;-><init>(Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/inventory/InventoryAdjustmentType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/inventory/InventoryAdjustmentType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
            ">;",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 112
    sget-object v0, Lcom/squareup/protos/inventory/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_id:Ljava/lang/Integer;

    const-string p1, "inventory_adjustment_line"

    .line 114
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->inventory_adjustment_line:Ljava/util/List;

    .line 115
    iput-object p3, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 116
    iput-object p4, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->payment_create_unique_key:Ljava/lang/String;

    .line 117
    iput-object p5, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->memo:Ljava/lang/String;

    .line 118
    iput-object p6, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 137
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/inventory/InventoryAdjustment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 138
    :cond_1
    check-cast p1, Lcom/squareup/protos/inventory/InventoryAdjustment;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/inventory/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_id:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_id:Ljava/lang/Integer;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->inventory_adjustment_line:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustment;->inventory_adjustment_line:Ljava/util/List;

    .line 141
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->payment_create_unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustment;->payment_create_unique_key:Ljava/lang/String;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->memo:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustment;->memo:Ljava/lang/String;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/inventory/InventoryAdjustment;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 145
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 150
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_id:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->inventory_adjustment_line:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->payment_create_unique_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->memo:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 159
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    .locals 2

    .line 123
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;-><init>()V

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_id:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->adjustment_id:Ljava/lang/Integer;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->inventory_adjustment_line:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->inventory_adjustment_line:Ljava/util/List;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->payment_create_unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->payment_create_unique_key:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->memo:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->memo:Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustment;->newBuilder()Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_id:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", adjustment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->inventory_adjustment_line:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", inventory_adjustment_line="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->inventory_adjustment_line:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    if-eqz v1, :cond_2

    const-string v1, ", adjustment_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->payment_create_unique_key:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", payment_create_unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->payment_create_unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->memo:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", memo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->memo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_5

    const-string v1, ", transaction_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InventoryAdjustment{"

    .line 173
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
