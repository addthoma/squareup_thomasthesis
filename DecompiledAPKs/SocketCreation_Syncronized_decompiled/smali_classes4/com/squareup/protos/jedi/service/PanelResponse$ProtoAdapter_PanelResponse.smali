.class final Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PanelResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/PanelResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PanelResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 177
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/jedi/service/PanelResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/jedi/service/PanelResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    new-instance v0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;-><init>()V

    .line 203
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 204
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 212
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 210
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->transition_id(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    goto :goto_0

    .line 209
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->code(Ljava/lang/Integer;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    goto :goto_0

    .line 208
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    goto :goto_0

    .line 207
    :cond_3
    sget-object v3, Lcom/squareup/protos/jedi/service/Panel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/jedi/service/Panel;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->panel(Lcom/squareup/protos/jedi/service/Panel;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    goto :goto_0

    .line 206
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    goto :goto_0

    .line 216
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 217
    invoke-virtual {v0}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->build()Lcom/squareup/protos/jedi/service/PanelResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 175
    invoke-virtual {p0, p1}, Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/jedi/service/PanelResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/jedi/service/PanelResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 192
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 193
    sget-object v0, Lcom/squareup/protos/jedi/service/Panel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    invoke-virtual {p2}, Lcom/squareup/protos/jedi/service/PanelResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 175
    check-cast p2, Lcom/squareup/protos/jedi/service/PanelResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/jedi/service/PanelResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/jedi/service/PanelResponse;)I
    .locals 4

    .line 182
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/jedi/service/Panel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    const/4 v3, 0x2

    .line 183
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x3

    .line 184
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 185
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    const/4 v3, 0x5

    .line 186
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/PanelResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 175
    check-cast p1, Lcom/squareup/protos/jedi/service/PanelResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;->encodedSize(Lcom/squareup/protos/jedi/service/PanelResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/protos/jedi/service/PanelResponse;
    .locals 2

    .line 222
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/PanelResponse;->newBuilder()Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    move-result-object p1

    .line 223
    iget-object v0, p1, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/jedi/service/Panel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/jedi/service/Panel;

    iput-object v0, p1, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    .line 224
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 225
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->build()Lcom/squareup/protos/jedi/service/PanelResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 175
    check-cast p1, Lcom/squareup/protos/jedi/service/PanelResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;->redact(Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/protos/jedi/service/PanelResponse;

    move-result-object p1

    return-object p1
.end method
