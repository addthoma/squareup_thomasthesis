.class public final Lcom/squareup/protos/jedi/service/PanelResponse;
.super Lcom/squareup/wire/Message;
.source "PanelResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;,
        Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        "Lcom/squareup/protos/jedi/service/PanelResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/PanelResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CODE:Ljava/lang/Integer;

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_TRANSITION_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final code:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x4
    .end annotation
.end field

.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final panel:Lcom/squareup/protos/jedi/service/Panel;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.jedi.service.Panel#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final transition_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/PanelResponse$ProtoAdapter_PanelResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/PanelResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/jedi/service/PanelResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/jedi/service/PanelResponse;->DEFAULT_CODE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 7

    .line 66
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/jedi/service/PanelResponse;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/protos/jedi/service/PanelResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    .line 73
    iput-object p2, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    .line 74
    iput-object p3, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    .line 75
    iput-object p4, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    .line 76
    iput-object p5, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 94
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/jedi/service/PanelResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/protos/jedi/service/PanelResponse;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/PanelResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/PanelResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    .line 101
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 106
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/PanelResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/Panel;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 114
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;-><init>()V

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->success:Ljava/lang/Boolean;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->error_message:Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->code:Ljava/lang/Integer;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->transition_id:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/PanelResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/PanelResponse;->newBuilder()Lcom/squareup/protos/jedi/service/PanelResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    if-eqz v1, :cond_1

    const-string v1, ", panel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->code:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", transition_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PanelResponse{"

    .line 127
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
