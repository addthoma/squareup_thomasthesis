.class public final enum Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;
.super Ljava/lang/Enum;
.source "MoneyMovingBlocker.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/ledger/service/MoneyMovingBlocker$ProtoAdapter_MoneyMovingBlocker;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD_DECLINED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field public static final enum CARD_EXPIRED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field public static final enum DEFAULT_UNKNOWN:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field public static final enum EXTERNAL_TRANSACTION_LIMIT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field public static final enum GENERIC_FAILURE:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field public static final enum INCORRECT_EXPECTED_DEPOSIT_AMOUNT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field public static final enum INSUFFICIENT_FUNDS_ON_INSTRUMENT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 17
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->DEFAULT_UNKNOWN:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 22
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v2, 0x1

    const-string v3, "CARD_EXPIRED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->CARD_EXPIRED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 27
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v3, 0x2

    const-string v4, "CARD_DECLINED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->CARD_DECLINED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 32
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v4, 0x3

    const-string v5, "INCORRECT_EXPECTED_DEPOSIT_AMOUNT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->INCORRECT_EXPECTED_DEPOSIT_AMOUNT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 37
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v5, 0x4

    const-string v6, "INSUFFICIENT_FUNDS_ON_INSTRUMENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->INSUFFICIENT_FUNDS_ON_INSTRUMENT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 42
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v6, 0x5

    const-string v7, "GENERIC_FAILURE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->GENERIC_FAILURE:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 48
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v7, 0x6

    const-string v8, "EXTERNAL_TRANSACTION_LIMIT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->EXTERNAL_TRANSACTION_LIMIT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 13
    sget-object v8, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->DEFAULT_UNKNOWN:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->CARD_EXPIRED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->CARD_DECLINED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->INCORRECT_EXPECTED_DEPOSIT_AMOUNT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->INSUFFICIENT_FUNDS_ON_INSTRUMENT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->GENERIC_FAILURE:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->EXTERNAL_TRANSACTION_LIMIT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->$VALUES:[Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 50
    new-instance v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker$ProtoAdapter_MoneyMovingBlocker;

    invoke-direct {v0}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker$ProtoAdapter_MoneyMovingBlocker;-><init>()V

    sput-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput p3, p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 69
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->EXTERNAL_TRANSACTION_LIMIT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0

    .line 68
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->GENERIC_FAILURE:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0

    .line 67
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->INSUFFICIENT_FUNDS_ON_INSTRUMENT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0

    .line 66
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->INCORRECT_EXPECTED_DEPOSIT_AMOUNT:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0

    .line 65
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->CARD_DECLINED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0

    .line 64
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->CARD_EXPIRED:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0

    .line 63
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->DEFAULT_UNKNOWN:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->$VALUES:[Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    invoke-virtual {v0}, [Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 76
    iget v0, p0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->value:I

    return v0
.end method
