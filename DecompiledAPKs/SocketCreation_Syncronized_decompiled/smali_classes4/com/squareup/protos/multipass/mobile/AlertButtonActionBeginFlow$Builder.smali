.class public final Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AlertButtonActionBeginFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
    .locals 3

    .line 103
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;-><init>(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    move-result-object v0

    return-object v0
.end method

.method public flow(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    return-object p0
.end method
