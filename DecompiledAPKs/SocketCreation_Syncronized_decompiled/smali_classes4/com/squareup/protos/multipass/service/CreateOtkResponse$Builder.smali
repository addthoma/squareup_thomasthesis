.class public final Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOtkResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/CreateOtkResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/service/CreateOtkResponse;",
        "Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/multipass/service/Error;

.field public one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/service/CreateOtkResponse;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->error:Lcom/squareup/protos/multipass/service/Error;

    iget-object v2, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/multipass/service/CreateOtkResponse;-><init>(Lcom/squareup/protos/multipass/service/Error;Lcom/squareup/protos/multipass/service/OneTimeKey;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->build()Lcom/squareup/protos/multipass/service/CreateOtkResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/multipass/service/Error;)Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->error:Lcom/squareup/protos/multipass/service/Error;

    return-object p0
.end method

.method public one_time_key(Lcom/squareup/protos/multipass/service/OneTimeKey;)Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    return-object p0
.end method
