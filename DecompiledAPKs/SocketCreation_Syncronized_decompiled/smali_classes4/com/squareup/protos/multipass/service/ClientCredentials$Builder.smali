.class public final Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientCredentials.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/ClientCredentials;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/service/ClientCredentials;",
        "Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public access_token:Ljava/lang/String;

.field public cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

.field public device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

.field public headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

.field public session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

.field public subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public access_token(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/multipass/service/ClientCredentials;
    .locals 9

    .line 214
    new-instance v8, Lcom/squareup/protos/multipass/service/ClientCredentials;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    iget-object v2, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    iget-object v3, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    iget-object v4, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    iget-object v5, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    iget-object v6, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/multipass/service/ClientCredentials;-><init>(Lcom/squareup/protos/multipass/service/DeviceDetails;Lcom/squareup/protos/multipass/service/ClientWebSessionToken;Lcom/squareup/protos/multipass/service/ClientSessionCookie;Lcom/squareup/protos/multipass/service/ClientSessionToken;Lcom/squareup/protos/multipass/service/SessionHeaders;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->build()Lcom/squareup/protos/multipass/service/ClientCredentials;

    move-result-object v0

    return-object v0
.end method

.method public cookie(Lcom/squareup/protos/multipass/service/ClientSessionCookie;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    const/4 p1, 0x0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token:Ljava/lang/String;

    return-object p0
.end method

.method public device_details(Lcom/squareup/protos/multipass/service/DeviceDetails;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    return-object p0
.end method

.method public headers(Lcom/squareup/protos/multipass/service/SessionHeaders;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    const/4 p1, 0x0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token:Ljava/lang/String;

    return-object p0
.end method

.method public session_id(Lcom/squareup/protos/multipass/service/ClientSessionToken;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    const/4 p1, 0x0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token:Ljava/lang/String;

    return-object p0
.end method

.method public subdomain_token(Lcom/squareup/protos/multipass/service/ClientWebSessionToken;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    return-object p0
.end method
