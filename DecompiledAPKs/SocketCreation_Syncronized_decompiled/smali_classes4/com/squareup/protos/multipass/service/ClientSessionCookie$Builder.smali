.class public final Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientSessionCookie.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/ClientSessionCookie;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/service/ClientSessionCookie;",
        "Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public scope:Lcom/squareup/protos/multipass/common/ScopedSession;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/service/ClientSessionCookie;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->value:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;-><init>(Ljava/lang/String;Lcom/squareup/protos/multipass/common/ScopedSession;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->build()Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    move-result-object v0

    return-object v0
.end method

.method public scope(Lcom/squareup/protos/multipass/common/ScopedSession;)Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
