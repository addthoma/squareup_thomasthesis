.class final Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$ProtoAdapter_GoogleauthTwoFactor;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GoogleauthTwoFactor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GoogleauthTwoFactor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 185
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 208
    new-instance v0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;-><init>()V

    .line 209
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 210
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 217
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 215
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_uri(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    goto :goto_0

    .line 214
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->account_name(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    goto :goto_0

    .line 213
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_base32(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    goto :goto_0

    .line 212
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->verification_code(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    goto :goto_0

    .line 221
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 222
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 183
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$ProtoAdapter_GoogleauthTwoFactor;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 199
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->verification_code:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 200
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->authenticator_key_base32:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 201
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->account_name:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 202
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->authenticator_key_uri:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 203
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 183
    check-cast p2, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$ProtoAdapter_GoogleauthTwoFactor;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)I
    .locals 4

    .line 190
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->verification_code:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->authenticator_key_base32:Ljava/lang/String;

    const/4 v3, 0x2

    .line 191
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->account_name:Ljava/lang/String;

    const/4 v3, 0x3

    .line 192
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->authenticator_key_uri:Ljava/lang/String;

    const/4 v3, 0x4

    .line 193
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$ProtoAdapter_GoogleauthTwoFactor;->encodedSize(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;
    .locals 1

    .line 227
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->newBuilder()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 228
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->verification_code:Ljava/lang/String;

    .line 229
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_base32:Ljava/lang/String;

    .line 230
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->account_name:Ljava/lang/String;

    .line 231
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_uri:Ljava/lang/String;

    .line 232
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 233
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$ProtoAdapter_GoogleauthTwoFactor;->redact(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    move-result-object p1

    return-object p1
.end method
