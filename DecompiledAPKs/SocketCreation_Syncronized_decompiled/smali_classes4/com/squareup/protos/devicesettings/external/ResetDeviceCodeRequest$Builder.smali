.class public final Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ResetDeviceCodeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;",
        "Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;
    .locals 3

    .line 99
    new-instance v0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->build()Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->id:Ljava/lang/String;

    return-object p0
.end method
