.class final Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$ProtoAdapter_ResetDeviceCodeRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ResetDeviceCodeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ResetDeviceCodeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 105
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 122
    new-instance v0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;-><init>()V

    .line 123
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 124
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 128
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 126
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;

    goto :goto_0

    .line 132
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->build()Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 103
    invoke-virtual {p0, p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$ProtoAdapter_ResetDeviceCodeRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 116
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 117
    invoke-virtual {p2}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 103
    check-cast p2, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$ProtoAdapter_ResetDeviceCodeRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;)I
    .locals 3

    .line 110
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 111
    invoke-virtual {p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 103
    check-cast p1, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$ProtoAdapter_ResetDeviceCodeRequest;->encodedSize(Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;
    .locals 0

    .line 138
    invoke-virtual {p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;->newBuilder()Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$Builder;->build()Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 103
    check-cast p1, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest$ProtoAdapter_ResetDeviceCodeRequest;->redact(Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeRequest;

    move-result-object p1

    return-object p1
.end method
