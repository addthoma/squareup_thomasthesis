.class public final Lcom/squareup/protos/common/time/DateTimeInterval$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DateTimeInterval.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/time/DateTimeInterval;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/time/DateTimeInterval;",
        "Lcom/squareup/protos/common/time/DateTimeInterval$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public exclusive_end:Lcom/squareup/protos/common/time/DateTime;

.field public inclusive_start:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/time/DateTimeInterval;
    .locals 4

    .line 106
    new-instance v0, Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->inclusive_start:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->exclusive_end:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/time/DateTimeInterval;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->build()Lcom/squareup/protos/common/time/DateTimeInterval;

    move-result-object v0

    return-object v0
.end method

.method public exclusive_end(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/common/time/DateTimeInterval$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->exclusive_end:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public inclusive_start(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/common/time/DateTimeInterval$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->inclusive_start:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
