.class public final Lcom/squareup/protos/common/time/YearMonth$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "YearMonth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/time/YearMonth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/time/YearMonth;",
        "Lcom/squareup/protos/common/time/YearMonth$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public month_of_year:Ljava/lang/Integer;

.field public year:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/time/YearMonth;
    .locals 4

    .line 135
    new-instance v0, Lcom/squareup/protos/common/time/YearMonth;

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth$Builder;->year:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/common/time/YearMonth$Builder;->month_of_year:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/time/YearMonth;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonth$Builder;->build()Lcom/squareup/protos/common/time/YearMonth;

    move-result-object v0

    return-object v0
.end method

.method public month_of_year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonth$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/common/time/YearMonth$Builder;->month_of_year:Ljava/lang/Integer;

    return-object p0
.end method

.method public year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonth$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/common/time/YearMonth$Builder;->year:Ljava/lang/Integer;

    return-object p0
.end method
