.class public final enum Lcom/squareup/protos/common/client/Device$Orientation;
.super Ljava/lang/Enum;
.source "Device.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Device$Orientation$ProtoAdapter_Orientation;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/client/Device$Orientation;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Device$Orientation;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FACE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final enum FACE_UP:Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final enum LANDSCAPE_LEFT:Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final enum LANDSCAPE_RIGHT:Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final enum PORTRAIT:Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final enum PORTRAIT_UPSIDE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final enum SQUARE:Lcom/squareup/protos/common/client/Device$Orientation;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 355
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v1, 0x0

    const-string v2, "PORTRAIT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/common/client/Device$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->PORTRAIT:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 357
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v2, 0x1

    const-string v3, "PORTRAIT_UPSIDE_DOWN"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/common/client/Device$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->PORTRAIT_UPSIDE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 359
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v3, 0x2

    const-string v4, "LANDSCAPE_LEFT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/common/client/Device$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->LANDSCAPE_LEFT:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 361
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v4, 0x3

    const-string v5, "LANDSCAPE_RIGHT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/common/client/Device$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->LANDSCAPE_RIGHT:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 363
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v5, 0x4

    const-string v6, "FACE_UP"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/common/client/Device$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->FACE_UP:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 365
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v6, 0x5

    const-string v7, "FACE_DOWN"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/common/client/Device$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->FACE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 367
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v7, 0x6

    const-string v8, "SQUARE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/common/client/Device$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->SQUARE:Lcom/squareup/protos/common/client/Device$Orientation;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/common/client/Device$Orientation;

    .line 354
    sget-object v8, Lcom/squareup/protos/common/client/Device$Orientation;->PORTRAIT:Lcom/squareup/protos/common/client/Device$Orientation;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/common/client/Device$Orientation;->PORTRAIT_UPSIDE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/client/Device$Orientation;->LANDSCAPE_LEFT:Lcom/squareup/protos/common/client/Device$Orientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/common/client/Device$Orientation;->LANDSCAPE_RIGHT:Lcom/squareup/protos/common/client/Device$Orientation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/common/client/Device$Orientation;->FACE_UP:Lcom/squareup/protos/common/client/Device$Orientation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/common/client/Device$Orientation;->FACE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/common/client/Device$Orientation;->SQUARE:Lcom/squareup/protos/common/client/Device$Orientation;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->$VALUES:[Lcom/squareup/protos/common/client/Device$Orientation;

    .line 369
    new-instance v0, Lcom/squareup/protos/common/client/Device$Orientation$ProtoAdapter_Orientation;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Device$Orientation$ProtoAdapter_Orientation;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 373
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 374
    iput p3, p0, Lcom/squareup/protos/common/client/Device$Orientation;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/client/Device$Orientation;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 388
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/common/client/Device$Orientation;->SQUARE:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0

    .line 387
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/common/client/Device$Orientation;->FACE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0

    .line 386
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/common/client/Device$Orientation;->FACE_UP:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0

    .line 385
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/common/client/Device$Orientation;->LANDSCAPE_RIGHT:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0

    .line 384
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/common/client/Device$Orientation;->LANDSCAPE_LEFT:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0

    .line 383
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/common/client/Device$Orientation;->PORTRAIT_UPSIDE_DOWN:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0

    .line 382
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/common/client/Device$Orientation;->PORTRAIT:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/client/Device$Orientation;
    .locals 1

    .line 354
    const-class v0, Lcom/squareup/protos/common/client/Device$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/client/Device$Orientation;
    .locals 1

    .line 354
    sget-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->$VALUES:[Lcom/squareup/protos/common/client/Device$Orientation;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/client/Device$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/client/Device$Orientation;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 395
    iget v0, p0, Lcom/squareup/protos/common/client/Device$Orientation;->value:I

    return v0
.end method
