.class public final enum Lcom/squareup/protos/common/client/Device$Platform;
.super Ljava/lang/Enum;
.source "Device.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Platform"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Device$Platform$ProtoAdapter_Platform;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/client/Device$Platform;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/client/Device$Platform;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Device$Platform;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ANDROID:Lcom/squareup/protos/common/client/Device$Platform;

.field public static final enum IOS:Lcom/squareup/protos/common/client/Device$Platform;

.field public static final enum JAVASCRIPT:Lcom/squareup/protos/common/client/Device$Platform;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 311
    new-instance v0, Lcom/squareup/protos/common/client/Device$Platform;

    const/4 v1, 0x0

    const-string v2, "IOS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/common/client/Device$Platform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Platform;->IOS:Lcom/squareup/protos/common/client/Device$Platform;

    .line 313
    new-instance v0, Lcom/squareup/protos/common/client/Device$Platform;

    const/4 v2, 0x1

    const-string v3, "ANDROID"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/common/client/Device$Platform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Platform;->ANDROID:Lcom/squareup/protos/common/client/Device$Platform;

    .line 315
    new-instance v0, Lcom/squareup/protos/common/client/Device$Platform;

    const/4 v3, 0x2

    const-string v4, "JAVASCRIPT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/common/client/Device$Platform;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Platform;->JAVASCRIPT:Lcom/squareup/protos/common/client/Device$Platform;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/common/client/Device$Platform;

    .line 310
    sget-object v4, Lcom/squareup/protos/common/client/Device$Platform;->IOS:Lcom/squareup/protos/common/client/Device$Platform;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/common/client/Device$Platform;->ANDROID:Lcom/squareup/protos/common/client/Device$Platform;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/client/Device$Platform;->JAVASCRIPT:Lcom/squareup/protos/common/client/Device$Platform;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/common/client/Device$Platform;->$VALUES:[Lcom/squareup/protos/common/client/Device$Platform;

    .line 317
    new-instance v0, Lcom/squareup/protos/common/client/Device$Platform$ProtoAdapter_Platform;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Device$Platform$ProtoAdapter_Platform;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Device$Platform;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 321
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 322
    iput p3, p0, Lcom/squareup/protos/common/client/Device$Platform;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/client/Device$Platform;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 332
    :cond_0
    sget-object p0, Lcom/squareup/protos/common/client/Device$Platform;->JAVASCRIPT:Lcom/squareup/protos/common/client/Device$Platform;

    return-object p0

    .line 331
    :cond_1
    sget-object p0, Lcom/squareup/protos/common/client/Device$Platform;->ANDROID:Lcom/squareup/protos/common/client/Device$Platform;

    return-object p0

    .line 330
    :cond_2
    sget-object p0, Lcom/squareup/protos/common/client/Device$Platform;->IOS:Lcom/squareup/protos/common/client/Device$Platform;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/client/Device$Platform;
    .locals 1

    .line 310
    const-class v0, Lcom/squareup/protos/common/client/Device$Platform;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/client/Device$Platform;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/client/Device$Platform;
    .locals 1

    .line 310
    sget-object v0, Lcom/squareup/protos/common/client/Device$Platform;->$VALUES:[Lcom/squareup/protos/common/client/Device$Platform;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/client/Device$Platform;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/client/Device$Platform;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 339
    iget v0, p0, Lcom/squareup/protos/common/client/Device$Platform;->value:I

    return v0
.end method
