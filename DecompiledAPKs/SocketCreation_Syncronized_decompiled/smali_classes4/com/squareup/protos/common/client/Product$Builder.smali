.class public final Lcom/squareup/protos/common/client/Product$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Product.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Product;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/client/Product;",
        "Lcom/squareup/protos/common/client/Product$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application:Lcom/squareup/protos/common/client/Product$Application;

.field public build_type:Lcom/squareup/protos/common/client/Product$BuildType;

.field public device_advertising_id:Ljava/lang/String;

.field public device_installation_id:Ljava/lang/String;

.field public device_tracking_enabled:Ljava/lang/String;

.field public version:Lcom/squareup/protos/common/client/Version;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 160
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public application(Lcom/squareup/protos/common/client/Product$Application;)Lcom/squareup/protos/common/client/Product$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/common/client/Product$Builder;->application:Lcom/squareup/protos/common/client/Product$Application;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/client/Product;
    .locals 9

    .line 195
    new-instance v8, Lcom/squareup/protos/common/client/Product;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product$Builder;->application:Lcom/squareup/protos/common/client/Product$Application;

    iget-object v2, p0, Lcom/squareup/protos/common/client/Product$Builder;->version:Lcom/squareup/protos/common/client/Version;

    iget-object v3, p0, Lcom/squareup/protos/common/client/Product$Builder;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    iget-object v4, p0, Lcom/squareup/protos/common/client/Product$Builder;->device_advertising_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/common/client/Product$Builder;->device_tracking_enabled:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/common/client/Product$Builder;->device_installation_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/common/client/Product;-><init>(Lcom/squareup/protos/common/client/Product$Application;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Product$BuildType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Product$Builder;->build()Lcom/squareup/protos/common/client/Product;

    move-result-object v0

    return-object v0
.end method

.method public build_type(Lcom/squareup/protos/common/client/Product$BuildType;)Lcom/squareup/protos/common/client/Product$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/common/client/Product$Builder;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    return-object p0
.end method

.method public device_advertising_id(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/common/client/Product$Builder;->device_advertising_id:Ljava/lang/String;

    return-object p0
.end method

.method public device_installation_id(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/common/client/Product$Builder;->device_installation_id:Ljava/lang/String;

    return-object p0
.end method

.method public device_tracking_enabled(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/common/client/Product$Builder;->device_tracking_enabled:Ljava/lang/String;

    return-object p0
.end method

.method public version(Lcom/squareup/protos/common/client/Version;)Lcom/squareup/protos/common/client/Product$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/common/client/Product$Builder;->version:Lcom/squareup/protos/common/client/Version;

    return-object p0
.end method
