.class public final Lcom/squareup/protos/common/client/Device$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/client/Device;",
        "Lcom/squareup/protos/common/client/Device$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Ljava/lang/String;

.field public country_code:Lcom/squareup/protos/common/countries/Country;

.field public id:Ljava/lang/String;

.field public language_code:Lcom/squareup/protos/common/languages/Language;

.field public manufacturer:Ljava/lang/String;

.field public model:Ljava/lang/String;

.field public orientation:Lcom/squareup/protos/common/client/Device$Orientation;

.field public os_version:Lcom/squareup/protos/common/client/Version;

.field public platform:Lcom/squareup/protos/common/client/Device$Platform;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Ljava/lang/String;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->brand:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/client/Device;
    .locals 12

    .line 306
    new-instance v11, Lcom/squareup/protos/common/client/Device;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/common/client/Device$Builder;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    iget-object v3, p0, Lcom/squareup/protos/common/client/Device$Builder;->manufacturer:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/common/client/Device$Builder;->brand:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/common/client/Device$Builder;->model:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/common/client/Device$Builder;->os_version:Lcom/squareup/protos/common/client/Version;

    iget-object v7, p0, Lcom/squareup/protos/common/client/Device$Builder;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    iget-object v8, p0, Lcom/squareup/protos/common/client/Device$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    iget-object v9, p0, Lcom/squareup/protos/common/client/Device$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/common/client/Device;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/client/Device$Platform;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Device$Orientation;Lcom/squareup/protos/common/languages/Language;Lcom/squareup/protos/common/countries/Country;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 217
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Device$Builder;->build()Lcom/squareup/protos/common/client/Device;

    move-result-object v0

    return-object v0
.end method

.method public country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public language_code(Lcom/squareup/protos/common/languages/Language;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    return-object p0
.end method

.method public manufacturer(Ljava/lang/String;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->manufacturer:Ljava/lang/String;

    return-object p0
.end method

.method public model(Ljava/lang/String;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->model:Ljava/lang/String;

    return-object p0
.end method

.method public orientation(Lcom/squareup/protos/common/client/Device$Orientation;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 287
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    return-object p0
.end method

.method public os_version(Lcom/squareup/protos/common/client/Version;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->os_version:Lcom/squareup/protos/common/client/Version;

    return-object p0
.end method

.method public platform(Lcom/squareup/protos/common/client/Device$Platform;)Lcom/squareup/protos/common/client/Device$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device$Builder;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    return-object p0
.end method
