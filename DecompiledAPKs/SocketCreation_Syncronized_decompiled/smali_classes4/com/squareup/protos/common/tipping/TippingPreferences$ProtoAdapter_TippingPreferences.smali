.class final Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TippingPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/tipping/TippingPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TippingPreferences"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/common/tipping/TippingPreferences;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 397
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/common/tipping/TippingPreferences;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/tipping/TippingPreferences;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 432
    new-instance v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;-><init>()V

    .line 433
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 434
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 447
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 445
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->client_calculated_tip_option:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 444
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_max_percentage(Ljava/lang/Double;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    goto :goto_0

    .line 443
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    goto :goto_0

    .line 442
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    goto :goto_0

    .line 441
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->allow_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    goto :goto_0

    .line 440
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->tipping_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 439
    :pswitch_6
    iget-object v3, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 438
    :pswitch_7
    iget-object v3, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 437
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    goto :goto_0

    .line 436
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->use_smart_tipping(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    goto/16 :goto_0

    .line 451
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 452
    invoke-virtual {v0}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->build()Lcom/squareup/protos/common/tipping/TippingPreferences;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 395
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/tipping/TippingPreferences;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/tipping/TippingPreferences;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 417
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 418
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 419
    sget-object v0, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 420
    sget-object v0, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 421
    sget-object v0, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 422
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 423
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 424
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 425
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 426
    sget-object v0, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 427
    invoke-virtual {p2}, Lcom/squareup/protos/common/tipping/TippingPreferences;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 395
    check-cast p2, Lcom/squareup/protos/common/tipping/TippingPreferences;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/tipping/TippingPreferences;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/common/tipping/TippingPreferences;)I
    .locals 4

    .line 402
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 403
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 404
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 405
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 406
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 407
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x7

    .line 408
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x8

    .line 409
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    const/16 v3, 0x9

    .line 410
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 411
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    invoke-virtual {p1}, Lcom/squareup/protos/common/tipping/TippingPreferences;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 395
    check-cast p1, Lcom/squareup/protos/common/tipping/TippingPreferences;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;->encodedSize(Lcom/squareup/protos/common/tipping/TippingPreferences;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/common/tipping/TippingPreferences;)Lcom/squareup/protos/common/tipping/TippingPreferences;
    .locals 2

    .line 457
    invoke-virtual {p1}, Lcom/squareup/protos/common/tipping/TippingPreferences;->newBuilder()Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object p1

    .line 458
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    .line 459
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 460
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 461
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->tipping_options:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 462
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    .line 463
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    .line 464
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->client_calculated_tip_option:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 465
    invoke-virtual {p1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 466
    invoke-virtual {p1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->build()Lcom/squareup/protos/common/tipping/TippingPreferences;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 395
    check-cast p1, Lcom/squareup/protos/common/tipping/TippingPreferences;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;->redact(Lcom/squareup/protos/common/tipping/TippingPreferences;)Lcom/squareup/protos/common/tipping/TippingPreferences;

    move-result-object p1

    return-object p1
.end method
