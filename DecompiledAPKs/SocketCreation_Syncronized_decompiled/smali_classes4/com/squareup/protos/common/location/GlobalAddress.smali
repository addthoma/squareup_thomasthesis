.class public final Lcom/squareup/protos/common/location/GlobalAddress;
.super Lcom/squareup/wire/Message;
.source "GlobalAddress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/location/GlobalAddress$ProtoAdapter_GlobalAddress;,
        Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "Lcom/squareup/protos/common/location/GlobalAddress$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADDRESS_LINE_1:Ljava/lang/String; = ""

.field public static final DEFAULT_ADDRESS_LINE_2:Ljava/lang/String; = ""

.field public static final DEFAULT_ADDRESS_LINE_3:Ljava/lang/String; = ""

.field public static final DEFAULT_ADDRESS_LINE_4:Ljava/lang/String; = ""

.field public static final DEFAULT_ADDRESS_LINE_5:Ljava/lang/String; = ""

.field public static final DEFAULT_ADMINISTRATIVE_DISTRICT_LEVEL_1:Ljava/lang/String; = ""

.field public static final DEFAULT_ADMINISTRATIVE_DISTRICT_LEVEL_2:Ljava/lang/String; = ""

.field public static final DEFAULT_ADMINISTRATIVE_DISTRICT_LEVEL_3:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

.field public static final DEFAULT_LOCALITY:Ljava/lang/String; = ""

.field public static final DEFAULT_POSTAL_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY_1:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY_2:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY_3:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY_4:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBLOCALITY_5:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address_coordinates:Lcom/squareup/protos/common/location/Coordinates;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.Coordinates#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final address_line_1:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final address_line_2:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final address_line_3:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final address_line_4:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final address_line_5:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final administrative_district_level_1:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final administrative_district_level_2:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final administrative_district_level_3:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final country_code:Lcom/squareup/protos/common/countries/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.countries.Country#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final locality:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final postal_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x10
    .end annotation
.end field

.field public final sublocality:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final sublocality_1:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final sublocality_2:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final sublocality_3:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final sublocality_4:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final sublocality_5:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/common/location/GlobalAddress$ProtoAdapter_GlobalAddress;

    invoke-direct {v0}, Lcom/squareup/protos/common/location/GlobalAddress$ProtoAdapter_GlobalAddress;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 60
    sget-object v0, Lcom/squareup/protos/common/countries/Country;->US:Lcom/squareup/protos/common/countries/Country;

    sput-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/location/GlobalAddress$Builder;Lokio/ByteString;)V
    .locals 1

    .line 204
    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 205
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_1:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    .line 206
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_2:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    .line 207
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_3:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    .line 208
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_4:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    .line 209
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_5:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    .line 210
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->locality:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    .line 211
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    .line 212
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_1:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    .line 213
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_2:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    .line 214
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_3:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    .line 215
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_4:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    .line 216
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_5:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    .line 217
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_1:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    .line 218
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_2:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    .line 219
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_3:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    .line 220
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->postal_code:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    .line 221
    iget-object p2, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    iput-object p2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 222
    iget-object p1, p1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 253
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 254
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/location/GlobalAddress;

    .line 255
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/GlobalAddress;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/location/GlobalAddress;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    .line 256
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    .line 257
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    .line 258
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    .line 259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    .line 260
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    .line 263
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    .line 264
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    .line 265
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    .line 266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    .line 267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    .line 268
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    .line 269
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    .line 270
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    .line 271
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    iget-object v3, p1, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 272
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    iget-object p1, p1, Lcom/squareup/protos/common/location/GlobalAddress;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    .line 273
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 278
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_12

    .line 280
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/GlobalAddress;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 285
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 286
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 288
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 289
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 291
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 292
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 293
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 294
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 295
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 296
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 297
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/common/countries/Country;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 298
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/Coordinates;->hashCode()I

    move-result v2

    :cond_11
    add-int/2addr v0, v2

    .line 299
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_12
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 2

    .line 227
    new-instance v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;-><init>()V

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_1:Ljava/lang/String;

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_2:Ljava/lang/String;

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_3:Ljava/lang/String;

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_4:Ljava/lang/String;

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_5:Ljava/lang/String;

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->locality:Ljava/lang/String;

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality:Ljava/lang/String;

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_1:Ljava/lang/String;

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_2:Ljava/lang/String;

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_3:Ljava/lang/String;

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_4:Ljava/lang/String;

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_5:Ljava/lang/String;

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_1:Ljava/lang/String;

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_2:Ljava/lang/String;

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_3:Ljava/lang/String;

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->postal_code:Ljava/lang/String;

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    iput-object v1, v0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    .line 246
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/GlobalAddress;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/GlobalAddress;->newBuilder()Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 307
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", address_line_1=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", address_line_2=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", address_line_3=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", address_line_4=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", address_line_5=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", locality="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", sublocality="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", sublocality_1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", sublocality_2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", sublocality_3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", sublocality_4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", sublocality_5="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", administrative_district_level_1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", administrative_district_level_2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", administrative_district_level_3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", postal_code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_10

    const-string v1, ", country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 324
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    if-eqz v1, :cond_11

    const-string v1, ", address_coordinates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_11
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GlobalAddress{"

    .line 325
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
