.class final Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType$ProtoAdapter_ConnectionType;
.super Lcom/squareup/wire/EnumAdapter;
.source "ConnectedPeripheral.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ConnectionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 275
    const-class v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;
    .locals 0

    .line 280
    invoke-static {p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->fromValue(I)Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 273
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType$ProtoAdapter_ConnectionType;->fromValue(I)Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    move-result-object p1

    return-object p1
.end method
