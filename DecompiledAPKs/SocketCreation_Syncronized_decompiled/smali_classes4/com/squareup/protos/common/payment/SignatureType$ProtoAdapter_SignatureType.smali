.class final Lcom/squareup/protos/common/payment/SignatureType$ProtoAdapter_SignatureType;
.super Lcom/squareup/wire/EnumAdapter;
.source "SignatureType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/payment/SignatureType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SignatureType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/common/payment/SignatureType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 62
    const-class v0, Lcom/squareup/protos/common/payment/SignatureType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/common/payment/SignatureType;
    .locals 0

    .line 67
    invoke-static {p1}, Lcom/squareup/protos/common/payment/SignatureType;->fromValue(I)Lcom/squareup/protos/common/payment/SignatureType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 60
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/SignatureType$ProtoAdapter_SignatureType;->fromValue(I)Lcom/squareup/protos/common/payment/SignatureType;

    move-result-object p1

    return-object p1
.end method
