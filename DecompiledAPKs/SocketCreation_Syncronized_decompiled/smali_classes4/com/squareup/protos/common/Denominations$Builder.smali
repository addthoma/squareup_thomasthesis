.class public final Lcom/squareup/protos/common/Denominations$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Denominations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/Denominations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/Denominations;",
        "Lcom/squareup/protos/common/Denominations$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public banknote_cents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public coin_cents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 96
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/Denominations$Builder;->banknote_cents:Ljava/util/List;

    .line 97
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/Denominations$Builder;->coin_cents:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public banknote_cents(Ljava/util/List;)Lcom/squareup/protos/common/Denominations$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/protos/common/Denominations$Builder;"
        }
    .end annotation

    .line 101
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/common/Denominations$Builder;->banknote_cents:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/Denominations;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/common/Denominations;

    iget-object v1, p0, Lcom/squareup/protos/common/Denominations$Builder;->banknote_cents:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/common/Denominations$Builder;->coin_cents:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/Denominations;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/common/Denominations$Builder;->build()Lcom/squareup/protos/common/Denominations;

    move-result-object v0

    return-object v0
.end method

.method public coin_cents(Ljava/util/List;)Lcom/squareup/protos/common/Denominations$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/protos/common/Denominations$Builder;"
        }
    .end annotation

    .line 107
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/common/Denominations$Builder;->coin_cents:Ljava/util/List;

    return-object p0
.end method
