.class public final Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBankAccountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;",
        "Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

.field public password_required_to_link:Ljava/lang/Boolean;

.field public pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

.field public recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

.field public recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 152
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public active_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;
    .locals 8

    .line 198
    new-instance v7, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v2, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v3, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v4, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v5, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->password_required_to_link:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->build()Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    move-result-object v0

    return-object v0
.end method

.method public password_required_to_link(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->password_required_to_link:Ljava/lang/Boolean;

    return-object p0
.end method

.method public pending_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object p0
.end method

.method public recently_failed_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object p0
.end method

.method public recently_failed_verification_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object p0
.end method
