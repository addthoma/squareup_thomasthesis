.class public final Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetLatestBankAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;",
        "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

.field public requires_password:Ljava/lang/Boolean;

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v3, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->requires_password:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/bankaccount/BankAccount;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->build()Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;

    move-result-object v0

    return-object v0
.end method

.method public latest_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object p0
.end method

.method public requires_password(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->requires_password:Ljava/lang/Boolean;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
