.class public final Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LinkBankAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

.field public error_message:Ljava/lang/String;

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;
    .locals 5

    .line 154
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->error:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->build()Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->error:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
