.class final Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FulfillmentAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/FulfillmentAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/orders/FulfillmentAction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 302
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/orders/FulfillmentAction;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/FulfillmentAction;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 335
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 336
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 337
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 349
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 347
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/orders/CompleteAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/CompleteAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete(Lcom/squareup/protos/client/orders/CompleteAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 346
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/orders/MarkInProgressAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/MarkInProgressAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress(Lcom/squareup/protos/client/orders/MarkInProgressAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 345
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/orders/LineItemQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 344
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/orders/MarkShippedAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/MarkShippedAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped(Lcom/squareup/protos/client/orders/MarkShippedAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 343
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/orders/CancelAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/CancelAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel(Lcom/squareup/protos/client/orders/CancelAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 342
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/orders/MarkReadyAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/MarkReadyAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready(Lcom/squareup/protos/client/orders/MarkReadyAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 341
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/orders/MarkPickedUpAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up(Lcom/squareup/protos/client/orders/MarkPickedUpAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 340
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/orders/AcceptAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/AcceptAction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept(Lcom/squareup/protos/client/orders/AcceptAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 339
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    goto :goto_0

    .line 353
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 354
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 300
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/FulfillmentAction;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 321
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 322
    sget-object v0, Lcom/squareup/protos/client/orders/LineItemQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 323
    sget-object v0, Lcom/squareup/protos/client/orders/AcceptAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 324
    sget-object v0, Lcom/squareup/protos/client/orders/MarkPickedUpAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 325
    sget-object v0, Lcom/squareup/protos/client/orders/MarkReadyAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 326
    sget-object v0, Lcom/squareup/protos/client/orders/CancelAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 327
    sget-object v0, Lcom/squareup/protos/client/orders/MarkShippedAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 328
    sget-object v0, Lcom/squareup/protos/client/orders/MarkInProgressAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 329
    sget-object v0, Lcom/squareup/protos/client/orders/CompleteAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 330
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/FulfillmentAction;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 300
    check-cast p2, Lcom/squareup/protos/client/orders/FulfillmentAction;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/FulfillmentAction;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/orders/FulfillmentAction;)I
    .locals 4

    .line 307
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/orders/LineItemQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 308
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/AcceptAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    const/4 v3, 0x2

    .line 309
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/MarkPickedUpAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    const/4 v3, 0x3

    .line 310
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/MarkReadyAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    const/4 v3, 0x4

    .line 311
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/CancelAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    const/4 v3, 0x5

    .line 312
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/MarkShippedAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    const/4 v3, 0x6

    .line 313
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/MarkInProgressAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    const/16 v3, 0x8

    .line 314
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/CompleteAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    const/16 v3, 0x9

    .line 315
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentAction;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 300
    check-cast p1, Lcom/squareup/protos/client/orders/FulfillmentAction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;->encodedSize(Lcom/squareup/protos/client/orders/FulfillmentAction;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/orders/FulfillmentAction;)Lcom/squareup/protos/client/orders/FulfillmentAction;
    .locals 2

    .line 359
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentAction;->newBuilder()Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object p1

    .line 360
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/orders/LineItemQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 361
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/orders/AcceptAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/AcceptAction;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 362
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/orders/MarkPickedUpAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 363
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/orders/MarkReadyAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/MarkReadyAction;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 364
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/orders/CancelAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/CancelAction;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 365
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/orders/MarkShippedAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/MarkShippedAction;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 366
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/orders/MarkInProgressAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/MarkInProgressAction;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 367
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/orders/CompleteAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/CompleteAction;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    .line 368
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 369
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 300
    check-cast p1, Lcom/squareup/protos/client/orders/FulfillmentAction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;->redact(Lcom/squareup/protos/client/orders/FulfillmentAction;)Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object p1

    return-object p1
.end method
