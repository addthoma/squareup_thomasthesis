.class public final Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchOrdersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/SearchOrdersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/SearchOrdersRequest;",
        "Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bazaar_location_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public client_support:Lcom/squareup/protos/client/orders/ClientSupport;

.field public cursor:Ljava/lang/String;

.field public date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

.field public fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

.field public limit:Ljava/lang/Integer;

.field public location_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public order_groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation
.end field

.field public order_source_name:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public search_term:Ljava/lang/String;

.field public sort:Lcom/squareup/orders/SearchOrdersSort;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 271
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 272
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids:Ljava/util/List;

    .line 273
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups:Ljava/util/List;

    .line 274
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_source_name:Ljava/util/List;

    .line 275
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bazaar_location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;"
        }
    .end annotation

    .line 365
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 366
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/orders/SearchOrdersRequest;
    .locals 14

    .line 380
    new-instance v13, Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->cursor:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort:Lcom/squareup/orders/SearchOrdersSort;

    iget-object v7, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    iget-object v8, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_source_name:Ljava/util/List;

    iget-object v9, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->search_term:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids:Ljava/util/List;

    iget-object v11, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;-><init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersSort;Lcom/squareup/orders/SearchOrdersFulfillmentFilter;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersDateTimeFilter;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 248
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->build()Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    return-object p0
.end method

.method public cursor(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->cursor:Ljava/lang/String;

    return-object p0
.end method

.method public date_time_filter(Lcom/squareup/orders/SearchOrdersDateTimeFilter;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0

    .line 374
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    return-object p0
.end method

.method public fulfillments(Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0

    .line 299
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public location_ids(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;"
        }
    .end annotation

    .line 290
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 291
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids:Ljava/util/List;

    return-object p0
.end method

.method public order_groups(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;)",
            "Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;"
        }
    .end annotation

    .line 320
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups:Ljava/util/List;

    return-object p0
.end method

.method public order_source_name(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;"
        }
    .end annotation

    .line 345
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_source_name:Ljava/util/List;

    return-object p0
.end method

.method public search_term(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->search_term:Ljava/lang/String;

    return-object p0
.end method

.method public sort(Lcom/squareup/orders/SearchOrdersSort;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort:Lcom/squareup/orders/SearchOrdersSort;

    return-object p0
.end method
