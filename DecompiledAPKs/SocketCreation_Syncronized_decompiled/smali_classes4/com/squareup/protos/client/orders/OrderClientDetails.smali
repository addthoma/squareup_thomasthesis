.class public final Lcom/squareup/protos/client/orders/OrderClientDetails;
.super Lcom/squareup/wire/Message;
.source "OrderClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;,
        Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/OrderClientDetails;",
        "Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/OrderClientDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EXTERNAL_LINK:Ljava/lang/String; = ""

.field public static final DEFAULT_FULFILLMENTS_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCALIZED_PROMPT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCALIZED_PROMPT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final channel:Lcom/squareup/protos/client/orders/Channel;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.Channel#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final external_link:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final fulfillments_display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final groups:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.OrderGroup#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final localized_prompt_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final localized_prompt_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.OrderDisplayStateData#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/orders/Channel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/orders/OrderDisplayStateData;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;",
            "Lcom/squareup/protos/client/orders/Channel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 100
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/orders/OrderClientDetails;-><init>(Ljava/util/List;Lcom/squareup/protos/client/orders/Channel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/orders/OrderDisplayStateData;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/orders/Channel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/orders/OrderDisplayStateData;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;",
            "Lcom/squareup/protos/client/orders/Channel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 107
    sget-object v0, Lcom/squareup/protos/client/orders/OrderClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p8, "groups"

    .line 108
    invoke-static {p8, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    .line 109
    iput-object p2, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    .line 110
    iput-object p3, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    .line 111
    iput-object p4, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    .line 112
    iput-object p5, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    .line 113
    iput-object p6, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    .line 114
    iput-object p7, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 134
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 135
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/OrderClientDetails;

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/OrderClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    .line 137
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    .line 143
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 148
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/Channel;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 158
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 2

    .line 119
    new-instance v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->groups:Ljava/util/List;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->channel:Lcom/squareup/protos/client/orders/Channel;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->fulfillments_display_name:Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_title:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_description:Ljava/lang/String;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->external_link:Ljava/lang/String;

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderClientDetails;->newBuilder()Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", groups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    if-eqz v1, :cond_1

    const-string v1, ", channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", fulfillments_display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", localized_prompt_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", localized_prompt_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    if-eqz v1, :cond_5

    const-string v1, ", order_display_state_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", external_link="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OrderClientDetails{"

    .line 173
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
