.class public final enum Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;
.super Ljava/lang/Enum;
.source "OrderDisplayStateData.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/OrderDisplayStateData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OrderDisplayState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState$ProtoAdapter_OrderDisplayState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum COMPLETED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum FAILED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum READY:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum REJECTED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field public static final enum UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 184
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->DO_NOT_USE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 194
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/16 v2, 0x8

    const/4 v3, 0x1

    const-string v4, "UPCOMING"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 199
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v4, 0x2

    const-string v5, "NEW"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 204
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v5, 0x3

    const-string v6, "IN_PROGRESS"

    invoke-direct {v0, v6, v5, v4}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 209
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v6, 0x4

    const-string v7, "READY"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->READY:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 214
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v7, 0x5

    const-string v8, "COMPLETED"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->COMPLETED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 219
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v8, 0x6

    const-string v9, "CANCELED"

    invoke-direct {v0, v9, v8, v7}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->CANCELED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 224
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v9, 0x7

    const-string v10, "REJECTED"

    invoke-direct {v0, v10, v9, v8}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->REJECTED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 229
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const-string v10, "FAILED"

    invoke-direct {v0, v10, v2, v9}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->FAILED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 183
    sget-object v10, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->DO_NOT_USE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->READY:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->COMPLETED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->CANCELED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->REJECTED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->FAILED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->$VALUES:[Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 231
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState$ProtoAdapter_OrderDisplayState;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState$ProtoAdapter_OrderDisplayState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 235
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 236
    iput p3, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 245
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 252
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->FAILED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 251
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->REJECTED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 250
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->CANCELED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 249
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->COMPLETED:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 248
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->READY:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 247
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->IN_PROGRESS:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 246
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    .line 244
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->DO_NOT_USE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;
    .locals 1

    .line 183
    const-class v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;
    .locals 1

    .line 183
    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->$VALUES:[Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 259
    iget v0, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->value:I

    return v0
.end method
