.class public final Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FulfillmentAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/FulfillmentAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/FulfillmentAction;",
        "Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accept:Lcom/squareup/protos/client/orders/AcceptAction;

.field public cancel:Lcom/squareup/protos/client/orders/CancelAction;

.field public complete:Lcom/squareup/protos/client/orders/CompleteAction;

.field public fulfillment_id:Ljava/lang/String;

.field public line_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;"
        }
    .end annotation
.end field

.field public mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

.field public mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

.field public mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

.field public mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 198
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 199
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accept(Lcom/squareup/protos/client/orders/AcceptAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    const/4 p1, 0x0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 221
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/orders/FulfillmentAction;
    .locals 12

    .line 296
    new-instance v11, Lcom/squareup/protos/client/orders/FulfillmentAction;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    iget-object v4, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    iget-object v5, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    iget-object v6, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    iget-object v7, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    iget-object v8, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    iget-object v9, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/orders/FulfillmentAction;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/orders/AcceptAction;Lcom/squareup/protos/client/orders/MarkPickedUpAction;Lcom/squareup/protos/client/orders/MarkReadyAction;Lcom/squareup/protos/client/orders/CancelAction;Lcom/squareup/protos/client/orders/MarkShippedAction;Lcom/squareup/protos/client/orders/MarkInProgressAction;Lcom/squareup/protos/client/orders/CompleteAction;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 179
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentAction;

    move-result-object v0

    return-object v0
.end method

.method public cancel(Lcom/squareup/protos/client/orders/CancelAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    const/4 p1, 0x0

    .line 252
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 254
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    return-object p0
.end method

.method public complete(Lcom/squareup/protos/client/orders/CompleteAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 284
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    const/4 p1, 0x0

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 287
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    return-object p0
.end method

.method public fulfillment_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id:Ljava/lang/String;

    return-object p0
.end method

.method public line_items(Ljava/util/List;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;)",
            "Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;"
        }
    .end annotation

    .line 212
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items:Ljava/util/List;

    return-object p0
.end method

.method public mark_in_progress(Lcom/squareup/protos/client/orders/MarkInProgressAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    const/4 p1, 0x0

    .line 274
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 275
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 276
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 277
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 278
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 279
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    return-object p0
.end method

.method public mark_picked_up(Lcom/squareup/protos/client/orders/MarkPickedUpAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    const/4 p1, 0x0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 234
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 235
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    return-object p0
.end method

.method public mark_ready(Lcom/squareup/protos/client/orders/MarkReadyAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    const/4 p1, 0x0

    .line 241
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 242
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    return-object p0
.end method

.method public mark_shipped(Lcom/squareup/protos/client/orders/MarkShippedAction;)Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    const/4 p1, 0x0

    .line 263
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 266
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 268
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    return-object p0
.end method
