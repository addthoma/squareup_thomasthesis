.class final Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SearchOrdersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/SearchOrdersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SearchOrdersRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/orders/SearchOrdersRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 386
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/SearchOrdersRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 423
    new-instance v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;-><init>()V

    .line 424
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 425
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 446
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 444
    :pswitch_0
    sget-object v3, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter(Lcom/squareup/orders/SearchOrdersDateTimeFilter;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    goto :goto_0

    .line 443
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 442
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->search_term(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    goto :goto_0

    .line 441
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_source_name:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 440
    :pswitch_4
    sget-object v3, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments(Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    goto :goto_0

    .line 439
    :pswitch_5
    sget-object v3, Lcom/squareup/orders/SearchOrdersSort;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/SearchOrdersSort;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort(Lcom/squareup/orders/SearchOrdersSort;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    goto :goto_0

    .line 433
    :pswitch_6
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/orders/OrderGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 435
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 430
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->cursor(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    goto :goto_0

    .line 429
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    goto/16 :goto_0

    .line 428
    :pswitch_9
    iget-object v3, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 427
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/orders/ClientSupport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/ClientSupport;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    goto/16 :goto_0

    .line 450
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 451
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->build()Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 384
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/SearchOrdersRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 407
    sget-object v0, Lcom/squareup/protos/client/orders/ClientSupport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 408
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 409
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 410
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 411
    sget-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 412
    sget-object v0, Lcom/squareup/orders/SearchOrdersSort;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 413
    sget-object v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 414
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 415
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 416
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 417
    sget-object v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 418
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 384
    check-cast p2, Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/SearchOrdersRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)I
    .locals 4

    .line 391
    sget-object v0, Lcom/squareup/protos/client/orders/ClientSupport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 392
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 393
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    const/4 v3, 0x4

    .line 394
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 395
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/SearchOrdersSort;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    const/4 v3, 0x6

    .line 396
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    const/4 v3, 0x7

    .line 397
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 398
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    const/16 v3, 0x9

    .line 399
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 400
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    const/16 v3, 0xb

    .line 401
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 402
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 384
    check-cast p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;->encodedSize(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)Lcom/squareup/protos/client/orders/SearchOrdersRequest;
    .locals 2

    .line 456
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->newBuilder()Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object p1

    .line 457
    iget-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/orders/ClientSupport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/ClientSupport;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 458
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort:Lcom/squareup/orders/SearchOrdersSort;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/orders/SearchOrdersSort;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort:Lcom/squareup/orders/SearchOrdersSort;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/SearchOrdersSort;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort:Lcom/squareup/orders/SearchOrdersSort;

    .line 459
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    .line 460
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    .line 461
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 462
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->build()Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 384
    check-cast p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;->redact(Lcom/squareup/protos/client/orders/SearchOrdersRequest;)Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    move-result-object p1

    return-object p1
.end method
