.class public final Lcom/squareup/protos/client/orders/GetOrderRequest;
.super Lcom/squareup/wire/Message;
.source "GetOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/GetOrderRequest$ProtoAdapter_GetOrderRequest;,
        Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/GetOrderRequest;",
        "Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/GetOrderRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ORDER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final client_support:Lcom/squareup/protos/client/orders/ClientSupport;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.ClientSupport#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final order_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/orders/GetOrderRequest$ProtoAdapter_GetOrderRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/GetOrderRequest$ProtoAdapter_GetOrderRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/GetOrderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/lang/String;)V
    .locals 1

    .line 42
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/orders/GetOrderRequest;-><init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/protos/client/orders/GetOrderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 48
    iput-object p2, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->order_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 63
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/GetOrderRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 64
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/GetOrderRequest;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/GetOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/GetOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 66
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->order_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/GetOrderRequest;->order_id:Ljava/lang/String;

    .line 67
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 72
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/ClientSupport;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 77
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;-><init>()V

    .line 54
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 55
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->order_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;->order_id:Ljava/lang/String;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/GetOrderRequest;->newBuilder()Lcom/squareup/protos/client/orders/GetOrderRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    if-eqz v1, :cond_0

    const-string v1, ", client_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", order_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/GetOrderRequest;->order_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetOrderRequest{"

    .line 87
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
