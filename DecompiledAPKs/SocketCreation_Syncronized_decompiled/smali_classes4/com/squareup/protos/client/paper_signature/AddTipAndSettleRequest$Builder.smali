.class public final Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTipAndSettleRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amendment_token:Ljava/lang/String;

.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public tender_id:Ljava/lang/String;

.field public tip_money:Lcom/squareup/protos/common/Money;

.field public total_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 164
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amendment_token(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->amendment_token:Ljava/lang/String;

    return-object p0
.end method

.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;
    .locals 8

    .line 217
    new-instance v7, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->tender_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->amendment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 153
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;

    move-result-object v0

    return-object v0
.end method

.method public tender_id(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->tender_id:Ljava/lang/String;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
