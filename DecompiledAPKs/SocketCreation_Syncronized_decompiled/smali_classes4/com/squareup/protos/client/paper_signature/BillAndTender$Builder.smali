.class public final Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BillAndTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/BillAndTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/BillAndTender;",
        "Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/paper_signature/BillAndTender;
    .locals 4

    .line 106
    new-instance v0, Lcom/squareup/protos/client/paper_signature/BillAndTender;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/paper_signature/BillAndTender;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->build()Lcom/squareup/protos/client/paper_signature/BillAndTender;

    move-result-object v0

    return-object v0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/BillAndTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
