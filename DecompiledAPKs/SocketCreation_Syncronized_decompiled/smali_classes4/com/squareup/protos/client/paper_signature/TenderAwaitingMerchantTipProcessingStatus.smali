.class public final enum Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;
.super Ljava/lang/Enum;
.source "TenderAwaitingMerchantTipProcessingStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus$ProtoAdapter_TenderAwaitingMerchantTipProcessingStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PROCESSED:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

.field public static final enum PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

.field public static final enum QUEUED_FOR_PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 17
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "QUEUED_FOR_PROCESSING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->QUEUED_FOR_PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    const/4 v3, 0x2

    const-string v4, "PROCESSING"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    .line 30
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    const/4 v4, 0x3

    const-string v5, "PROCESSED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->PROCESSED:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    new-array v0, v4, [Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    .line 13
    sget-object v4, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->QUEUED_FOR_PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->PROCESSED:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->$VALUES:[Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    .line 32
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus$ProtoAdapter_TenderAwaitingMerchantTipProcessingStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus$ProtoAdapter_TenderAwaitingMerchantTipProcessingStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 47
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->PROCESSED:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-object p0

    .line 46
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-object p0

    .line 45
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->QUEUED_FOR_PROCESSING:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->$VALUES:[Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;->value:I

    return v0
.end method
