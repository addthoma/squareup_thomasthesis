.class public final Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/v2/ListRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tickets/v2/ListRequest;",
        "Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public data_center_hint:Ljava/lang/String;

.field public include_omitted_open_tickets:Ljava/lang/Boolean;

.field public known_ticket_info:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 132
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 133
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->known_ticket_info:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tickets/v2/ListRequest;
    .locals 5

    .line 174
    new-instance v0, Lcom/squareup/protos/client/tickets/v2/ListRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->known_ticket_info:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->include_omitted_open_tickets:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->data_center_hint:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/tickets/v2/ListRequest;-><init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->build()Lcom/squareup/protos/client/tickets/v2/ListRequest;

    move-result-object v0

    return-object v0
.end method

.method public data_center_hint(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->data_center_hint:Ljava/lang/String;

    return-object p0
.end method

.method public include_omitted_open_tickets(Ljava/lang/Boolean;)Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->include_omitted_open_tickets:Ljava/lang/Boolean;

    return-object p0
.end method

.method public known_ticket_info(Ljava/util/List;)Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;)",
            "Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;"
        }
    .end annotation

    .line 145
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->known_ticket_info:Ljava/util/List;

    return-object p0
.end method
