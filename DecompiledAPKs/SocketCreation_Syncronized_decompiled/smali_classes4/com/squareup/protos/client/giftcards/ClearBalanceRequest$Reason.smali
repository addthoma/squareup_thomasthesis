.class public final enum Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;
.super Ljava/lang/Enum;
.source "ClearBalanceRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason$ProtoAdapter_Reason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CASH_OUT:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

.field public static final enum LOST:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

.field public static final enum OTHER:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

.field public static final enum REFUND:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

.field public static final enum RESET:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

.field public static final enum UNKNOWN_REASON:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 198
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_REASON"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 203
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    const/4 v2, 0x1

    const-string v3, "CASH_OUT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->CASH_OUT:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 210
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    const/4 v3, 0x2

    const-string v4, "REFUND"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->REFUND:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 215
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    const/4 v4, 0x3

    const-string v5, "OTHER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->OTHER:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 220
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    const/4 v5, 0x4

    const-string v6, "LOST"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->LOST:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 225
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    const/4 v6, 0x5

    const-string v7, "RESET"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->RESET:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 193
    sget-object v7, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->CASH_OUT:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->REFUND:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->OTHER:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->LOST:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->RESET:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->$VALUES:[Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 227
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason$ProtoAdapter_Reason;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason$ProtoAdapter_Reason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 231
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 232
    iput p3, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 245
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->RESET:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0

    .line 244
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->LOST:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0

    .line 243
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->OTHER:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0

    .line 242
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->REFUND:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0

    .line 241
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->CASH_OUT:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0

    .line 240
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;
    .locals 1

    .line 193
    const-class v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;
    .locals 1

    .line 193
    sget-object v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->$VALUES:[Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 252
    iget v0, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->value:I

    return v0
.end method
