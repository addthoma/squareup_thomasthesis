.class public final Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;
.super Lcom/squareup/wire/Message;
.source "GetEGiftOrderConfigurationResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$ProtoAdapter_GetEGiftOrderConfigurationResponse;,
        Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final all_egift_themes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.EGiftTheme#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field public final denomination_amounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field public final order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.EGiftOrderConfiguration#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$ProtoAdapter_GetEGiftOrderConfigurationResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$ProtoAdapter_GetEGiftOrderConfigurationResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 66
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 72
    sget-object v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    const-string p1, "all_egift_themes"

    .line 74
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    .line 75
    iput-object p3, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const-string p1, "denomination_amounts"

    .line 76
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 93
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 94
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    .line 97
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    .line 99
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 104
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;-><init>()V

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->denomination_amounts:Ljava/util/List;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->newBuilder()Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", all_egift_themes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v1, :cond_2

    const-string v1, ", order_configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", denomination_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;->denomination_amounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetEGiftOrderConfigurationResponse{"

    .line 123
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
