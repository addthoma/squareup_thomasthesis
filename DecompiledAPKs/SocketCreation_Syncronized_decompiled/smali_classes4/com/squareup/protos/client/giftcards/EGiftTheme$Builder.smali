.class public final Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EGiftTheme.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/EGiftTheme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public background_color:Ljava/lang/String;

.field public image_url:Ljava/lang/String;

.field public read_only_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public background_color(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->background_color:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/EGiftTheme;
    .locals 5

    .line 152
    new-instance v0, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->image_url:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->background_color:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->read_only_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/giftcards/EGiftTheme;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->build()Lcom/squareup/protos/client/giftcards/EGiftTheme;

    move-result-object v0

    return-object v0
.end method

.method public image_url(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->image_url:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftTheme$Builder;->read_only_token:Ljava/lang/String;

    return-object p0
.end method
