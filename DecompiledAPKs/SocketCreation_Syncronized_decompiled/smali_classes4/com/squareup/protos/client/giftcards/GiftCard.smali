.class public final Lcom/squareup/protos/client/giftcards/GiftCard;
.super Lcom/squareup/wire/Message;
.source "GiftCard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/GiftCard$ProtoAdapter_GiftCard;,
        Lcom/squareup/protos/client/giftcards/GiftCard$CardType;,
        Lcom/squareup/protos/client/giftcards/GiftCard$State;,
        Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "Lcom/squareup/protos/client/giftcards/GiftCard$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/GiftCard;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARD_TYPE:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

.field public static final DEFAULT_PAN_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_SERVER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/giftcards/GiftCard$State;

.field private static final serialVersionUID:J


# instance fields
.field public final activity:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.Transaction#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/Transaction;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final balance_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.GiftCard$CardType#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final server_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/giftcards/GiftCard$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.GiftCard$State#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/giftcards/GiftCard$ProtoAdapter_GiftCard;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/GiftCard$ProtoAdapter_GiftCard;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/GiftCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/client/giftcards/GiftCard$State;->UNKNOWN:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    sput-object v0, Lcom/squareup/protos/client/giftcards/GiftCard;->DEFAULT_STATE:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/giftcards/GiftCard$CardType;->SKIPPED:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    sput-object v0, Lcom/squareup/protos/client/giftcards/GiftCard;->DEFAULT_CARD_TYPE:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/giftcards/GiftCard$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/giftcards/GiftCard$CardType;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/giftcards/GiftCard$State;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/Transaction;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/GiftCard$CardType;",
            ")V"
        }
    .end annotation

    .line 84
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/giftcards/GiftCard;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/giftcards/GiftCard$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/giftcards/GiftCard$CardType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/giftcards/GiftCard$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/giftcards/GiftCard$CardType;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/giftcards/GiftCard$State;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/Transaction;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/GiftCard$CardType;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 89
    sget-object v0, Lcom/squareup/protos/client/giftcards/GiftCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    .line 91
    iput-object p2, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    .line 92
    iput-object p3, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    .line 93
    iput-object p4, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    const-string p1, "activity"

    .line 94
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->activity:Ljava/util/List;

    .line 95
    iput-object p6, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 114
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 115
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/giftcards/GiftCard;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GiftCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/GiftCard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->activity:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->activity:Ljava/util/List;

    .line 121
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    .line 122
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 127
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GiftCard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/GiftCard$State;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->activity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/GiftCard$CardType;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 136
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    .locals 2

    .line 100
    new-instance v0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->server_id:Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->balance_money:Lcom/squareup/protos/common/Money;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->pan_suffix:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->activity:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->activity:Ljava/util/List;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GiftCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GiftCard;->newBuilder()Lcom/squareup/protos/client/giftcards/GiftCard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", server_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    if-eqz v1, :cond_1

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", balance_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->activity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->activity:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    if-eqz v1, :cond_5

    const-string v1, ", card_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GiftCard{"

    .line 150
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
