.class public final Lcom/squareup/protos/client/Status$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Status.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/Status;",
        "Lcom/squareup/protos/client/Status$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ext_orders_error:Lcom/squareup/protos/connect/v2/resources/Error;

.field public localized_description:Ljava/lang/String;

.field public localized_title:Ljava/lang/String;

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/Status;
    .locals 7

    .line 158
    new-instance v6, Lcom/squareup/protos/client/Status;

    iget-object v1, p0, Lcom/squareup/protos/client/Status$Builder;->localized_title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/Status$Builder;->localized_description:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/Status$Builder;->success:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/Status$Builder;->ext_orders_error:Lcom/squareup/protos/connect/v2/resources/Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/resources/Error;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object v0

    return-object v0
.end method

.method public ext_orders_error(Lcom/squareup/protos/connect/v2/resources/Error;)Lcom/squareup/protos/client/Status$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/Status$Builder;->ext_orders_error:Lcom/squareup/protos/connect/v2/resources/Error;

    return-object p0
.end method

.method public localized_description(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/Status$Builder;->localized_description:Ljava/lang/String;

    return-object p0
.end method

.method public localized_title(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/Status$Builder;->localized_title:Ljava/lang/String;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/Status$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
