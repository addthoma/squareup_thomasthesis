.class public final enum Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;
.super Ljava/lang/Enum;
.source "FileAttachmentMetadata.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MimeType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType$ProtoAdapter_MimeType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BMP:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

.field public static final enum JPEG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

.field public static final enum PDF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

.field public static final enum PNG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

.field public static final enum TIFF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 258
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 260
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v2, 0x1

    const-string v3, "JPEG"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->JPEG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 262
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v3, 0x2

    const-string v4, "PNG"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->PNG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 264
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v4, 0x3

    const-string v5, "TIFF"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->TIFF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 266
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v5, 0x4

    const-string v6, "BMP"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->BMP:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 268
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v6, 0x5

    const-string v7, "PDF"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->PDF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 257
    sget-object v7, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->JPEG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->PNG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->TIFF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->BMP:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->PDF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->$VALUES:[Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 270
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType$ProtoAdapter_MimeType;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType$ProtoAdapter_MimeType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 274
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 275
    iput p3, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 288
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->PDF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object p0

    .line 287
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->BMP:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object p0

    .line 286
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->TIFF:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object p0

    .line 285
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->PNG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object p0

    .line 284
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->JPEG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object p0

    .line 283
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;
    .locals 1

    .line 257
    const-class v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;
    .locals 1

    .line 257
    sget-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->$VALUES:[Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 295
    iget v0, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->value:I

    return v0
.end method
