.class public final Lcom/squareup/protos/client/invoice/ListInvoicesRequest;
.super Lcom/squareup/wire/Message;
.source "ListInvoicesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/ListInvoicesRequest$ProtoAdapter_ListInvoicesRequest;,
        Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/ListInvoicesRequest;",
        "Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/ListInvoicesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHECK_ARCHIVE_FOR_MATCHES:Ljava/lang/Boolean;

.field public static final DEFAULT_CONTACT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_INCLUDE_ARCHIVED:Ljava/lang/Boolean;

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_PAGING_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_PARENT_SERIES_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_QUERY:Ljava/lang/String; = ""

.field public static final DEFAULT_SORT_ASCENDING:Ljava/lang/Boolean;

.field public static final DEFAULT_STATE_FILTER:Lcom/squareup/protos/client/invoice/StateFilter;

.field private static final serialVersionUID:J


# instance fields
.field public final check_archive_for_matches:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final contact_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final date_range:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final display_state_filter:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceDisplayDetails$DisplayState#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
            ">;"
        }
    .end annotation
.end field

.field public final include_archived:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final invoice_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final paging_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final parent_series_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final query:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final sort_ascending:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final state_filter:Lcom/squareup/protos/client/invoice/StateFilter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.StateFilter#ADAPTER"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$ProtoAdapter_ListInvoicesRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$ProtoAdapter_ListInvoicesRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->DEFAULT_LIMIT:Ljava/lang/Integer;

    const/4 v0, 0x1

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->DEFAULT_SORT_ASCENDING:Ljava/lang/Boolean;

    .line 37
    sget-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->UNKNOWN:Lcom/squareup/protos/client/invoice/StateFilter;

    sput-object v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->DEFAULT_STATE_FILTER:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 43
    sput-object v1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->DEFAULT_CHECK_ARCHIVE_FOR_MATCHES:Ljava/lang/Boolean;

    .line 45
    sput-object v1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->DEFAULT_INCLUDE_ARCHIVED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/StateFilter;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/invoice/StateFilter;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTimeInterval;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 163
    sget-object v13, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/StateFilter;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/StateFilter;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/invoice/StateFilter;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTimeInterval;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 171
    sget-object v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p13}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->paging_key:Ljava/lang/String;

    .line 173
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->limit:Ljava/lang/Integer;

    .line 174
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->query:Ljava/lang/String;

    const-string p1, "display_state_filter"

    .line 175
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->display_state_filter:Ljava/util/List;

    .line 176
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->sort_ascending:Ljava/lang/Boolean;

    .line 177
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 178
    iput-object p7, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 179
    iput-object p8, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->contact_token:Ljava/lang/String;

    .line 180
    iput-object p9, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 181
    iput-object p10, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->parent_series_token:Ljava/lang/String;

    .line 182
    iput-object p11, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    .line 183
    iput-object p12, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->include_archived:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 208
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 209
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;

    .line 210
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->paging_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->paging_key:Ljava/lang/String;

    .line 211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->limit:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->limit:Ljava/lang/Integer;

    .line 212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->query:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->query:Ljava/lang/String;

    .line 213
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->display_state_filter:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->display_state_filter:Ljava/util/List;

    .line 214
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->sort_ascending:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->sort_ascending:Ljava/lang/Boolean;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->contact_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->contact_token:Ljava/lang/String;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->parent_series_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->parent_series_token:Ljava/lang/String;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->include_archived:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->include_archived:Ljava/lang/Boolean;

    .line 222
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 227
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 229
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->paging_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->query:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->display_state_filter:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->sort_ascending:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/StateFilter;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->parent_series_token:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->include_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 242
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 2

    .line 188
    new-instance v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;-><init>()V

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->paging_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->paging_key:Ljava/lang/String;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->limit:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->limit:Ljava/lang/Integer;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->query:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->query:Ljava/lang/String;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->display_state_filter:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->display_state_filter:Ljava/util/List;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->sort_ascending:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->sort_ascending:Ljava/lang/Boolean;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->contact_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->contact_token:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->parent_series_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->parent_series_token:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->check_archive_for_matches:Ljava/lang/Boolean;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->include_archived:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->include_archived:Ljava/lang/Boolean;

    .line 201
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->newBuilder()Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", paging_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->paging_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->limit:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->query:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->display_state_filter:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", display_state_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->display_state_filter:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->sort_ascending:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", sort_ascending="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->sort_ascending:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 255
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_5

    const-string v1, ", invoice_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 256
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    if-eqz v1, :cond_6

    const-string v1, ", state_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 257
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", contact_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_8

    const-string v1, ", date_range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 259
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->parent_series_token:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", parent_series_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->parent_series_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", check_archive_for_matches="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 261
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->include_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", include_archived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;->include_archived:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListInvoicesRequest{"

    .line 262
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
