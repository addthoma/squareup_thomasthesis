.class public final enum Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
.super Ljava/lang/Enum;
.source "InvoiceTenderDetails.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TenderType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType$ProtoAdapter_TenderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CAPITAL:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final enum CARD:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final enum CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final enum CHECK:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final enum INSTRUMENT:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final enum OTHER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final enum SQ_REGISTER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 243
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v1, 0x0

    const-string v2, "CASH"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 245
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v2, 0x1

    const-string v3, "CHECK"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CHECK:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 247
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v3, 0x2

    const-string v4, "OTHER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->OTHER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 249
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v4, 0x3

    const-string v5, "CARD"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CARD:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 251
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v5, 0x4

    const-string v6, "INSTRUMENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->INSTRUMENT:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 253
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v6, 0x5

    const-string v7, "SQ_REGISTER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->SQ_REGISTER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 255
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v7, 0x6

    const-string v8, "CAPITAL"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CAPITAL:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 242
    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CHECK:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->OTHER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CARD:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->INSTRUMENT:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->SQ_REGISTER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CAPITAL:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->$VALUES:[Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 257
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType$ProtoAdapter_TenderType;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType$ProtoAdapter_TenderType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 262
    iput p3, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 276
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CAPITAL:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0

    .line 275
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->SQ_REGISTER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0

    .line 274
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->INSTRUMENT:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0

    .line 273
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CARD:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0

    .line 272
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->OTHER:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0

    .line 271
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CHECK:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0

    .line 270
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
    .locals 1

    .line 242
    const-class v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
    .locals 1

    .line 242
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->$VALUES:[Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 283
    iget v0, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->value:I

    return v0
.end method
