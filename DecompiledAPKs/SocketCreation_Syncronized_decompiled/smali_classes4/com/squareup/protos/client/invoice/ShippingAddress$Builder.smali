.class public final Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ShippingAddress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ShippingAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/ShippingAddress;",
        "Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public formatted_address:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/ShippingAddress;
    .locals 5

    .line 135
    new-instance v0, Lcom/squareup/protos/client/invoice/ShippingAddress;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;->formatted_address:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/ShippingAddress;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;->build()Lcom/squareup/protos/client/invoice/ShippingAddress;

    move-result-object v0

    return-object v0
.end method

.method public formatted_address(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;->formatted_address:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ShippingAddress$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
