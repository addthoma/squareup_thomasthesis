.class public final Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;
.super Lcom/squareup/wire/Message;
.source "FileAttachmentMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$ProtoAdapter_FileAttachmentMetadata;,
        Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;,
        Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_EXTENSION:Ljava/lang/String; = ""

.field public static final DEFAULT_INVOICE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MIME_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SIZE_BYTES:Ljava/lang/Integer;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final extension:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final invoice_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final mime_type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final size_bytes:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final updated_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final uploaded_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$ProtoAdapter_FileAttachmentMetadata;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$ProtoAdapter_FileAttachmentMetadata;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->DEFAULT_SIZE_BYTES:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 11

    .line 99
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1

    .line 105
    sget-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    .line 107
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->description:Ljava/lang/String;

    .line 108
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    .line 109
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    .line 110
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    .line 111
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->invoice_token:Ljava/lang/String;

    .line 112
    iput-object p7, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    .line 113
    iput-object p8, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    .line 114
    iput-object p9, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->description:Ljava/lang/String;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->invoice_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->invoice_token:Ljava/lang/String;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    .line 146
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    .line 147
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 152
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 154
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->invoice_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 164
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;
    .locals 2

    .line 119
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->name:Ljava/lang/String;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->description:Ljava/lang/String;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->extension:Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->mime_type:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->token:Ljava/lang/String;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->invoice_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->invoice_token:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->size_bytes:Ljava/lang/Integer;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->newBuilder()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", extension="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", mime_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->invoice_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", invoice_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->invoice_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", size_bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 179
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_7

    const-string v1, ", uploaded_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->uploaded_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 180
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_8

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FileAttachmentMetadata{"

    .line 181
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
