.class public final Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceRefund.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceRefund;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
        "Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Ljava/lang/String;

.field public last_four:Ljava/lang/String;

.field public reason:Ljava/lang/String;

.field public refunded_at:Lcom/squareup/protos/client/ISO8601Date;

.field public refunded_money:Lcom/squareup/protos/common/Money;

.field public tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 170
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->brand:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/InvoiceRefund;
    .locals 9

    .line 219
    new-instance v8, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->refunded_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->reason:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->brand:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->last_four:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/InvoiceRefund;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceRefund;

    move-result-object v0

    return-object v0
.end method

.method public last_four(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->last_four:Ljava/lang/String;

    return-object p0
.end method

.method public reason(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->reason:Ljava/lang/String;

    return-object p0
.end method

.method public refunded_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public refunded_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->refunded_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tender_type(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;)Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceRefund$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-object p0
.end method
