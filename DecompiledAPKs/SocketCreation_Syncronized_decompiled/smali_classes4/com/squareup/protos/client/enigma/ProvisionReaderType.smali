.class public final enum Lcom/squareup/protos/client/enigma/ProvisionReaderType;
.super Ljava/lang/Enum;
.source "ProvisionReaderType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/enigma/ProvisionReaderType$ProtoAdapter_ProvisionReaderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/enigma/ProvisionReaderType;

.field public static final enum A10:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/enigma/ProvisionReaderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/enigma/ProvisionReaderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 14
    new-instance v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/enigma/ProvisionReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    .line 16
    new-instance v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    const/4 v2, 0x1

    const-string v3, "A10"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/enigma/ProvisionReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->A10:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    .line 13
    sget-object v3, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->A10:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->$VALUES:[Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    .line 18
    new-instance v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType$ProtoAdapter_ProvisionReaderType;

    invoke-direct {v0}, Lcom/squareup/protos/client/enigma/ProvisionReaderType$ProtoAdapter_ProvisionReaderType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput p3, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/enigma/ProvisionReaderType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 32
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->A10:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    return-object p0

    .line 31
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/enigma/ProvisionReaderType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->$VALUES:[Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/enigma/ProvisionReaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->value:I

    return v0
.end method
