.class final Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetCardActivityResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetCardActivityResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 340
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 367
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;-><init>()V

    .line 368
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 369
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 378
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 376
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->max_instant_deposit_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    goto :goto_0

    .line 375
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    goto :goto_0

    .line 374
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->fetched_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    goto :goto_0

    .line 373
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    goto :goto_0

    .line 372
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->batch_response(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    goto :goto_0

    .line 371
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->activity_list:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 382
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 383
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 338
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 356
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 357
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 358
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 359
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 360
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 361
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 362
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 338
    check-cast p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)I
    .locals 4

    .line 345
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    const/4 v3, 0x2

    .line 346
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v3, 0x3

    .line 347
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x4

    .line 348
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 349
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 350
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 338
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;
    .locals 2

    .line 388
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    move-result-object p1

    .line 389
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->activity_list:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 390
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    .line 391
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 392
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 393
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    .line 394
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 395
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 396
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 338
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;->redact(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    move-result-object p1

    return-object p1
.end method
