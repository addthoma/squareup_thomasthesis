.class public final Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetCardActivityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

.field public filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public batch_request(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;-><init>(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public filters(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    return-object p0
.end method
