.class final Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FinishCardActivationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FinishCardActivationRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 297
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 318
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;-><init>()V

    .line 319
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 320
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 326
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 324
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    goto :goto_0

    .line 323
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    goto :goto_0

    .line 322
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->activation_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    goto :goto_0

    .line 330
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 331
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 295
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 311
    sget-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 295
    check-cast p2, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)I
    .locals 4

    .line 302
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    const/4 v3, 0x4

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 v3, 0x5

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 295
    check-cast p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;->encodedSize(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
    .locals 2

    .line 336
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 337
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->activation_token:Ljava/lang/String;

    .line 338
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    .line 339
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    .line 340
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 341
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 295
    check-cast p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;->redact(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    move-result-object p1

    return-object p1
.end method
