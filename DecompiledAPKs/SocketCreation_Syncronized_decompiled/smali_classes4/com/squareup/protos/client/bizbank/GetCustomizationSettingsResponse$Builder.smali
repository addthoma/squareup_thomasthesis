.class public final Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetCustomizationSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_name:Ljava/lang/String;

.field public can_retry_idv:Ljava/lang/Boolean;

.field public idv_needs_ssn:Ljava/lang/Boolean;

.field public idv_retries_remaining:Ljava/lang/Integer;

.field public idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

.field public max_ink_coverage:Ljava/lang/Float;

.field public min_ink_coverage:Ljava/lang/Float;

.field public owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public owner_name:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 249
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;
    .locals 14

    .line 319
    new-instance v13, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->business_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->min_ink_coverage:Ljava/lang/Float;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->max_ink_coverage:Ljava/lang/Float;

    iget-object v5, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v6, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    iget-object v7, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_needs_ssn:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->can_retry_idv:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_retries_remaining:Ljava/lang/Integer;

    iget-object v10, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v11, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 226
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public business_name(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->business_name:Ljava/lang/String;

    return-object p0
.end method

.method public can_retry_idv(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->can_retry_idv:Ljava/lang/Boolean;

    return-object p0
.end method

.method public idv_needs_ssn(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 293
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_needs_ssn:Ljava/lang/Boolean;

    return-object p0
.end method

.method public idv_retries_remaining(Ljava/lang/Integer;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 303
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_retries_remaining:Ljava/lang/Integer;

    return-object p0
.end method

.method public idv_state(Lcom/squareup/protos/client/bizbank/IdvState;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    return-object p0
.end method

.method public max_ink_coverage(Ljava/lang/Float;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 278
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->max_ink_coverage:Ljava/lang/Float;

    return-object p0
.end method

.method public min_ink_coverage(Ljava/lang/Float;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->min_ink_coverage:Ljava/lang/Float;

    return-object p0
.end method

.method public owner_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public owner_birth_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public owner_name(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_name:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
