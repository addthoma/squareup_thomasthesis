.class public final Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;
.super Lcom/squareup/wire/Message;
.source "VerifyCardActivationTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$ProtoAdapter_VerifyCardActivationTokenResponse;,
        Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;,
        Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RESULT:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

.field private static final serialVersionUID:J


# instance fields
.field public final result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.VerifyCardActivationTokenResponse$Result#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$ProtoAdapter_VerifyCardActivationTokenResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$ProtoAdapter_VerifyCardActivationTokenResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->VERIFIED:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->DEFAULT_RESULT:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;)V
    .locals 1

    .line 35
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;-><init>(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;Lokio/ByteString;)V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 40
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 54
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 55
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    .line 57
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 62
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 66
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;-><init>()V

    .line 46
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    if-eqz v1, :cond_0

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VerifyCardActivationTokenResponse{"

    .line 75
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
