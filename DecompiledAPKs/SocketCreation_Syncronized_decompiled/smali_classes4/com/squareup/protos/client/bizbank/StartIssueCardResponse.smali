.class public final Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;
.super Lcom/squareup/wire/Message;
.source "StartIssueCardResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$ProtoAdapter_StartIssueCardResponse;,
        Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;,
        Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADDRESS_NORMALIZATION_RESULT:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

.field public static final DEFAULT_CARD_ISSUE_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.StartIssueCardResponse$AddressNormalizationResult#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final card_issue_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final corrected_field:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.solidshop.VerifyShippingAddressResponse$CorrectedField#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;"
        }
    .end annotation
.end field

.field public final normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$ProtoAdapter_StartIssueCardResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$ProtoAdapter_StartIssueCardResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->SUCCESS:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->DEFAULT_ADDRESS_NORMALIZATION_RESULT:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;)V"
        }
    .end annotation

    .line 73
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 80
    sget-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    .line 82
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 84
    iput-object p4, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    const-string p1, "corrected_field"

    .line 85
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 103
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 104
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    .line 110
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 115
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
    .locals 2

    .line 90
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->card_issue_token:Ljava/lang/String;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->corrected_field:Ljava/util/List;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", card_issue_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_2

    const-string v1, ", normalized_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    if-eqz v1, :cond_3

    const-string v1, ", address_normalization_result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", corrected_field="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartIssueCardResponse{"

    .line 136
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
