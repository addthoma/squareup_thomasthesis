.class final Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetBalanceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetBalanceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetBalanceResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/GetBalanceResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 150
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 171
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;-><init>()V

    .line 172
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 173
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 179
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 177
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->pending_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;

    goto :goto_0

    .line 176
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->max_instant_deposit_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;

    goto :goto_0

    .line 175
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;

    goto :goto_0

    .line 183
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 184
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetBalanceResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 163
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 164
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 165
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 166
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    check-cast p2, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetBalanceResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/GetBalanceResponse;)I
    .locals 4

    .line 155
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 156
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 157
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/GetBalanceResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/GetBalanceResponse;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse;
    .locals 2

    .line 189
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;

    move-result-object p1

    .line 190
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    .line 191
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 192
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    .line 193
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;->redact(Lcom/squareup/protos/client/bizbank/GetBalanceResponse;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    move-result-object p1

    return-object p1
.end method
