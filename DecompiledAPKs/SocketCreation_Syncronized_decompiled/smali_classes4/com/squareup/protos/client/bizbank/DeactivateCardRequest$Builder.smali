.class public final Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeactivateCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_token:Ljava/lang/String;

.field public deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->card_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public deactivation_reason(Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object p0
.end method
