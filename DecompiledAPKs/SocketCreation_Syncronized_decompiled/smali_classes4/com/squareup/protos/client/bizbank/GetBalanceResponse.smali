.class public final Lcom/squareup/protos/client/bizbank/GetBalanceResponse;
.super Lcom/squareup/wire/Message;
.source "GetBalanceResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;,
        Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/GetBalanceResponse;",
        "Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/GetBalanceResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final max_instant_deposit_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final pending_balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$ProtoAdapter_GetBalanceResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 54
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 60
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    .line 61
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 62
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 78
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 79
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    .line 81
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 82
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    .line 83
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 88
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 94
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", max_instant_deposit_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", pending_balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;->pending_balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetBalanceResponse{"

    .line 105
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
