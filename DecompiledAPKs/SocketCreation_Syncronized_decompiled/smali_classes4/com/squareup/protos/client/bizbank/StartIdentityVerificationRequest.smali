.class public final Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;
.super Lcom/squareup/wire/Message;
.source "StartIdentityVerificationRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$ProtoAdapter_StartIdentityVerificationRequest;,
        Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OWNER_SOCIAL_SECURITY_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final owner_address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final owner_social_security_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$ProtoAdapter_StartIdentityVerificationRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$ProtoAdapter_StartIdentityVerificationRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 58
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 59
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 60
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_social_security_number:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 76
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 77
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 79
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_social_security_number:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_social_security_number:Ljava/lang/String;

    .line 81
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_social_security_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 92
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;-><init>()V

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_social_security_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_social_security_number:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_0

    const-string v1, ", owner_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_1

    const-string v1, ", owner_birth_date=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;->owner_social_security_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", owner_social_security_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartIdentityVerificationRequest{"

    .line 103
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
