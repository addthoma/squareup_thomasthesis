.class final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CheckIdentityVerificationStatusResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 660
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 682
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;-><init>()V

    .line 683
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 684
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 690
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 688
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    goto :goto_0

    .line 687
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    goto :goto_0

    .line 686
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    goto :goto_0

    .line 694
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 695
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 658
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 674
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 675
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 676
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 677
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 658
    check-cast p2, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)I
    .locals 4

    .line 665
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    const/4 v3, 0x2

    .line 666
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    const/4 v3, 0x3

    .line 667
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 668
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 658
    check-cast p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
    .locals 2

    .line 701
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    move-result-object p1

    .line 702
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    .line 703
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    .line 704
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    .line 705
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 706
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 658
    check-cast p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;->redact(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    move-result-object p1

    return-object p1
.end method
