.class final Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CouponReward.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/CouponReward;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CouponReward"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/coupons/CouponReward;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 293
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/coupons/CouponReward;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/coupons/CouponReward;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 320
    new-instance v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;-><init>()V

    .line 321
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 322
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 345
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 343
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->max_discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;

    goto :goto_0

    .line 342
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->percentage_discount(Ljava/lang/Integer;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;

    goto :goto_0

    .line 341
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->fixed_discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;

    goto :goto_0

    .line 335
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/coupons/CouponReward$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/coupons/CouponReward$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->type(Lcom/squareup/protos/client/coupons/CouponReward$Type;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 337
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 332
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->item_constraint:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/coupons/ItemConstraint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    :pswitch_5
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/coupons/Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/coupons/Scope;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->scope(Lcom/squareup/protos/client/coupons/Scope;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 328
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 349
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 350
    invoke-virtual {v0}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->build()Lcom/squareup/protos/client/coupons/CouponReward;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 291
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/coupons/CouponReward;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/coupons/CouponReward;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 309
    sget-object v0, Lcom/squareup/protos/client/coupons/Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 310
    sget-object v0, Lcom/squareup/protos/client/coupons/ItemConstraint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 311
    sget-object v0, Lcom/squareup/protos/client/coupons/CouponReward$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    invoke-virtual {p2}, Lcom/squareup/protos/client/coupons/CouponReward;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 291
    check-cast p2, Lcom/squareup/protos/client/coupons/CouponReward;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/coupons/CouponReward;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/coupons/CouponReward;)I
    .locals 4

    .line 298
    sget-object v0, Lcom/squareup/protos/client/coupons/Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/coupons/ItemConstraint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 299
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/CouponReward$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    const/4 v3, 0x3

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/CouponReward;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 291
    check-cast p1, Lcom/squareup/protos/client/coupons/CouponReward;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;->encodedSize(Lcom/squareup/protos/client/coupons/CouponReward;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/coupons/CouponReward;)Lcom/squareup/protos/client/coupons/CouponReward;
    .locals 2

    .line 355
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/CouponReward;->newBuilder()Lcom/squareup/protos/client/coupons/CouponReward$Builder;

    move-result-object p1

    .line 356
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->item_constraint:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/coupons/ItemConstraint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 357
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->fixed_discount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->fixed_discount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->fixed_discount:Lcom/squareup/protos/common/Money;

    .line 358
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->max_discount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->max_discount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->max_discount:Lcom/squareup/protos/common/Money;

    .line 359
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 360
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->build()Lcom/squareup/protos/client/coupons/CouponReward;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 291
    check-cast p1, Lcom/squareup/protos/client/coupons/CouponReward;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;->redact(Lcom/squareup/protos/client/coupons/CouponReward;)Lcom/squareup/protos/client/coupons/CouponReward;

    move-result-object p1

    return-object p1
.end method
