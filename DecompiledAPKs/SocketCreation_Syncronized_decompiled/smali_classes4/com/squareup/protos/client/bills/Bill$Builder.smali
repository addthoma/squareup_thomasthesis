.class public final Lcom/squareup/protos/client/bills/Bill$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Bill.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Bill;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Bill;",
        "Lcom/squareup/protos/client/bills/Bill$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public current_amendment_client_token:Ljava/lang/String;

.field public dates:Lcom/squareup/protos/client/bills/Bill$Dates;

.field public merchant:Lcom/squareup/protos/client/Merchant;

.field public order:Lcom/squareup/orders/model/Order;

.field public read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

.field public read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

.field public read_only_refund:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;"
        }
    .end annotation
.end field

.field public refund:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Refund;",
            ">;"
        }
    .end annotation
.end field

.field public square_product:Lcom/squareup/protos/client/bills/SquareProduct;

.field public square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

.field public tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 316
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 317
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->tender:Ljava/util/List;

    .line 318
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_refund:Ljava/util/List;

    .line 319
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->refund:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Bill;
    .locals 17

    move-object/from16 v0, p0

    .line 449
    new-instance v16, Lcom/squareup/protos/client/bills/Bill;

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v4, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->tender:Ljava/util/List;

    iget-object v5, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v6, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v7, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_refund:Ljava/util/List;

    iget-object v8, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v9, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    iget-object v10, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->refund:Ljava/util/List;

    iget-object v11, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    iget-object v12, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->order:Lcom/squareup/orders/model/Order;

    iget-object v13, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->current_amendment_client_token:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/client/bills/Bill;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Ljava/util/List;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/SquareProduct;Ljava/util/List;Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;Lcom/squareup/orders/model/Order;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 289
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Bill$Builder;->build()Lcom/squareup/protos/client/bills/Bill;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public current_amendment_client_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 433
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->current_amendment_client_token:Ljava/lang/String;

    return-object p0
.end method

.method public dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 361
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    return-object p0
.end method

.method public merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    return-object p0
.end method

.method public order(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 419
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->order:Lcom/squareup/orders/model/Order;

    return-object p0
.end method

.method public read_only_electronic_signature_details(Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    return-object p0
.end method

.method public read_only_loyalty_details(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 443
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    return-object p0
.end method

.method public read_only_refund(Ljava/util/List;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Bill$Builder;"
        }
    .end annotation

    .line 371
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 372
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_refund:Ljava/util/List;

    return-object p0
.end method

.method public refund(Ljava/util/List;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Refund;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Bill$Builder;"
        }
    .end annotation

    .line 397
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 398
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->refund:Ljava/util/List;

    return-object p0
.end method

.method public square_product(Lcom/squareup/protos/client/bills/SquareProduct;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0
.end method

.method public square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    return-object p0
.end method

.method public tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Bill$Builder;"
        }
    .end annotation

    .line 347
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 348
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Builder;->tender:Ljava/util/List;

    return-object p0
.end method
