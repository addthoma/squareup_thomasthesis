.class public final Lcom/squareup/protos/client/bills/Itemization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "Lcom/squareup/protos/client/bills/Itemization$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

.field public configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public custom_note:Ljava/lang/String;

.field public event:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation
.end field

.field public feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

.field public itemization_id_pair:Lcom/squareup/protos/client/IdPair;

.field public measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

.field public quantity:Ljava/lang/String;

.field public quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field public read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

.field public write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

.field public write_only_deleted:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 319
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 320
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->event:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/Itemization$Amounts;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 390
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Itemization;
    .locals 17

    move-object/from16 v0, p0

    .line 428
    new-instance v16, Lcom/squareup/protos/client/bills/Itemization;

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iget-object v5, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iget-object v6, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->custom_note:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v8, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v9, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v10, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v11, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v12, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_deleted:Ljava/lang/Boolean;

    iget-object v13, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->event:Ljava/util/List;

    iget-object v14, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/client/bills/Itemization;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Configuration;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Amounts;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 292
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v0

    return-object v0
.end method

.method public configuration(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 369
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 402
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public custom_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->custom_note:Ljava/lang/String;

    return-object p0
.end method

.method public event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$Builder;"
        }
    .end annotation

    .line 416
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 417
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->event:Ljava/util/List;

    return-object p0
.end method

.method public feature_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 422
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    return-object p0
.end method

.method public itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public measurement_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method

.method public quantity_entry_type(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0
.end method

.method public read_only_display_details(Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    return-object p0
.end method

.method public write_only_backing_details(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 377
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    return-object p0
.end method

.method public write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 0

    .line 411
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_deleted:Ljava/lang/Boolean;

    return-object p0
.end method
