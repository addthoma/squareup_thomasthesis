.class final Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AddTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AddTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/AddTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 370
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/AddTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTender;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 397
    new-instance v0, Lcom/squareup/protos/client/bills/AddTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTender$Builder;-><init>()V

    .line 398
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 399
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 408
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 406
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTender$Builder;->variable_capture_possible(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    goto :goto_0

    .line 405
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTender$Builder;->square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    goto :goto_0

    .line 404
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/AddTender$Logging;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddTender$Logging;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging(Lcom/squareup/protos/client/bills/AddTender$Logging;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    goto :goto_0

    .line 403
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument(Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    goto :goto_0

    .line 402
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/PaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    goto :goto_0

    .line 401
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    goto :goto_0

    .line 412
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/AddTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 413
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTender$Builder;->build()Lcom/squareup/protos/client/bills/AddTender;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 368
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 386
    sget-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 387
    sget-object v0, Lcom/squareup/protos/client/bills/PaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 388
    sget-object v0, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 389
    sget-object v0, Lcom/squareup/protos/client/bills/AddTender$Logging;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 390
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 391
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 392
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/AddTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 368
    check-cast p2, Lcom/squareup/protos/client/bills/AddTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/AddTender;)I
    .locals 4

    .line 375
    sget-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/PaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    const/4 v3, 0x2

    .line 376
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    const/4 v3, 0x3

    .line 377
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddTender$Logging;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    const/4 v3, 0x4

    .line 378
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v3, 0x5

    .line 379
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 380
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 381
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 368
    check-cast p1, Lcom/squareup/protos/client/bills/AddTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;->encodedSize(Lcom/squareup/protos/client/bills/AddTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/AddTender;)Lcom/squareup/protos/client/bills/AddTender;
    .locals 2

    .line 418
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender;->newBuilder()Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object p1

    .line 419
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 420
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/PaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/PaymentInstrument;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 421
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    .line 422
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/AddTender$Logging;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddTender$Logging;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    .line 423
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTender$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 424
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 425
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender$Builder;->build()Lcom/squareup/protos/client/bills/AddTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 368
    check-cast p1, Lcom/squareup/protos/client/bills/AddTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;->redact(Lcom/squareup/protos/client/bills/AddTender;)Lcom/squareup/protos/client/bills/AddTender;

    move-result-object p1

    return-object p1
.end method
