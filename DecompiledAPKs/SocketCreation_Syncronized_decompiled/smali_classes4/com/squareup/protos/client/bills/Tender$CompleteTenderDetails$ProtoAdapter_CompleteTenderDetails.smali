.class final Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ProtoAdapter_CompleteTenderDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CompleteTenderDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1923
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1940
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;-><init>()V

    .line 1941
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1942
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 1946
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1944
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    goto :goto_0

    .line 1950
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1951
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1921
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ProtoAdapter_CompleteTenderDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1934
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1935
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1921
    check-cast p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ProtoAdapter_CompleteTenderDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)I
    .locals 3

    .line 1928
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 1929
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1921
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ProtoAdapter_CompleteTenderDetails;->encodedSize(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .locals 2

    .line 1956
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->newBuilder()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    move-result-object p1

    .line 1957
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    .line 1958
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1959
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1921
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ProtoAdapter_CompleteTenderDetails;->redact(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object p1

    return-object p1
.end method
