.class public final Lcom/squareup/protos/client/bills/OtherTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OtherTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/OtherTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/OtherTender;",
        "Lcom/squareup/protos/client/bills/OtherTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

.field public other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

.field public tender_note:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/OtherTender;
    .locals 7

    .line 176
    new-instance v6, Lcom/squareup/protos/client/bills/OtherTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->tender_note:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/OtherTender;-><init>(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/client/bills/TranslatedName;Lcom/squareup/protos/client/bills/ExternalMetadata;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->build()Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object v0

    return-object v0
.end method

.method public external_metadata(Lcom/squareup/protos/client/bills/ExternalMetadata;)Lcom/squareup/protos/client/bills/OtherTender$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    return-object p0
.end method

.method public other_tender_type(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)Lcom/squareup/protos/client/bills/OtherTender$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0
.end method

.method public read_only_translated_name(Lcom/squareup/protos/client/bills/TranslatedName;)Lcom/squareup/protos/client/bills/OtherTender$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    return-object p0
.end method

.method public tender_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/OtherTender$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/bills/OtherTender$Builder;->tender_note:Ljava/lang/String;

    return-object p0
.end method
