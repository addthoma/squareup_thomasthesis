.class public final Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;",
        "Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_keyed_card_data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1486
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;
    .locals 3

    .line 1496
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->encrypted_keyed_card_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1483
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_keyed_card_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;
    .locals 0

    .line 1490
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->encrypted_keyed_card_data:Lokio/ByteString;

    return-object p0
.end method
