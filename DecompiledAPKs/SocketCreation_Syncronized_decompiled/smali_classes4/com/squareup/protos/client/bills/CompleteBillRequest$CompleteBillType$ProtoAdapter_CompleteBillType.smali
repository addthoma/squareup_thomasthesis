.class final Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType$ProtoAdapter_CompleteBillType;
.super Lcom/squareup/wire/EnumAdapter;
.source "CompleteBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CompleteBillType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 397
    const-class v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;
    .locals 0

    .line 402
    invoke-static {p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->fromValue(I)Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 395
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType$ProtoAdapter_CompleteBillType;->fromValue(I)Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    move-result-object p1

    return-object p1
.end method
