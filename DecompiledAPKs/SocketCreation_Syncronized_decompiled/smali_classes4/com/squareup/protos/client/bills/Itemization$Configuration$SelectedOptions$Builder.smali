.class public final Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

.field public discount:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public fee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

.field public item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

.field public modifier_option:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 787
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 788
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option:Ljava/util/List;

    .line 789
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount:Ljava/util/List;

    .line 790
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
    .locals 9

    .line 844
    new-instance v8, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;-><init>(Lcom/squareup/api/items/ItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Lcom/squareup/protos/client/bills/DiningOptionLineItem;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 774
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object v0

    return-object v0
.end method

.method public dining_option(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    .locals 0

    .line 838
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    return-object p0
.end method

.method public discount(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;"
        }
    .end annotation

    .line 805
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 806
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount:Ljava/util/List;

    return-object p0
.end method

.method public fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;"
        }
    .end annotation

    .line 811
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 812
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee:Ljava/util/List;

    return-object p0
.end method

.method public item_variation_deprecated(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    .locals 0

    .line 794
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_deprecated:Lcom/squareup/api/items/ItemVariation;

    return-object p0
.end method

.method public item_variation_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    .locals 0

    .line 817
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    return-object p0
.end method

.method public modifier_option(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;"
        }
    .end annotation

    .line 799
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 800
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option:Ljava/util/List;

    return-object p0
.end method
