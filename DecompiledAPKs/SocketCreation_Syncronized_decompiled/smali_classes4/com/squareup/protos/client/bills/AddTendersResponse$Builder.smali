.class public final Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTendersResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTendersResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public status:Lcom/squareup/protos/client/Status;

.field public tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 127
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->tender:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/AddTendersResponse;
    .locals 5

    .line 166
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->tender:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/AddTendersResponse;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;"
        }
    .end annotation

    .line 159
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersResponse$Builder;->tender:Ljava/util/List;

    return-object p0
.end method
