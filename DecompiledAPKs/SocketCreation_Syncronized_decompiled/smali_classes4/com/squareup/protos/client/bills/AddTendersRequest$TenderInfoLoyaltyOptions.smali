.class public final Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;
.super Lcom/squareup/wire/Message;
.source "AddTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTendersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TenderInfoLoyaltyOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;,
        Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_ACCRUE_LOYALTY:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final can_accrue_loyalty:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyTermsOfService#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 500
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x1

    .line 504
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->DEFAULT_CAN_ACCRUE_LOYALTY:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)V
    .locals 1

    .line 528
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;Lokio/ByteString;)V
    .locals 1

    .line 533
    sget-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 534
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    .line 535
    iput-object p2, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 550
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 551
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    .line 552
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    .line 553
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    .line 554
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 559
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 561
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 562
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 563
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 564
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;
    .locals 2

    .line 540
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;-><init>()V

    .line 541
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->can_accrue_loyalty:Ljava/lang/Boolean;

    .line 542
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    .line 543
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 499
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 572
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", can_accrue_loyalty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 573
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    if-eqz v1, :cond_1

    const-string v1, ", max_supported_tos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TenderInfoLoyaltyOptions{"

    .line 574
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
