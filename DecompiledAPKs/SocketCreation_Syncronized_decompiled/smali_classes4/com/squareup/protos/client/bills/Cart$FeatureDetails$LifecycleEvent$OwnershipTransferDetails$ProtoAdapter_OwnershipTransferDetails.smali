.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OwnershipTransferDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 6451
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6471
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;-><init>()V

    .line 6472
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 6473
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 6478
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 6476
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_to_employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;

    goto :goto_0

    .line 6475
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_from_employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;

    goto :goto_0

    .line 6482
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 6483
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6449
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6464
    sget-object v0, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6465
    sget-object v0, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6466
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6449
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)I
    .locals 4

    .line 6456
    sget-object v0, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    const/4 v3, 0x2

    .line 6457
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6458
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 6449
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;
    .locals 2

    .line 6488
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;

    move-result-object p1

    .line 6489
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Employee;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    .line 6490
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/Employee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Employee;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    .line 6491
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 6492
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 6449
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    move-result-object p1

    return-object p1
.end method
