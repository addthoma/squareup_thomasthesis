.class public final enum Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;
.super Ljava/lang/Enum;
.source "AddedTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReceiptBehavior"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior$ProtoAdapter_ReceiptBehavior;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

.field public static final enum SKIP_ASKING:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 386
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->UNKNOWN:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    .line 388
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    const/4 v3, 0x2

    const-string v4, "DEFAULT"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->DEFAULT:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    .line 390
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    const/4 v4, 0x3

    const-string v5, "SKIP_ASKING"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->SKIP_ASKING:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    new-array v0, v4, [Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    .line 385
    sget-object v4, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->UNKNOWN:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->DEFAULT:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->SKIP_ASKING:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->$VALUES:[Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    .line 392
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior$ProtoAdapter_ReceiptBehavior;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior$ProtoAdapter_ReceiptBehavior;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 396
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 397
    iput p3, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 407
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->SKIP_ASKING:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    return-object p0

    .line 406
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->DEFAULT:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    return-object p0

    .line 405
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->UNKNOWN:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;
    .locals 1

    .line 385
    const-class v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;
    .locals 1

    .line 385
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->$VALUES:[Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 414
    iget v0, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->value:I

    return v0
.end method
