.class public final Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;
.super Lcom/squareup/wire/Message;
.source "RefundRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RefundRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PresentedCardDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$ProtoAdapter_PresentedCardDetails;,
        Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;",
        "Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final DEFAULT_IS_REFUND_DESTINATION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final card_data:Lcom/squareup/protos/client/bills/CardData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$EntryMethod#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final is_refund_destination:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 158
    new-instance v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$ProtoAdapter_PresentedCardDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$ProtoAdapter_PresentedCardDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 162
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sput-object v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v0, 0x0

    .line 164
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->DEFAULT_IS_REFUND_DESTINATION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/Boolean;)V
    .locals 1

    .line 196
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;-><init>(Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 201
    sget-object v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 203
    iput-object p2, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 204
    iput-object p3, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->is_refund_destination:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 220
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 221
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->card_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->is_refund_destination:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->is_refund_destination:Ljava/lang/Boolean;

    .line 225
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 230
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 232
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->card_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->is_refund_destination:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 236
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;
    .locals 2

    .line 209
    new-instance v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;-><init>()V

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->card_data:Lcom/squareup/protos/client/bills/CardData;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->is_refund_destination:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->is_refund_destination:Ljava/lang/Boolean;

    .line 213
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->newBuilder()Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->card_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v1, :cond_0

    const-string v1, ", card_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->card_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_1

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->is_refund_destination:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", is_refund_destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;->is_refund_destination:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PresentedCardDetails{"

    .line 247
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
