.class public final Lcom/squareup/protos/client/bills/ClientDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ClientDetails;",
        "Lcom/squareup/protos/client/bills/ClientDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_version:Ljava/lang/String;

.field public device_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public app_version(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ClientDetails$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ClientDetails$Builder;->app_version:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/ClientDetails;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/client/bills/ClientDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ClientDetails$Builder;->device_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ClientDetails$Builder;->app_version:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ClientDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ClientDetails$Builder;->build()Lcom/squareup/protos/client/bills/ClientDetails;

    move-result-object v0

    return-object v0
.end method

.method public device_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ClientDetails$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ClientDetails$Builder;->device_id:Ljava/lang/String;

    return-object p0
.end method
