.class public final Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_accrue_loyalty:Ljava/lang/Boolean;

.field public max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 582
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;
    .locals 4

    .line 605
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->can_accrue_loyalty:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 577
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    move-result-object v0

    return-object v0
.end method

.method public can_accrue_loyalty(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;
    .locals 0

    .line 589
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->can_accrue_loyalty:Ljava/lang/Boolean;

    return-object p0
.end method

.method public max_supported_tos(Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;
    .locals 0

    .line 599
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    return-object p0
.end method
