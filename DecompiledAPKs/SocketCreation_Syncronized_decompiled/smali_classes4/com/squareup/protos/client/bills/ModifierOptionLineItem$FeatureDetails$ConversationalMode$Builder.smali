.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 897
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;
    .locals 3

    .line 910
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 894
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;

    move-result-object v0

    return-object v0
.end method

.method public type(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;
    .locals 0

    .line 904
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Builder;->type:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;

    return-object p0
.end method
