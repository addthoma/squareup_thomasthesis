.class public final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;
.super Lcom/squareup/wire/Message;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResidualTip"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SOURCE_BILL_SERVER_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.TipLineItem#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final source_bill_server_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 868
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/TipLineItem;)V
    .locals 1

    .line 894
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/TipLineItem;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/TipLineItem;Lokio/ByteString;)V
    .locals 1

    .line 899
    sget-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 900
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    .line 901
    iput-object p2, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 916
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 917
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    .line 918
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    .line 919
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    .line 920
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 925
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 927
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 928
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 929
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/TipLineItem;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 930
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;
    .locals 2

    .line 906
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;-><init>()V

    .line 907
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->source_bill_server_token:Ljava/lang/String;

    .line 908
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    .line 909
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 867
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 937
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 938
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", source_bill_server_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    if-eqz v1, :cond_1

    const-string v1, ", residual_tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ResidualTip{"

    .line 940
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
