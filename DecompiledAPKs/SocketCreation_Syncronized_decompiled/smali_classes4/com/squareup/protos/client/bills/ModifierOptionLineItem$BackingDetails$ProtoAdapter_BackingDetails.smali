.class final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BackingDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 689
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 708
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;-><init>()V

    .line 709
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 710
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 715
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 713
    :cond_0
    sget-object v3, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    goto :goto_0

    .line 712
    :cond_1
    sget-object v3, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    goto :goto_0

    .line 719
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 720
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 687
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 701
    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 702
    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 703
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 687
    check-cast p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)I
    .locals 4

    .line 694
    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    const/4 v3, 0x2

    .line 695
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 696
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 687
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;->encodedSize(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;
    .locals 2

    .line 725
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    move-result-object p1

    .line 726
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierOption;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 727
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 728
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 729
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 687
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;->redact(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    move-result-object p1

    return-object p1
.end method
