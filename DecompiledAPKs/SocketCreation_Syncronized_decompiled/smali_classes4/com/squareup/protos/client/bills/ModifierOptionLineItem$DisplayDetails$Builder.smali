.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cogs_object_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public sort_order:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 502
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;
    .locals 5

    .line 531
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->sort_order:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 495
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public cogs_object_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;
    .locals 0

    .line 525
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;
    .locals 0

    .line 509
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public sort_order(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;
    .locals 0

    .line 517
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->sort_order:Ljava/lang/Integer;

    return-object p0
.end method
