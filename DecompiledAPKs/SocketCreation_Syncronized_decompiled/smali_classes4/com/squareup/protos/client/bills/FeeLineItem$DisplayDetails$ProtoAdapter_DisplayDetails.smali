.class final Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 736
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 769
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;-><init>()V

    .line 770
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 771
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 797
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 795
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_label_order(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    goto :goto_0

    .line 794
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->cogs_object_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    goto :goto_0

    .line 793
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    goto :goto_0

    .line 792
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    goto :goto_0

    .line 791
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    goto :goto_0

    .line 785
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/Fee$InclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->inclusion_type(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 787
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 777
    :pswitch_6
    :try_start_1
    sget-object v4, Lcom/squareup/api/items/CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 779
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 774
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    goto/16 :goto_0

    .line 773
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    goto/16 :goto_0

    .line 801
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 802
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 734
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 755
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 756
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 757
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 758
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 759
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 760
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 761
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 762
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 763
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 764
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 734
    check-cast p2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)I
    .locals 4

    .line 741
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    const/4 v3, 0x2

    .line 742
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    const/4 v3, 0x3

    .line 743
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Fee$InclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    const/4 v3, 0x4

    .line 744
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    const/4 v3, 0x5

    .line 745
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    const/4 v3, 0x6

    .line 746
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    const/4 v3, 0x7

    .line 747
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    const/16 v3, 0x8

    .line 748
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    const/16 v3, 0x9

    .line 749
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 750
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 734
    check-cast p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;->encodedSize(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;
    .locals 0

    .line 807
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    move-result-object p1

    .line 808
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 809
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 734
    check-cast p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;->redact(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    move-result-object p1

    return-object p1
.end method
