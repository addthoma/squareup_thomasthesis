.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PredefinedTicket"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$ProtoAdapter_PredefinedTicket;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final ticket_group:Lcom/squareup/api/items/TicketGroup;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TicketGroup#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final ticket_template:Lcom/squareup/api/items/TicketTemplate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TicketTemplate#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1733
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$ProtoAdapter_PredefinedTicket;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$ProtoAdapter_PredefinedTicket;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 1

    .line 1750
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;-><init>(Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lokio/ByteString;)V
    .locals 1

    .line 1755
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1756
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    .line 1757
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1772
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1773
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 1774
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    .line 1775
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    .line 1776
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1781
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1783
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1784
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/TicketGroup;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1785
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/TicketTemplate;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1786
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;
    .locals 2

    .line 1762
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;-><init>()V

    .line 1763
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    .line 1764
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    .line 1765
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1732
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1793
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1794
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    if-eqz v1, :cond_0

    const-string v1, ", ticket_group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1795
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    if-eqz v1, :cond_1

    const-string v1, ", ticket_template="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PredefinedTicket{"

    .line 1796
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
