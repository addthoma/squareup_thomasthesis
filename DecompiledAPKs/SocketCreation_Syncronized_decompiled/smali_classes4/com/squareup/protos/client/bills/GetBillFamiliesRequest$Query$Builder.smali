.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillFamiliesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_server_token:Ljava/lang/String;

.field public instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

.field public payment_token:Ljava/lang/String;

.field public query_string:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 571
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 601
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->query_string:Ljava/lang/String;

    .line 602
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    .line 603
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;
    .locals 7

    .line 620
    new-instance v6, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->query_string:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->payment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 562
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    move-result-object v0

    return-object v0
.end method

.method public instrument_search(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;
    .locals 0

    .line 589
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    const/4 p1, 0x0

    .line 590
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->query_string:Ljava/lang/String;

    .line 591
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token:Ljava/lang/String;

    .line 592
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;
    .locals 0

    .line 611
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->payment_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 612
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->query_string:Ljava/lang/String;

    .line 613
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    .line 614
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public query_string(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;
    .locals 0

    .line 578
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->query_string:Ljava/lang/String;

    const/4 p1, 0x0

    .line 579
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    .line 580
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token:Ljava/lang/String;

    .line 581
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method
