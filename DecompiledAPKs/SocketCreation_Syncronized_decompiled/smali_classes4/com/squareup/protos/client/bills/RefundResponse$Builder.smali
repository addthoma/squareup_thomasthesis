.class public final Lcom/squareup/protos/client/bills/RefundResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RefundResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RefundResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/RefundResponse;",
        "Lcom/squareup/protos/client/bills/RefundResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public refund:Lcom/squareup/protos/client/bills/Refund;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/RefundResponse;
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/protos/client/bills/RefundResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->refund:Lcom/squareup/protos/client/bills/Refund;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/RefundResponse;-><init>(Lcom/squareup/protos/client/bills/Refund;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->build()Lcom/squareup/protos/client/bills/RefundResponse;

    move-result-object v0

    return-object v0
.end method

.method public refund(Lcom/squareup/protos/client/bills/Refund;)Lcom/squareup/protos/client/bills/RefundResponse$Builder;
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->refund:Lcom/squareup/protos/client/bills/Refund;

    return-object p0
.end method
