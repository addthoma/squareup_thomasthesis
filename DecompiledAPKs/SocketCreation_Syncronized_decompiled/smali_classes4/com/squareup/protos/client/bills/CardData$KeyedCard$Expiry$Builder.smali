.class public final Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public month:Ljava/lang/String;

.field public year:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 641
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;
    .locals 4

    .line 662
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->month:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->year:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 636
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object v0

    return-object v0
.end method

.method public month(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;
    .locals 0

    .line 648
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->month:Ljava/lang/String;

    return-object p0
.end method

.method public year(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;
    .locals 0

    .line 656
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->year:Ljava/lang/String;

    return-object p0
.end method
