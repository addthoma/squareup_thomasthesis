.class final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ResidualTip"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 976
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 995
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;-><init>()V

    .line 996
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 997
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1002
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1000
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/TipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/TipLineItem;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->residual_tip(Lcom/squareup/protos/client/bills/TipLineItem;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;

    goto :goto_0

    .line 999
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->source_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;

    goto :goto_0

    .line 1006
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1007
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 974
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 988
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 989
    sget-object v0, Lcom/squareup/protos/client/bills/TipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 990
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 974
    check-cast p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)I
    .locals 4

    .line 981
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/TipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    const/4 v3, 0x2

    .line 982
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 983
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 974
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;->encodedSize(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;
    .locals 2

    .line 1012
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;

    move-result-object p1

    .line 1013
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/TipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/TipLineItem;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    .line 1014
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1015
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 974
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip$ProtoAdapter_ResidualTip;->redact(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    move-result-object p1

    return-object p1
.end method
