.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invoice_id_pair:Lcom/squareup/protos/client/IdPair;

.field public is_final_payment:Ljava/lang/Boolean;

.field public tipping_enabled:Ljava/lang/Boolean;

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;
    .locals 7

    .line 2148
    new-instance v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->version:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->is_final_payment:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2103
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    move-result-object v0

    return-object v0
.end method

.method public invoice_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;
    .locals 0

    .line 2119
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public is_final_payment(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;
    .locals 0

    .line 2142
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->is_final_payment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;
    .locals 0

    .line 2124
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->tipping_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public version(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;
    .locals 0

    .line 2133
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->version:Ljava/lang/String;

    return-object p0
.end method
