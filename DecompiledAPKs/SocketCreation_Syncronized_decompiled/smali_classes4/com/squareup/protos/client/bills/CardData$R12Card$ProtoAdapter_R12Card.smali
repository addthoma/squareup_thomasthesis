.class final Lcom/squareup/protos/client/bills/CardData$R12Card$ProtoAdapter_R12Card;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$R12Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_R12Card"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardData$R12Card;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1871
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardData$R12Card;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$R12Card;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1890
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;-><init>()V

    .line 1891
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1892
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1897
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1895
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->encrypted_felica_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    goto :goto_0

    .line 1894
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    goto :goto_0

    .line 1901
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1902
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1869
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$ProtoAdapter_R12Card;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$R12Card;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1883
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$R12Card;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1884
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$R12Card;->encrypted_felica_data:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1885
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$R12Card;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1869
    check-cast p2, Lcom/squareup/protos/client/bills/CardData$R12Card;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardData$R12Card$ProtoAdapter_R12Card;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$R12Card;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardData$R12Card;)I
    .locals 4

    .line 1876
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$R12Card;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$R12Card;->encrypted_felica_data:Lokio/ByteString;

    const/4 v3, 0x2

    .line 1877
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1878
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$R12Card;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1869
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$R12Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$ProtoAdapter_R12Card;->encodedSize(Lcom/squareup/protos/client/bills/CardData$R12Card;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardData$R12Card;)Lcom/squareup/protos/client/bills/CardData$R12Card;
    .locals 1

    .line 1907
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$R12Card;->newBuilder()Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 1908
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    .line 1909
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->encrypted_felica_data:Lokio/ByteString;

    .line 1910
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1911
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1869
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$R12Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$R12Card$ProtoAdapter_R12Card;->redact(Lcom/squareup/protos/client/bills/CardData$R12Card;)Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-result-object p1

    return-object p1
.end method
