.class public final Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;
.super Lcom/squareup/wire/Message;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;,
        Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final DEFAULT_COGS_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_TAX_LABEL_ORDER:Ljava/lang/Integer;

.field public static final DEFAULT_TAX_TYPE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TAX_TYPE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TAX_TYPE_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final calculation_phase:Lcom/squareup/api/items/CalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.CalculationPhase#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final cogs_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Fee$InclusionType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final tax_label_order:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x9
    .end annotation
.end field

.field public final tax_type_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final tax_type_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final tax_type_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 464
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$ProtoAdapter_DisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 472
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 474
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

    const/4 v0, 0x0

    .line 484
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->DEFAULT_TAX_LABEL_ORDER:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 11

    .line 559
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 566
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 567
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    .line 568
    iput-object p2, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    .line 569
    iput-object p3, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 570
    iput-object p4, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 571
    iput-object p5, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    .line 572
    iput-object p6, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    .line 573
    iput-object p7, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    .line 574
    iput-object p8, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 575
    iput-object p9, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 597
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 598
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    .line 599
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    .line 600
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    .line 601
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 602
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 603
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    .line 604
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    .line 605
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    .line 606
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    .line 607
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    .line 608
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 613
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 615
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 616
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 617
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 618
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/items/CalculationPhase;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 619
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee$InclusionType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 620
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 621
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 622
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 623
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 624
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 625
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 2

    .line 580
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;-><init>()V

    .line 581
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    .line 582
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->percentage:Ljava/lang/String;

    .line 583
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 584
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 585
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_name:Ljava/lang/String;

    .line 586
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_number:Ljava/lang/String;

    .line 587
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_id:Ljava/lang/String;

    .line 588
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    .line 589
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_label_order:Ljava/lang/Integer;

    .line 590
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 463
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 633
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_2

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 636
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v1, :cond_3

    const-string v1, ", inclusion_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 637
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", tax_type_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 638
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", tax_type_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", tax_type_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 640
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", cogs_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 641
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", tax_label_order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_label_order:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayDetails{"

    .line 642
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
