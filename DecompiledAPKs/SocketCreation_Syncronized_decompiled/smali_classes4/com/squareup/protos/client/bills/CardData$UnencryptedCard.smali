.class public final Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;
.super Lcom/squareup/wire/Message;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UnencryptedCard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$ProtoAdapter_UnencryptedCard;,
        Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;",
        "Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TRACK2_DATA:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final track2_data:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 771
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$ProtoAdapter_UnencryptedCard;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$ProtoAdapter_UnencryptedCard;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 788
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 792
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 793
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->track2_data:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 807
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 808
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    .line 809
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->track2_data:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->track2_data:Ljava/lang/String;

    .line 810
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 815
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 817
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 818
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->track2_data:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 819
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;
    .locals 2

    .line 798
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;-><init>()V

    .line 799
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->track2_data:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;->track2_data:Ljava/lang/String;

    .line 800
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 770
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->newBuilder()Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 826
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 827
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->track2_data:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", track2_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UnencryptedCard{"

    .line 828
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
