.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

.field public instrument_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2403
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;
    .locals 4

    .line 2418
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;->instrument_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2398
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    move-result-object v0

    return-object v0
.end method

.method public display_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;
    .locals 0

    .line 2412
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    return-object p0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;
    .locals 0

    .line 2407
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method
