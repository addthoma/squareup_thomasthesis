.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public event_id_pair:Lcom/squareup/protos/client/IdPair;

.field public event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3802
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;
    .locals 7

    .line 3839
    new-instance v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3793
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;
    .locals 0

    .line 3825
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;
    .locals 0

    .line 3833
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public event_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;
    .locals 0

    .line 3809
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public event_type(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;
    .locals 0

    .line 3817
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    return-object p0
.end method
