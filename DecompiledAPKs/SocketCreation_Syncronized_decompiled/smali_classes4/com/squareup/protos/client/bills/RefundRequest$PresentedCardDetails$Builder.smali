.class public final Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RefundRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;",
        "Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_data:Lcom/squareup/protos/client/bills/CardData;

.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public is_refund_destination:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 257
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;
    .locals 5

    .line 287
    new-instance v0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->is_refund_destination:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;-><init>(Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 250
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->build()Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    move-result-object v0

    return-object v0
.end method

.method public card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    return-object p0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public is_refund_destination(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->is_refund_destination:Ljava/lang/Boolean;

    return-object p0
.end method
