.class public final enum Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
.super Ljava/lang/Enum;
.source "CardTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Brand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Card$Brand$ProtoAdapter_Brand;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum EBT:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final enum VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 473
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v1, 0x1

    const-string v2, "UNKNOWN_BRAND"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 475
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v2, 0x2

    const-string v3, "VISA"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 477
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v3, 0x3

    const-string v4, "MASTERCARD"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 479
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x4

    const-string v5, "AMERICAN_EXPRESS"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 481
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v5, 0x5

    const-string v6, "DISCOVER"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 483
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v6, 0x6

    const-string v7, "DISCOVER_DINERS"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 485
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v7, 0x7

    const-string v8, "JCB"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 487
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v8, 0x8

    const-string v9, "CHINA_UNIONPAY"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 492
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v9, 0x9

    const-string v10, "SQUARE_GIFT_CARD_V2"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 494
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v10, 0xa

    const-string v11, "INTERAC"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 496
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v11, 0xb

    const-string v12, "SQUARE_CAPITAL_CARD"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 498
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v12, 0xc

    const-string v13, "EFTPOS"

    invoke-direct {v0, v13, v11, v12}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 500
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v13, 0xd

    const-string v14, "FELICA"

    invoke-direct {v0, v14, v12, v13}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 502
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v14, 0xe

    const-string v15, "ALIPAY"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 504
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v15, 0xf

    const-string v13, "CASH_APP"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 506
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v13, "EBT"

    const/16 v14, 0x10

    invoke-direct {v0, v13, v15, v14}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EBT:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 472
    sget-object v13, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v14, 0x0

    aput-object v13, v0, v14

    sget-object v13, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EBT:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    aput-object v1, v0, v15

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 508
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand$ProtoAdapter_Brand;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand$ProtoAdapter_Brand;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 512
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 513
    iput p3, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 536
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EBT:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 535
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 534
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 533
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 532
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 531
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 530
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 529
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 528
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 527
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 526
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 525
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 524
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 523
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 522
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    .line 521
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .locals 1

    .line 472
    const-class v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .locals 1

    .line 472
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 543
    iget v0, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->value:I

    return v0
.end method
