.class final Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReceiptDisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1851
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1882
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;-><init>()V

    .line 1883
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1884
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1902
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1895
    :pswitch_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/common/payment/SignatureType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/payment/SignatureType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_signature(Lcom/squareup/protos/common/payment/SignatureType;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1897
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1892
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_selected_locale(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    goto :goto_0

    .line 1891
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    goto :goto_0

    .line 1890
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_custom_amount(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    goto :goto_0

    .line 1889
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_quick_tip_options(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    goto :goto_0

    .line 1888
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/tipping/TippingPreferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/tipping/TippingPreferences;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences(Lcom/squareup/protos/common/tipping/TippingPreferences;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    goto :goto_0

    .line 1887
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_tip_options_on_paper_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    goto :goto_0

    .line 1886
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_signature_line_on_paper_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    goto :goto_0

    .line 1906
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1907
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1849
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1869
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1870
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1871
    sget-object v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1872
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1873
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1874
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1875
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1876
    sget-object v0, Lcom/squareup/protos/common/payment/SignatureType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1877
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1849
    check-cast p2, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)I
    .locals 4

    .line 1856
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 1857
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/tipping/TippingPreferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    const/4 v3, 0x3

    .line 1858
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 1859
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 1860
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 1861
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    const/4 v3, 0x7

    .line 1862
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/payment/SignatureType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    const/16 v3, 0x8

    .line 1863
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1864
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1849
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;->encodedSize(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
    .locals 2

    .line 1912
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object p1

    .line 1913
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/tipping/TippingPreferences;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    .line 1914
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    .line 1915
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1916
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1849
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;->redact(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object p1

    return-object p1
.end method
