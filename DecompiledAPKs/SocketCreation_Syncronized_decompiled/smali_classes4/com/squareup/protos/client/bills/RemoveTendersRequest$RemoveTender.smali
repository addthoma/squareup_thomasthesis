.class public final Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;
.super Lcom/squareup/wire/Message;
.source "RemoveTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RemoveTendersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RemoveTender"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;,
        Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final tender_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final tender_type:Lcom/squareup/protos/client/bills/Tender$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$Type#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 150
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 154
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    sput-object v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;)V
    .locals 1

    .line 173
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lokio/ByteString;)V
    .locals 1

    .line 177
    sget-object v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 179
    iput-object p2, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 194
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 195
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    .line 196
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 198
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 203
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 205
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 208
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;
    .locals 2

    .line 184
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;-><init>()V

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 187
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 149
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->newBuilder()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", tender_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_1

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RemoveTender{"

    .line 218
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
