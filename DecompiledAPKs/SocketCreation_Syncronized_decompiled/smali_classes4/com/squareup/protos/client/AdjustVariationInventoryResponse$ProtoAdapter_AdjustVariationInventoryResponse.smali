.class final Lcom/squareup/protos/client/AdjustVariationInventoryResponse$ProtoAdapter_AdjustVariationInventoryResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AdjustVariationInventoryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/AdjustVariationInventoryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AdjustVariationInventoryResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/AdjustVariationInventoryResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 68
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/AdjustVariationInventoryResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 84
    new-instance v0, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$Builder;-><init>()V

    .line 85
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 86
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 89
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 93
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 94
    invoke-virtual {v0}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$Builder;->build()Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 66
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$ProtoAdapter_AdjustVariationInventoryResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/AdjustVariationInventoryResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 79
    invoke-virtual {p2}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 66
    check-cast p2, Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$ProtoAdapter_AdjustVariationInventoryResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/AdjustVariationInventoryResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/AdjustVariationInventoryResponse;)I
    .locals 0

    .line 73
    invoke-virtual {p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$ProtoAdapter_AdjustVariationInventoryResponse;->encodedSize(Lcom/squareup/protos/client/AdjustVariationInventoryResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/AdjustVariationInventoryResponse;)Lcom/squareup/protos/client/AdjustVariationInventoryResponse;
    .locals 0

    .line 99
    invoke-virtual {p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse;->newBuilder()Lcom/squareup/protos/client/AdjustVariationInventoryResponse$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 101
    invoke-virtual {p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$Builder;->build()Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/AdjustVariationInventoryResponse$ProtoAdapter_AdjustVariationInventoryResponse;->redact(Lcom/squareup/protos/client/AdjustVariationInventoryResponse;)Lcom/squareup/protos/client/AdjustVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method
