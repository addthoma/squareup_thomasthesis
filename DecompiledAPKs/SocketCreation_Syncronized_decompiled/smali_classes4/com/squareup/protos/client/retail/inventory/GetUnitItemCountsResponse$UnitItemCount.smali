.class public final Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;
.super Lcom/squareup/wire/Message;
.source "GetUnitItemCountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UnitItemCount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$ProtoAdapter_UnitItemCount;,
        Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;",
        "Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ITEM_COUNT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final item_count:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.retail.inventory.UnitProfile#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 102
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$ProtoAdapter_UnitItemCount;

    invoke-direct {v0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$ProtoAdapter_UnitItemCount;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/retail/inventory/UnitProfile;)V
    .locals 1

    .line 121
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/retail/inventory/UnitProfile;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/retail/inventory/UnitProfile;Lokio/ByteString;)V
    .locals 1

    .line 125
    sget-object v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->item_count:Ljava/lang/String;

    .line 127
    iput-object p2, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 142
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 143
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->item_count:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->item_count:Ljava/lang/String;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    .line 146
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 151
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 153
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->item_count:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/retail/inventory/UnitProfile;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 156
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;
    .locals 2

    .line 132
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;-><init>()V

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->item_count:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->item_count:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    iput-object v1, v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->newBuilder()Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->item_count:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", item_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->item_count:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    if-eqz v1, :cond_1

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->unit:Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UnitItemCount{"

    .line 166
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
