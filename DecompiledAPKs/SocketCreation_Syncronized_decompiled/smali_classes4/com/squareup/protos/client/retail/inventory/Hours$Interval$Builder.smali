.class public final Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Hours.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/Hours$Interval;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/retail/inventory/Hours$Interval;",
        "Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public duration_minutes:Ljava/lang/Integer;

.field public start_time:Lcom/squareup/protos/common/time/LocalTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 199
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/retail/inventory/Hours$Interval;
    .locals 4

    .line 214
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v2, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->duration_minutes:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;-><init>(Lcom/squareup/protos/common/time/LocalTime;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 194
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->build()Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    move-result-object v0

    return-object v0
.end method

.method public duration_minutes(Ljava/lang/Integer;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->duration_minutes:Ljava/lang/Integer;

    return-object p0
.end method

.method public start_time(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    return-object p0
.end method
