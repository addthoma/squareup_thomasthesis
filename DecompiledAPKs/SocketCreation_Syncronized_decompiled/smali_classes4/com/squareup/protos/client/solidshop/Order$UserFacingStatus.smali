.class public final enum Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;
.super Ljava/lang/Enum;
.source "Order.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserFacingStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus$ProtoAdapter_UserFacingStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AWAITING_RETURN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum CANCELED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum DECLINED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum DECLINED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum DECLINED_REFUND_PENDING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum OUT_FOR_DELIVERY:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum PARTIALLY_DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum PARTIALLY_DELIVERED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum PROCESSING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum RETURNED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum RETURNED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum RETURNED_TO_SENDER:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum SHIPPED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum SHIPPED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum SHIPPING_ERROR:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 233
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v1, 0x1

    const-string v2, "CANCELED"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->CANCELED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 235
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v2, 0x2

    const-string v3, "PROCESSING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PROCESSING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 237
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v3, 0x3

    const-string v4, "UNKNOWN"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->UNKNOWN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 242
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v4, 0x4

    const-string v5, "SHIPPED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 244
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v5, 0x5

    const-string v6, "OUT_FOR_DELIVERY"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->OUT_FOR_DELIVERY:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 246
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v6, 0x6

    const-string v7, "PARTIALLY_DELIVERED"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PARTIALLY_DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 248
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v7, 0x7

    const-string v8, "DELIVERED"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 253
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v8, 0x8

    const-string v9, "RETURNED_TO_SENDER"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED_TO_SENDER:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 255
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v9, 0x9

    const-string v10, "SHIPPING_ERROR"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPING_ERROR:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 257
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v10, 0xa

    const-string v11, "SHIPPED_WITH_ERRORS"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 259
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v11, 0xb

    const-string v12, "PARTIALLY_DELIVERED_WITH_ERRORS"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PARTIALLY_DELIVERED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 264
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v12, 0xc

    const-string v13, "AWAITING_RETURN"

    invoke-direct {v0, v13, v11, v12}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->AWAITING_RETURN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 266
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v13, 0xd

    const-string v14, "RETURNED"

    invoke-direct {v0, v14, v12, v13}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 268
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v14, 0xe

    const-string v15, "RETURNED_AND_REFUNDED"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 273
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v15, 0xf

    const-string v13, "DECLINED"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 275
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const-string v13, "DECLINED_REFUND_PENDING"

    const/16 v14, 0x10

    invoke-direct {v0, v13, v15, v14}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED_REFUND_PENDING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 277
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const-string v13, "DECLINED_AND_REFUNDED"

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 232
    sget-object v13, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->CANCELED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/4 v14, 0x0

    aput-object v13, v0, v14

    sget-object v13, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PROCESSING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->UNKNOWN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->OUT_FOR_DELIVERY:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PARTIALLY_DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED_TO_SENDER:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPING_ERROR:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PARTIALLY_DELIVERED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->AWAITING_RETURN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED_REFUND_PENDING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->$VALUES:[Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 279
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus$ProtoAdapter_UserFacingStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus$ProtoAdapter_UserFacingStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 283
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 284
    iput p3, p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 308
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 307
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED_REFUND_PENDING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 306
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DECLINED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 305
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED_AND_REFUNDED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 304
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 303
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->AWAITING_RETURN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 302
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PARTIALLY_DELIVERED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 301
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPED_WITH_ERRORS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 300
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPING_ERROR:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 299
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->RETURNED_TO_SENDER:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 298
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 297
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PARTIALLY_DELIVERED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 296
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->OUT_FOR_DELIVERY:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 295
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->SHIPPED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 294
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->UNKNOWN:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 293
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->PROCESSING:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    .line 292
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->CANCELED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;
    .locals 1

    .line 232
    const-class v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;
    .locals 1

    .line 232
    sget-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->$VALUES:[Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 315
    iget v0, p0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->value:I

    return v0
.end method
