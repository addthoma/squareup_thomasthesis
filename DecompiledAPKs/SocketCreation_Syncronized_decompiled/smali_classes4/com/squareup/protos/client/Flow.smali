.class public final enum Lcom/squareup/protos/client/Flow;
.super Ljava/lang/Enum;
.source "Flow.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/Flow$ProtoAdapter_Flow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/Flow;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/Flow;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/Flow;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum NONE:Lcom/squareup/protos/client/Flow;

.field public static final enum OPEN_TICKETS_SYNC:Lcom/squareup/protos/client/Flow;

.field public static final enum PAYMENT_CREATION:Lcom/squareup/protos/client/Flow;

.field public static final enum PAYMENT_HISTORY:Lcom/squareup/protos/client/Flow;

.field public static final enum REOPEN:Lcom/squareup/protos/client/Flow;

.field public static final enum UNKNOWN_FLOW:Lcom/squareup/protos/client/Flow;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 14
    new-instance v0, Lcom/squareup/protos/client/Flow;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_FLOW"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Flow;->UNKNOWN_FLOW:Lcom/squareup/protos/client/Flow;

    .line 16
    new-instance v0, Lcom/squareup/protos/client/Flow;

    const/4 v2, 0x1

    const-string v3, "NONE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Flow;->NONE:Lcom/squareup/protos/client/Flow;

    .line 18
    new-instance v0, Lcom/squareup/protos/client/Flow;

    const/4 v3, 0x2

    const-string v4, "OPEN_TICKETS_SYNC"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Flow;->OPEN_TICKETS_SYNC:Lcom/squareup/protos/client/Flow;

    .line 20
    new-instance v0, Lcom/squareup/protos/client/Flow;

    const/4 v4, 0x3

    const-string v5, "PAYMENT_CREATION"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Flow;->PAYMENT_CREATION:Lcom/squareup/protos/client/Flow;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/Flow;

    const/4 v5, 0x4

    const-string v6, "PAYMENT_HISTORY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Flow;->PAYMENT_HISTORY:Lcom/squareup/protos/client/Flow;

    .line 28
    new-instance v0, Lcom/squareup/protos/client/Flow;

    const/4 v6, 0x5

    const-string v7, "REOPEN"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/Flow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Flow;->REOPEN:Lcom/squareup/protos/client/Flow;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/Flow;

    .line 10
    sget-object v7, Lcom/squareup/protos/client/Flow;->UNKNOWN_FLOW:Lcom/squareup/protos/client/Flow;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/Flow;->NONE:Lcom/squareup/protos/client/Flow;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/Flow;->OPEN_TICKETS_SYNC:Lcom/squareup/protos/client/Flow;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/Flow;->PAYMENT_CREATION:Lcom/squareup/protos/client/Flow;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/Flow;->PAYMENT_HISTORY:Lcom/squareup/protos/client/Flow;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/Flow;->REOPEN:Lcom/squareup/protos/client/Flow;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/Flow;->$VALUES:[Lcom/squareup/protos/client/Flow;

    .line 30
    new-instance v0, Lcom/squareup/protos/client/Flow$ProtoAdapter_Flow;

    invoke-direct {v0}, Lcom/squareup/protos/client/Flow$ProtoAdapter_Flow;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/Flow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput p3, p0, Lcom/squareup/protos/client/Flow;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/Flow;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 48
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/Flow;->REOPEN:Lcom/squareup/protos/client/Flow;

    return-object p0

    .line 47
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/Flow;->PAYMENT_HISTORY:Lcom/squareup/protos/client/Flow;

    return-object p0

    .line 46
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/Flow;->PAYMENT_CREATION:Lcom/squareup/protos/client/Flow;

    return-object p0

    .line 45
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/Flow;->OPEN_TICKETS_SYNC:Lcom/squareup/protos/client/Flow;

    return-object p0

    .line 44
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/Flow;->NONE:Lcom/squareup/protos/client/Flow;

    return-object p0

    .line 43
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/Flow;->UNKNOWN_FLOW:Lcom/squareup/protos/client/Flow;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/Flow;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/Flow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/Flow;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/Flow;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/Flow;->$VALUES:[Lcom/squareup/protos/client/Flow;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/Flow;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/Flow;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/squareup/protos/client/Flow;->value:I

    return v0
.end method
