.class public final Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;
.super Lcom/squareup/wire/Message;
.source "EstimateDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;,
        Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;,
        Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;",
        "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_STATE:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final DEFAULT_IS_ARCHIVED:Ljava/lang/Boolean;

.field public static final DEFAULT_LINKED_INVOICE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SELECTED_PACKAGE_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final accepted_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final canceled_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final delivered_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.estimate.EstimateDisplayDetails$DisplayState#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final estimate:Lcom/squareup/protos/client/estimate/Estimate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.estimate.Estimate#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTimeline#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final event:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final is_archived:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final linked_invoice_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final selected_package_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final sort_date:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final updated_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$ProtoAdapter_EstimateDisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->DEFAULT_DISPLAY_STATE:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v0, 0x0

    .line 39
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->DEFAULT_IS_ARCHIVED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/estimate/Estimate;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/InvoiceTimeline;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/estimate/Estimate;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 166
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;-><init>(Lcom/squareup/protos/client/estimate/Estimate;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/InvoiceTimeline;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/estimate/Estimate;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/InvoiceTimeline;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/estimate/Estimate;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 174
    sget-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    .line 176
    iput-object p2, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 177
    iput-object p3, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 178
    iput-object p4, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 179
    iput-object p5, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 180
    iput-object p6, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 181
    iput-object p7, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 182
    iput-object p8, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    .line 183
    iput-object p9, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 184
    iput-object p10, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    const-string p1, "event"

    .line 185
    invoke-static {p1, p11}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    .line 186
    iput-object p12, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    .line 187
    iput-object p13, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 213
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 214
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    .line 226
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    .line 228
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 233
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 235
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/estimate/Estimate;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    .line 249
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 2

    .line 192
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;-><init>()V

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->linked_invoice_token:Ljava/lang/String;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->event:Ljava/util/List;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->is_archived:Ljava/lang/Boolean;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->selected_package_token:Ljava/lang/String;

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->newBuilder()Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    if-eqz v1, :cond_0

    const-string v1, ", estimate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 258
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", sort_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 259
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 260
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 261
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", delivered_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 262
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    const-string v1, ", accepted_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 263
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    if-eqz v1, :cond_6

    const-string v1, ", display_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 264
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", linked_invoice_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->linked_invoice_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_8

    const-string v1, ", canceled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 266
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    if-eqz v1, :cond_9

    const-string v1, ", estimate_timeline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 267
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->event:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 268
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", is_archived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->is_archived:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", selected_package_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->selected_package_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EstimateDisplayDetails{"

    .line 270
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
