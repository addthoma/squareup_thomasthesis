.class final Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EstimatePackage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/EstimatePackage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EstimatePackage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/estimate/EstimatePackage;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 208
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/estimate/EstimatePackage;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/EstimatePackage;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 233
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;-><init>()V

    .line 234
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 235
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 243
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 241
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    goto :goto_0

    .line 240
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->attachment:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 239
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    goto :goto_0

    .line 238
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    goto :goto_0

    .line 237
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    goto :goto_0

    .line 247
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 248
    invoke-virtual {v0}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->build()Lcom/squareup/protos/client/estimate/EstimatePackage;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 206
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/EstimatePackage;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/EstimatePackage;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 223
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 227
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 228
    invoke-virtual {p2}, Lcom/squareup/protos/client/estimate/EstimatePackage;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 206
    check-cast p2, Lcom/squareup/protos/client/estimate/EstimatePackage;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/EstimatePackage;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/estimate/EstimatePackage;)I
    .locals 4

    .line 213
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    const/4 v3, 0x2

    .line 214
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    const/4 v3, 0x3

    .line 215
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 216
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x5

    .line 217
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimatePackage;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 206
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimatePackage;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;->encodedSize(Lcom/squareup/protos/client/estimate/EstimatePackage;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/estimate/EstimatePackage;)Lcom/squareup/protos/client/estimate/EstimatePackage;
    .locals 2

    .line 253
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimatePackage;->newBuilder()Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 254
    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->title:Ljava/lang/String;

    .line 255
    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->description:Ljava/lang/String;

    .line 256
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->attachment:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 257
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 258
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 259
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->build()Lcom/squareup/protos/client/estimate/EstimatePackage;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 206
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimatePackage;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;->redact(Lcom/squareup/protos/client/estimate/EstimatePackage;)Lcom/squareup/protos/client/estimate/EstimatePackage;

    move-result-object p1

    return-object p1
.end method
