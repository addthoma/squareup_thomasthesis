.class public final Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EstimatePackage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/EstimatePackage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/EstimatePackage;",
        "Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public description:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 158
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 159
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->attachment:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attachment(Ljava/util/List;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)",
            "Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;"
        }
    .end annotation

    .line 187
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->attachment:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/estimate/EstimatePackage;
    .locals 8

    .line 202
    new-instance v7, Lcom/squareup/protos/client/estimate/EstimatePackage;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->attachment:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/estimate/EstimatePackage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->build()Lcom/squareup/protos/client/estimate/EstimatePackage;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
