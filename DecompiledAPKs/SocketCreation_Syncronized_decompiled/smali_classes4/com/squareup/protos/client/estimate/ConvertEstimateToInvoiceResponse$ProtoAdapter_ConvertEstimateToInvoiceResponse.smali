.class final Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$ProtoAdapter_ConvertEstimateToInvoiceResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ConvertEstimateToInvoiceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ConvertEstimateToInvoiceResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 114
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 134
    new-instance v0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;-><init>()V

    .line 135
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 136
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 141
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 139
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->estimate_display_details(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;

    goto :goto_0

    .line 138
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;

    goto :goto_0

    .line 145
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 146
    invoke-virtual {v0}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->build()Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$ProtoAdapter_ConvertEstimateToInvoiceResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 127
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 128
    sget-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;->estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 129
    invoke-virtual {p2}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    check-cast p2, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$ProtoAdapter_ConvertEstimateToInvoiceResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;)I
    .locals 4

    .line 119
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;->estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    const/4 v3, 0x2

    .line 120
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$ProtoAdapter_ConvertEstimateToInvoiceResponse;->encodedSize(Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;
    .locals 2

    .line 151
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;->newBuilder()Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;

    move-result-object p1

    .line 152
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 153
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->estimate_display_details:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    .line 154
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 155
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$Builder;->build()Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse$ProtoAdapter_ConvertEstimateToInvoiceResponse;->redact(Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceResponse;

    move-result-object p1

    return-object p1
.end method
