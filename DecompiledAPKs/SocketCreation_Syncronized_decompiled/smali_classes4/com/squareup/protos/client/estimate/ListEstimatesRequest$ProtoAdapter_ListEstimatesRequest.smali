.class final Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListEstimatesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/ListEstimatesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListEstimatesRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/estimate/ListEstimatesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 394
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 425
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;-><init>()V

    .line 426
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 427
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 445
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 443
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->include_archived(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    goto :goto_0

    .line 442
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->check_archive_for_matches(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    goto :goto_0

    .line 441
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    goto :goto_0

    .line 435
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->state_filter(Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 437
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 432
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    goto :goto_0

    .line 431
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->query(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    goto :goto_0

    .line 430
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    goto :goto_0

    .line 429
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    goto :goto_0

    .line 449
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 450
    invoke-virtual {v0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->build()Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 392
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/ListEstimatesRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 412
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 413
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 414
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 415
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 416
    sget-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 417
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 418
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 419
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 420
    invoke-virtual {p2}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 392
    check-cast p2, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/ListEstimatesRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/estimate/ListEstimatesRequest;)I
    .locals 4

    .line 399
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 400
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    const/4 v3, 0x3

    .line 401
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v3, 0x4

    .line 402
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v3, 0x5

    .line 403
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    const/4 v3, 0x6

    .line 404
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 405
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 406
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 407
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 392
    check-cast p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;->encodedSize(Lcom/squareup/protos/client/estimate/ListEstimatesRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/estimate/ListEstimatesRequest;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest;
    .locals 2

    .line 455
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->newBuilder()Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    move-result-object p1

    .line 456
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 457
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 458
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->build()Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 392
    check-cast p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;->redact(Lcom/squareup/protos/client/estimate/ListEstimatesRequest;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    move-result-object p1

    return-object p1
.end method
