.class public final Lcom/squareup/protos/client/estimate/EstimatePackage;
.super Lcom/squareup/wire/Message;
.source "EstimatePackage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;,
        Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/EstimatePackage;",
        "Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/EstimatePackage;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final attachment:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.FileAttachmentMetadata#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimatePackage$ProtoAdapter_EstimatePackage;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimatePackage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")V"
        }
    .end annotation

    .line 83
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/estimate/EstimatePackage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 88
    sget-object v0, Lcom/squareup/protos/client/estimate/EstimatePackage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 89
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    .line 90
    iput-object p2, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    .line 91
    iput-object p3, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    const-string p1, "attachment"

    .line 92
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    .line 93
    iput-object p5, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 111
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 112
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimatePackage;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimatePackage;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimatePackage;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    .line 117
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p1, p1, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 118
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 123
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimatePackage;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 131
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;
    .locals 2

    .line 98
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->token:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->title:Ljava/lang/String;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->description:Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->attachment:Ljava/util/List;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimatePackage;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimatePackage;->newBuilder()Lcom/squareup/protos/client/estimate/EstimatePackage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", title=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", description=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", attachment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->attachment:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_4

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimatePackage;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EstimatePackage{"

    .line 144
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
