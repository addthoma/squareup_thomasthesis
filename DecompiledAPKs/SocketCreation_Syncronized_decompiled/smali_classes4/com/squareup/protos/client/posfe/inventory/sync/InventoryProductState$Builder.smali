.class public final Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryProductState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

.field public product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

.field public tracking_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryTrackingState;

.field public updated_at:Ljava/lang/Long;

.field public valid_until:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;
    .locals 8

    .line 206
    new-instance v7, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    iget-object v3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->tracking_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryTrackingState;

    iget-object v4, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->valid_until:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->updated_at:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryTrackingState;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    move-result-object v0

    return-object v0
.end method

.method public inventory_type(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    return-object p0
.end method

.method public product_identifier(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    return-object p0
.end method

.method public tracking_state(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryTrackingState;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->tracking_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryTrackingState;

    return-object p0
.end method

.method public updated_at(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->updated_at:Ljava/lang/Long;

    return-object p0
.end method

.method public valid_until(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState$Builder;->valid_until:Ljava/lang/Long;

    return-object p0
.end method
