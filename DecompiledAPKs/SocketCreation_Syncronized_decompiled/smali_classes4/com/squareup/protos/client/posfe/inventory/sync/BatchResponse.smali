.class public final Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;
.super Lcom/squareup/wire/Message;
.source "BatchResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$ProtoAdapter_BatchResponse;,
        Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

.field private static final serialVersionUID:J


# instance fields
.field public final error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.SyncError#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final response:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.Response#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$ProtoAdapter_BatchResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$ProtoAdapter_BatchResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;->DO_NOT_USE_SYNC_ERROR:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->DEFAULT_ERROR:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
            ">;)V"
        }
    .end annotation

    .line 44
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 48
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    const-string p1, "response"

    .line 50
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->response:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 65
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 66
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    .line 68
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->response:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->response:Ljava/util/List;

    .line 69
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 74
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->response:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->response:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->response:Ljava/util/List;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    if-eqz v1, :cond_0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->response:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;->response:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BatchResponse{"

    .line 89
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
