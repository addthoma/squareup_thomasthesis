.class public final Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

.field public deleted:Ljava/lang/Boolean;

.field public product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;
    .locals 5

    .line 139
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->deleted:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    iget-object v3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    move-result-object v0

    return-object v0
.end method

.method public count(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    const/4 p1, 0x0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    return-object p0
.end method

.method public deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->deleted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public product_state(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    const/4 p1, 0x0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    return-object p0
.end method
