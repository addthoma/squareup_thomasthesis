.class public final Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;
.super Lcom/squareup/wire/Message;
.source "InventoryCount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;,
        Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURRENT_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_INVENTORY_TYPE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

.field public static final DEFAULT_UPDATED_AT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final current_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.InventoryType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.InventoryProductIdentifier#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.InventoryState#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final updated_at:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$ProtoAdapter_InventoryCount;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;->DO_NOT_USE_STATE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->DEFAULT_STATE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    const-wide/16 v0, 0x0

    .line 27
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->DEFAULT_CURRENT_COUNT:Ljava/lang/Long;

    .line 29
    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;->DO_NOT_USE_INVENTORY_TYPE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    sput-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->DEFAULT_INVENTORY_TYPE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    .line 31
    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->DEFAULT_UPDATED_AT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;Ljava/lang/Long;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;Ljava/lang/Long;)V
    .locals 7

    .line 81
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;Ljava/lang/Long;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;Ljava/lang/Long;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    .line 89
    iput-object p2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    .line 90
    iput-object p3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    .line 91
    iput-object p4, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    .line 92
    iput-object p5, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 110
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 111
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    .line 117
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 122
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 130
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->current_count:Ljava/lang/Long;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->updated_at:Ljava/lang/Long;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    if-eqz v1, :cond_0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", current_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->current_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    if-eqz v1, :cond_2

    const-string v1, ", product_identifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    if-eqz v1, :cond_3

    const-string v1, ", inventory_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->updated_at:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InventoryCount{"

    .line 143
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
