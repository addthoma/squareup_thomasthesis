.class public final Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Request;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

.field public put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

.field public scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/Request;
    .locals 5

    .line 129
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    iget-object v3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    move-result-object v0

    return-object v0
.end method

.method public get_request(Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    const/4 p1, 0x0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    return-object p0
.end method

.method public put_request(Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;)Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    const/4 p1, 0x0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    return-object p0
.end method

.method public scope(Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;)Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    return-object p0
.end method
