.class public final Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListDisputesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/ListDisputesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cbms/ListDisputesRequest;",
        "Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at_max:Lcom/squareup/protos/common/time/DateTime;

.field public created_at_min:Lcom/squareup/protos/common/time/DateTime;

.field public cursor:Ljava/lang/Integer;

.field public limit:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cbms/ListDisputesRequest;
    .locals 7

    .line 167
    new-instance v6, Lcom/squareup/protos/client/cbms/ListDisputesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->created_at_min:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->created_at_max:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->cursor:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->limit:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/cbms/ListDisputesRequest;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->build()Lcom/squareup/protos/client/cbms/ListDisputesRequest;

    move-result-object v0

    return-object v0
.end method

.method public created_at_max(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->created_at_max:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public created_at_min(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->created_at_min:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public cursor(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->cursor:Ljava/lang/Integer;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/ListDisputesRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method
