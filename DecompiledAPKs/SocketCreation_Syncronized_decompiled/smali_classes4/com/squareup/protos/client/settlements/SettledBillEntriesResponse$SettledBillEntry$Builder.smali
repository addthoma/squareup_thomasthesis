.class public final Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettledBillEntriesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public collected_money:Lcom/squareup/protos/common/Money;

.field public request_created_at:Lcom/squareup/protos/common/time/DateTime;

.field public settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

.field public settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

.field public to_user_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 256
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;
    .locals 9

    .line 312
    new-instance v8, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    iget-object v4, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->collected_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->to_user_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;-><init>(Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 243
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->build()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    .locals 0

    .line 306
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public collected_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    .locals 0

    .line 287
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->collected_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public request_created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public settled_bill_entry_token_group(Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    return-object p0
.end method

.method public settled_bill_entry_type(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    return-object p0
.end method

.method public to_user_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$Builder;->to_user_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
