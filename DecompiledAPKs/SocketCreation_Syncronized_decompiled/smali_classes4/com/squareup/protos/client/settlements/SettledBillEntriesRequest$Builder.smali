.class public final Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettledBillEntriesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public settled_bill_entry_token_group:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;"
        }
    .end annotation
.end field

.field public tz_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 105
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->settled_bill_entry_token_group:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;
    .locals 4

    .line 130
    new-instance v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->settled_bill_entry_token_group:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->tz_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;-><init>(Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest;

    move-result-object v0

    return-object v0
.end method

.method public settled_bill_entry_token_group(Ljava/util/List;)Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;)",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;"
        }
    .end annotation

    .line 113
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->settled_bill_entry_token_group:Ljava/util/List;

    return-object p0
.end method

.method public tz_name(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntriesRequest$Builder;->tz_name:Ljava/lang/String;

    return-object p0
.end method
