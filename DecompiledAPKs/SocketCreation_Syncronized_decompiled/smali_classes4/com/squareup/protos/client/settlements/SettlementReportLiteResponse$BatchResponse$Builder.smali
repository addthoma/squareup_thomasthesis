.class public final Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportLiteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public next_batch_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 178
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;
    .locals 3

    .line 188
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->next_batch_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    move-result-object v0

    return-object v0
.end method

.method public next_batch_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->next_batch_token:Ljava/lang/String;

    return-object p0
.end method
