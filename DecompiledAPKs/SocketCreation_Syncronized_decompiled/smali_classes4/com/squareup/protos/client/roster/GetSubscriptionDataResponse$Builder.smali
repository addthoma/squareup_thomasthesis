.class public final Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetSubscriptionDataResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse;",
        "Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_billing_data:Lcom/squareup/protos/roster/mds/MerchantBillingData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse;
    .locals 3

    .line 89
    new-instance v0, Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse$Builder;->merchant_billing_data:Lcom/squareup/protos/roster/mds/MerchantBillingData;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse;-><init>(Lcom/squareup/protos/roster/mds/MerchantBillingData;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse$Builder;->build()Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse;

    move-result-object v0

    return-object v0
.end method

.method public merchant_billing_data(Lcom/squareup/protos/roster/mds/MerchantBillingData;)Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse$Builder;
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/protos/client/roster/GetSubscriptionDataResponse$Builder;->merchant_billing_data:Lcom/squareup/protos/roster/mds/MerchantBillingData;

    return-object p0
.end method
