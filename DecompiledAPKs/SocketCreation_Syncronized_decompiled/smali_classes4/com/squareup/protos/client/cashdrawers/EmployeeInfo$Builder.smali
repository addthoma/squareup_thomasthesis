.class public final Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmployeeInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;",
        "Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_first_name:Ljava/lang/String;

.field public employee_id:Ljava/lang/String;

.field public employee_last_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;
    .locals 5

    .line 133
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;->employee_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;->employee_first_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;->employee_last_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    move-result-object v0

    return-object v0
.end method

.method public employee_first_name(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;->employee_first_name:Ljava/lang/String;

    return-object p0
.end method

.method public employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;->employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public employee_last_name(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo$Builder;->employee_last_name:Ljava/lang/String;

    return-object p0
.end method
