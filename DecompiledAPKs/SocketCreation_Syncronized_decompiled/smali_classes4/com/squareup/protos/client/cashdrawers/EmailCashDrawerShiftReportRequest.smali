.class public final Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
.super Lcom/squareup/wire/Message;
.source "EmailCashDrawerShiftReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;,
        Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLIENT_CASH_DRAWER_SHIFT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_UNIQUE_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final client_cash_drawer_shift_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final client_unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.CashDrawerShiftReportRenderingOptions#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;)V
    .locals 7

    .line 87
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;Lokio/ByteString;)V
    .locals 1

    .line 93
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 94
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    .line 95
    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    .line 96
    iput-object p3, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 97
    iput-object p4, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    .line 98
    iput-object p5, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 116
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 117
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    .line 123
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 128
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 136
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
    .locals 2

    .line 103
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_unique_key:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->merchant_id:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->email_address:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", client_unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", client_cash_drawer_shift_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", email_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    if-eqz v1, :cond_4

    const-string v1, ", rendering_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EmailCashDrawerShiftReportRequest{"

    .line 149
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
