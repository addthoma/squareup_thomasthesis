.class public final Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cash_drawer_shift_description:Ljava/lang/String;

.field public client_cash_drawer_shift_id:Ljava/lang/String;

.field public client_unique_key:Ljava/lang/String;

.field public device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field public expected_cash_money:Lcom/squareup/protos/common/Money;

.field public merchant_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 193
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 194
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
    .locals 10

    .line 253
    new-instance v9, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->cash_drawer_shift_description:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    move-result-object v0

    return-object v0
.end method

.method public cash_drawer_shift_description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->cash_drawer_shift_description:Ljava/lang/String;

    return-object p0
.end method

.method public client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    return-object p0
.end method

.method public device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public events(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;)",
            "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;"
        }
    .end annotation

    .line 238
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    return-object p0
.end method

.method public expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method
