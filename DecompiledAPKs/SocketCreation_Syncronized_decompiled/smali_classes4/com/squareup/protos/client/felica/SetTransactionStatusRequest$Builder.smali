.class public final Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetTransactionStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public connection_id:Ljava/lang/String;

.field public miryo_transaction_id:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/felica/Status;

.field public transaction_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 135
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;
    .locals 7

    .line 169
    new-instance v6, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->connection_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->transaction_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->status:Lcom/squareup/protos/client/felica/Status;

    iget-object v4, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->miryo_transaction_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->build()Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public connection_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->connection_id:Ljava/lang/String;

    return-object p0
.end method

.method public miryo_transaction_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->miryo_transaction_id:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/felica/Status;)Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->status:Lcom/squareup/protos/client/felica/Status;

    return-object p0
.end method

.method public transaction_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->transaction_id:Ljava/lang/String;

    return-object p0
.end method
