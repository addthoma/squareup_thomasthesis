.class final Lcom/squareup/protos/client/GetVariationInventoryResponse$ProtoAdapter_GetVariationInventoryResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetVariationInventoryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/GetVariationInventoryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetVariationInventoryResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/GetVariationInventoryResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 120
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/GetVariationInventoryResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/GetVariationInventoryResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    new-instance v0, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;-><init>()V

    .line 140
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 141
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 146
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 144
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->default_costs:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/VariationDefaultCost;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->current_counts:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 151
    invoke-virtual {v0}, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->build()Lcom/squareup/protos/client/GetVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse$ProtoAdapter_GetVariationInventoryResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/GetVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/GetVariationInventoryResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    sget-object v0, Lcom/squareup/protos/client/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/GetVariationInventoryResponse;->current_counts:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 133
    sget-object v0, Lcom/squareup/protos/client/VariationDefaultCost;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/GetVariationInventoryResponse;->default_costs:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 134
    invoke-virtual {p2}, Lcom/squareup/protos/client/GetVariationInventoryResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    check-cast p2, Lcom/squareup/protos/client/GetVariationInventoryResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/GetVariationInventoryResponse$ProtoAdapter_GetVariationInventoryResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/GetVariationInventoryResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/GetVariationInventoryResponse;)I
    .locals 4

    .line 125
    sget-object v0, Lcom/squareup/protos/client/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/GetVariationInventoryResponse;->current_counts:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/VariationDefaultCost;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 126
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/GetVariationInventoryResponse;->default_costs:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 118
    check-cast p1, Lcom/squareup/protos/client/GetVariationInventoryResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse$ProtoAdapter_GetVariationInventoryResponse;->encodedSize(Lcom/squareup/protos/client/GetVariationInventoryResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/GetVariationInventoryResponse;)Lcom/squareup/protos/client/GetVariationInventoryResponse;
    .locals 2

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse;->newBuilder()Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;

    move-result-object p1

    .line 157
    iget-object v0, p1, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->current_counts:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 158
    iget-object v0, p1, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->default_costs:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/VariationDefaultCost;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 159
    invoke-virtual {p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 160
    invoke-virtual {p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse$Builder;->build()Lcom/squareup/protos/client/GetVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 118
    check-cast p1, Lcom/squareup/protos/client/GetVariationInventoryResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/GetVariationInventoryResponse$ProtoAdapter_GetVariationInventoryResponse;->redact(Lcom/squareup/protos/client/GetVariationInventoryResponse;)Lcom/squareup/protos/client/GetVariationInventoryResponse;

    move-result-object p1

    return-object p1
.end method
