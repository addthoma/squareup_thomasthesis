.class public final Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;
.super Lcom/squareup/wire/Message;
.source "GetDepositScheduleResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;,
        Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SETTLEMENT_FREQUENCY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

.field private static final serialVersionUID:J


# instance fields
.field public final current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.WeeklyDepositSchedule#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.LocalTime#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.WeeklyDepositSchedule#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final potential_deposit_schedule:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.PotentialDepositSchedule#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
            ">;"
        }
    .end annotation
.end field

.field public final settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.SettlementFrequency#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->DO_NOT_USE:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    sput-object v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->DEFAULT_SETTLEMENT_FREQUENCY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Ljava/util/List;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/SettlementFrequency;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
            "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
            ">;",
            "Lcom/squareup/protos/common/time/LocalTime;",
            "Lcom/squareup/protos/client/depositsettings/SettlementFrequency;",
            ")V"
        }
    .end annotation

    .line 78
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;-><init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Ljava/util/List;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/SettlementFrequency;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Ljava/util/List;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/SettlementFrequency;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
            "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
            ">;",
            "Lcom/squareup/protos/common/time/LocalTime;",
            "Lcom/squareup/protos/client/depositsettings/SettlementFrequency;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 85
    sget-object v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 87
    iput-object p2, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const-string p1, "potential_deposit_schedule"

    .line 88
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    .line 89
    iput-object p4, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 90
    iput-object p5, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 108
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 109
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    .line 113
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/LocalTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 128
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    .locals 2

    .line 95
    new-instance v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->potential_deposit_schedule:Ljava/util/List;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->newBuilder()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v1, :cond_0

    const-string v1, ", current_weekly_deposit_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v1, :cond_1

    const-string v1, ", next_weekly_deposit_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", potential_deposit_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    if-eqz v1, :cond_3

    const-string v1, ", default_cutoff_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    if-eqz v1, :cond_4

    const-string v1, ", settlement_frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetDepositScheduleResponse{"

    .line 141
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
