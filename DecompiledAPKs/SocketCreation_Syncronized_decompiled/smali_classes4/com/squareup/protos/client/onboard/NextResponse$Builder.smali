.class public final Lcom/squareup/protos/client/onboard/NextResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NextResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/NextResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/NextResponse;",
        "Lcom/squareup/protos/client/onboard/NextResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/client/Status;

.field public step:Lcom/squareup/protos/client/onboard/Step;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/NextResponse;
    .locals 4

    .line 106
    new-instance v0, Lcom/squareup/protos/client/onboard/NextResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NextResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/NextResponse$Builder;->step:Lcom/squareup/protos/client/onboard/Step;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/onboard/NextResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/onboard/Step;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/NextResponse$Builder;->build()Lcom/squareup/protos/client/onboard/NextResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/onboard/NextResponse$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NextResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public step(Lcom/squareup/protos/client/onboard/Step;)Lcom/squareup/protos/client/onboard/NextResponse$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NextResponse$Builder;->step:Lcom/squareup/protos/client/onboard/Step;

    return-object p0
.end method
