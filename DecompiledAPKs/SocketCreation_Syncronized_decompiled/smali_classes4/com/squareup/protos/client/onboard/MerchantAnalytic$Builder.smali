.class public final Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantAnalytic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/MerchantAnalytic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/MerchantAnalytic;",
        "Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/MerchantAnalytic;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->name:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/onboard/MerchantAnalytic;-><init>(Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->build()Lcom/squareup/protos/client/onboard/MerchantAnalytic;

    move-result-object v0

    return-object v0
.end method

.method public name(Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;)Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->name:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
