.class public final Lcom/squareup/protos/client/multipass/DecryptEmailResponse;
.super Lcom/squareup/wire/Message;
.source "DecryptEmailResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/multipass/DecryptEmailResponse$ProtoAdapter_DecryptEmailResponse;,
        Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DECRYPTED_EMAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final decrypted_email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$ProtoAdapter_DecryptEmailResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$ProtoAdapter_DecryptEmailResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 38
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 43
    iput-object p1, p0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 57
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 58
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    .line 60
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 65
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 69
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;
    .locals 2

    .line 48
    new-instance v0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;-><init>()V

    .line 49
    iget-object v1, p0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;->decrypted_email:Ljava/lang/String;

    .line 50
    invoke-virtual {p0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->newBuilder()Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", decrypted_email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DecryptEmailResponse{"

    .line 78
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
