.class public final Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TransactionLedgerEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
        "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_data:Lokio/ByteString;

.field public timestamp:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v2, p0, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->encrypted_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;-><init>(Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->encrypted_data:Lokio/ByteString;

    return-object p0
.end method

.method public timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->timestamp:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
