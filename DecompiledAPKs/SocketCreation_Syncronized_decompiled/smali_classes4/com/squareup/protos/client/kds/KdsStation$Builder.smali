.class public final Lcom/squareup/protos/client/kds/KdsStation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "KdsStation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/kds/KdsStation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/kds/KdsStation;",
        "Lcom/squareup/protos/client/kds/KdsStation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public role:Lcom/squareup/protos/client/kds/KdsStation$Role;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/kds/KdsStation;
    .locals 5

    .line 146
    new-instance v0, Lcom/squareup/protos/client/kds/KdsStation;

    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/kds/KdsStation;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/kds/KdsStation$Role;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/kds/KdsStation$Builder;->build()Lcom/squareup/protos/client/kds/KdsStation;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/client/kds/KdsStation$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/kds/KdsStation$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public role(Lcom/squareup/protos/client/kds/KdsStation$Role;)Lcom/squareup/protos/client/kds/KdsStation$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-object p0
.end method
