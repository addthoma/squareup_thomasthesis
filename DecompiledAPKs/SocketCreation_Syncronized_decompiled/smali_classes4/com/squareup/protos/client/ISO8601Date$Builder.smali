.class public final Lcom/squareup/protos/client/ISO8601Date$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ISO8601Date.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ISO8601Date;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/ISO8601Date;",
        "Lcom/squareup/protos/client/ISO8601Date$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public date_string:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/ISO8601Date;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, p0, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    return-object v0
.end method

.method public date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string:Ljava/lang/String;

    return-object p0
.end method
