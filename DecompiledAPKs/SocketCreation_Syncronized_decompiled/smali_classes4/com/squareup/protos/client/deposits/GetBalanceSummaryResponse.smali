.class public final Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
.super Lcom/squareup/wire/Message;
.source "GetBalanceSummaryResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;,
        Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;,
        Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;,
        Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_PARTIAL_DEPOSIT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_partial_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.deposits.GetBalanceSummaryResponse$BalanceInformation#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final card_activity:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.deposits.GetBalanceSummaryResponse$DepositActivity#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final eligibility_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.deposits.EligibilityDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/deposits/EligibilityDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.deposits.InstantDepositDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bankaccount.BankAccount#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final linked_card:Lcom/squareup/protos/client/deposits/CardInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.deposits.CardInfo#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final square_card:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.ListCardsResponse$CardData#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->DEFAULT_ALLOW_PARTIAL_DEPOSIT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/InstantDepositDetails;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/client/deposits/InstantDepositDetails;",
            "Lcom/squareup/protos/client/bankaccount/BankAccount;",
            "Lcom/squareup/protos/client/deposits/CardInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/deposits/EligibilityDetails;",
            ">;)V"
        }
    .end annotation

    .line 128
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/InstantDepositDetails;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/InstantDepositDetails;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/client/deposits/InstantDepositDetails;",
            "Lcom/squareup/protos/client/bankaccount/BankAccount;",
            "Lcom/squareup/protos/client/deposits/CardInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/deposits/EligibilityDetails;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 137
    sget-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    .line 139
    iput-object p2, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    .line 140
    iput-object p3, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 141
    iput-object p4, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    const-string p1, "square_card"

    .line 142
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    .line 143
    iput-object p6, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    const-string p1, "card_activity"

    .line 144
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    .line 145
    iput-object p8, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    .line 146
    iput-object p9, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    const-string p1, "eligibility_details"

    .line 147
    invoke-static {p1, p10}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 170
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 171
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    .line 172
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    .line 173
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    .line 174
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    .line 177
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    .line 179
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    .line 182
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 187
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 189
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccount;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/deposits/CardInfo;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 2

    .line 152
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;-><init>()V

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->square_card:Ljava/util/List;

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->allow_partial_deposit:Ljava/lang/Boolean;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->card_activity:Ljava/util/List;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->eligibility_details:Ljava/util/List;

    .line 163
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->newBuilder()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    if-eqz v1, :cond_1

    const-string v1, ", instant_deposit_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v1, :cond_2

    const-string v1, ", linked_bank_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    if-eqz v1, :cond_3

    const-string v1, ", linked_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", square_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", allow_partial_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 214
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", card_activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 215
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    if-eqz v1, :cond_7

    const-string v1, ", deposit_activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    if-eqz v1, :cond_8

    const-string v1, ", balance_information="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 217
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", eligibility_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetBalanceSummaryResponse{"

    .line 218
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
