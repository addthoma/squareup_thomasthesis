.class public final Lcom/squareup/protos/client/dialogue/ConversationListItem;
.super Lcom/squareup/wire/Message;
.source "ConversationListItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/dialogue/ConversationListItem$ProtoAdapter_ConversationListItem;,
        Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
        "Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_COMMENT_CONTENT:Ljava/lang/String; = ""

.field public static final DEFAULT_OPENED:Ljava/lang/Boolean;

.field public static final DEFAULT_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final last_comment_content:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final opened:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Sentiment#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$ProtoAdapter_ConversationListItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/ConversationListItem$ProtoAdapter_ConversationListItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->DEFAULT_OPENED:Ljava/lang/Boolean;

    .line 36
    sget-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->INVALID_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

    sput-object v0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->DEFAULT_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;)V
    .locals 8

    .line 100
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/dialogue/ConversationListItem;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 106
    sget-object v0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    .line 108
    iput-object p2, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    .line 109
    iput-object p3, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 110
    iput-object p4, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    .line 111
    iput-object p5, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 112
    iput-object p6, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->display_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 131
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 132
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ConversationListItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/ConversationListItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->display_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/ConversationListItem;->display_name:Ljava/lang/String;

    .line 139
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 144
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ConversationListItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/Sentiment;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 153
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;
    .locals 2

    .line 117
    new-instance v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->token:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->opened:Ljava/lang/Boolean;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->last_comment_content:Ljava/lang/String;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->display_name:Ljava/lang/String;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ConversationListItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ConversationListItem;->newBuilder()Lcom/squareup/protos/client/dialogue/ConversationListItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", opened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->opened:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", last_comment_created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->last_comment_content:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", last_comment_content=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-eqz v1, :cond_4

    const-string v1, ", sentiment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", display_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ConversationListItem{"

    .line 167
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
