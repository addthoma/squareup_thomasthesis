.class public final Lcom/squareup/protos/client/dialogue/Message$Transaction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Message.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Message$Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/Message$Transaction;",
        "Lcom/squareup/protos/client/dialogue/Message$Transaction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public payment_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 239
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/Message$Transaction;
    .locals 3

    .line 249
    new-instance v0, Lcom/squareup/protos/client/dialogue/Message$Transaction;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Transaction$Builder;->payment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/dialogue/Message$Transaction;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 236
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Message$Transaction$Builder;->build()Lcom/squareup/protos/client/dialogue/Message$Transaction;

    move-result-object v0

    return-object v0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Message$Transaction$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Transaction$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method
