.class public final Lcom/squareup/protos/client/dialogue/Comment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Comment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Comment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/Comment;",
        "Lcom/squareup/protos/client/dialogue/Comment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public commenter:Lcom/squareup/protos/client/dialogue/Commenter;

.field public content:Ljava/lang/String;

.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public subject:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/Comment;
    .locals 8

    .line 207
    new-instance v7, Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->content:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->commenter:Lcom/squareup/protos/client/dialogue/Commenter;

    iget-object v5, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->subject:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/dialogue/Comment;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Commenter;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->build()Lcom/squareup/protos/client/dialogue/Comment;

    move-result-object v0

    return-object v0
.end method

.method public commenter(Lcom/squareup/protos/client/dialogue/Commenter;)Lcom/squareup/protos/client/dialogue/Comment$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->commenter:Lcom/squareup/protos/client/dialogue/Commenter;

    return-object p0
.end method

.method public content(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Comment$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->content:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/dialogue/Comment$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public subject(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Comment$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->subject:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Comment$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Comment$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
