.class public final Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateCouponRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/CreateCouponRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/CreateCouponRequest;",
        "Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conversation_token:Ljava/lang/String;

.field public discount:Lcom/squareup/api/items/Discount;

.field public request_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/CreateCouponRequest;
    .locals 5

    .line 132
    new-instance v0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->request_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->conversation_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->discount:Lcom/squareup/api/items/Discount;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/dialogue/CreateCouponRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateCouponRequest;

    move-result-object v0

    return-object v0
.end method

.method public conversation_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->conversation_token:Ljava/lang/String;

    return-object p0
.end method

.method public discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->discount:Lcom/squareup/api/items/Discount;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method
