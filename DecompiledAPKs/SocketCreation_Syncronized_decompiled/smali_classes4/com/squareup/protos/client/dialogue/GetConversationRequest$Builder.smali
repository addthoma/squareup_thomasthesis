.class public final Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetConversationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/GetConversationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/GetConversationRequest;",
        "Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conversation_token:Ljava/lang/String;

.field public opened:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/GetConversationRequest;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/client/dialogue/GetConversationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->conversation_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->opened:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/dialogue/GetConversationRequest;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/GetConversationRequest;

    move-result-object v0

    return-object v0
.end method

.method public conversation_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->conversation_token:Ljava/lang/String;

    return-object p0
.end method

.method public opened(Ljava/lang/Boolean;)Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->opened:Ljava/lang/Boolean;

    return-object p0
.end method
