.class public final Lcom/squareup/protos/client/ClientAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/ClientAction;",
        "Lcom/squareup/protos/client/ClientAction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

.field public create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

.field public fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

.field public finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

.field public link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

.field public start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

.field public start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

.field public view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

.field public view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

.field public view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

.field public view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

.field public view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

.field public view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

.field public view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

.field public view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

.field public view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

.field public view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

.field public view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

.field public view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

.field public view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

.field public view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

.field public view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

.field public view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

.field public view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

.field public view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

.field public view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

.field public view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

.field public view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

.field public view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

.field public view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

.field public view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 510
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activate_account(Lcom/squareup/protos/client/ClientAction$ActivateAccount;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1374
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    const/4 p1, 0x0

    .line 1375
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1376
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1377
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1378
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1379
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1380
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1381
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1382
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1383
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1384
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1385
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1386
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1387
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1388
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1389
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1390
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1391
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1392
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1393
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1394
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1395
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1396
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1397
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1398
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1399
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1400
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1401
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1402
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1403
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/ClientAction;
    .locals 36

    move-object/from16 v0, p0

    .line 1546
    new-instance v34, Lcom/squareup/protos/client/ClientAction;

    move-object/from16 v1, v34

    iget-object v2, v0, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    iget-object v3, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    iget-object v4, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    iget-object v5, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    iget-object v6, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    iget-object v7, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    iget-object v8, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    iget-object v9, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    iget-object v10, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    iget-object v11, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    iget-object v12, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    iget-object v13, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    iget-object v14, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    iget-object v15, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    move-object/from16 v35, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    move-object/from16 v23, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    move-object/from16 v24, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    move-object/from16 v25, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    move-object/from16 v26, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    move-object/from16 v27, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    move-object/from16 v28, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    move-object/from16 v29, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    move-object/from16 v30, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    move-object/from16 v31, v1

    iget-object v1, v0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    move-object/from16 v32, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v33

    move-object/from16 v1, v35

    invoke-direct/range {v1 .. v33}, Lcom/squareup/protos/client/ClientAction;-><init>(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;Lcom/squareup/protos/client/ClientAction$ViewInvoice;Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;Lcom/squareup/protos/client/ClientAction$ViewSalesReport;Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;Lcom/squareup/protos/client/ClientAction$ViewDispute;Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;Lcom/squareup/protos/client/ClientAction$CreateItem;Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;Lcom/squareup/protos/client/ClientAction$ViewBankAccount;Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;Lcom/squareup/protos/client/ClientAction$ActivateAccount;Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;Lcom/squareup/protos/client/ClientAction$LinkBankAccount;Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;Lokio/ByteString;)V

    return-object v34
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 447
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object v0

    return-object v0
.end method

.method public create_item(Lcom/squareup/protos/client/ClientAction$CreateItem;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1067
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    const/4 p1, 0x0

    .line 1068
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1069
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1070
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1071
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1072
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1073
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1074
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1075
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1076
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1077
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1078
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1079
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1080
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1081
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1082
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1083
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1084
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1085
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1086
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1087
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1088
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1089
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1090
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1091
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1092
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1093
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1094
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1095
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1096
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 517
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    return-object p0
.end method

.method public finish_account_setup(Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1408
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    const/4 p1, 0x0

    .line 1409
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1410
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1411
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1412
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1413
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1414
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1415
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1416
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1417
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1418
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1419
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1420
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1421
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1422
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1423
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1424
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1425
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1426
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1427
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1428
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1429
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1430
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1431
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1432
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1433
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1434
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1435
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1436
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1437
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public link_bank_account(Lcom/squareup/protos/client/ClientAction$LinkBankAccount;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1442
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    const/4 p1, 0x0

    .line 1443
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1444
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1445
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1446
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1447
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1448
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1449
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1450
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1451
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1452
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1453
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1454
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1455
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1456
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1457
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1458
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1459
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1460
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1461
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1462
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1463
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1464
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1465
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1466
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1467
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1468
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1469
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1470
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1471
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public start_create_item_tutorial(Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1476
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    const/4 p1, 0x0

    .line 1477
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1478
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1479
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1480
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1481
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1482
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1483
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1484
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1485
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1486
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1487
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1488
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1489
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1490
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1491
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1492
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1493
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1494
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1495
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1496
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1497
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1498
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1499
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1500
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1501
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1502
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1503
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1504
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1505
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public start_first_payment_tutorial(Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1511
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    const/4 p1, 0x0

    .line 1512
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1513
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1514
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1515
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1516
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1517
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1518
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1519
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1520
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1521
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1522
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1523
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1524
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1525
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1526
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1527
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1528
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1529
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1530
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1531
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1532
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1533
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1534
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1535
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1536
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1537
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1538
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1539
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1540
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    return-object p0
.end method

.method public view_all_deposits(Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 896
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    const/4 p1, 0x0

    .line 897
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 898
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 899
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 900
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 901
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 902
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 903
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 904
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 905
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 906
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 907
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 908
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 909
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 910
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 911
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 912
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 913
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 914
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 915
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 916
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 917
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 918
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 919
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 920
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 921
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 922
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 923
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 924
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 925
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_all_disputes(Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 794
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    const/4 p1, 0x0

    .line 795
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 796
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 797
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 798
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 799
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 800
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 801
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 802
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 803
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 804
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 805
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 806
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 807
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 808
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 809
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 810
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 811
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 812
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 813
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 814
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 815
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 816
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 817
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 818
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 819
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 820
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 821
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 822
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 823
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_all_invoices(Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 624
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    const/4 p1, 0x0

    .line 625
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 626
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 627
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 628
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 629
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 630
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 631
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 632
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 633
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 634
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 635
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 636
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 637
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 638
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 639
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 640
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 641
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 642
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 643
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 644
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 645
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 646
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 647
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 648
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 649
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 650
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 651
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 652
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 653
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_bank_account(Lcom/squareup/protos/client/ClientAction$ViewBankAccount;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1135
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    const/4 p1, 0x0

    .line 1136
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1137
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1138
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1139
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1140
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1141
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1142
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1143
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1144
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1145
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1146
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1147
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1148
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1149
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1150
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1151
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1152
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1153
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1154
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1155
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1156
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1157
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1158
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1159
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1160
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1161
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1162
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1163
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1164
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_checkout_applet(Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 522
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    const/4 p1, 0x0

    .line 523
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 524
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 525
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 526
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 527
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 528
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 529
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 530
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 531
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 532
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 533
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 534
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 535
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 536
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 537
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 538
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 539
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 540
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 541
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 542
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 543
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 544
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 545
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 546
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 547
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 548
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 549
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 550
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 551
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_customer_conversation(Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 999
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    const/4 p1, 0x0

    .line 1000
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1001
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1002
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1003
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1004
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1005
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1006
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1007
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1008
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1009
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1010
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1011
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1012
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1013
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1014
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1015
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1016
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1017
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1018
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1019
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1020
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1021
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1022
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1023
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1024
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1025
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1026
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1027
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1028
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_customer_message_center(Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 965
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    const/4 p1, 0x0

    .line 966
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 967
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 968
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 969
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 970
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 971
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 972
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 973
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 974
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 975
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 976
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 977
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 978
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 979
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 980
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 981
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 982
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 983
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 984
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 985
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 986
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 987
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 988
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 989
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 990
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 991
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 992
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 993
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 994
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_customers_applet(Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 930
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    const/4 p1, 0x0

    .line 931
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 932
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 933
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 934
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 935
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 936
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 937
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 938
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 939
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 940
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 941
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 942
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 943
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 944
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 945
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 946
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 947
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 948
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 949
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 950
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 951
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 952
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 953
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 954
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 955
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 956
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 957
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 958
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 959
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_deposits_applet(Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 862
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    const/4 p1, 0x0

    .line 863
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 864
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 865
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 866
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 867
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 868
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 869
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 870
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 871
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 872
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 873
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 874
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 875
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 876
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 877
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 878
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 879
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 880
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 881
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 882
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 883
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 884
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 885
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 886
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 887
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 888
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 889
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 890
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 891
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_deposits_settings(Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1272
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    const/4 p1, 0x0

    .line 1273
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1274
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1275
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1276
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1277
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1278
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1279
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1280
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1281
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1282
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1283
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1284
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1285
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1286
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1287
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1288
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1289
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1290
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1291
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1292
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1293
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1294
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1295
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1296
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1297
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1298
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1299
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1300
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1301
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_dispute(Lcom/squareup/protos/client/ClientAction$ViewDispute;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 828
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    const/4 p1, 0x0

    .line 829
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 830
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 831
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 832
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 833
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 834
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 835
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 836
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 837
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 838
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 839
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 840
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 841
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 842
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 843
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 844
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 845
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 846
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 847
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 848
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 849
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 850
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 851
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 852
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 853
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 854
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 855
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 856
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 857
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_free_processing(Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1340
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    const/4 p1, 0x0

    .line 1341
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1342
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1343
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1344
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1345
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1346
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1347
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1348
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1349
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1350
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1351
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1352
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1353
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1354
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1355
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1356
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1357
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1358
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1359
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1360
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1361
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1362
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1363
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1364
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1365
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1366
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1367
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1368
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1369
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_invoice(Lcom/squareup/protos/client/ClientAction$ViewInvoice;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 658
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    const/4 p1, 0x0

    .line 659
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 660
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 661
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 662
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 663
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 664
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 665
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 666
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 667
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 668
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 669
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 670
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 671
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 672
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 673
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 674
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 675
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 676
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 677
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 678
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 679
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 680
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 681
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 682
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 683
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 684
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 685
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 686
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 687
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_invoices_applet(Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 590
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    const/4 p1, 0x0

    .line 591
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 592
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 593
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 594
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 595
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 596
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 597
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 598
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 599
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 600
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 601
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 602
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 603
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 604
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 605
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 606
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 607
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 608
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 609
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 610
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 611
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 612
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 613
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 614
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 615
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 616
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 617
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 618
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 619
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_items_applet(Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1033
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    const/4 p1, 0x0

    .line 1034
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1035
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1036
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1037
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1038
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1039
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1040
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1041
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1042
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1043
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1044
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1045
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1046
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1047
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1048
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1049
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1050
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1051
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1052
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1053
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1054
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1055
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1056
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1057
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1058
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1059
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1060
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1061
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1062
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_orders_applet(Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 556
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    const/4 p1, 0x0

    .line 557
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 558
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 559
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 560
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 561
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 562
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 563
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 564
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 565
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 566
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 567
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 568
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 569
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 570
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 571
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 572
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 573
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 574
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 575
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 576
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 577
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 578
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 579
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 580
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 581
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 582
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 583
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 584
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 585
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_overdue_invoices(Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1306
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    const/4 p1, 0x0

    .line 1307
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1308
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1309
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1310
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1311
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1312
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1313
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1314
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1315
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1316
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1317
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1318
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1319
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1320
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1321
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1322
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1323
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1324
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1325
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1326
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1327
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1328
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1329
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1330
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1331
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1332
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1333
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1334
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1335
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_reports_applet(Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 726
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    const/4 p1, 0x0

    .line 727
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 728
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 729
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 730
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 731
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 732
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 733
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 734
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 735
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 736
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 737
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 738
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 739
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 740
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 741
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 742
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 743
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 744
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 745
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 746
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 747
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 748
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 749
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 750
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 751
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 752
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 753
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 754
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 755
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_sales_report(Lcom/squareup/protos/client/ClientAction$ViewSalesReport;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 760
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    const/4 p1, 0x0

    .line 761
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 762
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 763
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 764
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 765
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 766
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 767
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 768
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 769
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 770
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 771
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 772
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 773
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 774
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 775
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 776
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 777
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 778
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 779
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 780
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 781
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 782
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 783
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 784
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 785
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 786
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 787
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 788
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 789
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_settings_applet(Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1101
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    const/4 p1, 0x0

    .line 1102
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1103
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1104
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1105
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1106
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1107
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1108
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1109
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1110
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1111
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1112
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1113
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1114
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1115
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1116
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1117
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1118
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1119
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1120
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1121
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1122
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1123
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1124
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1125
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1126
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1127
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1128
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1129
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1130
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_support_applet(Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1169
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    const/4 p1, 0x0

    .line 1170
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1171
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1172
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1173
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1174
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1175
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1176
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1177
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1178
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1179
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1180
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1181
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1182
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1183
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1184
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1185
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1186
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1187
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1188
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1189
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1190
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1191
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1192
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1193
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1194
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1195
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1196
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1197
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1198
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_support_message_center(Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1204
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    const/4 p1, 0x0

    .line 1205
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1206
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1207
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1208
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1209
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1210
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1211
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1212
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1213
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1214
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1215
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1216
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1217
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1218
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1219
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1220
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1221
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1222
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1223
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1224
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1225
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 1226
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1227
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1228
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1229
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1230
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1231
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1232
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1233
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_transactions_applet(Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 692
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    const/4 p1, 0x0

    .line 693
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 694
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 695
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 696
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 697
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 698
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 699
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 700
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 701
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 702
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 703
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 704
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 705
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 706
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 707
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 708
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 709
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 710
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 711
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 712
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 713
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    .line 714
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 715
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 716
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 717
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 718
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 719
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 720
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 721
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method

.method public view_tutorials_and_tours(Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)Lcom/squareup/protos/client/ClientAction$Builder;
    .locals 0

    .line 1238
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    const/4 p1, 0x0

    .line 1239
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    .line 1240
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    .line 1241
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    .line 1242
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    .line 1243
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    .line 1244
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    .line 1245
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    .line 1246
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    .line 1247
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    .line 1248
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    .line 1249
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    .line 1250
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    .line 1251
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    .line 1252
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    .line 1253
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    .line 1254
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    .line 1255
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    .line 1256
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    .line 1257
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    .line 1258
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    .line 1259
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    .line 1260
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    .line 1261
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    .line 1262
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    .line 1263
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    .line 1264
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    .line 1265
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    .line 1266
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_create_item_tutorial:Lcom/squareup/protos/client/ClientAction$StartCreateItemTutorial;

    .line 1267
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$Builder;->start_first_payment_tutorial:Lcom/squareup/protos/client/ClientAction$StartFirstPaymentTutorial;

    return-object p0
.end method
