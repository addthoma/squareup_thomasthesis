.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetLoyaltyReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

.field public count:Ljava/lang/Double;

.field public money_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 539
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;
    .locals 5

    .line 561
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->count:Ljava/lang/Double;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->money_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;-><init>(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;Ljava/lang/Double;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 532
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;

    move-result-object v0

    return-object v0
.end method

.method public cohort_type(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;
    .locals 0

    .line 543
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->cohort_type:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    return-object p0
.end method

.method public count(Ljava/lang/Double;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;
    .locals 0

    .line 548
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->count:Ljava/lang/Double;

    const/4 p1, 0x0

    .line 549
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->money_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public money_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;
    .locals 0

    .line 554
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->money_amount:Lcom/squareup/protos/common/Money;

    const/4 p1, 0x0

    .line 555
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount$Builder;->count:Ljava/lang/Double;

    return-object p0
.end method
