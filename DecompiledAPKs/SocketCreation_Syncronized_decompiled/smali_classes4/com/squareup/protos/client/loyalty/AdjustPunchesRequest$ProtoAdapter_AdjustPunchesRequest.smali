.class final Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AdjustPunchesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AdjustPunchesRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 384
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 415
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;-><init>()V

    .line 416
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 417
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 442
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 440
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->balance_before_adjustment(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    goto :goto_0

    .line 434
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->increment_reason(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 436
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 426
    :pswitch_2
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->decrement_reason(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 428
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 423
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    goto :goto_0

    .line 422
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    goto :goto_0

    .line 421
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->current_stars(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    goto :goto_0

    .line 420
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    goto :goto_0

    .line 419
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->adjustment(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    goto/16 :goto_0

    .line 446
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 447
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 382
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 402
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 403
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 404
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 405
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 406
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 407
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 408
    sget-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 409
    sget-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 410
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 382
    check-cast p2, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)I
    .locals 4

    .line 389
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 390
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v3, 0x4

    .line 391
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    const/16 v3, 0x8

    .line 392
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    const/4 v3, 0x2

    .line 393
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    const/4 v3, 0x5

    .line 394
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    const/4 v3, 0x6

    .line 395
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    const/4 v3, 0x7

    .line 396
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 397
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 382
    check-cast p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;->encodedSize(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
    .locals 2

    .line 452
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object p1

    .line 453
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/CreatorDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 454
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    .line 455
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 456
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 382
    check-cast p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;->redact(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    move-result-object p1

    return-object p1
.end method
