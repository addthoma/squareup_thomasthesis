.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;
.super Lcom/squareup/wire/Message;
.source "GetLoyaltyReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$ProtoAdapter_GetLoyaltyReportResponse;,
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;,
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;,
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;,
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DATA_SIZE:Ljava/lang/Long;

.field public static final DEFAULT_LOYALTY_CUSTOMER_COUNT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final average_spend:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.GetLoyaltyReportResponse$AggregateCount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;"
        }
    .end annotation
.end field

.field public final average_visits:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.GetLoyaltyReportResponse$AggregateCount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;"
        }
    .end annotation
.end field

.field public final data_size:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x7
    .end annotation
.end field

.field public final loyalty_customer_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final top_customer_counts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.GetLoyaltyReportResponse$FrequencyCount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;"
        }
    .end annotation
.end field

.field public final top_reward_counts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.GetLoyaltyReportResponse$FrequencyCount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$ProtoAdapter_GetLoyaltyReportResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$ProtoAdapter_GetLoyaltyReportResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 31
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->DEFAULT_LOYALTY_CUSTOMER_COUNT:Ljava/lang/Long;

    .line 33
    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->DEFAULT_DATA_SIZE:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 103
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;",
            "Ljava/lang/Long;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 110
    sget-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->status:Lcom/squareup/protos/client/Status;

    .line 112
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    const-string p1, "average_visits"

    .line 113
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    const-string p1, "average_spend"

    .line 114
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    const-string p1, "top_customer_counts"

    .line 115
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    const-string p1, "top_reward_counts"

    .line 116
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    .line 117
    iput-object p7, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->data_size:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 137
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 138
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->status:Lcom/squareup/protos/client/Status;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    .line 142
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    .line 143
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    .line 144
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    .line 145
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->data_size:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->data_size:Ljava/lang/Long;

    .line 146
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 151
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 153
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->data_size:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 161
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 2

    .line 122
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->loyalty_customer_count:Ljava/lang/Long;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_visits:Ljava/util/List;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_spend:Ljava/util/List;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_customer_counts:Ljava/util/List;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_reward_counts:Ljava/util/List;

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->data_size:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->data_size:Ljava/lang/Long;

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", loyalty_customer_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->loyalty_customer_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", average_visits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_visits:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", average_spend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->average_spend:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", top_customer_counts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_customer_counts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", top_reward_counts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->top_reward_counts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->data_size:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", data_size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;->data_size:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetLoyaltyReportResponse{"

    .line 176
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
