.class public final Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public loyalty_account_mapping_options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

.field public loyalty_account_options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions;
    .locals 4

    .line 128
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->loyalty_account_options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->loyalty_account_mapping_options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    move-result-object v0

    return-object v0
.end method

.method public loyalty_account_mapping_options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->loyalty_account_mapping_options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

    return-object p0
.end method

.method public loyalty_account_options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$Builder;->loyalty_account_options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    return-object p0
.end method
