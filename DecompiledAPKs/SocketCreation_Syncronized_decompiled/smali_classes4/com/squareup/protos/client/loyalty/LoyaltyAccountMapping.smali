.class public final Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;
.super Lcom/squareup/wire/Message;
.source "LoyaltyAccountMapping.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$ProtoAdapter_LoyaltyAccountMapping;,
        Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;,
        Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOYALTY_ACCOUNT_MAPPING_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_RAW_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final loyalty_account_mapping_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final raw_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyAccountMapping$Type#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$ProtoAdapter_LoyaltyAccountMapping;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$ProtoAdapter_LoyaltyAccountMapping;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->TYPE_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->DEFAULT_TYPE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 80
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    .line 88
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    .line 89
    iput-object p4, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 106
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 107
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    .line 112
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 117
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 124
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;
    .locals 2

    .line 94
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->loyalty_account_mapping_token:Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->id:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->raw_id:Ljava/lang/String;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", loyalty_account_mapping_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", raw_id=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyAccountMapping{"

    .line 136
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
