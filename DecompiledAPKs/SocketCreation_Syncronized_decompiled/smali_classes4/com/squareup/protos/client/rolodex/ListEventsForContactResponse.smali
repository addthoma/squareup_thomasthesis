.class public final Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;
.super Lcom/squareup/wire/Message;
.source "ListEventsForContactResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$ProtoAdapter_ListEventsForContactResponse;,
        Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final event:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Event#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;"
        }
    .end annotation
.end field

.field public final list_option:Lcom/squareup/protos/client/rolodex/ListOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.ListOption#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$ProtoAdapter_ListEventsForContactResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$ProtoAdapter_ListEventsForContactResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListOption;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/ListOption;",
            ")V"
        }
    .end annotation

    .line 52
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListOption;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListOption;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/ListOption;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 57
    sget-object v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 58
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->status:Lcom/squareup/protos/client/Status;

    const-string p1, "event"

    .line 59
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->event:Ljava/util/List;

    .line 60
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 76
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 77
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->status:Lcom/squareup/protos/client/Status;

    .line 79
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->event:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->event:Ljava/util/List;

    .line 80
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    .line 81
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/ListOption;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 92
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;-><init>()V

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->event:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->event:Ljava/util/List;

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->newBuilder()Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->event:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    if-eqz v1, :cond_2

    const-string v1, ", list_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListEventsForContactResponse{"

    .line 103
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
