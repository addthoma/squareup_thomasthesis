.class public final Lcom/squareup/protos/client/rolodex/QueryContext;
.super Lcom/squareup/wire/Message;
.source "QueryContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/QueryContext$ProtoAdapter_QueryContext;,
        Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/QueryContext;",
        "Lcom/squareup/protos/client/rolodex/QueryContext$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/QueryContext;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTOMATIC:Ljava/lang/Boolean;

.field public static final DEFAULT_FULL_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_FULL_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_GROUP_V2_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_PAGING_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_QUERY:Ljava/lang/String; = ""

.field public static final DEFAULT_SORT_TYPE:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

.field private static final serialVersionUID:J


# instance fields
.field public final automatic:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final filters:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public final full_email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final full_phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final group_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final group_v2_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final paging_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final payment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final query:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.ListContactsSortType#ADAPTER"
        tag = 0xd
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/QueryContext$ProtoAdapter_QueryContext;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/QueryContext$ProtoAdapter_QueryContext;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/QueryContext;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/rolodex/QueryContext;->DEFAULT_LIMIT:Ljava/lang/Integer;

    .line 33
    sget-object v1, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    sput-object v1, Lcom/squareup/protos/client/rolodex/QueryContext;->DEFAULT_SORT_TYPE:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/QueryContext;->DEFAULT_AUTOMATIC:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 153
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/rolodex/QueryContext;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 160
    sget-object v0, Lcom/squareup/protos/client/rolodex/QueryContext;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->limit:Ljava/lang/Integer;

    .line 162
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->paging_key:Ljava/lang/String;

    .line 163
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->query:Ljava/lang/String;

    const-string p1, "group_token"

    .line 164
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_token:Ljava/util/List;

    .line 165
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 166
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_phone_number:Ljava/lang/String;

    .line 167
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->automatic:Ljava/lang/Boolean;

    .line 168
    iput-object p8, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_email_address:Ljava/lang/String;

    const-string p1, "filters"

    .line 169
    invoke-static {p1, p9}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->filters:Ljava/util/List;

    .line 170
    iput-object p10, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_v2_token:Ljava/lang/String;

    .line 171
    iput-object p11, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->payment_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 195
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/QueryContext;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 196
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/QueryContext;

    .line 197
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/QueryContext;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/QueryContext;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->limit:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->limit:Ljava/lang/Integer;

    .line 198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->paging_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->paging_key:Ljava/lang/String;

    .line 199
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->query:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->query:Ljava/lang/String;

    .line 200
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_token:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->group_token:Ljava/util/List;

    .line 201
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->full_phone_number:Ljava/lang/String;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->automatic:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->automatic:Ljava/lang/Boolean;

    .line 204
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->full_email_address:Ljava/lang/String;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->filters:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->filters:Ljava/util/List;

    .line 206
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_v2_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->group_v2_token:Ljava/lang/String;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->payment_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/QueryContext;->payment_token:Ljava/lang/String;

    .line 208
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 213
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 215
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/QueryContext;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->limit:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->query:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->automatic:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_email_address:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_v2_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 227
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/QueryContext$Builder;
    .locals 2

    .line 176
    new-instance v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;-><init>()V

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->limit:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->limit:Ljava/lang/Integer;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->paging_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->paging_key:Ljava/lang/String;

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->query:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->query:Ljava/lang/String;

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_token:Ljava/util/List;

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->full_phone_number:Ljava/lang/String;

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->automatic:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->automatic:Ljava/lang/Boolean;

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->full_email_address:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->filters:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->filters:Ljava/util/List;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_v2_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->group_v2_token:Ljava/lang/String;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->payment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->payment_token:Ljava/lang/String;

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/QueryContext;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/QueryContext$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/QueryContext;->newBuilder()Lcom/squareup/protos/client/rolodex/QueryContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->limit:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", paging_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->paging_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->query:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", group_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    if-eqz v1, :cond_4

    const-string v1, ", sort_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->sort_type:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 240
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", full_phone_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->automatic:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", automatic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->automatic:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 242
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_email_address:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", full_email_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->full_email_address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", filters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->filters:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 244
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_v2_token:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", group_v2_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->group_v2_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", payment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/QueryContext;->payment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "QueryContext{"

    .line 246
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
