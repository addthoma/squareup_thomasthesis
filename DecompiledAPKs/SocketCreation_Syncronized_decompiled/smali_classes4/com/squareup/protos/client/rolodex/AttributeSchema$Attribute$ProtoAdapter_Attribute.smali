.class final Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Attribute"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1119
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1148
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;-><init>()V

    .line 1149
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1150
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1167
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1165
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->was_boolean_ever_set(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    goto :goto_0

    .line 1164
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->is_visible_in_profile(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    goto :goto_0

    .line 1163
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    goto :goto_0

    .line 1162
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->fallback_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    goto :goto_0

    .line 1156
    :pswitch_4
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->type(Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1158
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1153
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    goto :goto_0

    .line 1152
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    goto :goto_0

    .line 1171
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1172
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1117
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1136
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1137
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1138
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1139
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1140
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1141
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1142
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1143
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1117
    check-cast p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)I
    .locals 4

    .line 1124
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1125
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v3, 0x3

    .line 1126
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    const/4 v3, 0x4

    .line 1127
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    const/4 v3, 0x5

    .line 1128
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 1129
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->was_boolean_ever_set:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 1130
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1131
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1117
    check-cast p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;->encodedSize(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 2

    .line 1177
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object p1

    .line 1178
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    .line 1179
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1180
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1117
    check-cast p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$ProtoAdapter_Attribute;->redact(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    return-object p1
.end method
