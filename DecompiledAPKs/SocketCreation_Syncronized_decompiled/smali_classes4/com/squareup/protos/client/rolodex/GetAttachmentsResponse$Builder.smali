.class public final Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetAttachmentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;",
        "Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment_responses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;"
        }
    .end annotation
.end field

.field public attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field public failed_attachments:Ljava/lang/Integer;

.field public status:Lcom/squareup/protos/client/Status;

.field public succeeded_attachments:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    .line 143
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attachment_responses(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;"
        }
    .end annotation

    .line 157
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    return-object p0
.end method

.method public attachments(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;"
        }
    .end annotation

    .line 163
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachments:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;
    .locals 8

    .line 175
    new-instance v7, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->succeeded_attachments:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->failed_attachments:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachments:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    move-result-object v0

    return-object v0
.end method

.method public failed_attachments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->failed_attachments:Ljava/lang/Integer;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public succeeded_attachments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->succeeded_attachments:Ljava/lang/Integer;

    return-object p0
.end method
