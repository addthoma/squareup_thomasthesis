.class public final Lcom/squareup/protos/client/rolodex/ListContactsRequest;
.super Lcom/squareup/wire/Message;
.source "ListContactsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/ListContactsRequest$ProtoAdapter_ListContactsRequest;,
        Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/ListContactsRequest;",
        "Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/ListContactsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.ContactOptions#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final context:Lcom/squareup/protos/client/rolodex/QueryContext;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.QueryContext#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$ProtoAdapter_ListContactsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$ProtoAdapter_ListContactsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/QueryContext;Lcom/squareup/protos/client/rolodex/ContactOptions;)V
    .locals 1

    .line 43
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest;-><init>(Lcom/squareup/protos/client/rolodex/QueryContext;Lcom/squareup/protos/client/rolodex/ContactOptions;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/QueryContext;Lcom/squareup/protos/client/rolodex/ContactOptions;Lokio/ByteString;)V
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    .line 50
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 65
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/ListContactsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 66
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/ListContactsRequest;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    .line 68
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    .line 69
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 74
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/QueryContext;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/ContactOptions;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 79
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    if-eqz v1, :cond_0

    const-string v1, ", context="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    if-eqz v1, :cond_1

    const-string v1, ", contact_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListContactsRequest{"

    .line 89
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
