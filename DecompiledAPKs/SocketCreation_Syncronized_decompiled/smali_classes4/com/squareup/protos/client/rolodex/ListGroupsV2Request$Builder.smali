.class public final Lcom/squareup/protos/client/rolodex/ListGroupsV2Request$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListGroupsV2Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListGroupsV2Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListGroupsV2Request;",
        "Lcom/squareup/protos/client/rolodex/ListGroupsV2Request$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group_options:Lcom/squareup/protos/client/rolodex/GroupV2Options;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListGroupsV2Request;
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Request;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Request$Builder;->group_options:Lcom/squareup/protos/client/rolodex/GroupV2Options;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/rolodex/ListGroupsV2Request;-><init>(Lcom/squareup/protos/client/rolodex/GroupV2Options;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListGroupsV2Request$Builder;->build()Lcom/squareup/protos/client/rolodex/ListGroupsV2Request;

    move-result-object v0

    return-object v0
.end method

.method public group_options(Lcom/squareup/protos/client/rolodex/GroupV2Options;)Lcom/squareup/protos/client/rolodex/ListGroupsV2Request$Builder;
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsV2Request$Builder;->group_options:Lcom/squareup/protos/client/rolodex/GroupV2Options;

    return-object p0
.end method
