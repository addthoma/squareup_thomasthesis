.class public final Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FrequentItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/FrequentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/FrequentItem;",
        "Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abbreviation:Ljava/lang/String;

.field public category_name:Ljava/lang/String;

.field public color:Lcom/squareup/protos/common/RGBAColor;

.field public display_name:Ljava/lang/String;

.field public image_url:Ljava/lang/String;

.field public purchase_count:Ljava/lang/Double;

.field public transaction_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public abbreviation(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->abbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/FrequentItem;
    .locals 10

    .line 263
    new-instance v9, Lcom/squareup/protos/client/rolodex/FrequentItem;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->display_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->purchase_count:Ljava/lang/Double;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->image_url:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->color:Lcom/squareup/protos/common/RGBAColor;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->category_name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->transaction_count:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->abbreviation:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/rolodex/FrequentItem;-><init>(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->build()Lcom/squareup/protos/client/rolodex/FrequentItem;

    move-result-object v0

    return-object v0
.end method

.method public category_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->category_name:Ljava/lang/String;

    return-object p0
.end method

.method public color(Lcom/squareup/protos/common/RGBAColor;)Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->color:Lcom/squareup/protos/common/RGBAColor;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public image_url(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->image_url:Ljava/lang/String;

    return-object p0
.end method

.method public purchase_count(Ljava/lang/Double;)Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->purchase_count:Ljava/lang/Double;

    return-object p0
.end method

.method public transaction_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/FrequentItem$Builder;->transaction_count:Ljava/lang/Integer;

    return-object p0
.end method
