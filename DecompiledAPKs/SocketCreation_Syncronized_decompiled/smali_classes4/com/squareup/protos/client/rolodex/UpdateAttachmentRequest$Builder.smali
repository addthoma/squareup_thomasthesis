.class public final Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateAttachmentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment:Lcom/squareup/protos/client/rolodex/Attachment;

.field public merchant_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attachment(Lcom/squareup/protos/client/rolodex/Attachment;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Attachment;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    move-result-object v0

    return-object v0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method
