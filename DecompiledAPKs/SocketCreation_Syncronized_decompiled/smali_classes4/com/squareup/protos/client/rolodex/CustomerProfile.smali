.class public final Lcom/squareup/protos/client/rolodex/CustomerProfile;
.super Lcom/squareup/wire/Message;
.source "CustomerProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/CustomerProfile$ProtoAdapter_CustomerProfile;,
        Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
        "Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COMPANY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_GIVEN_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_MASKED_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_MASKED_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_MEMO:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_PROVIDED_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NICKNAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_SURNAME:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        redacted = true
        tag = 0xb
    .end annotation
.end field

.field public final birthday:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        redacted = true
        tag = 0x7
    .end annotation
.end field

.field public final company_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x9
    .end annotation
.end field

.field public final given_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final masked_email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xc
    .end annotation
.end field

.field public final masked_phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xd
    .end annotation
.end field

.field public final memo:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x8
    .end annotation
.end field

.field public final merchant_provided_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final nickname:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xa
    .end annotation
.end field

.field public final surname:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$ProtoAdapter_CustomerProfile;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$ProtoAdapter_CustomerProfile;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15

    .line 166
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/client/rolodex/CustomerProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 173
    sget-object v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->version:Ljava/lang/String;

    .line 175
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    .line 176
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    .line 177
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    .line 178
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->nickname:Ljava/lang/String;

    .line 179
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    .line 180
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 181
    iput-object p8, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    .line 182
    iput-object p9, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    .line 183
    iput-object p10, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    .line 184
    iput-object p11, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 185
    iput-object p12, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    .line 186
    iput-object p13, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 212
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 213
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;

    .line 214
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->version:Ljava/lang/String;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->nickname:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->nickname:Ljava/lang/String;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    .line 227
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 232
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 234
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->version:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->nickname:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 248
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;
    .locals 2

    .line 191
    new-instance v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->version:Ljava/lang/String;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->merchant_provided_id:Ljava/lang/String;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name:Ljava/lang/String;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname:Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->nickname:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->nickname:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->company_name:Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->memo:Ljava/lang/String;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address:Ljava/lang/String;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->masked_email_address:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->masked_phone_number:Ljava/lang/String;

    .line 205
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->newBuilder()Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->version:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_provided_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", given_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", surname=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->nickname:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", nickname=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", company_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_6

    const-string v1, ", birthday=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", memo=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", email_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_a

    const-string v1, ", address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", masked_email_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", masked_phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomerProfile{"

    .line 269
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
