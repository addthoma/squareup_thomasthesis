.class public final Lcom/squareup/protos/client/bfd/cart/DisplayBanner;
.super Lcom/squareup/wire/Message;
.source "DisplayBanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bfd/cart/DisplayBanner$ProtoAdapter_DisplayBanner;,
        Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bfd/cart/DisplayBanner;",
        "Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayBanner;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ITEM_CLIENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICE:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final item_client_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final price:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final quantity:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$ProtoAdapter_DisplayBanner;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$ProtoAdapter_DisplayBanner;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->DEFAULT_QUANTITY:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 6

    .line 58
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->name:Ljava/lang/String;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->price:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->item_client_id:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->quantity:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 85
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->name:Ljava/lang/String;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->price:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->price:Ljava/lang/String;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->item_client_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->item_client_id:Ljava/lang/String;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->quantity:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->quantity:Ljava/lang/Integer;

    .line 90
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->price:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->item_client_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->quantity:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 102
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->name:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->price:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->price:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->item_client_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->item_client_id:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->quantity:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->quantity:Ljava/lang/Integer;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->newBuilder()Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->price:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->price:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->item_client_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", item_client_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->item_client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->quantity:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->quantity:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayBanner{"

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
