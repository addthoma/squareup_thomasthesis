.class final Lcom/squareup/protos/client/bfd/cart/Tax$ProtoAdapter_Tax;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bfd/cart/Tax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Tax"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bfd/cart/Tax;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 122
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bfd/cart/Tax;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bfd/cart/Tax;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 141
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;-><init>()V

    .line 142
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 143
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 148
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 146
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->amount(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/Tax$Builder;

    goto :goto_0

    .line 145
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/Tax$Builder;

    goto :goto_0

    .line 152
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 153
    invoke-virtual {v0}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->build()Lcom/squareup/protos/client/bfd/cart/Tax;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 120
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bfd/cart/Tax$ProtoAdapter_Tax;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bfd/cart/Tax;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bfd/cart/Tax;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 134
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bfd/cart/Tax;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 135
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bfd/cart/Tax;->amount:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 136
    invoke-virtual {p2}, Lcom/squareup/protos/client/bfd/cart/Tax;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 120
    check-cast p2, Lcom/squareup/protos/client/bfd/cart/Tax;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bfd/cart/Tax$ProtoAdapter_Tax;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bfd/cart/Tax;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bfd/cart/Tax;)I
    .locals 4

    .line 127
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bfd/cart/Tax;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bfd/cart/Tax;->amount:Ljava/lang/String;

    const/4 v3, 0x2

    .line 128
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/Tax;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 120
    check-cast p1, Lcom/squareup/protos/client/bfd/cart/Tax;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bfd/cart/Tax$ProtoAdapter_Tax;->encodedSize(Lcom/squareup/protos/client/bfd/cart/Tax;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bfd/cart/Tax;)Lcom/squareup/protos/client/bfd/cart/Tax;
    .locals 0

    .line 158
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/Tax;->newBuilder()Lcom/squareup/protos/client/bfd/cart/Tax$Builder;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 160
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->build()Lcom/squareup/protos/client/bfd/cart/Tax;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 120
    check-cast p1, Lcom/squareup/protos/client/bfd/cart/Tax;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bfd/cart/Tax$ProtoAdapter_Tax;->redact(Lcom/squareup/protos/client/bfd/cart/Tax;)Lcom/squareup/protos/client/bfd/cart/Tax;

    move-result-object p1

    return-object p1
.end method
