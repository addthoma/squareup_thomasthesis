.class public final enum Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;
.super Ljava/lang/Enum;
.source "InformationRequestDetail.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/InformationRequestDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/irf/InformationRequestDetail$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMPLETED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

.field public static final enum CREATED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

.field public static final enum DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

.field public static final enum REVIEWED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

.field public static final enum STARTED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 159
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 161
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    const/4 v2, 0x1

    const-string v3, "CREATED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->CREATED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 163
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    const/4 v3, 0x2

    const-string v4, "STARTED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->STARTED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 165
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    const/4 v4, 0x3

    const-string v5, "COMPLETED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->COMPLETED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 167
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    const/4 v5, 0x4

    const-string v6, "REVIEWED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->REVIEWED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 158
    sget-object v6, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->CREATED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->STARTED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->COMPLETED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->REVIEWED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->$VALUES:[Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 169
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 174
    iput p3, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 186
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->REVIEWED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object p0

    .line 185
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->COMPLETED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object p0

    .line 184
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->STARTED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object p0

    .line 183
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->CREATED:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object p0

    .line 182
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;
    .locals 1

    .line 158
    const-class v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;
    .locals 1

    .line 158
    sget-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->$VALUES:[Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 193
    iget v0, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->value:I

    return v0
.end method
