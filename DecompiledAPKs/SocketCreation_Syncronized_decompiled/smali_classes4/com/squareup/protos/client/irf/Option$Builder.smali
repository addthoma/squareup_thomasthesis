.class public final Lcom/squareup/protos/client/irf/Option$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Option.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Option;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/Option;",
        "Lcom/squareup/protos/client/irf/Option$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public option_label:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/irf/Option;
    .locals 4

    .line 115
    new-instance v0, Lcom/squareup/protos/client/irf/Option;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Option$Builder;->value:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/Option$Builder;->option_label:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/irf/Option;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Option$Builder;->build()Lcom/squareup/protos/client/irf/Option;

    move-result-object v0

    return-object v0
.end method

.method public option_label(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Option$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Option$Builder;->option_label:Ljava/lang/String;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Option$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Option$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
