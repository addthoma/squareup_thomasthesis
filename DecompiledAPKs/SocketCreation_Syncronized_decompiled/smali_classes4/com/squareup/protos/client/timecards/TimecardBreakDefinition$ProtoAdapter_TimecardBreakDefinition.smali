.class final Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TimecardBreakDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TimecardBreakDefinition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 235
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 262
    new-instance v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;-><init>()V

    .line 263
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 264
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 273
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 271
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->updated_at_timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    goto :goto_0

    .line 270
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    goto :goto_0

    .line 269
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->is_paid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    goto :goto_0

    .line 268
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->expected_duration_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    goto :goto_0

    .line 267
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->break_name(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    goto :goto_0

    .line 266
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    goto :goto_0

    .line 277
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 278
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->build()Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 233
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 251
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 252
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 253
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 254
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 255
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 256
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 257
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 233
    check-cast p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)I
    .locals 4

    .line 240
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 241
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 242
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 243
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 244
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x6

    .line 245
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 233
    check-cast p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;->encodedSize(Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;
    .locals 2

    .line 283
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->newBuilder()Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    move-result-object p1

    .line 284
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 285
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 286
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->build()Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 233
    check-cast p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;->redact(Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    move-result-object p1

    return-object p1
.end method
