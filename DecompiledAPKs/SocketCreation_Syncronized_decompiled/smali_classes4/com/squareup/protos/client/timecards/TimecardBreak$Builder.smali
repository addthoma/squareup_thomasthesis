.class public final Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TimecardBreak.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/TimecardBreak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/TimecardBreak;",
        "Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public break_name:Ljava/lang/String;

.field public end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

.field public expected_duration_seconds:Ljava/lang/Integer;

.field public is_paid:Ljava/lang/Boolean;

.field public merchant_token:Ljava/lang/String;

.field public minimum_duration_seconds:Ljava/lang/Integer;

.field public start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

.field public timecard_break_definition_token:Ljava/lang/String;

.field public timecard_token:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 250
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public break_name(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->break_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/timecards/TimecardBreak;
    .locals 13

    .line 335
    new-instance v12, Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->merchant_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v6, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_break_definition_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->is_paid:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->break_name:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->expected_duration_seconds:Ljava/lang/Integer;

    iget-object v10, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->minimum_duration_seconds:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/timecards/TimecardBreak;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 229
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->build()Lcom/squareup/protos/client/timecards/TimecardBreak;

    move-result-object v0

    return-object v0
.end method

.method public end_timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public expected_duration_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->expected_duration_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public is_paid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->is_paid:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public minimum_duration_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->minimum_duration_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public start_timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public timecard_break_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_break_definition_token:Ljava/lang/String;

    return-object p0
.end method

.method public timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_token:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
