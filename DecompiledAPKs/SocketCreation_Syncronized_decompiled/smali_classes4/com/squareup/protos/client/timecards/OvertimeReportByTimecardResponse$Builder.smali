.class public final Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OvertimeReportByTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public by_timecard:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;",
            ">;"
        }
    .end annotation
.end field

.field public calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

.field public next_cursor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 111
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->by_timecard:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;
    .locals 5

    .line 132
    new-instance v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->by_timecard:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->next_cursor:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;-><init>(Lcom/squareup/protos/client/timecards/CalculationTotal;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;

    move-result-object v0

    return-object v0
.end method

.method public by_timecard(Ljava/util/List;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;",
            ">;)",
            "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;"
        }
    .end annotation

    .line 120
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->by_timecard:Ljava/util/List;

    return-object p0
.end method

.method public calculation_total(Lcom/squareup/protos/client/timecards/CalculationTotal;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    return-object p0
.end method

.method public next_cursor(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$Builder;->next_cursor:Ljava/lang/String;

    return-object p0
.end method
