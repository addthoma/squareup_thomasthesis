.class final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DiagnosticReportRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 835
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 876
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;-><init>()V

    .line 877
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 878
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 901
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 899
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->reporter(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto :goto_0

    .line 893
    :pswitch_2
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->destinations:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 895
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 890
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->links:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 889
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->zip(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto :goto_0

    .line 888
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_generated_localtime_sec(Ljava/lang/Long;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto :goto_0

    .line 887
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_triggered_localtime_sec(Ljava/lang/Long;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto :goto_0

    .line 886
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->num_xmit_retries(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto :goto_0

    .line 885
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto :goto_0

    .line 884
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto/16 :goto_0

    .line 883
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->source(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto/16 :goto_0

    .line 882
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->device_metadata(Lcom/squareup/protos/client/tarkin/SquidProductManifest;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto/16 :goto_0

    .line 881
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->primary_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto/16 :goto_0

    .line 880
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->uuid(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    goto/16 :goto_0

    .line 905
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 906
    invoke-virtual {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 833
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 858
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 859
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 860
    sget-object v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 861
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 862
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 863
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 864
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 865
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 866
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 867
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 868
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 869
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 870
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 871
    invoke-virtual {p2}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 833
    check-cast p2, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;)I
    .locals 4

    .line 840
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    const/4 v3, 0x2

    .line 841
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    const/4 v3, 0x3

    .line 842
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    const/4 v3, 0x4

    .line 843
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    const/4 v3, 0x5

    .line 844
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    const/4 v3, 0x6

    .line 845
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 846
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    const/16 v3, 0x8

    .line 847
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    const/16 v3, 0x9

    .line 848
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    const/16 v3, 0xb

    .line 849
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 850
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    const/16 v3, 0xc

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 851
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    const/16 v3, 0xd

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    const/16 v3, 0xe

    .line 852
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 853
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 833
    check-cast p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;->encodedSize(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
    .locals 2

    .line 911
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    move-result-object p1

    .line 912
    iget-object v0, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    iput-object v0, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    .line 913
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    iput-object v0, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    .line 914
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->links:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 915
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 916
    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 833
    check-cast p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;->redact(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    move-result-object p1

    return-object p1
.end method
