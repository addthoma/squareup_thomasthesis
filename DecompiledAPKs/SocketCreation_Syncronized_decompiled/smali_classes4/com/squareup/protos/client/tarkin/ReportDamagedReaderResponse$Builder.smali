.class public final Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReportDamagedReaderResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;",
        "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/client/Status;

.field public warranty_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;
    .locals 4

    .line 128
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;->warranty_url:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;->build()Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public warranty_url(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse$Builder;->warranty_url:Ljava/lang/String;

    return-object p0
.end method
