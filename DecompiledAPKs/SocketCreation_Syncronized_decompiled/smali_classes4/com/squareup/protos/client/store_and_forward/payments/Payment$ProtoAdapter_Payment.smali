.class final Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Payment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/payments/Payment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Payment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/store_and_forward/payments/Payment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 351
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/store_and_forward/payments/Payment;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 384
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;-><init>()V

    .line 385
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 386
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 398
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 396
    :pswitch_0
    sget-object v3, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->auth_code(Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 395
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 394
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_last_four(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 393
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_brand(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 392
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 391
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->client_timestamp(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 390
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->encrypted_payload(Lokio/ByteString;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 389
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->bletchley_key_id(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 388
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    goto :goto_0

    .line 402
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 403
    invoke-virtual {v0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->build()Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 349
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/store_and_forward/payments/Payment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 370
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 371
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 372
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 373
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 374
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 375
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 376
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 377
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 378
    sget-object v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 379
    invoke-virtual {p2}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 349
    check-cast p2, Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/store_and_forward/payments/Payment;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/store_and_forward/payments/Payment;)I
    .locals 4

    .line 356
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 357
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    const/4 v3, 0x3

    .line 358
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    const/4 v3, 0x4

    .line 359
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 360
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    const/4 v3, 0x6

    .line 361
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    const/4 v3, 0x7

    .line 362
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    const/16 v3, 0x8

    .line 363
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    const/16 v3, 0x9

    .line 364
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 349
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;->encodedSize(Lcom/squareup/protos/client/store_and_forward/payments/Payment;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/store_and_forward/payments/Payment;)Lcom/squareup/protos/client/store_and_forward/payments/Payment;
    .locals 3

    .line 408
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->newBuilder()Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 409
    iput-object v0, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->encrypted_payload:Lokio/ByteString;

    .line 410
    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    iput-object v1, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 411
    :cond_0
    iput-object v0, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_brand:Ljava/lang/String;

    .line 412
    iput-object v0, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_last_four:Ljava/lang/String;

    .line 413
    iget-object v0, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    iput-object v0, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    .line 414
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 415
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->build()Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 349
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;->redact(Lcom/squareup/protos/client/store_and_forward/payments/Payment;)Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    move-result-object p1

    return-object p1
.end method
