.class final Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ProtoAdapter_BillEncryptionPayload;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BillEncryptionPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BillEncryptionPayload"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 479
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 506
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;-><init>()V

    .line 507
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 508
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 517
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 515
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->creator_credential(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    goto :goto_0

    .line 514
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Headers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Headers;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->headers(Lcom/squareup/protos/common/Headers;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    goto :goto_0

    .line 513
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->extra_tender_detail:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 512
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/CompleteBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->complete_bill_request(Lcom/squareup/protos/client/bills/CompleteBillRequest;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    goto :goto_0

    .line 511
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->add_tenders_request:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/AddTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 510
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    goto :goto_0

    .line 521
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 522
    invoke-virtual {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 477
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ProtoAdapter_BillEncryptionPayload;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 495
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->session_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 496
    sget-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->add_tenders_request:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 497
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 498
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->extra_tender_detail:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 499
    sget-object v0, Lcom/squareup/protos/common/Headers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->headers:Lcom/squareup/protos/common/Headers;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 500
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->creator_credential:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 501
    invoke-virtual {p2}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 477
    check-cast p2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ProtoAdapter_BillEncryptionPayload;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;)I
    .locals 4

    .line 484
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->session_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/AddTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 485
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->add_tenders_request:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    const/4 v3, 0x3

    .line 486
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 487
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->extra_tender_detail:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Headers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->headers:Lcom/squareup/protos/common/Headers;

    const/4 v3, 0x5

    .line 488
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->creator_credential:Ljava/lang/String;

    const/4 v3, 0x6

    .line 489
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 477
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ProtoAdapter_BillEncryptionPayload;->encodedSize(Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;
    .locals 3

    .line 527
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 528
    iput-object v0, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->session_token:Ljava/lang/String;

    .line 529
    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->add_tenders_request:Ljava/util/List;

    sget-object v2, Lcom/squareup/protos/client/bills/AddTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 530
    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iput-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    .line 531
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->extra_tender_detail:Ljava/util/List;

    sget-object v2, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 532
    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->headers:Lcom/squareup/protos/common/Headers;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/protos/common/Headers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->headers:Lcom/squareup/protos/common/Headers;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Headers;

    iput-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->headers:Lcom/squareup/protos/common/Headers;

    .line 533
    :cond_1
    iput-object v0, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->creator_credential:Ljava/lang/String;

    .line 534
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 535
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 477
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ProtoAdapter_BillEncryptionPayload;->redact(Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    move-result-object p1

    return-object p1
.end method
