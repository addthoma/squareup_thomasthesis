.class public final Lcom/squareup/protos/client/flipper/GetTicketResponse;
.super Lcom/squareup/wire/Message;
.source "GetTicketResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/flipper/GetTicketResponse$ProtoAdapter_GetTicketResponse;,
        Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/flipper/GetTicketResponse;",
        "Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/flipper/GetTicketResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MS_FRAME:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final ms_frame:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.flipper.SealedTicket#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/protos/client/flipper/GetTicketResponse$ProtoAdapter_GetTicketResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/GetTicketResponse$ProtoAdapter_GetTicketResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->DEFAULT_MS_FRAME:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SealedTicket;)V
    .locals 1

    .line 56
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/flipper/GetTicketResponse;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SealedTicket;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SealedTicket;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    .line 62
    iput-object p2, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 77
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/flipper/GetTicketResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 78
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/flipper/GetTicketResponse;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/GetTicketResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/GetTicketResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object p1, p1, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    .line 81
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/GetTicketResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/flipper/SealedTicket;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 91
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->ms_frame:Lokio/ByteString;

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/GetTicketResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/GetTicketResponse;->newBuilder()Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", ms_frame="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ms_frame:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    if-eqz v1, :cond_1

    const-string v1, ", new_ticket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetTicketResponse{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
