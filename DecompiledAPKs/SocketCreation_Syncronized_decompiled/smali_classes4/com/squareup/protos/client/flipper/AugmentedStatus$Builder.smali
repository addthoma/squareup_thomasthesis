.class public final Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AugmentedStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/AugmentedStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/flipper/AugmentedStatus;",
        "Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public failure_is_transient:Ljava/lang/Boolean;

.field public status:Lcom/squareup/protos/client/Status;

.field public ux_hint:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 132
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->ux_hint:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/flipper/AugmentedStatus;
    .locals 5

    .line 167
    new-instance v0, Lcom/squareup/protos/client/flipper/AugmentedStatus;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->failure_is_transient:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->ux_hint:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/flipper/AugmentedStatus;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->build()Lcom/squareup/protos/client/flipper/AugmentedStatus;

    move-result-object v0

    return-object v0
.end method

.method public failure_is_transient(Ljava/lang/Boolean;)Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->failure_is_transient:Ljava/lang/Boolean;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public ux_hint(Ljava/util/List;)Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;",
            ">;)",
            "Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;"
        }
    .end annotation

    .line 160
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$Builder;->ux_hint:Ljava/util/List;

    return-object p0
.end method
