.class public final Lcom/squareup/protos/bank_accounts/GbAccount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GbAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/GbAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bank_accounts/GbAccount;",
        "Lcom/squareup/protos/bank_accounts/GbAccount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

.field public sort_code:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_details(Lcom/squareup/protos/bank_accounts/GbAccountDetails;)Lcom/squareup/protos/bank_accounts/GbAccount$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bank_accounts/GbAccount;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/bank_accounts/GbAccount;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->sort_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bank_accounts/GbAccount;-><init>(Ljava/lang/String;Lcom/squareup/protos/bank_accounts/GbAccountDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/GbAccount;

    move-result-object v0

    return-object v0
.end method

.method public sort_code(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/GbAccount$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->sort_code:Ljava/lang/String;

    return-object p0
.end method
