.class public final enum Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;
.super Ljava/lang/Enum;
.source "ApplicationStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/external/business/models/ApplicationStatus$ProtoAdapter_ApplicationStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AS_DO_NOT_USE:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

.field public static final enum DECLINED:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

.field public static final enum IN_REVIEW:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

.field public static final enum PENDING:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 11
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    const/4 v1, 0x0

    const-string v2, "AS_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->AS_DO_NOT_USE:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 13
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    const/4 v2, 0x1

    const-string v3, "PENDING"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->PENDING:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 15
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    const/4 v3, 0x2

    const-string v4, "IN_REVIEW"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->IN_REVIEW:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 17
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    const/4 v4, 0x3

    const-string v5, "DECLINED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->DECLINED:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 10
    sget-object v5, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->AS_DO_NOT_USE:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->PENDING:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->IN_REVIEW:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->DECLINED:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->$VALUES:[Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 19
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus$ProtoAdapter_ApplicationStatus;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus$ProtoAdapter_ApplicationStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 35
    :cond_0
    sget-object p0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->DECLINED:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object p0

    .line 34
    :cond_1
    sget-object p0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->IN_REVIEW:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object p0

    .line 33
    :cond_2
    sget-object p0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->PENDING:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object p0

    .line 32
    :cond_3
    sget-object p0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->AS_DO_NOT_USE:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->$VALUES:[Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->value:I

    return v0
.end method
