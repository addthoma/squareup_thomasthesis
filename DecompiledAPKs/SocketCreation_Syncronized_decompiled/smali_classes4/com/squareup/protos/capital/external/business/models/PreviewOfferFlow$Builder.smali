.class public final Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreviewOfferFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public offer_flow_id:Ljava/lang/String;

.field public review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

.field public status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->offer_flow_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    iget-object v3, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;-><init>(Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;Lcom/squareup/protos/capital/external/business/models/ReviewType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    move-result-object v0

    return-object v0
.end method

.method public offer_flow_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->offer_flow_id:Ljava/lang/String;

    return-object p0
.end method

.method public review_type(Lcom/squareup/protos/capital/external/business/models/ReviewType;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    return-object p0
.end method
