.class public final Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchCustomersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public partner_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 83
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->partner_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->partner_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->build()Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    move-result-object v0

    return-object v0
.end method

.method public partner_ids(Ljava/util/List;)Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;"
        }
    .end annotation

    .line 90
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 91
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->partner_ids:Ljava/util/List;

    return-object p0
.end method
