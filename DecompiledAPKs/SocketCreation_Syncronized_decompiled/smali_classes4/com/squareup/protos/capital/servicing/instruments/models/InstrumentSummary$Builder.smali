.class public final Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstrumentSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
        "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

.field public card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

.field public id:Ljava/lang/String;

.field public is_active:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bank_account_summary(Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    const/4 p1, 0x0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;
    .locals 7

    .line 180
    new-instance v6, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->is_active:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;Lcom/squareup/protos/simple_instrument_store/api/CardSummary;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->build()Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    move-result-object v0

    return-object v0
.end method

.method public card_summary(Lcom/squareup/protos/simple_instrument_store/api/CardSummary;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    const/4 p1, 0x0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public is_active(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->is_active:Ljava/lang/Boolean;

    return-object p0
.end method
