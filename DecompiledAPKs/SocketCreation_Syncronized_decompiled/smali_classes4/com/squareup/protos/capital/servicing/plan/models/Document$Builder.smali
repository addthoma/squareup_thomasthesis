.class public final Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Document.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Document;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Document;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Ljava/lang/String;

.field public type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Document;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Document;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->created_at:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->url:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/capital/servicing/plan/models/Document;-><init>(Ljava/lang/String;Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Document;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;)Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->type:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$Builder;->url:Ljava/lang/String;

    return-object p0
.end method
