.class public final Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RisaDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;",
        "Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    move-result-object v0

    return-object v0
.end method
