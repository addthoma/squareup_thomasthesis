.class final Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status$ProtoAdapter_Status;
.super Lcom/squareup/wire/EnumAdapter;
.source "ScheduledPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 352
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;
    .locals 0

    .line 357
    invoke-static {p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 350
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status$ProtoAdapter_Status;->fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    move-result-object p1

    return-object p1
.end method
