.class public final Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Plan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public reasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 882
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 883
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->reasons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;
    .locals 4

    .line 899
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->reasons:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 877
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    move-result-object v0

    return-object v0
.end method

.method public reasons(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;"
        }
    .end annotation

    .line 892
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 893
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->reasons:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;
    .locals 0

    .line 887
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0
.end method
