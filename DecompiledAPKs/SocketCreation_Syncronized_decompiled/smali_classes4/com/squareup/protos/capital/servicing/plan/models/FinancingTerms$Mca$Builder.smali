.class public final Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FinancingTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;",
        "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public deposit_money:Lcom/squareup/protos/common/Money;

.field public fee_amount_bps:Ljava/lang/String;

.field public fee_money:Lcom/squareup/protos/common/Money;

.field public financed_money:Lcom/squareup/protos/common/Money;

.field public outstanding_balance_money:Lcom/squareup/protos/common/Money;

.field public total_owed_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 538
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;
    .locals 9

    .line 593
    new-instance v8, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->fee_amount_bps:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 525
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    move-result-object v0

    return-object v0
.end method

.method public deposit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    .locals 0

    .line 563
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public fee_amount_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    .locals 0

    .line 587
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->fee_amount_bps:Ljava/lang/String;

    return-object p0
.end method

.method public fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    .locals 0

    .line 553
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public financed_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    .locals 0

    .line 545
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public outstanding_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    .locals 0

    .line 571
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_owed_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    .locals 0

    .line 579
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
