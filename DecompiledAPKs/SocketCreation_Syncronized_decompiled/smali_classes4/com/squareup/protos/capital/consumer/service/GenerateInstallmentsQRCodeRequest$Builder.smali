.class public final Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GenerateInstallmentsQRCodeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public idempotence_token:Ljava/lang/String;

.field public image_height_px:Ljava/lang/Integer;

.field public image_width_px:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 129
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;
    .locals 7

    .line 157
    new-instance v6, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    iget-object v1, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->idempotence_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->image_width_px:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->image_height_px:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->build()Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->idempotence_token:Ljava/lang/String;

    return-object p0
.end method

.method public image_height_px(Ljava/lang/Integer;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->image_height_px:Ljava/lang/Integer;

    return-object p0
.end method

.method public image_width_px(Ljava/lang/Integer;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->image_width_px:Ljava/lang/Integer;

    return-object p0
.end method
