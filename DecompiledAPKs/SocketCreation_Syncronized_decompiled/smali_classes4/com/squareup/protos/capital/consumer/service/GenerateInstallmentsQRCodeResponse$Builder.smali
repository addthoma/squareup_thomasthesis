.class public final Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GenerateInstallmentsQRCodeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public qr_code_image_bytes:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;
    .locals 3

    .line 96
    new-instance v0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;

    iget-object v1, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse$Builder;->qr_code_image_bytes:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse$Builder;->build()Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;

    move-result-object v0

    return-object v0
.end method

.method public qr_code_image_bytes(Lokio/ByteString;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse$Builder;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse$Builder;->qr_code_image_bytes:Lokio/ByteString;

    return-object p0
.end method
