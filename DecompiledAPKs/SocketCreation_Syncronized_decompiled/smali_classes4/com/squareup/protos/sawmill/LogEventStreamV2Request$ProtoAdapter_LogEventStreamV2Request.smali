.class final Lcom/squareup/protos/sawmill/LogEventStreamV2Request$ProtoAdapter_LogEventStreamV2Request;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LogEventStreamV2Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/sawmill/LogEventStreamV2Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LogEventStreamV2Request"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/sawmill/LogEventStreamV2Request;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 119
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/sawmill/LogEventStreamV2Request;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    new-instance v0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;-><init>()V

    .line 139
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 140
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 145
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 143
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->sync(Ljava/lang/Boolean;)Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;

    goto :goto_0

    .line 142
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->events:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/sawmill/EventstreamV2Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 150
    invoke-virtual {v0}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->build()Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 117
    invoke-virtual {p0, p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$ProtoAdapter_LogEventStreamV2Request;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/sawmill/LogEventStreamV2Request;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    sget-object v0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;->events:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 132
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;->sync:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 133
    invoke-virtual {p2}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 117
    check-cast p2, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$ProtoAdapter_LogEventStreamV2Request;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/sawmill/LogEventStreamV2Request;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/sawmill/LogEventStreamV2Request;)I
    .locals 4

    .line 124
    sget-object v0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;->events:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;->sync:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 125
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    invoke-virtual {p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 117
    check-cast p1, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$ProtoAdapter_LogEventStreamV2Request;->encodedSize(Lcom/squareup/protos/sawmill/LogEventStreamV2Request;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/sawmill/LogEventStreamV2Request;)Lcom/squareup/protos/sawmill/LogEventStreamV2Request;
    .locals 2

    .line 155
    invoke-virtual {p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;->newBuilder()Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;

    move-result-object p1

    .line 156
    iget-object v0, p1, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->events:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 157
    invoke-virtual {p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 158
    invoke-virtual {p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->build()Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 117
    check-cast p1, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$ProtoAdapter_LogEventStreamV2Request;->redact(Lcom/squareup/protos/sawmill/LogEventStreamV2Request;)Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    move-result-object p1

    return-object p1
.end method
