.class public final enum Lcom/squareup/protos/precog/ProductIntent;
.super Ljava/lang/Enum;
.source "ProductIntent.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/precog/ProductIntent$ProtoAdapter_ProductIntent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/precog/ProductIntent;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/precog/ProductIntent;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/precog/ProductIntent;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PRODUCT_INTENT_APPOINTMENTS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_CAPITAL:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_DEVELOPERS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_ECOMMERCE:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_EMPLOYEES:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_INVOICES:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_MOBILE_POS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_PARTNER_POS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_PAYROLL:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_POS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_RESTAURANTS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_RESTAURANT_POS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_RETAIL:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_RETAIL_POS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_SERVICES_POS:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_UNKNOWN:Lcom/squareup/protos/precog/ProductIntent;

.field public static final enum PRODUCT_INTENT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v1, 0x0

    const-string v2, "PRODUCT_INTENT_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_UNKNOWN:Lcom/squareup/protos/precog/ProductIntent;

    .line 13
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v2, 0x1

    const-string v3, "PRODUCT_INTENT_APPOINTMENTS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_APPOINTMENTS:Lcom/squareup/protos/precog/ProductIntent;

    .line 15
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v3, 0x2

    const-string v4, "PRODUCT_INTENT_ECOMMERCE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_ECOMMERCE:Lcom/squareup/protos/precog/ProductIntent;

    .line 17
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v4, 0x3

    const-string v5, "PRODUCT_INTENT_EMPLOYEES"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_EMPLOYEES:Lcom/squareup/protos/precog/ProductIntent;

    .line 19
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v5, 0x4

    const-string v6, "PRODUCT_INTENT_INVOICES"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_INVOICES:Lcom/squareup/protos/precog/ProductIntent;

    .line 21
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v6, 0x5

    const-string v7, "PRODUCT_INTENT_MOBILE_POS"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_MOBILE_POS:Lcom/squareup/protos/precog/ProductIntent;

    .line 23
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v7, 0x6

    const-string v8, "PRODUCT_INTENT_PARTNER_POS"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_PARTNER_POS:Lcom/squareup/protos/precog/ProductIntent;

    .line 25
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/4 v8, 0x7

    const-string v9, "PRODUCT_INTENT_PAYROLL"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_PAYROLL:Lcom/squareup/protos/precog/ProductIntent;

    .line 27
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/16 v9, 0x8

    const-string v10, "PRODUCT_INTENT_POS"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_POS:Lcom/squareup/protos/precog/ProductIntent;

    .line 32
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/16 v10, 0x9

    const-string v11, "PRODUCT_INTENT_RESTAURANT_POS"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RESTAURANT_POS:Lcom/squareup/protos/precog/ProductIntent;

    .line 37
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/16 v11, 0xa

    const-string v12, "PRODUCT_INTENT_RETAIL_POS"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RETAIL_POS:Lcom/squareup/protos/precog/ProductIntent;

    .line 39
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/16 v12, 0xb

    const-string v13, "PRODUCT_INTENT_SERVICES_POS"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_SERVICES_POS:Lcom/squareup/protos/precog/ProductIntent;

    .line 41
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/16 v13, 0xc

    const-string v14, "PRODUCT_INTENT_TERMINAL"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;

    .line 43
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/16 v14, 0xd

    const-string v15, "PRODUCT_INTENT_VIRTUAL_TERMINAL"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;

    .line 45
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const/16 v15, 0xe

    const-string v14, "PRODUCT_INTENT_CAPITAL"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_CAPITAL:Lcom/squareup/protos/precog/ProductIntent;

    .line 47
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const-string v14, "PRODUCT_INTENT_DEVELOPERS"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_DEVELOPERS:Lcom/squareup/protos/precog/ProductIntent;

    .line 49
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const-string v13, "PRODUCT_INTENT_RESTAURANTS"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RESTAURANTS:Lcom/squareup/protos/precog/ProductIntent;

    .line 51
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent;

    const-string v13, "PRODUCT_INTENT_RETAIL"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/ProductIntent;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RETAIL:Lcom/squareup/protos/precog/ProductIntent;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/squareup/protos/precog/ProductIntent;

    .line 10
    sget-object v13, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_UNKNOWN:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_APPOINTMENTS:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_ECOMMERCE:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_EMPLOYEES:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_INVOICES:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_MOBILE_POS:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_PARTNER_POS:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_PAYROLL:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_POS:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RESTAURANT_POS:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RETAIL_POS:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_SERVICES_POS:Lcom/squareup/protos/precog/ProductIntent;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_CAPITAL:Lcom/squareup/protos/precog/ProductIntent;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_DEVELOPERS:Lcom/squareup/protos/precog/ProductIntent;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RESTAURANTS:Lcom/squareup/protos/precog/ProductIntent;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RETAIL:Lcom/squareup/protos/precog/ProductIntent;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->$VALUES:[Lcom/squareup/protos/precog/ProductIntent;

    .line 53
    new-instance v0, Lcom/squareup/protos/precog/ProductIntent$ProtoAdapter_ProductIntent;

    invoke-direct {v0}, Lcom/squareup/protos/precog/ProductIntent$ProtoAdapter_ProductIntent;-><init>()V

    sput-object v0, Lcom/squareup/protos/precog/ProductIntent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput p3, p0, Lcom/squareup/protos/precog/ProductIntent;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/precog/ProductIntent;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 83
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RETAIL:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 82
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RESTAURANTS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 81
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_DEVELOPERS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 80
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_CAPITAL:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 79
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 78
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_TERMINAL:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 77
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_SERVICES_POS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 76
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RETAIL_POS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 75
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_RESTAURANT_POS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 74
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_POS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 73
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_PAYROLL:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 72
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_PARTNER_POS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 71
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_MOBILE_POS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 70
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_INVOICES:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 69
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_EMPLOYEES:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 68
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_ECOMMERCE:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 67
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_APPOINTMENTS:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    .line 66
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_UNKNOWN:Lcom/squareup/protos/precog/ProductIntent;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/precog/ProductIntent;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/precog/ProductIntent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/precog/ProductIntent;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/precog/ProductIntent;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/precog/ProductIntent;->$VALUES:[Lcom/squareup/protos/precog/ProductIntent;

    invoke-virtual {v0}, [Lcom/squareup/protos/precog/ProductIntent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/precog/ProductIntent;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 90
    iget v0, p0, Lcom/squareup/protos/precog/ProductIntent;->value:I

    return v0
.end method
