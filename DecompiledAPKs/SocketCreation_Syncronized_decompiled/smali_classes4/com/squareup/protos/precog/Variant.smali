.class public final enum Lcom/squareup/protos/precog/Variant;
.super Ljava/lang/Enum;
.source "Variant.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/precog/Variant$ProtoAdapter_Variant;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/precog/Variant;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/precog/Variant;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/precog/Variant;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum VARIANT_APPOINTMENTS:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_CAVIAR:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_DEVELOPERS:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_INVOICES:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_ONLINE_STORE:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_PAYROLL:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_POS:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_RESTAURANTS:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_RETAIL:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_SQUARE_REGISTER:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_UNKNOWN:Lcom/squareup/protos/precog/Variant;

.field public static final enum VARIANT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/Variant;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 11
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v1, 0x0

    const-string v2, "VARIANT_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_UNKNOWN:Lcom/squareup/protos/precog/Variant;

    .line 13
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v2, 0x1

    const-string v3, "VARIANT_APPOINTMENTS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_APPOINTMENTS:Lcom/squareup/protos/precog/Variant;

    .line 15
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v3, 0x2

    const-string v4, "VARIANT_CAVIAR"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_CAVIAR:Lcom/squareup/protos/precog/Variant;

    .line 17
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v4, 0x3

    const-string v5, "VARIANT_DEVELOPERS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_DEVELOPERS:Lcom/squareup/protos/precog/Variant;

    .line 19
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v5, 0x4

    const-string v6, "VARIANT_INVOICES"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_INVOICES:Lcom/squareup/protos/precog/Variant;

    .line 21
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v6, 0x5

    const-string v7, "VARIANT_ONLINE_STORE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_ONLINE_STORE:Lcom/squareup/protos/precog/Variant;

    .line 23
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v7, 0x6

    const-string v8, "VARIANT_PAYROLL"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_PAYROLL:Lcom/squareup/protos/precog/Variant;

    .line 25
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/4 v8, 0x7

    const-string v9, "VARIANT_POS"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_POS:Lcom/squareup/protos/precog/Variant;

    .line 27
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/16 v9, 0x8

    const-string v10, "VARIANT_RESTAURANTS"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_RESTAURANTS:Lcom/squareup/protos/precog/Variant;

    .line 29
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/16 v10, 0x9

    const-string v11, "VARIANT_RETAIL"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_RETAIL:Lcom/squareup/protos/precog/Variant;

    .line 34
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/16 v11, 0xa

    const-string v12, "VARIANT_SQUARE_REGISTER"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_SQUARE_REGISTER:Lcom/squareup/protos/precog/Variant;

    .line 36
    new-instance v0, Lcom/squareup/protos/precog/Variant;

    const/16 v12, 0xb

    const-string v13, "VARIANT_VIRTUAL_TERMINAL"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/precog/Variant;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/Variant;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/protos/precog/Variant;

    .line 10
    sget-object v13, Lcom/squareup/protos/precog/Variant;->VARIANT_UNKNOWN:Lcom/squareup/protos/precog/Variant;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_APPOINTMENTS:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_CAVIAR:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_DEVELOPERS:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_INVOICES:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_ONLINE_STORE:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_PAYROLL:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_POS:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_RESTAURANTS:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_RETAIL:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_SQUARE_REGISTER:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/precog/Variant;->VARIANT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/Variant;

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/protos/precog/Variant;->$VALUES:[Lcom/squareup/protos/precog/Variant;

    .line 38
    new-instance v0, Lcom/squareup/protos/precog/Variant$ProtoAdapter_Variant;

    invoke-direct {v0}, Lcom/squareup/protos/precog/Variant$ProtoAdapter_Variant;-><init>()V

    sput-object v0, Lcom/squareup/protos/precog/Variant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/squareup/protos/precog/Variant;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/precog/Variant;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 62
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 61
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_SQUARE_REGISTER:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 60
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_RETAIL:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 59
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_RESTAURANTS:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 58
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_POS:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 57
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_PAYROLL:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 56
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_ONLINE_STORE:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 55
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_INVOICES:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 54
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_DEVELOPERS:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 53
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_CAVIAR:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 52
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_APPOINTMENTS:Lcom/squareup/protos/precog/Variant;

    return-object p0

    .line 51
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/precog/Variant;->VARIANT_UNKNOWN:Lcom/squareup/protos/precog/Variant;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/precog/Variant;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/precog/Variant;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/precog/Variant;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/precog/Variant;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/precog/Variant;->$VALUES:[Lcom/squareup/protos/precog/Variant;

    invoke-virtual {v0}, [Lcom/squareup/protos/precog/Variant;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/precog/Variant;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 69
    iget v0, p0, Lcom/squareup/protos/precog/Variant;->value:I

    return v0
.end method
