.class public final Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetSignalsByMerchantTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;",
        "Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_signals:Lcom/squareup/protos/precog/MerchantSignals;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;
    .locals 3

    .line 88
    new-instance v0, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;

    iget-object v1, p0, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse$Builder;->merchant_signals:Lcom/squareup/protos/precog/MerchantSignals;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;-><init>(Lcom/squareup/protos/precog/MerchantSignals;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse$Builder;->build()Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public merchant_signals(Lcom/squareup/protos/precog/MerchantSignals;)Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse$Builder;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse$Builder;->merchant_signals:Lcom/squareup/protos/precog/MerchantSignals;

    return-object p0
.end method
