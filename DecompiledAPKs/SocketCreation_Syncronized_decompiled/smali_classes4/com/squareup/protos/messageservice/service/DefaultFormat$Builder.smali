.class public final Lcom/squareup/protos/messageservice/service/DefaultFormat$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DefaultFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/DefaultFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messageservice/service/DefaultFormat;",
        "Lcom/squareup/protos/messageservice/service/DefaultFormat$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public body:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public body(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/DefaultFormat$Builder;
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/DefaultFormat$Builder;->body:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/messageservice/service/DefaultFormat;
    .locals 3

    .line 89
    new-instance v0, Lcom/squareup/protos/messageservice/service/DefaultFormat;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/DefaultFormat$Builder;->body:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/messageservice/service/DefaultFormat;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/DefaultFormat$Builder;->build()Lcom/squareup/protos/messageservice/service/DefaultFormat;

    move-result-object v0

    return-object v0
.end method
