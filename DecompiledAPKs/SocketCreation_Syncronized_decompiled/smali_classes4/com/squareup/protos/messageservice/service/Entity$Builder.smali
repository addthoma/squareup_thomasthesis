.class public final Lcom/squareup/protos/messageservice/service/Entity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/Entity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messageservice/service/Entity;",
        "Lcom/squareup/protos/messageservice/service/Entity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public token:Ljava/lang/String;

.field public type:Lcom/squareup/protos/messageservice/service/EntityType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/messageservice/service/Entity;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/messageservice/service/Entity;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/Entity$Builder;->type:Lcom/squareup/protos/messageservice/service/EntityType;

    iget-object v2, p0, Lcom/squareup/protos/messageservice/service/Entity$Builder;->token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/messageservice/service/Entity;-><init>(Lcom/squareup/protos/messageservice/service/EntityType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->build()Lcom/squareup/protos/messageservice/service/Entity;

    move-result-object v0

    return-object v0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/Entity$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/Entity$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/messageservice/service/EntityType;)Lcom/squareup/protos/messageservice/service/Entity$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/Entity$Builder;->type:Lcom/squareup/protos/messageservice/service/EntityType;

    return-object p0
.end method
