.class public final enum Lcom/squareup/protos/messageservice/service/Action;
.super Ljava/lang/Enum;
.source "Action.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/messageservice/service/Action$ProtoAdapter_Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/messageservice/service/Action;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/messageservice/service/Action;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/messageservice/service/Action;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CLICK:Lcom/squareup/protos/messageservice/service/Action;

.field public static final enum DISMISS:Lcom/squareup/protos/messageservice/service/Action;

.field public static final enum DO_NOT_USE_ACTION:Lcom/squareup/protos/messageservice/service/Action;

.field public static final enum SENT:Lcom/squareup/protos/messageservice/service/Action;

.field public static final enum VIEW:Lcom/squareup/protos/messageservice/service/Action;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 14
    new-instance v0, Lcom/squareup/protos/messageservice/service/Action;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE_ACTION"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/messageservice/service/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Action;->DO_NOT_USE_ACTION:Lcom/squareup/protos/messageservice/service/Action;

    .line 19
    new-instance v0, Lcom/squareup/protos/messageservice/service/Action;

    const/4 v2, 0x1

    const-string v3, "VIEW"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/messageservice/service/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Action;->VIEW:Lcom/squareup/protos/messageservice/service/Action;

    .line 24
    new-instance v0, Lcom/squareup/protos/messageservice/service/Action;

    const/4 v3, 0x2

    const-string v4, "CLICK"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/messageservice/service/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Action;->CLICK:Lcom/squareup/protos/messageservice/service/Action;

    .line 29
    new-instance v0, Lcom/squareup/protos/messageservice/service/Action;

    const/4 v4, 0x3

    const-string v5, "DISMISS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/messageservice/service/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Action;->DISMISS:Lcom/squareup/protos/messageservice/service/Action;

    .line 34
    new-instance v0, Lcom/squareup/protos/messageservice/service/Action;

    const/4 v5, 0x4

    const-string v6, "SENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/messageservice/service/Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Action;->SENT:Lcom/squareup/protos/messageservice/service/Action;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/messageservice/service/Action;

    .line 10
    sget-object v6, Lcom/squareup/protos/messageservice/service/Action;->DO_NOT_USE_ACTION:Lcom/squareup/protos/messageservice/service/Action;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/Action;->VIEW:Lcom/squareup/protos/messageservice/service/Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/messageservice/service/Action;->CLICK:Lcom/squareup/protos/messageservice/service/Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/messageservice/service/Action;->DISMISS:Lcom/squareup/protos/messageservice/service/Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/messageservice/service/Action;->SENT:Lcom/squareup/protos/messageservice/service/Action;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/messageservice/service/Action;->$VALUES:[Lcom/squareup/protos/messageservice/service/Action;

    .line 36
    new-instance v0, Lcom/squareup/protos/messageservice/service/Action$ProtoAdapter_Action;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/Action$ProtoAdapter_Action;-><init>()V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Action;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/squareup/protos/messageservice/service/Action;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/messageservice/service/Action;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 53
    :cond_0
    sget-object p0, Lcom/squareup/protos/messageservice/service/Action;->SENT:Lcom/squareup/protos/messageservice/service/Action;

    return-object p0

    .line 52
    :cond_1
    sget-object p0, Lcom/squareup/protos/messageservice/service/Action;->DISMISS:Lcom/squareup/protos/messageservice/service/Action;

    return-object p0

    .line 51
    :cond_2
    sget-object p0, Lcom/squareup/protos/messageservice/service/Action;->CLICK:Lcom/squareup/protos/messageservice/service/Action;

    return-object p0

    .line 50
    :cond_3
    sget-object p0, Lcom/squareup/protos/messageservice/service/Action;->VIEW:Lcom/squareup/protos/messageservice/service/Action;

    return-object p0

    .line 49
    :cond_4
    sget-object p0, Lcom/squareup/protos/messageservice/service/Action;->DO_NOT_USE_ACTION:Lcom/squareup/protos/messageservice/service/Action;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/Action;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/messageservice/service/Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/messageservice/service/Action;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/messageservice/service/Action;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/messageservice/service/Action;->$VALUES:[Lcom/squareup/protos/messageservice/service/Action;

    invoke-virtual {v0}, [Lcom/squareup/protos/messageservice/service/Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/messageservice/service/Action;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/squareup/protos/messageservice/service/Action;->value:I

    return v0
.end method
