.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;
.super Lcom/squareup/wire/Message;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ThirdPartyFeeEntity"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final unit_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1883
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$ProtoAdapter_ThirdPartyFeeEntity;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1921
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 1926
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1927
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    .line 1928
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    .line 1929
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1945
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1946
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    .line 1947
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    .line 1948
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    .line 1949
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    .line 1950
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1955
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 1957
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1958
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1959
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1960
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 1961
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;
    .locals 2

    .line 1934
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;-><init>()V

    .line 1935
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->unit_token:Ljava/lang/String;

    .line 1936
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->display_name:Ljava/lang/String;

    .line 1937
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->unit_name:Ljava/lang/String;

    .line 1938
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1882
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1968
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1969
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1970
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1971
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", unit_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;->unit_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ThirdPartyFeeEntity{"

    .line 1972
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
