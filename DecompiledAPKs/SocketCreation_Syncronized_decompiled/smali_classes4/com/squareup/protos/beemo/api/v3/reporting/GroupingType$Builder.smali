.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupingType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

.field public reporting_group_config_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;
    .locals 5

    .line 145
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/lang/String;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    move-result-object v0

    return-object v0
.end method

.method public group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0
.end method

.method public reporting_group_config_details(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    const/4 p1, 0x0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_id:Ljava/lang/String;

    return-object p0
.end method

.method public reporting_group_config_id(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_id:Ljava/lang/String;

    const/4 p1, 0x0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType$Builder;->reporting_group_config_details:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    return-object p0
.end method
