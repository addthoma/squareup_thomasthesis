.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
.super Lcom/squareup/wire/Message;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate$Details#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate$Details#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate$Details#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)V
    .locals 1

    .line 56
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 62
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 63
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 79
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 80
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 82
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 83
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 84
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 89
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 95
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v1, :cond_0

    const-string v1, ", sales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v1, :cond_1

    const-string v1, ", refunds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v1, :cond_2

    const-string v1, ", net="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Aggregate{"

    .line 106
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
