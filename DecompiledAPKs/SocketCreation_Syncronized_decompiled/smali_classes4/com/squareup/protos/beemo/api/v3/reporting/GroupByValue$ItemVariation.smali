.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ItemVariation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final DEFAULT_SKU:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.TranslationType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.NameOrTranslationType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final sku:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2273
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$ProtoAdapter_ItemVariation;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2279
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->DEFAULT_DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;)V
    .locals 6

    .line 2313
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V
    .locals 1

    .line 2319
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2320
    invoke-static {p3, p4}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p5

    const/4 v0, 0x1

    if-gt p5, v0, :cond_0

    .line 2323
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 2324
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    .line 2325
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 2326
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-void

    .line 2321
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of defined_measurement_unit, default_measurement_unit may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2343
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2344
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 2345
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 2346
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    .line 2347
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 2348
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 2349
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2354
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 2356
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2357
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2358
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2359
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2360
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 2361
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
    .locals 2

    .line 2331
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;-><init>()V

    .line 2332
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 2333
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->sku:Ljava/lang/String;

    .line 2334
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 2335
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 2336
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2272
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2369
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v1, :cond_0

    const-string v1, ", name_or_translation_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2370
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", sku="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->sku:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2371
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v1, :cond_2

    const-string v1, ", defined_measurement_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2372
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eqz v1, :cond_3

    const-string v1, ", default_measurement_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemVariation{"

    .line 2373
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
