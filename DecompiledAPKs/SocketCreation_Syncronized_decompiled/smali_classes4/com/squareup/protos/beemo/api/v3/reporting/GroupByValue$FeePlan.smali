.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeePlan"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$ProtoAdapter_FeePlan;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISCOUNT_BASIS_POINTS:Ljava/lang/Long;

.field public static final DEFAULT_INTERCHANGE_CENTS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final discount_basis_points:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final interchange_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 7231
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$ProtoAdapter_FeePlan;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$ProtoAdapter_FeePlan;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 7235
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->DEFAULT_DISCOUNT_BASIS_POINTS:Ljava/lang/Long;

    .line 7237
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->DEFAULT_INTERCHANGE_CENTS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .line 7258
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 7262
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7263
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->discount_basis_points:Ljava/lang/Long;

    .line 7264
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->interchange_cents:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 7279
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 7280
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    .line 7281
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->discount_basis_points:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->discount_basis_points:Ljava/lang/Long;

    .line 7282
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->interchange_cents:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->interchange_cents:Ljava/lang/Long;

    .line 7283
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 7288
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 7290
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 7291
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->discount_basis_points:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7292
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->interchange_cents:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 7293
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;
    .locals 2

    .line 7269
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;-><init>()V

    .line 7270
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->discount_basis_points:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->discount_basis_points:Ljava/lang/Long;

    .line 7271
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->interchange_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->interchange_cents:Ljava/lang/Long;

    .line 7272
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7230
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 7300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7301
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->discount_basis_points:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", discount_basis_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->discount_basis_points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7302
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->interchange_cents:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", interchange_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->interchange_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeePlan{"

    .line 7303
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
