.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;
.super Lcom/squareup/wire/Message;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CostOfGoodsAmounts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COGS_ITEMS_COUNT:Ljava/lang/String; = "0"

.field public static final DEFAULT_PROFIT_MARGIN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final average_cost_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final average_revenue_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final cogs_items_count:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final profit_margin:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final total_cost_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final total_profit_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final total_revenue_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2140
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .line 2219
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 2225
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2226
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    .line 2227
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    .line 2228
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    .line 2229
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2230
    iput-object p5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2231
    iput-object p6, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    .line 2232
    iput-object p7, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2252
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2253
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 2254
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    .line 2255
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    .line 2256
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    .line 2257
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2258
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    .line 2260
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    .line 2261
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2266
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 2268
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2269
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2270
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2271
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2272
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2273
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2274
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2275
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 2276
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;
    .locals 2

    .line 2237
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;-><init>()V

    .line 2238
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_cost_money:Lcom/squareup/protos/common/Money;

    .line 2239
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_cost_money:Lcom/squareup/protos/common/Money;

    .line 2240
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_profit_money:Lcom/squareup/protos/common/Money;

    .line 2241
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2242
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2243
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->profit_margin:Ljava/lang/String;

    .line 2244
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->cogs_items_count:Ljava/lang/String;

    .line 2245
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2139
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2284
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", total_cost_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2285
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", average_cost_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2286
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", total_profit_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2287
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", total_revenue_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2288
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", average_revenue_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2289
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", profit_margin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2290
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", cogs_items_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CostOfGoodsAmounts{"

    .line 2291
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
