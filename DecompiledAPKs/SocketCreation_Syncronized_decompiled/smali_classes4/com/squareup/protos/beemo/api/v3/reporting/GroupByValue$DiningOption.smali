.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiningOption"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$ProtoAdapter_DiningOption;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DINING_OPTION_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final dining_option_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.NameOrTranslationType#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3823
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$ProtoAdapter_DiningOption;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$ProtoAdapter_DiningOption;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)V
    .locals 1

    .line 3850
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;-><init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V
    .locals 1

    .line 3855
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 3856
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->dining_option_id:Ljava/lang/String;

    .line 3857
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3872
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3873
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    .line 3874
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->dining_option_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->dining_option_id:Ljava/lang/String;

    .line 3875
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 3876
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 3881
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 3883
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 3884
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->dining_option_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3885
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 3886
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;
    .locals 2

    .line 3862
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;-><init>()V

    .line 3863
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->dining_option_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->dining_option_id:Ljava/lang/String;

    .line 3864
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 3865
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3822
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3893
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3894
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->dining_option_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", dining_option_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->dining_option_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3895
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v1, :cond_1

    const-string v1, ", name_or_translation_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DiningOption{"

    .line 3896
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
