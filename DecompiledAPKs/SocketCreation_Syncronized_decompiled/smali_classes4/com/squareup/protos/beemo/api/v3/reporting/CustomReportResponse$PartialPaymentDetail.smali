.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;
.super Lcom/squareup/wire/Message;
.source "CustomReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PartialPaymentDetail"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$ProtoAdapter_PartialPaymentDetail;,
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BILL_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_INSIDE_RANGE_TOTAL_COLLECTED:Ljava/lang/Long;

.field public static final DEFAULT_TOTAL_COLLECTED:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final bill_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final inside_range_total_collected:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final total_collected:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 191
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$ProtoAdapter_PartialPaymentDetail;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$ProtoAdapter_PartialPaymentDetail;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 197
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->DEFAULT_TOTAL_COLLECTED:Ljava/lang/Long;

    .line 199
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->DEFAULT_INSIDE_RANGE_TOTAL_COLLECTED:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .line 227
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 232
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->bill_token:Ljava/lang/String;

    .line 234
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->total_collected:Ljava/lang/Long;

    .line 235
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->inside_range_total_collected:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 251
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 252
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;

    .line 253
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->bill_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->bill_token:Ljava/lang/String;

    .line 254
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->total_collected:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->total_collected:Ljava/lang/Long;

    .line 255
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->inside_range_total_collected:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->inside_range_total_collected:Ljava/lang/Long;

    .line 256
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 261
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 263
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->bill_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->total_collected:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->inside_range_total_collected:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 267
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;
    .locals 2

    .line 240
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;-><init>()V

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->bill_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->bill_token:Ljava/lang/String;

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->total_collected:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->total_collected:Ljava/lang/Long;

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->inside_range_total_collected:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->inside_range_total_collected:Ljava/lang/Long;

    .line 244
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->bill_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", bill_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->bill_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->total_collected:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", total_collected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->total_collected:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->inside_range_total_collected:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", inside_range_total_collected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->inside_range_total_collected:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PartialPaymentDetail{"

    .line 278
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
