.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public begin_time:Lcom/squareup/protos/common/time/DateTime;

.field public end_time:Lcom/squareup/protos/common/time/DateTime;

.field public tz_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3271
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public begin_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;
    .locals 0

    .line 3275
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .locals 5

    .line 3295
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->tz_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3264
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    move-result-object v0

    return-object v0
.end method

.method public end_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;
    .locals 0

    .line 3280
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public tz_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;
    .locals 0

    .line 3289
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange$Builder;->tz_name:Ljava/lang/String;

    return-object p0
.end method
