.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7173
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;
    .locals 3

    .line 7183
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7170
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    move-result-object v0

    return-object v0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu$Builder;
    .locals 0

    .line 7177
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
