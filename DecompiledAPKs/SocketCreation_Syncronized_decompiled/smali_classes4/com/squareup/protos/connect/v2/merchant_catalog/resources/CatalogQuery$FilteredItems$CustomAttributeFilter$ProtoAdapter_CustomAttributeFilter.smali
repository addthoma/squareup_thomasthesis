.class final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CustomAttributeFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2530
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2557
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;-><init>()V

    .line 2558
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2559
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 2575
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2573
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_max_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    goto :goto_0

    .line 2572
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_min_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    goto :goto_0

    .line 2571
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_prefix(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    goto :goto_0

    .line 2570
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_exact(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    goto :goto_0

    .line 2569
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_definition_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2563
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->filter_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 2565
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 2579
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2580
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2528
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2546
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2547
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2548
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2549
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2550
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2551
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2552
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2528
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;)I
    .locals 4

    .line 2535
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 2536
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    const/4 v3, 0x3

    .line 2537
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    const/4 v3, 0x4

    .line 2538
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    const/4 v3, 0x5

    .line 2539
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    const/4 v3, 0x6

    .line 2540
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2541
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2528
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;
    .locals 0

    .line 2585
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    move-result-object p1

    .line 2586
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2587
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2528
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    move-result-object p1

    return-object p1
.end method
