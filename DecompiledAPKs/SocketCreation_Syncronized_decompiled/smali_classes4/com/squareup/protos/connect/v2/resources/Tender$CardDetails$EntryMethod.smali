.class public final enum Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EntryMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod$ProtoAdapter_EntryMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONTACTLESS:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

.field public static final enum EMV:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

.field public static final enum KEYED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

.field public static final enum ON_FILE:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

.field public static final enum SWIPED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 889
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    const/4 v1, 0x0

    const-string v2, "SWIPED"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->SWIPED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 897
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    const/4 v2, 0x1

    const-string v3, "KEYED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->KEYED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 902
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    const/4 v3, 0x2

    const-string v4, "EMV"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->EMV:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 907
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    const/4 v4, 0x3

    const-string v5, "ON_FILE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->ON_FILE:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 915
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    const/4 v5, 0x4

    const-string v6, "CONTACTLESS"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 885
    sget-object v6, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->SWIPED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->KEYED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->EMV:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->ON_FILE:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 917
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod$ProtoAdapter_EntryMethod;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod$ProtoAdapter_EntryMethod;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 921
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 922
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 934
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object p0

    .line 933
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->ON_FILE:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object p0

    .line 932
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->EMV:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object p0

    .line 931
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->KEYED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object p0

    .line 930
    :cond_4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->SWIPED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;
    .locals 1

    .line 885
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;
    .locals 1

    .line 885
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 941
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->value:I

    return v0
.end method
