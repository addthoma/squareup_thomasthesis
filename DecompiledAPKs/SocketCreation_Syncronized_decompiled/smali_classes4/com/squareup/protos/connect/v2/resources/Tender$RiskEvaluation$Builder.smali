.class public final Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;",
        "Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public level:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

.field public numeric_score:Ljava/lang/Double;

.field public reasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1446
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1447
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->reasons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;
    .locals 7

    .line 1485
    new-instance v6, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->level:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->numeric_score:Ljava/lang/Double;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->reasons:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;-><init>(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;Ljava/lang/Double;Ljava/util/List;Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1437
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    move-result-object v0

    return-object v0
.end method

.method public level(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;
    .locals 0

    .line 1454
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->level:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    return-object p0
.end method

.method public numeric_score(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;
    .locals 0

    .line 1462
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->numeric_score:Ljava/lang/Double;

    return-object p0
.end method

.method public reasons(Ljava/util/List;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;"
        }
    .end annotation

    .line 1470
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1471
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->reasons:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;
    .locals 0

    .line 1479
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;

    return-object p0
.end method
