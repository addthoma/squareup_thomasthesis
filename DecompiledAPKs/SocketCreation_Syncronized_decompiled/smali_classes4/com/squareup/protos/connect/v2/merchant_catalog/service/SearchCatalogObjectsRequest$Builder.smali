.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchCatalogObjectsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public begin_time:Ljava/lang/String;

.field public cursor:Ljava/lang/String;

.field public end_time:Ljava/lang/String;

.field public include_counts:Ljava/lang/Boolean;

.field public include_deleted_objects:Ljava/lang/Boolean;

.field public include_related_objects:Ljava/lang/Boolean;

.field public limit:Ljava/lang/Integer;

.field public object_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field public query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 278
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 279
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->object_types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public begin_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 345
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->begin_time:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;
    .locals 12

    .line 402
    new-instance v11, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->cursor:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->object_types:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_deleted_objects:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_related_objects:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->begin_time:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->end_time:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_counts:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 259
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->cursor:Ljava/lang/String;

    return-object p0
.end method

.method public end_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 377
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->end_time:Ljava/lang/String;

    return-object p0
.end method

.method public include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 396
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_counts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_deleted_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_deleted_objects:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_related_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->include_related_objects:Ljava/lang/Boolean;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public object_types(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;"
        }
    .end annotation

    .line 301
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->object_types:Ljava/util/List;

    return-object p0
.end method

.method public query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;
    .locals 0

    .line 353
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest$Builder;->query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    return-object p0
.end method
