.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;
.super Ljava/lang/Enum;
.source "CatalogQuery.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NullableAttribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute$ProtoAdapter_NullableAttribute;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum VENDOR_TOKEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 2593
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "VENDOR_TOKEN"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->VENDOR_TOKEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    new-array v0, v1, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    .line 2592
    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->VENDOR_TOKEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    .line 2595
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute$ProtoAdapter_NullableAttribute;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute$ProtoAdapter_NullableAttribute;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 2599
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2600
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 2608
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->VENDOR_TOKEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;
    .locals 1

    .line 2592
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;
    .locals 1

    .line 2592
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 2615
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$NullableAttribute;->value:I

    return v0
.end method
