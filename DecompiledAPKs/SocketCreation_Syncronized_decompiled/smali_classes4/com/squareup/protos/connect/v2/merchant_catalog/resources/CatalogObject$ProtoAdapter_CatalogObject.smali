.class final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CatalogObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CatalogObject"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 968
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1047
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    .line 1048
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1049
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/16 v4, 0xe

    if-eq v3, v4, :cond_3

    const/16 v4, 0x65

    if-eq v3, v4, :cond_2

    const/16 v4, 0x6c

    if-eq v3, v4, :cond_1

    const/16 v4, 0x6e

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    packed-switch v3, :pswitch_data_2

    packed-switch v3, :pswitch_data_3

    .line 1091
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1089
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto :goto_0

    .line 1088
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->dining_option_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto :goto_0

    .line 1087
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_rule_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto :goto_0

    .line 1086
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto :goto_0

    .line 1085
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_definition_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto :goto_0

    .line 1084
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto :goto_0

    .line 1083
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto :goto_0

    .line 1082
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->subscription_plan_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1081
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1080
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1079
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->pricing_rule_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1078
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->product_set_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1077
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->time_period_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1076
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1073
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1072
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1071
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->category_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1068
    :pswitch_11
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1067
    :pswitch_12
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1066
    :pswitch_13
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1065
    :pswitch_14
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_all_locations(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1064
    :pswitch_15
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->catalog_v1_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1063
    :pswitch_16
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_values:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1062
    :pswitch_17
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->is_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1061
    :pswitch_18
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->version(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1060
    :pswitch_19
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->updated_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1059
    :pswitch_1a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1053
    :pswitch_1b
    :try_start_0
    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 1055
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1075
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_list_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1074
    :cond_1
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->discount_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1070
    :cond_2
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    goto/16 :goto_0

    .line 1069
    :cond_3
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->DEPRECATED_custom_attributes:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1095
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1096
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x68
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x70
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 966
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1010
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1011
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1012
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1013
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1014
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1015
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1016
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1017
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1018
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1019
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1020
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1021
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1022
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    const/16 v2, 0x65

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1023
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    const/16 v2, 0x68

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1024
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    const/16 v2, 0x69

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1025
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    const/16 v2, 0x6a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1026
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    const/16 v2, 0x6c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1027
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    const/16 v2, 0x6e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1028
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    const/16 v2, 0x70

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1029
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    const/16 v2, 0x71

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1030
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    const/16 v2, 0x72

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1031
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    const/16 v2, 0x73

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1032
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    const/16 v2, 0x74

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1033
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    const/16 v2, 0x75

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1034
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    const/16 v2, 0x76

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1035
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    const/16 v2, 0x77

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1036
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    const/16 v2, 0x78

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1037
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    const/16 v2, 0x79

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1038
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    const/16 v2, 0x7a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1039
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    const/16 v2, 0x7b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1040
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    const/16 v2, 0x7c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1041
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    const/16 v2, 0x7d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1042
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 966
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)I
    .locals 4

    .line 973
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 974
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    const/4 v3, 0x3

    .line 975
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 976
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 977
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 978
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 979
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    const/16 v3, 0xe

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 980
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    const/16 v3, 0x9

    .line 981
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 982
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 983
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    const/16 v3, 0xc

    .line 984
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    const/16 v3, 0x65

    .line 985
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    const/16 v3, 0x68

    .line 986
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    const/16 v3, 0x69

    .line 987
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    const/16 v3, 0x6a

    .line 988
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    const/16 v3, 0x6c

    .line 989
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    const/16 v3, 0x6e

    .line 990
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    const/16 v3, 0x70

    .line 991
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    const/16 v3, 0x71

    .line 992
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    const/16 v3, 0x72

    .line 993
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    const/16 v3, 0x73

    .line 994
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    const/16 v3, 0x74

    .line 995
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    const/16 v3, 0x75

    .line 996
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    const/16 v3, 0x76

    .line 997
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    const/16 v3, 0x77

    .line 998
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    const/16 v3, 0x78

    .line 999
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    const/16 v3, 0x79

    .line 1000
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    const/16 v3, 0x7a

    .line 1001
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    const/16 v3, 0x7b

    .line 1002
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    const/16 v3, 0x7c

    .line 1003
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    const/16 v3, 0x7d

    .line 1004
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1005
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 966
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
    .locals 2

    .line 1101
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object p1

    .line 1102
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_values:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1103
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->DEPRECATED_custom_attributes:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1104
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->catalog_v1_ids:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1105
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    .line 1106
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    .line 1107
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    .line 1108
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    .line 1109
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    .line 1110
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    .line 1111
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    .line 1112
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    .line 1113
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    .line 1114
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    .line 1115
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    .line 1116
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    .line 1117
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    .line 1118
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    .line 1119
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    .line 1120
    :cond_e
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    .line 1121
    :cond_f
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    .line 1122
    :cond_10
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    .line 1123
    :cond_11
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    .line 1124
    :cond_12
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    .line 1125
    :cond_13
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1126
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 966
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object p1

    return-object p1
.end method
