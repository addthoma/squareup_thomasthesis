.class public final Lcom/squareup/protos/connect/v2/resources/Card$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Card;",
        "Lcom/squareup/protos/connect/v2/resources/Card$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public bin:Ljava/lang/String;

.field public card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

.field public card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

.field public cardholder_name:Ljava/lang/String;

.field public enabled:Ljava/lang/Boolean;

.field public exp_month:Ljava/lang/Long;

.field public exp_year:Ljava/lang/Long;

.field public fingerprint:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public last_4:Ljava/lang/String;

.field public prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

.field public reference_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 345
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public billing_address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public bin(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 480
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->bin:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/resources/Card;
    .locals 17

    move-object/from16 v0, p0

    .line 486
    new-instance v16, Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object v2, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    iget-object v4, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->last_4:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->exp_month:Ljava/lang/Long;

    iget-object v6, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->exp_year:Ljava/lang/Long;

    iget-object v7, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->cardholder_name:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v9, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->fingerprint:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->reference_id:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->enabled:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    iget-object v13, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    iget-object v14, v0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->bin:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/connect/v2/resources/Card;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card$Brand;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/resources/Card$Type;Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 318
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Card;

    move-result-object v0

    return-object v0
.end method

.method public card_brand(Lcom/squareup/protos/connect/v2/resources/Card$Brand;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    return-object p0
.end method

.method public card_type(Lcom/squareup/protos/connect/v2/resources/Card$Type;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 458
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->card_type:Lcom/squareup/protos/connect/v2/resources/Card$Type;

    return-object p0
.end method

.method public cardholder_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 403
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->cardholder_name:Ljava/lang/String;

    return-object p0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 447
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public exp_month(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->exp_month:Ljava/lang/Long;

    return-object p0
.end method

.method public exp_year(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->exp_year:Ljava/lang/Long;

    return-object p0
.end method

.method public fingerprint(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 425
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->fingerprint:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public last_4(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->last_4:Ljava/lang/String;

    return-object p0
.end method

.method public prepaid_type(Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 469
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->prepaid_type:Lcom/squareup/protos/connect/v2/resources/Card$PrepaidType;

    return-object p0
.end method

.method public reference_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Card$Builder;
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Card$Builder;->reference_id:Ljava/lang/String;

    return-object p0
.end method
