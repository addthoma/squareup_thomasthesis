.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteCatalogObjectRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public object_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;
    .locals 3

    .line 103
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->object_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;

    move-result-object v0

    return-object v0
.end method

.method public object_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest$Builder;->object_id:Ljava/lang/String;

    return-object p0
.end method
