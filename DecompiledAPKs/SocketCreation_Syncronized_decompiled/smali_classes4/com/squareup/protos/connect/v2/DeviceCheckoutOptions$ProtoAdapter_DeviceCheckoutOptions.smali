.class final Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DeviceCheckoutOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DeviceCheckoutOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 294
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 323
    new-instance v0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;-><init>()V

    .line 324
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 325
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 335
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 333
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    goto :goto_0

    .line 332
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->allow_split_payment(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    goto :goto_0

    .line 331
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/connect/v2/TipSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/TipSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->tip_settings(Lcom/squareup/protos/connect/v2/TipSettings;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    goto :goto_0

    .line 330
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->collect_signature(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    goto :goto_0

    .line 329
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->print_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    goto :goto_0

    .line 328
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->display_digital_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    goto :goto_0

    .line 327
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->device_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    goto :goto_0

    .line 339
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 340
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->build()Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 292
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 311
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lcom/squareup/protos/connect/v2/TipSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 318
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 292
    check-cast p2, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;)I
    .locals 4

    .line 299
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->device_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->display_digital_receipt:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->print_receipt:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->skip_receipt_screen:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->collect_signature:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/TipSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    const/4 v3, 0x5

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->allow_split_payment:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 292
    check-cast p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;->encodedSize(Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;
    .locals 2

    .line 345
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->newBuilder()Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;

    move-result-object p1

    .line 346
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/TipSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/TipSettings;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    .line 347
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 348
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->build()Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 292
    check-cast p1, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$ProtoAdapter_DeviceCheckoutOptions;->redact(Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    move-result-object p1

    return-object p1
.end method
