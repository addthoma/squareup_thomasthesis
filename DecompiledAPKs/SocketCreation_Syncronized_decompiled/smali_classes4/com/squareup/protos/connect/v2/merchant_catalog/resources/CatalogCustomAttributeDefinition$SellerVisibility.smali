.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;
.super Ljava/lang/Enum;
.source "CatalogCustomAttributeDefinition.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SellerVisibility"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility$ProtoAdapter_SellerVisibility;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SELLER_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

.field public static final enum SELLER_VISIBILITY_HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

.field public static final enum SELLER_VISIBILITY_READ_WRITE_VALUES:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 480
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    const/4 v1, 0x0

    const-string v2, "SELLER_VISIBILITY_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 485
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    const/4 v2, 0x1

    const-string v3, "SELLER_VISIBILITY_HIDDEN"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 490
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    const/4 v3, 0x2

    const-string v4, "SELLER_VISIBILITY_READ_WRITE_VALUES"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_READ_WRITE_VALUES:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 476
    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_READ_WRITE_VALUES:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 492
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility$ProtoAdapter_SellerVisibility;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility$ProtoAdapter_SellerVisibility;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 496
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 497
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 507
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_READ_WRITE_VALUES:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    return-object p0

    .line 506
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    return-object p0

    .line 505
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;
    .locals 1

    .line 476
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;
    .locals 1

    .line 476
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 514
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->value:I

    return v0
.end method
