.class public final Lcom/squareup/protos/connect/v2/CheckoutMetadata;
.super Lcom/squareup/wire/Message;
.source "CheckoutMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/CheckoutMetadata$ProtoAdapter_CheckoutMetadata;,
        Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/CheckoutMetadata;",
        "Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/CheckoutMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACKED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_ACK_REASON:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final DEFAULT_CANCELED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_COMPLETED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PUSHED_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.AckReasons$AckReason#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final acked_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final canceled_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final completed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final payments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.AddedPaymentForTerminalCheckout#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
            ">;"
        }
    .end annotation
.end field

.field public final pushed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$ProtoAdapter_CheckoutMetadata;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CheckoutMetadata$ProtoAdapter_CheckoutMetadata;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->DO_NOT_USE:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    sput-object v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->DEFAULT_ACK_REASON:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/AckReasons$AckReason;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
            ">;)V"
        }
    .end annotation

    .line 107
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/AckReasons$AckReason;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AddedPaymentForTerminalCheckout;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 113
    sget-object v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->pushed_at:Ljava/lang/String;

    .line 115
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 116
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->acked_at:Ljava/lang/String;

    .line 117
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->completed_at:Ljava/lang/String;

    .line 118
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->canceled_at:Ljava/lang/String;

    const-string p1, "payments"

    .line 119
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->payments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 138
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 139
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    .line 140
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->pushed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->pushed_at:Ljava/lang/String;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->acked_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->acked_at:Ljava/lang/String;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->completed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->completed_at:Ljava/lang/String;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->canceled_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->canceled_at:Ljava/lang/String;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->payments:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->payments:Ljava/util/List;

    .line 146
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 151
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 153
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->pushed_at:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->acked_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->completed_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;
    .locals 2

    .line 124
    new-instance v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;-><init>()V

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->pushed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->pushed_at:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->acked_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->acked_at:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->completed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->completed_at:Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->canceled_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->canceled_at:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->payments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->payments:Ljava/util/List;

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->newBuilder()Lcom/squareup/protos/connect/v2/CheckoutMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->pushed_at:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", pushed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->pushed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    if-eqz v1, :cond_1

    const-string v1, ", ack_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->acked_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", acked_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->acked_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->completed_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", completed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->completed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", canceled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->canceled_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->payments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CheckoutMetadata{"

    .line 174
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
