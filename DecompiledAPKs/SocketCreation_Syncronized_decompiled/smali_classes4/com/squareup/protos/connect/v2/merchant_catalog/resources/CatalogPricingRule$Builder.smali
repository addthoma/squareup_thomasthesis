.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogPricingRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

.field public apply_products_id:Ljava/lang/String;

.field public discount_id:Ljava/lang/String;

.field public discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

.field public exclude_products_id:Ljava/lang/String;

.field public exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

.field public match_products_id:Ljava/lang/String;

.field public max_applications_per_attachment:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

.field public time_period_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public valid_from_date:Ljava/lang/String;

.field public valid_from_local_time:Ljava/lang/String;

.field public valid_until_date:Ljava/lang/String;

.field public valid_until_local_time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 415
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 416
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->time_period_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public application_mode(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 573
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    return-object p0
.end method

.method public apply_products_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 483
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->apply_products_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;
    .locals 20

    move-object/from16 v0, p0

    .line 605
    new-instance v18, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->time_period_ids:Ljava/util/List;

    iget-object v4, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->discount_id:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->match_products_id:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->apply_products_id:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    iget-object v8, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->exclude_products_id:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_from_date:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_from_local_time:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_until_date:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_until_local_time:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    iget-object v14, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    iget-object v15, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->max_applications_per_attachment:Ljava/lang/Integer;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 384
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    move-result-object v0

    return-object v0
.end method

.method public discount_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 451
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->discount_id:Ljava/lang/String;

    return-object p0
.end method

.method public discount_target_scope(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 586
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    return-object p0
.end method

.method public exclude_products_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 507
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->exclude_products_id:Ljava/lang/String;

    return-object p0
.end method

.method public exclude_strategy(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 562
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    return-object p0
.end method

.method public match_products_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 464
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->match_products_id:Ljava/lang/String;

    return-object p0
.end method

.method public max_applications_per_attachment(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 599
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->max_applications_per_attachment:Ljava/lang/Integer;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public stackable(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 493
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    return-object p0
.end method

.method public time_period_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;"
        }
    .end annotation

    .line 438
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 439
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->time_period_ids:Ljava/util/List;

    return-object p0
.end method

.method public valid_from_date(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 517
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_from_date:Ljava/lang/String;

    return-object p0
.end method

.method public valid_from_local_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 528
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_from_local_time:Ljava/lang/String;

    return-object p0
.end method

.method public valid_until_date(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 538
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_until_date:Ljava/lang/String;

    return-object p0
.end method

.method public valid_until_local_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 0

    .line 549
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_until_local_time:Ljava/lang/String;

    return-object p0
.end method
