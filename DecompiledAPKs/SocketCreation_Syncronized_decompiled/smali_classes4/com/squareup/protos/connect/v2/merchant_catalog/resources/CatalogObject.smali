.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
.super Lcom/squareup/wire/Message;
.source "CatalogObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IMAGE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_DELETED:Ljava/lang/Boolean;

.field public static final DEFAULT_PRESENT_AT_ALL_LOCATIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final DEFAULT_UPDATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final DEPRECATED_custom_attributes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeValue#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public final absent_at_location_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final catalog_v1_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogV1Id#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;",
            ">;"
        }
    .end annotation
.end field

.field public final category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCategory#ADAPTER"
        tag = 0x68
    .end annotation
.end field

.field public final custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeDefinition#ADAPTER"
        tag = 0x79
    .end annotation
.end field

.field public final custom_attribute_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeValue#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public final dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogDiningOption#ADAPTER"
        tag = 0x7c
    .end annotation
.end field

.field public final discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogDiscount#ADAPTER"
        tag = 0x6c
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogImage#ADAPTER"
        tag = 0x74
    .end annotation
.end field

.field public final image_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final is_deleted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogItem#ADAPTER"
        tag = 0x65
    .end annotation
.end field

.field public final item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogItemOption#ADAPTER"
        tag = 0x77
    .end annotation
.end field

.field public final item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogItemOptionValue#ADAPTER"
        tag = 0x78
    .end annotation
.end field

.field public final item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogItemVariation#ADAPTER"
        tag = 0x69
    .end annotation
.end field

.field public final measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogMeasurementUnit#ADAPTER"
        tag = 0x75
    .end annotation
.end field

.field public final modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogModifier#ADAPTER"
        tag = 0x70
    .end annotation
.end field

.field public final modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogModifierList#ADAPTER"
        tag = 0x6e
    .end annotation
.end field

.field public final present_at_all_locations:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final present_at_location_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogPricingRule#ADAPTER"
        tag = 0x73
    .end annotation
.end field

.field public final product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogProductSet#ADAPTER"
        tag = 0x72
    .end annotation
.end field

.field public final quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuickAmountsSettings#ADAPTER"
        tag = 0x7a
    .end annotation
.end field

.field public final resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.Resource#ADAPTER"
        tag = 0x7d
    .end annotation
.end field

.field public final subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogSubscriptionPlan#ADAPTER"
        tag = 0x76
    .end annotation
.end field

.field public final tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogTax#ADAPTER"
        tag = 0x6a
    .end annotation
.end field

.field public final tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogTaxRule#ADAPTER"
        tag = 0x7b
    .end annotation
.end field

.field public final time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogTimePeriod#ADAPTER"
        tag = 0x71
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogObjectType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final updated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 42
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$ProtoAdapter_CatalogObject;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 46
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-wide/16 v0, 0x0

    .line 52
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEFAULT_VERSION:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 54
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEFAULT_IS_DELETED:Ljava/lang/Boolean;

    const/4 v0, 0x1

    .line 56
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEFAULT_PRESENT_AT_ALL_LOCATIONS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;Lokio/ByteString;)V
    .locals 1

    .line 391
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 392
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 393
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    .line 394
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->updated_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    .line 395
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->version:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    .line 396
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->is_deleted:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    .line 397
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_values:Ljava/util/List;

    const-string v0, "custom_attribute_values"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    .line 398
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->DEPRECATED_custom_attributes:Ljava/util/List;

    const-string v0, "DEPRECATED_custom_attributes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    .line 399
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->catalog_v1_ids:Ljava/util/List;

    const-string v0, "catalog_v1_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    .line 400
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_all_locations:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    .line 401
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    const-string v0, "present_at_location_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    .line 402
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    const-string v0, "absent_at_location_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    .line 403
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    .line 404
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    .line 405
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    .line 406
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    .line 407
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    .line 408
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    .line 409
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    .line 410
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    .line 411
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    .line 412
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    .line 413
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    .line 414
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    .line 415
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    .line 416
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    .line 417
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    .line 418
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    .line 419
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    .line 420
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    .line 421
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    .line 422
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    .line 423
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 468
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 469
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 470
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 471
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    .line 472
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    .line 473
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    .line 474
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    .line 475
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    .line 476
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    .line 477
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    .line 478
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    .line 479
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    .line 480
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    .line 481
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    .line 482
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    .line 483
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    .line 484
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    .line 485
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    .line 486
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    .line 487
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    .line 488
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    .line 489
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    .line 490
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    .line 491
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    .line 492
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    .line 493
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    .line 494
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    .line 495
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    .line 496
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    .line 497
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    .line 498
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    .line 499
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    .line 500
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    .line 501
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    .line 502
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 507
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1b

    .line 509
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 510
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 511
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 512
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 513
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 514
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 515
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 516
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 517
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 518
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 519
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 520
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 521
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 522
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 523
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 524
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 525
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 526
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 527
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 528
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 529
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 530
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 531
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 532
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 533
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 534
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 535
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 536
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 537
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 538
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 539
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 540
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 541
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;->hashCode()I

    move-result v2

    :cond_1a
    add-int/2addr v0, v2

    .line 542
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 2

    .line 428
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    .line 429
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 430
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id:Ljava/lang/String;

    .line 431
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->updated_at:Ljava/lang/String;

    .line 432
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->version:Ljava/lang/Long;

    .line 433
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->is_deleted:Ljava/lang/Boolean;

    .line 434
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_values:Ljava/util/List;

    .line 435
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->DEPRECATED_custom_attributes:Ljava/util/List;

    .line 436
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->catalog_v1_ids:Ljava/util/List;

    .line 437
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_all_locations:Ljava/lang/Boolean;

    .line 438
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    .line 439
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    .line 440
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_id:Ljava/lang/String;

    .line 441
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    .line 442
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    .line 443
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    .line 444
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    .line 445
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    .line 446
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    .line 447
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    .line 448
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    .line 449
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    .line 450
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    .line 451
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    .line 452
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    .line 453
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    .line 454
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    .line 455
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    .line 456
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    .line 457
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    .line 458
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    .line 459
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    .line 460
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    .line 461
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 550
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 551
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->updated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 554
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", is_deleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->is_deleted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 555
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", custom_attribute_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 556
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", DEPRECATED_custom_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEPRECATED_custom_attributes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 557
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", catalog_v1_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->catalog_v1_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 558
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", present_at_all_locations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 559
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", present_at_location_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 560
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", absent_at_location_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 561
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", image_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    if-eqz v1, :cond_c

    const-string v1, ", item_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 563
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    if-eqz v1, :cond_d

    const-string v1, ", category_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 564
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    if-eqz v1, :cond_e

    const-string v1, ", item_variation_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 565
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    if-eqz v1, :cond_f

    const-string v1, ", tax_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 566
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    if-eqz v1, :cond_10

    const-string v1, ", discount_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 567
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    if-eqz v1, :cond_11

    const-string v1, ", modifier_list_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 568
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    if-eqz v1, :cond_12

    const-string v1, ", modifier_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 569
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    if-eqz v1, :cond_13

    const-string v1, ", time_period_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 570
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    if-eqz v1, :cond_14

    const-string v1, ", product_set_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 571
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    if-eqz v1, :cond_15

    const-string v1, ", pricing_rule_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 572
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    if-eqz v1, :cond_16

    const-string v1, ", image_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 573
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    if-eqz v1, :cond_17

    const-string v1, ", measurement_unit_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 574
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    if-eqz v1, :cond_18

    const-string v1, ", subscription_plan_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 575
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    if-eqz v1, :cond_19

    const-string v1, ", item_option_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 576
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    if-eqz v1, :cond_1a

    const-string v1, ", item_option_value_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 577
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    if-eqz v1, :cond_1b

    const-string v1, ", custom_attribute_definition_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 578
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    if-eqz v1, :cond_1c

    const-string v1, ", quick_amounts_settings_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 579
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    if-eqz v1, :cond_1d

    const-string v1, ", tax_rule_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 580
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    if-eqz v1, :cond_1e

    const-string v1, ", dining_option_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 581
    :cond_1e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    if-eqz v1, :cond_1f

    const-string v1, ", resource_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1f
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogObject{"

    .line 582
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
