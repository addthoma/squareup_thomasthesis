.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogTax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/connect/v2/common/Money;

.field public applies_to_custom_amounts:Ljava/lang/Boolean;

.field public applies_to_product_set_id:Ljava/lang/String;

.field public calculation_phase:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

.field public enabled:Ljava/lang/Boolean;

.field public inclusion_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public tax_type_id:Ljava/lang/String;

.field public tax_type_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 262
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 340
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public applies_to_custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public applies_to_product_set_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 350
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->applies_to_product_set_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;
    .locals 13

    .line 356
    new-instance v12, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->calculation_phase:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->inclusion_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->percentage:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->enabled:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->tax_type_id:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->tax_type_name:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->applies_to_product_set_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 241
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->calculation_phase:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxCalculationPhase;

    return-object p0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public inclusion_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->inclusion_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 296
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public tax_type_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->tax_type_id:Ljava/lang/String;

    return-object p0
.end method

.method public tax_type_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;
    .locals 0

    .line 332
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax$Builder;->tax_type_name:Ljava/lang/String;

    return-object p0
.end method
