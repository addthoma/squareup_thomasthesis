.class public final enum Lcom/squareup/protos/connect/v2/common/DayOfWeek;
.super Ljava/lang/Enum;
.source "DayOfWeek.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/DayOfWeek$ProtoAdapter_DayOfWeek;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/DayOfWeek;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/DayOfWeek;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FRI:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public static final enum MON:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public static final enum SAT:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public static final enum SUN:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public static final enum THU:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public static final enum TUE:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public static final enum WED:Lcom/squareup/protos/connect/v2/common/DayOfWeek;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 20
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v1, 0x0

    const-string v2, "SUN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->SUN:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 25
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v2, 0x1

    const-string v3, "MON"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->MON:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 30
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v3, 0x2

    const-string v4, "TUE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/common/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->TUE:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 35
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v4, 0x3

    const-string v5, "WED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/common/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->WED:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 40
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v5, 0x4

    const-string v6, "THU"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/connect/v2/common/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->THU:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 45
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v6, 0x5

    const-string v7, "FRI"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/connect/v2/common/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->FRI:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 50
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v7, 0x6

    const-string v8, "SAT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/connect/v2/common/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->SAT:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 16
    sget-object v8, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->SUN:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->MON:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->TUE:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->WED:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->THU:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->FRI:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->SAT:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->$VALUES:[Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    .line 52
    new-instance v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek$ProtoAdapter_DayOfWeek;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/DayOfWeek$ProtoAdapter_DayOfWeek;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/DayOfWeek;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 71
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->SAT:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0

    .line 70
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->FRI:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0

    .line 69
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->THU:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0

    .line 68
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->WED:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0

    .line 67
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->TUE:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0

    .line 66
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->MON:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0

    .line 65
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->SUN:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/DayOfWeek;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/DayOfWeek;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->$VALUES:[Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/DayOfWeek;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/DayOfWeek;->value:I

    return v0
.end method
