.class public final Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Tender;",
        "Lcom/squareup/protos/connect/v2/resources/Tender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public additional_recipients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;"
        }
    .end annotation
.end field

.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

.field public cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

.field public created_at:Ljava/lang/String;

.field public customer_id:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public location_id:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public payment_id:Ljava/lang/String;

.field public processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

.field public source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

.field public tip_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public transaction_id:Ljava/lang/String;

.field public type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 394
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 395
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->additional_recipients:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public additional_recipients(Ljava/util/List;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/resources/Tender$Builder;"
        }
    .end annotation

    .line 531
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 532
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->additional_recipients:Ljava/util/List;

    return-object p0
.end method

.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 456
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/resources/Tender;
    .locals 2

    .line 571
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/resources/Tender;-><init>(Lcom/squareup/protos/connect/v2/resources/Tender$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 359
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender;

    move-result-object v0

    return-object v0
.end method

.method public card_details(Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    return-object p0
.end method

.method public cash_details(Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 520
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    return-object p0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 436
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 488
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 402
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 420
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 444
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public payment_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 565
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->payment_id:Ljava/lang/String;

    return-object p0
.end method

.method public processing_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 477
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public risk_evaluation(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 554
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    return-object p0
.end method

.method public source(Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 544
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 464
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public transaction_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 428
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->transaction_id:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/connect/v2/resources/Tender$Type;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 496
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 0

    .line 412
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
