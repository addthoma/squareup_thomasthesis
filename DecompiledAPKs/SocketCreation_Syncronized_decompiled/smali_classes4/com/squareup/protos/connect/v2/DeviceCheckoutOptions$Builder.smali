.class public final Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeviceCheckoutOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;",
        "Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_split_payment:Ljava/lang/Boolean;

.field public collect_signature:Ljava/lang/Boolean;

.field public device_id:Ljava/lang/String;

.field public display_digital_receipt:Ljava/lang/Boolean;

.field public print_receipt:Ljava/lang/Boolean;

.field public skip_receipt_screen:Ljava/lang/Boolean;

.field public tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 210
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_split_payment(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->allow_split_payment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;
    .locals 10

    .line 288
    new-instance v9, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->device_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->display_digital_receipt:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->print_receipt:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->collect_signature:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->allow_split_payment:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/TipSettings;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 195
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->build()Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    move-result-object v0

    return-object v0
.end method

.method public collect_signature(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->collect_signature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public device_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->device_id:Ljava/lang/String;

    return-object p0
.end method

.method public display_digital_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->display_digital_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public print_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->print_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tip_settings(Lcom/squareup/protos/connect/v2/TipSettings;)Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions$Builder;->tip_settings:Lcom/squareup/protos/connect/v2/TipSettings;

    return-object p0
.end method
