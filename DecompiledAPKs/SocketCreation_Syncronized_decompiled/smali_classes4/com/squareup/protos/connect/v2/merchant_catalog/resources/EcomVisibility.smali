.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;
.super Ljava/lang/Enum;
.source "EcomVisibility.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility$ProtoAdapter_EcomVisibility;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ECOM_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

.field public static final enum HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

.field public static final enum UNAVAILABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

.field public static final enum UNINDEXED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

.field public static final enum VISIBLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 16
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    const/4 v1, 0x0

    const-string v2, "ECOM_VISIBILITY_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->ECOM_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    .line 21
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    const/4 v2, 0x1

    const-string v3, "UNINDEXED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->UNINDEXED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    const/4 v3, 0x2

    const-string v4, "UNAVAILABLE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->UNAVAILABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    const/4 v4, 0x3

    const-string v5, "HIDDEN"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    .line 36
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    const/4 v5, 0x4

    const-string v6, "VISIBLE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->VISIBLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    .line 15
    sget-object v6, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->ECOM_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->UNINDEXED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->UNAVAILABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->VISIBLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    .line 38
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility$ProtoAdapter_EcomVisibility;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility$ProtoAdapter_EcomVisibility;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 55
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->VISIBLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object p0

    .line 54
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object p0

    .line 53
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->UNAVAILABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object p0

    .line 52
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->UNINDEXED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object p0

    .line 51
    :cond_4
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->ECOM_VISIBILITY_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 62
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/EcomVisibility;->value:I

    return v0
.end method
