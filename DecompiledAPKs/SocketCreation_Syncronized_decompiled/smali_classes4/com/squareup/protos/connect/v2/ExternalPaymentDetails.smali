.class public final Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
.super Lcom/squareup/wire/Message;
.source "ExternalPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;,
        Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;",
        "Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SOURCE:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final source:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final source_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 6

    .line 97
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    .line 105
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    .line 106
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 123
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 124
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 129
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 134
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 141
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;
    .locals 2

    .line 111
    new-instance v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->type:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source:Ljava/lang/String;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_id:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->newBuilder()Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", source_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", source_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ExternalPaymentDetails{"

    .line 153
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
