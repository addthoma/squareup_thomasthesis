.class final Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$ProtoAdapter_BatchRetrieveCatalogObjectsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BatchRetrieveCatalogObjectsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BatchRetrieveCatalogObjectsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 293
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 321
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;-><init>()V

    .line 322
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 323
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    const/4 v4, 0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    const/4 v4, 0x4

    if-eq v3, v4, :cond_3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_2

    const/4 v4, 0x6

    if-eq v3, v4, :cond_1

    const/4 v4, 0x7

    if-eq v3, v4, :cond_0

    .line 332
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 330
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_nested_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 329
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->version(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 328
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 327
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_related_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 326
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_deleted_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    goto :goto_0

    .line 325
    :cond_5
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->object_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336
    :cond_6
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 337
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 291
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$ProtoAdapter_BatchRetrieveCatalogObjectsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->object_ids:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 311
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_related_objects:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_counts:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->version:Ljava/lang/Long;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_deleted_objects:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_nested_objects:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 291
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$ProtoAdapter_BatchRetrieveCatalogObjectsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)I
    .locals 4

    .line 298
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->object_ids:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_related_objects:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 299
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_counts:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->version:Ljava/lang/Long;

    const/4 v3, 0x6

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_deleted_objects:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->include_nested_objects:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 291
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$ProtoAdapter_BatchRetrieveCatalogObjectsRequest;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;
    .locals 0

    .line 342
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;

    move-result-object p1

    .line 343
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 344
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 291
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$ProtoAdapter_BatchRetrieveCatalogObjectsRequest;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    move-result-object p1

    return-object p1
.end method
