.class public final Lcom/squareup/protos/privacyvault/model/Entity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/model/Entity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/privacyvault/model/Entity;",
        "Lcom/squareup/protos/privacyvault/model/Entity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public token:Ljava/lang/String;

.field public token_type:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/privacyvault/model/Entity;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity;

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/model/Entity$Builder;->token_type:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    iget-object v2, p0, Lcom/squareup/protos/privacyvault/model/Entity$Builder;->token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/privacyvault/model/Entity;-><init>(Lcom/squareup/protos/privacyvault/model/Entity$TokenType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/model/Entity$Builder;->build()Lcom/squareup/protos/privacyvault/model/Entity;

    move-result-object v0

    return-object v0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/model/Entity$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/model/Entity$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public token_type(Lcom/squareup/protos/privacyvault/model/Entity$TokenType;)Lcom/squareup/protos/privacyvault/model/Entity$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/model/Entity$Builder;->token_type:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0
.end method
