.class public final Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetSubscriptionInvitationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public locale:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;
    .locals 5

    .line 129
    new-instance v0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    iget-object v3, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;->locale:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;->build()Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;

    move-result-object v0

    return-object v0
.end method

.method public locale(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;->locale:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public subscriber(Lcom/squareup/protos/postoffice/sms/Subscriber;)Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    return-object p0
.end method
