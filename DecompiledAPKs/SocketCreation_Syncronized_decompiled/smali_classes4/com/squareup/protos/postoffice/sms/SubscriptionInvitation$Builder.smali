.class public final Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SubscriptionInvitation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;",
        "Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

.field public subtitle:Ljava/lang/String;

.field public terms:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;
    .locals 8

    .line 169
    new-instance v7, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    iget-object v3, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->title:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subtitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->terms:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;-><init>(Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public subscriber(Lcom/squareup/protos/postoffice/sms/Subscriber;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    return-object p0
.end method

.method public subtitle(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subtitle:Ljava/lang/String;

    return-object p0
.end method

.method public terms(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->terms:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
