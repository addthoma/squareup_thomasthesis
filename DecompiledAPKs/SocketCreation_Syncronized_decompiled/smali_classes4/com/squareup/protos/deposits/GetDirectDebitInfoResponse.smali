.class public final Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;
.super Lcom/squareup/wire/Message;
.source "GetDirectDebitInfoResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$ProtoAdapter_GetDirectDebitInfoResponse;,
        Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;,
        Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;,
        Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCOUNT_OWNER_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_ACCOUNT_OWNER_FIRST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ACCOUNT_OWNER_LAST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_BANK_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_DIRECT_DEBIT_REFERENCE_ID:Ljava/lang/Long;

.field public static final DEFAULT_ERROR:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

.field private static final serialVersionUID:J


# instance fields
.field public final account_owner_email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final account_owner_first_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final account_owner_last_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final bank_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.GetDirectDebitInfoResponse$Address#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final direct_debit_reference_id:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x6
    .end annotation
.end field

.field public final error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.GetDirectDebitInfoResponse$GetDirectDebitInfoError#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$ProtoAdapter_GetDirectDebitInfoResponse;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$ProtoAdapter_GetDirectDebitInfoResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->GET_DIRECT_DEBIT_INFO_ERROR_DO_NOT_USE:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->DEFAULT_ERROR:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    const-wide/16 v0, 0x0

    .line 36
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->DEFAULT_DIRECT_DEBIT_REFERENCE_ID:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 9

    .line 100
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;-><init>(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 106
    sget-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    .line 108
    iput-object p2, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    .line 109
    iput-object p3, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    .line 110
    iput-object p4, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    .line 111
    iput-object p5, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    .line 112
    iput-object p6, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    .line 113
    iput-object p7, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->bank_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 133
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 134
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    iget-object v3, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    iget-object v3, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->bank_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->bank_name:Ljava/lang/String;

    .line 142
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 147
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 149
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->bank_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 157
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 2

    .line 118
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    iput-object v1, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_first_name:Ljava/lang/String;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_last_name:Ljava/lang/String;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_email:Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    iput-object v1, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->direct_debit_reference_id:Ljava/lang/Long;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->bank_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->bank_name:Ljava/lang/String;

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->newBuilder()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    if-eqz v1, :cond_0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", account_owner_first_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", account_owner_last_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", account_owner_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    if-eqz v1, :cond_4

    const-string v1, ", business_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", direct_debit_reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->bank_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", bank_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->bank_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetDirectDebitInfoResponse{"

    .line 172
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
