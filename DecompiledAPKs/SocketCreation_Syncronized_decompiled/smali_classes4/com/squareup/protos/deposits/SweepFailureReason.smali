.class public final Lcom/squareup/protos/deposits/SweepFailureReason;
.super Lcom/squareup/wire/Message;
.source "SweepFailureReason.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;,
        Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/SweepFailureReason;",
        "Lcom/squareup/protos/deposits/SweepFailureReason$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/SweepFailureReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BOOKKEEPER_PULL_FAILURE:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

.field public static final DEFAULT_BOOKKEEPER_PUSH_FAILURE:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

.field public static final DEFAULT_ESPERANTO_FAILURE:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public static final DEFAULT_TELLER_FAILURE:Lcom/squareup/protos/teller/FailureReason;

.field public static final DEFAULT_WALLET_PULL_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field private static final serialVersionUID:J


# instance fields
.field public final bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bookkeeper.SweepBizbankMoneyResponse$Error#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bookkeeper.ReverseSweepBizbankMoneyResponse$Error#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.payments.common.PushMoneyError#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final teller_failure:Lcom/squareup/protos/teller/FailureReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.teller.FailureReason#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error$Code#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/SweepFailureReason$ProtoAdapter_SweepFailureReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/SweepFailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/payments/common/PushMoneyError;->DEFAULT_PUSH_MONEY_ERROR_ENUM_DO_NOT_USE:Lcom/squareup/protos/payments/common/PushMoneyError;

    sput-object v0, Lcom/squareup/protos/deposits/SweepFailureReason;->DEFAULT_ESPERANTO_FAILURE:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 34
    sget-object v0, Lcom/squareup/protos/teller/FailureReason;->INVALID_BANK_ACCOUNT:Lcom/squareup/protos/teller/FailureReason;

    sput-object v0, Lcom/squareup/protos/deposits/SweepFailureReason;->DEFAULT_TELLER_FAILURE:Lcom/squareup/protos/teller/FailureReason;

    .line 36
    sget-object v0, Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;->SWEEP_ERROR_UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    sput-object v0, Lcom/squareup/protos/deposits/SweepFailureReason;->DEFAULT_BOOKKEEPER_PULL_FAILURE:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 38
    sget-object v0, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->REVERSE_SWEEP_ERROR_UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    sput-object v0, Lcom/squareup/protos/deposits/SweepFailureReason;->DEFAULT_BOOKKEEPER_PUSH_FAILURE:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 40
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INTERNAL_SERVER_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    sput-object v0, Lcom/squareup/protos/deposits/SweepFailureReason;->DEFAULT_WALLET_PULL_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/payments/common/PushMoneyError;Lcom/squareup/protos/teller/FailureReason;Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;Lcom/squareup/protos/connect/v2/resources/Error$Code;)V
    .locals 7

    .line 91
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/deposits/SweepFailureReason;-><init>(Lcom/squareup/protos/payments/common/PushMoneyError;Lcom/squareup/protos/teller/FailureReason;Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;Lcom/squareup/protos/connect/v2/resources/Error$Code;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/payments/common/PushMoneyError;Lcom/squareup/protos/teller/FailureReason;Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;Lcom/squareup/protos/connect/v2/resources/Error$Code;Lokio/ByteString;)V
    .locals 2

    .line 98
    sget-object v0, Lcom/squareup/protos/deposits/SweepFailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p6, 0x1

    new-array v0, p6, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p5, v0, v1

    .line 99
    invoke-static {p1, p2, p3, p4, v0}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    if-gt v0, p6, :cond_0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 103
    iput-object p2, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    .line 104
    iput-object p3, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 105
    iput-object p4, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 106
    iput-object p5, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-void

    .line 100
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of esperanto_failure, teller_failure, bookkeeper_pull_failure, bookkeeper_push_failure, wallet_pull_failure may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 124
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/SweepFailureReason;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 125
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/SweepFailureReason;

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/SweepFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/SweepFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    iget-object v3, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    iget-object v3, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    iget-object v3, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    iget-object v3, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    iget-object p1, p1, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 131
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 136
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/SweepFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/payments/common/PushMoneyError;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/teller/FailureReason;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Error$Code;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 144
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    .locals 2

    .line 111
    new-instance v0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    iput-object v1, v0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    iput-object v1, v0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    iput-object v1, v0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    iput-object v1, v0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    iput-object v1, v0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/SweepFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/SweepFailureReason;->newBuilder()Lcom/squareup/protos/deposits/SweepFailureReason$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    if-eqz v1, :cond_0

    const-string v1, ", esperanto_failure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    if-eqz v1, :cond_1

    const-string v1, ", teller_failure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    if-eqz v1, :cond_2

    const-string v1, ", bookkeeper_pull_failure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    if-eqz v1, :cond_3

    const-string v1, ", bookkeeper_push_failure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    if-eqz v1, :cond_4

    const-string v1, ", wallet_pull_failure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SweepFailureReason{"

    .line 157
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
