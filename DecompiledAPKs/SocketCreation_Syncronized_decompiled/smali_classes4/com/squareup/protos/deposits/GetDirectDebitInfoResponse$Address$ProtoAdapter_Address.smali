.class final Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$ProtoAdapter_Address;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetDirectDebitInfoResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Address"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 411
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 430
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;-><init>()V

    .line 431
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 432
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 437
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 435
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;

    goto :goto_0

    .line 434
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->recipient(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;

    goto :goto_0

    .line 441
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 442
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->build()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 409
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$ProtoAdapter_Address;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 423
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->recipient:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 424
    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 425
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 409
    check-cast p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$ProtoAdapter_Address;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;)I
    .locals 4

    .line 416
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->recipient:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v3, 0x2

    .line 417
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 418
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 409
    check-cast p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$ProtoAdapter_Address;->encodedSize(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;
    .locals 2

    .line 447
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->newBuilder()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;

    move-result-object p1

    .line 448
    iget-object v0, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v0, p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 449
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 450
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$Builder;->build()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 409
    check-cast p1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address$ProtoAdapter_Address;->redact(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    move-result-object p1

    return-object p1
.end method
