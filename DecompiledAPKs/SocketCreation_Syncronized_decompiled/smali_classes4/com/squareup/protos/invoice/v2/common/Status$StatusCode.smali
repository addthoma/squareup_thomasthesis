.class public final enum Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;
.super Ljava/lang/Enum;
.source "Status.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/common/Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StatusCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/invoice/v2/common/Status$StatusCode$ProtoAdapter_StatusCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

.field public static final enum PAYMENT_DECLINED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

.field public static final enum TRANSIENT_ERROR:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

.field public static final enum UNACTIVATED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 159
    new-instance v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    .line 164
    new-instance v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    const/4 v2, 0x1

    const-string v3, "UNACTIVATED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->UNACTIVATED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    .line 169
    new-instance v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    const/4 v3, 0x2

    const-string v4, "TRANSIENT_ERROR"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->TRANSIENT_ERROR:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    .line 174
    new-instance v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    const/4 v4, 0x3

    const-string v5, "PAYMENT_DECLINED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->PAYMENT_DECLINED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    .line 158
    sget-object v5, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->UNACTIVATED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->TRANSIENT_ERROR:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->PAYMENT_DECLINED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->$VALUES:[Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    .line 176
    new-instance v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode$ProtoAdapter_StatusCode;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode$ProtoAdapter_StatusCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 181
    iput p3, p0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 192
    :cond_0
    sget-object p0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->PAYMENT_DECLINED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-object p0

    .line 191
    :cond_1
    sget-object p0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->TRANSIENT_ERROR:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-object p0

    .line 190
    :cond_2
    sget-object p0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->UNACTIVATED:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-object p0

    .line 189
    :cond_3
    sget-object p0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;
    .locals 1

    .line 158
    const-class v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;
    .locals 1

    .line 158
    sget-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->$VALUES:[Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    invoke-virtual {v0}, [Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 199
    iget v0, p0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->value:I

    return v0
.end method
