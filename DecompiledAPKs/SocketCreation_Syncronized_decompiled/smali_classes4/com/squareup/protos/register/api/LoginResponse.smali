.class public final Lcom/squareup/protos/register/api/LoginResponse;
.super Lcom/squareup/wire/Message;
.source "LoginResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/register/api/LoginResponse$ProtoAdapter_LoginResponse;,
        Lcom/squareup/protos/register/api/LoginResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "Lcom/squareup/protos/register/api/LoginResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_SKIP_TWO_FACTOR_ENROLL:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_GOOGLE_AUTHENTICATOR:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_SMS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAPTCHA_API_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_CAPTCHA_REQUIRED:Ljava/lang/Boolean;

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUIRES_TWO_FACTOR:Ljava/lang/Boolean;

.field public static final DEFAULT_SESSION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final can_skip_two_factor_enroll:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final can_use_google_authenticator:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final can_use_sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final captcha_api_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final captcha_required:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final error_alert:Lcom/squareup/protos/multipass/mobile/Alert;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.mobile.Alert#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final requires_two_factor:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final session_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final two_factor_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.TwoFactorDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final unit:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.register.api.Unit#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/register/api/LoginResponse$ProtoAdapter_LoginResponse;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/LoginResponse$ProtoAdapter_LoginResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/register/api/LoginResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_REQUIRES_TWO_FACTOR:Ljava/lang/Boolean;

    .line 36
    sput-object v0, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_CAN_SKIP_TWO_FACTOR_ENROLL:Ljava/lang/Boolean;

    .line 42
    sput-object v0, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_CAN_USE_GOOGLE_AUTHENTICATOR:Ljava/lang/Boolean;

    .line 44
    sput-object v0, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_CAN_USE_SMS:Ljava/lang/Boolean;

    .line 46
    sput-object v0, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_CAPTCHA_REQUIRED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/Alert;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/mobile/Alert;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 177
    sget-object v13, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/register/api/LoginResponse;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/Alert;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/Alert;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/mobile/Alert;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 185
    sget-object v0, Lcom/squareup/protos/register/api/LoginResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p13}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    const-string p1, "unit"

    .line 187
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    .line 188
    iput-object p3, p0, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    const-string p1, "two_factor_details"

    .line 189
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    .line 190
    iput-object p5, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    .line 191
    iput-object p6, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    .line 192
    iput-object p7, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    .line 193
    iput-object p8, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    .line 194
    iput-object p9, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    .line 195
    iput-object p10, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    .line 196
    iput-object p11, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    .line 197
    iput-object p12, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 222
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/register/api/LoginResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 223
    :cond_1
    check-cast p1, Lcom/squareup/protos/register/api/LoginResponse;

    .line 224
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/register/api/LoginResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    .line 226
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    .line 228
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    .line 236
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 241
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 243
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/mobile/Alert;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 256
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 2

    .line 202
    new-instance v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/LoginResponse$Builder;-><init>()V

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->session_token:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->unit:Ljava/util/List;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->requires_two_factor:Ljava/lang/Boolean;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->two_factor_details:Ljava/util/List;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_title:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_message:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_use_google_authenticator:Ljava/lang/Boolean;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_use_sms:Ljava/lang/Boolean;

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->captcha_required:Ljava/lang/Boolean;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->captcha_api_key:Ljava/lang/String;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/register/api/LoginResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginResponse;->newBuilder()Lcom/squareup/protos/register/api/LoginResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", session_token=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", requires_two_factor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 267
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", two_factor_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 268
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", can_skip_two_factor_enroll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    if-eqz v1, :cond_7

    const-string v1, ", error_alert="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 272
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", can_use_google_authenticator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", can_use_sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", captcha_required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", captcha_api_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoginResponse{"

    .line 276
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
