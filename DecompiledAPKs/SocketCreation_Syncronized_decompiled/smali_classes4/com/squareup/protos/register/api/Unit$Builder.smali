.class public final Lcom/squareup/protos/register/api/Unit$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Unit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/Unit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/Unit;",
        "Lcom/squareup/protos/register/api/Unit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_display_name:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public unit_display_name:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/Unit;
    .locals 7

    .line 175
    new-instance v6, Lcom/squareup/protos/register/api/Unit;

    iget-object v1, p0, Lcom/squareup/protos/register/api/Unit$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/register/api/Unit$Builder;->merchant_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/register/api/Unit$Builder;->merchant_display_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/register/api/Unit$Builder;->unit_display_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/register/api/Unit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/Unit$Builder;->build()Lcom/squareup/protos/register/api/Unit;

    move-result-object v0

    return-object v0
.end method

.method public merchant_display_name(Ljava/lang/String;)Lcom/squareup/protos/register/api/Unit$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/register/api/Unit$Builder;->merchant_display_name:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/Unit$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/register/api/Unit$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_display_name(Ljava/lang/String;)Lcom/squareup/protos/register/api/Unit$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/register/api/Unit$Builder;->unit_display_name:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/Unit$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/register/api/Unit$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
