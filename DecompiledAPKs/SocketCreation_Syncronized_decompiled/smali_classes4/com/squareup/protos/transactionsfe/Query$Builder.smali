.class public final Lcom/squareup/protos/transactionsfe/Query$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/transactionsfe/Query;",
        "Lcom/squareup/protos/transactionsfe/Query$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public query_string:Ljava/lang/String;

.field public search_instrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/transactionsfe/Query;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/protos/transactionsfe/Query;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/Query$Builder;->query_string:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/Query$Builder;->search_instrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/transactionsfe/Query;-><init>(Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/Query$Builder;->build()Lcom/squareup/protos/transactionsfe/Query;

    move-result-object v0

    return-object v0
.end method

.method public query_string(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/Query$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/Query$Builder;->query_string:Ljava/lang/String;

    return-object p0
.end method

.method public search_instrument(Lcom/squareup/protos/transactionsfe/SearchInstrument;)Lcom/squareup/protos/transactionsfe/Query$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/Query$Builder;->search_instrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    return-object p0
.end method
