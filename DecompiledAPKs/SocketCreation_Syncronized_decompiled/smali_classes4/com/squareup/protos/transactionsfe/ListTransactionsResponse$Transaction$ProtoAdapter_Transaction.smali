.class final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Transaction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1015
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1054
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;-><init>()V

    .line 1055
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1056
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1078
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1076
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->search_match:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1075
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_tip_expiring(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto :goto_0

    .line 1074
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_awaiting_tip(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto :goto_0

    .line 1073
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_fully_voided(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto :goto_0

    .line 1072
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_error(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto :goto_0

    .line 1071
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_related_transactions(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto :goto_0

    .line 1070
    :pswitch_6
    iget-object v3, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->tender_information:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1069
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto :goto_0

    .line 1068
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto :goto_0

    .line 1067
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto/16 :goto_0

    .line 1061
    :pswitch_a
    :try_start_0
    sget-object v4, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->type(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 1063
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1058
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    goto/16 :goto_0

    .line 1082
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1083
    invoke-virtual {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1013
    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1037
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1038
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1039
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1040
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1041
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1042
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1043
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1044
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1045
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1046
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1047
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1048
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1049
    invoke-virtual {p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1013
    check-cast p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;)I
    .locals 4

    .line 1020
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v3, 0x2

    .line 1021
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x3

    .line 1022
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 1023
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    const/4 v3, 0x5

    .line 1024
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1025
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 1026
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 1027
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    const/16 v3, 0x9

    .line 1028
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 1029
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    const/16 v3, 0xb

    .line 1030
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1031
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    const/16 v3, 0xc

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1032
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1013
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;->encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
    .locals 2

    .line 1088
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    move-result-object p1

    .line 1089
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1090
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 1091
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 1092
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->tender_information:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1093
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->search_match:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1094
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1095
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1013
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;->redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    move-result-object p1

    return-object p1
.end method
