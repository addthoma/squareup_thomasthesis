.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;
.super Lcom/squareup/wire/Message;
.source "ListTransactionsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EMPLOYEE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final date_range:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final employee_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.transactionsfe.ListTransactionsRequest$PaginationParameters#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final query:Lcom/squareup/protos/transactionsfe/Query;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.transactionsfe.Query#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final unit_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/transactionsfe/Query;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTimeInterval;",
            "Lcom/squareup/protos/transactionsfe/Query;",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;",
            ")V"
        }
    .end annotation

    .line 105
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/transactionsfe/Query;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/transactionsfe/Query;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTimeInterval;",
            "Lcom/squareup/protos/transactionsfe/Query;",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 111
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    const-string p1, "unit_token"

    .line 113
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    .line 114
    iput-object p3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    .line 115
    iput-object p4, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 116
    iput-object p5, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    .line 117
    iput-object p6, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    .line 140
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    iget-object p1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    .line 144
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 149
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/transactionsfe/Query;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 158
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    .locals 2

    .line 122
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->unit_token:Ljava/util/List;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->employee_token:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query:Lcom/squareup/protos/transactionsfe/Query;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", employee_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_3

    const-string v1, ", date_range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    if-eqz v1, :cond_4

    const-string v1, ", query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    if-eqz v1, :cond_5

    const-string v1, ", pagination_parameters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListTransactionsRequest{"

    .line 172
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
