.class public Lcom/squareup/picasso/pollexor/PollexorRequestTransformer;
.super Ljava/lang/Object;
.source "PollexorRequestTransformer.java"

# interfaces
.implements Lcom/squareup/picasso/Picasso$RequestTransformer;


# instance fields
.field private final thumbor:Lcom/squareup/pollexor/Thumbor;

.field private final thumborAuthority:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/pollexor/Thumbor;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/picasso/pollexor/PollexorRequestTransformer;->thumbor:Lcom/squareup/pollexor/Thumbor;

    .line 26
    invoke-virtual {p1}, Lcom/squareup/pollexor/Thumbor;->getHost()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/picasso/pollexor/PollexorRequestTransformer;->thumborAuthority:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public transformRequest(Lcom/squareup/picasso/Request;)Lcom/squareup/picasso/Request;
    .locals 4

    .line 30
    iget v0, p1, Lcom/squareup/picasso/Request;->resourceId:I

    if-eqz v0, :cond_0

    return-object p1

    .line 33
    :cond_0
    iget-object v0, p1, Lcom/squareup/picasso/Request;->uri:Landroid/net/Uri;

    .line 34
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    .line 35
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "http"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object p1

    .line 41
    :cond_1
    iget-object v1, p0, Lcom/squareup/picasso/pollexor/PollexorRequestTransformer;->thumborAuthority:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-object p1

    .line 46
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/picasso/Request;->buildUpon()Lcom/squareup/picasso/Request$Builder;

    move-result-object v1

    .line 49
    iget-object v2, p0, Lcom/squareup/picasso/pollexor/PollexorRequestTransformer;->thumbor:Lcom/squareup/pollexor/Thumbor;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/pollexor/Thumbor;->buildImage(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object v0

    .line 52
    invoke-virtual {p1}, Lcom/squareup/picasso/Request;->hasSize()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 53
    iget v2, p1, Lcom/squareup/picasso/Request;->targetWidth:I

    iget v3, p1, Lcom/squareup/picasso/Request;->targetHeight:I

    invoke-virtual {v0, v2, v3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->resize(II)Lcom/squareup/pollexor/ThumborUrlBuilder;

    .line 54
    invoke-virtual {v1}, Lcom/squareup/picasso/Request$Builder;->clearResize()Lcom/squareup/picasso/Request$Builder;

    .line 58
    :cond_3
    iget-boolean p1, p1, Lcom/squareup/picasso/Request;->centerInside:Z

    if-eqz p1, :cond_4

    .line 59
    invoke-virtual {v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->fitIn()Lcom/squareup/pollexor/ThumborUrlBuilder;

    .line 60
    invoke-virtual {v1}, Lcom/squareup/picasso/Request$Builder;->clearCenterInside()Lcom/squareup/picasso/Request$Builder;

    :cond_4
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 64
    sget-object v3, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->WEBP:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    invoke-static {v3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->format(Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v2

    invoke-virtual {v0, p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->filter([Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    .line 67
    invoke-virtual {v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrl()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/picasso/Request$Builder;->setUri(Landroid/net/Uri;)Lcom/squareup/picasso/Request$Builder;

    .line 69
    invoke-virtual {v1}, Lcom/squareup/picasso/Request$Builder;->build()Lcom/squareup/picasso/Request;

    move-result-object p1

    return-object p1
.end method
