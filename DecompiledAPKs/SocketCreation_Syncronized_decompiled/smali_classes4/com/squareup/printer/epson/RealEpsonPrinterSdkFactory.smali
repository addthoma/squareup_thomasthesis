.class public final Lcom/squareup/printer/epson/RealEpsonPrinterSdkFactory;
.super Ljava/lang/Object;
.source "EpsonPrinterSdkFactory.kt"

# interfaces
.implements Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/printer/epson/RealEpsonPrinterSdkFactory;",
        "Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;",
        "context",
        "Landroid/app/Application;",
        "(Landroid/app/Application;)V",
        "create",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "printerCode",
        "",
        "lang",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdkFactory;->context:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public create(II)Lcom/squareup/printer/epson/EpsonPrinterSdk;
    .locals 2

    .line 18
    new-instance v0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;

    iget-object v1, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdkFactory;->context:Landroid/app/Application;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;-><init>(IILandroid/app/Application;)V

    check-cast v0, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    return-object v0
.end method
