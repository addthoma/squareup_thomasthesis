.class public final Lcom/squareup/printer/epson/EpsonPrinter;
.super Lcom/squareup/print/HardwarePrinter;
.source "EpsonPrinter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/printer/epson/EpsonPrinter$Factory;,
        Lcom/squareup/printer/epson/EpsonPrinter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEpsonPrinter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EpsonPrinter.kt\ncom/squareup/printer/epson/EpsonPrinter\n*L\n1#1,121:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001b2\u00020\u0001:\u0002\u001b\u001cB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\u000c8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000f\u0010\u0010\u001a\u0004\u0008\r\u0010\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinter;",
        "Lcom/squareup/print/HardwarePrinter;",
        "epsonPrinterInfo",
        "Lcom/squareup/printer/epson/EpsonPrinterInfo;",
        "epsonPrinterConnection",
        "Lcom/squareup/printer/epson/EpsonPrinterConnection;",
        "epsonPrinterSdkFactory",
        "Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;Lcom/squareup/util/Clock;)V",
        "epsonPrinterSdk",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "getEpsonPrinterSdk",
        "()Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "epsonPrinterSdk$delegate",
        "Lkotlin/Lazy;",
        "performOpenCashDrawer",
        "",
        "performPrint",
        "Lcom/squareup/print/PrintJob$PrintAttempt;",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "printTimingData",
        "Lcom/squareup/print/PrintTimingData;",
        "renderedRows",
        "Lcom/squareup/print/text/RenderedRows;",
        "Companion",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/printer/epson/EpsonPrinter$Companion;

.field private static final MANUFACTURER:Ljava/lang/String; = "Epson"


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final epsonPrinterConnection:Lcom/squareup/printer/epson/EpsonPrinterConnection;

.field private final epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

.field private final epsonPrinterSdk$delegate:Lkotlin/Lazy;

.field private final epsonPrinterSdkFactory:Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/printer/epson/EpsonPrinter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinter;->Companion:Lcom/squareup/printer/epson/EpsonPrinter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;Lcom/squareup/util/Clock;)V
    .locals 10

    const-string v0, "epsonPrinterInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterConnection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterSdkFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getEpsonPrinterAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;->getPrinterName()Ljava/lang/String;

    move-result-object v3

    .line 22
    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getConnectionType()Lcom/squareup/print/ConnectionType;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getEpsonPrinterAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;->isImpactPrinter()Z

    move-result v0

    xor-int/lit8 v5, v0, 0x1

    const/4 v6, 0x1

    .line 23
    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v7, v0

    .line 24
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinter;->Companion:Lcom/squareup/printer/epson/EpsonPrinter$Companion;

    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getEpsonPrinterAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;->getPrinterSpecs()Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/printer/epson/EpsonPrinter$Companion;->access$getBitmapRenderingSpecs(Lcom/squareup/printer/epson/EpsonPrinter$Companion;Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;)Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    move-result-object v8

    .line 25
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinter;->Companion:Lcom/squareup/printer/epson/EpsonPrinter$Companion;

    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getEpsonPrinterAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;->getPrinterSpecs()Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/printer/epson/EpsonPrinter$Companion;->access$getTextRenderingSpecs(Lcom/squareup/printer/epson/EpsonPrinter$Companion;Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;)Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;

    move-result-object v9

    const-string v2, "Epson"

    move-object v1, p0

    .line 20
    invoke-direct/range {v1 .. v9}, Lcom/squareup/print/HardwarePrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;ZZLjava/lang/String;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;)V

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterConnection:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterSdkFactory:Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;

    iput-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinter;->clock:Lcom/squareup/util/Clock;

    .line 32
    new-instance p1, Lcom/squareup/printer/epson/EpsonPrinter$epsonPrinterSdk$2;

    invoke-direct {p1, p0}, Lcom/squareup/printer/epson/EpsonPrinter$epsonPrinterSdk$2;-><init>(Lcom/squareup/printer/epson/EpsonPrinter;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterSdk$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getClock$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/util/Clock;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/printer/epson/EpsonPrinter;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method public static final synthetic access$getEpsonPrinterConnection$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterConnection;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterConnection:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    return-object p0
.end method

.method public static final synthetic access$getEpsonPrinterInfo$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterInfo;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    return-object p0
.end method

.method public static final synthetic access$getEpsonPrinterSdk$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterSdk;
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/printer/epson/EpsonPrinter;->getEpsonPrinterSdk()Lcom/squareup/printer/epson/EpsonPrinterSdk;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getEpsonPrinterSdkFactory$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterSdkFactory:Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;

    return-object p0
.end method

.method private final getEpsonPrinterSdk()Lcom/squareup/printer/epson/EpsonPrinterSdk;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinter;->epsonPrinterSdk$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    return-object v0
.end method


# virtual methods
.method public performOpenCashDrawer()V
    .locals 3

    .line 39
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinter$performOpenCashDrawer$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/printer/epson/EpsonPrinter$performOpenCashDrawer$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinter;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2, v1}, Lkotlinx/coroutines/BuildersKt;->runBlocking$default(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public performPrint(Landroid/graphics/Bitmap;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;
    .locals 2

    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printTimingData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, p1, v1}, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinter;Lcom/squareup/print/PrintTimingData;Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    const/4 p1, 0x1

    invoke-static {v1, v0, p1, v1}, Lkotlinx/coroutines/BuildersKt;->runBlocking$default(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PrintJob$PrintAttempt;

    return-object p1
.end method

.method public performPrint(Lcom/squareup/print/text/RenderedRows;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;
    .locals 2

    const-string v0, "renderedRows"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printTimingData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, p1, v1}, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;-><init>(Lcom/squareup/printer/epson/EpsonPrinter;Lcom/squareup/print/PrintTimingData;Lcom/squareup/print/text/RenderedRows;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    const/4 p1, 0x1

    invoke-static {v1, v0, p1, v1}, Lkotlinx/coroutines/BuildersKt;->runBlocking$default(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PrintJob$PrintAttempt;

    return-object p1
.end method
