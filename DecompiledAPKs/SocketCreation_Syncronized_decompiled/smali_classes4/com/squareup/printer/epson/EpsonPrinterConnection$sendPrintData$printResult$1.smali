.class final Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "EpsonPrinterConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/EpsonPrinterConnection;->sendPrintData(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.printer.epson.EpsonPrinterConnection$sendPrintData$printResult$1"
    f = "EpsonPrinterConnection.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x1,
        0x1,
        0x1
    }
    l = {
        0x5d,
        0x77
    }
    m = "invokeSuspend"
    n = {
        "$this$withTimeoutOrNull",
        "status",
        "connectAttemptCount",
        "$this$withTimeoutOrNull",
        "status",
        "connectAttemptCount"
    }
    s = {
        "L$0",
        "L$1",
        "I$0",
        "L$0",
        "L$1",
        "I$0"
    }
.end annotation


# instance fields
.field final synthetic $addPrintData:Lkotlin/jvm/functions/Function1;

.field final synthetic $clock:Lcom/squareup/util/Clock;

.field final synthetic $deferredPrintResult:Lkotlinx/coroutines/CompletableDeferred;

.field final synthetic $epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

.field final synthetic $epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

.field final synthetic $printTimingData:Lcom/squareup/print/PrintTimingData;

.field I$0:I

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlinx/coroutines/CompletableDeferred;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$deferredPrintResult:Lkotlinx/coroutines/CompletableDeferred;

    iput-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iput-object p5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    iput-object p6, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$clock:Lcom/squareup/util/Clock;

    iput-object p7, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$addPrintData:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p8}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    iget-object v3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iget-object v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$deferredPrintResult:Lkotlinx/coroutines/CompletableDeferred;

    iget-object v5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iget-object v6, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    iget-object v7, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$clock:Lcom/squareup/util/Clock;

    iget-object v8, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$addPrintData:Lkotlin/jvm/functions/Function1;

    move-object v1, v0

    move-object v9, p2

    invoke-direct/range {v1 .. v9}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlinx/coroutines/CompletableDeferred;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 67
    iget v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->label:I

    const/4 v2, -0x2

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v4, :cond_1

    if-ne v1, v3, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->I$0:I

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/printer/epson/PrinterStatusInfo;

    iget-object v3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$0:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v0

    goto/16 :goto_2

    :catch_0
    move-exception p1

    move-object v0, p1

    move-object p1, v1

    goto/16 :goto_3

    :catch_1
    move-exception p1

    move-object v0, p1

    move-object p1, v1

    goto/16 :goto_4

    .line 136
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 67
    :cond_1
    iget v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->I$0:I

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/printer/epson/PrinterStatusInfo;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 68
    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    new-instance v5, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1$1;

    invoke-direct {v5, p0}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;)V

    check-cast v5, Lkotlin/jvm/functions/Function3;

    invoke-interface {p1, v5}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->setReceiveEventListener(Lkotlin/jvm/functions/Function3;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    invoke-interface {p1}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->getPrinterStatusInfo()Lcom/squareup/printer/epson/PrinterStatusInfo;

    move-result-object p1

    const/4 v5, 0x0

    .line 86
    iget-object v6, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    invoke-static {v6}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->access$getFeatures$p(Lcom/squareup/printer/epson/EpsonPrinterConnection;)Lcom/squareup/settings/server/Features;

    move-result-object v6

    sget-object v7, Lcom/squareup/settings/server/Features$Feature;->RETAIN_PRINTER_CONNECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v6, v7}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lcom/squareup/printer/epson/PrinterStatusInfo;->getConnection()I

    move-result v6

    if-ne v6, v4, :cond_3

    const/4 v8, -0x2

    goto :goto_1

    .line 89
    :cond_3
    iget-object v7, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    .line 90
    iget-object v8, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    .line 91
    iget-object v9, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    .line 92
    iget-object v10, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    .line 93
    iget-object v11, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$clock:Lcom/squareup/util/Clock;

    iput-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$0:Ljava/lang/Object;

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$1:Ljava/lang/Object;

    iput v5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->I$0:I

    iput v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->label:I

    move-object v12, p0

    .line 89
    invoke-virtual/range {v7 .. v12}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->connectWithRetry(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_4

    return-object v0

    .line 93
    :cond_4
    :goto_0
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v8

    .line 97
    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    invoke-interface {p1}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->getPrinterStatusInfo()Lcom/squareup/printer/epson/PrinterStatusInfo;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/squareup/printer/epson/PrinterStatusInfo;->getConnection()I

    move-result v5

    if-eq v5, v4, :cond_5

    .line 99
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    .line 100
    sget-object v6, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 101
    invoke-static {p1}, Lcom/squareup/printer/epson/EpsonPrinterExtensionsKt;->toFormattedString(Lcom/squareup/printer/epson/PrinterStatusInfo;)Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    const/4 p1, -0x1

    .line 103
    invoke-static {p1}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v10

    const/16 v11, 0x8

    const/4 v12, 0x0

    move-object v5, v0

    .line 99
    invoke-direct/range {v5 .. v12}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0

    .line 109
    :cond_5
    :goto_1
    :try_start_1
    iget-object v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$addPrintData:Lkotlin/jvm/functions/Function1;

    iget-object v5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    invoke-interface {v4, v5}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    if-eqz v4, :cond_6

    iget-object v5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$clock:Lcom/squareup/util/Clock;

    invoke-virtual {v4, v5}, Lcom/squareup/print/PrintTimingData;->sendingDataToPrinter(Lcom/squareup/util/Clock;)V

    .line 111
    :cond_6
    iget-object v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    const/16 v5, 0x3a98

    invoke-interface {v4, v5}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->sendData(I)V

    .line 119
    iget-object v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$deferredPrintResult:Lkotlinx/coroutines/CompletableDeferred;

    iput-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$0:Ljava/lang/Object;

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->L$1:Ljava/lang/Object;

    iput v8, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->I$0:I

    iput v3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->label:I

    invoke-interface {v4, p0}, Lkotlinx/coroutines/CompletableDeferred;->await(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    if-ne v1, v0, :cond_7

    return-object v0

    :cond_7
    move v6, v8

    move-object v13, v1

    move-object v1, p1

    move-object p1, v13

    .line 67
    :goto_2
    :try_start_2
    move-object v3, p1

    check-cast v3, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1b

    const/4 v10, 0x0

    .line 120
    invoke-static/range {v3 .. v10}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->copy$default(Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    move-result-object p1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, p1

    goto :goto_5

    :catch_2
    move-exception v0

    .line 130
    :goto_3
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    check-cast v0, Ljava/lang/Exception;

    const-string v3, "TimeoutException from Printer::finalize()"

    invoke-static {v1, v0, v3}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->access$debug(Lcom/squareup/printer/epson/EpsonPrinterConnection;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 131
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    .line 132
    sget-object v5, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_UNRECOVERABLE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 133
    invoke-static {p1}, Lcom/squareup/printer/epson/EpsonPrinterExtensionsKt;->toFormattedString(Lcom/squareup/printer/epson/PrinterStatusInfo;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 134
    invoke-static {v2}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v9

    const/16 v10, 0xc

    const/4 v11, 0x0

    move-object v4, v0

    .line 131
    invoke-direct/range {v4 .. v11}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_5

    :catch_3
    move-exception v0

    .line 122
    :goto_4
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    .line 123
    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    check-cast v0, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error during print attempt: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->access$debug(Lcom/squareup/printer/epson/EpsonPrinterConnection;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    .line 125
    sget-object v2, Lcom/squareup/printer/epson/EpsonCallbackMapper;->INSTANCE:Lcom/squareup/printer/epson/EpsonCallbackMapper;

    invoke-virtual {v2, v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper;->classifyEpsonException(I)Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    move-result-object v5

    .line 126
    invoke-static {p1}, Lcom/squareup/printer/epson/EpsonPrinterExtensionsKt;->toFormattedString(Lcom/squareup/printer/epson/PrinterStatusInfo;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 127
    invoke-static {v1}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v9

    const/16 v10, 0xc

    const/4 v11, 0x0

    move-object v4, v0

    .line 124
    invoke-direct/range {v4 .. v11}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_5
    return-object v0
.end method
