.class public Lcom/squareup/request/RequestMessages;
.super Ljava/lang/Object;
.source "RequestMessages.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final defaultFailureTitle:I

.field private final dismissButton:I

.field private final loadingMessage:I

.field private final networkErrorMessage:I

.field private final networkErrorTitle:I

.field protected final res:Lcom/squareup/util/Res;

.field private final retryButton:I

.field private final serverErrorMessage:I

.field private final serverErrorTitle:I


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;I)V
    .locals 1

    const/4 v0, -0x1

    .line 50
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/request/RequestMessages;-><init>(Lcom/squareup/util/Res;II)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;II)V
    .locals 10

    .line 41
    sget v4, Lcom/squareup/common/strings/R$string;->network_error_title:I

    sget v5, Lcom/squareup/common/strings/R$string;->network_error_message:I

    sget v6, Lcom/squareup/common/strings/R$string;->server_error_title:I

    sget v7, Lcom/squareup/common/strings/R$string;->server_error_message:I

    sget v8, Lcom/squareup/common/strings/R$string;->retry:I

    sget v9, Lcom/squareup/common/strings/R$string;->dismiss:I

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v9}, Lcom/squareup/request/RequestMessages;-><init>(Lcom/squareup/util/Res;IIIIIIII)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;IIIIIIII)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    .line 29
    iput p2, p0, Lcom/squareup/request/RequestMessages;->loadingMessage:I

    .line 30
    iput p3, p0, Lcom/squareup/request/RequestMessages;->defaultFailureTitle:I

    .line 31
    iput p4, p0, Lcom/squareup/request/RequestMessages;->networkErrorTitle:I

    .line 32
    iput p5, p0, Lcom/squareup/request/RequestMessages;->networkErrorMessage:I

    .line 33
    iput p6, p0, Lcom/squareup/request/RequestMessages;->serverErrorTitle:I

    .line 34
    iput p7, p0, Lcom/squareup/request/RequestMessages;->serverErrorMessage:I

    .line 35
    iput p8, p0, Lcom/squareup/request/RequestMessages;->retryButton:I

    .line 36
    iput p9, p0, Lcom/squareup/request/RequestMessages;->dismissButton:I

    return-void
.end method


# virtual methods
.method public getDefaultFailureTitle()Ljava/lang/String;
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->defaultFailureTitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDismissButton()Ljava/lang/String;
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->dismissButton:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLoadingCompleteMessage()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getLoadingMessage()Ljava/lang/String;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->loadingMessage:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkErrorMessage()Ljava/lang/String;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->networkErrorMessage:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkErrorTitle()Ljava/lang/String;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->networkErrorTitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRetryButton()Ljava/lang/String;
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->retryButton:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServerErrorMessage()Ljava/lang/String;
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->serverErrorMessage:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServerErrorTitle()Ljava/lang/String;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/request/RequestMessages;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/request/RequestMessages;->serverErrorTitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
