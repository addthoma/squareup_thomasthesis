.class public Lcom/squareup/prices/PricingEngineService;
.super Ljava/lang/Object;
.source "PricingEngineService.java"


# instance fields
.field private final LEGACY_DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field private final clock:Lcom/squareup/util/Clock;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final detailsLoader:Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lrx/Scheduler;

.field private final pricingEngine:Lcom/squareup/shared/pricing/engine/PricingEngine;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;Lcom/squareup/shared/pricing/engine/PricingEngine;Lrx/Scheduler;)V
    .locals 3
    .param p6    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/squareup/prices/PricingEngineService;->LEGACY_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 58
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineService;->clock:Lcom/squareup/util/Clock;

    .line 59
    iput-object p2, p0, Lcom/squareup/prices/PricingEngineService;->cogs:Lcom/squareup/cogs/Cogs;

    .line 60
    iput-object p3, p0, Lcom/squareup/prices/PricingEngineService;->features:Lcom/squareup/settings/server/Features;

    .line 61
    iput-object p4, p0, Lcom/squareup/prices/PricingEngineService;->detailsLoader:Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    .line 62
    iput-object p5, p0, Lcom/squareup/prices/PricingEngineService;->pricingEngine:Lcom/squareup/shared/pricing/engine/PricingEngine;

    .line 63
    iput-object p6, p0, Lcom/squareup/prices/PricingEngineService;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method static synthetic lambda$null$0(Lrx/SingleSubscriber;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 87
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lrx/SingleSubscriber;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$4(Ljava/util/Set;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/Map;
    .locals 1

    .line 124
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-interface {p1, v0, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$rulesForOrder$2(Lcom/squareup/shared/pricing/engine/PricingEngineResult;)Ljava/util/Set;
    .locals 4

    .line 94
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 95
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->getBlocks()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 96
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;

    .line 97
    invoke-virtual {v3}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getDiscountIds()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->getWholePurchaseApplications()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;

    .line 101
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->getDiscountId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method static synthetic lambda$rulesForOrder$3(Lcom/squareup/payment/Order;Ljava/util/Set;)Ljava/util/Set;
    .locals 3

    .line 110
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 111
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 112
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->getCatalogId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->isAmountDiscount()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method static synthetic lambda$rulesForOrder$6(Lcom/squareup/prices/PricingEngineServiceResult$Builder;Lcom/squareup/shared/pricing/engine/PricingEngineResult;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    .locals 0

    .line 130
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->getBlocks()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->setBlocks(Ljava/util/Map;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$rulesForOrder$7(Lcom/squareup/prices/PricingEngineServiceResult$Builder;Lcom/squareup/shared/pricing/engine/PricingEngineResult;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    .locals 0

    .line 131
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/PricingEngineResult;->getWholePurchaseApplications()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->setWholeBlocks(Ljava/util/List;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$rulesForOrder$1$PricingEngineService(Ljava/util/List;Lrx/SingleSubscriber;)V
    .locals 4

    .line 86
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService;->pricingEngine:Lcom/squareup/shared/pricing/engine/PricingEngine;

    iget-object v1, p0, Lcom/squareup/prices/PricingEngineService;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    new-instance v2, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper;

    iget-object v3, p0, Lcom/squareup/prices/PricingEngineService;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-direct {v2, v3}, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper;-><init>(Lcom/squareup/shared/catalog/Catalog;)V

    new-instance v3, Lcom/squareup/prices/-$$Lambda$PricingEngineService$A3q7JdZm-aBkjwnL0QzIojlKShw;

    invoke-direct {v3, p2}, Lcom/squareup/prices/-$$Lambda$PricingEngineService$A3q7JdZm-aBkjwnL0QzIojlKShw;-><init>(Lrx/SingleSubscriber;)V

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/squareup/shared/pricing/engine/PricingEngine;->applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public synthetic lambda$rulesForOrder$5$PricingEngineService(Ljava/util/Set;)Lrx/Single;
    .locals 2

    .line 122
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    invoke-static {p1}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/prices/-$$Lambda$PricingEngineService$xcPo9uPhyMkInI416MIilst0w54;

    invoke-direct {v1, p1}, Lcom/squareup/prices/-$$Lambda$PricingEngineService$xcPo9uPhyMkInI416MIilst0w54;-><init>(Ljava/util/Set;)V

    .line 124
    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService;->mainScheduler:Lrx/Scheduler;

    .line 125
    invoke-virtual {p1, v0}, Lrx/Single;->observeOn(Lrx/Scheduler;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public rulesForOrder(Lcom/squareup/payment/Order;)Lrx/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/prices/PricingEngineServiceResult;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 73
    invoke-static {p1}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    return-object p1

    :cond_0
    const-wide/16 v0, 0x0

    .line 79
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 80
    invoke-virtual {p1, v0, v0, v0}, Lcom/squareup/payment/Order;->getCartProtoForBill(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/squareup/prices/PricingEngineService;->detailsLoader:Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    iget-object v2, p0, Lcom/squareup/prices/PricingEngineService;->LEGACY_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;->load(Lcom/squareup/protos/client/bills/Cart;Ljava/text/SimpleDateFormat;)Ljava/util/List;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/squareup/prices/-$$Lambda$PricingEngineService$4-3Px-vYmZP9xekCZ-MI1otcPYA;

    invoke-direct {v1, p0, v0}, Lcom/squareup/prices/-$$Lambda$PricingEngineService$4-3Px-vYmZP9xekCZ-MI1otcPYA;-><init>(Lcom/squareup/prices/PricingEngineService;Ljava/util/List;)V

    invoke-static {v1}, Lrx/Single;->create(Lrx/Single$OnSubscribe;)Lrx/Single;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lrx/Single;->cache()Lrx/Single;

    move-result-object v0

    .line 93
    sget-object v1, Lcom/squareup/prices/-$$Lambda$PricingEngineService$yEff6uycnmqiJZ4Tbz9FDn62MmM;->INSTANCE:Lcom/squareup/prices/-$$Lambda$PricingEngineService$yEff6uycnmqiJZ4Tbz9FDn62MmM;

    invoke-virtual {v0, v1}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Lrx/Single;->cache()Lrx/Single;

    move-result-object v1

    .line 109
    new-instance v2, Lcom/squareup/prices/-$$Lambda$PricingEngineService$5SJ7Re2k80U6ethVZPkhTAEUdJI;

    invoke-direct {v2, p1}, Lcom/squareup/prices/-$$Lambda$PricingEngineService$5SJ7Re2k80U6ethVZPkhTAEUdJI;-><init>(Lcom/squareup/payment/Order;)V

    invoke-virtual {v1, v2}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object v2

    .line 117
    invoke-virtual {v2}, Lrx/Single;->cache()Lrx/Single;

    move-result-object v2

    .line 121
    new-instance v3, Lcom/squareup/prices/-$$Lambda$PricingEngineService$lfpY7_OUeklqvudqCGs2107M1Xo;

    invoke-direct {v3, p0}, Lcom/squareup/prices/-$$Lambda$PricingEngineService$lfpY7_OUeklqvudqCGs2107M1Xo;-><init>(Lcom/squareup/prices/PricingEngineService;)V

    invoke-virtual {v1, v3}, Lrx/Single;->flatMap(Lrx/functions/Func1;)Lrx/Single;

    move-result-object v1

    .line 126
    invoke-virtual {v1}, Lrx/Single;->cache()Lrx/Single;

    move-result-object v1

    .line 129
    new-instance v3, Lcom/squareup/prices/PricingEngineServiceResult$Builder;

    invoke-direct {v3}, Lcom/squareup/prices/PricingEngineServiceResult$Builder;-><init>()V

    invoke-virtual {v3, p1}, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->setOrder(Lcom/squareup/payment/Order;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;

    move-result-object p1

    invoke-static {p1}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    sget-object v3, Lcom/squareup/prices/-$$Lambda$PricingEngineService$s34kyQtpDeyunqkfVYjf-O44XBc;->INSTANCE:Lcom/squareup/prices/-$$Lambda$PricingEngineService$s34kyQtpDeyunqkfVYjf-O44XBc;

    .line 130
    invoke-virtual {p1, v0, v3}, Lrx/Single;->zipWith(Lrx/Single;Lrx/functions/Func2;)Lrx/Single;

    move-result-object p1

    sget-object v3, Lcom/squareup/prices/-$$Lambda$PricingEngineService$EBue7_L-jF93rFlvn4SUmA0O4IQ;->INSTANCE:Lcom/squareup/prices/-$$Lambda$PricingEngineService$EBue7_L-jF93rFlvn4SUmA0O4IQ;

    .line 131
    invoke-virtual {p1, v0, v3}, Lrx/Single;->zipWith(Lrx/Single;Lrx/functions/Func2;)Lrx/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;->INSTANCE:Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;

    .line 132
    invoke-virtual {p1, v2, v0}, Lrx/Single;->zipWith(Lrx/Single;Lrx/functions/Func2;)Lrx/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/prices/-$$Lambda$gEKRKbgiVXovDXMSM4m8wvyO6wA;->INSTANCE:Lcom/squareup/prices/-$$Lambda$gEKRKbgiVXovDXMSM4m8wvyO6wA;

    .line 133
    invoke-virtual {p1, v1, v0}, Lrx/Single;->zipWith(Lrx/Single;Lrx/functions/Func2;)Lrx/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/prices/-$$Lambda$dRiFtoHqQTCwWZmChj_CxcMKi64;->INSTANCE:Lcom/squareup/prices/-$$Lambda$dRiFtoHqQTCwWZmChj_CxcMKi64;

    .line 134
    invoke-virtual {p1, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Single;->cache()Lrx/Single;

    move-result-object p1

    return-object p1
.end method
