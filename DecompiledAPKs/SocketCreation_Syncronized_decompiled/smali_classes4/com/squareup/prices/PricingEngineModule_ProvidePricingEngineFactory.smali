.class public final Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;
.super Ljava/lang/Object;
.source "PricingEngineModule_ProvidePricingEngineFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/shared/pricing/engine/PricingEngine;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePricingEngine(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/shared/pricing/engine/PricingEngine;
    .locals 0

    .line 37
    invoke-static {p0, p1}, Lcom/squareup/prices/PricingEngineModule;->providePricingEngine(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/shared/pricing/engine/PricingEngine;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/pricing/engine/PricingEngine;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/shared/pricing/engine/PricingEngine;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Clock;

    invoke-static {v0, v1}, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;->providePricingEngine(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/shared/pricing/engine/PricingEngine;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/prices/PricingEngineModule_ProvidePricingEngineFactory;->get()Lcom/squareup/shared/pricing/engine/PricingEngine;

    move-result-object v0

    return-object v0
.end method
