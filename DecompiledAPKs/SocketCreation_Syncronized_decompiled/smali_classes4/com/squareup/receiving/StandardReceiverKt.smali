.class public final Lcom/squareup/receiving/StandardReceiverKt;
.super Ljava/lang/Object;
.source "StandardReceiver.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aH\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b\u001a\u001c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0002\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\r\u00a8\u0006\u000e"
    }
    d2 = {
        "retryExponentialBackoff",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "T",
        "numRetries",
        "",
        "delay",
        "",
        "timeUnit",
        "Ljava/util/concurrent/TimeUnit;",
        "scheduler",
        "Lio/reactivex/Scheduler;",
        "toSuccessOrFailure",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final retryExponentialBackoff(Lio/reactivex/Single;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "$this$retryExponentialBackoff"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeUnit"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    sget-object v0, Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$1;->INSTANCE:Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    .line 212
    sget-object v0, Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$2;->INSTANCE:Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$2;

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    move v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 210
    invoke-static/range {v1 .. v6}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenErrorFlowable(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;

    move-result-object p1

    .line 209
    invoke-virtual {p0, p1}, Lio/reactivex/Single;->retryWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    .line 214
    sget-object p1, Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;->INSTANCE:Lcom/squareup/receiving/StandardReceiverKt$retryExponentialBackoff$3;

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "this\n      .map {\n      \u2026hrow it\n        }\n      }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toSuccessOrFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+TT;>;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toSuccessOrFailure"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;->getResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    goto :goto_0

    .line 230
    :cond_0
    new-instance v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;-><init>(Lcom/squareup/receiving/ReceivedResponse;)V

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    :goto_0
    return-object v0
.end method
