.class public final Lcom/squareup/receiving/ReceivedResponse$Companion;
.super Ljava/lang/Object;
.source "ReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receiving/ReceivedResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceivedResponse.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceivedResponse.kt\ncom/squareup/receiving/ReceivedResponse$Companion\n*L\n1#1,239:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u001b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JC\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u0002\u00a2\u0006\u0002\u0010\u000fJY\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00050\u00120\u0011\"\u0008\u0008\u0001\u0010\u0005*\u00020\u00012\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u00150\u0014\u00a2\u0006\u0002\u0010\u0016Ja\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0012\"\u0008\u0008\u0001\u0010\u0005*\u00020\u00012\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u00150\u00142\u000e\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00050\u0019H\u0000\u00a2\u0006\u0004\u0008\u001a\u0010\u001bJA\u0010\u001c\u001a\u0004\u0018\u0001H\u0005\"\u0004\u0008\u0001\u0010\u00052\u000c\u0010\u001d\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u001e2\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u0002\u00a2\u0006\u0002\u0010\u001fJ1\u0010 \u001a\u0008\u0012\u0004\u0012\u0002H\u00050!\"\u0004\u0008\u0001\u0010\u0005*\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\"\u001a\u0002H\u0005H\u0002\u00a2\u0006\u0002\u0010#J6\u0010$\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0012\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0001*\u0008\u0012\u0004\u0012\u0002H\u00050\u00122\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u00150\u0014H\u0007\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponse$Companion;",
        "",
        "()V",
        "fromHttpException",
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "T",
        "exception",
        "Lretrofit2/HttpException;",
        "retrofit",
        "Lretrofit2/Retrofit;",
        "type",
        "Ljava/lang/reflect/Type;",
        "annotations",
        "",
        "",
        "(Lretrofit2/HttpException;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lcom/squareup/receiving/ReceivedResponse$Error;",
        "receiveFromRetrofit",
        "Lio/reactivex/SingleTransformer;",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "isSuccessful",
        "Lkotlin/Function1;",
        "",
        "(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;",
        "receiveFromRetrofitBlocking",
        "call",
        "Lretrofit2/Call;",
        "receiveFromRetrofitBlocking$public_release",
        "(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;Lretrofit2/Call;)Lcom/squareup/receiving/ReceivedResponse;",
        "tryToReadErrorBody",
        "response",
        "Lretrofit2/Response;",
        "(Lretrofit2/Response;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Ljava/lang/Object;",
        "accept",
        "Lcom/squareup/receiving/ReceivedResponse$Okay;",
        "t",
        "(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;",
        "rejectIfNot",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/receiving/ReceivedResponse$Companion;-><init>()V

    return-void
.end method

.method private final accept(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;TT;)",
            "Lcom/squareup/receiving/ReceivedResponse$Okay<",
            "TT;>;"
        }
    .end annotation

    .line 236
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-direct {p1, p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-direct {p1, p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;-><init>(Ljava/lang/Object;)V

    :goto_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay;

    return-object p1
.end method

.method public static final synthetic access$accept(Lcom/squareup/receiving/ReceivedResponse$Companion;Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;
    .locals 0

    .line 113
    invoke-direct {p0, p1, p2}, Lcom/squareup/receiving/ReceivedResponse$Companion;->accept(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fromHttpException(Lcom/squareup/receiving/ReceivedResponse$Companion;Lretrofit2/HttpException;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lcom/squareup/receiving/ReceivedResponse$Error;
    .locals 0

    .line 113
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/receiving/ReceivedResponse$Companion;->fromHttpException(Lretrofit2/HttpException;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lcom/squareup/receiving/ReceivedResponse$Error;

    move-result-object p0

    return-object p0
.end method

.method private final fromHttpException(Lretrofit2/HttpException;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lcom/squareup/receiving/ReceivedResponse$Error;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/HttpException;",
            "Lretrofit2/Retrofit;",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Lcom/squareup/receiving/ReceivedResponse$Error<",
            "TT;>;"
        }
    .end annotation

    .line 181
    invoke-virtual {p1}, Lretrofit2/HttpException;->code()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 184
    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    .line 185
    move-object v1, p0

    check-cast v1, Lcom/squareup/receiving/ReceivedResponse$Companion;

    invoke-virtual {p1}, Lretrofit2/HttpException;->response()Lretrofit2/Response;

    move-result-object p1

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/receiving/ReceivedResponse$Companion;->tryToReadErrorBody(Lretrofit2/Response;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Ljava/lang/Object;

    move-result-object p1

    .line 184
    invoke-direct {v0, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_0
    const/16 v1, 0x198

    if-ne v0, v1, :cond_1

    .line 189
    sget-object p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_1
    const/16 v1, 0x1ad

    if-ne v0, v1, :cond_2

    .line 193
    sget-object p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_2
    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_3

    .line 196
    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-direct {p1, v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;-><init>(I)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    goto :goto_0

    :cond_3
    const/16 v1, 0x190

    if-lt v0, v1, :cond_4

    .line 200
    new-instance v1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    .line 201
    move-object v2, p0

    check-cast v2, Lcom/squareup/receiving/ReceivedResponse$Companion;

    invoke-virtual {p1}, Lretrofit2/HttpException;->response()Lretrofit2/Response;

    move-result-object p1

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/squareup/receiving/ReceivedResponse$Companion;->tryToReadErrorBody(Lretrofit2/Response;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Ljava/lang/Object;

    move-result-object p1

    .line 200
    invoke-direct {v1, p1, v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;-><init>(Ljava/lang/Object;I)V

    move-object v0, v1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    :goto_0
    return-object v0

    .line 205
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unexpected HTTP status: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final tryToReadErrorBody(Lretrofit2/Response;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/Response<",
            "*>;",
            "Lretrofit2/Retrofit;",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            ")TT;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 216
    :try_start_0
    invoke-virtual {p1}, Lretrofit2/Response;->errorBody()Lokhttp3/ResponseBody;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v1, "response?.errorBody() ?: return null"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-virtual {p2, p3, p4}, Lretrofit2/Retrofit;->responseBodyConverter(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lretrofit2/Converter;

    move-result-object p2

    .line 219
    invoke-interface {p2, p1}, Lretrofit2/Converter;->convert(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final receiveFromRetrofit(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/Retrofit;",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/SingleTransformer<",
            "TT;",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isSuccessful"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;

    invoke-direct {v0, p4, p1, p2, p3}, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;-><init>(Lkotlin/jvm/functions/Function1;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)V

    check-cast v0, Lio/reactivex/SingleTransformer;

    return-object v0
.end method

.method public final receiveFromRetrofitBlocking$public_release(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;Lretrofit2/Call;)Lcom/squareup/receiving/ReceivedResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/Retrofit;",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lretrofit2/Call<",
            "TT;>;)",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "retrofit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isSuccessful"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    :try_start_0
    invoke-interface {p5}, Lretrofit2/Call;->execute()Lretrofit2/Response;

    move-result-object p5

    const-string v0, "response"

    .line 160
    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Lretrofit2/Response;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    move-object p1, p0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Companion;

    .line 162
    invoke-virtual {p5}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 161
    invoke-direct {p1, p4, p2}, Lcom/squareup/receiving/ReceivedResponse$Companion;->accept(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Okay;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    goto :goto_0

    :cond_0
    const-string p1, "The body should not be null. If it\'s nullable, then consider using Optional for your response type."

    .line 162
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 168
    :cond_1
    move-object p4, p0

    check-cast p4, Lcom/squareup/receiving/ReceivedResponse$Companion;

    new-instance v0, Lretrofit2/HttpException;

    invoke-direct {v0, p5}, Lretrofit2/HttpException;-><init>(Lretrofit2/Response;)V

    invoke-direct {p4, v0, p1, p2, p3}, Lcom/squareup/receiving/ReceivedResponse$Companion;->fromHttpException(Lretrofit2/HttpException;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lcom/squareup/receiving/ReceivedResponse$Error;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 171
    :catch_0
    sget-object p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    :goto_0
    return-object p1
.end method

.method public final rejectIfNot(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$rejectIfNot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isSuccessful"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;->getResponse()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_0

    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;->getResponse()Ljava/lang/Object;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    :cond_0
    return-object p1
.end method
