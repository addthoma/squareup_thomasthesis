.class public interface abstract Lcom/squareup/receiving/retrofit/StandardResponseHelper;
.super Ljava/lang/Object;
.source "StandardResponseHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/receiving/retrofit/StandardResponseHelper$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008`\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0096\u0010J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0008\u001a\u00020\u0007H\u0016J\u000c\u0010\u000b\u001a\u00020\u0007*\u00020\u000cH&J\u0010\u0010\r\u001a\u0006\u0012\u0002\u0008\u00030\u000e*\u00020\u0007H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/receiving/retrofit/StandardResponseHelper;",
        "",
        "fail",
        "",
        "message",
        "",
        "findResponseType",
        "Ljava/lang/reflect/Type;",
        "type",
        "isExpectedType",
        "",
        "getParameterUpperBound",
        "Ljava/lang/reflect/ParameterizedType;",
        "getRawTypeHelper",
        "Ljava/lang/Class;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract fail(Ljava/lang/String;)Ljava/lang/Void;
.end method

.method public abstract findResponseType(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method

.method public abstract getParameterUpperBound(Ljava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;
.end method

.method public abstract getRawTypeHelper(Ljava/lang/reflect/Type;)Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end method

.method public abstract isExpectedType(Ljava/lang/reflect/Type;)Z
.end method
