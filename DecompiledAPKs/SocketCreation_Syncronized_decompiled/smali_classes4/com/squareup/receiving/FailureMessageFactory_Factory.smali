.class public final Lcom/squareup/receiving/FailureMessageFactory_Factory;
.super Ljava/lang/Object;
.source "FailureMessageFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/receiving/FailureMessageFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/receiving/FailureMessageFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/receiving/FailureMessageFactory_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/receiving/FailureMessageFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessageFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/receiving/FailureMessageFactory;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessageFactory;-><init>(Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/receiving/FailureMessageFactory;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/receiving/FailureMessageFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {v0}, Lcom/squareup/receiving/FailureMessageFactory_Factory;->newInstance(Lcom/squareup/util/Res;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/receiving/FailureMessageFactory_Factory;->get()Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v0

    return-object v0
.end method
