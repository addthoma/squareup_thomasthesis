.class abstract Lcom/squareup/phrase/Phrase$Token;
.super Ljava/lang/Object;
.source "Phrase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/phrase/Phrase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Token"
.end annotation


# instance fields
.field private next:Lcom/squareup/phrase/Phrase$Token;

.field private final prev:Lcom/squareup/phrase/Phrase$Token;


# direct methods
.method protected constructor <init>(Lcom/squareup/phrase/Phrase$Token;)V
    .locals 0

    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366
    iput-object p1, p0, Lcom/squareup/phrase/Phrase$Token;->prev:Lcom/squareup/phrase/Phrase$Token;

    if-eqz p1, :cond_0

    .line 367
    iput-object p0, p1, Lcom/squareup/phrase/Phrase$Token;->next:Lcom/squareup/phrase/Phrase$Token;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$Token;
    .locals 0

    .line 361
    iget-object p0, p0, Lcom/squareup/phrase/Phrase$Token;->next:Lcom/squareup/phrase/Phrase$Token;

    return-object p0
.end method


# virtual methods
.method abstract expand(Landroid/text/Editable;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/Editable;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation
.end method

.method abstract getFormattedLength()I
.end method

.method final getFormattedStart()I
    .locals 2

    .line 378
    iget-object v0, p0, Lcom/squareup/phrase/Phrase$Token;->prev:Lcom/squareup/phrase/Phrase$Token;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 383
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase$Token;->getFormattedStart()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/phrase/Phrase$Token;->prev:Lcom/squareup/phrase/Phrase$Token;

    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase$Token;->getFormattedLength()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
