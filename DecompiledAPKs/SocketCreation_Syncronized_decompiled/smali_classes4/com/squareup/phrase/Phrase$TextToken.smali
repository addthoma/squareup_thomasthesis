.class Lcom/squareup/phrase/Phrase$TextToken;
.super Lcom/squareup/phrase/Phrase$Token;
.source "Phrase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/phrase/Phrase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TextToken"
.end annotation


# instance fields
.field private final textLength:I


# direct methods
.method constructor <init>(Lcom/squareup/phrase/Phrase$Token;I)V
    .locals 0

    .line 393
    invoke-direct {p0, p1}, Lcom/squareup/phrase/Phrase$Token;-><init>(Lcom/squareup/phrase/Phrase$Token;)V

    .line 394
    iput p2, p0, Lcom/squareup/phrase/Phrase$TextToken;->textLength:I

    return-void
.end method


# virtual methods
.method expand(Landroid/text/Editable;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/Editable;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method getFormattedLength()I
    .locals 1

    .line 402
    iget v0, p0, Lcom/squareup/phrase/Phrase$TextToken;->textLength:I

    return v0
.end method
