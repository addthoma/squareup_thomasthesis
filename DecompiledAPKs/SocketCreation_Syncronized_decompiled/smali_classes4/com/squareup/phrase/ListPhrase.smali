.class public final Lcom/squareup/phrase/ListPhrase;
.super Ljava/lang/Object;
.source "ListPhrase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/phrase/ListPhrase$Formatter;
    }
.end annotation


# instance fields
.field private final finalElementSeparator:Ljava/lang/CharSequence;

.field private final nonFinalElementSeparator:Ljava/lang/CharSequence;

.field private final twoElementSeparator:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "two-element separator"

    .line 96
    invoke-static {v0, p1}, Lcom/squareup/phrase/ListPhrase;->checkNotNull(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/squareup/phrase/ListPhrase;->twoElementSeparator:Ljava/lang/CharSequence;

    const-string p1, "non-final separator"

    .line 97
    invoke-static {p1, p2}, Lcom/squareup/phrase/ListPhrase;->checkNotNull(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/squareup/phrase/ListPhrase;->nonFinalElementSeparator:Ljava/lang/CharSequence;

    const-string p1, "final separator"

    .line 98
    invoke-static {p1, p3}, Lcom/squareup/phrase/ListPhrase;->checkNotNull(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/squareup/phrase/ListPhrase;->finalElementSeparator:Ljava/lang/CharSequence;

    return-void
.end method

.method private static asList(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;[TT;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 193
    new-instance v0, Lcom/squareup/phrase/ListPhrase$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/phrase/ListPhrase$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private static checkNotNull(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    return-object p1

    .line 237
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " cannot be null"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static checkNotNullOrEmpty(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;)V"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 246
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    return-void

    .line 247
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "list cannot be empty"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 244
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "list cannot be null"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static formatOrThrow(Ljava/lang/Object;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;I",
            "Lcom/squareup/phrase/ListPhrase$Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    if-eqz p0, :cond_3

    if-nez p2, :cond_0

    .line 222
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-interface {p2, p0}, Lcom/squareup/phrase/ListPhrase$Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    :goto_0
    if-eqz p0, :cond_2

    .line 227
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-eqz p2, :cond_1

    return-object p0

    .line 228
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "formatted list element cannot be empty at index "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 225
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "formatted list element cannot be null at index "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 219
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "list element cannot be null at index "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/ListPhrase;
    .locals 1

    const-string v0, "separator"

    .line 67
    invoke-static {v0, p0}, Lcom/squareup/phrase/ListPhrase;->checkNotNull(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-static {p0, p0, p0}, Lcom/squareup/phrase/ListPhrase;->from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/phrase/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/phrase/ListPhrase;
    .locals 1

    .line 81
    new-instance v0, Lcom/squareup/phrase/ListPhrase;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/phrase/ListPhrase;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private static getSize(Ljava/lang/Iterable;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "*>;)I"
        }
    .end annotation

    .line 179
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    .line 180
    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result p0

    return p0

    :cond_0
    const/4 v0, 0x0

    .line 184
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 185
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    .line 187
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return v0
.end method

.method private joinIterableWithSize(Ljava/lang/Iterable;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;I",
            "Lcom/squareup/phrase/ListPhrase$Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 143
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/phrase/ListPhrase;->joinMoreThanTwoElements(Ljava/lang/Iterable;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 141
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/squareup/phrase/ListPhrase;->joinTwoElements(Ljava/lang/Iterable;Lcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 139
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p2, p3}, Lcom/squareup/phrase/ListPhrase;->formatOrThrow(Ljava/lang/Object;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 137
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "list cannot be empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private joinMoreThanTwoElements(Ljava/lang/Iterable;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;I",
            "Lcom/squareup/phrase/ListPhrase$Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v1, p2, -0x2

    .line 162
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p2, :cond_2

    .line 166
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, v2, p3}, Lcom/squareup/phrase/ListPhrase;->formatOrThrow(Ljava/lang/Object;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    if-ge v2, v1, :cond_0

    .line 169
    iget-object v3, p0, Lcom/squareup/phrase/ListPhrase;->nonFinalElementSeparator:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_0
    if-ne v2, v1, :cond_1

    .line 171
    iget-object v3, p0, Lcom/squareup/phrase/ListPhrase;->finalElementSeparator:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private joinTwoElements(Ljava/lang/Iterable;Lcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;",
            "Lcom/squareup/phrase/ListPhrase$Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 152
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, p2}, Lcom/squareup/phrase/ListPhrase;->formatOrThrow(Ljava/lang/Object;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 153
    iget-object v1, p0, Lcom/squareup/phrase/ListPhrase;->twoElementSeparator:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 154
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    const/4 v1, 0x1

    invoke-static {p1, v1, p2}, Lcom/squareup/phrase/ListPhrase;->formatOrThrow(Ljava/lang/Object;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public join(Ljava/lang/Iterable;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 116
    invoke-static {p1}, Lcom/squareup/phrase/ListPhrase;->checkNotNullOrEmpty(Ljava/lang/Iterable;)V

    const/4 v0, 0x0

    .line 117
    invoke-virtual {p0, p1, v0}, Lcom/squareup/phrase/ListPhrase;->join(Ljava/lang/Iterable;Lcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public join(Ljava/lang/Iterable;Lcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;",
            "Lcom/squareup/phrase/ListPhrase$Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 128
    invoke-static {p1}, Lcom/squareup/phrase/ListPhrase;->checkNotNullOrEmpty(Ljava/lang/Iterable;)V

    .line 129
    invoke-static {p1}, Lcom/squareup/phrase/ListPhrase;->getSize(Ljava/lang/Iterable;)I

    move-result v0

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/phrase/ListPhrase;->joinIterableWithSize(Ljava/lang/Iterable;ILcom/squareup/phrase/ListPhrase$Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public varargs join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;[TT;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 107
    invoke-static {p1, p2, p3}, Lcom/squareup/phrase/ListPhrase;->asList(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/phrase/ListPhrase;->join(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
