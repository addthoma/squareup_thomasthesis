.class public Lcom/squareup/queue/QueueDumper$TaskEnvelope;
.super Ljava/lang/Object;
.source "QueueDumper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueDumper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TaskEnvelope"
.end annotation


# instance fields
.field public final className:Ljava/lang/String;

.field public final task:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/QueueDumper$TaskEnvelope;->className:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/squareup/queue/QueueDumper$TaskEnvelope;->task:Ljava/lang/Object;

    return-void
.end method
