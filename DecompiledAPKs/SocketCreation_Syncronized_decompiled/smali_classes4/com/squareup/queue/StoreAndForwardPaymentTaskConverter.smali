.class public Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;
.super Ljava/lang/Object;
.source "StoreAndForwardPaymentTaskConverter.java"

# interfaces
.implements Lcom/squareup/tape/FileObjectQueue$Converter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/FileObjectQueue$Converter<",
        "Lcom/squareup/payment/offline/StoredPayment;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_INTERVAL_HOURS:J = 0xcL


# instance fields
.field private final corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

.field private final corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

.field private final lastEmptyStoredPaymentLoggedAt:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final retrofitTaskConverter:Lcom/squareup/tape/FileObjectQueue$Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field

.field private final serializedConverter:Lcom/squareup/tape/SerializedConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/tape/SerializedConverter;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/queue/CorruptQueueHelper;Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->retrofitTaskConverter:Lcom/squareup/tape/FileObjectQueue$Converter;

    .line 32
    iput-object p2, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->serializedConverter:Lcom/squareup/tape/SerializedConverter;

    .line 33
    iput-object p4, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    .line 34
    iput-object p3, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->lastEmptyStoredPaymentLoggedAt:Lcom/f2prateek/rx/preferences2/Preference;

    .line 35
    iput-object p5, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    return-void
.end method


# virtual methods
.method public from([B)Lcom/squareup/payment/offline/StoredPayment;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 39
    array-length v0, p1

    if-nez v0, :cond_1

    .line 41
    new-instance p1, Ljava/util/Date;

    iget-object v0, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->lastEmptyStoredPaymentLoggedAt:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 42
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 43
    invoke-static {p1, v0}, Lcom/squareup/util/Times;->countHoursBetween(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v1

    const-wide/16 v3, 0xc

    cmp-long p1, v1, v3

    if-ltz p1, :cond_0

    .line 44
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Got zero-length byte array, returning null StoredPayment"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/queue/CorruptQueueRecorder;->recordCorruptQueue(Ljava/lang/Throwable;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->lastEmptyStoredPaymentLoggedAt:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 48
    :cond_0
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-virtual {p1}, Lcom/squareup/queue/CorruptQueueRecorder;->recordCorruptQueue()V

    .line 50
    :goto_0
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    invoke-virtual {p1}, Lcom/squareup/queue/CorruptQueueHelper;->corruptStoredPaymentPlaceholder()Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p1

    return-object p1

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->serializedConverter:Lcom/squareup/tape/SerializedConverter;

    invoke-virtual {v0, p1}, Lcom/squareup/tape/SerializedConverter;->from([B)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;

    .line 53
    iget-object v0, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->retrofitTaskConverter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoredPayment;->fromEnqueuedBytes(Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic from([B)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->from([B)Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p1

    return-object p1
.end method

.method public toStream(Lcom/squareup/payment/offline/StoredPayment;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->retrofitTaskConverter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/offline/StoredPayment;->toEnqueuedBytes(Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;

    move-result-object p1

    .line 58
    iget-object v0, p0, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->serializedConverter:Lcom/squareup/tape/SerializedConverter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/tape/SerializedConverter;->toStream(Ljava/io/Serializable;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic toStream(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15
    check-cast p1, Lcom/squareup/payment/offline/StoredPayment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->toStream(Lcom/squareup/payment/offline/StoredPayment;Ljava/io/OutputStream;)V

    return-void
.end method
