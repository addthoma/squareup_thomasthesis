.class public final Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;
.super Ljava/lang/Object;
.source "UploadFailure_ForCapture_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/UploadFailure$ForCapture;",
        ">;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;->gsonProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/UploadFailure$ForCapture;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/UploadFailure$ForCapture;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    invoke-static {p1, v0}, Lcom/squareup/queue/UploadFailure_MembersInjector;->injectGson(Lcom/squareup/queue/UploadFailure;Lcom/google/gson/Gson;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {p1, v0}, Lcom/squareup/queue/UploadFailure_MembersInjector;->injectOhSnapLogger(Lcom/squareup/queue/UploadFailure;Lcom/squareup/log/OhSnapLogger;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/queue/UploadFailure$ForCapture;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/UploadFailure_ForCapture_MembersInjector;->injectMembers(Lcom/squareup/queue/UploadFailure$ForCapture;)V

    return-void
.end method
