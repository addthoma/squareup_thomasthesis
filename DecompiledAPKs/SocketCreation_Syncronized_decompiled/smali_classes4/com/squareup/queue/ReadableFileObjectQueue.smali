.class public Lcom/squareup/queue/ReadableFileObjectQueue;
.super Lcom/squareup/tape/FileObjectQueue;
.source "ReadableFileObjectQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/tape/FileObjectQueue<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private listenerDelegate:Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/tape/FileObjectQueue;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    return-void
.end method


# virtual methods
.method readAll(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "TT;>;)V"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/queue/ReadableFileObjectQueue;->listenerDelegate:Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->setIgnoreEvents(Z)V

    .line 31
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/tape/FileObjectQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    .line 32
    iget-object p1, p0, Lcom/squareup/queue/ReadableFileObjectQueue;->listenerDelegate:Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;

    invoke-super {p0, p1}, Lcom/squareup/tape/FileObjectQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/queue/ReadableFileObjectQueue;->listenerDelegate:Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->setIgnoreEvents(Z)V

    :cond_1
    return-void
.end method

.method public setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "TT;>;)V"
        }
    .end annotation

    .line 18
    new-instance v0, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;

    invoke-direct {v0, p1}, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;-><init>(Lcom/squareup/tape/ObjectQueue$Listener;)V

    iput-object v0, p0, Lcom/squareup/queue/ReadableFileObjectQueue;->listenerDelegate:Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;

    .line 19
    iget-object p1, p0, Lcom/squareup/queue/ReadableFileObjectQueue;->listenerDelegate:Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;

    invoke-super {p0, p1}, Lcom/squareup/tape/FileObjectQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method
