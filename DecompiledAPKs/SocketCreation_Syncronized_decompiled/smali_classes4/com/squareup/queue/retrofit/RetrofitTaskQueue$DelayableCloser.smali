.class public Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;
.super Ljava/lang/Object;
.source "RetrofitTaskQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/retrofit/RetrofitTaskQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DelayableCloser"
.end annotation


# instance fields
.field private final close:Lrx/functions/Action0;

.field private closeDelayed:Z

.field private delayClose:Z


# direct methods
.method public constructor <init>(Lrx/functions/Action0;)V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->close:Lrx/functions/Action0;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->delayClose:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 86
    iput-boolean v0, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->closeDelayed:Z

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->close:Lrx/functions/Action0;

    invoke-interface {v0}, Lrx/functions/Action0;->call()V

    :goto_0
    return-void
.end method

.method public delayClose(Z)V
    .locals 0

    .line 93
    iput-boolean p1, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->delayClose:Z

    if-nez p1, :cond_0

    .line 94
    iget-boolean p1, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->closeDelayed:Z

    if-eqz p1, :cond_0

    .line 95
    iget-object p1, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->close:Lrx/functions/Action0;

    invoke-interface {p1}, Lrx/functions/Action0;->call()V

    const/4 p1, 0x0

    .line 96
    iput-boolean p1, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->closeDelayed:Z

    :cond_0
    return-void
.end method
