.class public interface abstract Lcom/squareup/queue/retrofit/RetrofitTask;
.super Ljava/lang/Object;
.source "RetrofitTask.java"

# interfaces
.implements Lcom/squareup/tape/Task;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/Task<",
        "Lcom/squareup/server/SquareCallback<",
        "Lcom/squareup/server/SimpleResponse;",
        ">;>;"
    }
.end annotation


# virtual methods
.method public abstract inject(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract secureCopyWithoutPIIForLogs()Ljava/lang/Object;
.end method
