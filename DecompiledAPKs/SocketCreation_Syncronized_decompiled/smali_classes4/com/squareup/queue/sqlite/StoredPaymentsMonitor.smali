.class public interface abstract Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;
.super Ljava/lang/Object;
.source "StoredPaymentsMonitor.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;


# virtual methods
.method public abstract allDistinctStoredPaymentIds()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract allDistinctStoredPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract distinctStoredPaymentsCount()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract oldestStoredPayment()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;>;"
        }
    .end annotation
.end method
