.class final Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;
.super Ljava/lang/Object;
.source "QueueStores.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/QueueStoresKt;->delete(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $deleteFirst:Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

.field final synthetic $this_delete:Lcom/squareup/sqlbrite3/BriteDatabase;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;->$this_delete:Lcom/squareup/sqlbrite3/BriteDatabase;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;->$deleteFirst:Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()I
    .locals 3

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;->$this_delete:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;->$deleteFirst:Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

    invoke-virtual {v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->getTable()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;->$deleteFirst:Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

    check-cast v2, Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeUpdateDelete(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    .line 212
    new-instance v1, Ljava/lang/RuntimeException;

    check-cast v0, Ljava/lang/Throwable;

    const-string v2, "Unable to delete first entry"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;->call()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
