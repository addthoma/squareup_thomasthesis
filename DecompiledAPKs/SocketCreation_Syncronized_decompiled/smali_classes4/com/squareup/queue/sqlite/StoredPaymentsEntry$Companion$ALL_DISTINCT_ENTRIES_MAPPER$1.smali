.class final synthetic Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$ALL_DISTINCT_ENTRIES_MAPPER$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "StoredPaymentsEntry.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function4<",
        "Ljava/lang/Long;",
        "Ljava/lang/String;",
        "Ljava/lang/Long;",
        "[B",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0012\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0017\u0010\u0002\u001a\u0013\u0018\u00010\u0003\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00062\u0015\u0010\u0007\u001a\u00110\u0008\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\t2\u0015\u0010\n\u001a\u00110\u0003\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u000b2\u0015\u0010\u000c\u001a\u00110\r\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u000e\u00a2\u0006\u0004\u0008\u000f\u0010\u0010"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        "p1",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "_id",
        "p2",
        "",
        "entryId",
        "p3",
        "timestampMs",
        "p4",
        "",
        "data",
        "invoke",
        "(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$ALL_DISTINCT_ENTRIES_MAPPER$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$ALL_DISTINCT_ENTRIES_MAPPER$1;

    invoke-direct {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$ALL_DISTINCT_ENTRIES_MAPPER$1;-><init>()V

    sput-object v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$ALL_DISTINCT_ENTRIES_MAPPER$1;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$ALL_DISTINCT_ENTRIES_MAPPER$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>(Ljava/lang/Long;Ljava/lang/String;J[B)V"

    return-object v0
.end method

.method public final invoke(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;
    .locals 8

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p4"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    .line 75
    invoke-direct/range {v1 .. v7}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;J[BLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .line 57
    move-object v1, p1

    check-cast v1, Ljava/lang/Long;

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    move-object v5, p4

    check-cast v5, [B

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$ALL_DISTINCT_ENTRIES_MAPPER$1;->invoke(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    move-result-object p1

    return-object p1
.end method
