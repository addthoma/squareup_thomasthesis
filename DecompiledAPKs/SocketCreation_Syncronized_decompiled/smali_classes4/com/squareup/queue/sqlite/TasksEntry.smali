.class public final Lcom/squareup/queue/sqlite/TasksEntry;
.super Ljava/lang/Object;
.source "TasksEntry.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/TasksModel;
.implements Lcom/squareup/queue/sqlite/QueueStoreEntry;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/TasksEntry$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTasksEntry.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TasksEntry.kt\ncom/squareup/queue/sqlite/TasksEntry\n*L\n1#1,109:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0012\n\u0002\u0008\u0013\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\u0008\u0086\u0008\u0018\u0000 (2\u00020\u00012\u00020\u0002:\u0001(B1\u0008\u0002\u0012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\u0008\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u000f\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0016\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0016\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003JB\u0010\u001a\u001a\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00042\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001\u00a2\u0006\u0002\u0010\u001bJ\u0008\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u001c\u001a\u00020\u0006H\u0016J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0096\u0002J\u0008\u0010!\u001a\u00020\"H\u0016J\u0008\u0010#\u001a\u00020\u0004H\u0016J\u000e\u0010$\u001a\u00020\u00062\u0006\u0010%\u001a\u00020\u0006J\u0008\u0010&\u001a\u00020\u0004H\u0016J\t\u0010\'\u001a\u00020\u0006H\u00d6\u0001R\u0015\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\n\n\u0002\u0010\u000e\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0013R\u0011\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/TasksEntry;",
        "Lcom/squareup/queue/sqlite/TasksModel;",
        "Lcom/squareup/queue/sqlite/QueueStoreEntry;",
        "_id",
        "",
        "entryId",
        "",
        "timestampMs",
        "isLocalPayment",
        "data",
        "",
        "(Ljava/lang/Long;Ljava/lang/String;JJ[B)V",
        "get_id",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "getData",
        "()[B",
        "getEntryId",
        "()Ljava/lang/String;",
        "()J",
        "getTimestampMs",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksEntry;",
        "entry_id",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "is_local_payment",
        "logAs",
        "prefix",
        "timestamp_ms",
        "toString",
        "Companion",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ALL_ENTRIES_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final ALL_LOCAL_PAYMENT_ENTRIES_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/queue/sqlite/TasksEntry$Companion;

.field private static final FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/TasksModel$Factory<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIRST_ENTRY_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final GET_ENTRY_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/TasksEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOCAL_PAYMENTS_COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final RIPENED_LOCAL_PAYMENTS_COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _id:Ljava/lang/Long;

.field private final data:[B

.field private final entryId:Ljava/lang/String;

.field private final isLocalPayment:J

.field private final timestampMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/queue/sqlite/TasksEntry$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/queue/sqlite/TasksEntry$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->Companion:Lcom/squareup/queue/sqlite/TasksEntry$Companion;

    .line 66
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Factory;

    .line 67
    sget-object v1, Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;->INSTANCE:Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;

    check-cast v1, Lcom/squareup/queue/sqlite/TasksModel$Creator;

    .line 66
    invoke-direct {v0, v1}, Lcom/squareup/queue/sqlite/TasksModel$Factory;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Creator;)V

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    .line 77
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksModel$Factory;->countMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    const-string v1, "FACTORY.countMapper()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    .line 78
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksModel$Factory;->localPaymentsCountMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    const-string v1, "FACTORY.localPaymentsCountMapper()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->LOCAL_PAYMENTS_COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    .line 80
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksModel$Factory;->ripenedLocalPaymentsCountMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    const-string v1, "FACTORY.ripenedLocalPaymentsCountMapper()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->RIPENED_LOCAL_PAYMENTS_COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    .line 81
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksModel$Factory;->firstEntryMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    move-result-object v0

    const-string v1, "FACTORY.firstEntryMapper()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/sqldelight/prerelease/RowMapper;

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FIRST_ENTRY_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    .line 82
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksModel$Factory;->getEntryMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    move-result-object v0

    const-string v1, "FACTORY.entryMapper"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/sqldelight/prerelease/RowMapper;

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->GET_ENTRY_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    .line 84
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksModel$Factory;->allLocalPaymentEntriesMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    move-result-object v0

    const-string v1, "FACTORY.allLocalPaymentEntriesMapper()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/sqldelight/prerelease/RowMapper;

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->ALL_LOCAL_PAYMENT_ENTRIES_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    .line 85
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksModel$Factory;->allEntriesMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    move-result-object v0

    const-string v1, "FACTORY.allEntriesMapper()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/sqldelight/prerelease/RowMapper;

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->ALL_ENTRIES_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/String;JJ[B)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    iput-wide p5, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    iput-object p7, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Long;Ljava/lang/String;JJ[BLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct/range {p0 .. p7}, Lcom/squareup/queue/sqlite/TasksEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;JJ[B)V

    return-void
.end method

.method public static final synthetic access$getALL_ENTRIES_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->ALL_ENTRIES_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-object v0
.end method

.method public static final synthetic access$getALL_LOCAL_PAYMENT_ENTRIES_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->ALL_LOCAL_PAYMENT_ENTRIES_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-object v0
.end method

.method public static final synthetic access$getCOUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-object v0
.end method

.method public static final synthetic access$getFACTORY$cp()Lcom/squareup/queue/sqlite/TasksModel$Factory;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FACTORY:Lcom/squareup/queue/sqlite/TasksModel$Factory;

    return-object v0
.end method

.method public static final synthetic access$getFIRST_ENTRY_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->FIRST_ENTRY_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-object v0
.end method

.method public static final synthetic access$getGET_ENTRY_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->GET_ENTRY_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-object v0
.end method

.method public static final synthetic access$getLOCAL_PAYMENTS_COUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->LOCAL_PAYMENTS_COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-object v0
.end method

.method public static final synthetic access$getRIPENED_LOCAL_PAYMENTS_COUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->RIPENED_LOCAL_PAYMENTS_COUNT_MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/queue/sqlite/TasksEntry;Ljava/lang/Long;Ljava/lang/String;JJ[BILjava/lang/Object;)Lcom/squareup/queue/sqlite/TasksEntry;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-wide p3, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    :cond_2
    move-wide v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-wide p5, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    :cond_3
    move-wide v2, p5

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p7, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    :cond_4
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-wide p5, v0

    move-wide p7, v2

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/queue/sqlite/TasksEntry;->copy(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksEntry;

    move-result-object p0

    return-object p0
.end method

.method public static final newTasksEntry(Ljava/lang/String;JZ[B)Lcom/squareup/queue/sqlite/TasksEntry;
    .locals 6
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->Companion:Lcom/squareup/queue/sqlite/TasksEntry$Companion;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/queue/sqlite/TasksEntry$Companion;->newTasksEntry(Ljava/lang/String;JZ[B)Lcom/squareup/queue/sqlite/TasksEntry;

    move-result-object p0

    return-object p0
.end method

.method public static final newTasksEntryForTest(Ljava/lang/Long;Ljava/lang/String;JZ[B)Lcom/squareup/queue/sqlite/TasksEntry;
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/queue/sqlite/TasksEntry;->Companion:Lcom/squareup/queue/sqlite/TasksEntry$Companion;

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/queue/sqlite/TasksEntry$Companion;->newTasksEntryForTest(Ljava/lang/Long;Ljava/lang/String;JZ[B)Lcom/squareup/queue/sqlite/TasksEntry;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public _id()Ljava/lang/Long;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    return-object v0
.end method

.method public final component1()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    return-wide v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    return-wide v0
.end method

.method public final component5()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    return-object v0
.end method

.method public final copy(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksEntry;
    .locals 9

    const-string v0, "entryId"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/queue/sqlite/TasksEntry;

    move-object v1, v0

    move-object v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/queue/sqlite/TasksEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;JJ[B)V

    return-object v0
.end method

.method public data()[B
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    return-object v0
.end method

.method public entry_id()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 32
    move-object v0, p0

    check-cast v0, Lcom/squareup/queue/sqlite/TasksEntry;

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    return v1

    .line 33
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    if-eqz p1, :cond_8

    .line 35
    check-cast p1, Lcom/squareup/queue/sqlite/TasksEntry;

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_3

    return v2

    .line 38
    :cond_3
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_4

    return v2

    .line 39
    :cond_4
    iget-wide v3, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    iget-wide v5, p1, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    cmp-long v0, v3, v5

    if-eqz v0, :cond_5

    return v2

    .line 40
    :cond_5
    iget-wide v3, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    iget-wide v5, p1, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    cmp-long v0, v3, v5

    if-eqz v0, :cond_6

    return v2

    .line 41
    :cond_6
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    iget-object p1, p1, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    if-nez p1, :cond_7

    return v2

    :cond_7
    return v1

    .line 35
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.queue.sqlite.TasksEntry"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getData()[B
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    return-object v0
.end method

.method public final getEntryId()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTimestampMs()J
    .locals 2

    .line 12
    iget-wide v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    return-wide v0
.end method

.method public final get_id()Ljava/lang/Long;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 47
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 48
    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 49
    iget-wide v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 50
    iget-wide v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 51
    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final isLocalPayment()J
    .locals 2

    .line 13
    iget-wide v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    return-wide v0
.end method

.method public is_local_payment()J
    .locals 2

    .line 20
    iget-wide v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    return-wide v0
.end method

.method public final logAs(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "prefix"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/TasksEntry;->entry_id()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "none"

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/TasksEntry;->entry_id()Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": entry id %"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", timestamp %"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, ", isLocalPayment "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    iget-wide v2, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    invoke-static {v2, v3}, Lcom/squareup/queue/sqlite/QueueStoresKt;->toBoolean(J)Z

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public timestamp_ms()J
    .locals 2

    .line 22
    iget-wide v0, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TasksEntry(_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->_id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", entryId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->entryId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", timestampMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->timestampMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", isLocalPayment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->isLocalPayment:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksEntry;->data:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
