.class final Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;
.super Ljava/lang/Object;
.source "TasksSqliteStore.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/TasksSqliteStore;->insert(Lcom/squareup/queue/sqlite/TasksEntry;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $entry:Lcom/squareup/queue/sqlite/TasksEntry;

.field final synthetic this$0:Lcom/squareup/queue/sqlite/TasksSqliteStore;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/TasksSqliteStore;Lcom/squareup/queue/sqlite/TasksEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->call()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final call()Z
    .locals 8

    .line 190
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->access$getInsert$p(Lcom/squareup/queue/sqlite/TasksSqliteStore;)Lcom/squareup/queue/sqlite/TasksModel$InsertEntry;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksEntry;->entry_id()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksEntry;->timestamp_ms()J

    move-result-wide v3

    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksEntry;->is_local_payment()J

    move-result-wide v5

    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksEntry;->data()[B

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/queue/sqlite/TasksModel$InsertEntry;->bind(Ljava/lang/String;JJ[B)V

    .line 192
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->access$getDb$p(Lcom/squareup/queue/sqlite/TasksSqliteStore;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    invoke-static {v1}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->access$getInsert$p(Lcom/squareup/queue/sqlite/TasksSqliteStore;)Lcom/squareup/queue/sqlite/TasksModel$InsertEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/TasksModel$InsertEntry;->getTable()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    invoke-static {v2}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->access$getInsert$p(Lcom/squareup/queue/sqlite/TasksSqliteStore;)Lcom/squareup/queue/sqlite/TasksModel$InsertEntry;

    move-result-object v2

    check-cast v2, Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeInsert(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)J

    move-result-wide v0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    .line 194
    new-instance v1, Ljava/lang/RuntimeException;

    iget-object v2, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    iget-object v3, p0, Lcom/squareup/queue/sqlite/TasksSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/TasksEntry;

    invoke-static {v2, v3}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->access$failedInsertMessage(Lcom/squareup/queue/sqlite/TasksSqliteStore;Lcom/squareup/queue/sqlite/TasksEntry;)Ljava/lang/String;

    move-result-object v2

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
