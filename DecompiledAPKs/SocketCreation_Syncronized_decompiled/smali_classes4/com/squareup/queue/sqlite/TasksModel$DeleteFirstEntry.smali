.class public final Lcom/squareup/queue/sqlite/TasksModel$DeleteFirstEntry;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;
.source "TasksModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/TasksModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteFirstEntry"
.end annotation


# direct methods
.method public constructor <init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE FROM tasks\nWHERE _id = ( SELECT MIN(_id) FROM tasks )"

    .line 262
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteStatement;

    move-result-object p1

    const-string v0, "tasks"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;-><init>(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)V

    return-void
.end method
