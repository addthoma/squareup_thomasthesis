.class final Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$RipenedCountQuery;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
.source "PendingCapturesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RipenedCountQuery"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

.field private final timestamp_ms:J


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;J)V
    .locals 1

    .line 177
    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$RipenedCountQuery;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    .line 178
    new-instance p1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v0, "pending_captures"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v0, "SELECT COUNT(*)\nFROM pending_captures\nWHERE timestamp_ms <= ?1"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    .line 183
    iput-wide p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$RipenedCountQuery;->timestamp_ms:J

    return-void
.end method


# virtual methods
.method public bindTo(Landroidx/sqlite/db/SupportSQLiteProgram;)V
    .locals 3

    .line 188
    iget-wide v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$RipenedCountQuery;->timestamp_ms:J

    const/4 v2, 0x1

    invoke-interface {p1, v2, v0, v1}, Landroidx/sqlite/db/SupportSQLiteProgram;->bindLong(IJ)V

    return-void
.end method
