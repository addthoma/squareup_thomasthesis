.class public interface abstract Lcom/squareup/queue/sqlite/shared/SqliteQueue;
.super Ljava/lang/Object;
.source "SqliteQueue.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\t0\u0008H\'J\u0008\u0010\n\u001a\u00020\u0004H&J\u001c\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH&J\u0014\u0010\u0010\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\r0\u0008H&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000cH&J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000cH&J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0008H&\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/shared/SqliteQueue;",
        "T",
        "",
        "add",
        "Lio/reactivex/Completable;",
        "entry",
        "(Ljava/lang/Object;)Lio/reactivex/Completable;",
        "allEntries",
        "Lio/reactivex/Observable;",
        "",
        "close",
        "fetchEntry",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/Optional;",
        "entryId",
        "",
        "peekFirst",
        "removeAll",
        "",
        "removeFirst",
        "size",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract add(Ljava/lang/Object;)Lio/reactivex/Completable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation
.end method

.method public abstract allEntries()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Memory pressure, do not use"
    .end annotation
.end method

.method public abstract close()Lio/reactivex/Completable;
.end method

.method public abstract fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation
.end method

.method public abstract peekFirst()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation
.end method

.method public abstract removeAll()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeFirst()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract size()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method
