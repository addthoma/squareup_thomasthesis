.class final Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$allEntries$1;
.super Ljava/lang/Object;
.source "RealSqliteQueue.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->allEntries()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0002*\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Q",
        "S",
        "",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$allEntries$1;->this$0:Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$allEntries$1;->apply(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TS;>;)",
            "Ljava/util/List<",
            "TQ;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$allEntries$1;->this$0:Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->access$getConverter$p(Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;)Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;->toQueueEntries(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
