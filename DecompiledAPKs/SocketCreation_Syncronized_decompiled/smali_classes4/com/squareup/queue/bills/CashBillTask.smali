.class public Lcom/squareup/queue/bills/CashBillTask;
.super Lcom/squareup/queue/bills/LocalBillTask;
.source "CashBillTask.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final change:Lcom/squareup/protos/common/Money;

.field private final tendered:Lcom/squareup/protos/common/Money;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object/from16 v15, p0

    .line 38
    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move-object/from16 v8, p9

    move-object/from16 v9, p8

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v14}, Lcom/squareup/queue/bills/LocalBillTask;-><init>(Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    .line 40
    iput-object v0, v15, Lcom/squareup/queue/bills/CashBillTask;->tendered:Lcom/squareup/protos/common/Money;

    move-object/from16 v0, p7

    .line 41
    iput-object v0, v15, Lcom/squareup/queue/bills/CashBillTask;->change:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public static createForTesting(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/queue/bills/CashBillTask;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/queue/bills/CashBillTask;"
        }
    .end annotation

    .line 27
    new-instance v15, Lcom/squareup/queue/bills/CashBillTask;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/queue/bills/CashBillTask;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v15
.end method


# virtual methods
.method protected asTenderHistory(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;-><init>()V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/queue/bills/CashBillTask;->getBillUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/queue/bills/CashBillTask;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    .line 48
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    iget-object v0, p0, Lcom/squareup/queue/bills/CashBillTask;->tendered:Lcom/squareup/protos/common/Money;

    .line 49
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->tenderedAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/bills/CashBillTask;->change:Lcom/squareup/protos/common/Money;

    .line 50
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->changeAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/CashTenderHistory;

    move-result-object p1

    return-object p1
.end method

.method public getChange()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/queue/bills/CashBillTask;->change:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method protected getSingleTenderMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 4

    .line 55
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/CashTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CashTender$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/queue/bills/CashBillTask;->tendered:Lcom/squareup/protos/common/Money;

    .line 58
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;->buyer_tendered_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/queue/bills/CashBillTask;->change:Lcom/squareup/protos/common/Money;

    .line 59
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;->change_back_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;

    move-result-object v2

    .line 60
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CashTender$Amounts;

    move-result-object v2

    .line 57
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CashTender$Builder;->amounts(Lcom/squareup/protos/client/bills/CashTender$Amounts;)Lcom/squareup/protos/client/bills/CashTender$Builder;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CashTender$Builder;->build()Lcom/squareup/protos/client/bills/CashTender;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender(Lcom/squareup/protos/client/bills/CashTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getTendered()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/queue/bills/CashBillTask;->tendered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 74
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/CashBillTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/CashBillTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method
