.class public final Lcom/squareup/queue/bills/RemoveTendersTask;
.super Lcom/squareup/queue/TransactionRpcThreadTask;
.source "RemoveTendersTask.kt"

# interfaces
.implements Lcom/squareup/queue/CancelTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/TransactionRpcThreadTask<",
        "Lcom/squareup/protos/client/bills/RemoveTendersResponse;",
        ">;",
        "Lcom/squareup/queue/CancelTask;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003B#\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u001d\u001a\u00020\u0002H\u0016J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0002H\u0014J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0016J\u0010\u0010%\u001a\u00020\"2\u0006\u0010&\u001a\u00020\u0018H\u0016J\u0008\u0010\'\u001a\u00020(H\u0016R\u001e\u0010\u000c\u001a\u00020\r8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\n \u0014*\u0004\u0018\u00010\u00130\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u001e\u0010\u0017\u001a\u00020\u00188\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001c\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/queue/bills/RemoveTendersTask;",
        "Lcom/squareup/queue/TransactionRpcThreadTask;",
        "Lcom/squareup/protos/client/bills/RemoveTendersResponse;",
        "Lcom/squareup/queue/CancelTask;",
        "billId",
        "Lcom/squareup/protos/client/IdPair;",
        "merchant",
        "Lcom/squareup/protos/client/Merchant;",
        "tenders",
        "",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
        "(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;)V",
        "removeTenderService",
        "Lcom/squareup/tenders/RemoveTenderService;",
        "getRemoveTenderService",
        "()Lcom/squareup/tenders/RemoveTenderService;",
        "setRemoveTenderService",
        "(Lcom/squareup/tenders/RemoveTenderService;)V",
        "request",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest;",
        "kotlin.jvm.PlatformType",
        "getRequest",
        "()Lcom/squareup/protos/client/bills/RemoveTendersRequest;",
        "transactionLedgerManager",
        "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        "getTransactionLedgerManager",
        "()Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        "setTransactionLedgerManager",
        "(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V",
        "callOnRpcThread",
        "handleResponseOnMainThread",
        "Lcom/squareup/server/SimpleResponse;",
        "response",
        "inject",
        "",
        "component",
        "Lcom/squareup/queue/TransactionTasksComponent;",
        "logEnqueued",
        "ledger",
        "secureCopyWithoutPIIForLogs",
        "",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public transient removeTenderService:Lcom/squareup/tenders/RemoveTenderService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final request:Lcom/squareup/protos/client/bills/RemoveTendersRequest;

.field public transient transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
            ">;)V"
        }
    .end annotation

    const-string v0, "billId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchant"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenders"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/queue/TransactionRpcThreadTask;-><init>()V

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;

    move-result-object p1

    .line 24
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;

    move-result-object p1

    .line 25
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->tenders_to_remove(Ljava/util/List;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;

    move-result-object p1

    .line 26
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->request:Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    return-void
.end method


# virtual methods
.method public callOnRpcThread()Lcom/squareup/protos/client/bills/RemoveTendersResponse;
    .locals 4

    .line 44
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->removeTenderService:Lcom/squareup/tenders/RemoveTenderService;

    if-nez v0, :cond_0

    const-string v1, "removeTenderService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->request:Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    const-string v2, "request"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/tenders/RemoveTenderService;->removeTenders(Lcom/squareup/protos/client/bills/RemoveTendersRequest;)Lcom/squareup/protos/client/bills/RemoveTendersResponse;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v1, :cond_1

    const-string v2, "transactionLedgerManager"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logRemoveTendersResponse(Lcom/squareup/protos/client/bills/RemoveTendersResponse;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 46
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/RemoveTendersResponse;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "RemoveTendersResponse %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/queue/bills/RemoveTendersTask;->callOnRpcThread()Lcom/squareup/protos/client/bills/RemoveTendersResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getRemoveTenderService()Lcom/squareup/tenders/RemoveTenderService;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->removeTenderService:Lcom/squareup/tenders/RemoveTenderService;

    if-nez v0, :cond_0

    const-string v1, "removeTenderService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getRequest()Lcom/squareup/protos/client/bills/RemoveTendersRequest;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->request:Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    return-object v0
.end method

.method public final getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v0, :cond_0

    const-string v1, "transactionLedgerManager"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/bills/RemoveTendersResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/RemoveTendersResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method public bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/protos/client/bills/RemoveTendersResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/RemoveTendersTask;->handleResponseOnMainThread(Lcom/squareup/protos/client/bills/RemoveTendersResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/RemoveTendersTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/RemoveTendersTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    const-string v0, "ledger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->request:Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logRemoveTendersRequest(Lcom/squareup/protos/client/bills/RemoveTendersRequest;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->request:Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    const-string v1, "request"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setRemoveTenderService(Lcom/squareup/tenders/RemoveTenderService;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->removeTenderService:Lcom/squareup/tenders/RemoveTenderService;

    return-void
.end method

.method public final setTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method
