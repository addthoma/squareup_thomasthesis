.class public Lcom/squareup/queue/bills/OtherTenderBillTask;
.super Lcom/squareup/queue/bills/LocalBillTask;
.source "OtherTenderBillTask.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final otherTenderType:Lcom/squareup/server/account/protos/OtherTenderType;

.field private final tenderNote:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object/from16 v15, p0

    .line 28
    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    move-object/from16 v0, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-wide/from16 v4, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    move-object/from16 v9, p9

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    invoke-direct/range {v0 .. v14}, Lcom/squareup/queue/bills/LocalBillTask;-><init>(Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    .line 30
    iput-object v0, v15, Lcom/squareup/queue/bills/OtherTenderBillTask;->otherTenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    move-object/from16 v0, p2

    .line 31
    iput-object v0, v15, Lcom/squareup/queue/bills/OtherTenderBillTask;->tenderNote:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected asTenderHistory(Ljava/util/Date;)Lcom/squareup/billhistory/model/OtherTenderHistory;
    .locals 2

    .line 35
    new-instance v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/queue/bills/OtherTenderBillTask;->getBillUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    .line 37
    invoke-virtual {p0}, Lcom/squareup/queue/bills/OtherTenderBillTask;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    .line 38
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object v0, p0, Lcom/squareup/queue/bills/OtherTenderBillTask;->otherTenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v0, v0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    .line 39
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->name(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/bills/OtherTenderBillTask;->tenderNote:Ljava/lang/String;

    .line 40
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->note(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/queue/bills/OtherTenderBillTask;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/bills/OtherTenderBillTask;->otherTenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v0, v0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 42
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderType(I)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/OtherTenderHistory;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic asTenderHistory(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/OtherTenderBillTask;->asTenderHistory(Ljava/util/Date;)Lcom/squareup/billhistory/model/OtherTenderHistory;

    move-result-object p1

    return-object p1
.end method

.method public getOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/queue/bills/OtherTenderBillTask;->otherTenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    return-object v0
.end method

.method protected getSingleTenderMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 3

    .line 47
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/OtherTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/OtherTender$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/queue/bills/OtherTenderBillTask;->otherTenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v2, v2, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 50
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->fromValue(I)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object v2

    .line 49
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->other_tender_type(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/bills/OtherTenderBillTask;->tenderNote:Ljava/lang/String;

    .line 51
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->tender_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object v1

    .line 52
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->build()Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->other_tender(Lcom/squareup/protos/client/bills/OtherTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getTenderNote()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/queue/bills/OtherTenderBillTask;->tenderNote:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 64
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/OtherTenderBillTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/OtherTenderBillTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method
