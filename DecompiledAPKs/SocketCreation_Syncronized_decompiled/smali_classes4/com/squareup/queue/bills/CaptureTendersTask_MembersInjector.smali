.class public final Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;
.super Ljava/lang/Object;
.source "CaptureTendersTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/bills/CaptureTendersTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final danglingAuthProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tenders/CaptureTenderService;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tenders/CaptureTenderService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->serviceProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->danglingAuthProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tenders/CaptureTenderService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/bills/CaptureTendersTask;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectDanglingAuth(Lcom/squareup/queue/bills/CaptureTendersTask;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V
    .locals 0
    .annotation runtime Lcom/squareup/payment/DanglingPayment;
    .end annotation

    .line 74
    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    return-void
.end method

.method public static injectService(Lcom/squareup/queue/bills/CaptureTendersTask;Lcom/squareup/server/tenders/CaptureTenderService;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->service:Lcom/squareup/server/tenders/CaptureTenderService;

    return-void
.end method

.method public static injectTransactionLedgerManager(Lcom/squareup/queue/bills/CaptureTendersTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/queue/bills/CaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/bills/CaptureTendersTask;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/tenders/CaptureTenderService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->injectService(Lcom/squareup/queue/bills/CaptureTendersTask;Lcom/squareup/server/tenders/CaptureTenderService;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/bills/CaptureTendersTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->danglingAuthProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->injectDanglingAuth(Lcom/squareup/queue/bills/CaptureTendersTask;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/queue/bills/CaptureTendersTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/CaptureTendersTask_MembersInjector;->injectMembers(Lcom/squareup/queue/bills/CaptureTendersTask;)V

    return-void
.end method
