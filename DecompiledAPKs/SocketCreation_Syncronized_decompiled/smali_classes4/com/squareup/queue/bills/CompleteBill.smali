.class public Lcom/squareup/queue/bills/CompleteBill;
.super Lcom/squareup/queue/bills/AbstractCompleteBillTask;
.source "CompleteBill.java"

# interfaces
.implements Lcom/squareup/queue/CaptureTask;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 24
    invoke-direct/range {p0 .. p7}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;-><init>(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/protos/client/bills/CompleteBillResponse;
    .locals 4

    .line 29
    invoke-virtual {p0}, Lcom/squareup/queue/bills/CompleteBill;->clearDanglingAuthForThisBill()V

    .line 30
    iget-object v0, p0, Lcom/squareup/queue/bills/CompleteBill;->service:Lcom/squareup/server/bills/BillCreationService;

    iget-object v1, p0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v2, p0, Lcom/squareup/queue/bills/CompleteBill;->clientId:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/server/bills/ApiClientId;->clientIdOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/server/bills/BillCreationService;->completeBill(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteBillResponse;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/squareup/queue/bills/CompleteBill;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCompleteBillResponse(Lcom/squareup/protos/client/bills/CompleteBillResponse;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 32
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteBillResponse;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "CompleteBillResponse: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/queue/bills/CompleteBill;->callOnRpcThread()Lcom/squareup/protos/client/bills/CompleteBillResponse;

    move-result-object v0

    return-object v0
.end method

.method clearDanglingAuthForThisBill()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/queue/bills/CompleteBill;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->getDanglingAuthBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v1, p0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/bills/CompleteBill;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->clearLastAuth()V

    .line 50
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Found danglingAuth for a bill that is already enqueued"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Found danglingAuth for a bill that is already enqueued, but couldn\'t clear it"

    .line 54
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public getAuthorizationId()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 70
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/CompleteBill;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/CompleteBill;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
