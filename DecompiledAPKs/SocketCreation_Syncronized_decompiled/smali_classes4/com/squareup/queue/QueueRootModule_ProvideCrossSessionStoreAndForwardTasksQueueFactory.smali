.class public final Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;
.super Ljava/lang/Object;
.source "QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        ">;"
    }
.end annotation


# instance fields
.field private final directoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final queueFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->directoryProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->queueFactoryProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;)",
            "Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCrossSessionStoreAndForwardTasksQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/RetrofitQueueFactory;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/queue/QueueRootModule;->provideCrossSessionStoreAndForwardTasksQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/RetrofitQueueFactory;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->directoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->queueFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;

    iget-object v2, p0, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static {v0, v1, v2}, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->provideCrossSessionStoreAndForwardTasksQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/RetrofitQueueFactory;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/queue/QueueRootModule_ProvideCrossSessionStoreAndForwardTasksQueueFactory;->get()Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    return-object v0
.end method
