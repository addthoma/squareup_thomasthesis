.class public Lcom/squareup/queue/Itemize;
.super Ljava/lang/Object;
.source "Itemize.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/Itemize$OopsAdapter;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private adjustmentList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustments:Ljava/lang/String;

.field final billId:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private itemizationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final itemizations:Ljava/lang/String;

.field final paymentId:Ljava/lang/String;

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    .line 70
    invoke-direct/range {v0 .. v7}, Lcom/squareup/queue/Itemize;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/squareup/queue/Itemize;->billId:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/squareup/queue/Itemize;->adjustmentList:Ljava/util/List;

    .line 78
    iput-object p4, p0, Lcom/squareup/queue/Itemize;->itemizationList:Ljava/util/List;

    .line 80
    iput-object p2, p0, Lcom/squareup/queue/Itemize;->paymentId:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lcom/squareup/queue/Itemize;->adjustments:Ljava/lang/String;

    .line 82
    iput-object p6, p0, Lcom/squareup/queue/Itemize;->itemizations:Ljava/lang/String;

    const-string p1, ""

    .line 83
    invoke-static {p7, p1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/Itemize;->description:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "billId"

    .line 63
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    const-string p1, "adjustments"

    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Ljava/util/List;

    const-string p1, "itemizations"

    .line 64
    invoke-static {p3, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Ljava/util/List;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v7, p4

    .line 63
    invoke-direct/range {v0 .. v7}, Lcom/squareup/queue/Itemize;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static adjustmentListType()Ljava/lang/reflect/Type;
    .locals 1

    .line 137
    new-instance v0, Lcom/squareup/queue/Itemize$1;

    invoke-direct {v0}, Lcom/squareup/queue/Itemize$1;-><init>()V

    .line 138
    invoke-virtual {v0}, Lcom/squareup/queue/Itemize$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method static itemizationListType()Ljava/lang/reflect/Type;
    .locals 1

    .line 142
    new-instance v0, Lcom/squareup/queue/Itemize$2;

    invoke-direct {v0}, Lcom/squareup/queue/Itemize$2;-><init>()V

    .line 143
    invoke-virtual {v0}, Lcom/squareup/queue/Itemize$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/queue/Itemize;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Sending payments/itemizations request: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    iget-object v1, p0, Lcom/squareup/queue/Itemize;->billId:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/squareup/queue/Itemize;->billId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/queue/Itemize;->getAdjustments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/queue/Itemize;->getItemizations()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/queue/Itemize;->description:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/server/payment/ItemizationsMessage;->forBill(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/server/payment/ItemizationsMessage;

    move-result-object v0

    goto :goto_0

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/squareup/queue/Itemize;->paymentId:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 96
    iget-object v1, p0, Lcom/squareup/queue/Itemize;->paymentId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/queue/Itemize;->getAdjustments()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/queue/Itemize;->getItemizations()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/queue/Itemize;->description:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/squareup/server/payment/ItemizationsMessage;->forCapture(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/server/payment/ItemizationsMessage;

    move-result-object v0

    .line 103
    :goto_0
    iget-object v1, p0, Lcom/squareup/queue/Itemize;->paymentService:Lcom/squareup/server/payment/PaymentService;

    invoke-interface {v1, v0, p1}, Lcom/squareup/server/payment/PaymentService;->itemizations(Lcom/squareup/server/payment/ItemizationsMessage;Lcom/squareup/server/SquareCallback;)V

    return-void

    .line 99
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v3

    const-string v1, "Corrupt Itemize instance has no billId or paymentId: %s"

    .line 100
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Itemize;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method getAdjustments()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/queue/Itemize;->adjustmentList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/squareup/queue/Itemize;->adjustments:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/Itemize;->adjustmentList:Ljava/util/List;

    goto :goto_0

    .line 129
    :cond_0
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/Itemize;->adjustments:Ljava/lang/String;

    invoke-static {}, Lcom/squareup/queue/Itemize;->adjustmentListType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Itemize;->adjustmentList:Ljava/util/List;

    .line 133
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/Itemize;->adjustmentList:Ljava/util/List;

    return-object v0
.end method

.method getItemizations()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/queue/Itemize;->itemizationList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/squareup/queue/Itemize;->itemizations:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/Itemize;->itemizationList:Ljava/util/List;

    goto :goto_0

    .line 154
    :cond_0
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/Itemize;->itemizations:Ljava/lang/String;

    invoke-static {}, Lcom/squareup/queue/Itemize;->itemizationListType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Itemize;->itemizationList:Ljava/util/List;

    .line 158
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/Itemize;->itemizationList:Ljava/util/List;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 162
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/Itemize;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Itemize;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Itemize:{billId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Itemize;->billId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "paymentId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Itemize;->paymentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Itemize;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", adjustmentList: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {p0}, Lcom/squareup/queue/Itemize;->getAdjustments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemizationLists: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/queue/Itemize;->getItemizations()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
