.class public Lcom/squareup/queue/EmailReceipt$Builder;
.super Ljava/lang/Object;
.source "EmailReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/EmailReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private email:Ljava/lang/String;

.field private legacyBillId:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private paymentId:Ljava/lang/String;

.field private resend:Z

.field private uniqueKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/queue/EmailReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/queue/EmailReceipt$Builder;->legacyBillId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/queue/EmailReceipt$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/queue/EmailReceipt$Builder;)Z
    .locals 0

    .line 64
    iget-boolean p0, p0, Lcom/squareup/queue/EmailReceipt$Builder;->resend:Z

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/queue/EmailReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/queue/EmailReceipt$Builder;Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/queue/EmailReceipt$Builder;->legacyBillId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object p0

    return-object p0
.end method

.method private legacyBillId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 87
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt$Builder;->legacyBillId:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/EmailReceipt;
    .locals 2

    .line 107
    new-instance v0, Lcom/squareup/queue/EmailReceipt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/EmailReceipt;-><init>(Lcom/squareup/queue/EmailReceipt$Builder;Lcom/squareup/queue/EmailReceipt$1;)V

    return-object v0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public paymentId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method public paymentIdOrLegacyBillId(Ljava/lang/String;Lcom/squareup/PaymentType;)Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 72
    sget-object v0, Lcom/squareup/PaymentType;->BILL:Lcom/squareup/PaymentType;

    if-ne p2, v0, :cond_0

    .line 73
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt$Builder;->legacyBillId:Ljava/lang/String;

    goto :goto_0

    .line 75
    :cond_0
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt$Builder;->paymentId:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method resend(Z)Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 0

    .line 97
    iput-boolean p1, p0, Lcom/squareup/queue/EmailReceipt$Builder;->resend:Z

    return-object p0
.end method

.method uniqueKey(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method
