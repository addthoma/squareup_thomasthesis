.class final Lcom/squareup/queue/QueueService$RetrofitQueueStarter;
.super Ljava/lang/Object;
.source "QueueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RetrofitQueueStarter"
.end annotation


# instance fields
.field private final queue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final scope:Lmortar/MortarScope;

.field private final taskName:Ljava/lang/String;

.field private final taskWatcher:Lcom/squareup/queue/TaskWatcher;

.field final synthetic this$0:Lcom/squareup/queue/QueueService;


# direct methods
.method constructor <init>(Lcom/squareup/queue/QueueService;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;Ljava/lang/String;)V
    .locals 0

    .line 556
    iput-object p1, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->this$0:Lcom/squareup/queue/QueueService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 557
    iput-object p2, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->scope:Lmortar/MortarScope;

    .line 558
    iput-object p3, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->queue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 559
    iput-object p4, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->taskWatcher:Lcom/squareup/queue/TaskWatcher;

    .line 560
    iput-object p5, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->taskName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/QueueService$RetrofitQueueStarter;)Ljava/lang/String;
    .locals 0

    .line 549
    iget-object p0, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->taskName:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method startNextTask()Z
    .locals 4

    .line 569
    iget-object v0, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->this$0:Lcom/squareup/queue/QueueService;

    iget-object v1, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->scope:Lmortar/MortarScope;

    iget-object v2, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->queue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v3, p0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->taskWatcher:Lcom/squareup/queue/TaskWatcher;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/queue/QueueService;->access$800(Lcom/squareup/queue/QueueService;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;)Z

    move-result v0

    return v0
.end method
