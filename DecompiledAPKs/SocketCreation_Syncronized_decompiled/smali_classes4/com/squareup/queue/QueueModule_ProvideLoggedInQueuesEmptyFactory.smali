.class public final Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideLoggedInQueuesEmptyFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final pendingCapturesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;->pendingCapturesProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;->tasksProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLoggedInQueuesEmpty(Lcom/squareup/queue/retrofit/RetrofitQueue;Ljavax/inject/Provider;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)Z"
        }
    .end annotation

    .line 39
    invoke-static {p0, p1}, Lcom/squareup/queue/QueueModule;->provideLoggedInQueuesEmpty(Lcom/squareup/queue/retrofit/RetrofitQueue;Ljavax/inject/Provider;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;->pendingCapturesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;->tasksProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;->provideLoggedInQueuesEmpty(Lcom/squareup/queue/retrofit/RetrofitQueue;Ljavax/inject/Provider;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideLoggedInQueuesEmptyFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
