.class public Lcom/squareup/persistent/DefaultPersistentFactory;
.super Ljava/lang/Object;
.source "DefaultPersistentFactory.java"

# interfaces
.implements Lcom/squareup/persistent/PersistentFactory;


# static fields
.field private static final CHUNK_SIZE_16KB:I = 0x4000

.field private static final EMPTY_STRING_BYTES:[B


# instance fields
.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final gson:Lcom/google/gson/Gson;

.field private final mainThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    .line 25
    invoke-static {v0}, Lcom/squareup/util/Strings;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/squareup/persistent/DefaultPersistentFactory;->EMPTY_STRING_BYTES:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/gson/Gson;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0
    .param p1    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->gson:Lcom/google/gson/Gson;

    .line 36
    iput-object p2, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    .line 37
    iput-object p3, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/persistent/DefaultPersistentFactory;)Lcom/google/gson/Gson;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->gson:Lcom/google/gson/Gson;

    return-object p0
.end method

.method static synthetic access$100()[B
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/persistent/DefaultPersistentFactory;->EMPTY_STRING_BYTES:[B

    return-object v0
.end method


# virtual methods
.method public exists(Ljava/io/File;)Z
    .locals 0

    .line 41
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    return p1
.end method

.method public getByteFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/persistent/Persistent<",
            "[B>;"
        }
    .end annotation

    .line 73
    new-instance v0, Lcom/squareup/persistent/DefaultPersistentFactory$3;

    iget-object v1, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/squareup/persistent/DefaultPersistentFactory$3;-><init>(Lcom/squareup/persistent/DefaultPersistentFactory;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public getJsonFile(Ljava/io/File;Ljava/lang/reflect/Type;)Lcom/squareup/persistent/Persistent;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/File;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/squareup/persistent/Persistent<",
            "TT;>;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/persistent/DefaultPersistentFactory$1;

    iget-object v3, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/persistent/DefaultPersistentFactory$1;-><init>(Lcom/squareup/persistent/DefaultPersistentFactory;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/lang/reflect/Type;)V

    return-object v6
.end method

.method public getStringFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/persistent/Persistent<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 60
    new-instance v0, Lcom/squareup/persistent/DefaultPersistentFactory$2;

    iget-object v1, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/persistent/DefaultPersistentFactory;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/squareup/persistent/DefaultPersistentFactory$2;-><init>(Lcom/squareup/persistent/DefaultPersistentFactory;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method
