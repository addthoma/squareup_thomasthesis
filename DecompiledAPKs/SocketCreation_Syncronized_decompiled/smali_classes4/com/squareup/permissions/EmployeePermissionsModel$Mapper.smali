.class public final Lcom/squareup/permissions/EmployeePermissionsModel$Mapper;
.super Ljava/lang/Object;
.source "EmployeePermissionsModel.java"

# interfaces
.implements Lcom/squareup/sqldelight/prerelease/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeePermissionsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Mapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/permissions/EmployeePermissionsModel;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/sqldelight/prerelease/RowMapper<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final employeePermissionsModelFactory:Lcom/squareup/permissions/EmployeePermissionsModel$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/permissions/EmployeePermissionsModel$Factory<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/EmployeePermissionsModel$Factory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/EmployeePermissionsModel$Factory<",
            "TT;>;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/permissions/EmployeePermissionsModel$Mapper;->employeePermissionsModelFactory:Lcom/squareup/permissions/EmployeePermissionsModel$Factory;

    return-void
.end method


# virtual methods
.method public map(Landroid/database/Cursor;)Lcom/squareup/permissions/EmployeePermissionsModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/permissions/EmployeePermissionsModel$Mapper;->employeePermissionsModelFactory:Lcom/squareup/permissions/EmployeePermissionsModel$Factory;

    iget-object v0, v0, Lcom/squareup/permissions/EmployeePermissionsModel$Factory;->creator:Lcom/squareup/permissions/EmployeePermissionsModel$Creator;

    const/4 v1, 0x0

    .line 49
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 50
    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 48
    :goto_0
    invoke-interface {v0, v1, p1}, Lcom/squareup/permissions/EmployeePermissionsModel$Creator;->create(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/permissions/EmployeePermissionsModel;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic map(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeePermissionsModel$Mapper;->map(Landroid/database/Cursor;)Lcom/squareup/permissions/EmployeePermissionsModel;

    move-result-object p1

    return-object p1
.end method
