.class public final Lcom/squareup/permissions/PermissionGatekeepersKt;
.super Ljava/lang/Object;
.source "PermissionGatekeepers.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPermissionGatekeepers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PermissionGatekeepers.kt\ncom/squareup/permissions/PermissionGatekeepersKt\n*L\n1#1,73:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u001a\u001d\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005\u001a\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001d\u0010\t\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\n"
    }
    d2 = {
        "seekExplicitPermissionSuspend",
        "Lcom/squareup/permissions/PermissionAccessResult;",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "permission",
        "Lcom/squareup/permissions/Permission;",
        "(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "seekPermission",
        "Lio/reactivex/Single;",
        "",
        "seekPermissionSuspend",
        "employees_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final seekExplicitPermissionSuspend(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/Permission;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/permissions/PermissionAccessResult;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 61
    new-instance v0, Lkotlin/coroutines/SafeContinuation;

    invoke-static {p2}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->intercepted(Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/coroutines/SafeContinuation;-><init>(Lkotlin/coroutines/Continuation;)V

    move-object v1, v0

    check-cast v1, Lkotlin/coroutines/Continuation;

    .line 62
    new-instance v2, Lcom/squareup/permissions/PermissionGatekeepersKt$seekExplicitPermissionSuspend$2$1;

    invoke-direct {v2, v1}, Lcom/squareup/permissions/PermissionGatekeepersKt$seekExplicitPermissionSuspend$2$1;-><init>(Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p0, p1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    .line 61
    invoke-virtual {v0}, Lkotlin/coroutines/SafeContinuation;->getOrThrow()Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object p1

    if-ne p0, p1, :cond_0

    invoke-static {p2}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V

    :cond_0
    return-object p0
.end method

.method public static final seekPermission(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/Permission;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$seekPermission"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permission"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance v0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;-><init>(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "Single.create<Boolean> {\u2026false)\n      }\n    })\n  }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final seekPermissionSuspend(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/Permission;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/permissions/PermissionAccessResult;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 41
    new-instance v0, Lkotlin/coroutines/SafeContinuation;

    invoke-static {p2}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->intercepted(Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object v1

    invoke-direct {v0, v1}, Lkotlin/coroutines/SafeContinuation;-><init>(Lkotlin/coroutines/Continuation;)V

    move-object v1, v0

    check-cast v1, Lkotlin/coroutines/Continuation;

    .line 42
    new-instance v2, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1;

    invoke-direct {v2, v1}, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1;-><init>(Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p0, p1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    .line 41
    invoke-virtual {v0}, Lkotlin/coroutines/SafeContinuation;->getOrThrow()Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object p1

    if-ne p0, p1, :cond_0

    invoke-static {p2}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V

    :cond_0
    return-object p0
.end method
