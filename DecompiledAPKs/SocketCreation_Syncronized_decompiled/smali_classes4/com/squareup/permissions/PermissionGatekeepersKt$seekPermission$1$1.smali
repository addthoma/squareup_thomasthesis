.class public final Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "PermissionGatekeepers.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;->subscribe(Lio/reactivex/SingleEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/permissions/PermissionGatekeepersKt$seekPermission$1$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "failure",
        "",
        "success",
        "employees_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/SingleEmitter;


# direct methods
.method constructor <init>(Lio/reactivex/SingleEmitter;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1$1;->$emitter:Lio/reactivex/SingleEmitter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1$1;->$emitter:Lio/reactivex/SingleEmitter;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method public success()V
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1$1;->$emitter:Lio/reactivex/SingleEmitter;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method
