.class public final Lcom/squareup/permissions/ui/RealLockScreenMonitor$Companion$LOGGED_OUT_TOAST_BUNDLE$1;
.super Ljava/lang/Object;
.source "RealLockScreenMonitor.kt"

# interfaces
.implements Lcom/squareup/hudtoaster/HudToaster$ToastBundle;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/ui/RealLockScreenMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0005H\u0017J\u0008\u0010\u0006\u001a\u00020\u0005H\u0017\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/permissions/ui/RealLockScreenMonitor$Companion$LOGGED_OUT_TOAST_BUNDLE$1",
        "Lcom/squareup/hudtoaster/HudToaster$ToastBundle;",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "messageId",
        "",
        "titleId",
        "employees-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public glyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 209
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_LOGOUT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public messageId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public titleId()I
    .locals 1

    .line 213
    sget v0, Lcom/squareup/common/strings/R$string;->employee_management_logged_out:I

    return v0
.end method
