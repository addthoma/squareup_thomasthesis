.class public interface abstract Lcom/squareup/permissions/ui/LockScreenMonitor;
.super Ljava/lang/Object;
.source "LockScreenMonitor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/ui/LockScreenMonitor$NoLockScreenMonitor;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0008J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/permissions/ui/LockScreenMonitor;",
        "Lmortar/Scoped;",
        "getEmployeeLockScreenKey",
        "Lcom/squareup/container/ContainerTreeKey;",
        "goToEmployeeLockScreen",
        "",
        "showLockScreen",
        "",
        "NoLockScreenMonitor",
        "employees_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getEmployeeLockScreenKey()Lcom/squareup/container/ContainerTreeKey;
.end method

.method public abstract goToEmployeeLockScreen()V
.end method

.method public abstract showLockScreen()Z
.end method
