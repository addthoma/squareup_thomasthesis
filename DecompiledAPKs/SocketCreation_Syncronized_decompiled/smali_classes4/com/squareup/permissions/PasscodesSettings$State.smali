.class public Lcom/squareup/permissions/PasscodesSettings$State;
.super Ljava/lang/Object;
.source "PasscodesSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PasscodesSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    }
.end annotation


# instance fields
.field public final passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

.field public final passcodesEnabled:Z

.field public final previousTeamPasscode:Ljava/lang/String;

.field public final requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

.field public final requirePasscodeAfterEachSale:Z

.field public final requirePasscodeAfterLogout:Z

.field public final requirePasscodeWhenBackingOutOfSale:Z

.field public final teamPasscode:Ljava/lang/String;

.field public final teamPasscodeEnabled:Z

.field public final teamPermissionRoleHasSharedPosAccess:Z

.field public final teamRoleToken:Ljava/lang/String;

.field public final unsavedOwnerPasscode:Ljava/lang/String;

.field public final unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

.field public final unsavedTeamPasscode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PasscodesSettings$RequestState;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/squareup/permissions/PasscodesSettings$Timeout;ZZ)V
    .locals 0

    .line 540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 542
    iput-boolean p2, p0, Lcom/squareup/permissions/PasscodesSettings$State;->passcodesEnabled:Z

    .line 543
    iput-object p3, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamRoleToken:Ljava/lang/String;

    .line 544
    iput-boolean p4, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    .line 545
    iput-object p5, p0, Lcom/squareup/permissions/PasscodesSettings$State;->previousTeamPasscode:Ljava/lang/String;

    .line 546
    iput-object p6, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    .line 547
    iput-object p7, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedTeamPasscode:Ljava/lang/String;

    .line 548
    iput-object p8, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscode:Ljava/lang/String;

    .line 549
    iput-object p9, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    .line 550
    iput-boolean p10, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeAfterEachSale:Z

    .line 551
    iput-boolean p11, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeWhenBackingOutOfSale:Z

    .line 552
    iput-object p12, p0, Lcom/squareup/permissions/PasscodesSettings$State;->passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 553
    iput-boolean p13, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeAfterLogout:Z

    .line 554
    iput-boolean p14, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPermissionRoleHasSharedPosAccess:Z

    return-void
.end method


# virtual methods
.method public builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 2

    .line 558
    new-instance v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    invoke-direct {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->passcodesEnabled:Z

    .line 559
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPasscodesEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamRoleToken:Ljava/lang/String;

    .line 560
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamRoleToken(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    .line 561
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPasscodeEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->previousTeamPasscode:Ljava/lang/String;

    .line 562
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPreviousTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    .line 563
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedTeamPasscode:Ljava/lang/String;

    .line 564
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscode:Ljava/lang/String;

    .line 565
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedOwnerPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    .line 566
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedOwnerPasscodeConfirmation(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeAfterEachSale:Z

    .line 567
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterEachSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeWhenBackingOutOfSale:Z

    .line 568
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeWhenBackingOutOfSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 569
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPasscodeTimeout(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeAfterLogout:Z

    .line 570
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterLogout(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPermissionRoleHasSharedPosAccess:Z

    .line 571
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPermissionRoleHasSharedPosAccess(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    return-object v0
.end method
