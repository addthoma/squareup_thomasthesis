.class final Lcom/squareup/permissions/SqliteEmployeesStore$update$1;
.super Ljava/lang/Object;
.source "SqliteEmployeesStore.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/SqliteEmployeesStore;->update(Ljava/util/Set;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSqliteEmployeesStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SqliteEmployeesStore.kt\ncom/squareup/permissions/SqliteEmployeesStore$update$1\n+ 2 extensions.kt\ncom/squareup/sqlbrite3/ExtensionsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,195:1\n94#2,6:196\n100#2,4:204\n1642#3,2:202\n*E\n*S KotlinDebug\n*F\n+ 1 SqliteEmployeesStore.kt\ncom/squareup/permissions/SqliteEmployeesStore$update$1\n*L\n90#1,6:196\n90#1,4:204\n90#1,2:202\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $employees:Ljava/util/Set;

.field final synthetic this$0:Lcom/squareup/permissions/SqliteEmployeesStore;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/SqliteEmployeesStore;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;->this$0:Lcom/squareup/permissions/SqliteEmployeesStore;

    iput-object p2, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;->$employees:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 90
    iget-object v0, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;->this$0:Lcom/squareup/permissions/SqliteEmployeesStore;

    invoke-static {v0}, Lcom/squareup/permissions/SqliteEmployeesStore;->access$getDb$p(Lcom/squareup/permissions/SqliteEmployeesStore;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->newTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    move-result-object v1

    :try_start_0
    const-string v2, "transaction"

    .line 201
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "employees_table"

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 91
    invoke-virtual {v0, v2, v5, v4}, Lcom/squareup/sqlbrite3/BriteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "employee_permissions_table"

    new-array v3, v3, [Ljava/lang/String;

    .line 92
    invoke-virtual {v0, v2, v5, v3}, Lcom/squareup/sqlbrite3/BriteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 93
    iget-object v2, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;->$employees:Ljava/util/Set;

    check-cast v2, Ljava/lang/Iterable;

    .line 202
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/permissions/Employee;

    .line 94
    iget-object v4, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;->this$0:Lcom/squareup/permissions/SqliteEmployeesStore;

    invoke-static {v4, v3}, Lcom/squareup/permissions/SqliteEmployeesStore;->access$updatePasscodeIfOwner(Lcom/squareup/permissions/SqliteEmployeesStore;Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/Employee;

    move-result-object v3

    .line 95
    iget-object v4, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;->this$0:Lcom/squareup/permissions/SqliteEmployeesStore;

    invoke-static {v4, v3, v0}, Lcom/squareup/permissions/SqliteEmployeesStore;->access$insertEmployeeInDatabase(Lcom/squareup/permissions/SqliteEmployeesStore;Lcom/squareup/permissions/Employee;Lcom/squareup/sqlbrite3/BriteDatabase;)V

    goto :goto_0

    .line 97
    :cond_0
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 204
    invoke-interface {v1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->markSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    invoke-interface {v1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    throw v0
.end method
