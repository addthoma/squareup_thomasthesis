.class public Lcom/squareup/permissions/EmployeeCacheUpdater;
.super Ljava/lang/Object;
.source "EmployeeCacheUpdater.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/jailkeeper/JailKeeperService;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0017\u0018\u00002\u00020\u00012\u00020\u0002B3\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000cJ\u0006\u0010\u0013\u001a\u00020\u0014J\u0010\u0010\u0015\u001a\u00020\u00142\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u0012J\u001e\u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001a0\u00190\u00182\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u0012H\u0002J\u0008\u0010\u001b\u001a\u00020\u000fH\u0016J\u0010\u0010\u001c\u001a\u00020\u000f2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u000fH\u0016J\u0008\u0010 \u001a\u00020\u0014H\u0016J\u0008\u0010!\u001a\u00020\u000fH\u0016J\u0008\u0010\"\u001a\u00020\u0014H\u0016R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00120\u00120\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/permissions/EmployeeCacheUpdater;",
        "Lmortar/Scoped;",
        "Lcom/squareup/jailkeeper/JailKeeperService;",
        "employeeManagementModeDecider",
        "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
        "employees",
        "Lcom/squareup/permissions/Employees;",
        "service",
        "Lcom/squareup/server/employees/EmployeesService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "computationScheduler",
        "(Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/Employees;Lcom/squareup/server/employees/EmployeesService;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V",
        "forceRefreshRequested",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "refreshRequested",
        "",
        "cacheHasActiveEmployees",
        "Lio/reactivex/Completable;",
        "fetchAndUpdateEmployees",
        "force",
        "fetchEmployees",
        "Lio/reactivex/Single;",
        "",
        "Lcom/squareup/permissions/Employee;",
        "forceRefresh",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "preload",
        "refresh",
        "reload",
        "employees_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final computationScheduler:Lio/reactivex/Scheduler;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final employees:Lcom/squareup/permissions/Employees;

.field private final forceRefreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final service:Lcom/squareup/server/employees/EmployeesService;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/Employees;Lcom/squareup/server/employees/EmployeesService;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "employeeManagementModeDecider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employees"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "service"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computationScheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iput-object p2, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->employees:Lcom/squareup/permissions/Employees;

    iput-object p3, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->service:Lcom/squareup/server/employees/EmployeesService;

    iput-object p4, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->computationScheduler:Lio/reactivex/Scheduler;

    .line 54
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Boolean>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 55
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->forceRefreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getComputationScheduler$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lio/reactivex/Scheduler;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->computationScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getEmployees$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lcom/squareup/permissions/Employees;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->employees:Lcom/squareup/permissions/Employees;

    return-object p0
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lio/reactivex/Scheduler;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getRefreshRequested$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getService$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lcom/squareup/server/employees/EmployeesService;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->service:Lcom/squareup/server/employees/EmployeesService;

    return-object p0
.end method

.method public static synthetic fetchAndUpdateEmployees$default(Lcom/squareup/permissions/EmployeeCacheUpdater;ZILjava/lang/Object;)Lio/reactivex/Completable;
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 105
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string p2, "FALSE"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeeCacheUpdater;->fetchAndUpdateEmployees(Z)Lio/reactivex/Completable;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: fetchAndUpdateEmployees"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final fetchEmployees(Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 129
    new-instance v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$1;

    iget-object v1, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-direct {v0, v1}, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$1;-><init>(Lcom/squareup/permissions/EmployeeManagementModeDecider;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v1, Lcom/squareup/permissions/EmployeeCacheUpdaterKt$sam$java_util_concurrent_Callable$0;

    invoke-direct {v1, v0}, Lcom/squareup/permissions/EmployeeCacheUpdaterKt$sam$java_util_concurrent_Callable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-static {v1}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v0

    .line 130
    new-instance v1, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2;-><init>(Lcom/squareup/permissions/EmployeeCacheUpdater;Z)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 163
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single\n        .fromCall\u2026.observeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic fetchEmployees$default(Lcom/squareup/permissions/EmployeeCacheUpdater;ZILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 127
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string p2, "FALSE"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/permissions/EmployeeCacheUpdater;->fetchEmployees(Z)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: fetchEmployees"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public final cacheHasActiveEmployees()Lio/reactivex/Completable;
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->employees:Lcom/squareup/permissions/Employees;

    invoke-virtual {v0}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/squareup/permissions/EmployeeCacheUpdater$cacheHasActiveEmployees$1;->INSTANCE:Lcom/squareup/permissions/EmployeeCacheUpdater$cacheHasActiveEmployees$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 68
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lio/reactivex/Observable;->ignoreElements()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "employees.activeEmployee\u2026        .ignoreElements()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final fetchAndUpdateEmployees(Z)Lio/reactivex/Completable;
    .locals 1

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/permissions/EmployeeCacheUpdater;->fetchEmployees(Z)Lio/reactivex/Single;

    move-result-object p1

    .line 109
    sget-object v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$1;->INSTANCE:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 110
    new-instance v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$2;

    invoke-direct {v0, p0}, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$2;-><init>(Lcom/squareup/permissions/EmployeeCacheUpdater;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object p1

    .line 113
    sget-object v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$3;->INSTANCE:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$3;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->onErrorComplete(Lio/reactivex/functions/Predicate;)Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "fetchEmployees(force)\n  \u2026it)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public forceRefresh()V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->forceRefreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 10

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-static {p0, v0, v1, v2}, Lcom/squareup/permissions/EmployeeCacheUpdater;->fetchAndUpdateEmployees$default(Lcom/squareup/permissions/EmployeeCacheUpdater;ZILjava/lang/Object;)Lio/reactivex/Completable;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/squareup/permissions/EmployeeCacheUpdater$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/EmployeeCacheUpdater$onEnterScope$1;-><init>(Lcom/squareup/permissions/EmployeeCacheUpdater;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Completable;

    move-result-object v2

    const-string v0, "fetchAndUpdateEmployees(\u2026Requested.accept(FALSE) }"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v3

    const-string v0, "refreshRequested.toFlowable(LATEST)"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 91
    iget-object v9, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v4, 0x0

    const-wide/32 v6, 0x493e0

    .line 86
    invoke-static/range {v2 .. v9}, Lcom/squareup/util/rx2/Rx2TransformersKt;->repeatWhen(Lio/reactivex/Completable;Lio/reactivex/Flowable;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "fetchAndUpdateEmployees(\u2026   )\n        .subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->forceRefreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 98
    new-instance v1, Lcom/squareup/permissions/EmployeeCacheUpdater$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/EmployeeCacheUpdater$onEnterScope$2;-><init>(Lcom/squareup/permissions/EmployeeCacheUpdater;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "forceRefreshRequested\n  \u2026E) }\n        .subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public preload()Lio/reactivex/Completable;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->updateMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 74
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->modeSupportsEmployeeCache()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/squareup/permissions/EmployeeCacheUpdater;->cacheHasActiveEmployees()Lio/reactivex/Completable;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_0
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public refresh()V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public reload()Lio/reactivex/Completable;
    .locals 2

    .line 81
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
