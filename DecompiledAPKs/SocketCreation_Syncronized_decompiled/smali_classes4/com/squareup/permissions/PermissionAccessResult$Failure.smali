.class public final Lcom/squareup/permissions/PermissionAccessResult$Failure;
.super Lcom/squareup/permissions/PermissionAccessResult;
.source "PermissionGatekeepers.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PermissionAccessResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failure"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/permissions/PermissionAccessResult$Failure;",
        "Lcom/squareup/permissions/PermissionAccessResult;",
        "()V",
        "employees_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/permissions/PermissionAccessResult$Failure;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/permissions/PermissionAccessResult$Failure;

    invoke-direct {v0}, Lcom/squareup/permissions/PermissionAccessResult$Failure;-><init>()V

    sput-object v0, Lcom/squareup/permissions/PermissionAccessResult$Failure;->INSTANCE:Lcom/squareup/permissions/PermissionAccessResult$Failure;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, v0}, Lcom/squareup/permissions/PermissionAccessResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
