.class final Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;
.super Ljava/lang/Object;
.source "PermissionGatekeepers.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/PermissionGatekeepersKt;->seekPermission(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $permission:Lcom/squareup/permissions/Permission;

.field final synthetic $this_seekPermission:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;->$this_seekPermission:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p2, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;->$permission:Lcom/squareup/permissions/Permission;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;->$this_seekPermission:Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1;->$permission:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1$1;

    invoke-direct {v2, p1}, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermission$1$1;-><init>(Lio/reactivex/SingleEmitter;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
