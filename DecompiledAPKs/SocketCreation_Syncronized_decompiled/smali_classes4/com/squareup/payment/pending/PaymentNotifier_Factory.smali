.class public final Lcom/squareup/payment/pending/PaymentNotifier_Factory;
.super Ljava/lang/Object;
.source "PaymentNotifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/pending/PaymentNotifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final appContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final jobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field

.field private final jobNotificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingPaymentNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/PendingPaymentNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final queueConformerWatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformerWatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformerWatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/PendingPaymentNotifier;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->appContextProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->queueConformerWatcherProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->pendingPaymentNotifierProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/pending/PaymentNotifier_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformerWatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/PendingPaymentNotifier;",
            ">;)",
            "Lcom/squareup/payment/pending/PaymentNotifier_Factory;"
        }
    .end annotation

    .line 70
    new-instance v9, Lcom/squareup/payment/pending/PaymentNotifier_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/payment/pending/PaymentNotifier_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/queue/redundant/QueueConformerWatcher;Lcom/squareup/notifications/PendingPaymentNotifier;)Lcom/squareup/payment/pending/PaymentNotifier;
    .locals 10

    .line 78
    new-instance v9, Lcom/squareup/payment/pending/PaymentNotifier;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/payment/pending/PaymentNotifier;-><init>(Landroid/app/Application;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/queue/redundant/QueueConformerWatcher;Lcom/squareup/notifications/PendingPaymentNotifier;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/payment/pending/PaymentNotifier;
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->appContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/pending/PendingTransactionsStore;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->queueConformerWatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/queue/redundant/QueueConformerWatcher;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->pendingPaymentNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/notifications/PendingPaymentNotifier;

    invoke-static/range {v1 .. v8}, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/queue/redundant/QueueConformerWatcher;Lcom/squareup/notifications/PendingPaymentNotifier;)Lcom/squareup/payment/pending/PaymentNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/payment/pending/PaymentNotifier_Factory;->get()Lcom/squareup/payment/pending/PaymentNotifier;

    move-result-object v0

    return-object v0
.end method
