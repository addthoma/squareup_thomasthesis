.class Lcom/squareup/payment/BillPayment$LastAddedTender;
.super Ljava/lang/Object;
.source "BillPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/BillPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LastAddedTender"
.end annotation


# instance fields
.field final builder:Lcom/squareup/payment/tender/BaseTender$Builder;

.field captured:Z

.field receipt:Lcom/squareup/payment/PaymentReceipt;

.field final tender:Lcom/squareup/payment/tender/BaseTender;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/BaseTender$Builder;Z)V
    .locals 0

    .line 1595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1596
    iput-object p1, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->builder:Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 1597
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->build()Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method


# virtual methods
.method captureAndCreateReceipt(Lcom/squareup/payment/PaymentReceipt$Factory;)V
    .locals 1

    const/4 v0, 0x1

    .line 1601
    iput-boolean v0, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->captured:Z

    .line 1602
    iget-object v0, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->captureAndCreateReceipt(Lcom/squareup/payment/PaymentReceipt$Factory;)Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->receipt:Lcom/squareup/payment/PaymentReceipt;

    return-void
.end method

.method consumeReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 2

    .line 1606
    iget-object v0, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->receipt:Lcom/squareup/payment/PaymentReceipt;

    const-string v1, "receipt"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/PaymentReceipt;

    const/4 v1, 0x0

    .line 1607
    iput-object v1, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->receipt:Lcom/squareup/payment/PaymentReceipt;

    return-object v0
.end method
