.class Lcom/squareup/payment/tender/SmartCardTender$1;
.super Ljava/lang/Object;
.source "SmartCardTender.java"

# interfaces
.implements Lcom/squareup/Card$InputType$InputTypeHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/tender/SmartCardTender;->getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/Card$InputType$InputTypeHandler<",
        "Lcom/squareup/protos/client/bills/CardData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/tender/SmartCardTender;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/SmartCardTender;)V
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTender$1;->this$0:Lcom/squareup/payment/tender/SmartCardTender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleA10(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 227
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The A10 is no longer."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic handleA10(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleA10(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public handleGen2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 211
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t use gen2 reader as a fallback"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic handleGen2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleGen2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public handleManual(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 207
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t use CNP as a fallback."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic handleManual(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleManual(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public handleO1(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 215
    new-instance p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->O1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender$1;->this$0:Lcom/squareup/payment/tender/SmartCardTender;

    invoke-static {v0}, Lcom/squareup/payment/tender/SmartCardTender;->access$000(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$O1Card;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card(Lcom/squareup/protos/client/bills/CardData$O1Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleO1(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleO1(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public handleR4(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 219
    new-instance p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender$1;->this$0:Lcom/squareup/payment/tender/SmartCardTender;

    invoke-static {v0}, Lcom/squareup/payment/tender/SmartCardTender;->access$100(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$R4Card;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card(Lcom/squareup/protos/client/bills/CardData$R4Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleR4(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleR4(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public handleR6(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 223
    new-instance p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender$1;->this$0:Lcom/squareup/payment/tender/SmartCardTender;

    invoke-static {v0}, Lcom/squareup/payment/tender/SmartCardTender;->access$200(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card(Lcom/squareup/protos/client/bills/CardData$R6Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleR6(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleR6(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public handleT2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 235
    new-instance p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender$1;->this$0:Lcom/squareup/payment/tender/SmartCardTender;

    invoke-static {v0}, Lcom/squareup/payment/tender/SmartCardTender;->access$400(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card(Lcom/squareup/protos/client/bills/CardData$T2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleT2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleT2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public handleX2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 231
    new-instance p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender$1;->this$0:Lcom/squareup/payment/tender/SmartCardTender;

    invoke-static {v0}, Lcom/squareup/payment/tender/SmartCardTender;->access$300(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleX2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTender$1;->handleX2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method
