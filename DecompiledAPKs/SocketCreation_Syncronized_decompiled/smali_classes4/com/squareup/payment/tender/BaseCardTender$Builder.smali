.class public abstract Lcom/squareup/payment/tender/BaseCardTender$Builder;
.super Lcom/squareup/payment/tender/BaseTender$Builder;
.source "BaseCardTender.java"

# interfaces
.implements Lcom/squareup/payment/AcceptsTips;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/BaseCardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# instance fields
.field protected cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field protected final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field protected final delayCapture:Z

.field protected final isEmvCapableReaderPresent:Z

.field protected final isTakingPaymentFromInvoice:Z

.field protected final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field protected final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field protected final shouldBeReopenable:Z

.field protected final showSignatureForCardNotPresent:Z

.field protected final showSignatureForCardOnFile:Z

.field protected tipAmount:Lcom/squareup/protos/common/Money;

.field protected tipPercentage:Lcom/squareup/util/Percentage;

.field protected final tipSettings:Lcom/squareup/settings/server/TipSettings;

.field protected final tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

.field protected final usePreAuthTipping:Z

.field protected final useSeparateTippingScreen:Z


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V
    .locals 1

    .line 563
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->CARD:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/payment/tender/BaseTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/String;)V

    .line 565
    iget-object p2, p1, Lcom/squareup/payment/tender/TenderFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 566
    iget-object p2, p1, Lcom/squareup/payment/tender/TenderFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 567
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 568
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->isEmvCapableReaderPresent()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->isEmvCapableReaderPresent:Z

    .line 569
    iget-object p2, p1, Lcom/squareup/payment/tender/TenderFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 570
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->tipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 571
    new-instance p2, Lcom/squareup/tipping/TippingCalculator;

    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-direct {p2, v0}, Lcom/squareup/tipping/TippingCalculator;-><init>(Lcom/squareup/settings/server/TipSettings;)V

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    .line 572
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->usePreAuthTipping()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->usePreAuthTipping:Z

    .line 573
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->showSignatureForCardNotPresent()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->showSignatureForCardNotPresent:Z

    .line 574
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->showSignatureForCardOnFile()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->showSignatureForCardOnFile:Z

    .line 576
    iget-boolean p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->usePreAuthTipping:Z

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 577
    invoke-virtual {p2}, Lcom/squareup/settings/server/TipSettings;->isUsingSeparateTippingScreen()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput-boolean p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->useSeparateTippingScreen:Z

    const/4 p2, 0x0

    .line 578
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipAmount:Lcom/squareup/protos/common/Money;

    .line 579
    invoke-virtual {p1}, Lcom/squareup/payment/tender/TenderFactory;->isTakingPaymentForInvoice()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->isTakingPaymentFromInvoice:Z

    .line 580
    iget-object p1, p1, Lcom/squareup/payment/tender/TenderFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseBillAmendments()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->shouldBeReopenable:Z

    .line 581
    iget-object p1, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isDelayCapture()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->delayCapture:Z

    return-void
.end method


# virtual methods
.method public askForTip()Z
    .locals 3

    .line 595
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender$Builder;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    .line 596
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v2

    .line 595
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tipping/TippingCalculator;->askForTip(Lcom/squareup/protos/common/Money;Z)Z

    move-result v0

    return v0
.end method

.method public getAmountForTipping()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 627
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingTipPreTax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender$Builder;->getAmountWithoutTax()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 630
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 635
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object v0
.end method

.method public getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 605
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender$Builder;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tipping/TippingCalculator;->customTipMaxMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getMaxAmount()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 589
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 590
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 591
    invoke-static {v1, v0}, Lcom/squareup/money/MoneyMath;->min(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getMinAmount()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 585
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 609
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getTipOptions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 600
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender$Builder;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    .line 601
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v2

    .line 600
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tipping/TippingCalculator;->tipOptions(Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 640
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method

.method public setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
    .locals 0

    .line 613
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipAmount:Lcom/squareup/protos/common/Money;

    .line 614
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipPercentage:Lcom/squareup/util/Percentage;

    return-void
.end method

.method public tipOnPrintedReceipt()Z
    .locals 3

    .line 622
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 623
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " does not implement tipOnPrintedReceipt()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public useSeparateTippingScreen()Z
    .locals 1

    .line 618
    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender$Builder;->useSeparateTippingScreen:Z

    return v0
.end method
