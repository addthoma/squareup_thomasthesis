.class public abstract Lcom/squareup/payment/tender/BaseLocalTender;
.super Lcom/squareup/payment/tender/BaseTender;
.source "BaseLocalTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/BaseLocalTender$Builder;
    }
.end annotation


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private final eventType:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field private final localTenderCache:Lcom/squareup/print/LocalTenderCache;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V
    .locals 1

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseTender;-><init>(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 19
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->access$000(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    .line 20
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->access$100(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->eventType:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 21
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->access$200(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)Lcom/squareup/print/LocalTenderCache;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/BaseLocalTender;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-void
.end method


# virtual methods
.method public captureAndCreateReceipt(Lcom/squareup/payment/PaymentReceipt$Factory;)Lcom/squareup/payment/PaymentReceipt;
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->eventType:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseLocalTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseLocalTender;->eventType:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    iget-object v3, p0, Lcom/squareup/payment/tender/BaseLocalTender;->clientId:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->addTenderWithId(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    .line 33
    invoke-interface {p1, v0, v0}, Lcom/squareup/payment/PaymentReceipt$Factory;->createTenderReceipt(Lcom/squareup/payment/Obfuscated;Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    return-object p1
.end method

.method protected getCashDrawerShiftId()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->getCurrentCashDrawerShiftId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getReceiptNumber()Ljava/lang/String;
    .locals 2

    .line 56
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptNumber()Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseLocalTender;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/LocalTenderCache;->getLastReceiptNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getServerId()Ljava/lang/String;
    .locals 2

    .line 44
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseTender;->getServerId()Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseLocalTender;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseLocalTender;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/LocalTenderCache;->getLastTenderServerId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public isLocalTender()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
