.class public abstract Lcom/squareup/payment/tender/BaseTender;
.super Ljava/lang/Object;
.source "BaseTender.java"

# interfaces
.implements Lcom/squareup/payment/crm/HoldsCustomer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/BaseTender$Builder;,
        Lcom/squareup/payment/tender/BaseTender$ReturnsChange;,
        Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;
    }
.end annotation


# instance fields
.field private amount:Lcom/squareup/protos/common/Money;

.field private amountWithoutTax:Lcom/squareup/protos/common/Money;

.field private autoGratuityAmount:Lcom/squareup/protos/common/Money;

.field public final clientId:Ljava/lang/String;

.field private final customerChanged:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private customerContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private customerInstruments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field private customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

.field private final deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

.field private final employeeToken:Ljava/lang/String;

.field private final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final merchantToken:Ljava/lang/String;

.field private receiptDestination:Ljava/lang/String;

.field private receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

.field private receiptDetails:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

.field private remainingBalance:Lcom/squareup/protos/common/Money;

.field protected final res:Lcom/squareup/util/Res;

.field private surchargeAmount:Lcom/squareup/protos/common/Money;

.field protected final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private tender:Lcom/squareup/protos/client/bills/Tender;

.field private final tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

.field private final tenderType:Lcom/squareup/protos/client/bills/Tender$Type;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/BaseTender$Builder;)V
    .locals 2

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->customerChanged:Lrx/subjects/PublishSubject;

    const/4 v0, 0x0

    .line 93
    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    .line 94
    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    .line 97
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 98
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->locationProvider:Ljavax/inject/Provider;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->locationProvider:Ljavax/inject/Provider;

    .line 99
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    .line 100
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->res:Lcom/squareup/util/Res;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->res:Lcom/squareup/util/Res;

    .line 101
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 102
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$000(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->amount:Lcom/squareup/protos/common/Money;

    .line 103
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$100(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->amountWithoutTax:Lcom/squareup/protos/common/Money;

    .line 104
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$200(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    .line 105
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$300(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->surchargeAmount:Lcom/squareup/protos/common/Money;

    .line 106
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->clientId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->clientId:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    .line 107
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseTender$Builder;->employeeToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->employeeToken:Ljava/lang/String;

    .line 108
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$400(Lcom/squareup/payment/tender/BaseTender$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->merchantToken:Ljava/lang/String;

    .line 109
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$500(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/server/account/protos/DeviceCredential;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

    .line 111
    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$600(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$700(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->access$800(Lcom/squareup/payment/tender/BaseTender$Builder;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, v0, v1, p1}, Lcom/squareup/payment/tender/BaseTender;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    return-void
.end method

.method private formatLocale(Ljava/util/Locale;)Ljava/lang/String;
    .locals 0

    .line 457
    invoke-static {p1}, Lcom/squareup/util/LocaleHelper;->getDelimitedStringFromLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getLocation()Lcom/squareup/protos/client/GeoLocation;
    .locals 5

    .line 377
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->locationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "No geolocation available"

    .line 379
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    .line 382
    :cond_0
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    .line 383
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    .line 384
    new-instance v0, Lcom/squareup/protos/client/GeoLocation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/GeoLocation$Builder;-><init>()V

    .line 385
    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/GeoLocation$Builder;->latitude(Ljava/lang/String;)Lcom/squareup/protos/client/GeoLocation$Builder;

    move-result-object v0

    .line 386
    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/GeoLocation$Builder;->longitude(Ljava/lang/String;)Lcom/squareup/protos/client/GeoLocation$Builder;

    move-result-object v0

    .line 387
    invoke-virtual {v0}, Lcom/squareup/protos/client/GeoLocation$Builder;->build()Lcom/squareup/protos/client/GeoLocation;

    move-result-object v0

    return-object v0
.end method

.method private getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
    .locals 1

    .line 483
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private getReceiptTask()Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 3

    .line 498
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 502
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getServerId()Ljava/lang/String;

    move-result-object v0

    .line 503
    sget-object v1, Lcom/squareup/payment/tender/BaseTender$1;->$SwitchMap$com$squareup$payment$tender$BaseTender$ReceiptDestinationType:[I

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    invoke-virtual {v2}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 524
    new-instance v1, Lcom/squareup/queue/SmsReceiptById$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/SmsReceiptById$Builder;-><init>()V

    .line 525
    invoke-virtual {v1, v0}, Lcom/squareup/queue/SmsReceiptById$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceiptById$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    .line 526
    invoke-virtual {v0, v1}, Lcom/squareup/queue/SmsReceiptById$Builder;->phoneId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceiptById$Builder;

    move-result-object v0

    .line 527
    invoke-virtual {v0}, Lcom/squareup/queue/SmsReceiptById$Builder;->build()Lcom/squareup/queue/SmsReceiptById;

    move-result-object v0

    return-object v0

    .line 529
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown receipt type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    invoke-virtual {v2}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 519
    :cond_2
    new-instance v1, Lcom/squareup/queue/SmsReceipt$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/SmsReceipt$Builder;-><init>()V

    .line 520
    invoke-virtual {v1, v0}, Lcom/squareup/queue/SmsReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    .line 521
    invoke-virtual {v0, v1}, Lcom/squareup/queue/SmsReceipt$Builder;->phone(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;

    move-result-object v0

    .line 522
    invoke-virtual {v0}, Lcom/squareup/queue/SmsReceipt$Builder;->build()Lcom/squareup/queue/SmsReceipt;

    move-result-object v0

    return-object v0

    .line 514
    :cond_3
    new-instance v1, Lcom/squareup/queue/EmailReceiptById$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/EmailReceiptById$Builder;-><init>()V

    .line 515
    invoke-virtual {v1, v0}, Lcom/squareup/queue/EmailReceiptById$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceiptById$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    .line 516
    invoke-virtual {v0, v1}, Lcom/squareup/queue/EmailReceiptById$Builder;->emailId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceiptById$Builder;

    move-result-object v0

    .line 517
    invoke-virtual {v0}, Lcom/squareup/queue/EmailReceiptById$Builder;->build()Lcom/squareup/queue/EmailReceiptById;

    move-result-object v0

    return-object v0

    .line 509
    :cond_4
    new-instance v1, Lcom/squareup/queue/EmailReceipt$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/EmailReceipt$Builder;-><init>()V

    .line 510
    invoke-virtual {v1, v0}, Lcom/squareup/queue/EmailReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    .line 511
    invoke-virtual {v0, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->email(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    .line 512
    invoke-virtual {v0}, Lcom/squareup/queue/EmailReceipt$Builder;->build()Lcom/squareup/queue/EmailReceipt;

    move-result-object v0

    return-object v0

    .line 505
    :cond_5
    new-instance v1, Lcom/squareup/queue/DeclineReceipt$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/DeclineReceipt$Builder;-><init>()V

    .line 506
    invoke-virtual {v1, v0}, Lcom/squareup/queue/DeclineReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/DeclineReceipt$Builder;

    move-result-object v0

    .line 507
    invoke-virtual {v0}, Lcom/squareup/queue/DeclineReceipt$Builder;->build()Lcom/squareup/queue/DeclineReceipt;

    move-result-object v0

    return-object v0
.end method

.method private nowAsIsoDate()Lcom/squareup/protos/client/ISO8601Date;
    .locals 1

    .line 373
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    return-object v0
.end method

.method private setTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 400
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 405
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDetails:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 406
    iput-object p3, p0, Lcom/squareup/payment/tender/BaseTender;->remainingBalance:Lcom/squareup/protos/common/Money;

    return-void

    .line 401
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "tenderId does not match client token: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method


# virtual methods
.method public canAutoFlush()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract captureAndCreateReceipt(Lcom/squareup/payment/PaymentReceipt$Factory;)Lcom/squareup/payment/PaymentReceipt;
.end method

.method public createEncodedSignatureData()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public declineReceipt()V
    .locals 1

    .line 236
    sget-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->DECLINE:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    const/4 v0, 0x0

    .line 237
    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    return-void
.end method

.method public emailReceipt(Ljava/lang/String;)V
    .locals 1

    .line 251
    sget-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->EMAIL:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    .line 252
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    return-void
.end method

.method public emailReceiptById(Ljava/lang/String;)V
    .locals 1

    .line 256
    sget-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->EMAIL_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    .line 257
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    return-void
.end method

.method public enqueueAttachContactTask()V
    .locals 4

    .line 410
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getServerId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "serverId"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 413
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v2, Lcom/squareup/queue/crm/AttachContactTask$Builder;

    invoke-direct {v2}, Lcom/squareup/queue/crm/AttachContactTask$Builder;-><init>()V

    .line 414
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->contactToken(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/BaseTender;->merchantToken:Ljava/lang/String;

    .line 415
    invoke-virtual {v2, v3}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->merchantToken(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;

    move-result-object v2

    .line 416
    invoke-virtual {v2, v0}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;

    move-result-object v0

    .line 417
    invoke-virtual {v0}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->build()Lcom/squareup/queue/crm/AttachContactTask;

    move-result-object v0

    .line 413
    invoke-interface {v1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public enqueueReceipt()V
    .locals 2

    .line 422
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptTask()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 424
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {v1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public enqueueSignature()V
    .locals 0

    return-void
.end method

.method public getAllSurchargesAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->surchargeAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getAmountWithoutTax()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->amountWithoutTax:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getAutoGratuityAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getBuyerLocaleString(Ljava/util/Locale;)Ljava/lang/String;
    .locals 0

    .line 453
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/BaseTender;->getBuyerSelectedLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseTender;->formatLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getBuyerSelectedLocale(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 2

    .line 444
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 446
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 445
    invoke-static {v0, p1}, Lcom/squareup/util/LocaleHelper;->getLocaleFromDelimitedString(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method protected getCashDrawerShiftId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public getCompleteDetails()Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCompleteTenderDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .locals 1

    .line 353
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    return-object v0
.end method

.method public getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getCustomerId()Ljava/lang/String;
    .locals 1

    .line 216
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getCustomerInstrumentDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .line 228
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->customerInstruments:Ljava/util/List;

    return-object v0
.end method

.method public getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object v0
.end method

.method public abstract getDeclinedMessage()Ljava/lang/String;
.end method

.method public getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->employeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getLogging()Lcom/squareup/protos/client/bills/AddTender$Logging;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method abstract getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
.end method

.method public getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getReceiptDestination()Ljava/lang/String;
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    return-object v0
.end method

.method public getReceiptDestinationType()Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    return-object v0
.end method

.method public getReceiptDetails()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDetails:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    return-object v0
.end method

.method public getReceiptNumber()Ljava/lang/String;
    .locals 1

    .line 434
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method

.method public getReceiptTenderShortName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    .line 492
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/BaseTender;->getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRemainingBalance()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->remainingBalance:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemainingBalanceTextForHud()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemainingBalanceTextForReceipt(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getServerId()Ljava/lang/String;
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getShortTenderMessage()Ljava/lang/String;
.end method

.method protected getTender()Lcom/squareup/protos/client/bills/Tender;
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    return-object v0
.end method

.method public getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object v0
.end method

.method public abstract getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
.end method

.method public abstract getTenderTypeGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTipPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public hasAutoGratuity()Z
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasCustomer()Z
    .locals 1

    .line 203
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public abstract isLocalTender()Z
.end method

.method public offlineCompatible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCustomerChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 232
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->customerChanged:Lrx/subjects/PublishSubject;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public requireTender()Lcom/squareup/protos/client/bills/Tender;
    .locals 2

    .line 313
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    const-string v1, "tender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender;

    return-object v0
.end method

.method public requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;
    .locals 2

    .line 327
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    .line 328
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getCompleteTenderDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    .line 329
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    return-object v0
.end method

.method public setAmount(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->amount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public setAmountWithoutTax(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->amountWithoutTax:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public setBuyerSelectedLanguage(Ljava/util/Locale;)V
    .locals 3

    .line 462
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    if-nez v0, :cond_0

    .line 464
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    .line 467
    :cond_0
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-nez v1, :cond_1

    .line 469
    new-instance v1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object v1

    .line 472
    :cond_1
    iget-object v2, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v2

    .line 473
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->newBuilder()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    move-result-object v1

    .line 475
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object v0

    .line 476
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseTender;->formatLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_selected_locale(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object p1

    .line 477
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object p1

    .line 474
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    move-result-object p1

    .line 478
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object p1

    .line 473
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object p1

    .line 479
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    return-void
.end method

.method public setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)V"
        }
    .end annotation

    .line 208
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 209
    invoke-static {p1, p3}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->getInstrumentFromContactUnlessNull(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Ljava/util/List;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/payment/tender/BaseTender;->customerInstruments:Ljava/util/List;

    if-eqz p2, :cond_0

    goto :goto_0

    .line 210
    :cond_0
    iget-object p2, p0, Lcom/squareup/payment/tender/BaseTender;->customerInstruments:Ljava/util/List;

    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->toBuyerInfo(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object p2

    :goto_0
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseTender;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 212
    iget-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->customerChanged:Lrx/subjects/PublishSubject;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lrx/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setTenderOnFailure(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/common/Money;)V
    .locals 0

    const/4 p1, 0x0

    .line 396
    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/payment/tender/BaseTender;->setTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public setTenderOnSuccess(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 392
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/payment/tender/BaseTender;->setTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public shouldAutoSendReceipt()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public smsReceipt(Ljava/lang/String;)V
    .locals 1

    .line 241
    sget-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->SMS:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    .line 242
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    return-void
.end method

.method public smsReceiptById(Ljava/lang/String;)V
    .locals 1

    .line 246
    sget-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->SMS_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestinationType:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    .line 247
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender;->receiptDestination:Ljava/lang/String;

    return-void
.end method

.method public startTender()V
    .locals 4

    .line 261
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-nez v0, :cond_3

    .line 265
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;-><init>()V

    .line 266
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    move-result-object v0

    .line 267
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    move-result-object v0

    .line 268
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getAutoGratuityAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    move-result-object v0

    .line 269
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getAllSurchargesAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    move-result-object v0

    .line 270
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Amounts;

    move-result-object v0

    .line 272
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    invoke-interface {v1}, Lcom/squareup/payment/tender/TenderProtoFactory;->startTender()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    .line 274
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v2

    .line 275
    invoke-virtual {v2}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 273
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 276
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 277
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getMethod()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 278
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseTender;->nowAsIsoDate()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 279
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts(Lcom/squareup/protos/client/bills/Tender$Amounts;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    .line 280
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseTender;->getLocation()Lcom/squareup/protos/client/GeoLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location(Lcom/squareup/protos/client/GeoLocation;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    .line 281
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getCashDrawerShiftId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 285
    iget-object v2, p0, Lcom/squareup/payment/tender/BaseTender;->employeeToken:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 286
    new-instance v1, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/tender/BaseTender;->employeeToken:Ljava/lang/String;

    .line 288
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    .line 289
    invoke-virtual {v2}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v2

    .line 287
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v1

    .line 290
    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v1

    .line 293
    :cond_0
    iget-object v2, p0, Lcom/squareup/payment/tender/BaseTender;->deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

    if-eqz v2, :cond_2

    .line 294
    iget-object v2, v2, Lcom/squareup/server/account/protos/DeviceCredential;->token:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const-string v3, "Device credential must include a token."

    invoke-static {v2, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    if-nez v1, :cond_1

    .line 296
    new-instance v1, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    goto :goto_0

    .line 297
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->newBuilder()Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v1

    .line 299
    :goto_0
    new-instance v2, Lcom/squareup/protos/client/DeviceCredential$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/DeviceCredential$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/tender/BaseTender;->deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

    iget-object v3, v3, Lcom/squareup/server/account/protos/DeviceCredential;->token:Ljava/lang/String;

    .line 301
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/DeviceCredential$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/DeviceCredential$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/BaseTender;->deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

    iget-object v3, v3, Lcom/squareup/server/account/protos/DeviceCredential;->name:Ljava/lang/String;

    .line 302
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/DeviceCredential$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/DeviceCredential$Builder;

    move-result-object v2

    .line 303
    invoke-virtual {v2}, Lcom/squareup/protos/client/DeviceCredential$Builder;->build()Lcom/squareup/protos/client/DeviceCredential;

    move-result-object v2

    .line 299
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->device_credential(Lcom/squareup/protos/client/DeviceCredential;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v1

    .line 304
    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v1

    .line 307
    :cond_2
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    .line 309
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    return-void

    .line 262
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "tender != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract supportsPaperSigAndTip()Z
.end method
