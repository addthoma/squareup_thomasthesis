.class Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;
.super Lcom/squareup/payment/RealDanglingAuth$Voidable;
.source "RealDanglingAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealDanglingAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CardPaymentVoidable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/RealDanglingAuth;


# direct methods
.method constructor <init>(Lcom/squareup/payment/RealDanglingAuth;Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    .line 216
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/RealDanglingAuth$Voidable;-><init>(Lcom/squareup/payment/RealDanglingAuth;Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)V

    return-void
.end method


# virtual methods
.method doVoidAuth(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 225
    new-instance v0, Lcom/squareup/queue/Cancel;

    invoke-virtual {p1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-direct {v0, p1, p2}, Lcom/squareup/queue/Cancel;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 226
    iget-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    invoke-static {p1}, Lcom/squareup/payment/RealDanglingAuth;->access$500(Lcom/squareup/payment/RealDanglingAuth;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 227
    iget-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    invoke-static {p1}, Lcom/squareup/payment/RealDanglingAuth;->access$600(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCancelEnqueued(Lcom/squareup/queue/Cancel;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardPaymentVoidable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;->billId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
