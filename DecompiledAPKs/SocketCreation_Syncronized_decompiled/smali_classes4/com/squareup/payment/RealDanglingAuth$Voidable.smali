.class abstract Lcom/squareup/payment/RealDanglingAuth$Voidable;
.super Ljava/lang/Object;
.source "RealDanglingAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealDanglingAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "Voidable"
.end annotation


# instance fields
.field final amountToReport:Lcom/squareup/protos/common/Money;

.field final authorizationInfo:Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

.field final billId:Lcom/squareup/protos/client/IdPair;

.field final synthetic this$0:Lcom/squareup/payment/RealDanglingAuth;


# direct methods
.method constructor <init>(Lcom/squareup/payment/RealDanglingAuth;Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)V
    .locals 5

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iget-wide v0, p2, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->amountToReportOnAutoVoid:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 192
    :cond_0
    invoke-static {p1}, Lcom/squareup/payment/RealDanglingAuth;->access$300(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->amountToReport:Lcom/squareup/protos/common/Money;

    .line 193
    iput-object p2, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->authorizationInfo:Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    .line 194
    invoke-virtual {p2}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->billId:Lcom/squareup/protos/client/IdPair;

    return-void
.end method


# virtual methods
.method abstract doVoidAuth(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
.end method

.method public getAmountToReport()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->amountToReport:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final voidAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    invoke-static {v0}, Lcom/squareup/payment/RealDanglingAuth;->access$400(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/payment/VoidMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->billId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/payment/VoidMonitor;->onDanglingAuthVoided(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 199
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->authorizationInfo:Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/payment/RealDanglingAuth$Voidable;->doVoidAuth(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method
