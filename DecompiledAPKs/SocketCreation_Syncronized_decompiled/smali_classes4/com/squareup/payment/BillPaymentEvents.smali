.class public Lcom/squareup/payment/BillPaymentEvents;
.super Ljava/lang/Object;
.source "BillPaymentEvents.java"


# instance fields
.field private final onBillCaptured:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field private final onBillPaymentCreated:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/payment/BillPayment;",
            ">;"
        }
    .end annotation
.end field

.field private final onTenderAddedFromCompleteBill:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/payment/AddedTenderEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onBillPaymentCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 18
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onTenderAddedFromCompleteBill:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onBillCaptured:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/protos/client/bills/AddedTender;)Ljava/lang/Boolean;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/client/bills/AddedTender;)Ljava/lang/Boolean;
    .locals 0

    .line 48
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getFirstAddTenderRequestCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$2(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/client/bills/AddedTender;)Lcom/squareup/payment/AddedTenderEvent;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/payment/AddedTenderEvent;

    .line 51
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getFirstAddTenderRequestCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object p0

    invoke-direct {v0, p1, v1, p0}, Lcom/squareup/payment/AddedTenderEvent;-><init>(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)V

    return-object v0
.end method

.method static synthetic lambda$onAddedTenderEvent$3(Lcom/squareup/payment/BillPayment;)Lrx/Observable;
    .locals 2

    .line 43
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->onAddTendersResponse()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$EsB4PU09E9Eah8WrwstQOwijRY8;->INSTANCE:Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$EsB4PU09E9Eah8WrwstQOwijRY8;

    .line 44
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$Zx9tobVxpVNF99gs1KSVfQd9UbA;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$Zx9tobVxpVNF99gs1KSVfQd9UbA;-><init>(Lcom/squareup/payment/BillPayment;)V

    .line 48
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$cnVZRyrV8Qxb3R9VtFr_1luxKgU;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$cnVZRyrV8Qxb3R9VtFr_1luxKgU;-><init>(Lcom/squareup/payment/BillPayment;)V

    .line 49
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public emitNewBillPayment(Lcom/squareup/payment/BillPayment;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onBillPaymentCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public notifyAddTenderResponseReceived(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)V
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onTenderAddedFromCompleteBill:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/payment/AddedTenderEvent;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/payment/AddedTenderEvent;-><init>(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public notifyBillCaptured(Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onBillCaptured:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onAddedTenderEvent()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/payment/AddedTenderEvent;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-virtual {p0}, Lcom/squareup/payment/BillPaymentEvents;->onBillPaymentCreated()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$ASECyPYPqowPlILvoJXablIh2wA;->INSTANCE:Lcom/squareup/payment/-$$Lambda$BillPaymentEvents$ASECyPYPqowPlILvoJXablIh2wA;

    .line 43
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/BillPaymentEvents;->onTenderAddedFromCompleteBill:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 56
    invoke-virtual {v0, v1}, Lrx/Observable;->mergeWith(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onAddedTenderEventFromCompleteBill()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/payment/AddedTenderEvent;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onTenderAddedFromCompleteBill:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onBillCompleted()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onBillCaptured:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onBillPaymentCreated()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/payment/BillPayment;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentEvents;->onBillPaymentCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method
