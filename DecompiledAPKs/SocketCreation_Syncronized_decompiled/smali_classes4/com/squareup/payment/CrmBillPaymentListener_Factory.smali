.class public final Lcom/squareup/payment/CrmBillPaymentListener_Factory;
.super Ljava/lang/Object;
.source "CrmBillPaymentListener_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/CrmBillPaymentListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final billPaymentEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyEventPublisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->billPaymentEventsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->loyaltyEventPublisherProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->settingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/CrmBillPaymentListener_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/payment/CrmBillPaymentListener_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/payment/CrmBillPaymentListener_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/payment/CrmBillPaymentListener_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/payment/CrmBillPaymentListener;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/payment/CrmBillPaymentListener;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/payment/CrmBillPaymentListener;-><init>(Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/CrmBillPaymentListener;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->billPaymentEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/BillPaymentEvents;

    iget-object v1, p0, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->loyaltyEventPublisherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/loyalty/LoyaltyEventPublisher;

    iget-object v2, p0, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v0, v1, v2}, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->newInstance(Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/payment/CrmBillPaymentListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/payment/CrmBillPaymentListener_Factory;->get()Lcom/squareup/payment/CrmBillPaymentListener;

    move-result-object v0

    return-object v0
.end method
