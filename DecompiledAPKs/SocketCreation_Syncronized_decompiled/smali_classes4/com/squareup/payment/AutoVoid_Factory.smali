.class public final Lcom/squareup/payment/AutoVoid_Factory;
.super Ljava/lang/Object;
.source "AutoVoid_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/AutoVoid;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoVoidNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/AutoVoidNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final danglingAuthProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;"
        }
    .end annotation
.end field

.field private final danglingMiryoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/AutoVoidNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/payment/AutoVoid_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/payment/AutoVoid_Factory;->danglingAuthProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/payment/AutoVoid_Factory;->autoVoidNotifierProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/payment/AutoVoid_Factory;->danglingMiryoProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/AutoVoid_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/AutoVoidNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;)",
            "Lcom/squareup/payment/AutoVoid_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/payment/AutoVoid_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/AutoVoid_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/notifications/AutoVoidNotifier;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)Lcom/squareup/payment/AutoVoid;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/payment/AutoVoid;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/AutoVoid;-><init>(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/notifications/AutoVoidNotifier;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/AutoVoid;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/payment/AutoVoid_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    iget-object v1, p0, Lcom/squareup/payment/AutoVoid_Factory;->danglingAuthProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    iget-object v2, p0, Lcom/squareup/payment/AutoVoid_Factory;->autoVoidNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/notifications/AutoVoidNotifier;

    iget-object v3, p0, Lcom/squareup/payment/AutoVoid_Factory;->danglingMiryoProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/payment/AutoVoid_Factory;->newInstance(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/notifications/AutoVoidNotifier;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)Lcom/squareup/payment/AutoVoid;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/payment/AutoVoid_Factory;->get()Lcom/squareup/payment/AutoVoid;

    move-result-object v0

    return-object v0
.end method
