.class Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;
.super Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
.source "RealOfflineModeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealOfflineModeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WaitingForPaymentToCompleteState"
.end annotation


# static fields
.field static final CHECK_INTERVAL_MILLIS:J = 0xfaL


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/RealOfflineModeMonitor;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V
    .locals 0

    .line 368
    iput-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V
    .locals 0

    .line 368
    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    return-void
.end method

.method private scheduleRecordServiceAvailable()V
    .locals 4

    .line 393
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$900(Lcom/squareup/payment/RealOfflineModeMonitor;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$WaitingForPaymentToCompleteState$oVrY4WTZqWAKlZ6RUbFmI0QXqrQ;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$WaitingForPaymentToCompleteState$oVrY4WTZqWAKlZ6RUbFmI0QXqrQ;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;)V

    const-wide/16 v2, 0xfa

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public init()V
    .locals 1

    .line 387
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$300(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    .line 388
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->scheduleRecordServiceAvailable()V

    return-void
.end method

.method public isUnavailable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$scheduleRecordServiceAvailable$0$RealOfflineModeMonitor$WaitingForPaymentToCompleteState()V
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$1000(Lcom/squareup/payment/RealOfflineModeMonitor;Z)V

    return-void
.end method

.method public recordServiceAvailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
    .locals 2

    .line 373
    iget-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$600(Lcom/squareup/payment/RealOfflineModeMonitor;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 374
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->scheduleRecordServiceAvailable()V

    return-object p0

    .line 377
    :cond_0
    new-instance p1, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-object p1
.end method
