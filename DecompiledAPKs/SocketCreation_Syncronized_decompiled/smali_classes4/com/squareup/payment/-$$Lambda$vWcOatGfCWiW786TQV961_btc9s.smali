.class public final synthetic Lcom/squareup/payment/-$$Lambda$vWcOatGfCWiW786TQV961_btc9s;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field private final synthetic f$0:Lcom/squareup/loyalty/LoyaltyEventPublisher;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/loyalty/LoyaltyEventPublisher;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/-$$Lambda$vWcOatGfCWiW786TQV961_btc9s;->f$0:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/-$$Lambda$vWcOatGfCWiW786TQV961_btc9s;->f$0:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    check-cast p1, Lcom/squareup/payment/AddedTenderEvent;

    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->maybeTriggerLoyaltyEvent(Lcom/squareup/payment/AddedTenderEvent;)V

    return-void
.end method
