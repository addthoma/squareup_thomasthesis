.class public final Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;
.super Ljava/lang/Object;
.source "TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final factoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;->factoryProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;->userIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 0

    .line 39
    invoke-static {p0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedIn;->provideTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;->factoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    iget-object v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;->userIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;->provideTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedIn_ProvideTransactionLedgerManagerFactory;->get()Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v0

    return-object v0
.end method
