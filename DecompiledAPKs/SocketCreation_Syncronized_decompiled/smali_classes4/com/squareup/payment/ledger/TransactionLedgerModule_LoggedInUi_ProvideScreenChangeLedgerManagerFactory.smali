.class public final Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;
.super Ljava/lang/Object;
.source "TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final mtlmProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;->mtlmProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;",
            ">;)",
            "Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideScreenChangeLedgerManager(Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;)Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedInUi;->provideScreenChangeLedgerManager(Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;)Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;->mtlmProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    invoke-static {v0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;->provideScreenChangeLedgerManager(Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;)Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedInUi_ProvideScreenChangeLedgerManagerFactory;->get()Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;

    move-result-object v0

    return-object v0
.end method
