.class Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;
.super Ljava/lang/Object;
.source "LoggedInTransactionLedgerManager.java"

# interfaces
.implements Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;
    }
.end annotation


# static fields
.field private static final BODY:Ljava/lang/String; = "See attachment"

.field private static final MAILTO_URI:Ljava/lang/String;

.field private static final RECIPIENT:Ljava/lang/String; = "help@help-messaging.squareup.com"

.field private static final SUBJECT:Ljava/lang/String; = "Support Ledger"

.field private static final TRANSACTION_LEDGER_TEMP_FILE:Ljava/lang/String; = "{token}.json"


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final context:Landroid/content/Context;

.field private final encryptor:Lcom/squareup/encryption/JweEncryptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/encryption/JweEncryptor<",
            "Lcom/squareup/payment/ledger/LedgerKey;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final fileExecutor:Ljava/util/concurrent/Executor;

.field private final gson:Lcom/google/gson/Gson;

.field private final helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

.field private final transactionLedgerUploader:Lcom/squareup/payment/ledger/TransactionLedgerUploader;

.field private final userId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mailto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "help@help-messaging.squareup.com"

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "?subject="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Support Ledger"

    .line 80
    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ({token})&body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "See attachment"

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->MAILTO_URI:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/Application;Lcom/google/gson/Gson;Ljava/io/File;Ljava/lang/String;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)V
    .locals 10

    move-object v1, p1

    move-object v0, p3

    move-object v3, p4

    move-object v4, p5

    .line 124
    invoke-static {p1, p5, p3, p4}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->createDbHelper(Landroid/content/Context;Lcom/squareup/util/Clock;Ljava/io/File;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    move-result-object v6

    invoke-static {}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->createEncryptor()Lcom/squareup/encryption/JweEncryptor;

    move-result-object v7

    move-object v0, p0

    move-object v2, p2

    move-object/from16 v5, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 123
    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;-><init>(Landroid/content/Context;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;Lcom/squareup/encryption/JweEncryptor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;Lcom/squareup/encryption/JweEncryptor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/gson/Gson;",
            "Ljava/lang/String;",
            "Lcom/squareup/util/Clock;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;",
            "Lcom/squareup/encryption/JweEncryptor<",
            "Lcom/squareup/payment/ledger/LedgerKey;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/payment/ledger/TransactionLedgerUploader;",
            ")V"
        }
    .end annotation

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->context:Landroid/content/Context;

    .line 133
    iput-object p2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->gson:Lcom/google/gson/Gson;

    .line 134
    iput-object p3, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    .line 135
    iput-object p4, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->clock:Lcom/squareup/util/Clock;

    .line 136
    iput-object p5, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->fileExecutor:Ljava/util/concurrent/Executor;

    .line 137
    iput-object p6, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    .line 138
    iput-object p7, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->encryptor:Lcom/squareup/encryption/JweEncryptor;

    .line 139
    iput-object p8, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->features:Lcom/squareup/settings/server/Features;

    .line 140
    iput-object p9, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->transactionLedgerUploader:Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    return-void
.end method

.method private addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$YimNG5qrJ80mPZQ6PFYhg08Yzq8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$YimNG5qrJ80mPZQ6PFYhg08Yzq8;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;Lcom/squareup/payment/ledger/LedgerEntry;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private createEmailIntent(Ljava/io/File;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .line 191
    sget-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->MAILTO_URI:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "token"

    .line 192
    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 193
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 195
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    .line 196
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-direct {v0, v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 p2, 0x10000000

    .line 197
    invoke-virtual {v0, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 198
    iget-object p2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->context:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/squareup/util/Files;->getUriForFile(Ljava/io/File;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object p1

    const-string p2, "android.intent.extra.STREAM"

    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private static createEncryptor()Lcom/squareup/encryption/JweEncryptor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/encryption/JweEncryptor<",
            "Lcom/squareup/payment/ledger/LedgerKey;",
            ">;"
        }
    .end annotation

    .line 531
    :try_start_0
    new-instance v0, Lcom/squareup/encryption/JweEncryptor;

    sget-object v1, Lcom/squareup/payment/ledger/LedgerKey;->INSTANCE:Lcom/squareup/payment/ledger/LedgerKey;

    sget-object v2, Lcom/squareup/payment/ledger/LedgerKey;->ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;

    invoke-direct {v0, v1, v2}, Lcom/squareup/encryption/JweEncryptor;-><init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Bad transaction ledger public key certificate"

    .line 533
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private encrypt(Ljava/lang/String;)[B
    .locals 1

    .line 293
    sget-object v0, Lcom/squareup/util/Strings;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->encrypt([B)[B

    move-result-object p1

    return-object p1
.end method

.method private encrypt([B)[B
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->encryptor:Lcom/squareup/encryption/JweEncryptor;

    if-eqz v0, :cond_0

    .line 284
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/squareup/encryption/JweEncryptor;->compute([B)Lcom/squareup/encryption/CryptoResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/encryption/CryptoResult;->getValue()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string v0, "Invalid transaction ledger key"

    .line 286
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private getTotalAmount(Lcom/squareup/protos/client/bills/AddTendersRequest;)J
    .locals 4

    .line 418
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const-wide/16 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/AddTender;

    .line 419
    iget-object v2, v2, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method static synthetic lambda$null$0(Lio/reactivex/SingleEmitter;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 235
    iget-boolean v0, p1, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    if-nez v0, :cond_0

    .line 236
    invoke-interface {p0, p1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$null$1(Lio/reactivex/SingleEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 244
    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    const/4 v1, 0x1

    const-string v2, "Success"

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;-><init>(ZLjava/lang/String;)V

    invoke-interface {p0, v0}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$streamingLedgerUpload$2(Ljava/util/List;Lio/reactivex/SingleEmitter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 227
    new-instance v0, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    .line 228
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/payment/ledger/-$$Lambda$bRY59Ww6PD56uNK_OpuIFbus8mg;

    invoke-direct {v1, v0}, Lcom/squareup/payment/ledger/-$$Lambda$bRY59Ww6PD56uNK_OpuIFbus8mg;-><init>(Lio/reactivex/disposables/SerialDisposable;)V

    invoke-interface {p1, v1}, Lio/reactivex/SingleEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 230
    invoke-static {p0}, Lio/reactivex/Single;->concat(Ljava/lang/Iterable;)Lio/reactivex/Flowable;

    move-result-object p0

    new-instance v1, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$Hkz-5LrBL3eYtHzFQfueY_Q2ftg;

    invoke-direct {v1, p1}, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$Hkz-5LrBL3eYtHzFQfueY_Q2ftg;-><init>(Lio/reactivex/SingleEmitter;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/payment/ledger/-$$Lambda$_ij82XSKaA6JgN9-Ae2-y4jExAg;

    invoke-direct {v2, p1}, Lcom/squareup/payment/ledger/-$$Lambda$_ij82XSKaA6JgN9-Ae2-y4jExAg;-><init>(Lio/reactivex/SingleEmitter;)V

    new-instance v3, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$Lb77MLoivHCMDl74_9PFf2vueNI;

    invoke-direct {v3, p1}, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$Lb77MLoivHCMDl74_9PFf2vueNI;-><init>(Lio/reactivex/SingleEmitter;)V

    .line 231
    invoke-virtual {p0, v1, v2, v3}, Lio/reactivex/Flowable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    .line 230
    invoke-virtual {v0, p0}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private logAddTender(Lcom/squareup/protos/client/bills/AddTender;)V
    .locals 3

    .line 425
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDER:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 426
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 427
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 428
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 429
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Ljava/lang/Long;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 430
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 432
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 434
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne v1, v2, :cond_0

    .line 437
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    .line 438
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    .line 441
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    if-eqz v1, :cond_1

    .line 442
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Method;->other_tender:Lcom/squareup/protos/client/bills/OtherTender;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setOtherTenderName(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    .line 445
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method private randomId()Lcom/squareup/protos/client/IdPair;
    .locals 2

    .line 539
    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method private streamingLedgerUpload(JJ)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    .line 218
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->transactionLedgerEntriesIterators(JJ)Ljava/util/List;

    move-result-object p1

    .line 220
    new-instance p2, Ljava/util/ArrayList;

    .line 221
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 222
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;

    .line 223
    iget-object p4, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->transactionLedgerUploader:Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    invoke-virtual {p4, p3}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->uploadLedger(Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;)Lio/reactivex/Single;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 226
    :cond_0
    new-instance p1, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$36mr-uVh61wKDMKvq0_X3y5Q1Zo;

    invoke-direct {p1, p2}, Lcom/squareup/payment/ledger/-$$Lambda$LoggedInTransactionLedgerManager$36mr-uVh61wKDMKvq0_X3y5Q1Zo;-><init>(Ljava/util/List;)V

    invoke-static {p1}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private writeStringToFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "com.squareup.register"

    .line 172
    invoke-virtual {p1, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    const-string/jumbo v0, "{token}.json"

    .line 173
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 174
    invoke-static {p3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "token"

    invoke-virtual {v0, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p3

    .line 176
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 177
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 179
    :try_start_0
    sget-object p3, Lcom/squareup/util/Strings;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p2, p3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    invoke-static {p1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    const/4 p1, 0x1

    .line 184
    invoke-virtual {v0, p1}, Ljava/io/File;->setReadable(Z)Z

    move-result p2

    if-nez p2, :cond_0

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    .line 185
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, p2

    const-string p2, "Call to setReadable failed on %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v0

    :catchall_0
    move-exception p2

    .line 181
    invoke-static {p1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 182
    throw p2
.end method


# virtual methods
.method public clearLedger()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->fileExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/payment/ledger/-$$Lambda$_LtXcY9sFA43Zh0bt3ePm9mUnws;

    invoke-direct {v2, v1}, Lcom/squareup/payment/ledger/-$$Lambda$_LtXcY9sFA43Zh0bt3ePm9mUnws;-><init>(Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public close()V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-virtual {v0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->close()V

    return-void
.end method

.method public emailLedger(Ljava/lang/String;)V
    .locals 3

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->gson:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-virtual {v2}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getAllEntries()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->writeStringToFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 161
    invoke-direct {p0, v0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->createEmailIntent(Ljava/io/File;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 162
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->context:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 166
    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public expireOldPayments()V
    .locals 3

    .line 144
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->fileExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/payment/ledger/-$$Lambda$wMS2PszzVrkXqh-3ls173my3D30;

    invoke-direct {v2, v1}, Lcom/squareup/payment/ledger/-$$Lambda$wMS2PszzVrkXqh-3ls173my3D30;-><init>(Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$addEntry$3$LoggedInTransactionLedgerManager(Lcom/squareup/payment/ledger/LedgerEntry;)V
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v0, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 274
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->encrypt(Ljava/lang/String;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    .line 276
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->addEntry([B)V

    :cond_0
    return-void
.end method

.method public logAddTenderBeforeAuth(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 6

    .line 386
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    .line 387
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v3, v2

    goto :goto_0

    .line 388
    :cond_0
    iget-object v3, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-static {v3}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/Card$Brand;->name()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    .line 389
    :cond_1
    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v2, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    .line 391
    :goto_1
    new-instance v1, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v4, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDER_BEFORE_AUTH:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v5, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v4, p1, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    .line 392
    invoke-virtual {v1, v4}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v1

    .line 393
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getServerId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v1

    iget-object v4, v0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, v4, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 394
    invoke-virtual {v1, v4}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v1

    .line 395
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Ljava/lang/Long;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 396
    invoke-virtual {p1, v3}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 397
    invoke-virtual {p1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 398
    invoke-virtual {p1, v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 399
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 391
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V
    .locals 3

    .line 403
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 404
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 405
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    const/4 v2, 0x0

    .line 406
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/AddTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 407
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->getTotalAmount(Lcom/squareup/protos/client/bills/AddTendersRequest;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Ljava/lang/Long;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 408
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object v0

    .line 403
    invoke-direct {p0, v0}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    .line 411
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddTender;

    .line 412
    invoke-direct {p0, v0}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->logAddTender(Lcom/squareup/protos/client/bills/AddTender;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public logAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 3

    .line 449
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 450
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 451
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz p1, :cond_0

    .line 453
    iget-object v1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    .line 454
    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    .line 456
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCancelBillEnqueued(Lcom/squareup/protos/client/bills/CancelBillRequest;)V
    .locals 3

    .line 490
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 491
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 492
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 490
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCancelBillResponse(Lcom/squareup/protos/client/bills/CancelBillResponse;)V
    .locals 3

    .line 496
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 497
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 498
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 496
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCancelEnqueued(Lcom/squareup/queue/Cancel;)V
    .locals 3

    .line 381
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 382
    invoke-virtual {p1}, Lcom/squareup/queue/Cancel;->getUniqueKey()Ljava/lang/String;

    move-result-object p1

    .line 381
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 382
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 381
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCaptureEnqueued(Lcom/squareup/queue/Capture;)V
    .locals 3

    .line 350
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 351
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getAuthorizationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 352
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 353
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 354
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getCardBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 355
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 356
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 350
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCaptureFailed(Lcom/squareup/queue/Capture;Ljava/lang/String;)V
    .locals 3

    .line 370
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 371
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getAuthorizationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 372
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 373
    invoke-virtual {v0, p2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setFailureReason(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p2

    .line 374
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p2

    .line 375
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getCardBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card$Brand;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p2

    .line 376
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 377
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 370
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCaptureProcessed(Lcom/squareup/queue/Capture;)V
    .locals 3

    .line 360
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 361
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getAuthorizationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 362
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 363
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 364
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getCardBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 365
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 366
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 360
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCaptureTenderRequest(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)V
    .locals 3

    .line 482
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_TENDER_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 483
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 484
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 485
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 486
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 482
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCaptureTenderResponse(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)V
    .locals 3

    .line 475
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_TENDER_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 476
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 477
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 475
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCompleteBillEnqueued(Lcom/squareup/protos/client/bills/CompleteBillRequest;)V
    .locals 3

    .line 460
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->COMPLETE_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 461
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 462
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setServerUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 463
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 464
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 460
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logCompleteBillResponse(Lcom/squareup/protos/client/bills/CompleteBillResponse;)V
    .locals 3

    .line 468
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->COMPLETE_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 469
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 470
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 468
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logIssueRefundsRequest(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)V
    .locals 3

    .line 502
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ISSUE_REFUNDS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v1, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->request_uuid:Ljava/lang/String;

    .line 503
    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 504
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 505
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 502
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logIssueRefundsResponse(Lcom/squareup/protos/client/bills/IssueRefundsResponse;)V
    .locals 3

    .line 509
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ISSUE_REFUNDS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 510
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 511
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 509
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logRemoveTendersRequest(Lcom/squareup/protos/client/bills/RemoveTendersRequest;)V
    .locals 3

    .line 515
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->REMOVE_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 516
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 517
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 515
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logRemoveTendersResponse(Lcom/squareup/protos/client/bills/RemoveTendersResponse;)V
    .locals 3

    .line 522
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->REMOVE_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 523
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setDetails(Lcom/squareup/wire/Message;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 524
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 522
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logShowScreen(Ljava/lang/String;)V
    .locals 3

    .line 265
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->SCREEN_TRANSITION:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->clock:Lcom/squareup/util/Clock;

    .line 266
    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 267
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setMessage(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 265
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logStoreAndForwardBillFailed(Lcom/squareup/payment/offline/BillInFlight;Ljava/lang/String;)V
    .locals 3

    .line 317
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 318
    invoke-virtual {p1}, Lcom/squareup/payment/offline/BillInFlight;->getBillClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 319
    invoke-virtual {p1}, Lcom/squareup/payment/offline/BillInFlight;->getTimestamp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 320
    invoke-virtual {v0, p2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setFailureReason(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p2

    .line 321
    invoke-virtual {p1}, Lcom/squareup/payment/offline/BillInFlight;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 322
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 317
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logStoreAndForwardBillReady(Lcom/squareup/payment/offline/BillInFlight;Z)V
    .locals 2

    .line 327
    new-instance p2, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_READY:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {p2, v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 328
    invoke-virtual {p1}, Lcom/squareup/payment/offline/BillInFlight;->getBillClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p2

    .line 329
    invoke-virtual {p1}, Lcom/squareup/payment/offline/BillInFlight;->getTimestamp()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p2

    .line 330
    invoke-virtual {p1}, Lcom/squareup/payment/offline/BillInFlight;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 331
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 327
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logStoreAndForwardPaymentEnqueued(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 3

    .line 297
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 298
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 299
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 300
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTotalMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 301
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getCardBrand()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 302
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getCardLastFour()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 303
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 297
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logStoreAndForwardPaymentProcessed(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 3

    .line 307
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 308
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 309
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 310
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTotalMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setPaymentAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 311
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getCardBrand()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardBrand(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 312
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getCardLastFour()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setCardPanSuffix(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 313
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 307
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logStoreAndForwardTaskStatus(Ljava/lang/String;)V
    .locals 3

    .line 335
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_TASK_STATUS:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->clock:Lcom/squareup/util/Clock;

    .line 336
    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object v0

    .line 337
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setMessage(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 338
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 335
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public logVoidDanglingAuthorization(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 342
    new-instance v0, Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->AUTO_VOID:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    iget-object v2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->userId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;-><init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;)V

    .line 343
    invoke-virtual {v0, p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setClientUniqueKey(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->clock:Lcom/squareup/util/Clock;

    .line 344
    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setTimestamp(J)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 345
    invoke-virtual {p1, p2}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->setFailureReason(Ljava/lang/String;)Lcom/squareup/payment/ledger/LedgerEntry$Builder;

    move-result-object p1

    .line 346
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerEntry$Builder;->build()Lcom/squareup/payment/ledger/LedgerEntry;

    move-result-object p1

    .line 342
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->addEntry(Lcom/squareup/payment/ledger/LedgerEntry;)V

    return-void
.end method

.method public uploadLedger()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .line 206
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER_STREAM:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    const-wide v2, 0x7fffffffffffffffL

    .line 207
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->streamingLedgerUpload(JJ)Lio/reactivex/Single;

    move-result-object v0

    return-object v0

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->transactionLedgerUploader:Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-virtual {v1}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getAllTransactionLedgerEntries()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->uploadLedger(Ljava/util/List;Lokio/ByteString;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public uploadLedger(JJ)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .line 213
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->streamingLedgerUpload(JJ)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public uploadLedgerWithDiagnosticsData(Ljava/lang/String;Ljava/io/File;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .line 255
    :try_start_0
    invoke-static {p2}, Lcom/squareup/util/Streams;->read(Ljava/io/File;)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->encrypt([B)[B

    move-result-object p1

    invoke-static {p1}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    iget-object p2, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->transactionLedgerUploader:Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    iget-object v0, p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;->helper:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-virtual {v0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getOneEntry()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;->uploadLedger(Ljava/util/List;Lokio/ByteString;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 257
    invoke-static {p1}, Lcom/squareup/util/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1
.end method
