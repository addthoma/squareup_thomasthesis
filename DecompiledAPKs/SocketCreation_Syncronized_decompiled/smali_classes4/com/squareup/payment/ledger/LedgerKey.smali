.class Lcom/squareup/payment/ledger/LedgerKey;
.super Ljava/lang/Object;
.source "LedgerKey.java"


# static fields
.field public static final ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/encryption/CryptoKeyAdapter<",
            "Lcom/squareup/payment/ledger/LedgerKey;",
            ">;"
        }
    .end annotation
.end field

.field private static final CERTIFICATE:[B

.field private static final EXPIRATION_TIME_MILLIS:J = 0x23b15d29bd0L

.field public static final INSTANCE:Lcom/squareup/payment/ledger/LedgerKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "MIIDxDCCAqygAwIBAgIEQxkK3jANBgkqhkiG9w0BAQsFADCBizEeMBwGA1UEAxMVUmVnaXN0ZXIgTG9jYWwgTGVkZ2VyMRgwFgYDVQQLEw9TcXVhcmUgUmVnaXN0ZXIxFTATBgNVBAoTDFNxdWFyZSwgSW5jLjEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzETMBEGA1UECBMKQ2FsaWZvcm5pYTELMAkGA1UEBhMCVVMwHhcNMTYwMzI4MTkxNDEwWhcNNDcwOTIyMTkxNDEwWjCBizEeMBwGA1UEAxMVUmVnaXN0ZXIgTG9jYWwgTGVkZ2VyMRgwFgYDVQQLEw9TcXVhcmUgUmVnaXN0ZXIxFTATBgNVBAoTDFNxdWFyZSwgSW5jLjEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzETMBEGA1UECBMKQ2FsaWZvcm5pYTELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC//TJVzfbtIv9catbAr8HQ9OEnfUjn/crMaSqvsVjBQZTrCAtSkFGPlpAXXEQ+5JupDjPO7v2sGUC/xts1SeTTw+z4fPkt5J4KxHLsdsJnWtD2AKPNOYi7wNmnP8gI4EryPkZfelG2WRkTDAceP9QoANS2cp385iXpCCn6LBovsS0WHp2GaQrocKrLlreN8o81mq57B8RAuyBfRYAXt4i3twDI4bqIKT+KVFBZNvOG2vCR+BEZUD7NXoFPHj2MVcIXyF8UB+0CPbyg42gngg7fSefSqlnyC4ZQnQlsxt6Q5AtMUWdMaRX87sEv0qpr6XVNYfgS5bMmhpwkoCOltzT5AgMBAAGjLjAsMAsGA1UdDwQEAwID+DAdBgNVHQ4EFgQU7gn3G3vj/o+1oOwPDuzoArsLUF4wDQYJKoZIhvcNAQELBQADggEBAE6oYfPss1Om8ovBTDSh5+MNOg0OKUMhjwePe9+NDfqTtlUx+ONikFMJTO11U3ZDJsVfNFhPE/X0JqfcBOYZRJV7wM+DhIgLBe1EKpadkoDQpp0/x8hErL7yyFo8vuTuVnFljmi4TT8fjAo1FK2sU3JP/FryLQexPCITbD+YyfNRdX/xJYaBgZXeNQJIUiNd4nNoRPpKMmPeXLNZFz/Dtv73rOC1xzh1n1Q3NvjC9zVGpZh+u9+C7n26mmxY7SYmb6kt0slrjeHAk8c6dwNnXf7GYpcxBlg3k/ZcSH96xyeDW4xu8NDWLZw5oTLEVXRnrCEnFZeQwxJ7EwLb1MgiRFw="

    const/4 v1, 0x0

    .line 15
    invoke-static {v0, v1}, Lcom/squareup/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/ledger/LedgerKey;->CERTIFICATE:[B

    .line 38
    new-instance v0, Lcom/squareup/payment/ledger/LedgerKey;

    invoke-direct {v0}, Lcom/squareup/payment/ledger/LedgerKey;-><init>()V

    sput-object v0, Lcom/squareup/payment/ledger/LedgerKey;->INSTANCE:Lcom/squareup/payment/ledger/LedgerKey;

    .line 43
    new-instance v0, Lcom/squareup/payment/ledger/LedgerKey$1;

    invoke-direct {v0}, Lcom/squareup/payment/ledger/LedgerKey$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/ledger/LedgerKey;->ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()[B
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/payment/ledger/LedgerKey;->CERTIFICATE:[B

    return-object v0
.end method


# virtual methods
.method public isExpired()Z
    .locals 5

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v2, 0x23b15d29bd0L

    cmp-long v4, v2, v0

    if-gtz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
