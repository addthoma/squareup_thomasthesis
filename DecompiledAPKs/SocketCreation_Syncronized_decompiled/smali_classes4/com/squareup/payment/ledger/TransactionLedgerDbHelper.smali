.class public Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "TransactionLedgerDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;,
        Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;
    }
.end annotation


# static fields
.field private static final DATA:Ljava/lang/String; = "data"

.field private static final DATABASE_DIR_NAME:Ljava/lang/String; = "transaction-ledger"

.field private static final DATABASE_NAME:Ljava/lang/String; = "ledger"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final EXPIRATION_TIME_MILLIS:J = 0x288e14800L

.field private static final EXPIRE_INTERVAL_MILLIS:J = 0x36ee80L

.field private static final ID:Ljava/lang/String; = "id"

.field private static final INDEX:Ljava/lang/String; = "timestamp_index"

.field private static final LEDGER_TABLE_NAME:Ljava/lang/String; = "ledger"

.field private static final NUMBER_OF_ELEMENTS_FOR_LEDGER_UPLOAD:I = 0x3a98

.field private static final SELECT_ALL_DATA_QUERY:Ljava/lang/String;

.field private static SELECT_ALL_IN_DATE_RANGE_QUERY:Lcom/squareup/phrase/Phrase; = null

.field private static final SELECT_ALL_TIMESTAMP_DATA_QUERY:Ljava/lang/String;

.field private static final SELECT_ONE_QUERY:Ljava/lang/String;

.field private static final SELECT_RANGE_TIMESTAMP_DATA_QUERY:Lcom/squareup/phrase/Phrase;

.field private static final TIMESTAMP:Ljava/lang/String; = "timestamp"


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private nextExpireTimeMillis:J


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const-string v0, "SELECT {data} FROM {table} ORDER BY {time_stamp} ASC"

    .line 57
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "data"

    .line 62
    invoke-virtual {v1, v2, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v3, "ledger"

    const-string v4, "table"

    .line 63
    invoke-virtual {v1, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v5, "time_stamp"

    const-string v6, "timestamp"

    .line 64
    invoke-virtual {v1, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ALL_DATA_QUERY:Ljava/lang/String;

    const-string v1, "SELECT {data} FROM {table} WHERE {time_stamp} >= {start_time} AND {time_stamp} <= {end_time} ORDER BY {time_stamp} ASC"

    .line 67
    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const/4 v7, 0x2

    new-array v8, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    const/4 v10, 0x1

    aput-object v2, v8, v10

    const-string v11, "%s,%s"

    .line 74
    invoke-static {v11, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v2, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 75
    invoke-virtual {v1, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 76
    invoke-virtual {v1, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    sput-object v1, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ALL_IN_DATE_RANGE_QUERY:Lcom/squareup/phrase/Phrase;

    .line 78
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v6, v1, v9

    aput-object v2, v1, v10

    .line 83
    invoke-static {v11, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 84
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 85
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ALL_TIMESTAMP_DATA_QUERY:Ljava/lang/String;

    const-string v0, "SELECT {data} FROM {table} WHERE {time_stamp} >= {start_time} AND {time_stamp} <= {end_time} ORDER BY {time_stamp} ASC LIMIT {count} OFFSET {offset}"

    .line 88
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v6, v1, v9

    aput-object v2, v1, v10

    .line 96
    invoke-static {v11, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "count"

    const/16 v8, 0x3a98

    .line 99
    invoke-virtual {v0, v1, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_RANGE_TIMESTAMP_DATA_QUERY:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {data} FROM {table} ORDER BY {time_stamp} ASC LIMIT {limit}"

    .line 101
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v6, v1, v9

    aput-object v2, v1, v10

    .line 107
    invoke-static {v11, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 109
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "limit"

    .line 110
    invoke-virtual {v0, v1, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ONE_QUERY:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/squareup/util/Clock;Ljava/io/File;)V
    .locals 3

    .line 123
    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 124
    iput-object p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->clock:Lcom/squareup/util/Clock;

    new-array p1, v1, [Ljava/lang/Object;

    .line 125
    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    aput-object p2, p1, p3

    const-string p2, "TransactionLedgerManager: database in %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100()Lcom/squareup/phrase/Phrase;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_RANGE_TIMESTAMP_DATA_QUERY:Lcom/squareup/phrase/Phrase;

    return-object v0
.end method

.method public static createDbHelper(Landroid/content/Context;Lcom/squareup/util/Clock;Ljava/io/File;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;
    .locals 1

    .line 115
    invoke-static {p2, p3}, Lcom/squareup/user/Users;->getUserDirectory(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object p2

    .line 116
    new-instance p3, Ljava/io/File;

    const-string v0, "transaction-ledger"

    invoke-direct {p3, p2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p3}, Ljava/io/File;->mkdir()Z

    .line 118
    new-instance p2, Ljava/io/File;

    const-string v0, "ledger.db"

    invoke-direct {p2, p3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 119
    new-instance p3, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;

    invoke-direct {p3, p0, p1, p2}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;-><init>(Landroid/content/Context;Lcom/squareup/util/Clock;Ljava/io/File;)V

    return-object p3
.end method

.method private getNumberOfElementsForLedgerUpload(JJ)I
    .locals 2

    .line 287
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 289
    sget-object v1, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ALL_IN_DATE_RANGE_QUERY:Lcom/squareup/phrase/Phrase;

    .line 290
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "start_time"

    invoke-virtual {v1, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 291
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "end_time"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 292
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 289
    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 294
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result p2

    .line 296
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 297
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return p2
.end method

.method private iterateAllTransactionLedgerEntries(Lrx/functions/Action1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Action1<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;)V"
        }
    .end annotation

    .line 242
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 243
    sget-object v1, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ALL_TIMESTAMP_DATA_QUERY:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 245
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 247
    :cond_0
    new-instance v2, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;-><init>()V

    new-instance v3, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    new-instance v4, Ljava/util/Date;

    const/4 v5, 0x0

    .line 249
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-static {v4}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object v3

    .line 250
    invoke-virtual {v3}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v3

    .line 248
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 251
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object v2

    .line 252
    invoke-virtual {v2}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    move-result-object v2

    .line 247
    invoke-interface {p1, v2}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    .line 253
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 256
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 257
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-void

    :catchall_0
    move-exception p1

    .line 256
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 257
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 258
    throw p1
.end method

.method private iterateSomeTransactionLedgerEntries(JJLrx/functions/Action1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lrx/functions/Action1<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;)V"
        }
    .end annotation

    .line 263
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 265
    sget-object v1, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ALL_IN_DATE_RANGE_QUERY:Lcom/squareup/phrase/Phrase;

    .line 266
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "start_time"

    invoke-virtual {v1, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 267
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "end_time"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    .line 265
    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 270
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 272
    :cond_0
    new-instance p2, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;-><init>()V

    new-instance p3, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    new-instance p4, Ljava/util/Date;

    const/4 v1, 0x0

    .line 274
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-direct {p4, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-static {p4}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object p3

    .line 275
    invoke-virtual {p3}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p3

    .line 273
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object p2

    const/4 p3, 0x1

    .line 276
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object p3

    invoke-static {p3}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object p2

    .line 277
    invoke-virtual {p2}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    move-result-object p2

    .line 272
    invoke-interface {p5, p2}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    .line 278
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_0

    .line 281
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 282
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-void

    :catchall_0
    move-exception p2

    .line 281
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 282
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 283
    throw p2
.end method


# virtual methods
.method addEntry([B)V
    .locals 3

    .line 157
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "timestamp"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "data"

    .line 159
    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 161
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    const-string v1, "ledger"

    const/4 v2, 0x0

    .line 162
    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method clearLedger()V
    .locals 2

    .line 141
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DELETE FROM ledger"

    .line 142
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method expireOldPayments()V
    .locals 7

    .line 147
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 148
    iget-wide v2, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->nextExpireTimeMillis:J

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 150
    iget-object v3, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v3}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v3

    const-wide v5, 0x288e14800L

    sub-long/2addr v3, v5

    .line 151
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DELETE FROM ledger WHERE timestamp < "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    .line 152
    iput-wide v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->nextExpireTimeMillis:J

    :cond_0
    return-void
.end method

.method getAllEntries()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 168
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 169
    sget-object v2, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ALL_DATA_QUERY:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 171
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x0

    .line 173
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 177
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 178
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    .line 177
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 178
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 179
    throw v0
.end method

.method getAllTransactionLedgerEntries()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;"
        }
    .end annotation

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 213
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/payment/ledger/-$$Lambda$yHZVcgKfcXDxYcSF0VU61pLHlWc;

    invoke-direct {v1, v0}, Lcom/squareup/payment/ledger/-$$Lambda$yHZVcgKfcXDxYcSF0VU61pLHlWc;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->iterateAllTransactionLedgerEntries(Lrx/functions/Action1;)V

    return-object v0
.end method

.method getOneEntry()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;"
        }
    .end annotation

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 186
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 187
    sget-object v2, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->SELECT_ONE_QUERY:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 189
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    new-instance v3, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;-><init>()V

    new-instance v4, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    new-instance v5, Ljava/util/Date;

    const/4 v6, 0x0

    .line 192
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-static {v5}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object v4

    .line 193
    invoke-virtual {v4}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v4

    .line 191
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 194
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-static {v4}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object v3

    .line 195
    invoke-virtual {v3}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    move-result-object v3

    .line 190
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 199
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    .line 198
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 199
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 200
    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE ledger(id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp INTEGER, data BLOB)"

    .line 129
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX timestamp_index ON ledger (timestamp)"

    .line 133
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .line 137
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method transactionLedgerEntriesIterators(JJ)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;",
            ">;"
        }
    .end annotation

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 228
    invoke-direct/range {p0 .. p4}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->getNumberOfElementsForLedgerUpload(JJ)I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 230
    new-instance v11, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;

    const/4 v10, 0x0

    move-object v3, v11

    move-object v4, p0

    move v5, v2

    move-wide v6, p1

    move-wide v8, p3

    invoke-direct/range {v3 .. v10}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;IJJLcom/squareup/payment/ledger/TransactionLedgerDbHelper$1;)V

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit16 v2, v2, 0x3a98

    goto :goto_0

    :cond_0
    return-object v0
.end method
