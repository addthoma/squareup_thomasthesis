.class public Lcom/squareup/payment/AutoCaptureStatusBarNotifier;
.super Ljava/lang/Object;
.source "AutoCaptureStatusBarNotifier.java"

# interfaces
.implements Lcom/squareup/notifications/AutoCaptureNotifier;


# instance fields
.field private final context:Landroid/content/Context;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/text/Formatter;Landroid/app/NotificationManager;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Landroid/app/NotificationManager;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/notification/NotificationWrapper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->context:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 36
    iput-object p3, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    .line 37
    iput-object p4, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->res:Lcom/squareup/util/Res;

    .line 38
    iput-object p5, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    return-void
.end method


# virtual methods
.method public show(Lcom/squareup/protos/common/Money;)V
    .locals 3

    .line 42
    iget-object v0, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/payment/notifiers/impl/R$string;->auto_capture_body:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 43
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 46
    iget-object v0, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v1, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->context:Landroid/content/Context;

    sget-object v2, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x2

    .line 47
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/payment/notifiers/impl/R$string;->auto_capture_title:I

    .line 48
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/payment/notifiers/impl/R$string;->auto_capture_title:I

    .line 49
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    new-instance v1, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 51
    invoke-virtual {v1, p1}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->context:Landroid/content/Context;

    const-string v1, "SALES_HISTORY"

    .line 53
    invoke-static {v0, v1}, Lcom/squareup/ui/main/PosIntentParser;->createPendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 52
    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    .line 56
    iget-object v0, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/payment/notifiers/impl/R$id;->notification_auto_capture:I

    invoke-virtual {v0, v1, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
