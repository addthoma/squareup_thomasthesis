.class public final Lcom/squareup/payment/Transaction_Factory;
.super Ljava/lang/Object;
.source "Transaction_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/Transaction;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final availableDiscountsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final billPaymentFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPayment$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final diningOptionCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicePaymentFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/InvoicePayment$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final nextDiscountsKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final nextTaxRulesKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final nextTaxesKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final orderKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/payment/Order;",
            ">;>;"
        }
    .end annotation
.end field

.field private final paymentFlowTaskProviderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final retrofitQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final tipSettingsKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/settings/server/TipSettings;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/payment/Order;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPayment$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/InvoicePayment$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/settings/server/TipSettings;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 112
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->orderKeyProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 113
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->nextTaxesKeyProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 114
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->nextTaxRulesKeyProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 115
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->nextDiscountsKeyProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 116
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 117
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->currencyProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 118
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->userIdProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 119
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 120
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->billPaymentFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 121
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->invoicePaymentFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 122
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 123
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->ticketsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 124
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 125
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 126
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 127
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->diningOptionCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 128
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 129
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 130
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 131
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 132
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 133
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->tipSettingsKeyProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 134
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 135
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->availableDiscountsStoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 136
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 137
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->paymentFlowTaskProviderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 138
    iput-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/Transaction_Factory;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/payment/Order;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPayment$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/InvoicePayment$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/settings/server/TipSettings;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/payment/Transaction_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    .line 167
    new-instance v28, Lcom/squareup/payment/Transaction_Factory;

    move-object/from16 v0, v28

    invoke-direct/range {v0 .. v27}, Lcom/squareup/payment/Transaction_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v28
.end method

.method public static newInstance(Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/InvoicePayment$Factory;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/tickets/Tickets;Lcom/squareup/util/Res;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;ZLcom/squareup/BundleKey;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/AvailableDiscountsStore;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/Transaction;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/payment/Order;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;>;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;>;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;>;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/BillPayment$Factory;",
            "Lcom/squareup/payment/InvoicePayment$Factory;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Z",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/settings/server/TipSettings;",
            ">;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/payment/Transaction;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    .line 181
    new-instance v28, Lcom/squareup/payment/Transaction;

    move-object/from16 v0, v28

    invoke-direct/range {v0 .. v27}, Lcom/squareup/payment/Transaction;-><init>(Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/InvoicePayment$Factory;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/tickets/Tickets;Lcom/squareup/util/Res;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;ZLcom/squareup/BundleKey;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/AvailableDiscountsStore;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)V

    return-object v28
.end method


# virtual methods
.method public get()Lcom/squareup/payment/Transaction;
    .locals 29

    move-object/from16 v0, p0

    .line 143
    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->orderKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/BundleKey;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->nextTaxesKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/BundleKey;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->nextTaxRulesKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/BundleKey;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->nextDiscountsKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/BundleKey;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/badbus/BadEventSink;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->userIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->billPaymentFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/payment/BillPayment$Factory;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->invoicePaymentFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/payment/InvoicePayment$Factory;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tickets/Tickets;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/log/tickets/OpenTicketsLogger;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/log/OhSnapLogger;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->diningOptionCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/main/DiningOptionCache;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v22

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->tipSettingsKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/BundleKey;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->availableDiscountsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/payment/AvailableDiscountsStore;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->paymentFlowTaskProviderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    iget-object v1, v0, Lcom/squareup/payment/Transaction_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/settings/server/Features;

    invoke-static/range {v2 .. v28}, Lcom/squareup/payment/Transaction_Factory;->newInstance(Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/InvoicePayment$Factory;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/tickets/Tickets;Lcom/squareup/util/Res;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;ZLcom/squareup/BundleKey;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/AvailableDiscountsStore;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/Transaction;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction_Factory;->get()Lcom/squareup/payment/Transaction;

    move-result-object v0

    return-object v0
.end method
