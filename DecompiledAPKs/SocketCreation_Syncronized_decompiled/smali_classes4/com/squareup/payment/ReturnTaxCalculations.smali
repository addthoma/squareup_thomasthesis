.class public final Lcom/squareup/payment/ReturnTaxCalculations;
.super Ljava/lang/Object;
.source "ReturnTaxCalculations.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReturnTaxCalculations.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReturnTaxCalculations.kt\ncom/squareup/payment/ReturnTaxCalculations\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,92:1\n704#2:93\n777#2,2:94\n1642#2,2:96\n704#2:98\n777#2,2:99\n704#2:101\n777#2,2:102\n1642#2,2:104\n1587#2,2:106\n1550#2,3:108\n1589#2:111\n*E\n*S KotlinDebug\n*F\n+ 1 ReturnTaxCalculations.kt\ncom/squareup/payment/ReturnTaxCalculations\n*L\n35#1:93\n35#1,2:94\n36#1,2:96\n65#1:98\n65#1,2:99\n66#1:101\n66#1,2:102\n67#1,2:104\n80#1,2:106\n80#1,3:108\n80#1:111\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007J\u0018\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/payment/ReturnTaxCalculations;",
        "",
        "()V",
        "calculateAfterApplicationItemizationsTotalAmountForReturn",
        "",
        "returnCart",
        "Lcom/squareup/checkout/ReturnCart;",
        "specifiedTax",
        "Lcom/squareup/checkout/Tax;",
        "calculateAppliedToAmountForReturn",
        "getTotalAmountOfItemizationsHavingTaxForReturn",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/ReturnTaxCalculations;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/payment/ReturnTaxCalculations;

    invoke-direct {v0}, Lcom/squareup/payment/ReturnTaxCalculations;-><init>()V

    sput-object v0, Lcom/squareup/payment/ReturnTaxCalculations;->INSTANCE:Lcom/squareup/payment/ReturnTaxCalculations;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final calculateAfterApplicationItemizationsTotalAmountForReturn(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/Tax;)J
    .locals 9
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "returnCart"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "specifiedTax"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p0, p1}, Lcom/squareup/payment/ReturnTaxCalculations;->getTotalAmountOfItemizationsHavingTaxForReturn(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/Tax;)J

    move-result-wide v0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnItemsAsCartItems()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 58
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    .line 61
    iget-object v4, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 64
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 98
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 99
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/squareup/checkout/Tax;

    .line 65
    iget-object v7, v7, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    iget-object v8, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v6, v7

    if-eqz v6, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_2
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 102
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/squareup/checkout/Tax;

    .line 66
    iget-object v7, v7, Lcom/squareup/checkout/Tax;->phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v8, p1, Lcom/squareup/checkout/Tax;->phase:Lcom/squareup/api/items/CalculationPhase;

    check-cast v8, Ljava/lang/Enum;

    invoke-virtual {v7, v8}, Lcom/squareup/api/items/CalculationPhase;->compareTo(Ljava/lang/Enum;)I

    move-result v7

    if-ltz v7, :cond_4

    const/4 v7, 0x1

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    :goto_2
    if-eqz v7, :cond_3

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 103
    :cond_5
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 104
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Tax;

    .line 67
    iget-object v4, v4, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    sub-long/2addr v0, v4

    goto :goto_3

    :cond_7
    return-wide v0
.end method

.method public static final calculateAppliedToAmountForReturn(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/Tax;)J
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "returnCart"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "specifiedTax"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p0, p1}, Lcom/squareup/payment/ReturnTaxCalculations;->getTotalAmountOfItemizationsHavingTaxForReturn(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/Tax;)J

    move-result-wide v0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnItemsAsCartItems()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 28
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    .line 31
    iget-object v4, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 34
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 93
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 94
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/checkout/Tax;

    .line 35
    iget-object v6, v6, Lcom/squareup/checkout/Tax;->phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v7, p1, Lcom/squareup/checkout/Tax;->phase:Lcom/squareup/api/items/CalculationPhase;

    check-cast v7, Ljava/lang/Enum;

    invoke-virtual {v6, v7}, Lcom/squareup/api/items/CalculationPhase;->compareTo(Ljava/lang/Enum;)I

    move-result v6

    if-ltz v6, :cond_2

    const/4 v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    :cond_3
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 96
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Tax;

    .line 36
    iget-object v4, v4, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    sub-long/2addr v0, v4

    goto :goto_2

    :cond_5
    return-wide v0
.end method

.method public static final getTotalAmountOfItemizationsHavingTaxForReturn(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/Tax;)J
    .locals 9
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "returnCart"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "specifiedTax"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnItems()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 107
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const-wide/16 v0, 0x0

    move-wide v2, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/ReturnCartItem;

    .line 82
    invoke-virtual {v4}, Lcom/squareup/checkout/ReturnCartItem;->getCartItem()Lcom/squareup/checkout/CartItem;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 108
    instance-of v6, v5, Ljava/util/Collection;

    const/4 v7, 0x0

    if-eqz v6, :cond_0

    move-object v6, v5

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_1

    .line 109
    :cond_0
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/Tax;

    .line 82
    iget-object v6, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    iget-object v8, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v7, 0x1

    :cond_2
    :goto_1
    if-eqz v7, :cond_3

    .line 84
    invoke-virtual {v4}, Lcom/squareup/checkout/ReturnCartItem;->getTotalAmountCollected()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    goto :goto_2

    .line 86
    :cond_3
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    :goto_2
    const-string v5, "additionalAmount"

    .line 88
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0

    :cond_4
    return-wide v2
.end method
