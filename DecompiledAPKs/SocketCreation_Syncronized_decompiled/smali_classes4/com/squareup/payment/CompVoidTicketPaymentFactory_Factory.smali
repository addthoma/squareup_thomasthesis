.class public final Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;
.super Ljava/lang/Object;
.source "CompVoidTicketPaymentFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/CompVoidTicketPaymentFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final advancedModifierLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final billPaymentEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final localStrategyFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentFlowTaskProviderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final serverClockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final voidMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->serverClockProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->gsonProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->advancedModifierLoggerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->localStrategyFactoryProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->billPaymentEventsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->paymentFlowTaskProviderFactoryProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p13, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;"
        }
    .end annotation

    .line 94
    new-instance v14, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Ljava/lang/Object;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/CompVoidTicketPaymentFactory;
    .locals 15

    .line 104
    new-instance v14, Lcom/squareup/payment/CompVoidTicketPaymentFactory;

    move-object/from16 v7, p6

    check-cast v7, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/payment/CompVoidTicketPaymentFactory;-><init>(Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/payment/CompVoidTicketPaymentFactory;
    .locals 14

    .line 79
    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->serverClockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/account/ServerClock;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->advancedModifierLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->localStrategyFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->billPaymentEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/payment/BillPaymentEvents;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/payment/VoidMonitor;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->paymentFlowTaskProviderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v13}, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->newInstance(Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Ljava/lang/Object;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/CompVoidTicketPaymentFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/payment/CompVoidTicketPaymentFactory_Factory;->get()Lcom/squareup/payment/CompVoidTicketPaymentFactory;

    move-result-object v0

    return-object v0
.end method
