.class public Lcom/squareup/payment/offline/ForwardedPayment;
.super Ljava/lang/Object;
.source "ForwardedPayment.java"

# interfaces
.implements Lcom/squareup/queue/PendingPayment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;,
        Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;,
        Lcom/squareup/payment/offline/ForwardedPayment$Helper;
    }
.end annotation


# instance fields
.field public final billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

.field public final captureForDisplay:Lcom/squareup/queue/Capture;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

.field private transient helper:Lcom/squareup/payment/offline/ForwardedPayment$Helper;

.field public final paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/PaymentResult;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move-object p1, v0

    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->secureCopyWithoutPIIForStoreAndForwardPayments()Lcom/squareup/queue/Capture;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-nez p2, :cond_1

    goto :goto_1

    .line 38
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/queue/bills/CompleteBill;->secureCopyWithoutPIIForStoreAndForwardPayments()Lcom/squareup/queue/bills/CompleteBill;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    .line 39
    iput-object p3, p0, Lcom/squareup/payment/offline/ForwardedPayment;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    .line 40
    iput-object p4, p0, Lcom/squareup/payment/offline/ForwardedPayment;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    .line 45
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    if-nez p1, :cond_2

    goto :goto_2

    .line 46
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t have both captureForDisplay and completeBillForDisplay"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 49
    :cond_3
    :goto_2
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    if-nez p1, :cond_4

    goto :goto_3

    .line 50
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t have billProcessingResult with captureForDisplay"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 52
    :cond_5
    :goto_3
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    if-nez p1, :cond_6

    goto :goto_4

    .line 53
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t have paymentResult with completeBillForDisplay"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_4
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/PaymentResult;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;Lcom/squareup/payment/offline/ForwardedPayment$1;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/payment/offline/ForwardedPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/PaymentResult;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V

    return-void
.end method

.method static fromStoredPaymentCapture(Lcom/squareup/queue/Capture;Ljava/lang/String;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPayment;

    new-instance v1, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;-><init>()V

    .line 75
    invoke-virtual {v1, p1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->unique_key(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    move-result-object p1

    sget-object v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNPROCESSED:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 76
    invoke-virtual {p1, v1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->status(Lcom/squareup/protos/queuebert/model/PaymentStatus;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->build()Lcom/squareup/protos/queuebert/model/PaymentResult;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, v1}, Lcom/squareup/payment/offline/ForwardedPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/PaymentResult;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V

    return-object v0
.end method

.method static fromStoredPaymentCompleteBill(Lcom/squareup/queue/bills/CompleteBill;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 3

    .line 81
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPayment;

    new-instance v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;-><init>()V

    .line 82
    invoke-virtual {p0}, Lcom/squareup/queue/bills/CompleteBill;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->bill_or_payment_id(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->UNPROCESSED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 83
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->status(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, p0, v2, v1}, Lcom/squareup/payment/offline/ForwardedPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/PaymentResult;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V

    return-object v0
.end method

.method private declared-synchronized getHelper()Lcom/squareup/payment/offline/ForwardedPayment$Helper;
    .locals 3

    monitor-enter p0

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment;->helper:Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    if-nez v0, :cond_1

    .line 64
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPayment;->isV1Payment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    iget-object v2, p0, Lcom/squareup/payment/offline/ForwardedPayment;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/protos/queuebert/model/PaymentResult;)V

    iput-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment;->helper:Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    goto :goto_0

    .line 67
    :cond_0
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    iget-object v2, p0, Lcom/squareup/payment/offline/ForwardedPayment;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    invoke-direct {v0, v1, v2}, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;-><init>(Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V

    iput-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment;->helper:Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    .line 70
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment;->helper:Lcom/squareup/payment/offline/ForwardedPayment$Helper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private isV1Payment()Z
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 105
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPayment;->getHelper()Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/payment/offline/ForwardedPayment$Helper;->asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public getBillId()Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 1

    .line 101
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPayment;->getHelper()Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/offline/ForwardedPayment$Helper;->getBillId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 109
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPayment;->getHelper()Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/offline/ForwardedPayment$Helper;->getUniqueKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPending()Z
    .locals 1

    .line 96
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPayment;->getHelper()Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/offline/ForwardedPayment$Helper;->isPending()Z

    move-result v0

    return v0
.end method

.method public withResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 1

    .line 92
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPayment;->getHelper()Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/payment/offline/ForwardedPayment$Helper;->withResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object p1

    return-object p1
.end method

.method public withResult(Lcom/squareup/protos/queuebert/model/PaymentResult;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 1

    .line 88
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPayment;->getHelper()Lcom/squareup/payment/offline/ForwardedPayment$Helper;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/payment/offline/ForwardedPayment$Helper;->withResult(Lcom/squareup/protos/queuebert/model/PaymentResult;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object p1

    return-object p1
.end method
