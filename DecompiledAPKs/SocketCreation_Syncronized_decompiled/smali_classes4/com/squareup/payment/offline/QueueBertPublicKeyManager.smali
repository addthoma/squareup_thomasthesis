.class public Lcom/squareup/payment/offline/QueueBertPublicKeyManager;
.super Ljava/lang/Object;
.source "QueueBertPublicKeyManager.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field private final checkEncryptor:Ljava/lang/Runnable;

.field private final clock:Lcom/squareup/util/Clock;

.field private longLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private shortLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/thread/executor/MainThread;Ldagger/Lazy;Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Lcom/squareup/util/Clock;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    invoke-direct {v0}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->shortLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    .line 43
    new-instance v0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    invoke-direct {v0}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->longLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    .line 48
    iput-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 49
    iput-object p2, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    .line 50
    iput-object p3, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 51
    iput-object p5, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->clock:Lcom/squareup/util/Clock;

    .line 52
    new-instance p1, Lcom/squareup/payment/offline/-$$Lambda$QueueBertPublicKeyManager$gfREocOw4uTx_Hqet5-PbVyhHJc;

    invoke-direct {p1, p4}, Lcom/squareup/payment/offline/-$$Lambda$QueueBertPublicKeyManager$gfREocOw4uTx_Hqet5-PbVyhHJc;-><init>(Ldagger/Lazy;)V

    iput-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->checkEncryptor:Ljava/lang/Runnable;

    return-void
.end method

.method private declared-synchronized getEncryptor(Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;)Lcom/squareup/encryption/JweEncryptor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;",
            ")",
            "Lcom/squareup/encryption/JweEncryptor<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 68
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->updateEncryptionKeys()V

    .line 70
    iget-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;

    invoke-virtual {v0}, Lcom/squareup/encryption/JweEncryptor;->isExpired()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object p1, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_0
    const/4 v0, 0x0

    .line 74
    :try_start_1
    iput-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;

    .line 76
    iget-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->publicKey:Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->publicKey:Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->isExpired()Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 78
    :try_start_2
    new-instance v0, Lcom/squareup/encryption/JweEncryptor;

    iget-object v1, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->publicKey:Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    sget-object v2, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->ADAPTER:Lcom/squareup/encryption/CryptoKeyAdapter;

    invoke-direct {v0, v1, v2}, Lcom/squareup/encryption/JweEncryptor;-><init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V

    iput-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;
    :try_end_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "Bad QueueBert public key certificate"

    .line 80
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v0}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logBadCertificate()V

    goto :goto_0

    :cond_1
    const-string v0, "Invalid or expired QueueBert public key"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 84
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v0}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logInvalidKey()V

    .line 88
    :goto_0
    iget-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;

    if-nez v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {v0}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logNoEncryptor()V

    .line 91
    :cond_2
    iget-object p1, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method static synthetic lambda$new$0(Ldagger/Lazy;)V
    .locals 0

    .line 54
    invoke-interface {p0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {p0}, Lcom/squareup/payment/OfflineModeMonitor;->checkEncryptor()V

    return-void
.end method

.method private declared-synchronized setEncryptionKey(Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;Lcom/squareup/server/account/protos/StoreAndForwardKey;)V
    .locals 10

    monitor-enter p0

    .line 115
    :try_start_0
    iget-object v0, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 123
    :cond_0
    iget-object v0, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->publicKey:Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->publicKey:Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    iget-object v1, v1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->bletchleyKeyId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 124
    monitor-exit p0

    return-void

    .line 129
    :cond_1
    :try_start_1
    iget-object v0, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v7
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :try_start_2
    iget-object v0, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    invoke-static {v0}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v0

    if-nez v0, :cond_2

    .line 138
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot decode raw certificate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 140
    iget-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logInvalidKey()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    monitor-exit p0

    return-void

    .line 144
    :cond_2
    :try_start_3
    new-instance v9, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    iget-object v2, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->clock:Lcom/squareup/util/Clock;

    iget-object v3, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    .line 145
    invoke-virtual {v0}, Lokio/ByteString;->toByteArray()[B

    move-result-object v4

    move-object v1, v9

    move-wide v5, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;[BJ)V

    iput-object v9, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->publicKey:Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    const/4 p2, 0x0

    .line 146
    iput-object p2, p1, Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;->encryptor:Lcom/squareup/encryption/JweEncryptor;

    .line 149
    iget-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide p1

    sub-long/2addr v7, p1

    .line 150
    iget-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object p2, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->checkEncryptor:Ljava/lang/Runnable;

    invoke-interface {p1, p2, v7, v8}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 151
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    .line 131
    :try_start_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bad public key expiration date: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 132
    iget-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logInvalidKeyExpiration()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 133
    monitor-exit p0

    return-void

    .line 116
    :cond_3
    :goto_0
    :try_start_5
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid public key "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 117
    new-instance p2, Ljava/lang/NullPointerException;

    invoke-direct {p2, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logInvalidKey()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 119
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private updateEncryptionKeys()V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getStoreAndForwardKey()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->shortLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    invoke-direct {p0, v0, v1}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->updateKey(Lcom/squareup/server/account/protos/StoreAndForwardKey;Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getStoreAndForwardBillKey()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->longLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    invoke-direct {p0, v0, v1}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->updateKey(Lcom/squareup/server/account/protos/StoreAndForwardKey;Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;)V

    return-void
.end method

.method private updateKey(Lcom/squareup/server/account/protos/StoreAndForwardKey;Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 107
    invoke-direct {p0, p2, p1}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->setEncryptionKey(Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;Lcom/squareup/server/account/protos/StoreAndForwardKey;)V

    goto :goto_0

    .line 109
    :cond_0
    iget-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logBadAccountStatus()V

    :goto_0
    return-void
.end method


# virtual methods
.method public declared-synchronized clearKeys()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 95
    :try_start_0
    iput-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->shortLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    .line 96
    iput-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->longLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getBillEncryptor()Lcom/squareup/encryption/JweEncryptor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/encryption/JweEncryptor<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->longLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    invoke-direct {p0, v0}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->getEncryptor(Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;)Lcom/squareup/encryption/JweEncryptor;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getEncryptor()Lcom/squareup/encryption/JweEncryptor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/encryption/JweEncryptor<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->shortLivedEncryptor:Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;

    invoke-direct {p0, v0}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->getEncryptor(Lcom/squareup/payment/offline/QueueBertPublicKeyManager$EncryptorInfo;)Lcom/squareup/encryption/JweEncryptor;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->checkEncryptor:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method
