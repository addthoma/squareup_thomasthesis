.class Lcom/squareup/payment/offline/StoreAndForwardTask$5;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;->sendBatch(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoredPayment;ZLjava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

.field final synthetic val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

.field final synthetic val$callback:Lcom/squareup/server/SquareCallback;

.field final synthetic val$excludeLastPayment:Z

.field final synthetic val$lastPaymentInBatch:Lcom/squareup/payment/offline/StoredPayment;

.field final synthetic val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/payment/offline/StoredPayment;Z)V
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$callback:Lcom/squareup/server/SquareCallback;

    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    iput-object p4, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    iput-object p5, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$lastPaymentInBatch:Lcom/squareup/payment/offline/StoredPayment;

    iput-boolean p6, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$excludeLastPayment:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-static {v0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$700(Lcom/squareup/server/SquareCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;)V
    .locals 7

    .line 272
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$callback:Lcom/squareup/server/SquareCallback;

    iget-object v3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    iget-object v4, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    iget-object v5, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$lastPaymentInBatch:Lcom/squareup/payment/offline/StoredPayment;

    iget-boolean v6, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->val$excludeLastPayment:Z

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$800(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/payment/offline/StoredPayment;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 275
    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 260
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$5;->onNext(Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;)V

    return-void
.end method
