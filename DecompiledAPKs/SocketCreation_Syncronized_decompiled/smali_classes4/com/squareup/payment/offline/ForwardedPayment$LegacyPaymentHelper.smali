.class Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;
.super Ljava/lang/Object;
.source "ForwardedPayment.java"

# interfaces
.implements Lcom/squareup/payment/offline/ForwardedPayment$Helper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/ForwardedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LegacyPaymentHelper"
.end annotation


# instance fields
.field private final captureForDisplay:Lcom/squareup/queue/Capture;

.field private final paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;


# direct methods
.method constructor <init>(Lcom/squareup/queue/Capture;Lcom/squareup/protos/queuebert/model/PaymentResult;)V
    .locals 0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->captureForDisplay:Lcom/squareup/queue/Capture;

    .line 132
    iput-object p2, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    return-void
.end method

.method private asPaymentResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/protos/queuebert/model/PaymentResult;
    .locals 2

    .line 193
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->unique_key(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 194
    invoke-direct {p0, p1}, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->asPaymentStatus(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;)Lcom/squareup/protos/queuebert/model/PaymentStatus;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->status(Lcom/squareup/protos/queuebert/model/PaymentStatus;)Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    move-result-object p1

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->build()Lcom/squareup/protos/queuebert/model/PaymentResult;

    move-result-object p1

    return-object p1
.end method

.method private asPaymentStatus(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;)Lcom/squareup/protos/queuebert/model/PaymentStatus;
    .locals 2

    .line 199
    sget-object v0, Lcom/squareup/payment/offline/ForwardedPayment$1;->$SwitchMap$com$squareup$protos$client$store_and_forward$bills$BillProcessingResult$Status:[I

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->STATUS_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-static {p1, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 210
    sget-object p1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNPROCESSED:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p1

    .line 207
    :cond_0
    sget-object p1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p1

    .line 204
    :cond_1
    sget-object p1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->CAPTURE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p1

    .line 202
    :cond_2
    sget-object p1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_PENDING:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p1
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 3

    .line 155
    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->getBillId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->captureForDisplay:Lcom/squareup/queue/Capture;

    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->isPending()Z

    move-result v2

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/activity/BillHistoryHelper;->asCaptureBuilder(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/queue/Capture;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    .line 156
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setStoreAndForward(Lcom/squareup/transactionhistory/pending/StoreAndForwardState;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 158
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    iget-object v1, v1, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 160
    sget-object v0, Lcom/squareup/payment/offline/ForwardedPayment$1;->$SwitchMap$com$squareup$protos$queuebert$model$PaymentStatus:[I

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    iget-object v1, v1, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    sget-object v2, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNKNOWN_PAYMENT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/queuebert/model/PaymentStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 166
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    goto :goto_0

    .line 162
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 174
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public getBillId()Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forPayment(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    return-object v0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->captureForDisplay:Lcom/squareup/queue/Capture;

    invoke-virtual {v0}, Lcom/squareup/queue/Capture;->getAuthorizationId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forPayment(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public isPending()Z
    .locals 3

    .line 136
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 139
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 140
    sget-object v2, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNPROCESSED:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_PENDING:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public withResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 0

    .line 189
    invoke-direct {p0, p1}, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->asPaymentResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/protos/queuebert/model/PaymentResult;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->withResult(Lcom/squareup/protos/queuebert/model/PaymentResult;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object p1

    return-object p1
.end method

.method public withResult(Lcom/squareup/protos/queuebert/model/PaymentResult;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 182
    iget-object v1, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/queuebert/model/PaymentStatus;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Payment %s now in state %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPayment;

    iget-object v3, p0, Lcom/squareup/payment/offline/ForwardedPayment$LegacyPaymentHelper;->captureForDisplay:Lcom/squareup/queue/Capture;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    move-object v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/payment/offline/ForwardedPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/PaymentResult;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;Lcom/squareup/payment/offline/ForwardedPayment$1;)V

    return-object v0
.end method
