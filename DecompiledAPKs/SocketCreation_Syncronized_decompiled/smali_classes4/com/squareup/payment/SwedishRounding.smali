.class public Lcom/squareup/payment/SwedishRounding;
.super Ljava/lang/Object;
.source "SwedishRounding.java"


# static fields
.field public static final SWEDISH_ROUNDING_TYPE:Ljava/lang/String; = "swedish_rounding"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5

    .line 18
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->getCashRounding(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    return-object p0

    .line 20
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v3, v0

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/payment/SwedishRounding;->roundedAmount(JJ)J

    move-result-wide v0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method private static getCashRounding(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/Integer;
    .locals 0

    .line 14
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getSwedishRoundingInterval(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static getDifference(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5

    .line 29
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->getCashRounding(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 30
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0

    .line 31
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v3, v0

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/payment/SwedishRounding;->roundedAmount(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static isRequired(Lcom/squareup/protos/common/Money;)Z
    .locals 5

    .line 24
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->getCashRounding(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 25
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    int-to-long v3, p0

    rem-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long p0, v1, v3

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static roundedAmount(JJ)J
    .locals 2

    const-wide/16 v0, 0x2

    mul-long p0, p0, v0

    add-long/2addr p0, p2

    mul-long v0, v0, p2

    .line 44
    div-long/2addr p0, v0

    mul-long p2, p2, p0

    return-wide p2
.end method
