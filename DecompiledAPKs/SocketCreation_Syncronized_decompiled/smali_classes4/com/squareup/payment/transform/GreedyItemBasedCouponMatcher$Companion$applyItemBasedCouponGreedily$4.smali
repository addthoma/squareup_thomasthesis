.class final Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;
.super Ljava/lang/Object;
.source "GreedyItemBasedCouponMatcher.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;->applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/checkout/CartItem$Builder;",
        "Lcom/squareup/checkout/CartItem$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGreedyItemBasedCouponMatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GreedyItemBasedCouponMatcher.kt\ncom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/checkout/CartItem$Builder;",
        "kotlin.jvm.PlatformType",
        "builder",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $discount:Lcom/squareup/checkout/Discount;

.field final synthetic $employee:Lcom/squareup/protos/client/Employee;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;->$discount:Lcom/squareup/checkout/Discount;

    iput-object p2, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;->$employee:Lcom/squareup/protos/client/Employee;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;->$discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    .line 93
    iget-object v0, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;->$employee:Lcom/squareup/protos/client/Employee;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;->$discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/checkout/util/ItemizationEvents;->discountAddEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;->call(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1
.end method
