.class public final Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;
.super Ljava/lang/Object;
.source "GreedyItemBasedCouponMatcher.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGreedyItemBasedCouponMatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GreedyItemBasedCouponMatcher.kt\ncom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,111:1\n1185#2,14:112\n704#3:126\n777#3,2:127\n1642#3,2:129\n*E\n*S KotlinDebug\n*F\n+ 1 GreedyItemBasedCouponMatcher.kt\ncom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion\n*L\n43#1,14:112\n72#1:126\n72#1,2:127\n73#1,2:129\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0007J&\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e2\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011H\u0007\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;",
        "",
        "()V",
        "applyItemBasedCouponGreedily",
        "",
        "discount",
        "Lcom/squareup/checkout/Discount;",
        "order",
        "Lcom/squareup/payment/Order;",
        "cartBuilder",
        "Lcom/squareup/checkout/Cart$Builder;",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "findBestMatchingItemOrNull",
        "Lkotlin/collections/IndexedValue;",
        "Lcom/squareup/checkout/CartItem;",
        "cartItems",
        "",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;-><init>()V

    return-void
.end method

.method public static synthetic applyItemBasedCouponGreedily$default(Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 63
    check-cast p4, Lcom/squareup/protos/client/Employee;

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;->applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method


# virtual methods
.method public final applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;)V
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;->applyItemBasedCouponGreedily$default(Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;ILjava/lang/Object;)V

    return-void
.end method

.method public final applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "discount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "order"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartBuilder"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 70
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object v0

    const-string v1, "order.cart"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v2, "order.cart.items"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 71
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->withIndex(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    .line 126
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 127
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lkotlin/collections/IndexedValue;

    invoke-virtual {v5}, Lkotlin/collections/IndexedValue;->component2()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/CartItem;

    .line 72
    iget-object v5, v5, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    iget-object v6, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_1
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 129
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/collections/IndexedValue;

    invoke-virtual {v3}, Lkotlin/collections/IndexedValue;->component1()I

    move-result v3

    .line 74
    new-instance v4, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;

    invoke-direct {v4, p3, p1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;-><init>(Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/checkout/Discount;)V

    check-cast v4, Lrx/functions/Func1;

    invoke-virtual {p3, v3, v4}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    goto :goto_1

    .line 81
    :cond_2
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;

    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/checkout/Cart;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;->findBestMatchingItemOrNull(Lcom/squareup/checkout/Discount;Ljava/util/List;)Lkotlin/collections/IndexedValue;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 86
    invoke-virtual {v0}, Lkotlin/collections/IndexedValue;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    const-string v2, "bestMatchingItem.value.quantity"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    const-string v4, "ONE"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v3}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 89
    invoke-virtual {v0}, Lkotlin/collections/IndexedValue;->getIndex()I

    move-result v1

    const/4 v2, 0x0

    sget-object v3, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p2, v1, v2, v3}, Lcom/squareup/payment/Order;->splitItem(ILcom/squareup/protos/client/Employee;Ljava/math/BigDecimal;)V

    .line 91
    invoke-virtual {v0}, Lkotlin/collections/IndexedValue;->getIndex()I

    move-result p2

    new-instance v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;

    invoke-direct {v0, p1, p4}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$4;-><init>(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p3, p2, v0}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    goto :goto_2

    .line 98
    :cond_3
    invoke-virtual {v0}, Lkotlin/collections/IndexedValue;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/CartItem;

    iget-object p2, p2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lcom/squareup/util/BigDecimals;->equalsIgnoringScale(Ljava/math/BigDecimal;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 100
    invoke-virtual {v0}, Lkotlin/collections/IndexedValue;->getIndex()I

    move-result p2

    new-instance v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$5;

    invoke-direct {v0, p1, p4}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$5;-><init>(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p3, p2, v0}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    :cond_4
    :goto_2
    return-void

    .line 65
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Only discounts that apply only to one item may use this matcher"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final findBestMatchingItemOrNull(Lcom/squareup/checkout/Discount;Ljava/util/List;)Lkotlin/collections/IndexedValue;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Discount;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lkotlin/collections/IndexedValue<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    const-string v0, "discount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p2, Ljava/lang/Iterable;

    .line 29
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 30
    invoke-static {p2}, Lkotlin/sequences/SequencesKt;->withIndex(Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 31
    new-instance v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$findBestMatchingItemOrNull$1;

    invoke-direct {v0, p1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$findBestMatchingItemOrNull$1;-><init>(Lcom/squareup/checkout/Discount;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 33
    sget-object v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$findBestMatchingItemOrNull$2;->INSTANCE:Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$findBestMatchingItemOrNull$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 35
    new-instance v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$findBestMatchingItemOrNull$3;

    invoke-direct {v0, p1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$findBestMatchingItemOrNull$3;-><init>(Lcom/squareup/checkout/Discount;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 112
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 113
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    move-object p2, p1

    goto :goto_0

    .line 114
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    .line 115
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 116
    :cond_1
    move-object v0, p2

    check-cast v0, Lkotlin/collections/IndexedValue;

    invoke-virtual {v0}, Lkotlin/collections/IndexedValue;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 44
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->unitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 45
    check-cast v0, Ljava/lang/Comparable;

    .line 118
    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 119
    move-object v2, v1

    check-cast v2, Lkotlin/collections/IndexedValue;

    invoke-virtual {v2}, Lkotlin/collections/IndexedValue;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 44
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->unitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 45
    check-cast v2, Ljava/lang/Comparable;

    .line 120
    invoke-interface {v0, v2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_3

    move-object p2, v1

    move-object v0, v2

    .line 124
    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 125
    :goto_0
    check-cast p2, Lkotlin/collections/IndexedValue;

    return-object p2
.end method
