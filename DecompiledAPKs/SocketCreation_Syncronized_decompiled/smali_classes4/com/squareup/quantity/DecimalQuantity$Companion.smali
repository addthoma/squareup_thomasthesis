.class public final Lcom/squareup/quantity/DecimalQuantity$Companion;
.super Ljava/lang/Object;
.source "DecimalQuantity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quantity/DecimalQuantity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u0007J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0008H\u0007J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\tH\u0007J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\u0008H\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/quantity/DecimalQuantity$Companion;",
        "",
        "()V",
        "of",
        "Lcom/squareup/quantity/DecimalQuantity;",
        "value",
        "Ljava/math/BigDecimal;",
        "roundingScale",
        "",
        "",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/quantity/DecimalQuantity$Companion;-><init>()V

    return-void
.end method

.method public static synthetic of$default(Lcom/squareup/quantity/DecimalQuantity$Companion;Ljava/math/BigDecimal;IILjava/lang/Object;)Lcom/squareup/quantity/DecimalQuantity;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 131
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quantity/DecimalQuantity$Companion;->of(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final of(I)Lcom/squareup/quantity/DecimalQuantity;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 153
    new-instance v0, Lcom/squareup/quantity/DecimalQuantity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final of(Ljava/lang/String;)Lcom/squareup/quantity/DecimalQuantity;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 141
    new-instance p1, Lcom/squareup/quantity/DecimalQuantity;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public final of(Ljava/lang/String;I)Lcom/squareup/quantity/DecimalQuantity;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v0, Lcom/squareup/quantity/DecimalQuantity;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final of(Ljava/math/BigDecimal;)Lcom/squareup/quantity/DecimalQuantity;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/quantity/DecimalQuantity$Companion;->of$default(Lcom/squareup/quantity/DecimalQuantity$Companion;Ljava/math/BigDecimal;IILjava/lang/Object;)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object p1

    return-object p1
.end method

.method public final of(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/DecimalQuantity;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/squareup/quantity/DecimalQuantity;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/quantity/DecimalQuantity;-><init>(Ljava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
