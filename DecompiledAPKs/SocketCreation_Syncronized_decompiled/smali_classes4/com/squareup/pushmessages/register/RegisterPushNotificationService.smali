.class public interface abstract Lcom/squareup/pushmessages/register/RegisterPushNotificationService;
.super Ljava/lang/Object;
.source "RegisterPushNotificationService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
        "",
        "updateRegistrationId",
        "Lcom/squareup/server/SimpleStandardResponse;",
        "Lcom/squareup/server/SimpleResponse;",
        "body",
        "Lcom/squareup/pushmessages/register/UpdateRegistrationBody;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract updateRegistrationId(Lcom/squareup/pushmessages/register/UpdateRegistrationBody;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Lcom/squareup/pushmessages/register/UpdateRegistrationBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/pushmessages/register/UpdateRegistrationBody;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/account/push-notifications/update"
    .end annotation
.end method
