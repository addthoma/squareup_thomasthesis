.class public final Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;
.super Ljava/lang/Object;
.source "RealPushNotificationNotifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/pushmessages/RealPushNotificationNotifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final appContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final delegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->appContextProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->delegateProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;)",
            "Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Landroid/app/NotificationManager;Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/notification/NotificationWrapper;)Lcom/squareup/pushmessages/RealPushNotificationNotifier;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/pushmessages/RealPushNotificationNotifier;-><init>(Landroid/app/Application;Landroid/app/NotificationManager;Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/notification/NotificationWrapper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/pushmessages/RealPushNotificationNotifier;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->appContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->delegateProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/pushmessages/PushMessageDelegate;

    iget-object v3, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/notification/NotificationWrapper;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->newInstance(Landroid/app/Application;Landroid/app/NotificationManager;Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/notification/NotificationWrapper;)Lcom/squareup/pushmessages/RealPushNotificationNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/pushmessages/RealPushNotificationNotifier_Factory;->get()Lcom/squareup/pushmessages/RealPushNotificationNotifier;

    move-result-object v0

    return-object v0
.end method
