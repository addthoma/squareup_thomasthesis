.class final enum Lcom/squareup/pushmessages/PushMessageOp$14;
.super Lcom/squareup/pushmessages/PushMessageOp;
.source "PushMessageOp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pushmessages/PushMessageOp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 127
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/pushmessages/PushMessageOp;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/pushmessages/PushMessageOp$1;)V

    return-void
.end method


# virtual methods
.method createMessage(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessage;"
        }
    .end annotation

    const-string v0, "client-ledger-start-time-stamp"

    .line 129
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-string v2, "client-ledger-end-time-stamp"

    .line 130
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 131
    new-instance p1, Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;-><init>(JJ)V

    return-object p1
.end method
