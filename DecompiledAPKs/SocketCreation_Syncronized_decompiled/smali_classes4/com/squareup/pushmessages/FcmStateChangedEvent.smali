.class public Lcom/squareup/pushmessages/FcmStateChangedEvent;
.super Lcom/squareup/log/OhSnapLoggable$JavaOhSnapLoggable;
.source "FcmStateChangedEvent.java"


# instance fields
.field private final enabled:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/log/OhSnapLoggable$JavaOhSnapLoggable;-><init>()V

    .line 16
    iput-boolean p1, p0, Lcom/squareup/pushmessages/FcmStateChangedEvent;->enabled:Z

    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/pushmessages/FcmStateChangedEvent;->enabled:Z

    return v0
.end method
