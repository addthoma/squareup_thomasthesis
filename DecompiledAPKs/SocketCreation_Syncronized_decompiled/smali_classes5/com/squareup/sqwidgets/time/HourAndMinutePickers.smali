.class public final Lcom/squareup/sqwidgets/time/HourAndMinutePickers;
.super Ljava/lang/Object;
.source "HourAndMinutePickers.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/time/HourAndMinutePickers$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHourAndMinutePickers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HourAndMinutePickers.kt\ncom/squareup/sqwidgets/time/HourAndMinutePickers\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,111:1\n1360#2:112\n1429#2,3:113\n*E\n*S KotlinDebug\n*F\n+ 1 HourAndMinutePickers.kt\ncom/squareup/sqwidgets/time/HourAndMinutePickers\n*L\n87#1:112\n87#1,3:113\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u001f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u0000 12\u00020\u0001:\u00011B\u0080\u0001\u0012U\u0010\u0002\u001aQ\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\n\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r0\u0003j\u0002`\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u0004\u0012\u0006\u0010\u0011\u001a\u00020\u0004\u0012\u0006\u0010\u0012\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0013Jy\u0010\"\u001a\u00020\r2U\u0010\u0002\u001aQ\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\n\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r0\u0003j\u0002`\u000e2\u0008\u0008\u0001\u0010#\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0004H\u0002Ji\u0010$\u001a\u00020\r2U\u0010\u0002\u001aQ\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\n\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r0\u0003j\u0002`\u000e2\u0008\u0008\u0001\u0010%\u001a\u00020\u0004H\u0002Jw\u0010&\u001a\u00020\r2U\u0010\u0002\u001aQ\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\n\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r0\u0003j\u0002`\u000e2\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00042\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0002J\u0010\u0010\'\u001a\u00020\t2\u0006\u0010(\u001a\u00020\u0004H\u0002J\u0010\u0010)\u001a\u00020\t2\u0006\u0010(\u001a\u00020\u0004H\u0002J2\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00042\u0012\u0010-\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\t0.H\u0002J\u0016\u0010/\u001a\u0002002\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u0004R\u0011\u0010\u0014\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0018\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u0016R\u0011\u0010\u001a\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u0016R\u0011\u0010\u001c\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u0016R\u000e\u0010\u001e\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u001f\u001a\u00020\u0004*\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010!\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/time/HourAndMinutePickers;",
        "",
        "pickerFactory",
        "Lkotlin/Function3;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "id",
        "",
        "",
        "values",
        "",
        "isInfiniteScroll",
        "Lcom/squareup/sqwidgets/time/Picker;",
        "Lcom/squareup/sqwidgets/time/PickerFactory;",
        "hourPickerId",
        "minutePickerId",
        "hourPickerStart",
        "hourPickerEnd",
        "(Lkotlin/jvm/functions/Function3;IIII)V",
        "hour",
        "getHour",
        "()I",
        "hourPicker",
        "hourPickerHeight",
        "getHourPickerHeight",
        "hourPickerWidth",
        "getHourPickerWidth",
        "minute",
        "getMinute",
        "minutePicker",
        "currentValue",
        "getCurrentValue",
        "(Lcom/squareup/sqwidgets/time/Picker;)I",
        "getHourPicker",
        "hourId",
        "getMinutePicker",
        "minuteId",
        "getPicker",
        "getValueForHourPicker",
        "value",
        "getValueForMinutePicker",
        "getValuesForPicker",
        "start",
        "end",
        "getValue",
        "Lkotlin/Function1;",
        "selectValues",
        "",
        "Companion",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/sqwidgets/time/HourAndMinutePickers$Companion;

.field private static final MINUTE_PICKER_END:I = 0x3b

.field private static final MINUTE_PICKER_START:I


# instance fields
.field private final hourPicker:Lcom/squareup/sqwidgets/time/Picker;

.field private final minutePicker:Lcom/squareup/sqwidgets/time/Picker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->Companion:Lcom/squareup/sqwidgets/time/HourAndMinutePickers$Companion;

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function3;IIII)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/sqwidgets/time/Picker;",
            ">;IIII)V"
        }
    .end annotation

    const-string v0, "pickerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getHourPicker(Lkotlin/jvm/functions/Function3;III)Lcom/squareup/sqwidgets/time/Picker;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->hourPicker:Lcom/squareup/sqwidgets/time/Picker;

    .line 26
    invoke-direct {p0, p1, p3}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getMinutePicker(Lkotlin/jvm/functions/Function3;I)Lcom/squareup/sqwidgets/time/Picker;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->minutePicker:Lcom/squareup/sqwidgets/time/Picker;

    return-void
.end method

.method public static final synthetic access$getValueForHourPicker(Lcom/squareup/sqwidgets/time/HourAndMinutePickers;I)Ljava/lang/String;
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getValueForHourPicker(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getValueForMinutePicker(Lcom/squareup/sqwidgets/time/HourAndMinutePickers;I)Ljava/lang/String;
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getValueForMinutePicker(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final getCurrentValue(Lcom/squareup/sqwidgets/time/Picker;)I
    .locals 0

    .line 96
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/time/Picker;->getSelectedValue()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method private final getHourPicker(Lkotlin/jvm/functions/Function3;III)Lcom/squareup/sqwidgets/time/Picker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/sqwidgets/time/Picker;",
            ">;III)",
            "Lcom/squareup/sqwidgets/time/Picker;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers$getHourPicker$1;

    move-object v1, p0

    check-cast v1, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    invoke-direct {v0, v1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers$getHourPicker$1;-><init>(Lcom/squareup/sqwidgets/time/HourAndMinutePickers;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p3, p4, v0}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getValuesForPicker(IILkotlin/jvm/functions/Function1;)Ljava/util/List;

    move-result-object p3

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getPicker(Lkotlin/jvm/functions/Function3;ILjava/util/List;)Lcom/squareup/sqwidgets/time/Picker;

    move-result-object p1

    return-object p1
.end method

.method private final getMinutePicker(Lkotlin/jvm/functions/Function3;I)Lcom/squareup/sqwidgets/time/Picker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/sqwidgets/time/Picker;",
            ">;I)",
            "Lcom/squareup/sqwidgets/time/Picker;"
        }
    .end annotation

    .line 69
    new-instance v0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers$getMinutePicker$1;

    move-object v1, p0

    check-cast v1, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    invoke-direct {v0, v1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers$getMinutePicker$1;-><init>(Lcom/squareup/sqwidgets/time/HourAndMinutePickers;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/16 v2, 0x3b

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getValuesForPicker(IILkotlin/jvm/functions/Function1;)Ljava/util/List;

    move-result-object v0

    .line 67
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getPicker(Lkotlin/jvm/functions/Function3;ILjava/util/List;)Lcom/squareup/sqwidgets/time/Picker;

    move-result-object p1

    return-object p1
.end method

.method private final getPicker(Lkotlin/jvm/functions/Function3;ILjava/util/List;)Lcom/squareup/sqwidgets/time/Picker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/sqwidgets/time/Picker;",
            ">;I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/sqwidgets/time/Picker;"
        }
    .end annotation

    .line 78
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, p2, p3, v0}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sqwidgets/time/Picker;

    return-object p1
.end method

.method private final getValueForHourPicker(I)Ljava/lang/String;
    .locals 1

    .line 90
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Integer.toString(value)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getValueForMinutePicker(I)Ljava/lang/String;
    .locals 3

    .line 93
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Locale.US"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    array-length p1, v1

    invoke-static {v1, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    const-string v1, "%02d"

    invoke-static {v0, v1, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "java.lang.String.format(locale, format, *args)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getValuesForPicker(IILkotlin/jvm/functions/Function1;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-ge p1, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 87
    new-instance v0, Lkotlin/ranges/IntRange;

    invoke-direct {v0, p1, p2}, Lkotlin/ranges/IntRange;-><init>(II)V

    check-cast v0, Ljava/lang/Iterable;

    .line 112
    new-instance p1, Ljava/util/ArrayList;

    const/16 p2, 0xa

    invoke-static {v0, p2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 113
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lkotlin/collections/IntIterator;

    invoke-virtual {v0}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v0

    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 115
    :cond_1
    check-cast p1, Ljava/util/List;

    return-object p1

    .line 86
    :cond_2
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is not less than "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method


# virtual methods
.method public final getHour()I
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->hourPicker:Lcom/squareup/sqwidgets/time/Picker;

    invoke-direct {p0, v0}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getCurrentValue(Lcom/squareup/sqwidgets/time/Picker;)I

    move-result v0

    return v0
.end method

.method public final getHourPickerHeight()I
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->hourPicker:Lcom/squareup/sqwidgets/time/Picker;

    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/Picker;->getHeight()I

    move-result v0

    return v0
.end method

.method public final getHourPickerWidth()I
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->hourPicker:Lcom/squareup/sqwidgets/time/Picker;

    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/Picker;->getWidth()I

    move-result v0

    return v0
.end method

.method public final getMinute()I
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->minutePicker:Lcom/squareup/sqwidgets/time/Picker;

    invoke-direct {p0, v0}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getCurrentValue(Lcom/squareup/sqwidgets/time/Picker;)I

    move-result v0

    return v0
.end method

.method public final selectValues(II)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->hourPicker:Lcom/squareup/sqwidgets/time/Picker;

    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getValueForHourPicker(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/sqwidgets/time/Picker;->selectValue(Ljava/lang/String;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->minutePicker:Lcom/squareup/sqwidgets/time/Picker;

    invoke-direct {p0, p2}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getValueForMinutePicker(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/sqwidgets/time/Picker;->selectValue(Ljava/lang/String;)V

    return-void
.end method
