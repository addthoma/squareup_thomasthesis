.class public final Lcom/squareup/sqwidgets/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final date_picker_calendar_vertical_padding:I = 0x7f0700ef

.field public static final default_status_bar_font_size:I = 0x7f0700f4

.field public static final default_status_bar_glyph_font_size:I = 0x7f0700f5

.field public static final default_status_bar_height:I = 0x7f0700f6

.field public static final default_status_bar_icon_height:I = 0x7f0700f7

.field public static final default_status_bar_icon_padding_horizontal:I = 0x7f0700f8

.field public static final default_status_bar_padding_full:I = 0x7f0700f9

.field public static final default_status_bar_padding_half:I = 0x7f0700fa

.field public static final status_bar_font_size:I = 0x7f0704fc

.field public static final status_bar_glyph_font_size:I = 0x7f0704fd

.field public static final status_bar_height:I = 0x7f0704fe

.field public static final status_bar_icon_height:I = 0x7f0704ff

.field public static final status_bar_icon_padding_horizontal:I = 0x7f070500

.field public static final status_bar_padding_full:I = 0x7f070501

.field public static final status_bar_padding_half:I = 0x7f070502

.field public static final status_bar_widget_width:I = 0x7f070503

.field public static final switcher_button_font_size:I = 0x7f070510

.field public static final switcher_button_height:I = 0x7f070511

.field public static final switcher_button_margin_half:I = 0x7f070512

.field public static final switcher_button_vertical_strip_margin:I = 0x7f070513

.field public static final switcher_button_vertical_strip_width:I = 0x7f070514

.field public static final switcher_button_width:I = 0x7f070515

.field public static final time_picker_size:I = 0x7f070532

.field public static final x2_dialog_min_height:I = 0x7f070574

.field public static final x2_dialog_width:I = 0x7f070575

.field public static final x2_gap_large:I = 0x7f070576

.field public static final x2_gap_small:I = 0x7f070577

.field public static final x2_glyph_size:I = 0x7f070578

.field public static final x2_image_title_gap:I = 0x7f070579

.field public static final x2_min_width:I = 0x7f07057a

.field public static final x2_text_size_extra_large:I = 0x7f07057b

.field public static final x2_text_size_extra_small:I = 0x7f07057c

.field public static final x2_text_size_large:I = 0x7f07057d

.field public static final x2_text_size_medium:I = 0x7f07057e

.field public static final x2_text_size_small:I = 0x7f07057f

.field public static final x2_title_message_gap:I = 0x7f070580


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
