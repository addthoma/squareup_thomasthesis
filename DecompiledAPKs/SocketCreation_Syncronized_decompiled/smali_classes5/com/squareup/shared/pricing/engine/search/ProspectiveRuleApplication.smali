.class public Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
.super Ljava/lang/Object;
.source "ProspectiveRuleApplication.java"


# static fields
.field public static TIEBREAKERS:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;",
            ">;"
        }
    .end annotation
.end field

.field public static TOTAL_VALUE:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final applied:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final inflateFactor:J

.field private final matched:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final matchedQuantity:Ljava/math/BigDecimal;

.field private final offsets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

.field private final totalValue:J

.field private final uninflatedMatchedQuantity:Ljava/math/BigDecimal;

.field private final unitValue:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication$$Lambda$0;->$instance:Ljava/util/Comparator;

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->TIEBREAKERS:Ljava/util/Comparator;

    .line 42
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication$$Lambda$1;->$instance:Ljava/util/Comparator;

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->TOTAL_VALUE:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "JJ",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 70
    iput-wide p2, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->unitValue:J

    mul-long p2, p2, p4

    .line 71
    iput-wide p2, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->totalValue:J

    .line 72
    iput-wide p4, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->inflateFactor:J

    .line 73
    invoke-direct {p0, p6, p4, p5}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->inflate(Ljava/util/Map;J)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->matched:Ljava/util/Map;

    .line 74
    invoke-direct {p0, p7, p4, p5}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->inflate(Ljava/util/Map;J)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->applied:Ljava/util/Map;

    .line 75
    iput-object p8, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->offsets:Ljava/util/Map;

    .line 77
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 78
    invoke-interface {p6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/math/BigDecimal;

    .line 79
    invoke-virtual {p1, p3}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    goto :goto_0

    .line 81
    :cond_0
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->uninflatedMatchedQuantity:Ljava/math/BigDecimal;

    .line 82
    invoke-static {p4, p5}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->matchedQuantity:Ljava/math/BigDecimal;

    return-void
.end method

.method private inflate(Ljava/util/Map;J)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;J)",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 182
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 183
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    invoke-static {p2, p3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static final synthetic lambda$static$0$ProspectiveRuleApplication(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)I
    .locals 5

    .line 23
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getMatchedQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getMatchedQuantity()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 28
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    const/4 p0, 0x1

    return p0

    .line 31
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_2

    const/4 p0, -0x1

    return p0

    .line 35
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object p0

    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static final synthetic lambda$static$1$ProspectiveRuleApplication(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)I
    .locals 5

    .line 44
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getTotalValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return v4

    .line 50
    :cond_0
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->TIEBREAKERS:Ljava/util/Comparator;

    invoke-interface {v0, p0, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p0

    return p0
.end method


# virtual methods
.method public getApplied()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->applied:Ljava/util/Map;

    return-object v0
.end method

.method public getCost(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)Ljava/math/BigDecimal;
    .locals 5

    .line 148
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getUnitValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    .line 149
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getUninflatedMatchedQuantity()Ljava/math/BigDecimal;

    move-result-object v1

    sget-object v2, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v0, v1, v2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 151
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMaxApplicationsPerAttachment()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getUninflatedMatchedQuantity()Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getUninflatedMatchedQuantity()Ljava/math/BigDecimal;

    move-result-object v2

    .line 157
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getUninflatedMatchedQuantity()Ljava/math/BigDecimal;

    move-result-object p1

    int-to-long v3, v1

    .line 158
    invoke-static {v3, v4}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    .line 157
    invoke-virtual {p1, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 156
    invoke-virtual {v2, p1}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 155
    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public getInflateFactor()J
    .locals 2

    .line 113
    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->inflateFactor:J

    return-wide v0
.end method

.method public getMatched()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->matched:Ljava/util/Map;

    return-object v0
.end method

.method public getMatchedQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->matchedQuantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getOffsets()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->offsets:Ljava/util/Map;

    return-object v0
.end method

.method public getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    return-object v0
.end method

.method public getTotalValue()J
    .locals 2

    .line 106
    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->totalValue:J

    return-wide v0
.end method

.method public getUninflatedMatchedQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->uninflatedMatchedQuantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getUnitValue()J
    .locals 2

    .line 166
    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->unitValue:J

    return-wide v0
.end method
