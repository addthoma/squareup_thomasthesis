.class public Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;
.super Lcom/squareup/shared/pricing/engine/search/JunctionSet;
.source "ConjunctiveSet.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    return-void
.end method


# virtual methods
.method public getExhaustGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;>;"
        }
    .end annotation

    move-object v0, p0

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 59
    iget-object v2, v0, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;->children:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object/from16 v11, p3

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/pricing/engine/search/Provider;

    .line 60
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 62
    iget-object v7, v0, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;->rangeMax:Ljava/math/BigDecimal;

    move-object v4, p1

    move-object v5, p2

    move-object v6, v11

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    invoke-interface/range {v3 .. v10}, Lcom/squareup/shared/pricing/engine/search/Provider;->exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v3

    move-object/from16 v5, p4

    .line 64
    invoke-static {p1, v5}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->exhaustComparator(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)Ljava/util/Comparator;

    move-result-object v6

    invoke-static {v3, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 66
    iget-object v6, v0, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;->rangeMax:Ljava/math/BigDecimal;

    invoke-static {v12, v3, v11, v6}, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;->populateGroup(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/math/BigDecimal;)Ljava/util/Map;

    move-result-object v11

    .line 68
    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public getSatisfyGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;>;"
        }
    .end annotation

    move-object v0, p0

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 34
    iget-object v2, v0, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;->children:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v10, p3

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/pricing/engine/search/Provider;

    .line 35
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    move-object v4, p1

    move-object v5, p2

    move-object v6, v10

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    .line 38
    invoke-interface/range {v3 .. v9}, Lcom/squareup/shared/pricing/engine/search/Provider;->satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v3

    move-object/from16 v5, p4

    .line 39
    invoke-static {p1, v5}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->satisfyComparator(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)Ljava/util/Comparator;

    move-result-object v6

    invoke-static {v3, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 41
    iget-object v6, v0, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;->rangeMin:Ljava/math/BigDecimal;

    invoke-static {v11, v3, v10, v6}, Lcom/squareup/shared/pricing/engine/search/ConjunctiveSet;->populateGroup(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/math/BigDecimal;)Ljava/util/Map;

    move-result-object v10

    .line 43
    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method
