.class public Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;
.super Ljava/lang/Object;
.source "RuleApplication.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/rules/RuleApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Target"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;",
        ">;"
    }
.end annotation


# instance fields
.field private offset:Ljava/math/BigDecimal;

.field private quantity:Ljava/math/BigDecimal;

.field private target:Lcom/squareup/shared/pricing/models/ClientServerIds;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;)V
    .locals 1

    .line 17
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;-><init>(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/pricing/models/ClientServerIds;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_2

    .line 24
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->target:Lcom/squareup/shared/pricing/models/ClientServerIds;

    if-eqz p2, :cond_1

    .line 26
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p2, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    if-lez p1, :cond_1

    .line 29
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->quantity:Ljava/math/BigDecimal;

    if-eqz p3, :cond_0

    .line 31
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p3, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p1

    if-ltz p1, :cond_0

    .line 34
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->offset:Ljava/math/BigDecimal;

    return-void

    .line 32
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "offset may not be null and may not be negative"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 27
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "quantity may not be null and must be positive"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 22
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "target may not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public compareTo(Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;)I
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->target:Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->target:Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->target:Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getServerId()Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->target:Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getServerId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->compareTo(Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;)I

    move-result p1

    return p1
.end method

.method public getOffset()Ljava/math/BigDecimal;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->offset:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getTarget()Lcom/squareup/shared/pricing/models/ClientServerIds;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->target:Lcom/squareup/shared/pricing/models/ClientServerIds;

    return-object v0
.end method
