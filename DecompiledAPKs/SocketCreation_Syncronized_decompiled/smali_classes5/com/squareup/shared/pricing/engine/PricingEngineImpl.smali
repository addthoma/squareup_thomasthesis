.class public Lcom/squareup/shared/pricing/engine/PricingEngineImpl;
.super Ljava/lang/Object;
.source "PricingEngineImpl.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/PricingEngine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;

.field private final blacklist:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            ">;>;"
        }
    .end annotation
.end field

.field private final clock:Lcom/squareup/shared/catalog/logging/Clock;

.field private final localizer:Lcom/squareup/shared/i18n/Localizer;

.field private final ruleSetLoader:Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;

.field private final searchLoader:Lcom/squareup/shared/pricing/engine/search/SearchLoader;

.field private final timePeriodVerificationCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;Lcom/squareup/shared/pricing/engine/search/SearchLoader;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 95
    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;-><init>(Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;Lcom/squareup/shared/pricing/engine/search/SearchLoader;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;Lcom/squareup/shared/i18n/Localizer;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;Lcom/squareup/shared/pricing/engine/search/SearchLoader;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;Lcom/squareup/shared/i18n/Localizer;)V
    .locals 4

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;

    const/16 v1, 0x7530

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$SimpleLRUMap;-><init>(Lcom/squareup/shared/pricing/engine/PricingEngineImpl;IFZ)V

    .line 47
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->timePeriodVerificationCache:Ljava/util/Map;

    .line 81
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->ruleSetLoader:Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;

    .line 82
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->searchLoader:Lcom/squareup/shared/pricing/engine/search/SearchLoader;

    .line 83
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    .line 84
    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->analytics:Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;

    .line 85
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->blacklist:Ljava/util/Map;

    .line 86
    iput-object p5, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->localizer:Lcom/squareup/shared/i18n/Localizer;

    return-void
.end method

.method private getLineItemApplications(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;"
        }
    .end annotation

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 151
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;

    .line 152
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountTargetScope()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    if-ne v2, v3, :cond_0

    .line 154
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getWholePurchaseApplications(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;"
        }
    .end annotation

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;

    .line 165
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountTargetScope()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    if-ne v2, v3, :cond_0

    .line 167
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 168
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 169
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getBlacklistedDiscounts()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v6

    invoke-interface {v6}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 170
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 174
    :cond_2
    new-instance v3, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;

    .line 175
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v4

    invoke-interface {v4}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v4

    .line 176
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v1, v2}, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 178
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method static final synthetic lambda$applyRules$0$PricingEngineImpl(Lcom/squareup/shared/pricing/engine/PricingEngineResult;)Lcom/squareup/shared/pricing/engine/PricingEngineResult;
    .locals 0

    return-object p0
.end method

.method private loadRulesAndSearch(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/TimeZone;Z)Lcom/squareup/shared/pricing/engine/PricingEngineResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;",
            "Ljava/util/TimeZone;",
            "Z)",
            "Lcom/squareup/shared/pricing/engine/PricingEngineResult;"
        }
    .end annotation

    .line 115
    invoke-static {p2}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->earliestAdded(Ljava/util/List;)Ljava/util/Date;

    move-result-object v4

    .line 116
    invoke-static {p2}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->latestAdded(Ljava/util/List;)Ljava/util/Date;

    move-result-object v5

    .line 118
    invoke-virtual {p4}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->startLoader()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 119
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 120
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 121
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getVariationID()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getItemID()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getCategoryID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getCategoryID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->ruleSetLoader:Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;

    move-object v1, p1

    move-object v2, p5

    move-object v3, p3

    move v7, p6

    .line 127
    invoke-interface/range {v0 .. v7}, Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;->load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/TimeZone;Ljava/util/Set;Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;Z)Lcom/squareup/shared/pricing/engine/rules/RuleSet;

    move-result-object p3

    .line 130
    invoke-virtual {p4, p3}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->recordRuleSet(Lcom/squareup/shared/pricing/engine/rules/RuleSet;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 132
    iget-object p6, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->searchLoader:Lcom/squareup/shared/pricing/engine/search/SearchLoader;

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->timePeriodVerificationCache:Ljava/util/Map;

    invoke-virtual {p6, p1, p3, v0}, Lcom/squareup/shared/pricing/engine/search/SearchLoader;->load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Lcom/squareup/shared/pricing/engine/rules/RuleSet;Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/SearchBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    iget-object v6, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->blacklist:Ljava/util/Map;

    move-object v2, p4

    move-object v3, p2

    move-object v4, p5

    .line 133
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;->build(Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/List;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/logging/Clock;Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Search;

    move-result-object p1

    .line 134
    invoke-virtual {p4}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->stopLoader()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 136
    invoke-virtual {p4}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->startSearch()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 137
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/Search;->bestApplications()Ljava/util/List;

    move-result-object p3

    .line 138
    invoke-virtual {p4}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->stopSearch()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 140
    invoke-direct {p0, p3}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->getLineItemApplications(Ljava/util/List;)Ljava/util/List;

    move-result-object p4

    .line 142
    invoke-direct {p0, p2, p3}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->getWholePurchaseApplications(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    .line 144
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/Search;->breakCycles()V

    .line 146
    new-instance p1, Lcom/squareup/shared/pricing/engine/PricingEngineResult;

    invoke-direct {p1, p2, p4}, Lcom/squareup/shared/pricing/engine/PricingEngineResult;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object p1
.end method

.method public static newPricingEngine(Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;)Lcom/squareup/shared/pricing/engine/PricingEngineImpl;
    .locals 3

    .line 101
    new-instance v0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;

    new-instance v1, Lcom/squareup/shared/pricing/engine/rules/RuleSetLoaderImpl;

    invoke-direct {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleSetLoaderImpl;-><init>()V

    new-instance v2, Lcom/squareup/shared/pricing/engine/search/SearchLoader;

    invoke-direct {v2}, Lcom/squareup/shared/pricing/engine/search/SearchLoader;-><init>()V

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;-><init>(Lcom/squareup/shared/pricing/engine/rules/RuleSetLoader;Lcom/squareup/shared/pricing/engine/search/SearchLoader;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/shared/pricing/engine/PricingEngineResult;",
            ">;)V"
        }
    .end annotation

    .line 189
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/shared/pricing/engine/PricingEngineResult;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    .line 198
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Ljava/util/Set;ZLcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public applyRules(Ljava/util/TimeZone;Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;Ljava/util/List;Ljava/util/Set;ZLcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/shared/pricing/engine/PricingEngineResult;",
            ">;)V"
        }
    .end annotation

    .line 208
    new-instance v4, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-direct {v4, v0}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;-><init>(Lcom/squareup/shared/catalog/logging/Clock;)V

    .line 209
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->startClock()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 211
    new-instance v0, Lcom/squareup/shared/pricing/engine/PricingEngineResult;

    .line 212
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/shared/pricing/engine/PricingEngineResult;-><init>(Ljava/util/List;Ljava/util/List;)V

    if-eqz p3, :cond_1

    .line 217
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 222
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getUnitPrice()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    .line 223
    invoke-virtual {v4, v0}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->setCurrency(Ljava/util/Currency;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 225
    invoke-virtual {v4, p3}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->recordItemizations(Ljava/util/List;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 227
    new-instance v7, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v5, p1

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$1;-><init>(Lcom/squareup/shared/pricing/engine/PricingEngineImpl;Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/TimeZone;Z)V

    invoke-interface {p2, v7, p6}, Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;->execute(Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 218
    :cond_1
    :goto_0
    new-instance p1, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$0;

    invoke-direct {p1, v0}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$0;-><init>(Lcom/squareup/shared/pricing/engine/PricingEngineResult;)V

    invoke-interface {p6, p1}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method

.method public blacklist(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 242
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->blacklist:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->blacklist:Ljava/util/Map;

    new-instance v1, Ljava/util/TreeSet;

    sget-object v2, Lcom/squareup/shared/pricing/engine/PricingEngineImpl$$Lambda$2;->$instance:Ljava/util/Comparator;

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    :cond_0
    new-instance v0, Lcom/squareup/shared/pricing/models/ClientServerIds;

    invoke-direct {v0, p2, p3}, Lcom/squareup/shared/pricing/models/ClientServerIds;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object p2, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->blacklist:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearBlacklist()V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->blacklist:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method final synthetic lambda$applyRules$1$PricingEngineImpl(Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/TimeZone;ZLcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;)Lcom/squareup/shared/pricing/engine/PricingEngineResult;
    .locals 7

    move-object v0, p0

    move-object v1, p6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    .line 229
    invoke-direct/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->loadRulesAndSearch(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Ljava/util/List;Ljava/util/Set;Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;Ljava/util/TimeZone;Z)Lcom/squareup/shared/pricing/engine/PricingEngineResult;

    move-result-object p1

    .line 232
    invoke-virtual {p3}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->stopClock()Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;

    .line 234
    iget-object p2, p0, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->analytics:Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;

    if-eqz p2, :cond_0

    .line 235
    invoke-virtual {p3}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;->commit()Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;->onRuleSearch(Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;)V

    :cond_0
    return-object p1
.end method
