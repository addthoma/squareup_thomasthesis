.class public Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;
.super Lcom/squareup/shared/pricing/engine/search/JunctionSet;
.source "DisjunctiveSet.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    return-void
.end method


# virtual methods
.method public getExhaustGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;>;"
        }
    .end annotation

    move-object v0, p0

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 61
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 62
    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/shared/pricing/engine/search/Provider;

    .line 63
    iget-object v9, v0, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;->rangeMax:Ljava/math/BigDecimal;

    move-object v6, p1

    move-object v7, p2

    move-object/from16 v8, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    invoke-interface/range {v5 .. v12}, Lcom/squareup/shared/pricing/engine/search/Provider;->exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    move-object v4, p1

    move-object/from16 v5, p4

    .line 65
    invoke-static {p1, v5}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->exhaustComparator(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 67
    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;->rangeMax:Ljava/math/BigDecimal;

    move-object/from16 v4, p3

    invoke-static {v1, v2, v4, v3}, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;->populateGroup(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/math/BigDecimal;)Ljava/util/Map;

    .line 69
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 70
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method

.method public getSatisfyGroups(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;>;"
        }
    .end annotation

    move-object v0, p0

    .line 31
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 36
    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/shared/pricing/engine/search/Provider;

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    .line 37
    invoke-interface/range {v5 .. v11}, Lcom/squareup/shared/pricing/engine/search/Provider;->satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    move-object v4, p1

    move-object/from16 v5, p4

    .line 39
    invoke-static {p1, v5}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->satisfyComparator(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 41
    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;->rangeMin:Ljava/math/BigDecimal;

    move-object v4, p3

    invoke-static {v1, v2, p3, v3}, Lcom/squareup/shared/pricing/engine/search/DisjunctiveSet;->populateGroup(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/math/BigDecimal;)Ljava/util/Map;

    .line 43
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 44
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method
