.class public Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper;
.super Ljava/lang/Object;
.source "CatalogWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade;


# instance fields
.field private final catalog:Lcom/squareup/shared/catalog/Catalog;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/Catalog;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper;->catalog:Lcom/squareup/shared/catalog/Catalog;

    return-void
.end method

.method static final synthetic lambda$execute$0$CatalogWrapper(Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;

    invoke-direct {v0, p1}, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;-><init>(Lcom/squareup/shared/catalog/Catalog$Local;)V

    .line 25
    invoke-interface {p0, v0}, Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade;->perform(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public execute(Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper;->catalog:Lcom/squareup/shared/catalog/Catalog;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/Catalog;->isCloseEnqueued()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper;->catalog:Lcom/squareup/shared/catalog/Catalog;

    new-instance v1, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper$$Lambda$0;

    invoke-direct {v1, p1}, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper$$Lambda$0;-><init>(Lcom/squareup/shared/pricing/engine/catalog/CatalogTaskFacade;)V

    invoke-interface {v0, v1, p2}, Lcom/squareup/shared/catalog/Catalog;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public getCatalog()Lcom/squareup/shared/catalog/Catalog;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogWrapper;->catalog:Lcom/squareup/shared/catalog/Catalog;

    return-object v0
.end method
