.class public interface abstract Lcom/squareup/shared/pricing/engine/search/Candidate;
.super Ljava/lang/Object;
.source "Candidate.java"


# virtual methods
.method public abstract alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z
.end method

.method public abstract canSplit()Z
.end method

.method public abstract filter(Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Candidate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;"
        }
    .end annotation
.end method

.method public abstract getApplications()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCaps()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFractional()Z
.end method

.method public abstract getMaxUnitPrice()J
.end method

.method public abstract getMinUnitPrice()J
.end method

.method public abstract getOffsets()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPreMatched()Z
.end method

.method public abstract getQuantity()Ljava/math/BigDecimal;
.end method

.method public abstract getTiebreakers()Ljava/lang/String;
.end method

.method public abstract getUnitQuantity()Ljava/math/BigDecimal;
.end method

.method public abstract getUnitValue()J
.end method

.method public abstract take(Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/search/Candidate;
.end method
