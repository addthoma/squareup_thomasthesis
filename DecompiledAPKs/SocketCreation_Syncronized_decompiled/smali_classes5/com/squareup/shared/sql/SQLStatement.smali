.class public interface abstract Lcom/squareup/shared/sql/SQLStatement;
.super Ljava/lang/Object;
.source "SQLStatement.java"


# virtual methods
.method public abstract bindBlob([BI)V
.end method

.method public abstract bindLong(JI)V
.end method

.method public abstract bindNull(I)V
.end method

.method public abstract bindString(Ljava/lang/String;I)V
.end method

.method public abstract clearBindings()V
.end method

.method public abstract execute()V
.end method

.method public abstract executeUpdateDelete()I
.end method
