.class public interface abstract Lcom/squareup/shared/i18n/LocalizedStringBuilder;
.super Ljava/lang/Object;
.source "LocalizedStringBuilder.java"


# virtual methods
.method public abstract format()Ljava/lang/String;
.end method

.method public abstract put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
.end method

.method public abstract putInt(Ljava/lang/String;I)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
.end method
