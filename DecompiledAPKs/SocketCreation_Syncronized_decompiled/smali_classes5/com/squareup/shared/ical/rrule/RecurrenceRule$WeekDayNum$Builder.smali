.class public Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;
.super Ljava/lang/Object;
.source "RecurrenceRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private ordwk:Ljava/lang/Integer;

.field private weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;
    .locals 3

    .line 116
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->ordwk:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-direct {v0, v1, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;-><init>(Ljava/lang/Integer;Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)V

    return-object v0
.end method

.method public setOrdwk(Ljava/lang/Integer;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->ordwk:Ljava/lang/Integer;

    return-object p0
.end method

.method public setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->weekDay:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-object p0
.end method
