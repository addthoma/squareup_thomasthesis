.class Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;
.super Ljava/lang/Object;
.source "RecurrenceRuleIterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NegativeWatchSet"
.end annotation


# instance fields
.field private final calendarField:I

.field lastWatchedValue:I

.field private final materializedValues:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final negativeValues:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;

.field private final watchField:I


# direct methods
.method constructor <init>(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;ILjava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 680
    iput-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->this$0:Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->materializedValues:Ljava/util/Set;

    const/4 v0, -0x1

    .line 678
    iput v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->lastWatchedValue:I

    .line 681
    iput p2, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->calendarField:I

    .line 682
    iput-object p3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->negativeValues:Ljava/util/Set;

    .line 683
    invoke-static {p1, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;->access$200(Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator;I)I

    move-result p1

    iput p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->watchField:I

    return-void
.end method


# virtual methods
.method getMaterializedValues(Ljava/util/Calendar;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 688
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->watchField:I

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 689
    iget v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->lastWatchedValue:I

    if-eq v0, v1, :cond_2

    .line 690
    iget-object v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->materializedValues:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 691
    iget v1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->calendarField:I

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result p1

    int-to-long v1, p1

    .line 693
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->negativeValues:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 694
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v3, v1

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_0

    .line 696
    iget-object v5, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->materializedValues:Ljava/util/Set;

    long-to-int v4, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 700
    :cond_1
    iput v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->lastWatchedValue:I

    .line 703
    :cond_2
    iget-object p1, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleIterator$NegativeWatchSet;->materializedValues:Ljava/util/Set;

    return-object p1
.end method
