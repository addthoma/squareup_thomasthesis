.class public final enum Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;
.super Ljava/lang/Enum;
.source "RecurrenceRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Frequency"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

.field public static final enum DAILY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

.field public static final enum MONTHLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

.field public static final enum WEEKLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

.field public static final enum YEARLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;


# instance fields
.field private final calendarField:I

.field private final ordinal:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 12
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x3

    const-string v4, "DAILY"

    invoke-direct {v0, v4, v2, v1, v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->DAILY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    .line 13
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    const/4 v4, 0x4

    const/4 v5, 0x1

    const-string v6, "WEEKLY"

    invoke-direct {v0, v6, v5, v3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->WEEKLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    .line 14
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    const/4 v6, 0x2

    const-string v7, "MONTHLY"

    invoke-direct {v0, v7, v6, v6, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->MONTHLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    .line 15
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    const-string v1, "YEARLY"

    const/4 v7, 0x6

    invoke-direct {v0, v1, v3, v5, v7}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->YEARLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    new-array v0, v4, [Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    .line 11
    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->DAILY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->WEEKLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->MONTHLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->YEARLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->$VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->calendarField:I

    .line 21
    iput p4, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->ordinal:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->$VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    invoke-virtual {v0}, [Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    return-object v0
.end method


# virtual methods
.method public getCalendarField()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->calendarField:I

    return v0
.end method

.method public getOrdinal()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->ordinal:I

    return v0
.end method
