.class public Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;
.super Ljava/lang/Object;
.source "RecurrenceRuleConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RecurrenceRuleFormatError;,
        Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;
    }
.end annotation


# static fields
.field private static final DATE_FORMATTER:Ljava/lang/String; = "yyyyMMdd"

.field private static final DATE_TIME_FORMATTER:Ljava/lang/String; = "yyyyMMdd\'T\'HHmmss\'Z\'"

.field private static final DURATION_FACTORS:[I

.field private static final DURATION_SEPARATORS:[C

.field private static final LOCAL_DATE_TIME_FORMATTER:Ljava/lang/String; = "yyyyMMdd\'T\'HHmmss"

.field private static final NUM_RE:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "^[+-]?[0-9]+"

    .line 21
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->NUM_RE:Ljava/util/regex/Pattern;

    const-string v0, "PWDTHMS"

    .line 22
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->DURATION_SEPARATORS:[C

    const/4 v0, 0x7

    new-array v0, v0, [I

    .line 23
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->DURATION_FACTORS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x7
        0x18
        0x1
        0x3c
        0x3c
        0x3e8
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseDateTime(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/util/Date;
    .locals 2

    const/16 v0, 0x5a

    .line 120
    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 121
    new-instance p1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v0, "yyyyMMdd\'T\'HHmmss\'Z\'"

    invoke-direct {p1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const-string v0, "UTC"

    .line 122
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    goto :goto_1

    :cond_0
    if-eqz p1, :cond_2

    const/16 v0, 0x54

    .line 128
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 129
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyyMMdd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyyMMdd\'T\'HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 133
    :goto_0
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    move-object p1, v0

    .line 135
    :goto_1
    invoke-virtual {p1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0

    .line 125
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "allDayEventTimezone must be supplied when parsing datetimes without a timezone."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :catch_0
    new-instance p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RecurrenceRuleFormatError;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string p0, "Illegal DATE-TIME value: %s"

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RecurrenceRuleFormatError;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static parseDuration(Ljava/lang/String;)J
    .locals 8

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    move-wide v3, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 150
    :goto_0
    sget-object v5, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->DURATION_SEPARATORS:[C

    array-length v6, v5

    if-ge v1, v6, :cond_2

    .line 151
    aget-char v5, v5, v1

    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    if-le v5, v6, :cond_0

    .line 153
    invoke-virtual {p0, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v3, v6

    :cond_0
    const/4 v6, -0x1

    if-le v5, v6, :cond_1

    move v2, v5

    .line 158
    :cond_1
    sget-object v5, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->DURATION_FACTORS:[I

    aget v5, v5, v1

    int-to-long v5, v5

    mul-long v3, v3, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 v0, 0x2d

    if-ne p0, v0, :cond_3

    const-wide/16 v0, -0x1

    mul-long v3, v3, v0

    :cond_3
    return-wide v3
.end method

.method private parseIntList(Ljava/lang/String;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, ","

    .line 197
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 198
    new-instance v0, Ljava/util/HashSet;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 199
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 200
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private parseWeekDayNum(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;
    .locals 3

    .line 176
    sget-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->NUM_RE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 177
    invoke-static {}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->newBuilder()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    move-result-object v1

    .line 179
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 180
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 181
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setOrdwk(Ljava/lang/Integer;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    .line 182
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 185
    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 187
    invoke-direct {p0, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseWeekday(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->setWeekDay(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;

    .line 189
    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object p1

    return-object p1
.end method

.method private parseWeekDayNumList(Ljava/lang/String;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation

    const-string v0, ","

    .line 167
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 168
    new-instance v0, Ljava/util/HashSet;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 169
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 170
    invoke-direct {p0, v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseWeekDayNum(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private parseWeekday(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;
    .locals 0

    .line 193
    invoke-static {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public parseRecurrenceRule(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;
    .locals 9

    .line 43
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;-><init>()V

    .line 44
    invoke-virtual {v0, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setTimeZone(Ljava/util/TimeZone;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0, p3}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setDtStart(Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    move-result-object p3

    .line 47
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 49
    new-instance v0, Ljava/util/HashSet;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 51
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, p1, v3

    const/4 v5, 0x2

    const-string v6, "="

    .line 53
    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v6

    .line 54
    array-length v7, v6

    const/4 v8, 0x1

    if-ne v7, v5, :cond_1

    .line 59
    aget-object v4, v6, v2

    .line 60
    aget-object v5, v6, v8

    .line 62
    invoke-static {v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    move-result-object v4

    .line 64
    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 69
    sget-object v6, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$1;->$SwitchMap$com$squareup$shared$ical$rrule$RecurrenceRuleConverter$RulePartLabel:[I

    invoke-virtual {v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 102
    :pswitch_0
    invoke-direct {p0, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseWeekday(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setWkst(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 99
    :pswitch_1
    invoke-direct {p0, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseIntList(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setBySetPos(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 96
    :pswitch_2
    invoke-direct {p0, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseIntList(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setByMonth(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 93
    :pswitch_3
    invoke-direct {p0, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseIntList(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setByWeekNo(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 90
    :pswitch_4
    invoke-direct {p0, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseIntList(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setByYearDay(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 87
    :pswitch_5
    invoke-direct {p0, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseIntList(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setByMonthDay(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 84
    :pswitch_6
    invoke-direct {p0, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseWeekDayNumList(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setByDay(Ljava/util/Set;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 81
    :pswitch_7
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setInterval(I)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 78
    :pswitch_8
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setCount(Ljava/lang/Integer;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 74
    :pswitch_9
    invoke-static {v5, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;->parseDateTime(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v4

    .line 75
    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setUntil(Ljava/util/Date;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    goto :goto_1

    .line 71
    :pswitch_a
    invoke-static {v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->setFrequency(Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 65
    :cond_0
    new-instance p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RecurrenceRuleFormatError;

    new-array p2, v8, [Ljava/lang/Object;

    aput-object v4, p2, v2

    const-string p3, "Seen rule label more than once: %s"

    invoke-static {p3, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RecurrenceRuleFormatError;-><init>(Ljava/lang/String;)V

    throw p1

    .line 55
    :cond_1
    new-instance p1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RecurrenceRuleFormatError;

    new-array p2, v8, [Ljava/lang/Object;

    aput-object v4, p2, v2

    const-string p3, "Could not split key/value pair: %s"

    invoke-static {p3, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RecurrenceRuleFormatError;-><init>(Ljava/lang/String;)V

    throw p1

    .line 107
    :cond_2
    invoke-virtual {p3}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl$Builder;->build()Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
