.class public final enum Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;
.super Ljava/lang/Enum;
.source "RecurrenceRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WeekDay"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

.field public static final enum FR:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

.field public static final enum MO:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

.field public static final enum SA:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

.field public static final enum SU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

.field public static final enum TH:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

.field public static final enum TU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

.field public static final enum WE:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;


# instance fields
.field private final dayValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 41
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SU"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 42
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v3, 0x2

    const-string v4, "MO"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->MO:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 43
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v4, 0x3

    const-string v5, "TU"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->TU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 44
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v5, 0x4

    const-string v6, "WE"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->WE:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 45
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v6, 0x5

    const-string v7, "TH"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->TH:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 46
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v7, 0x6

    const-string v8, "FR"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->FR:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 47
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    const/4 v8, 0x7

    const-string v9, "SA"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SA:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    new-array v0, v8, [Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 40
    sget-object v8, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->MO:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->TU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->WE:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->TH:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->FR:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SA:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->$VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->dayValue:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;)I
    .locals 0

    .line 40
    iget p0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->dayValue:I

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;
    .locals 1

    .line 40
    const-class v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->$VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v0}, [Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    return-object v0
.end method


# virtual methods
.method public getDayValue()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->dayValue:I

    return v0
.end method
