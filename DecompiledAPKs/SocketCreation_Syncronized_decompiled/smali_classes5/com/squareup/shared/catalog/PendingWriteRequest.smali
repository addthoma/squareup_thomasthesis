.class public Lcom/squareup/shared/catalog/PendingWriteRequest;
.super Ljava/lang/Object;
.source "PendingWriteRequest.java"


# instance fields
.field public final id:J

.field public final request:Lcom/squareup/api/rpc/Request;


# direct methods
.method public constructor <init>(JLcom/squareup/api/rpc/Request;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/squareup/shared/catalog/PendingWriteRequest;->id:J

    .line 18
    iput-object p3, p0, Lcom/squareup/shared/catalog/PendingWriteRequest;->request:Lcom/squareup/api/rpc/Request;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 22
    instance-of v1, p1, Lcom/squareup/shared/catalog/PendingWriteRequest;

    if-nez v1, :cond_0

    goto :goto_0

    .line 23
    :cond_0
    check-cast p1, Lcom/squareup/shared/catalog/PendingWriteRequest;

    .line 24
    iget-wide v1, p0, Lcom/squareup/shared/catalog/PendingWriteRequest;->id:J

    iget-wide v3, p1, Lcom/squareup/shared/catalog/PendingWriteRequest;->id:J

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    iget-object v1, p0, Lcom/squareup/shared/catalog/PendingWriteRequest;->request:Lcom/squareup/api/rpc/Request;

    iget-object p1, p1, Lcom/squareup/shared/catalog/PendingWriteRequest;->request:Lcom/squareup/api/rpc/Request;

    invoke-virtual {v1, p1}, Lcom/squareup/api/rpc/Request;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 28
    iget-wide v1, p0, Lcom/squareup/shared/catalog/PendingWriteRequest;->id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/shared/catalog/PendingWriteRequest;->request:Lcom/squareup/api/rpc/Request;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/shared/catalog/PendingWriteRequest;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
