.class Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;
.super Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$LocalSyncCallback;
.source "CatalogSyncHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->lambda$run$4$CatalogSyncHandler$ResponseHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V
    .locals 1

    .line 187
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$LocalSyncCallback;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 190
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    .line 191
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$400(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;

    move-result-object p1

    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2$$Lambda$0;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method final synthetic lambda$call$0$CatalogSyncHandler$ResponseHandler$2()V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$500(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->access$600(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncSucceed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method
