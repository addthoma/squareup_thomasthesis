.class synthetic Lcom/squareup/shared/catalog/sync/SyncError$1;
.super Ljava/lang/Object;
.source "SyncError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/SyncError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$api$rpc$Error$ServerErrorCode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 44
    invoke-static {}, Lcom/squareup/api/rpc/Error$ServerErrorCode;->values()[Lcom/squareup/api/rpc/Error$ServerErrorCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$1;->$SwitchMap$com$squareup$api$rpc$Error$ServerErrorCode:[I

    :try_start_0
    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncError$1;->$SwitchMap$com$squareup$api$rpc$Error$ServerErrorCode:[I

    sget-object v1, Lcom/squareup/api/rpc/Error$ServerErrorCode;->INTERNAL_SERVER_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-virtual {v1}, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncError$1;->$SwitchMap$com$squareup$api$rpc$Error$ServerErrorCode:[I

    sget-object v1, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_METHOD_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-virtual {v1}, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncError$1;->$SwitchMap$com$squareup$api$rpc$Error$ServerErrorCode:[I

    sget-object v1, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_SERVICE_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-virtual {v1}, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
