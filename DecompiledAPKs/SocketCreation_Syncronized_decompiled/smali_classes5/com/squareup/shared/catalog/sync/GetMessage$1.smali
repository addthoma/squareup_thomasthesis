.class Lcom/squareup/shared/catalog/sync/GetMessage$1;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "GetMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/GetMessage;->onComplete(ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/GetMessage;Z)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$1;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    iput-boolean p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$1;->val$success:Z

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 184
    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$3;->$SwitchMap$com$squareup$shared$catalog$sync$GetMessage$MessageStatus:[I

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$1;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$200(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p1, 0x2

    if-eq v0, p1, :cond_2

    const/4 p1, 0x3

    if-ne v0, p1, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onComplete() must not be called when status is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$1;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    .line 195
    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$200(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$1;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$300(Lcom/squareup/shared/catalog/sync/GetMessage;)J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$1;->val$success:Z

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->endVersionSync(JZ)V

    .line 187
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$1;->this$0:Lcom/squareup/shared/catalog/sync/GetMessage;

    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->COMPLETE:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/sync/GetMessage;->access$202(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    .line 197
    :cond_2
    :goto_0
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method public shouldUpdateLastKnownServerVersion()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
