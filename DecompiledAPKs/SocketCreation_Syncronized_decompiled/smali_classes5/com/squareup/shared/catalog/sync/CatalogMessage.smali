.class public abstract Lcom/squareup/shared/catalog/sync/CatalogMessage;
.super Ljava/lang/Object;
.source "CatalogMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;
    }
.end annotation


# instance fields
.field private final request:Lcom/squareup/api/rpc/Request;


# direct methods
.method public constructor <init>(Lcom/squareup/api/rpc/Request;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogMessage;->request:Lcom/squareup/api/rpc/Request;

    return-void
.end method


# virtual methods
.method public getRequestId()Ljava/lang/Long;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogMessage;->request:Lcom/squareup/api/rpc/Request;

    iget-object v0, v0, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public onComplete(ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/Response;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method

.method public startRequest()Lcom/squareup/api/rpc/Request;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogMessage;->request:Lcom/squareup/api/rpc/Request;

    return-object v0
.end method
