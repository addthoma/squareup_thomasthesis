.class Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;
.super Lcom/squareup/shared/catalog/utils/PhraseLite$Token;
.source "PhraseLite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/PhraseLite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TextToken"
.end annotation


# instance fields
.field private final textLength:I


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;I)V
    .locals 0

    .line 277
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;-><init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)V

    .line 278
    iput p2, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;->textLength:I

    return-void
.end method


# virtual methods
.method expand(Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method getFormattedLength()I
    .locals 1

    .line 286
    iget v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;->textLength:I

    return v0
.end method
