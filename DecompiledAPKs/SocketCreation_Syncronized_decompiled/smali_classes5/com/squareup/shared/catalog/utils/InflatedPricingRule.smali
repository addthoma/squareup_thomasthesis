.class public Lcom/squareup/shared/catalog/utils/InflatedPricingRule;
.super Ljava/lang/Object;
.source "InflatedPricingRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;,
        Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;
    }
.end annotation


# instance fields
.field private final applyProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

.field private final applyProductSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation
.end field

.field private final excludeProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

.field private final excludeProductSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation
.end field

.field private final filteredMatchProductSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation
.end field

.field private localizedDescription:Ljava/lang/String;

.field private final matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

.field private final matchProductSets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation
.end field

.field private final periods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTimePeriod;",
            ">;"
        }
    .end annotation
.end field

.field private final pricingRule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

.field private final timeZone:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogPricingRule;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogProductSet;Lcom/squareup/shared/catalog/models/CatalogProductSet;Lcom/squareup/shared/catalog/models/CatalogProductSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/TimeZone;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogPricingRule;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTimePeriod;",
            ">;",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;",
            "Ljava/util/TimeZone;",
            ")V"
        }
    .end annotation

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 98
    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->localizedDescription:Ljava/lang/String;

    .line 105
    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->pricingRule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 106
    iput-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->periods:Ljava/util/List;

    .line 107
    iput-object p9, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    .line 108
    iput-object p3, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 109
    iput-object p6, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    .line 110
    iput-object p4, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->applyProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 111
    iput-object p7, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->applyProductSets:Ljava/util/List;

    .line 112
    iput-object p5, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 113
    iput-object p8, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    .line 114
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFilteredMatchProductSets()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->filteredMatchProductSets:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Lcom/squareup/shared/catalog/models/CatalogPricingRule;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->pricingRule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    return-object p0
.end method

.method private anyProductSets(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 575
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectId;

    .line 576
    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    sget-object v2, Lcom/squareup/api/items/Type;->PRODUCT_SET:Lcom/squareup/api/items/Type;

    if-ne v1, v2, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    return v0
.end method

.method private cacheDescription(ZLjava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    return-object p2

    .line 460
    :cond_0
    iput-object p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->localizedDescription:Ljava/lang/String;

    .line 461
    iget-object p1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->localizedDescription:Ljava/lang/String;

    return-object p1
.end method

.method private dateRangeDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 5

    .line 365
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->pricingRule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->validFrom(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    .line 366
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    const-string v2, "start_time"

    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 368
    sget-object v1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->STARTS_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v1

    .line 369
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-interface {p1, v0, v3}, Lcom/squareup/shared/i18n/Localizer;->dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 370
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 372
    :cond_0
    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->pricingRule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    iget-object v4, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->validUntil(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_2

    if-nez v0, :cond_1

    const-string p1, ""

    return-object p1

    .line 377
    :cond_1
    sget-object v1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->STARTED_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v1

    .line 378
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-interface {p1, v0, v3}, Lcom/squareup/shared/i18n/Localizer;->dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 379
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 381
    :cond_2
    invoke-virtual {v3, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    const-string v1, "end_time"

    if-eqz v0, :cond_3

    .line 382
    sget-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ENDS_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v0}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v0

    .line 383
    invoke-static {v3}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-interface {p1, v2, v3}, Lcom/squareup/shared/i18n/Localizer;->dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 384
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 386
    :cond_3
    sget-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ENDED_AT_LABEL:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v0}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v0

    .line 387
    invoke-static {v3}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-interface {p1, v2, v3}, Lcom/squareup/shared/i18n/Localizer;->dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 388
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getFilteredMatchProductSets()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    if-nez v0, :cond_0

    goto :goto_2

    .line 150
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 151
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 152
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 155
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 156
    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 157
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 158
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object v1

    .line 147
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    return-object v0
.end method

.method private getObjectsOfTypeFromSet(Lcom/squareup/api/items/Type;Ljava/util/Collection;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 243
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    if-nez p2, :cond_0

    return-object v0

    .line 247
    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 248
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProducts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/sync/ObjectId;

    .line 249
    iget-object v3, v2, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v3, v3, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    if-ne v3, p1, :cond_2

    .line 250
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private itemsDescription(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 10

    .line 290
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->hasComplexProducts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SPECIFIED_PRODUCTS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 294
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchesAllProducts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ALL_ITEMS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 298
    :cond_1
    sget-object v0, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfType(Lcom/squareup/api/items/Type;)Ljava/util/Set;

    move-result-object v0

    .line 299
    sget-object v1, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    invoke-virtual {p0, v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfType(Lcom/squareup/api/items/Type;)Ljava/util/Set;

    move-result-object v1

    .line 300
    sget-object v2, Lcom/squareup/api/items/Type;->ITEM_VARIATION:Lcom/squareup/api/items/Type;

    invoke-virtual {p0, v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfType(Lcom/squareup/api/items/Type;)Ljava/util/Set;

    move-result-object v2

    const/4 v3, 0x3

    new-array v4, v3, [Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    .line 302
    sget-object v5, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    const/4 v6, 0x1

    .line 303
    invoke-static {v6, v5}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v5

    const/4 v7, 0x0

    aput-object v5, v4, v7

    sget-object v5, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_TWO:Lcom/squareup/shared/i18n/Key;

    const/4 v8, 0x2

    .line 304
    invoke-static {v8, v5}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v5

    aput-object v5, v4, v6

    sget-object v5, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_THREE:Lcom/squareup/shared/i18n/Key;

    .line 305
    invoke-static {v3, v5}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v5

    aput-object v5, v4, v8

    .line 302
    invoke-static {p2, v4}, Lcom/squareup/shared/i18n/LocalizedListHelper;->list(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    move-result-object v4

    .line 307
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    if-nez p1, :cond_3

    .line 310
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result p1

    if-ne p1, v6, :cond_2

    .line 311
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->CATEGORY_ONE:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_0

    .line 313
    :cond_2
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->CATEGORIES_MANY:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const-string v3, "num_categories"

    invoke-interface {p1, v3, v0}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->putInt(Ljava/lang/String;I)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 315
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    goto :goto_3

    :cond_3
    new-array v5, v3, [Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    .line 317
    sget-object v9, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    .line 318
    invoke-static {v6, v9}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v9

    aput-object v9, v5, v7

    sget-object v7, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_TWO:Lcom/squareup/shared/i18n/Key;

    .line 319
    invoke-static {v8, v7}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v7

    aput-object v7, v5, v6

    sget-object v7, Lcom/squareup/shared/catalog/CatalogKeyManifest;->CATEGORIES_MANY:Lcom/squareup/shared/i18n/Key;

    .line 320
    invoke-static {v3, v7}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->orMore(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v3

    aput-object v3, v5, v8

    .line 317
    invoke-static {p2, v5}, Lcom/squareup/shared/i18n/LocalizedListHelper;->list(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    move-result-object v3

    .line 321
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 322
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/api/sync/ObjectId;

    .line 323
    iget-object v7, v7, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v5, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 325
    :cond_4
    invoke-interface {p1, v5}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 326
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 327
    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 328
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    goto :goto_2

    .line 330
    :cond_5
    invoke-virtual {v3}, Lcom/squareup/shared/i18n/LocalizedListHelper;->format()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    .line 334
    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_8

    .line 336
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result p1

    if-ne p1, v6, :cond_7

    .line 337
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ITEM_ONE:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_4

    .line 339
    :cond_7
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->ITEMS_PLURAL:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    const-string v1, "num_items"

    invoke-interface {p1, v1, v0}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->putInt(Ljava/lang/String;I)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 341
    :goto_4
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    .line 344
    :cond_8
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_a

    .line 346
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result p1

    if-ne p1, v6, :cond_9

    .line 347
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->VARIATION_ONE:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_5

    .line 349
    :cond_9
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->VARIATIONS_PLURAL:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result p2

    const-string v0, "num_variations"

    invoke-interface {p1, v0, p2}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->putInt(Ljava/lang/String;I)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 351
    :goto_5
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    .line 353
    :cond_a
    invoke-virtual {v4}, Lcom/squareup/shared/i18n/LocalizedListHelper;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static quantityLocalizedStringBuilder(Lcom/squareup/shared/i18n/Localizer;Ljava/math/BigDecimal;Lcom/squareup/shared/i18n/Key;Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
    .locals 1

    .line 665
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 666
    invoke-interface {p0, p2}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p0

    return-object p0

    .line 668
    :cond_0
    invoke-interface {p0, p3}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p0

    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "num_items"

    invoke-interface {p0, p2, p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public activeAt()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;",
            ">;"
        }
    .end annotation

    .line 217
    new-instance v0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-direct {v0, v1, p0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$TimePeriodSetIterator;-><init>(Ljava/util/TimeZone;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)V

    return-object v0
.end method

.method public applyDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 4

    .line 624
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 627
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->filteredMatchProductSets:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 628
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->filteredMatchProductSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 629
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isExact()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 632
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityExact()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

    .line 630
    invoke-static {p1, v0, v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->quantityLocalizedStringBuilder(Lcom/squareup/shared/i18n/Localizer;Ljava/math/BigDecimal;Lcom/squareup/shared/i18n/Key;Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_0

    .line 635
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isRange()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 636
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMin()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMax()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 639
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMax()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

    .line 637
    invoke-static {p1, v0, v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->quantityLocalizedStringBuilder(Lcom/squareup/shared/i18n/Localizer;Ljava/math/BigDecimal;Lcom/squareup/shared/i18n/Key;Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_0

    .line 643
    :cond_1
    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS_RANGE:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v2}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 644
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMin()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "min_num_items"

    invoke-interface {p1, v3, v2}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 645
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMax()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "max_num_items"

    invoke-interface {p1, v2, v0}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_0

    .line 649
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 651
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 654
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isGreedy()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getAllProducts()Z

    move-result v0

    if-nez v0, :cond_3

    .line 655
    sget-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->APPLY_ITEMS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v0}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_0

    :cond_3
    move-object p1, v1

    :goto_0
    if-nez p1, :cond_4

    return-object v1

    .line 661
    :cond_4
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public endsAt()Ljava/util/Date;
    .locals 5

    .line 195
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->periods:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 196
    iget-object v4, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->endsAt(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    if-eqz v2, :cond_2

    .line 201
    invoke-virtual {v3, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    move-object v2, v3

    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 205
    :goto_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->pricingRule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->validUntil(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_5

    if-eqz v1, :cond_4

    .line 206
    invoke-virtual {v1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    return-object v0

    :cond_5
    return-object v1
.end method

.method public getApplyProductSet()Lcom/squareup/shared/catalog/models/CatalogProductSet;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->applyProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    return-object v0
.end method

.method public getExcludeObjectsOfType(Lcom/squareup/api/items/Type;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 239
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfTypeFromSet(Lcom/squareup/api/items/Type;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public getExcludeProductSet()Lcom/squareup/shared/catalog/models/CatalogProductSet;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    return-object v0
.end method

.method public getFlattenedExcludeProducts()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 276
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedProductsFromSet(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getFlattenedMatchProducts()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 261
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedProductsFromSet(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getFlattenedMatchProductsWithoutExcludes()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 269
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->filteredMatchProductSets:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedProductsFromSet(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getFlattenedProductsFromSet(Ljava/util/Collection;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 283
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfTypeFromSet(Lcom/squareup/api/items/Type;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    .line 284
    sget-object v1, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    invoke-direct {p0, v1, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfTypeFromSet(Lcom/squareup/api/items/Type;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 285
    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_VARIATION:Lcom/squareup/api/items/Type;

    invoke-direct {p0, v1, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfTypeFromSet(Lcom/squareup/api/items/Type;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public getMatchProductSet()Lcom/squareup/shared/catalog/models/CatalogProductSet;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    return-object v0
.end method

.method public getObjectsOfType(Lcom/squareup/api/items/Type;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 224
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfTypeFromSet(Lcom/squareup/api/items/Type;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public getObjectsOfTypeWithoutExcludes(Lcom/squareup/api/items/Type;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 232
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->filteredMatchProductSets:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getObjectsOfTypeFromSet(Lcom/squareup/api/items/Type;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public getPeriods()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogTimePeriod;",
            ">;"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->periods:Ljava/util/List;

    return-object v0
.end method

.method public hasComplexProducts()Z
    .locals 5

    .line 531
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->applyProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    if-nez v0, :cond_1

    return v1

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->applyProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    return v2

    .line 548
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->hasProductsAll()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 549
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProductsAll()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 550
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityExact()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSet:Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 551
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityExact()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v4, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v4}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 556
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v3, :cond_4

    return v2

    .line 560
    :cond_4
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 561
    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 564
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProducts()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->anyProductSets(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getProducts()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->anyProductSets(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_0

    :cond_5
    return v1

    :cond_6
    :goto_0
    return v2
.end method

.method public hasComplexSchedule()Z
    .locals 5

    .line 478
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->periods:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 479
    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->recurrenceRule(Ljava/util/TimeZone;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 485
    :cond_1
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getFrequency()Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    move-result-object v2

    .line 486
    sget-object v3, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->DAILY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    const/4 v4, 0x1

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->WEEKLY:Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    if-eq v2, v3, :cond_2

    return v4

    .line 491
    :cond_2
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getWkst()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v2

    .line 492
    sget-object v3, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->MO:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    if-eq v2, v3, :cond_3

    return v4

    .line 496
    :cond_3
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getInterval()I

    move-result v2

    if-le v2, v4, :cond_4

    return v4

    .line 501
    :cond_4
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getCount()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 502
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_5

    return v4

    .line 507
    :cond_5
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonthDay()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonthDay()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    return v4

    .line 510
    :cond_6
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByYearDay()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByYearDay()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    return v4

    .line 513
    :cond_7
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByWeekNo()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByWeekNo()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    return v4

    .line 516
    :cond_8
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonth()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonth()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    return v4

    .line 519
    :cond_9
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getBySetPos()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getBySetPos()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    return v4

    .line 522
    :cond_a
    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getExDates()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getExDates()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    return v4

    :cond_b
    const/4 v0, 0x0

    return v0
.end method

.method public localizedDescription(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 7

    .line 403
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->localizedDescription:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 410
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->hasComplexSchedule()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 412
    sget-object v1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SPECIFIED_TIMES:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, v1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 414
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->schedule()Lcom/squareup/shared/catalog/utils/WeeklySchedule;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->toLocalizedString(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v1

    .line 416
    :goto_1
    invoke-direct {p0, p2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->dateRangeDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v2

    .line 417
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->itemsDescription(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object p1

    .line 419
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v2}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string p1, ""

    .line 420
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 423
    :cond_3
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, "date_range"

    const-string v5, "schedules"

    if-nez v3, :cond_7

    .line 424
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v6, "items_description"

    if-nez v3, :cond_4

    invoke-static {v2}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 425
    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->PRICING_RULE_DESCRIPTION_LONG:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, v3}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p2

    .line 426
    invoke-interface {p2, v5, v1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p2

    .line 427
    invoke-interface {p2, v4, v2}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p2

    .line 428
    invoke-interface {p2, v6, p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 429
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    .line 425
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 431
    :cond_4
    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->PRICING_RULE_DESCRIPTION_MEDIUM_WITH_ITEMS:Lcom/squareup/shared/i18n/Key;

    .line 432
    invoke-interface {p2, v3}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p2

    .line 433
    invoke-interface {p2, v6, p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p2

    .line 434
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, "schedules_or_date_range"

    if-nez v3, :cond_5

    .line 436
    invoke-interface {p2, v4, v1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    .line 435
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 438
    :cond_5
    invoke-static {v2}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 440
    invoke-interface {p2, v4, v2}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    .line 439
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 442
    :cond_6
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 445
    :cond_7
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_8

    invoke-static {v2}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_8

    .line 446
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->PRICING_RULE_DESCRIPTION_MEDIUM_WITHOUT_ITEMS:Lcom/squareup/shared/i18n/Key;

    .line 447
    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 448
    invoke-interface {p1, v5, v1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 449
    invoke-interface {p1, v4, v2}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 450
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    .line 446
    invoke-direct {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 452
    :cond_8
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_9

    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 453
    :cond_9
    invoke-direct {p0, v0, v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->cacheDescription(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public localizedDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 395
    invoke-virtual {p0, v0, p1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->localizedDescription(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public matchDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 4

    .line 585
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->excludeProductSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 591
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityExact()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

    .line 589
    invoke-static {p1, v0, v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->quantityLocalizedStringBuilder(Lcom/squareup/shared/i18n/Localizer;Ljava/math/BigDecimal;Lcom/squareup/shared/i18n/Key;Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_2

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 596
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 597
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isGreedy()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getAllProducts()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 600
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isExact()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 603
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityExact()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_EXACT_ONE:Lcom/squareup/shared/i18n/Key;

    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_EXACT_MANY:Lcom/squareup/shared/i18n/Key;

    .line 601
    invoke-static {p1, v0, v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->quantityLocalizedStringBuilder(Lcom/squareup/shared/i18n/Localizer;Ljava/math/BigDecimal;Lcom/squareup/shared/i18n/Key;Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_2

    .line 606
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isRange()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 608
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->isMin()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 611
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getQuantityMin()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_MIN_ONE:Lcom/squareup/shared/i18n/Key;

    sget-object v3, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_MIN_MANY:Lcom/squareup/shared/i18n/Key;

    .line 609
    invoke-static {p1, v0, v2, v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->quantityLocalizedStringBuilder(Lcom/squareup/shared/i18n/Localizer;Ljava/math/BigDecimal;Lcom/squareup/shared/i18n/Key;Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_2

    .line 599
    :cond_4
    :goto_0
    sget-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->BUY_ITEMS_ANY:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v0}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    goto :goto_2

    :cond_5
    :goto_1
    move-object p1, v1

    :goto_2
    if-nez p1, :cond_6

    return-object v1

    .line 619
    :cond_6
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public matchesAllProducts()Z
    .locals 3

    .line 466
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    return v1

    .line 473
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchProductSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 474
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getAllProducts()Z

    move-result v0

    return v0

    :cond_2
    :goto_0
    return v1
.end method

.method public schedule()Lcom/squareup/shared/catalog/utils/WeeklySchedule;
    .locals 4

    .line 357
    new-instance v0, Lcom/squareup/shared/catalog/utils/WeeklySchedule;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;-><init>()V

    .line 358
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->periods:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 359
    iget-object v3, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getSchedule(Ljava/util/TimeZone;)Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/utils/WeeklySchedule;->addSchedule(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public startsAt()Ljava/util/Date;
    .locals 5

    .line 170
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->periods:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 171
    iget-object v4, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->startsAt(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_1

    goto :goto_2

    :cond_1
    if-nez v2, :cond_2

    goto :goto_1

    .line 178
    :cond_2
    invoke-virtual {v2, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    move-object v2, v3

    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 182
    :goto_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->pricingRule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->validFrom(Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_5

    if-eqz v1, :cond_4

    .line 183
    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    return-object v0

    :cond_5
    return-object v1
.end method
