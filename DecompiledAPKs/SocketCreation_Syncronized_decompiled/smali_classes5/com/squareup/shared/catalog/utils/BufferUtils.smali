.class public Lcom/squareup/shared/catalog/utils/BufferUtils;
.super Ljava/lang/Object;
.source "BufferUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readByteArrayWithLength(Lokio/BufferedSource;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 27
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    int-to-long v0, v0

    .line 28
    invoke-interface {p0, v0, v1}, Lokio/BufferedSource;->readByteArray(J)[B

    move-result-object p0

    return-object p0
.end method

.method public static readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    int-to-long v0, v0

    .line 18
    invoke-interface {p0, v0, v1}, Lokio/BufferedSource;->readUtf8(J)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static writeByteArrayWithLength(Lokio/BufferedSink;[B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 23
    array-length v0, p1

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    move-result-object p0

    invoke-interface {p0, p1}, Lokio/BufferedSink;->write([B)Lokio/BufferedSink;

    return-void
.end method

.method public static writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12
    invoke-static {p1}, Lokio/ByteString;->encodeUtf8(Ljava/lang/String;)Lokio/ByteString;

    move-result-object p1

    .line 13
    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    move-result-object p0

    invoke-interface {p0, p1}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    return-void
.end method
