.class public Lcom/squareup/shared/catalog/utils/Dineros;
.super Ljava/lang/Object;
.source "Dineros.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 34
    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/protos/common/dinero/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/dinero/CurrencyCode;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/squareup/protos/common/dinero/Money$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/dinero/Money$Builder;-><init>()V

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1, p0}, Lcom/squareup/protos/common/dinero/Money$Builder;->cents(Ljava/lang/Long;)Lcom/squareup/protos/common/dinero/Money$Builder;

    move-result-object p0

    .line 36
    invoke-virtual {p0, v0}, Lcom/squareup/protos/common/dinero/Money$Builder;->currency_code(Lcom/squareup/protos/common/dinero/CurrencyCode;)Lcom/squareup/protos/common/dinero/Money$Builder;

    move-result-object p0

    .line 37
    invoke-virtual {p0}, Lcom/squareup/protos/common/dinero/Money$Builder;->build()Lcom/squareup/protos/common/dinero/Money;

    move-result-object p0

    return-object p0
.end method

.method public static toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 23
    :cond_0
    invoke-static {p0}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p0

    return-object p0
.end method

.method public static toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    sget-object v1, Lcom/squareup/protos/common/dinero/Money;->DEFAULT_CURRENCY_CODE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 53
    invoke-virtual {v0}, Lcom/squareup/protos/common/dinero/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/protos/common/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    iget-object p0, p0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    sget-object v2, Lcom/squareup/protos/common/dinero/Money;->DEFAULT_CENTS:Ljava/lang/Long;

    .line 55
    invoke-static {p0, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    .line 54
    invoke-virtual {v1, p0}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p0

    .line 56
    invoke-virtual {p0, v0}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p0

    .line 57
    invoke-virtual {p0}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method
