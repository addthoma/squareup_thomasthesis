.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable;
.super Ljava/lang/Object;
.source "ItemVariationInventoryAlertConfigTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;
    }
.end annotation


# static fields
.field static final COLUMN_DELETED:Ljava/lang/String; = "deleted"

.field static final COLUMN_INVENTORY_ALERT_THRESHOLD:Ljava/lang/String; = "inventory_alert_threshold"

.field static final COLUMN_INVENTORY_ALERT_TYPE:Ljava/lang/String; = "inventory_alert_type"

.field static final COLUMN_INVENTORY_TRACKING_STATE:Ljava/lang/String; = "inventory_tracking_state"

.field static final COLUMN_ITEM_ID:Ljava/lang/String; = "item_id"

.field static final COLUMN_SERVER_TIMESTAMP:Ljava/lang/String; = "server_timestamp"

.field static final COLUMN_VARIATION_ID:Ljava/lang/String; = "variation_id"

.field static final COLUMN_VARIATION_TOKEN:Ljava/lang/String; = "variation_token"

.field static final NAME:Ljava/lang/String; = "variation_alert_config"

.field private static final VERSION:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/DeletedCatalogObjects;)V
    .locals 5

    .line 156
    iget-boolean v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->isLocalEdit:Z

    if-nez v0, :cond_2

    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 160
    :cond_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 162
    :try_start_0
    iget-object p2, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariations:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    const/4 v1, 0x1

    .line 164
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v2

    .line 165
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    const-string v3, "ObjectWrapper timestamp"

    invoke-static {v0, v3}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 167
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->UPDATE_VARIATION_DELETED:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    .line 168
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 169
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 170
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 171
    invoke-virtual {v0, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    goto :goto_0

    .line 174
    :cond_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2

    :cond_2
    :goto_1
    return-void
.end method

.method public applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V
    .locals 10

    .line 117
    iget-boolean p3, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->isLocalEdit:Z

    if-nez p3, :cond_5

    iget-object p3, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    .line 118
    invoke-interface {p3}, Ljava/util/Map;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    goto/16 :goto_3

    .line 122
    :cond_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->beginTransaction()V

    .line 124
    :try_start_0
    iget-object p2, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    .line 125
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 126
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isInventoryTracked()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    .line 130
    :goto_1
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isInventoryAlertEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    .line 131
    :goto_2
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getInventoryAlertThreshold()J

    move-result-wide v6

    .line 133
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p4

    iget-object p4, p4, Lcom/squareup/api/sync/ObjectWrapper;->timestamp:Ljava/lang/Long;

    const-string v8, "ObjectWrapper timestamp"

    invoke-static {p4, v8}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Long;

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 135
    sget-object p4, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->REPLACE_VARIATION_ALERT_CONFIG:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    .line 136
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p4

    invoke-static {p1, p4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 137
    invoke-virtual {p4, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 138
    invoke-virtual {p4, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 139
    invoke-virtual {p4, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 140
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 141
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 142
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 143
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 144
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p4

    .line 145
    invoke-virtual {p4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    goto/16 :goto_0

    .line 148
    :cond_4
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLDatabase;->endTransaction()V

    throw p2

    :cond_5
    :goto_3
    return-void
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 110
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 111
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->CREATE_TIMESTAMP_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfigTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public tableName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "variation_alert_config"

    return-object v0
.end method

.method public tableVersion()J
    .locals 2

    const-wide/16 v0, 0x1

    return-wide v0
.end method
