.class public interface abstract Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;
.super Ljava/lang/Object;
.source "SyntheticTable.java"


# virtual methods
.method public abstract applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/DeletedCatalogObjects;)V
.end method

.method public abstract applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V
.end method

.method public abstract create(Lcom/squareup/shared/sql/SQLDatabase;)V
.end method

.method public abstract tableName()Ljava/lang/String;
.end method

.method public abstract tableVersion()J
.end method
