.class public final enum Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;
.super Ljava/lang/Enum;
.source "ItemVariationLookupTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

.field public static final enum CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

.field public static final enum CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

.field public static final enum DELETE_BY_ITEM_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

.field public static final enum DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

.field public static final enum LOOKUP_SKU_FOR_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

.field public static final enum UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 42
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const-string v1, "CREATE TABLE {table} ({sku} TEXT, {item_id} TEXT, {variation_id} TEXT PRIMARY KEY, {variation_name} TEXT, {variation_price_amt} INTEGER, {variation_price_currency} INTEGER, {variation_ordinal} INTEGER)"

    .line 43
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_lookup"

    const-string v3, "table"

    .line 47
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "sku"

    .line 48
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "item_id"

    .line 49
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v6, "variation_id"

    .line 50
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v7, "variation_name"

    .line 51
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v8, "variation_price_amount"

    const-string/jumbo v9, "variation_price_amt"

    .line 52
    invoke-virtual {v1, v9, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v9, "variation_price_currency"

    .line 53
    invoke-virtual {v1, v9, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v10, "variation_ordinal"

    .line 54
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 56
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x0

    const-string v12, "CREATE"

    invoke-direct {v0, v12, v11, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    .line 58
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({item_id})"

    .line 59
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "idx"

    const-string v13, "idx_item_id"

    .line 60
    invoke-virtual {v1, v12, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 61
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 62
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 64
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v13, 0x1

    const-string v14, "CREATE_ITEM_ID_INDEX"

    invoke-direct {v0, v14, v13, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    .line 66
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({sku} COLLATE NOCASE)"

    .line 67
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v14, "idx_sku"

    .line 68
    invoke-virtual {v1, v12, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 69
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 72
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v12, 0x2

    const-string v14, "CREATE_SKU_INDEX"

    invoke-direct {v0, v14, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    .line 74
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({sku}, {item_id}, {variation_id}, {variation_name}, {price_amt}, {price_currency}, {variation_ordinal}) VALUES (?, ?, ?, ?, ?, ?, ?)"

    .line 75
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 78
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 80
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 81
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 82
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "price_amt"

    .line 83
    invoke-virtual {v1, v7, v8}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "price_currency"

    .line 84
    invoke-virtual {v1, v7, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 85
    invoke-virtual {v1, v10, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 87
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x3

    const-string v8, "UPSERT_VARIATION"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    .line 89
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const-string v1, "DELETE FROM {table} WHERE {item_id} = ?"

    .line 90
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 91
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 92
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 94
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x4

    const-string v8, "DELETE_BY_ITEM_ID"

    invoke-direct {v0, v8, v5, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->DELETE_BY_ITEM_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    .line 96
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const-string v1, "DELETE FROM {table} WHERE {variation_id} = ?"

    .line 97
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 98
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 101
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "DELETE_BY_VARIATION_ID"

    const/4 v9, 0x5

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    .line 103
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const-string v1, "SELECT {sku} FROM {table} WHERE {variation_id} = ? LIMIT 1"

    .line 104
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 105
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 106
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 107
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 108
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 109
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LOOKUP_SKU_FOR_VARIATION_ID"

    const/4 v3, 0x6

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->LOOKUP_SKU_FOR_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    .line 41
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->DELETE_BY_ITEM_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->LOOKUP_SKU_FOR_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 114
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;
    .locals 1

    .line 41
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
