.class public Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;
.super Ljava/lang/Object;
.source "PricingRuleLookupTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;
    }
.end annotation


# static fields
.field static final COLUMN_ENDS_AT_MS:Ljava/lang/String; = "ends_at"

.field static final COLUMN_PRICING_RULE_ID:Ljava/lang/String; = "pricing_rule_id"

.field static final COLUMN_STARTS_AT_MS:Ljava/lang/String; = "starts_at"

.field static final NAME:Ljava/lang/String; = "pricing_rule_lookup"

.field private static final UTC:Ljava/util/TimeZone;

.field private static final VERSION:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "UTC"

    .line 40
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;->UTC:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/models/CatalogPricingRule;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogTimePeriod;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    .line 137
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 138
    new-instance v3, Ljava/util/HashSet;

    move-object/from16 v4, p2

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 139
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 140
    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_RULE_TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {v0, v4, v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->findReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 141
    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 144
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 145
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 146
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidity()Ljava/util/List;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 148
    :cond_0
    invoke-interface {v2, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 149
    const-class v4, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    invoke-static {v0, v4, v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readByIds(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v2

    .line 152
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 153
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidity()Ljava/util/List;

    move-result-object v5

    .line 154
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 155
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 156
    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_2

    :cond_2
    move-object/from16 v15, p3

    .line 159
    invoke-interface {v15, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    if-nez v8, :cond_3

    .line 161
    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v8, v6

    check-cast v8, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    :cond_3
    if-eqz v8, :cond_1

    .line 164
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object/from16 v15, p3

    .line 167
    new-instance v16, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    sget-object v14, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;->UTC:Ljava/util/TimeZone;

    move-object/from16 v5, v16

    move-object v6, v4

    invoke-direct/range {v5 .. v14}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;-><init>(Lcom/squareup/shared/catalog/models/CatalogPricingRule;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogProductSet;Lcom/squareup/shared/catalog/models/CatalogProductSet;Lcom/squareup/shared/catalog/models/CatalogProductSet;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/TimeZone;)V

    .line 172
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->startsAt()Ljava/util/Date;

    move-result-object v5

    .line 173
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->endsAt()Ljava/util/Date;

    move-result-object v6

    const/4 v7, 0x0

    if-eqz v5, :cond_5

    .line 175
    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_3

    :cond_5
    move-object v5, v7

    :goto_3
    if-eqz v6, :cond_6

    .line 178
    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 181
    :cond_6
    sget-object v6, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->INSERT_OR_REPLACE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v6

    .line 182
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v4

    .line 183
    invoke-virtual {v4, v5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v4

    .line 184
    invoke-virtual {v4, v7}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindLong(Ljava/lang/Long;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v4

    .line 185
    invoke-virtual {v4}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    goto/16 :goto_1

    :cond_7
    return-void
.end method


# virtual methods
.method public applyDeletes(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/DeletedCatalogObjects;)V
    .locals 3

    .line 125
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedPricingRuleIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 126
    sget-object v2, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->DELETE_BY_PRICING_RULE_ID:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v2

    .line 127
    invoke-virtual {v2, v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object v1

    .line 128
    invoke-virtual {v1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    goto :goto_0

    .line 130
    :cond_0
    iget-object v0, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTimePeriodIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 131
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    iget-object p2, p2, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTimePeriodIds:Ljava/util/Set;

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;->applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;)V

    :cond_1
    return-void
.end method

.method public applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V
    .locals 0

    .line 120
    new-instance p3, Ljava/util/HashSet;

    iget-object p4, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedPricingRulesById:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p4

    invoke-direct {p3, p4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 121
    iget-object p2, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedTimePeriodsById:Ljava/util/Map;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p4

    invoke-direct {p0, p1, p3, p2, p4}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;->applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;)V

    return-void
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 113
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 114
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE_STARTS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 115
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->CREATE_ENDS_AT_INDEX:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public tableName()Ljava/lang/String;
    .locals 1

    const-string v0, "pricing_rule_lookup"

    return-object v0
.end method

.method public tableVersion()J
    .locals 2

    const-wide/16 v0, 0x1

    return-wide v0
.end method
