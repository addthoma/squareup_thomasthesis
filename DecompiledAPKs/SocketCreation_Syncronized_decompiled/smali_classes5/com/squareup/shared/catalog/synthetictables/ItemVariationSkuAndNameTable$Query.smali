.class public final enum Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;
.super Ljava/lang/Enum;
.source "ItemVariationSkuAndNameTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

.field public static final enum CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

.field public static final enum CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

.field public static final enum CREATE_VARIATION_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

.field public static final enum DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

.field public static final enum UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 41
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    const-string v1, "CREATE TABLE {table} ({sku} TEXT, {item_id} TEXT, {variation_id} TEXT PRIMARY KEY, {search_words} TEXT)"

    .line 42
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v2, "variation_sku_name"

    const-string v3, "table"

    .line 44
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "sku"

    .line 45
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "item_id"

    .line 46
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v6, "variation_id"

    .line 47
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v7, "variation_search_words"

    const-string v8, "search_words"

    .line 48
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 50
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x0

    const-string v10, "CREATE"

    invoke-direct {v0, v10, v9, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    .line 52
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({item_id})"

    .line 53
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "idx"

    const-string v11, "idx_variation_sku_name_item_id"

    .line 54
    invoke-virtual {v1, v10, v11}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 55
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 56
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 58
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x1

    const-string v12, "CREATE_ITEM_ID_INDEX"

    invoke-direct {v0, v12, v11, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    .line 60
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({sku} COLLATE NOCASE)"

    .line 61
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "idx_variation_sku_name_sku"

    .line 62
    invoke-virtual {v1, v10, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 63
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 66
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v12, 0x2

    const-string v13, "CREATE_SKU_INDEX"

    invoke-direct {v0, v13, v12, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    .line 68
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {table} ({search_words} COLLATE NOCASE)"

    .line 69
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "idx_variation_sku_name_name"

    .line 70
    invoke-virtual {v1, v10, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 71
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 72
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 74
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x3

    const-string v13, "CREATE_VARIATION_NAME_INDEX"

    invoke-direct {v0, v13, v10, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_VARIATION_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    .line 76
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({sku}, {item_id}, {variation_id}, {search_words}) VALUES (?, ?, ?, ?)"

    .line 77
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 80
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 81
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 82
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 83
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 85
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x4

    const-string v5, "UPSERT_VARIATION"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    .line 87
    new-instance v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    const-string v1, "DELETE FROM {table} WHERE {variation_id} = ?"

    .line 88
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 89
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 90
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 92
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const-string v3, "DELETE_BY_VARIATION_ID"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    .line 40
    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_ITEM_ID_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_SKU_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->CREATE_VARIATION_NAME_INDEX:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->DELETE_BY_VARIATION_ID:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 97
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;
    .locals 1

    .line 40
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
