.class public abstract Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.super Lcom/squareup/shared/catalog/connectv2/models/ModelObject;
.source "CatalogConnectV2Object.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObject<",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
        ">;"
    }
.end annotation


# instance fields
.field protected final backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;


# direct methods
.method protected constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 4

    .line 18
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 19
    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->getValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->typeFromRaw(J)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;-><init>(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/connectv2/models/ModelObject;-><init>(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;)V

    const-string v0, "backing object"

    .line 21
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 38
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 40
    :cond_1
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    .line 41
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->getBackingObject()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->getBackingObject()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public getBackingObject()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->version:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEFAULT_VERSION:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public isAvailableAtLocation(Ljava/lang/String;)Z
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->presentAtAllLocations()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->locationsAbsentAt()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->locationsAvailableAt()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public locationsAbsentAt()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->absent_at_location_ids:Ljava/util/List;

    return-object v0
.end method

.method public locationsAvailableAt()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_location_ids:Ljava/util/List;

    return-object v0
.end method

.method public presentAtAllLocations()Z
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->present_at_all_locations:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->DEFAULT_PRESENT_AT_ALL_LOCATIONS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public toByteArray()[B
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->encode()[B

    move-result-object v0

    return-object v0
.end method
