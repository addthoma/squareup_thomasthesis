.class public final Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;
.super Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.source "CatalogItemOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 25
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    const-string v0, "Item Option data"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getAllValues()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 47
    new-instance v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    invoke-direct {v3, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->display_name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getShowColors()Z
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->show_colors:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;->DEFAULT_SHOW_COLORS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
