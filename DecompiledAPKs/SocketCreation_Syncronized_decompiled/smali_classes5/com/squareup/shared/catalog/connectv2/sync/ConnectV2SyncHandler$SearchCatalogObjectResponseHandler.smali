.class public interface abstract Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;
.super Ljava/lang/Object;
.source "ConnectV2SyncHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SearchCatalogObjectResponseHandler"
.end annotation


# virtual methods
.method public abstract complete(Z)V
.end method

.method public abstract handle(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ">;)V"
        }
    .end annotation
.end method
