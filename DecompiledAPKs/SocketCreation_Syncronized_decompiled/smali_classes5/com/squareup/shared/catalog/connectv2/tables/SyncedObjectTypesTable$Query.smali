.class public final enum Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;
.super Ljava/lang/Enum;
.source "SyncedObjectTypesTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

.field public static final enum DELETE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

.field public static final enum INSERT:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

.field public static final enum READ_ALL_OBJECT_TYES:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 32
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    const-string v1, "CREATE TABLE {table} ({object_type} INTEGER PRIMARY KEY)"

    .line 33
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->NAME:Ljava/lang/String;

    const-string v3, "table"

    .line 34
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->COLUMN_OBJECT_TYPE:Ljava/lang/String;

    const-string v4, "object_type"

    .line 35
    invoke-virtual {v1, v4, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 37
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v5, "CREATE"

    invoke-direct {v0, v5, v2, v1}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->CREATE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    .line 39
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    const-string v1, "DELETE from {table} WHERE {object_type} = ?"

    .line 40
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v5, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->NAME:Ljava/lang/String;

    .line 41
    invoke-virtual {v1, v3, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v5, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->COLUMN_OBJECT_TYPE:Ljava/lang/String;

    .line 42
    invoke-virtual {v1, v4, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 44
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x1

    const-string v6, "DELETE"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->DELETE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    .line 45
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    const-string v1, "INSERT INTO {table} ({object_type}) VALUES (?)"

    .line 46
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v6, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->NAME:Ljava/lang/String;

    .line 47
    invoke-virtual {v1, v3, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v6, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->COLUMN_OBJECT_TYPE:Ljava/lang/String;

    .line 48
    invoke-virtual {v1, v4, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 50
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x2

    const-string v7, "INSERT"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->INSERT:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    .line 52
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    const-string v1, "SELECT {object_type} FROM {table} ORDER BY {object_type} ASC"

    .line 53
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v7, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->NAME:Ljava/lang/String;

    .line 54
    invoke-virtual {v1, v3, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    sget-object v3, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->COLUMN_OBJECT_TYPE:Ljava/lang/String;

    .line 55
    invoke-virtual {v1, v4, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 57
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x3

    const-string v4, "READ_ALL_OBJECT_TYES"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->READ_ALL_OBJECT_TYES:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    .line 31
    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->CREATE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->DELETE:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->INSERT:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->READ_ALL_OBJECT_TYES:Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput-object p3, p0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;
    .locals 1

    .line 31
    const-class v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
