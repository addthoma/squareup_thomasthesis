.class public final enum Lcom/squareup/shared/catalog/MetadataTable$Query;
.super Ljava/lang/Enum;
.source "MetadataTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/MetadataTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/MetadataTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/MetadataTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/MetadataTable$Query;

.field public static final enum INSERT_INITIAL_ROW:Lcom/squareup/shared/catalog/MetadataTable$Query;

.field public static final enum SELECT_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

.field public static final enum UPDATE_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 41
    new-instance v0, Lcom/squareup/shared/catalog/MetadataTable$Query;

    const-string v1, "CREATE TABLE {table} ({session_id} INTEGER, {seq} INTEGER, {cogs_server_version} INTEGER, {timestamp} STRING, {sync_incomplete} INTEGER)"

    .line 42
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "catalog_metadata"

    const-string v3, "table"

    .line 46
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "session_id"

    .line 47
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "seq"

    .line 48
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "cogs_server_version"

    .line 49
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "last_sync_timestamp"

    const-string/jumbo v8, "timestamp"

    .line 50
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string/jumbo v9, "version_sync_incomplete"

    const-string v10, "sync_incomplete"

    .line 51
    invoke-virtual {v1, v10, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 52
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 53
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x0

    const-string v12, "CREATE"

    invoke-direct {v0, v12, v11, v1}, Lcom/squareup/shared/catalog/MetadataTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->CREATE:Lcom/squareup/shared/catalog/MetadataTable$Query;

    .line 55
    new-instance v0, Lcom/squareup/shared/catalog/MetadataTable$Query;

    const-string v1, "INSERT INTO {table} ({session_id}, {seq}, {cogs_server_version}, {timestamp}, {sync_incomplete}) VALUES (null, 0, 0, null, 0)"

    .line 56
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 59
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 60
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 61
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 62
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 63
    invoke-virtual {v1, v8, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v10, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 66
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    const-string v5, "INSERT_INITIAL_ROW"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/shared/catalog/MetadataTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->INSERT_INITIAL_ROW:Lcom/squareup/shared/catalog/MetadataTable$Query;

    .line 68
    new-instance v0, Lcom/squareup/shared/catalog/MetadataTable$Query;

    const-string v1, "UPDATE {table} SET %s = ?"

    .line 69
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 72
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x2

    const-string v6, "UPDATE_COLUMN"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/shared/catalog/MetadataTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->UPDATE_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    .line 74
    new-instance v0, Lcom/squareup/shared/catalog/MetadataTable$Query;

    const-string v1, "SELECT %s FROM {table}"

    .line 75
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 76
    invoke-virtual {v1, v3, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 78
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const-string v3, "SELECT_COLUMN"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/shared/catalog/MetadataTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->SELECT_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/shared/catalog/MetadataTable$Query;

    .line 40
    sget-object v1, Lcom/squareup/shared/catalog/MetadataTable$Query;->CREATE:Lcom/squareup/shared/catalog/MetadataTable$Query;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/shared/catalog/MetadataTable$Query;->INSERT_INITIAL_ROW:Lcom/squareup/shared/catalog/MetadataTable$Query;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/catalog/MetadataTable$Query;->UPDATE_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/catalog/MetadataTable$Query;->SELECT_COLUMN:Lcom/squareup/shared/catalog/MetadataTable$Query;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/MetadataTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 83
    iput-object p3, p0, Lcom/squareup/shared/catalog/MetadataTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/MetadataTable$Query;
    .locals 1

    .line 40
    const-class v0, Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/MetadataTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/MetadataTable$Query;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/shared/catalog/MetadataTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/MetadataTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/MetadataTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/MetadataTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/shared/catalog/MetadataTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
