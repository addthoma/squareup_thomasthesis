.class public interface abstract Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;
.super Ljava/lang/Object;
.source "CatalogOnlineThenLocalTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Online;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract perform(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "TT;)TS;"
        }
    .end annotation
.end method
