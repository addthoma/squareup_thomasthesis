.class public Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogSurchargeFeeMembership.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/SurchargeFeeMembership;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getFeeId()Ljava/lang/String;
    .locals 2

    .line 34
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/SurchargeFeeMembership;

    .line 35
    iget-object v1, v0, Lcom/squareup/api/items/SurchargeFeeMembership;->fee:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/SurchargeFeeMembership;->fee:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 36
    iget-object v0, v0, Lcom/squareup/api/items/SurchargeFeeMembership;->fee:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Type;

    .line 49
    sget-object v1, Lcom/squareup/api/items/Type;->SURCHARGE:Lcom/squareup/api/items/Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->FEE:Lcom/squareup/api/items/Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 42
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_SURCHARGE_FEE_MEMBERSHIP_SURCHARGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;->getSurchargeId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 42
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_SURCHARGE_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;->getFeeId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSurchargeId()Ljava/lang/String;
    .locals 2

    .line 27
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/SurchargeFeeMembership;

    .line 28
    iget-object v1, v0, Lcom/squareup/api/items/SurchargeFeeMembership;->surcharge:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/SurchargeFeeMembership;->surcharge:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29
    iget-object v0, v0, Lcom/squareup/api/items/SurchargeFeeMembership;->surcharge:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method
