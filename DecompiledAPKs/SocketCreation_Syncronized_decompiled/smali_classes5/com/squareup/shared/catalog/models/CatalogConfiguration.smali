.class public final Lcom/squareup/shared/catalog/models/CatalogConfiguration;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Configuration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static from(Lcom/squareup/shared/catalog/models/CatalogConfiguration;Z)Lcom/squareup/shared/catalog/models/CatalogConfiguration;
    .locals 0

    .line 17
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->from(Ljava/lang/String;Z)Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    move-result-object p0

    return-object p0
.end method

.method public static from(Ljava/lang/String;Z)Lcom/squareup/shared/catalog/models/CatalogConfiguration;
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;-><init>()V

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TILE_APPEARANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 22
    invoke-virtual {v1, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/api/items/Configuration$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/Configuration$Builder;-><init>()V

    if-eqz p1, :cond_0

    .line 24
    sget-object p1, Lcom/squareup/api/items/PageLayout;->TEXT_TILES_3X9:Lcom/squareup/api/items/PageLayout;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_5X5:Lcom/squareup/api/items/PageLayout;

    :goto_0
    invoke-virtual {v1, p1}, Lcom/squareup/api/items/Configuration$Builder;->page_layout(Lcom/squareup/api/items/PageLayout;)Lcom/squareup/api/items/Configuration$Builder;

    move-result-object p1

    .line 25
    invoke-virtual {p1, p0}, Lcom/squareup/api/items/Configuration$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Configuration$Builder;

    move-result-object p0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/api/items/Configuration$Builder;->build()Lcom/squareup/api/items/Configuration;

    move-result-object p0

    .line 23
    invoke-virtual {v0, p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration(Lcom/squareup/api/items/Configuration;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 27
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    .line 28
    new-instance p1, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    invoke-direct {p1, p0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object p1
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogConfiguration;
    .locals 0

    .line 32
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 44
    instance-of v0, p1, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 47
    :cond_0
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    .line 48
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->getPageLayout()Lcom/squareup/api/items/PageLayout;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->getPageLayout()Lcom/squareup/api/items/PageLayout;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/PageLayout;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getPageLayout()Lcom/squareup/api/items/PageLayout;
    .locals 2

    .line 40
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Configuration;

    iget-object v0, v0, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    sget-object v1, Lcom/squareup/api/items/PageLayout;->IMAGE_TILES_5X5:Lcom/squareup/api/items/PageLayout;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/PageLayout;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->getPageLayout()Lcom/squareup/api/items/PageLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/items/PageLayout;->hashCode()I

    move-result v0

    return v0
.end method
