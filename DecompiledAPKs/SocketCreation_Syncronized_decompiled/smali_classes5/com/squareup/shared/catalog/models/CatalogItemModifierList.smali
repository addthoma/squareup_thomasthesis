.class public Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItemModifierList.java"

# interfaces
.implements Lcom/squareup/shared/catalog/models/SupportsSearch;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/ItemModifierList;",
        ">;",
        "Lcom/squareup/shared/catalog/models/SupportsSearch;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static create(ILjava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
    .locals 2

    .line 28
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p2

    .line 29
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    new-instance v1, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    .line 31
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/squareup/api/items/ItemModifierList$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p0

    iget-object p1, p2, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object p0

    .line 30
    invoke-virtual {p2, p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public static create(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
    .locals 3

    .line 53
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    new-instance v2, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    .line 56
    invoke-virtual {v2, p0}, Lcom/squareup/api/items/ItemModifierList$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p0

    iget-object v2, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v2, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 57
    invoke-virtual {p0, v2}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p0

    .line 58
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object p0

    .line 55
    invoke-virtual {v0, p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v1
.end method

.method public static createForTesting(Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
    .locals 2

    .line 41
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 42
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    new-instance v1, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    .line 44
    invoke-virtual {v1, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 45
    invoke-virtual {p1, v1}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 46
    sget-object p2, Lcom/squareup/api/items/ItemModifierList$SelectionType;->MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/api/items/ItemModifierList$SelectionType;->SINGLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object p1

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 48
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public static isMultipleSelection(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Z
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->DEFAULT_SELECTION_TYPE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    invoke-static {p0, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    sget-object v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static name(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, ""

    .line 24
    invoke-static {p0, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->name(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 102
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemModifierList;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getOrdinal()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/Queries;->fixedLengthOrdinal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isMultipleSelection()Z
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->isMultipleSelection(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Z

    move-result v0

    return v0
.end method

.method public searchKeywords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 114
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->normalizedKeywordsForWordPrefixSearchWithSpecialCharactersIgnored(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateOrSame(Ljava/lang/String;I)Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
    .locals 3

    .line 68
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getOrdinal()I

    move-result v0

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    return-object p0

    .line 74
    :cond_2
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v2}, Lcom/squareup/api/items/ItemModifierList;->newBuilder()Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object v2

    .line 76
    invoke-virtual {v2, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    .line 77
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/ItemModifierList$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object p1

    .line 75
    invoke-virtual {v1, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method
