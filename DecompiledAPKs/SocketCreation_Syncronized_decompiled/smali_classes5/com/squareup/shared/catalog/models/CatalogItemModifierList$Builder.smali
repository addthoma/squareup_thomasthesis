.class public final Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;
.super Ljava/lang/Object;
.source "CatalogItemModifierList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 124
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 129
    new-instance p1, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;
    .locals 3

    .line 159
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v2, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-virtual {v2}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public selectionType(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Lcom/squareup/api/items/ItemModifierList$Builder;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    .line 134
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    return-object p0
.end method

.method public setIsConversational(Z)Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->is_conversational(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemModifierList$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    return-object p0
.end method

.method public setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList$Builder;->itemModifierList:Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierList$Builder;

    return-object p0
.end method
