.class public Lcom/squareup/shared/catalog/models/CatalogTax;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogTax.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Fee;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogTax;
    .locals 0

    .line 89
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogTax;

    return-object p0
.end method


# virtual methods
.method public appliesForCustomTypes()Z
    .locals 2

    .line 36
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_APPLIES_TO_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public appliesForCustomTypesOnIOS()Z
    .locals 2

    .line 45
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public builder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;
    .locals 1

    .line 81
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogTax;)V

    return-object v0
.end method

.method public getAdjustmentType()Lcom/squareup/api/items/Fee$AdjustmentType;
    .locals 2

    .line 85
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_ADJUSTMENT_TYPE:Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee$AdjustmentType;

    return-object v0
.end method

.method public getAppliesToProductSet()Ljava/lang/String;
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getCalculationPhase()Lcom/squareup/api/items/CalculationPhase;
    .locals 2

    .line 65
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/CalculationPhase;

    return-object v0
.end method

.method public getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;
    .locals 2

    .line 61
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee$InclusionType;

    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 28
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPercentage()Ljava/lang/String;
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 2

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTypeId()Ljava/lang/String;
    .locals 2

    .line 53
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 2

    .line 32
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Fee;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isInclusive()Z
    .locals 2

    .line 57
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTax;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Fee$InclusionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
