.class public final Lcom/squareup/shared/catalog/models/CatalogTicketGroup;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogTicketGroup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/TicketGroup;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method

.method public static create(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/shared/catalog/models/CatalogTicketGroup;
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v2, p0, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapObjectMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public static create(Ljava/lang/String;Lcom/squareup/api/items/TicketGroup$NamingMethod;I)Lcom/squareup/shared/catalog/models/CatalogTicketGroup;
    .locals 4

    .line 16
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    .line 17
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    new-instance v2, Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/TicketGroup$Builder;-><init>()V

    iget-object v3, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, v3, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    .line 19
    invoke-virtual {v2, v3}, Lcom/squareup/api/items/TicketGroup$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v2

    .line 20
    invoke-virtual {v2, p0}, Lcom/squareup/api/items/TicketGroup$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object p0

    .line 21
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method(Lcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object p0

    .line 22
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object p0

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object p0

    .line 18
    invoke-virtual {v0, p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->ticket_group(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v1
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 2

    .line 42
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;
    .locals 2

    .line 46
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    sget-object v1, Lcom/squareup/api/items/TicketGroup;->DEFAULT_NAMING_METHOD:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup$NamingMethod;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 50
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemModifierList;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 2

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getOrdinal()I

    move-result v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/Queries;->fixedLengthOrdinal(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/items/TicketGroup$NamingMethod;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->getOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "%s{id=%s, name=%s, naming_method=%s, ordinal=%d}"

    .line 54
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
