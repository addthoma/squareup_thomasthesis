.class Lcom/squareup/shared/catalog/SqliteCatalogStore$1;
.super Lcom/squareup/shared/catalog/TypedCursor;
.source "SqliteCatalogStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/SqliteCatalogStore;->resolveOuter(Lcom/squareup/shared/catalog/ManyToMany$Lookup;)Lcom/squareup/shared/catalog/TypedCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/TypedCursor<",
        "Lcom/squareup/shared/catalog/Related<",
        "TT;TJ;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/SqliteCatalogStore;

.field final synthetic val$joinType:Lcom/squareup/api/items/Type;

.field final synthetic val$targetType:Lcom/squareup/api/items/Type;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/SqliteCatalogStore;Lcom/squareup/shared/sql/SQLCursor;Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V
    .locals 0

    .line 918
    iput-object p1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->this$0:Lcom/squareup/shared/catalog/SqliteCatalogStore;

    iput-object p3, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->val$targetType:Lcom/squareup/api/items/Type;

    iput-object p4, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->val$joinType:Lcom/squareup/api/items/Type;

    invoke-direct {p0, p2}, Lcom/squareup/shared/catalog/TypedCursor;-><init>(Lcom/squareup/shared/sql/SQLCursor;)V

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/shared/catalog/Related;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/Related<",
            "TT;TJ;>;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 920
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->getBlob(I)[B

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->val$targetType:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->access$000([BLcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    const/4 v1, 0x1

    .line 921
    invoke-virtual {p0, v1}, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->isNull(I)Z

    move-result v2

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    const/4 v2, 0x2

    .line 924
    invoke-virtual {p0, v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->getBlob(I)[B

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->val$joinType:Lcom/squareup/api/items/Type;

    invoke-static {v2, v3}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->access$000([BLcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 928
    :goto_0
    new-instance v3, Lcom/squareup/shared/catalog/Related;

    invoke-direct {v3, v0, v1, v2}, Lcom/squareup/shared/catalog/Related;-><init>(Lcom/squareup/shared/catalog/models/CatalogObject;ZLcom/squareup/shared/catalog/models/CatalogObject;)V

    return-object v3
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 918
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/SqliteCatalogStore$1;->get()Lcom/squareup/shared/catalog/Related;

    move-result-object v0

    return-object v0
.end method
