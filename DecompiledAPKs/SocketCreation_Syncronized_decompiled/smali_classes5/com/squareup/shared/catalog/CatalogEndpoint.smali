.class public abstract Lcom/squareup/shared/catalog/CatalogEndpoint;
.super Ljava/lang/Object;
.source "CatalogEndpoint.java"


# static fields
.field private static final API_VERSION:Lcom/squareup/api/sync/ApiVersion;

.field private static final EMPTY_CREATE_SESSION_REQUEST:Lcom/squareup/api/sync/CreateSessionRequest;

.field private static final SYNC_METHOD_CREATE_SESSION:Ljava/lang/String; = "CreateSession"

.field private static final SYNC_METHOD_GET:Ljava/lang/String; = "Get"

.field private static final SYNC_METHOD_PUT:Ljava/lang/String; = "Put"

.field private static final SYNC_SERVICE:Ljava/lang/String; = "SyncService"


# instance fields
.field private final canUpdateItems:Z

.field private final name:Ljava/lang/String;

.field private final userRequestScope:Lcom/squareup/api/sync/RequestScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/api/sync/ApiVersion;->VERSION_1:Lcom/squareup/api/sync/ApiVersion;

    sput-object v0, Lcom/squareup/shared/catalog/CatalogEndpoint;->API_VERSION:Lcom/squareup/api/sync/ApiVersion;

    .line 33
    new-instance v0, Lcom/squareup/api/sync/CreateSessionRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/CreateSessionRequest$Builder;-><init>()V

    .line 34
    invoke-virtual {v0}, Lcom/squareup/api/sync/CreateSessionRequest$Builder;->build()Lcom/squareup/api/sync/CreateSessionRequest;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/CatalogEndpoint;->EMPTY_CREATE_SESSION_REQUEST:Lcom/squareup/api/sync/CreateSessionRequest;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->name:Ljava/lang/String;

    .line 44
    new-instance p1, Lcom/squareup/api/sync/RequestScope$Builder;

    invoke-direct {p1}, Lcom/squareup/api/sync/RequestScope$Builder;-><init>()V

    .line 45
    invoke-virtual {p1, p2}, Lcom/squareup/api/sync/RequestScope$Builder;->supported_features(Ljava/util/List;)Lcom/squareup/api/sync/RequestScope$Builder;

    move-result-object p1

    const-string/jumbo p2, "userToken"

    .line 46
    invoke-static {p3, p2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/api/sync/RequestScope$Builder;->user_token(Ljava/lang/String;)Lcom/squareup/api/sync/RequestScope$Builder;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/squareup/api/sync/RequestScope$Builder;->build()Lcom/squareup/api/sync/RequestScope;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    .line 48
    iput-boolean p4, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->canUpdateItems:Z

    return-void
.end method


# virtual methods
.method public canUpdateItems()Z
    .locals 1

    .line 75
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->canUpdateItems:Z

    return v0
.end method

.method public createCreateSessionRequest(J)Lcom/squareup/api/rpc/Request;
    .locals 1

    .line 80
    new-instance v0, Lcom/squareup/api/rpc/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Request$Builder;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/rpc/Request$Builder;->id(Ljava/lang/Long;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/shared/catalog/CatalogEndpoint;->EMPTY_CREATE_SESSION_REQUEST:Lcom/squareup/api/sync/CreateSessionRequest;

    .line 81
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->create_session_request(Lcom/squareup/api/sync/CreateSessionRequest;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    const-string p2, "SyncService"

    .line 82
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->service_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    const-string p2, "CreateSession"

    .line 83
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->method_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    .line 84
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->scope(Lcom/squareup/api/sync/RequestScope;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/shared/catalog/CatalogEndpoint;->API_VERSION:Lcom/squareup/api/sync/ApiVersion;

    .line 85
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->api_version(Lcom/squareup/api/sync/ApiVersion;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request$Builder;->build()Lcom/squareup/api/rpc/Request;

    move-result-object p1

    return-object p1
.end method

.method public createGetRequest(JLcom/squareup/api/sync/GetRequest;)Lcom/squareup/api/rpc/Request;
    .locals 1

    .line 90
    new-instance v0, Lcom/squareup/api/rpc/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Request$Builder;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/rpc/Request$Builder;->id(Ljava/lang/Long;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    const-string p2, "SyncService"

    .line 91
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->service_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    const-string p2, "Get"

    .line 92
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->method_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1, p3}, Lcom/squareup/api/rpc/Request$Builder;->get_request(Lcom/squareup/api/sync/GetRequest;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/shared/catalog/CatalogEndpoint;->API_VERSION:Lcom/squareup/api/sync/ApiVersion;

    .line 94
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->api_version(Lcom/squareup/api/sync/ApiVersion;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    .line 95
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->scope(Lcom/squareup/api/sync/RequestScope;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request$Builder;->build()Lcom/squareup/api/rpc/Request;

    move-result-object p1

    return-object p1
.end method

.method public createPutRequest(Ljava/util/List;Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/rpc/Request;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;",
            "Lcom/squareup/api/sync/WritableSessionState;",
            ")",
            "Lcom/squareup/api/rpc/Request;"
        }
    .end annotation

    .line 100
    new-instance v0, Lcom/squareup/api/sync/PutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/PutRequest$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/PutRequest$Builder;->objects(Ljava/util/List;)Lcom/squareup/api/sync/PutRequest$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/PutRequest$Builder;->build()Lcom/squareup/api/sync/PutRequest;

    move-result-object p1

    .line 102
    new-instance v0, Lcom/squareup/api/rpc/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Request$Builder;-><init>()V

    const-string v1, "SyncService"

    invoke-virtual {v0, v1}, Lcom/squareup/api/rpc/Request$Builder;->service_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object v0

    const-string v1, "Put"

    .line 103
    invoke-virtual {v0, v1}, Lcom/squareup/api/rpc/Request$Builder;->method_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0, p1}, Lcom/squareup/api/rpc/Request$Builder;->put_request(Lcom/squareup/api/sync/PutRequest;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/shared/catalog/CatalogEndpoint;->API_VERSION:Lcom/squareup/api/sync/ApiVersion;

    .line 105
    invoke-virtual {p1, v0}, Lcom/squareup/api/rpc/Request$Builder;->api_version(Lcom/squareup/api/sync/ApiVersion;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    .line 106
    invoke-virtual {p1, v0}, Lcom/squareup/api/rpc/Request$Builder;->scope(Lcom/squareup/api/sync/RequestScope;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    .line 107
    invoke-virtual {p1, p2}, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state(Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request$Builder;->build()Lcom/squareup/api/rpc/Request;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 112
    instance-of v0, p1, Lcom/squareup/shared/catalog/CatalogEndpoint;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 115
    :cond_0
    check-cast p1, Lcom/squareup/shared/catalog/CatalogEndpoint;

    .line 116
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    iget-object v0, v0, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    iget-object v2, v2, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->canUpdateItems:Z

    .line 117
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean p1, p1, Lcom/squareup/shared/catalog/CatalogEndpoint;->canUpdateItems:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    .line 116
    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public abstract executeRequest(Lcom/squareup/api/rpc/RequestBatch;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/RequestBatch;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation
.end method

.method public getDatabasePath()Ljava/lang/String;
    .locals 2

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    iget-object v1, v1, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedFeatures()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    iget-object v0, v0, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 121
    iget-object v1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    iget-object v1, v1, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->canUpdateItems:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/CatalogEndpoint;->userRequestScope:Lcom/squareup/api/sync/RequestScope;

    iget-object v1, v1, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
