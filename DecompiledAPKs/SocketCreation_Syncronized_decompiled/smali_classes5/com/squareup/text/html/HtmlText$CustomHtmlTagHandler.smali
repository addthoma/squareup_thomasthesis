.class Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;
.super Ljava/lang/Object;
.source "HtmlText.java"

# interfaces
.implements Landroid/text/Html$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/html/HtmlText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomHtmlTagHandler"
.end annotation


# instance fields
.field private final parsers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/text/html/TagParser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private varargs constructor <init>([Lcom/squareup/text/html/TagParser;)V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;->parsers:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>([Lcom/squareup/text/html/TagParser;Lcom/squareup/text/html/HtmlText$1;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;-><init>([Lcom/squareup/text/html/TagParser;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;->fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;->replaceTags(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 61
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x18

    if-lt v0, v2, :cond_0

    const/4 v0, 0x0

    .line 62
    invoke-static {p1, v0, v1, p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;ILandroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object p1

    goto :goto_0

    .line 63
    :cond_0
    invoke-static {p1, v1, p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object p1

    .line 67
    :goto_0
    invoke-static {p1}, Lcom/squareup/util/Strings;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private replaceTags(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 78
    iget-object v0, p0, Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;->parsers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/html/TagParser;

    .line 79
    invoke-interface {v1}, Lcom/squareup/text/html/TagParser;->tagsToHandle()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 80
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 81
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 82
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    return-object p1
.end method


# virtual methods
.method public handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/text/html/HtmlText$CustomHtmlTagHandler;->parsers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/html/TagParser;

    .line 91
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/squareup/text/html/TagParser;->handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V

    goto :goto_0

    :cond_0
    return-void
.end method
