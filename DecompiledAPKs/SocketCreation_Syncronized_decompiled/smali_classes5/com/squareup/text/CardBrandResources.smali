.class public Lcom/squareup/text/CardBrandResources;
.super Ljava/lang/Object;
.source "CardBrandResources.java"


# static fields
.field private static instances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/Card$Brand;",
            "Lcom/squareup/text/CardBrandResources;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final brandNameId:I

.field public final buyerBrandNameId:I

.field public final buyerCardPhrase:I

.field public final shortBrandNameId:I

.field public final shortUppercaseBrandNameId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 29
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/squareup/Card$Brand;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    .line 32
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_visa:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_visa:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_visa_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_visa:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_visa_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_mastercard:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_mastercard:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_mastercard_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_mastercard:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_mastercard:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->ALIPAY:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_unknown_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_amex:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_amex_short:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_amex_short_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_amex:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_amex_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->CASH_APP:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_unknown_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_discover:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_discover:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_discover_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_discover:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_discover_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_discover:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_discover:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_discover_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_discover:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_discover_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_jcb:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_jcb:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_jcb_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_jcb:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_jcb_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_cup:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_cup:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_cup_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_cup:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_cup_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->gift_card:I

    sget v4, Lcom/squareup/common/card/R$string;->gift_card:I

    sget v5, Lcom/squareup/common/card/R$string;->gift_card_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_gift_card:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_gift_card_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_eftpos:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_eftpos:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_eftpos:I

    sget v6, Lcom/squareup/common/card/R$string;->card_brand_eftpos:I

    sget v7, Lcom/squareup/common/card/R$string;->card_brand_eftpos:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_unknown_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_unknown_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_unknown_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_interac:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_interac:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_interac_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_interac:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_interac_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

    new-instance v8, Lcom/squareup/text/CardBrandResources;

    sget v3, Lcom/squareup/common/card/R$string;->card_brand_square_capital_card:I

    sget v4, Lcom/squareup/common/card/R$string;->card_brand_square_capital_card:I

    sget v5, Lcom/squareup/common/card/R$string;->card_brand_square_capital_card_uppercase:I

    sget v6, Lcom/squareup/common/card/R$string;->buyer_card_brand_square_capital_card:I

    sget v7, Lcom/squareup/common/card/R$string;->buyer_card_brand_square_capital_card_phrase:I

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/text/CardBrandResources;-><init>(IIIII)V

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(IIIII)V
    .locals 0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput p1, p0, Lcom/squareup/text/CardBrandResources;->brandNameId:I

    .line 170
    iput p2, p0, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    .line 171
    iput p3, p0, Lcom/squareup/text/CardBrandResources;->shortUppercaseBrandNameId:I

    .line 172
    iput p4, p0, Lcom/squareup/text/CardBrandResources;->buyerBrandNameId:I

    .line 173
    iput p5, p0, Lcom/squareup/text/CardBrandResources;->buyerCardPhrase:I

    return-void
.end method

.method public static forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;
    .locals 3

    .line 156
    sget-object v0, Lcom/squareup/text/CardBrandResources;->instances:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/CardBrandResources;

    if-eqz v0, :cond_0

    return-object v0

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown brand: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
