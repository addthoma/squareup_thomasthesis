.class public final Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;
.super Ljava/lang/Object;
.source "TextModule_ProvideMediumDateTimeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/text/DateFormat;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMediumDateTime(Ljava/util/Locale;)Ljava/text/DateFormat;
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/squareup/text/TextModule;->provideMediumDateTime(Ljava/util/Locale;)Ljava/text/DateFormat;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/text/DateFormat;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;->get()Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/text/DateFormat;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideMediumDateTimeFactory;->provideMediumDateTime(Ljava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method
