.class public Lcom/squareup/text/CardPostalScrubber;
.super Ljava/lang/Object;
.source "CardPostalScrubber.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# static fields
.field private static final INVALID_CHARS:Ljava/util/regex/Pattern;

.field private static final MAX_LIMIT:I = 0xc

.field private static final STRIP_CHARS:Ljava/util/regex/Pattern;


# instance fields
.field private onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

.field private final postalScrubber:Lcom/squareup/text/PostalScrubber;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "[^A-Z0-9]"

    .line 10
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/CardPostalScrubber;->INVALID_CHARS:Ljava/util/regex/Pattern;

    const-string v0, "[- ]"

    .line 11
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/CardPostalScrubber;->STRIP_CHARS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {}, Lcom/squareup/text/PostalScrubber;->forUsOrCanada()Lcom/squareup/text/PostalScrubber;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/text/CardPostalScrubber;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    return-void
.end method

.method private static hasInvalidCharacters(Ljava/lang/String;)Z
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/text/CardPostalScrubber;->INVALID_CHARS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    return p0
.end method

.method public static isValid(Ljava/lang/String;)Z
    .locals 1

    .line 60
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/squareup/text/CardPostalScrubber;->strip(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/text/CardPostalScrubber;->hasInvalidCharacters(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private notifyInvalidContent()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/text/CardPostalScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    if-eqz v0, :cond_0

    .line 55
    invoke-interface {v0}, Lcom/squareup/text/OnInvalidContentListener;->onInvalidContent()V

    :cond_0
    return-void
.end method

.method private static strip(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/text/CardPostalScrubber;->STRIP_CHARS:Ljava/util/regex/Pattern;

    invoke-static {p0, v0}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private tryFormat(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/text/CardPostalScrubber;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/PostalScrubber;->scrub(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    sget-object v1, Lcom/squareup/text/CardPostalScrubber;->STRIP_CHARS:Ljava/util/regex/Pattern;

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    return-object p1
.end method


# virtual methods
.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 22
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 24
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    .line 26
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 29
    :cond_0
    invoke-static {p1}, Lcom/squareup/text/CardPostalScrubber;->strip(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 31
    invoke-static {p1}, Lcom/squareup/text/CardPostalScrubber;->hasInvalidCharacters(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    sget-object v0, Lcom/squareup/text/CardPostalScrubber;->INVALID_CHARS:Ljava/util/regex/Pattern;

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p1

    .line 33
    invoke-direct {p0}, Lcom/squareup/text/CardPostalScrubber;->notifyInvalidContent()V

    .line 36
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/text/CardPostalScrubber;->tryFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public setOnInvalidContentListener(Lcom/squareup/text/OnInvalidContentListener;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/text/CardPostalScrubber;->onInvalidContentListener:Lcom/squareup/text/OnInvalidContentListener;

    return-void
.end method
