.class public Lcom/squareup/text/PostalScrubber$Result;
.super Ljava/lang/Object;
.source "PostalScrubber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/PostalScrubber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Result"
.end annotation


# instance fields
.field final scrubbed:Ljava/lang/String;

.field final valid:Z


# direct methods
.method protected constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-boolean p1, p0, Lcom/squareup/text/PostalScrubber$Result;->valid:Z

    .line 69
    iput-object p2, p0, Lcom/squareup/text/PostalScrubber$Result;->scrubbed:Ljava/lang/String;

    return-void
.end method
