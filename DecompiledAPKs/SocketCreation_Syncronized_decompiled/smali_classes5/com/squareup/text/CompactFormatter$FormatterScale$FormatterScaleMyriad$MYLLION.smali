.class public final Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;
.super Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;
.source "CompactFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MYLLION"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCompactFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CompactFormatter.kt\ncom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION\n*L\n1#1,163:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 109
    new-instance v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;

    invoke-direct {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;-><init>()V

    sput-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYLLION;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 109
    sget-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    invoke-virtual {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    invoke-virtual {v1}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->getMagnitude()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    const-string/jumbo v1, "this.multiply(other)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;-><init>(Ljava/math/BigDecimal;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
