.class public Lcom/squareup/text/LetterSpacingSpan;
.super Landroid/text/style/ReplacementSpan;
.source "LetterSpacingSpan.java"


# instance fields
.field private final letterSpacing:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 12
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 13
    iput p1, p0, Lcom/squareup/text/LetterSpacingSpan;->letterSpacing:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 12

    move v8, p3

    move/from16 v0, p4

    move/from16 v9, p5

    :goto_0
    if-ge v8, v0, :cond_0

    add-int/lit8 v10, v8, 0x1

    move/from16 v11, p7

    int-to-float v6, v11

    move-object v1, p1

    move-object v2, p2

    move v3, v8

    move v4, v10

    move v5, v9

    move-object/from16 v7, p9

    .line 27
    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    move-object v1, p2

    move-object/from16 v2, p9

    .line 28
    invoke-virtual {v2, p2, v8, v10}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v3

    move-object v4, p0

    iget v5, v4, Lcom/squareup/text/LetterSpacingSpan;->letterSpacing:I

    int-to-float v5, v5

    add-float/2addr v3, v5

    add-float/2addr v9, v3

    move v8, v10

    goto :goto_0

    :cond_0
    move-object v4, p0

    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 0

    if-eqz p5, :cond_0

    .line 18
    invoke-virtual {p1, p5}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    .line 20
    :cond_0
    invoke-virtual {p1, p2, p3, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result p1

    iget p2, p0, Lcom/squareup/text/LetterSpacingSpan;->letterSpacing:I

    sub-int/2addr p4, p3

    add-int/lit8 p4, p4, -0x1

    mul-int p2, p2, p4

    int-to-float p2, p2

    add-float/2addr p1, p2

    float-to-int p1, p1

    return p1
.end method
