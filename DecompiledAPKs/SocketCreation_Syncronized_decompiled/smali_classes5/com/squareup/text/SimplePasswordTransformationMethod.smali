.class public Lcom/squareup/text/SimplePasswordTransformationMethod;
.super Landroid/text/method/PasswordTransformationMethod;
.source "SimplePasswordTransformationMethod.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;
    }
.end annotation


# static fields
.field private static final DOT:C = '\u2022'

.field private static final SPACE_CHAR:C = ' '


# instance fields
.field private focused:Z

.field private showLastChar:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/text/SimplePasswordTransformationMethod;)Z
    .locals 0

    .line 18
    iget-boolean p0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod;->focused:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/text/SimplePasswordTransformationMethod;)Z
    .locals 0

    .line 18
    iget-boolean p0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod;->showLastChar:Z

    return p0
.end method

.method public static install(Landroid/widget/TextView;)V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/text/SimplePasswordTransformationMethod;

    invoke-direct {v0}, Lcom/squareup/text/SimplePasswordTransformationMethod;-><init>()V

    .line 22
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 23
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ge p4, p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 46
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-ne p2, p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p3, :cond_2

    .line 48
    iput-boolean v1, p0, Lcom/squareup/text/SimplePasswordTransformationMethod;->showLastChar:Z

    return-void

    :cond_2
    if-eqz p1, :cond_3

    .line 52
    iput-boolean v0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod;->showLastChar:Z

    :cond_3
    return-void
.end method

.method public getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 0

    .line 36
    new-instance p2, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;

    invoke-direct {p2, p0, p1}, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;-><init>(Lcom/squareup/text/SimplePasswordTransformationMethod;Ljava/lang/CharSequence;)V

    return-object p2
.end method

.method public onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .locals 0

    .line 41
    iput-boolean p3, p0, Lcom/squareup/text/SimplePasswordTransformationMethod;->focused:Z

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
