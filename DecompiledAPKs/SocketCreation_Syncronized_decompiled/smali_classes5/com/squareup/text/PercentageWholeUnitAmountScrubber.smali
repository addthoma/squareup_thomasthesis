.class public Lcom/squareup/text/PercentageWholeUnitAmountScrubber;
.super Lcom/squareup/money/WholeUnitAmountScrubber;
.source "PercentageWholeUnitAmountScrubber.java"


# static fields
.field private static final MAX_VALUE:I = 0x2710


# instance fields
.field private final numberFormat:Ljava/text/DecimalFormat;

.field private final percentPhrase:Lcom/squareup/phrase/Phrase;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V
    .locals 2

    .line 30
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;-><init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/text/DecimalFormat;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/text/DecimalFormat;)V
    .locals 2

    const-wide/16 v0, 0x2710

    .line 35
    invoke-direct {p0, v0, v1, p2}, Lcom/squareup/money/WholeUnitAmountScrubber;-><init>(JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    .line 36
    iput-object p3, p0, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;->numberFormat:Ljava/text/DecimalFormat;

    const/4 p2, 0x2

    .line 37
    invoke-virtual {p3, p2}, Ljava/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 38
    sget p2, Lcom/squareup/widgets/R$string;->percent_character_pattern:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;->percentPhrase:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/util/Locale;)V
    .locals 2

    .line 25
    new-instance v0, Ljava/text/DecimalFormat;

    new-instance v1, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v1, p3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    const-string p3, "0.00"

    invoke-direct {v0, p3, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;-><init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/text/DecimalFormat;)V

    return-void
.end method


# virtual methods
.method protected formatAmount(J)Ljava/lang/String;
    .locals 2

    long-to-double p1, p1

    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    div-double/2addr p1, v0

    .line 43
    iget-object v0, p0, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;->percentPhrase:Lcom/squareup/phrase/Phrase;

    iget-object v1, p0, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;->numberFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v1, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo p2, "value"

    invoke-virtual {v0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
