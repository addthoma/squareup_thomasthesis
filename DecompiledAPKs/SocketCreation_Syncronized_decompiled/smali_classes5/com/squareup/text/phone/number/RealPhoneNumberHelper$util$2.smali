.class final Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealPhoneNumberHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/text/phone/number/RealPhoneNumberHelper;-><init>(Lcom/squareup/CountryCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/google/i18n/phonenumbers/PhoneNumberUtil;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/google/i18n/phonenumbers/PhoneNumberUtil;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;

    invoke-direct {v0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;-><init>()V

    sput-object v0, Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;->INSTANCE:Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
    .locals 1

    .line 16
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/text/phone/number/RealPhoneNumberHelper$util$2;->invoke()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v0

    return-object v0
.end method
