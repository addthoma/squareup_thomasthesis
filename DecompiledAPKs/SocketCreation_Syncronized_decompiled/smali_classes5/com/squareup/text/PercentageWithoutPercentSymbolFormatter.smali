.class public Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;
.super Ljava/lang/Object;
.source "PercentageWithoutPercentSymbolFormatter.java"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/util/Percentage;",
        ">;"
    }
.end annotation


# instance fields
.field private locale:Ljava/util/Locale;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private numberFormat:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private doFormat(Lcom/squareup/util/Percentage;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->locale:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    iput-object p2, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->locale:Ljava/util/Locale;

    .line 34
    invoke-static {p2}, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->setUpNumberFormat(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->numberFormat:Ljava/text/NumberFormat;

    .line 37
    :cond_0
    iget-object p2, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->bigDecimalRate()Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static setUpNumberFormat(Ljava/util/Locale;)Ljava/text/NumberFormat;
    .locals 4

    .line 41
    invoke-static {p0}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p0

    const/4 v0, 0x0

    .line 42
    invoke-virtual {p0, v0}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    const/4 v0, 0x2

    .line 43
    invoke-virtual {p0, v0}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 45
    instance-of v0, p0, Ljava/text/DecimalFormat;

    if-eqz v0, :cond_0

    .line 46
    move-object v0, p0

    check-cast v0, Ljava/text/DecimalFormat;

    .line 47
    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getPositivePrefix()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa0

    const/16 v3, 0x20

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setPositivePrefix(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getNegativePrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setNegativePrefix(Ljava/lang/String;)V

    const-string v1, ""

    .line 50
    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setPositiveSuffix(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setNegativeSuffix(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method


# virtual methods
.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/util/Percentage;

    invoke-virtual {p0, p1}, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format(Lcom/squareup/util/Percentage;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {p0, p1, v0}, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;->doFormat(Lcom/squareup/util/Percentage;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
