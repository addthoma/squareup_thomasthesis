.class public Lcom/squareup/text/ScrubbingTextWatcher;
.super Ljava/lang/Object;
.source "ScrubbingTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/squareup/text/SelectionWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/text/ScrubbingTextWatcher$InsertingScrubberBridge;,
        Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;
    }
.end annotation


# instance fields
.field private current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

.field private enforceCursorAtEndOfDigits:Z

.field private formatting:Z

.field private restartImeAfterScrubChange:Z

.field private final scrubbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Lcom/squareup/text/HasSelectableText;


# direct methods
.method public constructor <init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 2

    .line 42
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher$InsertingScrubberBridge;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/text/ScrubbingTextWatcher$InsertingScrubberBridge;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/ScrubbingTextWatcher$1;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 2

    .line 38
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/ScrubbingTextWatcher$1;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 2

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->scrubbers:Ljava/util/List;

    .line 20
    new-instance v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->enforceCursorAtEndOfDigits:Z

    const/4 v0, 0x1

    .line 30
    iput-boolean v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->restartImeAfterScrubChange:Z

    .line 33
    iput-object p2, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    .line 34
    iget-object p2, p0, Lcom/squareup/text/ScrubbingTextWatcher;->scrubbers:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private maybeSetCursorToEndOfDigits()V
    .locals 3

    .line 151
    iget-boolean v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->enforceCursorAtEndOfDigits:Z

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v0, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    if-lez v0, :cond_0

    .line 153
    iget-object v1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v1, v1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/squareup/util/Characters;->isLatinDigit(C)Z

    move-result v1

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    .line 157
    iget-object v1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    invoke-interface {v1, v0, v0}, Lcom/squareup/text/HasSelectableText;->setSelection(II)V

    :cond_1
    return-void
.end method

.method private scrub(Landroid/text/Editable;)V
    .locals 9

    .line 84
    iget-boolean v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->formatting:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 85
    iput-boolean v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->formatting:Z

    const/4 v1, 0x0

    .line 93
    :try_start_0
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    .line 94
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    .line 95
    invoke-interface {v4}, Lcom/squareup/text/HasSelectableText;->getSelectionStart()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    .line 96
    invoke-interface {v5}, Lcom/squareup/text/HasSelectableText;->getSelectionEnd()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    .line 100
    iget-object v3, p0, Lcom/squareup/text/ScrubbingTextWatcher;->scrubbers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/text/SelectableTextScrubber;

    .line 101
    iget-object v6, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-interface {v5, v6, v2}, Lcom/squareup/text/SelectableTextScrubber;->scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    move-result-object v5

    .line 102
    iget v6, v5, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    if-ltz v6, :cond_1

    iget v6, v5, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    if-ltz v6, :cond_1

    move-object v2, v5

    const/4 v4, 0x1

    goto :goto_0

    .line 108
    :cond_1
    iget-object v6, v5, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    .line 109
    new-instance v7, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v5, v5, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    iget v8, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    .line 110
    invoke-static {v8, v6}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget v2, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    .line 111
    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-direct {v7, v5, v8, v2}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    move-object v2, v7

    goto :goto_0

    .line 114
    :cond_2
    iput-object v2, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    .line 117
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v2, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 118
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v2, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v2, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-interface {p1, v1, v0, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 120
    iget-boolean p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->restartImeAfterScrubChange:Z

    if-eqz p1, :cond_3

    .line 123
    iget-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    invoke-interface {p1}, Lcom/squareup/text/HasSelectableText;->postRestartInput()V

    .line 130
    :cond_3
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    invoke-interface {v0}, Lcom/squareup/text/HasSelectableText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget v2, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget-object v3, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    .line 131
    invoke-interface {v3}, Lcom/squareup/text/HasSelectableText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget v3, v3, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    iget-object v5, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    .line 132
    invoke-interface {v5}, Lcom/squareup/text/HasSelectableText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-direct {p1, v0, v2, v3}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    iput-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    if-eqz v4, :cond_5

    .line 134
    iget-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    .line 135
    invoke-interface {v0}, Lcom/squareup/text/HasSelectableText;->getSelectionStart()I

    move-result v0

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    .line 136
    invoke-interface {v0}, Lcom/squareup/text/HasSelectableText;->getSelectionEnd()I

    move-result v0

    if-eq p1, v0, :cond_5

    .line 137
    :cond_4
    iget-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget v0, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget-object v2, p0, Lcom/squareup/text/ScrubbingTextWatcher;->current:Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget v2, v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    invoke-interface {p1, v0, v2}, Lcom/squareup/text/HasSelectableText;->setSelection(II)V

    .line 140
    :cond_5
    invoke-direct {p0}, Lcom/squareup/text/ScrubbingTextWatcher;->maybeSetCursorToEndOfDigits()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    iput-boolean v1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->formatting:Z

    return-void

    :catchall_0
    move-exception p1

    iput-boolean v1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->formatting:Z

    .line 143
    throw p1
.end method


# virtual methods
.method public addScrubber(Lcom/squareup/text/Scrubber;)V
    .locals 2

    .line 50
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/ScrubbingTextWatcher$1;)V

    invoke-virtual {p0, v0}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/SelectableTextScrubber;)V

    return-void
.end method

.method public addScrubber(Lcom/squareup/text/SelectableTextScrubber;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->scrubbers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->scrub(Landroid/text/Editable;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public clearAddedScrubbers()V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher;->scrubbers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public onSelectionChanged(II)V
    .locals 0

    .line 79
    iget-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->view:Lcom/squareup/text/HasSelectableText;

    invoke-interface {p1}, Lcom/squareup/text/HasSelectableText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->scrub(Landroid/text/Editable;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public setEnforceCursorAtEndOfDigits(Z)V
    .locals 0

    .line 54
    iput-boolean p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->enforceCursorAtEndOfDigits:Z

    return-void
.end method

.method public setRestartImeAfterScrubChange(Z)V
    .locals 0

    .line 58
    iput-boolean p1, p0, Lcom/squareup/text/ScrubbingTextWatcher;->restartImeAfterScrubChange:Z

    return-void
.end method
