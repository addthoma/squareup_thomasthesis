.class public final Lcom/squareup/scales/RealShowingConnectedScalesWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "ShowingConnectedScalesWorkflow.kt"

# interfaces
.implements Lcom/squareup/scales/ShowingConnectedScalesWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/RealShowingConnectedScalesWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/scales/ShowingConnectedScalesProps;",
        "Lkotlin/Unit;",
        "Lcom/squareup/scales/ShowingConnectedScalesScreen;",
        ">;",
        "Lcom/squareup/scales/ShowingConnectedScalesWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0002:\u0001\u000cB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J$\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00040\nH\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/scales/RealShowingConnectedScalesWorkflow;",
        "Lcom/squareup/scales/ShowingConnectedScalesWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/scales/ShowingConnectedScalesProps;",
        "",
        "Lcom/squareup/scales/ShowingConnectedScalesScreen;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/scales/ShowingConnectedScalesProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/scales/ShowingConnectedScalesScreen;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 29
    new-instance v0, Lcom/squareup/scales/ShowingConnectedScalesScreen;

    .line 30
    invoke-virtual {p1}, Lcom/squareup/scales/ShowingConnectedScalesProps;->getConnectedScales()Ljava/util/List;

    move-result-object p1

    .line 31
    new-instance v1, Lcom/squareup/scales/RealShowingConnectedScalesWorkflow$render$1;

    invoke-direct {v1, p2}, Lcom/squareup/scales/RealShowingConnectedScalesWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 29
    invoke-direct {v0, p1, v1}, Lcom/squareup/scales/ShowingConnectedScalesScreen;-><init>(Ljava/util/List;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/scales/ShowingConnectedScalesProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/scales/RealShowingConnectedScalesWorkflow;->render(Lcom/squareup/scales/ShowingConnectedScalesProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/scales/ShowingConnectedScalesScreen;

    move-result-object p1

    return-object p1
.end method
