.class public final Lcom/squareup/scales/RealNoConnectedScalesWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "NoConnectedScalesWorkflow.kt"

# interfaces
.implements Lcom/squareup/scales/NoConnectedScalesWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/RealNoConnectedScalesWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        "Lcom/squareup/scales/NoConnectedScalesScreen;",
        ">;",
        "Lcom/squareup/scales/NoConnectedScalesWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002:\u0001\u000cB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0005J)\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00032\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00030\tH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/scales/RealNoConnectedScalesWorkflow;",
        "Lcom/squareup/scales/NoConnectedScalesWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/scales/NoConnectedScalesScreen;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/scales/NoConnectedScalesScreen;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/scales/NoConnectedScalesScreen;
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 28
    new-instance p2, Lcom/squareup/scales/NoConnectedScalesScreen;

    new-instance v0, Lcom/squareup/scales/RealNoConnectedScalesWorkflow$render$1;

    invoke-direct {v0, p1}, Lcom/squareup/scales/RealNoConnectedScalesWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p2, v0}, Lcom/squareup/scales/NoConnectedScalesScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    return-object p2
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/scales/RealNoConnectedScalesWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/scales/NoConnectedScalesScreen;

    move-result-object p1

    return-object p1
.end method
