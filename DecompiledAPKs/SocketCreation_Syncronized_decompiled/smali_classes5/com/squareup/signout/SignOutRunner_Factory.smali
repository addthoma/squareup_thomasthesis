.class public final Lcom/squareup/signout/SignOutRunner_Factory;
.super Ljava/lang/Object;
.source "SignOutRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/signout/SignOutRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final signOutTaskStackUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final unsyncedOpenTicketsSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/squareup/signout/SignOutRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p2, p0, Lcom/squareup/signout/SignOutRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p3, p0, Lcom/squareup/signout/SignOutRunner_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p4, p0, Lcom/squareup/signout/SignOutRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p5, p0, Lcom/squareup/signout/SignOutRunner_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p6, p0, Lcom/squareup/signout/SignOutRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p7, p0, Lcom/squareup/signout/SignOutRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p8, p0, Lcom/squareup/signout/SignOutRunner_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p9, p0, Lcom/squareup/signout/SignOutRunner_Factory;->unsyncedOpenTicketsSpinnerProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p10, p0, Lcom/squareup/signout/SignOutRunner_Factory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p11, p0, Lcom/squareup/signout/SignOutRunner_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p12, p0, Lcom/squareup/signout/SignOutRunner_Factory;->signOutTaskStackUpdaterProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p13, p0, Lcom/squareup/signout/SignOutRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p14, p0, Lcom/squareup/signout/SignOutRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/signout/SignOutRunner_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/signout/SignOutRunner_Factory;"
        }
    .end annotation

    .line 104
    new-instance v15, Lcom/squareup/signout/SignOutRunner_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/signout/SignOutRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/tickets/Tickets;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;Lcom/squareup/settings/server/Features;Ldagger/Lazy;)Lcom/squareup/signout/SignOutRunner;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/account/LegacyAuthenticator;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            "Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/signout/SignOutRunner;"
        }
    .end annotation

    .line 117
    new-instance v15, Lcom/squareup/signout/SignOutRunner;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/signout/SignOutRunner;-><init>(Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/tickets/Tickets;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;Lcom/squareup/settings/server/Features;Ldagger/Lazy;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/signout/SignOutRunner;
    .locals 15

    .line 89
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/pending/PendingTransactionsStore;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->unsyncedOpenTicketsSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/cashmanagement/CashManagementSettings;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->signOutTaskStackUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v14

    invoke-static/range {v1 .. v14}, Lcom/squareup/signout/SignOutRunner_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/tickets/Tickets;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/signout/SignOutRunner$SignOutTaskStackUpdater;Lcom/squareup/settings/server/Features;Ldagger/Lazy;)Lcom/squareup/signout/SignOutRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/signout/SignOutRunner_Factory;->get()Lcom/squareup/signout/SignOutRunner;

    move-result-object v0

    return-object v0
.end method
