.class public final Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "OpenCashDrawerShiftDialogScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOpenCashDrawerShiftDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OpenCashDrawerShiftDialogScreen.kt\ncom/squareup/signout/OpenCashDrawerShiftDialogScreen\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,39:1\n24#2,4:40\n*E\n*S KotlinDebug\n*F\n+ 1 OpenCashDrawerShiftDialogScreen.kt\ncom/squareup/signout/OpenCashDrawerShiftDialogScreen\n*L\n37#1,4:40\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/MaybePersistent;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Factory",
        "sign-out_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;

    invoke-direct {v0}, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;-><init>()V

    sput-object v0, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;->INSTANCE:Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;

    .line 40
    new-instance v0, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 43
    sput-object v0, Lcom/squareup/signout/OpenCashDrawerShiftDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    .line 16
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
