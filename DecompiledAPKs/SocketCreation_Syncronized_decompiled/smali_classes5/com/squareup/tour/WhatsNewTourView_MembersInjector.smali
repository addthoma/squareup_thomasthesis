.class public final Lcom/squareup/tour/WhatsNewTourView_MembersInjector;
.super Ljava/lang/Object;
.source "WhatsNewTourView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/tour/WhatsNewTourView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewTourScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewTourScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewTourScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/tour/WhatsNewTourView;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/tour/WhatsNewTourView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewTourView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/tour/WhatsNewTourView;Ljava/lang/Object;)V
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/tour/WhatsNewTourView;->presenter:Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/tour/WhatsNewTourView;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;->injectPresenter(Lcom/squareup/tour/WhatsNewTourView;Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;->injectDevice(Lcom/squareup/tour/WhatsNewTourView;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/tour/WhatsNewTourView;

    invoke-virtual {p0, p1}, Lcom/squareup/tour/WhatsNewTourView_MembersInjector;->injectMembers(Lcom/squareup/tour/WhatsNewTourView;)V

    return-void
.end method
