.class public final enum Lcom/squareup/tour/Education;
.super Ljava/lang/Enum;
.source "Education.java"

# interfaces
.implements Lcom/squareup/tour/Tour$HasTourPages;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tour/Education;",
        ">;",
        "Lcom/squareup/tour/Tour$HasTourPages;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tour/Education;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/tour/Education;

    .line 15
    sput-object v0, Lcom/squareup/tour/Education;->$VALUES:[Lcom/squareup/tour/Education;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tour/Education;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/tour/Education;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tour/Education;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tour/Education;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/tour/Education;->$VALUES:[Lcom/squareup/tour/Education;

    invoke-virtual {v0}, [Lcom/squareup/tour/Education;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tour/Education;

    return-object v0
.end method


# virtual methods
.method public addPages(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tour/TourPage;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
