.class public final Lcom/squareup/tour/WhatsNewUi_Factory;
.super Ljava/lang/Object;
.source "WhatsNewUi_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tour/WhatsNewUi;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final switchEmployeesEducationPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->whatsNewSettingsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->switchEmployeesEducationPresenterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->tutorialPresenterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->homeProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tour/WhatsNewUi_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/tour/WhatsNewUi_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/tour/WhatsNewUi_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tour/WhatsNewUi_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/tour/WhatsNewUi;
    .locals 8

    .line 63
    new-instance v7, Lcom/squareup/tour/WhatsNewUi;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tour/WhatsNewUi;-><init>(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/tour/WhatsNewUi;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->whatsNewSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/tour/WhatsNewSettings;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->switchEmployeesEducationPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->tutorialPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/main/Home;

    iget-object v0, p0, Lcom/squareup/tour/WhatsNewUi_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v6}, Lcom/squareup/tour/WhatsNewUi_Factory;->newInstance(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/tour/WhatsNewUi;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewUi_Factory;->get()Lcom/squareup/tour/WhatsNewUi;

    move-result-object v0

    return-object v0
.end method
