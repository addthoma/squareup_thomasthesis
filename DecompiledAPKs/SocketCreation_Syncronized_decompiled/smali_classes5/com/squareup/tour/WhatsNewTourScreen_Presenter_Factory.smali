.class public final Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "WhatsNewTourScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tour/WhatsNewTourScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->whatsNewSettingsProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/analytics/Analytics;Lflow/Flow;)Lcom/squareup/tour/WhatsNewTourScreen$Presenter;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tour/WhatsNewTourScreen$Presenter;-><init>(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/analytics/Analytics;Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tour/WhatsNewTourScreen$Presenter;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->whatsNewSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tour/WhatsNewSettings;

    iget-object v1, p0, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflow/Flow;

    invoke-static {v0, v1, v2}, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->newInstance(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/analytics/Analytics;Lflow/Flow;)Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewTourScreen_Presenter_Factory;->get()Lcom/squareup/tour/WhatsNewTourScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
