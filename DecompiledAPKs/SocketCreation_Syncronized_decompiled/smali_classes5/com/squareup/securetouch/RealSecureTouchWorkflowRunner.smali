.class public final Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealSecureTouchWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/securetouch/SecureTouchWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;,
        Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/securetouch/SecureTouchInput;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;",
        "Lcom/squareup/securetouch/SecureTouchWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002:\u0002 !BG\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0014J\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020\u0003H\u0002R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u00020\u0010X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;",
        "Lcom/squareup/securetouch/SecureTouchWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/securetouch/SecureTouchInput;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "secureTouchWorkflowResultRunner",
        "Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "statusBarEventManager",
        "Lcom/squareup/statusbar/event/StatusBarEventManager;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "workflow",
        "Lcom/squareup/securetouch/RealSecureTouchWorkflow;",
        "viewFactory",
        "Lcom/squareup/securetouch/SecureTouchViewFactory;",
        "analyticsFactory",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/payment/Transaction;Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/SecureTouchViewFactory;Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;)V",
        "analytics",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;",
        "getWorkflow",
        "()Lcom/squareup/securetouch/RealSecureTouchWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "onInput",
        "secureTouchInput",
        "SecureTouchBootstrapScreen",
        "SecureTouchWorkflowScope",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final secureTouchWorkflowResultRunner:Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;

.field private final statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final workflow:Lcom/squareup/securetouch/RealSecureTouchWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/payment/Transaction;Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/SecureTouchViewFactory;Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;)V
    .locals 16
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    move-object/from16 v0, p7

    move-object/from16 v15, p8

    const-string v1, "container"

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "secureTouchWorkflowResultRunner"

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "tenderInEdit"

    invoke-static {v11, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "statusBarEventManager"

    invoke-static {v12, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "transaction"

    invoke-static {v13, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "workflow"

    invoke-static {v14, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "viewFactory"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "analyticsFactory"

    invoke-static {v15, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v1, Lcom/squareup/securetouch/SecureTouchWorkflowRunner;->Companion:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;

    invoke-virtual {v1}, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-interface/range {p1 .. p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v2

    .line 33
    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object/from16 v0, p0

    .line 30
    invoke-direct/range {v0 .. v7}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v9, v8, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object v10, v8, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->secureTouchWorkflowResultRunner:Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;

    iput-object v11, v8, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iput-object v12, v8, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    iput-object v13, v8, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v14, v8, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->workflow:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 36
    invoke-static {v15, v0, v1, v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;->create$default(Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    move-result-object v0

    iput-object v0, v8, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;)V
    .locals 0

    .line 19
    invoke-virtual {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$onInput(Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;Lcom/squareup/securetouch/SecureTouchInput;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->onInput(Lcom/squareup/securetouch/SecureTouchInput;)V

    return-void
.end method

.method private final onInput(Lcom/squareup/securetouch/SecureTouchInput;)V
    .locals 1

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->analytics:Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchInput;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;->logStart(Lcom/squareup/securetouch/SecureTouchPinEntryState;)V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/securetouch/RealSecureTouchWorkflow;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->workflow:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->getWorkflow()Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens()\n      \u2026ibe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    const-string v1, "tenderInEdit.requireSmartCardTender()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->isContactless()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->asAcceptsTips()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    const-string v1, "tenderInEdit.asAcceptsTips()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    invoke-static {p1}, Lcom/squareup/mortar/MortarScopes;->completeOnExit(Lmortar/MortarScope;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/statusbar/event/StatusBarEventManager;->enterFullscreenMode(Lio/reactivex/Completable;)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$onEnterScope$2;

    iget-object v2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;->secureTouchWorkflowResultRunner:Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;

    invoke-direct {v1, v2}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/securetouch/SecureTouchWorkflowResultRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult().subscribe(sec\u2026owResultRunner::onResult)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method
