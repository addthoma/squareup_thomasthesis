.class public final Lcom/squareup/securetouch/FinishFlow;
.super Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;
.source "SecureTouchAnalyticsLogger.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/securetouch/FinishFlow;",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;",
        "()V",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/FinishFlow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 70
    new-instance v0, Lcom/squareup/securetouch/FinishFlow;

    invoke-direct {v0}, Lcom/squareup/securetouch/FinishFlow;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/FinishFlow;->INSTANCE:Lcom/squareup/securetouch/FinishFlow;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 70
    sget-object v1, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->READER:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    const-string v2, "Finish secure touch flow"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;-><init>(Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
