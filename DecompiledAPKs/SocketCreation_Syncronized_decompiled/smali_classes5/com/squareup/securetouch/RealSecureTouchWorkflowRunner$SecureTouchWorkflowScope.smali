.class public final Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;
.super Lcom/squareup/container/ContainerTreeKey;
.source "RealSecureTouchWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SecureTouchWorkflowScope"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchWorkflowRunner.kt\ncom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,120:1\n35#2:121\n*E\n*S KotlinDebug\n*F\n+ 1 RealSecureTouchWorkflowRunner.kt\ncom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope\n*L\n91#1:121\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\nB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "parentScope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "Lmortar/MortarScope;",
        "getParentKey",
        "ParentComponent",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentScope:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;->parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 121
    const-class v1, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope$ParentComponent;

    .line 92
    invoke-interface {p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope$ParentComponent;->secureTouchWorkflowRunner()Lcom/squareup/securetouch/SecureTouchWorkflowRunner;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/securetouch/SecureTouchWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string p1, "super.buildScope(parentS\u2026isterServices\n          )"

    .line 90
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;->parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 78
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
