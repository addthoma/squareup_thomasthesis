.class public final Lcom/squareup/securetouch/RealSecureTouchFeature;
.super Ljava/lang/Object;
.source "RealSecureTouchFeature.kt"

# interfaces
.implements Lcom/squareup/securetouch/SecureTouchFeature;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000eH\u0016J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000eH\u0016J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\tH\u0016J\u0008\u0010\u0013\u001a\u00020\u0011H\u0016J\u0008\u0010\u0014\u001a\u00020\u0011H\u0016J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0007H\u0016J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u00078BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/securetouch/RealSecureTouchFeature;",
        "Lcom/squareup/securetouch/SecureTouchFeature;",
        "()V",
        "isSecureTouchEnabled",
        "",
        "messagesFromReader",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "messagesToReader",
        "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
        "secureTouchStatus",
        "getSecureTouchStatus",
        "()Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "eventsForReader",
        "Lio/reactivex/Observable;",
        "eventsFromReader",
        "onSecureTouchApplicationEvent",
        "",
        "event",
        "onSecureTouchDisabled",
        "onSecureTouchEnabled",
        "onSecureTouchFeatureEvent",
        "onUnexpectedReleaseEvent",
        "secureTouchPoint",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "onUnexpectedTouchEvent",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private isSecureTouchEnabled:Z

.field private final messagesFromReader:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesToReader:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesFromReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 15
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesToReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method private final getSecureTouchStatus()Lcom/squareup/securetouch/SecureTouchFeatureEvent;
    .locals 1

    .line 58
    iget-boolean v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->isSecureTouchEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/securetouch/SecureTouchEnabled;->INSTANCE:Lcom/squareup/securetouch/SecureTouchEnabled;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/securetouch/SecureTouchDisabled;->INSTANCE:Lcom/squareup/securetouch/SecureTouchDisabled;

    :goto_0
    check-cast v0, Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    return-object v0
.end method


# virtual methods
.method public eventsForReader()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesToReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public eventsFromReader()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesFromReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-direct {p0}, Lcom/squareup/securetouch/RealSecureTouchFeature;->getSecureTouchStatus()Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "messagesFromReader.startWith(secureTouchStatus)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesToReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onSecureTouchDisabled()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "RealSecureTouchFeature onSecureTouchDisabled"

    .line 40
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    iput-boolean v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->isSecureTouchEnabled:Z

    .line 42
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesFromReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-direct {p0}, Lcom/squareup/securetouch/RealSecureTouchFeature;->getSecureTouchStatus()Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onSecureTouchEnabled()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "RealSecureTouchFeature onSecureTouchEnabled"

    .line 34
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    .line 35
    iput-boolean v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->isSecureTouchEnabled:Z

    .line 36
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesFromReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-direct {p0}, Lcom/squareup/securetouch/RealSecureTouchFeature;->getSecureTouchStatus()Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onSecureTouchFeatureEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SecureTouchFeature: Received lcr event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchFeature;->messagesFromReader:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onUnexpectedReleaseEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    const-string v0, "secureTouchPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Received a touch up event. This should be impossible!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onUnexpectedTouchEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    const-string v0, "secureTouchPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Received a touch down event. This should be impossible!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
