.class final Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;
.super Ljava/lang/Object;
.source "SecureTouchCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchCoordinator;->onKeypadActivated()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012:\u0010\u0002\u001a6\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006 \u0007*\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u00080\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "Lcom/squareup/securetouch/SecureKey;",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/securetouch/ButtonRects;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;

    invoke-direct {v0}, Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;->INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;",
            "Lcom/squareup/securetouch/ButtonRects;",
            ">;)",
            "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/ButtonRects;

    const-string v1, "keyLayout"

    .line 163
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/securetouch/ButtonRects;->getCancelButtonRect()Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object v1

    .line 165
    invoke-virtual {p1}, Lcom/squareup/securetouch/ButtonRects;->getAccessibilityButtonRect()Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object v2

    .line 166
    invoke-virtual {p1}, Lcom/squareup/securetouch/ButtonRects;->getDoneButtonRect()Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object p1

    .line 162
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/securetouch/SecureTouchCoordinatorKt;->access$allButtonsOnThisScreen(Ljava/util/Map;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator$onKeypadActivated$1;->apply(Lkotlin/Pair;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    move-result-object p1

    return-object p1
.end method
