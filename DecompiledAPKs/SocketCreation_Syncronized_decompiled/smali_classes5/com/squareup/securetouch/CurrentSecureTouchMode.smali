.class public final Lcom/squareup/securetouch/CurrentSecureTouchMode;
.super Ljava/lang/Object;
.source "CurrentSecureTouchMode.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\nH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
        "Lmortar/Scoped;",
        "()V",
        "secureTouchMode",
        "Lcom/squareup/securetouch/SecureTouchMode;",
        "getSecureTouchMode",
        "()Lcom/squareup/securetouch/SecureTouchMode;",
        "setSecureTouchMode",
        "(Lcom/squareup/securetouch/SecureTouchMode;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private secureTouchMode:Lcom/squareup/securetouch/SecureTouchMode;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    sget-object v0, Lcom/squareup/securetouch/SecureTouchMode$RegularMode;->INSTANCE:Lcom/squareup/securetouch/SecureTouchMode$RegularMode;

    check-cast v0, Lcom/squareup/securetouch/SecureTouchMode;

    iput-object v0, p0, Lcom/squareup/securetouch/CurrentSecureTouchMode;->secureTouchMode:Lcom/squareup/securetouch/SecureTouchMode;

    return-void
.end method


# virtual methods
.method public final getSecureTouchMode()Lcom/squareup/securetouch/SecureTouchMode;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/securetouch/CurrentSecureTouchMode;->secureTouchMode:Lcom/squareup/securetouch/SecureTouchMode;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/securetouch/SecureTouchMode$RegularMode;->INSTANCE:Lcom/squareup/securetouch/SecureTouchMode$RegularMode;

    check-cast v0, Lcom/squareup/securetouch/SecureTouchMode;

    iput-object v0, p0, Lcom/squareup/securetouch/CurrentSecureTouchMode;->secureTouchMode:Lcom/squareup/securetouch/SecureTouchMode;

    return-void
.end method

.method public final setSecureTouchMode(Lcom/squareup/securetouch/SecureTouchMode;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iput-object p1, p0, Lcom/squareup/securetouch/CurrentSecureTouchMode;->secureTouchMode:Lcom/squareup/securetouch/SecureTouchMode;

    return-void
.end method
