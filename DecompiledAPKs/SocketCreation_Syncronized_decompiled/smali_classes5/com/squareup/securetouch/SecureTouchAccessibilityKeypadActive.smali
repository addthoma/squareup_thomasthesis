.class public final Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;
.super Lcom/squareup/securetouch/SecureTouchKeypadActive;
.source "SecureTouchFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\u0015\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J3\u0010\u0012\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\r\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
        "Lcom/squareup/securetouch/SecureTouchKeypadActive;",
        "keyCoordinates",
        "",
        "Lcom/squareup/securetouch/SecureKey;",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "keypadBorderSize",
        "",
        "keypadDigitRadius",
        "(Ljava/util/Map;II)V",
        "getKeyCoordinates",
        "()Ljava/util/Map;",
        "getKeypadBorderSize",
        "()I",
        "getKeypadDigitRadius",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final keyCoordinates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation
.end field

.field private final keypadBorderSize:I

.field private final keypadDigitRadius:I


# direct methods
.method public constructor <init>(Ljava/util/Map;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;II)V"
        }
    .end annotation

    const-string v0, "keyCoordinates"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 104
    invoke-direct {p0, p1, v0}, Lcom/squareup/securetouch/SecureTouchKeypadActive;-><init>(Ljava/util/Map;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    iput p2, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    iput p3, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;Ljava/util/Map;IIILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->copy(Ljava/util/Map;II)Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    return v0
.end method

.method public final copy(Ljava/util/Map;II)Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;II)",
            "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;"
        }
    .end annotation

    const-string v0, "keyCoordinates"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;-><init>(Ljava/util/Map;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    iget v1, p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    iget p1, p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getKeyCoordinates()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    return-object v0
.end method

.method public final getKeypadBorderSize()I
    .locals 1

    .line 102
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    return v0
.end method

.method public final getKeypadDigitRadius()I
    .locals 1

    .line 103
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SecureTouchAccessibilityKeypadActive(keyCoordinates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keyCoordinates:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", keypadBorderSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadBorderSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", keypadDigitRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->keypadDigitRadius:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
