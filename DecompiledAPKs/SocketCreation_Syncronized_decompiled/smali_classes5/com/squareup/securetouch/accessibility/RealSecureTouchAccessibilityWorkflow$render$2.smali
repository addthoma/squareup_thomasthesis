.class final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchAccessibilityWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$2;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    new-instance p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$2;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;

    invoke-static {v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->access$getAccessibilitySettings$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)Lcom/squareup/accessibility/AccessibilitySettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/accessibility/AccessibilitySettings;->isTalkBackEnabled()Z

    move-result v0

    invoke-direct {p1, v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;-><init>(Z)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$2;->invoke(Lkotlin/Unit;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;

    move-result-object p1

    return-object p1
.end method
