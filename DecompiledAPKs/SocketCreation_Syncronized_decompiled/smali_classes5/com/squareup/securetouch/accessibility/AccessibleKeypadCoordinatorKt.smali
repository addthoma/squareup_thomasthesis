.class public final Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;
.super Ljava/lang/Object;
.source "AccessibleKeypadCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccessibleKeypadCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccessibleKeypadCoordinator.kt\ncom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt\n*L\n1#1,318:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0002\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0001H\u0002\u001a\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0007H\u0002\u001a\u0014\u0010\u0008\u001a\u00020\t*\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u001a\u000c\u0010\r\u001a\u00020\u000c*\u00020\u000eH\u0002\u001a\u0014\u0010\u000f\u001a\u00020\t*\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0011H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "MAX_DISPLAYABLE_STARS",
        "",
        "getDimensionInPx",
        "Landroid/content/res/Resources;",
        "id",
        "getGlobalVisibleRect",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "Landroid/view/View;",
        "moveTo",
        "",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "secureTouchPoint",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "rawIntPoint",
        "Landroid/view/MotionEvent;",
        "reportTouchEventsDevOnly",
        "screen",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MAX_DISPLAYABLE_STARS:I = 0xc


# direct methods
.method public static final synthetic access$getDimensionInPx(Landroid/content/res/Resources;I)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->getDimensionInPx(Landroid/content/res/Resources;I)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getGlobalVisibleRect(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->getGlobalVisibleRect(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$moveTo(Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->moveTo(Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/squareup/securetouch/SecureTouchPoint;)V

    return-void
.end method

.method public static final synthetic access$rawIntPoint(Landroid/view/MotionEvent;)Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->rawIntPoint(Landroid/view/MotionEvent;)Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt;->reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V

    return-void
.end method

.method private static final getDimensionInPx(Landroid/content/res/Resources;I)I
    .locals 0

    .line 316
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    iget p0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {p1, p0}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p0

    return p0
.end method

.method private static final getGlobalVisibleRect(Landroid/view/View;)Lcom/squareup/securetouch/SecureTouchRect;
    .locals 4

    .line 304
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 305
    new-instance p0, Lcom/squareup/securetouch/SecureTouchRect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/squareup/securetouch/SecureTouchRect;-><init>(IIII)V

    return-object p0
.end method

.method private static final moveTo(Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    .line 311
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchPoint;->getX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->setX(F)V

    .line 312
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchPoint;->getY()I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->setY(F)V

    return-void
.end method

.method private static final rawIntPoint(Landroid/view/MotionEvent;)Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 2

    .line 308
    new-instance v0, Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawY()F

    move-result p0

    float-to-int p0, p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/securetouch/SecureTouchPoint;-><init>(II)V

    return-object v0
.end method

.method private static final reportTouchEventsDevOnly(Landroid/view/View;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V
    .locals 1

    .line 293
    new-instance v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt$reportTouchEventsDevOnly$1;

    invoke-direct {v0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinatorKt$reportTouchEventsDevOnly$1;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)V

    check-cast v0, Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
