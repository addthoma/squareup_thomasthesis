.class final Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$8;
.super Ljava/lang/Object;
.source "AccessibleKeypadCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012<\u0010\u0002\u001a8\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u001c\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u00060\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$8;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    .line 124
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$8;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-static {v0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->access$spinnerState(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$8;->call(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method
