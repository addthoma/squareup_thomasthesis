.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "RealSecureTouchAccessibilityViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityViewFactory;",
        "accessibilityPinEntryViewFactory",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryViewFactory;",
        "accessiblePinTutorialViewFactory",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialViewFactory;",
        "(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryViewFactory;Lcom/squareup/accessibility/pin/AccessiblePinTutorialViewFactory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryViewFactory;Lcom/squareup/accessibility/pin/AccessiblePinTutorialViewFactory;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accessibilityPinEntryViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessiblePinTutorialViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 12
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 13
    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 14
    new-instance p2, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory$1;

    new-array p1, p1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 15
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 16
    const-class v3, Lcom/squareup/securetouch/accessibility/GlassDialogScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    .line 17
    sget-object v4, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory$2;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 15
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    aput-object v2, p1, v1

    invoke-direct {p2, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityViewFactory$1;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x2

    aput-object p2, v0, p1

    .line 11
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
