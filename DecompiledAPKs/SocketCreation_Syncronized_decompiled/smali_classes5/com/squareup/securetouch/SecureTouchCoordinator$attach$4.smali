.class final Lcom/squareup/securetouch/SecureTouchCoordinator$attach$4;
.super Ljava/lang/Object;
.source "SecureTouchCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/workflow/legacy/Screen;",
        "+",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        ">;",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012R\u0010\u0002\u001aN\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u0012\u0004\u0012\u00020\u0008 \t*&\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/SecureTouchScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/SecureTouchCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/SecureTouchCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$4;->this$0:Lcom/squareup/securetouch/SecureTouchCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lkotlin/Pair;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/legacy/Screen;",
            "+",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;)",
            "Lcom/squareup/register/widgets/GlassSpinnerState;"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 96
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$4;->this$0:Lcom/squareup/securetouch/SecureTouchCoordinator;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/securetouch/SecureTouchScreen;

    invoke-static {v0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator;->access$spinnerState(Lcom/squareup/securetouch/SecureTouchCoordinator;Lcom/squareup/securetouch/SecureTouchScreen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$4;->call(Lkotlin/Pair;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method
