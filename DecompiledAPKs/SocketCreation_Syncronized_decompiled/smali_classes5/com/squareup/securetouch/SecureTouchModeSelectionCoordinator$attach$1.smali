.class final Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SecureTouchModeSelectionCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u001c\u0010\u0002\u001a\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$attach$1;->this$0:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$attach$1;->this$0:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    invoke-static {v0, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->access$updateView(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V

    return-void
.end method
