.class public final Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;
.super Ljava/lang/Object;
.source "SecureTouchCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J*\u0010\t\u001a\u00020\n2\"\u0010\u000b\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0008\u0012\u0004\u0012\u00020\u000e`\u00100\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;",
        "",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)V",
        "create",
        "Lcom/squareup/securetouch/SecureTouchCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/SecureTouchScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLocaleOverride"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/securetouch/SecureTouchCoordinator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/securetouch/SecureTouchCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/squareup/securetouch/SecureTouchCoordinator;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v2, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v3, p0, Lcom/squareup/securetouch/SecureTouchCoordinator$Factory;->features:Lcom/squareup/settings/server/Features;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/securetouch/SecureTouchCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method
