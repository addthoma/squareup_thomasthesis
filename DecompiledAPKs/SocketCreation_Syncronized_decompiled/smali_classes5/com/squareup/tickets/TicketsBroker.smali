.class public Lcom/squareup/tickets/TicketsBroker;
.super Ljava/lang/Object;
.source "TicketsBroker.java"


# static fields
.field private static final OMITTED_TICKETS_STATE:Z = true


# instance fields
.field private final localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

.field private final ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

.field private final ticketsService:Lcom/squareup/server/tickets/TicketsService;


# direct methods
.method constructor <init>(Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/server/tickets/TicketsService;Lcom/squareup/tickets/Tickets$InternalTickets;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/tickets/TicketsBroker;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    .line 41
    iput-object p2, p0, Lcom/squareup/tickets/TicketsBroker;->ticketsService:Lcom/squareup/server/tickets/TicketsService;

    .line 42
    iput-object p3, p0, Lcom/squareup/tickets/TicketsBroker;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    return-void
.end method

.method private static toOpenTickets(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/Ticket;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;"
        }
    .end annotation

    .line 200
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 201
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/tickets/Ticket;

    .line 202
    invoke-static {v1}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public synthetic lambda$listTickets$1$TicketsBroker(Ljava/lang/String;Lcom/squareup/util/LockWithTimeout;Lcom/squareup/tickets/TicketsCallback;Lcom/squareup/tickets/TicketsResult;)V
    .locals 4

    .line 61
    invoke-interface {p4}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/util/List;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 63
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "[Ticket_Broker_List] Requesting list tickets with %d ticket infos."

    .line 62
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    new-instance v1, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;-><init>()V

    .line 65
    invoke-virtual {v1, p4}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->known_ticket_info(Ljava/util/List;)Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;

    move-result-object p4

    .line 66
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->include_omitted_open_tickets(Ljava/lang/Boolean;)Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;

    move-result-object p4

    .line 67
    invoke-virtual {p4, p1}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->data_center_hint(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->build()Lcom/squareup/protos/client/tickets/v2/ListRequest;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 71
    invoke-virtual {p2}, Lcom/squareup/util/LockWithTimeout;->unlock()V

    .line 76
    :cond_0
    iget-object p2, p0, Lcom/squareup/tickets/TicketsBroker;->ticketsService:Lcom/squareup/server/tickets/TicketsService;

    invoke-interface {p2, p1}, Lcom/squareup/server/tickets/TicketsService;->listTickets(Lcom/squareup/protos/client/tickets/v2/ListRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    new-instance p2, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$hTvYgBvZl7hQBbBttOehcNFVlpM;

    invoke-direct {p2, p0, p3}, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$hTvYgBvZl7hQBbBttOehcNFVlpM;-><init>(Lcom/squareup/tickets/TicketsBroker;Lcom/squareup/tickets/TicketsCallback;)V

    .line 78
    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public synthetic lambda$null$0$TicketsBroker(Lcom/squareup/tickets/TicketsCallback;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    instance-of v0, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 81
    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 82
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/tickets/v2/ListResponse;

    .line 83
    iget-object v0, p2, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 84
    iget-object v0, p2, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 85
    iget-object v0, p2, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    .line 90
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v8, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v1

    const-string v1, "[Ticket_Broker_List] Received response of %d tickets from smart list tickets response. %d remote unsyced fixable tickets, %d remote unsyced unfixable tickets, ."

    .line 87
    invoke-static {v1, v8}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget-object v1, p0, Lcom/squareup/tickets/TicketsBroker;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    invoke-virtual {v1, v0}, Lcom/squareup/log/tickets/OpenTicketsLogger;->startListResponseToTicketList(I)V

    .line 93
    iget-object v0, p0, Lcom/squareup/tickets/TicketsBroker;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-interface {v0, v6, v7}, Lcom/squareup/tickets/Tickets$InternalTickets;->setRemoteUnsyncedFixableTicketCount(J)V

    .line 94
    iget-object v0, p0, Lcom/squareup/tickets/TicketsBroker;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-interface {v0, v4, v5}, Lcom/squareup/tickets/Tickets$InternalTickets;->setRemoteUnsyncedUnfixableTicketCount(J)V

    .line 95
    iget-object v0, p0, Lcom/squareup/tickets/TicketsBroker;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    iget-object p2, p2, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    invoke-static {p2}, Lcom/squareup/tickets/TicketsBroker;->toOpenTickets(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    invoke-interface {v0, p2, p1}, Lcom/squareup/tickets/Tickets$InternalTickets;->processListResponse(Ljava/util/List;Lcom/squareup/tickets/TicketsCallback;)V

    return-void

    .line 99
    :cond_0
    move-object v0, p2

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object v0

    .line 102
    instance-of v4, v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v4, :cond_1

    .line 103
    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;

    new-array p2, v1, [Ljava/lang/Object;

    .line 104
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    aput-object v0, p2, v3

    iget-object p1, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    aput-object p1, p2, v2

    const-string p1, "[Ticket_Broker_List] ERROR : %s | %s"

    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 109
    :cond_1
    instance-of v0, v0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_2

    new-array p2, v3, [Ljava/lang/Object;

    const-string v0, "[Ticket_Broker_List] Network error! Executing callback on main thread..."

    .line 110
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/tickets/TicketsResults;->of(Ljava/lang/Object;)Lcom/squareup/tickets/TicketsResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/tickets/TicketsCallback;->call(Lcom/squareup/tickets/TicketsResult;)V

    return-void

    :cond_2
    const-string p1, "[Ticket_Broker_List]"

    .line 115
    invoke-static {p2, p1}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$updateTicket$2$TicketsBroker(JLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 146
    instance-of v0, p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_0

    .line 147
    check-cast p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 148
    invoke-virtual {p3}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/client/tickets/v2/UpdateResponse;

    .line 149
    iget-object p3, p3, Lcom/squareup/protos/client/tickets/v2/UpdateResponse;->ticket:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-static {p3}, Lcom/squareup/tickets/OpenTicket;->createTicket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p3

    .line 152
    invoke-virtual {p3, p1, p2}, Lcom/squareup/tickets/OpenTicket;->setClientClockVersion(J)V

    new-array p1, v3, [Ljava/lang/Object;

    .line 155
    invoke-virtual {p3}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    invoke-virtual {p3}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v1

    const-string p2, "[Ticket_Broker_Update] Server returned ticket %s with %s itemizations."

    .line 154
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    iget-object p1, p0, Lcom/squareup/tickets/TicketsBroker;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-interface {p1, p3}, Lcom/squareup/tickets/Tickets$InternalTickets;->processUpdateResponse(Lcom/squareup/tickets/OpenTicket;)V

    return-void

    .line 161
    :cond_0
    move-object p1, p3

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    .line 162
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 164
    instance-of p2, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz p2, :cond_1

    .line 165
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/tickets/v2/UpdateResponse;

    new-array p2, v3, [Ljava/lang/Object;

    .line 166
    iget-object p3, p1, Lcom/squareup/protos/client/tickets/v2/UpdateResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p3, p3, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    aput-object p3, p2, v2

    iget-object p1, p1, Lcom/squareup/protos/client/tickets/v2/UpdateResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    aput-object p1, p2, v1

    const-string p1, "[Ticket_Broker_Update] ERROR : %s | %s"

    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    const-string p1, "[Ticket_Broker_Update]"

    .line 171
    invoke-static {p3, p1}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$updateUnsyncedTickets$3$TicketsBroker(Lcom/squareup/tickets/TicketsResult;)V
    .locals 7

    .line 182
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/TicketRowCursorList;

    .line 183
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    .line 185
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "[Ticket_Broker_Update_Unsynced] Found %d tickets that are unsynced."

    .line 184
    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    iget-object v2, p0, Lcom/squareup/tickets/TicketsBroker;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    new-instance v3, Lcom/squareup/log/tickets/UnsyncedTicketsUploaded;

    invoke-direct {v3, v0}, Lcom/squareup/log/tickets/UnsyncedTicketsUploaded;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTicketAction(Lcom/squareup/analytics/event/v1/ActionEvent;)V

    .line 189
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    .line 190
    invoke-interface {v0}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getOpenTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 192
    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getClientClockVersion()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v3, "[Ticket_Broker_Update_Unsynced] Requesting to update %s with local version %d"

    .line 191
    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    invoke-virtual {p0, v0}, Lcom/squareup/tickets/TicketsBroker;->updateTicket(Lcom/squareup/tickets/OpenTicket;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public listTickets(Lcom/squareup/tickets/TicketsCallback;Ljava/lang/String;Lcom/squareup/util/LockWithTimeout;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/util/LockWithTimeout;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Ticket_Broker_List] Hitting local store to get ticket infos for list request."

    .line 57
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/tickets/TicketsBroker;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$H0c-peOixbli8qBH6q5eeIKV8ws;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$H0c-peOixbli8qBH6q5eeIKV8ws;-><init>(Lcom/squareup/tickets/TicketsBroker;Ljava/lang/String;Lcom/squareup/util/LockWithTimeout;Lcom/squareup/tickets/TicketsCallback;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/Tickets$InternalTickets;->retrieveTicketInfo(Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method updateTicket(Lcom/squareup/tickets/OpenTicket;)V
    .locals 5

    .line 130
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getClientClockVersion()J

    move-result-wide v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 132
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getItemsCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, "[Ticket_Broker_Update] Requesting to update %s with %s itemizations."

    .line 131
    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    new-instance v2, Lcom/squareup/protos/client/tickets/v2/UpdateRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/tickets/v2/UpdateRequest$Builder;-><init>()V

    .line 136
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getTicketProtoWithoutClientClock()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/tickets/v2/UpdateRequest$Builder;->ticket(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/v2/UpdateRequest$Builder;

    move-result-object p1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/v2/UpdateRequest$Builder;->build()Lcom/squareup/protos/client/tickets/v2/UpdateRequest;

    move-result-object p1

    .line 143
    iget-object v2, p0, Lcom/squareup/tickets/TicketsBroker;->ticketsService:Lcom/squareup/server/tickets/TicketsService;

    invoke-interface {v2, p1}, Lcom/squareup/server/tickets/TicketsService;->updateTicket(Lcom/squareup/protos/client/tickets/v2/UpdateRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    new-instance v2, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$U25Zb8ZW7O7N0iOewFliXjxQO38;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$U25Zb8ZW7O7N0iOewFliXjxQO38;-><init>(Lcom/squareup/tickets/TicketsBroker;J)V

    .line 145
    invoke-virtual {p1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public updateUnsyncedTickets()V
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/squareup/tickets/TicketsBroker;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    new-instance v1, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$UrTACmlLn7Bon6HVoXkrB5GBTtw;

    invoke-direct {v1, p0}, Lcom/squareup/tickets/-$$Lambda$TicketsBroker$UrTACmlLn7Bon6HVoXkrB5GBTtw;-><init>(Lcom/squareup/tickets/TicketsBroker;)V

    invoke-interface {v0, v1}, Lcom/squareup/tickets/Tickets$InternalTickets;->retrieveNonzeroClientClockTickets(Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method
