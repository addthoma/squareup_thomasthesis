.class public final synthetic Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/tickets/RegisterTicketTask;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;

    invoke-direct {v0}, Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;-><init>()V

    sput-object v0, Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;->INSTANCE:Lcom/squareup/tickets/-$$Lambda$RealTickets$QgYrbWtNsmrvgULZWmFF7k923lg;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/tickets/RegisterTicketTask$-CC;->$default$perform(Lcom/squareup/tickets/RegisterTicketTask;Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/tickets/TicketStore;)Ljava/lang/Object;
    .locals 0

    invoke-static {p1}, Lcom/squareup/tickets/RealTickets;->lambda$retrieveNonzeroClientClockTickets$21(Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    return-object p1
.end method
