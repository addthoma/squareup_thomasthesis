.class public Lcom/squareup/tickets/VectorClocks;
.super Ljava/lang/Object;
.source "VectorClocks.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/VectorClocks$SyncAction;
    }
.end annotation


# static fields
.field static final CLIENT_CLOCK_NAME:Ljava/lang/String; = "ClientClock"

.field static final CLIENT_CLOCK_POSITION:I

.field static final CLOCK_INITIAL_VERSION:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static addClientClockIfMissing(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/Ticket;
    .locals 4

    .line 190
    iget-object v0, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/VectorClock;->clock_list:Ljava/util/List;

    .line 192
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 193
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    const-string v3, "ClientClock"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object p0

    .line 197
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 198
    invoke-static {}, Lcom/squareup/tickets/VectorClocks;->buildClientClock()Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 199
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p0

    new-instance v0, Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/tickets/VectorClock;-><init>(Ljava/util/List;)V

    .line 200
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p0

    .line 201
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p0

    return-object p0
.end method

.method static anyClockAhead(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Z
    .locals 5

    .line 146
    invoke-static {p0}, Lcom/squareup/tickets/VectorClocks;->getClockMap(Lcom/squareup/protos/client/tickets/VectorClock;)Ljava/util/Map;

    move-result-object p0

    .line 147
    invoke-static {p1}, Lcom/squareup/tickets/VectorClocks;->getClockMap(Lcom/squareup/protos/client/tickets/VectorClock;)Ljava/util/Map;

    move-result-object p1

    .line 148
    invoke-static {p0, p1}, Lcom/squareup/tickets/VectorClocks;->populateMissingClocks(Ljava/util/Map;Ljava/util/Map;)V

    .line 149
    invoke-static {p1, p0}, Lcom/squareup/tickets/VectorClocks;->populateMissingClocks(Ljava/util/Map;Ljava/util/Map;)V

    .line 151
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 152
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 153
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 155
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method static assertClientClock(Lcom/squareup/protos/client/tickets/VectorClock$Clock;)V
    .locals 3

    const-string v0, "Client Clock"

    .line 215
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 216
    iget-object v0, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    const-string v1, "ClientClock"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 217
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p0, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " found when looking for "

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static atLeastOneClockAheadAndNoneBehind(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Z
    .locals 11

    .line 121
    invoke-static {p0}, Lcom/squareup/tickets/VectorClocks;->getClockMap(Lcom/squareup/protos/client/tickets/VectorClock;)Ljava/util/Map;

    move-result-object p0

    .line 122
    invoke-static {p1}, Lcom/squareup/tickets/VectorClocks;->getClockMap(Lcom/squareup/protos/client/tickets/VectorClock;)Ljava/util/Map;

    move-result-object p1

    .line 123
    invoke-static {p0, p1}, Lcom/squareup/tickets/VectorClocks;->populateMissingClocks(Ljava/util/Map;Ljava/util/Map;)V

    .line 124
    invoke-static {p1, p0}, Lcom/squareup/tickets/VectorClocks;->populateMissingClocks(Ljava/util/Map;Ljava/util/Map;)V

    .line 129
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 130
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 131
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 133
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v10, v6, v8

    if-gez v10, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    or-int/2addr v2, v6

    .line 134
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-lez v3, :cond_1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    :goto_2
    or-int/2addr v1, v4

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method static buildClientClock()Lcom/squareup/protos/client/tickets/VectorClock$Clock;
    .locals 3

    .line 174
    new-instance v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "ClientClock"

    invoke-direct {v0, v2, v1}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method static clientClocksAreEqual(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Z
    .locals 0

    .line 169
    invoke-static {p0}, Lcom/squareup/tickets/VectorClocks;->getClientClock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    invoke-static {p1}, Lcom/squareup/tickets/VectorClocks;->getClientClock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    invoke-static {p0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method static getClientClock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/VectorClock$Clock;
    .locals 1

    .line 209
    iget-object p0, p0, Lcom/squareup/protos/client/tickets/VectorClock;->clock_list:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    .line 210
    invoke-static {p0}, Lcom/squareup/tickets/VectorClocks;->assertClientClock(Lcom/squareup/protos/client/tickets/VectorClock$Clock;)V

    return-object p0
.end method

.method static getClientlessVectorClock(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/VectorClock;
    .locals 1

    .line 182
    new-instance v0, Ljava/util/ArrayList;

    iget-object p0, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    iget-object p0, p0, Lcom/squareup/protos/client/tickets/VectorClock;->clock_list:Ljava/util/List;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 p0, 0x0

    .line 183
    invoke-interface {v0, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    .line 184
    invoke-static {p0}, Lcom/squareup/tickets/VectorClocks;->assertClientClock(Lcom/squareup/protos/client/tickets/VectorClock$Clock;)V

    .line 185
    new-instance p0, Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-direct {p0, v0}, Lcom/squareup/protos/client/tickets/VectorClock;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method private static getClockMap(Lcom/squareup/protos/client/tickets/VectorClock;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/tickets/VectorClock;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 223
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock;->clock_list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 224
    iget-object p0, p0, Lcom/squareup/protos/client/tickets/VectorClock;->clock_list:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    .line 225
    iget-object v2, v1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static getSyncActionForListRepsonse(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/tickets/VectorClocks$SyncAction;
    .locals 1

    .line 81
    invoke-static {p0, p1}, Lcom/squareup/tickets/VectorClocks;->anyClockAhead(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    sget-object p0, Lcom/squareup/tickets/VectorClocks$SyncAction;->REJECT_CHANGE_UPDATE_SERVER:Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-object p0

    .line 85
    :cond_0
    invoke-static {p1, p0}, Lcom/squareup/tickets/VectorClocks;->atLeastOneClockAheadAndNoneBehind(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 86
    sget-object p0, Lcom/squareup/tickets/VectorClocks$SyncAction;->ACCEPT_CHANGE:Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-object p0

    .line 90
    :cond_1
    sget-object p0, Lcom/squareup/tickets/VectorClocks$SyncAction;->NO_OP:Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-object p0
.end method

.method static getSyncActionForUpdateRepsonse(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/tickets/VectorClocks$SyncAction;
    .locals 0

    .line 104
    invoke-static {p0, p1}, Lcom/squareup/tickets/VectorClocks;->clientClocksAreEqual(Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/tickets/VectorClock;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 105
    sget-object p0, Lcom/squareup/tickets/VectorClocks$SyncAction;->ACCEPT_CHANGE:Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-object p0

    .line 109
    :cond_0
    sget-object p0, Lcom/squareup/tickets/VectorClocks$SyncAction;->NO_OP:Lcom/squareup/tickets/VectorClocks$SyncAction;

    return-object p0
.end method

.method private static populateMissingClocks(Ljava/util/Map;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 235
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 236
    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-wide/16 v1, 0x0

    .line 237
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method
