.class public final Lcom/squareup/tickets/RealTickets_Factory;
.super Ljava/lang/Object;
.source "RealTickets_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tickets/RealTickets;",
        ">;"
    }
.end annotation


# instance fields
.field private final executorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketsExecutor<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;>;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteUnsyncedFixableTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final remoteUnsyncedUnfixableTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tickets/TicketsService;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tickets/TicketsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketsExecutor<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/tickets/RealTickets_Factory;->ticketStoreProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/tickets/RealTickets_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/tickets/RealTickets_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/tickets/RealTickets_Factory;->serviceProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/tickets/RealTickets_Factory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/tickets/RealTickets_Factory;->remoteUnsyncedFixableTicketsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/tickets/RealTickets_Factory;->remoteUnsyncedUnfixableTicketsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/tickets/RealTickets_Factory;->executorProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/tickets/RealTickets_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/tickets/RealTickets_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p11, p0, Lcom/squareup/tickets/RealTickets_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tickets/RealTickets_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tickets/TicketsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketsExecutor<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;)",
            "Lcom/squareup/tickets/RealTickets_Factory;"
        }
    .end annotation

    .line 81
    new-instance v12, Lcom/squareup/tickets/RealTickets_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/tickets/RealTickets_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/tickets/TicketStore;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/tickets/TicketsService;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/tickets/TicketsExecutor;Lrx/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;)Lcom/squareup/tickets/RealTickets;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketStore;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/server/tickets/TicketsService;",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/tickets/TicketsExecutor<",
            "Lcom/squareup/tickets/TicketStore;",
            ">;",
            "Lrx/Scheduler;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ")",
            "Lcom/squareup/tickets/RealTickets;"
        }
    .end annotation

    .line 91
    new-instance v12, Lcom/squareup/tickets/RealTickets;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/tickets/RealTickets;-><init>(Lcom/squareup/tickets/TicketStore;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/tickets/TicketsService;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/tickets/TicketsExecutor;Lrx/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/tickets/RealTickets;
    .locals 12

    .line 69
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->ticketStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/tickets/TicketStore;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/server/tickets/TicketsService;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/log/tickets/OpenTicketsLogger;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->remoteUnsyncedFixableTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->remoteUnsyncedUnfixableTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->executorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/tickets/TicketsExecutor;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lrx/Scheduler;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/tickets/RealTickets_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static/range {v1 .. v11}, Lcom/squareup/tickets/RealTickets_Factory;->newInstance(Lcom/squareup/tickets/TicketStore;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/tickets/TicketsService;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/tickets/TicketsExecutor;Lrx/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;)Lcom/squareup/tickets/RealTickets;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/tickets/RealTickets_Factory;->get()Lcom/squareup/tickets/RealTickets;

    move-result-object v0

    return-object v0
.end method
