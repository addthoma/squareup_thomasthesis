.class Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;
.super Ljava/lang/Object;
.source "SqliteTicketStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/SqliteTicketStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryPhrases"
.end annotation


# instance fields
.field final countQueryPhrase:Lcom/squareup/phrase/Phrase;

.field final listQueryPhrase:Lcom/squareup/phrase/Phrase;


# direct methods
.method private constructor <init>(Lcom/squareup/phrase/Phrase;)V
    .locals 1

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1, v0}, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;->listQueryPhrase:Lcom/squareup/phrase/Phrase;

    .line 50
    iput-object p2, p0, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;->countQueryPhrase:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/tickets/SqliteTicketStore$1;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/tickets/SqliteTicketStore$1;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/tickets/SqliteTicketStore$QueryPhrases;-><init>(Lcom/squareup/phrase/Phrase;)V

    return-void
.end method
