.class public Lcom/squareup/tickets/NoTickets;
.super Ljava/lang/Object;
.source "NoTickets.java"

# interfaces
.implements Lcom/squareup/tickets/Tickets;
.implements Lcom/squareup/tickets/Tickets$InternalTickets;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addEmptyTicketAndLock(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;
    .locals 0

    .line 66
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot addEmptyTicketAndLock"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 0

    return-void
.end method

.method public addTicketAndLock(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;
    .locals 0

    .line 60
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot addTicketAndLock"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public additiveMergeToTicket(Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public closeTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V
    .locals 0

    return-void
.end method

.method public compTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$CompHandler;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/Tickets$CompHandler;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$CompResults;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public debugCloseAllTickets()V
    .locals 0

    return-void
.end method

.method public deleteExpiredClosedTickets()V
    .locals 0

    return-void
.end method

.method public deleteTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    return-void
.end method

.method public deleteTickets(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getBroker()Lcom/squareup/tickets/TicketsBroker;
    .locals 2

    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot getBroker"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 1

    .line 145
    new-instance v0, Lcom/squareup/tickets/NoTickets$1;

    invoke-direct {v0, p0}, Lcom/squareup/tickets/NoTickets$1;-><init>(Lcom/squareup/tickets/NoTickets;)V

    return-object v0
.end method

.method public getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getRemoteUnsyncedFixableTicketCount()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getRemoteUnsyncedUnfixableTicketCount()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getTicketAndLock(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getTicketCounts(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketCounter;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public getUnsyncedTicketCount(Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public mergeTicketsToExisting(Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public mergeTicketsToNew(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onLocalTicketsUpdated()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/LocalTicketUpdateEvent;",
            ">;>;"
        }
    .end annotation

    .line 125
    invoke-static {}, Lrx/Observable;->never()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public processListResponse(Ljava/util/List;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tickets/OpenTicket;",
            ">;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public processUpdateResponse(Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    return-void
.end method

.method public retrieveNonzeroClientClockTickets(Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public retrieveTicketInfo(Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method

.method public setRemoteUnsyncedFixableTicketCount(J)V
    .locals 0

    return-void
.end method

.method public setRemoteUnsyncedUnfixableTicketCount(J)V
    .locals 0

    return-void
.end method

.method public transferOwnership(Ljava/util/List;Lcom/squareup/permissions/EmployeeInfo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/permissions/EmployeeInfo;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public unlock(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V
    .locals 0

    return-void
.end method

.method public updateTicketAndMaintainLock(Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    return-void
.end method

.method public updateTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    return-void
.end method

.method public voidTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$VoidHandler;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/Tickets$VoidHandler;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$VoidResults;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
