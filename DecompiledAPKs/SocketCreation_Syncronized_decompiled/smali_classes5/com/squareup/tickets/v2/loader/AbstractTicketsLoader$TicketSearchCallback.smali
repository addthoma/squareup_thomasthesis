.class Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;
.source "AbstractTicketsLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketSearchCallback"
.end annotation


# instance fields
.field private final searchFilter:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/lang/String;)V
    .locals 1

    .line 290
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V

    .line 291
    iput-object p2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;->searchFilter:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getSearchFilter()Ljava/lang/String;
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;->searchFilter:Ljava/lang/String;

    return-object v0
.end method

.method protected onAborted(Lcom/squareup/tickets/TicketsResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    .line 301
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$402(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;)Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    return-void
.end method

.method protected onCompleted(Lcom/squareup/tickets/TicketsResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 296
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "[Tickets_Loader] Setting filtered ticket cursor with %d results."

    .line 295
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$402(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;)Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSearchCallback;

    return-void
.end method
