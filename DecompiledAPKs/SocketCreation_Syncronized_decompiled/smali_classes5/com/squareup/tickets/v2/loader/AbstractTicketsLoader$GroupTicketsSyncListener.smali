.class public Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;
.source "AbstractTicketsLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GroupTicketsSyncListener"
.end annotation


# instance fields
.field private final groupId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/TicketSort;",
            ")V"
        }
    .end annotation

    .line 425
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 426
    invoke-direct/range {v0 .. v5}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$SyncListener;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketSort;)V

    .line 427
    iput-object p2, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;->groupId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected fetchTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            "Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketSyncCallback;",
            ")V"
        }
    .end annotation

    .line 433
    iget-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    invoke-static {p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;->access$900(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)Lcom/squareup/tickets/Tickets;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$GroupTicketsSyncListener;->groupId:Ljava/lang/String;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/squareup/tickets/Tickets;->getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method
