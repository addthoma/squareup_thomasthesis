.class final Lcom/squareup/tmn/TmnTimings$start$1;
.super Ljava/lang/Object;
.source "TmnTimings.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/TmnTimings;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTmnTimings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TmnTimings.kt\ncom/squareup/tmn/TmnTimings$start$1\n*L\n1#1,422:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $timeNanos:J

.field final synthetic this$0:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/TmnTimings;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings$start$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    iput-wide p2, p0, Lcom/squareup/tmn/TmnTimings$start$1;->$timeNanos:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$start$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v0}, Lcom/squareup/tmn/TmnTimings;->access$getEvents$p(Lcom/squareup/tmn/TmnTimings;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "events != null when starting!"

    invoke-static {v1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$start$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v0}, Lcom/squareup/tmn/TmnTimings;->access$getFeatures$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/settings/server/Features;

    move-result-object v1

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->TMN_VERBOSE_TIMINGS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/tmn/TmnTimings;->access$setVerbose$p(Lcom/squareup/tmn/TmnTimings;Z)V

    .line 97
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$start$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/Phase;->BEFORE_TAP:Lcom/squareup/tmn/Phase;

    invoke-static {v0, v1}, Lcom/squareup/tmn/TmnTimings;->access$setCurrentPhase$p(Lcom/squareup/tmn/TmnTimings;Lcom/squareup/tmn/Phase;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$start$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    invoke-static {v0, v1}, Lcom/squareup/tmn/TmnTimings;->access$setEvents$p(Lcom/squareup/tmn/TmnTimings;Ljava/util/List;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimings$start$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimings$start$1;->$timeNanos:J

    invoke-static {v0, v1, v2}, Lcom/squareup/tmn/TmnTimings;->access$setStartTimeNanos$p(Lcom/squareup/tmn/TmnTimings;J)V

    return-void
.end method
