.class final Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;
.super Ljava/lang/Object;
.source "CardReaderHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/CardReaderHelper;->anyCardReaderHasSecureEmoneySessionObservable$impl_release()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardReaderHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardReaderHelper.kt\ncom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,159:1\n704#2:160\n777#2,2:161\n*E\n*S KotlinDebug\n*F\n+ 1 CardReaderHelper.kt\ncom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1\n*L\n49#1:160\n49#1,2:161\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u001f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\u0010\u0000\u001a\u00020\u00012(\u0010\u0002\u001a$\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "readerInfos",
        "",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "kotlin.jvm.PlatformType",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;

    invoke-direct {v0}, Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;-><init>()V

    sput-object v0, Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;->INSTANCE:Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;->apply(Ljava/util/Collection;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "readerInfos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    check-cast p1, Ljava/lang/Iterable;

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 161
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cardreader/CardReaderInfo;

    .line 49
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsFelicaCards()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 50
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->any(Ljava/lang/Iterable;)Z

    move-result p1

    return p1
.end method
