.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->render(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "+",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "it",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$2;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/dipper/events/TmnEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/dipper/events/TmnEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$2;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 170
    sget-object v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    check-cast v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->CARDREADER_DISCONNECTED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    new-instance v2, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4, v3}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/tmn/TmnTransactionOutput;

    .line 169
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$2;->invoke(Lcom/squareup/dipper/events/TmnEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
