.class public final Lcom/squareup/tmn/CardReaderHelper;
.super Ljava/lang/Object;
.source "CardReaderHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardReaderHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardReaderHelper.kt\ncom/squareup/tmn/CardReaderHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,159:1\n250#2,2:160\n*E\n*S KotlinDebug\n*F\n+ 1 CardReaderHelper.kt\ncom/squareup/tmn/CardReaderHelper\n*L\n78#1,2:160\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0012\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\r\u0010\r\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008\u000fJ\u001d\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0000\u00a2\u0006\u0002\u0008\u0015J\u0013\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0011H\u0000\u00a2\u0006\u0002\u0008\u0018J\r\u0010\u0019\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008\u001aJ\r\u0010\u001b\u001a\u00020\u0017H\u0000\u00a2\u0006\u0002\u0008\u001cJ\u001d\u0010\u001d\u001a\u00020\u000e2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0000\u00a2\u0006\u0002\u0008\"J\r\u0010#\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008$J\u0015\u0010%\u001a\u00020\u00172\u0006\u0010&\u001a\u00020\'H\u0000\u00a2\u0006\u0002\u0008(J9\u0010)\u001a\u0004\u0018\u00010\u00142\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0008\u00102\u001a\u0004\u0018\u00010\'H\u0000\u00a2\u0006\u0002\u00083J<\u00104\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u00105\u001a\u00020-2\u0006\u00106\u001a\u00020+2\u0006\u0010.\u001a\u00020/2\u0006\u00107\u001a\u0002012\n\u0008\u0002\u00102\u001a\u0004\u0018\u00010\'H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/tmn/CardReaderHelper;",
        "",
        "cardReaderHub",
        "Lcom/squareup/cardreader/CardReaderHub;",
        "activeCardReader",
        "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
        "readerSessionIds",
        "Lcom/squareup/log/ReaderSessionIds;",
        "readerEventLogger",
        "Lcom/squareup/log/ReaderEventLogger;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/CardReaderHubUtils;)V",
        "ackTmnWriteNotify",
        "",
        "ackTmnWriteNotify$impl_release",
        "activeCardReaderDisconnectedEvents",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/dipper/events/TmnEvent$TmnCardReaderConnectionEvent$OnActiveCardReaderDisconnected;",
        "cardReader",
        "Lcom/squareup/cardreader/CardReader;",
        "activeCardReaderDisconnectedEvents$impl_release",
        "anyCardReaderHasSecureEmoneySessionObservable",
        "",
        "anyCardReaderHasSecureEmoneySessionObservable$impl_release",
        "cancelTmnRequest",
        "cancelTmnRequest$impl_release",
        "hasEmoneyCompatibleReader",
        "hasEmoneyCompatibleReader$impl_release",
        "onTmnCompletion",
        "tmnTransactionResult",
        "Lcom/squareup/cardreader/lcr/TmnTransactionResult;",
        "paymentTimings",
        "Lcom/squareup/cardreader/PaymentTimings;",
        "onTmnCompletion$impl_release",
        "resetCardReader",
        "resetCardReader$impl_release",
        "sendTmnDataToCardReader",
        "tmnData",
        "",
        "sendTmnDataToCardReader$impl_release",
        "startCardReader",
        "cardBrand",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "transactionType",
        "Lcom/squareup/tmn/TmnTransactionType;",
        "transactionId",
        "",
        "amount",
        "",
        "miryoData",
        "startCardReader$impl_release",
        "startTmnTransaction",
        "tmnTransactionType",
        "brandId",
        "amountAuthorized",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final readerSessionIds:Lcom/squareup/log/ReaderSessionIds;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/CardReaderHubUtils;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardReaderHub"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activeCardReader"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerSessionIds"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerEventLogger"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHubUtils"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/CardReaderHelper;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iput-object p2, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iput-object p3, p0, Lcom/squareup/tmn/CardReaderHelper;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    iput-object p4, p0, Lcom/squareup/tmn/CardReaderHelper;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iput-object p5, p0, Lcom/squareup/tmn/CardReaderHelper;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    return-void
.end method

.method private final startTmnTransaction(Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J[B)V
    .locals 1

    .line 150
    sget-object v0, Lcom/squareup/tmn/CardReaderHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/tmn/TmnTransactionType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_4

    const/4 v0, 0x2

    if-eq p2, v0, :cond_3

    const/4 v0, 0x3

    if-eq p2, v0, :cond_2

    const/4 v0, 0x4

    if-eq p2, v0, :cond_1

    const/4 p3, 0x5

    if-eq p2, p3, :cond_0

    goto :goto_0

    .line 155
    :cond_0
    invoke-interface {p1, p7}, Lcom/squareup/cardreader/CardReader;->startTmnMiryo([B)V

    goto :goto_0

    .line 154
    :cond_1
    invoke-interface {p1, p3, p4, p5, p6}, Lcom/squareup/cardreader/CardReader;->startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    goto :goto_0

    .line 153
    :cond_2
    invoke-interface {p1, p3, p4, p5, p6}, Lcom/squareup/cardreader/CardReader;->startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    goto :goto_0

    .line 152
    :cond_3
    invoke-interface {p1, p3, p4, p5, p6}, Lcom/squareup/cardreader/CardReader;->startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    goto :goto_0

    .line 151
    :cond_4
    invoke-interface {p1, p3, p4, p5, p6}, Lcom/squareup/cardreader/CardReader;->startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    :goto_0
    return-void
.end method

.method static synthetic startTmnTransaction$default(Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J[BILjava/lang/Object;)V
    .locals 9

    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 148
    check-cast v0, [B

    move-object v8, v0

    goto :goto_0

    :cond_0
    move-object/from16 v8, p7

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/tmn/CardReaderHelper;->startTmnTransaction(Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J[B)V

    return-void
.end method


# virtual methods
.method public final ackTmnWriteNotify$impl_release()V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->ackTmnWriteNotify()V

    return-void

    .line 134
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activecardreader set after receiving ackTmnWriteNotify callback."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final activeCardReaderDisconnectedEvents$impl_release(Lcom/squareup/cardreader/CardReader;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReader;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$TmnCardReaderConnectionEvent$OnActiveCardReaderDisconnected;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->cardReaderConnectionEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$1;

    invoke-direct {v1, p1}, Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$1;-><init>(Lcom/squareup/cardreader/CardReader;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 59
    sget-object v0, Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$2;->INSTANCE:Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "cardReaderHub.cardReader\u2026eCardReaderDisconnected }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final anyCardReaderHasSecureEmoneySessionObservable$impl_release()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->cardReaderInfos()Lrx/Observable;

    move-result-object v0

    const-string v1, "cardReaderHub.cardReaderInfos()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    sget-object v1, Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;->INSTANCE:Lcom/squareup/tmn/CardReaderHelper$anyCardReaderHasSecureEmoneySessionObservable$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "cardReaderHub.cardReader\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final cancelTmnRequest$impl_release()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelTmnRequest()V

    :cond_0
    return-void
.end method

.method public final hasEmoneyCompatibleReader$impl_release()Z
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isPaymentReadyFelicaReaderConnected()Z

    move-result v0

    return v0
.end method

.method public final onTmnCompletion$impl_release(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 4

    const-string/jumbo v0, "tmnTransactionResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTimings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 123
    iget-object v1, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "activeCardReader.activeCardReader!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    .line 124
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->TMN_TRANSACTION_RESULT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    .line 122
    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 127
    iget-object p1, p0, Lcom/squareup/tmn/CardReaderHelper;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    iget-object p2, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentTerminated(Lcom/squareup/cardreader/CardReaderId;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void

    .line 119
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No activecardreader set after receiving tmncompletion callback."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final resetCardReader$impl_release()V
    .locals 3

    .line 88
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "activeCardReader.activeCardReader!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    const-string v1, "activeCardReader.activeCardReader!!.cardReaderInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "TMN resetting cardreader"

    .line 91
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 93
    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 94
    iget-object v1, p0, Lcom/squareup/tmn/CardReaderHelper;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentTerminated(Lcom/squareup/cardreader/CardReaderId;)V

    .line 95
    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    :cond_2
    return-void
.end method

.method public final sendTmnDataToCardReader$impl_release([B)Z
    .locals 1

    const-string/jumbo v0, "tmnData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->sendTmnDataToReader([B)V

    const/4 p1, 0x1

    return p1
.end method

.method public final startCardReader$impl_release(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionType;Ljava/lang/String;J[B)Lcom/squareup/cardreader/CardReader;
    .locals 10

    move-object v8, p0

    const-string v0, "cardBrand"

    move-object v3, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionType"

    move-object v2, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionId"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, v8, Lcom/squareup/tmn/CardReaderHelper;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "cardReaderHub.cardReaders"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 160
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v5, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardreader/CardReader;

    .line 78
    iget-object v7, v8, Lcom/squareup/tmn/CardReaderHelper;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    const-string v9, "it"

    invoke-static {v6, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_1
    move-object v1, v5

    .line 161
    :goto_0
    move-object v9, v1

    check-cast v9, Lcom/squareup/cardreader/CardReader;

    if-eqz v9, :cond_2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "TMN starting cardreader"

    .line 80
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    iget-object v0, v8, Lcom/squareup/tmn/CardReaderHelper;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-interface {v9}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    move-object v0, p0

    move-object v1, v9

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-wide v5, p4

    move-object/from16 v7, p6

    .line 82
    invoke-direct/range {v0 .. v7}, Lcom/squareup/tmn/CardReaderHelper;->startTmnTransaction(Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J[B)V

    goto :goto_1

    :cond_2
    move-object v9, v5

    :goto_1
    return-object v9
.end method
