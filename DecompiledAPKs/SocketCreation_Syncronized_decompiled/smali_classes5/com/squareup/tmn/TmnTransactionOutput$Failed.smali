.class public abstract Lcom/squareup/tmn/TmnTransactionOutput$Failed;
.super Lcom/squareup/tmn/TmnTransactionOutput;
.source "TmnTransactionOutput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tmn/TmnTransactionOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Failed"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;,
        Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u0011\u0008\u0002\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/tmn/TmnTransactionOutput$Failed;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "afterWriteNotifyData",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
        "(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V",
        "getAfterWriteNotifyData",
        "()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
        "CardReaderError",
        "NetworkError",
        "Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;",
        "Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;


# direct methods
.method private constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V
    .locals 1

    const/4 v0, 0x0

    .line 7
    invoke-direct {p0, v0}, Lcom/squareup/tmn/TmnTransactionOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tmn/TmnTransactionOutput$Failed;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1}, Lcom/squareup/tmn/TmnTransactionOutput$Failed;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    return-void
.end method


# virtual methods
.method public getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/tmn/TmnTransactionOutput$Failed;->afterWriteNotifyData:Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    return-object v0
.end method
