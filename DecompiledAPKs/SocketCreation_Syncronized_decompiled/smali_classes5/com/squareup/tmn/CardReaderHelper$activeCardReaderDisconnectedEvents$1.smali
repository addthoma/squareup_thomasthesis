.class final Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$1;
.super Ljava/lang/Object;
.source "CardReaderHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/CardReaderHelper;->activeCardReaderDisconnectedEvents$impl_release(Lcom/squareup/cardreader/CardReader;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/dipper/events/CardReaderConnectionEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/dipper/events/CardReaderConnectionEvent;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cardReader:Lcom/squareup/cardreader/CardReader;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$1;->$cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/dipper/events/CardReaderConnectionEvent;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    instance-of v0, p1, Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderDisconnected;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/dipper/events/CardReaderConnectionEvent;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$1;->$cardReader:Lcom/squareup/cardreader/CardReader;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/dipper/events/CardReaderConnectionEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/CardReaderHelper$activeCardReaderDisconnectedEvents$1;->test(Lcom/squareup/dipper/events/CardReaderConnectionEvent;)Z

    move-result p1

    return p1
.end method
