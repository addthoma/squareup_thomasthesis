.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;
.super Ljava/lang/Object;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getPaymentStarterWorker(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionType;J[B)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lio/reactivex/ObservableSource<",
        "+TT;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTmnTransactionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTmnTransactionWorkflow.kt\ncom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1\n*L\n1#1,786:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $amountAuthorized:J

.field final synthetic $brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field final synthetic $miryoData:[B

.field final synthetic $transactionType:Lcom/squareup/tmn/TmnTransactionType;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionType;J[B)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$transactionType:Lcom/squareup/tmn/TmnTransactionType;

    iput-wide p4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$amountAuthorized:J

    iput-object p6, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$miryoData:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lio/reactivex/Observable;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
            ">;"
        }
    .end annotation

    .line 621
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$generateTransactionId(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Ljava/lang/String;

    move-result-object v0

    .line 622
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tmn/TmnTimings;->start()V

    .line 623
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v1

    sget-object v2, Lcom/squareup/tmn/What;->WORKFLOW_STARTING_PAYMENT_ON_READER:Lcom/squareup/tmn/What;

    invoke-virtual {v1, v2}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 624
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object v10

    .line 625
    new-instance v11, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 626
    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->STARTING_PAYMENT_ON_READER:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v1}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3c

    const/4 v9, 0x0

    move-object v1, v11

    move-object v3, v0

    .line 625
    invoke-direct/range {v1 .. v9}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v11, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 624
    invoke-virtual {v10, v11}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 629
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getCardReaderHelper$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/CardReaderHelper;

    move-result-object v1

    .line 630
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$transactionType:Lcom/squareup/tmn/TmnTransactionType;

    iget-wide v5, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$amountAuthorized:J

    iget-object v7, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->$miryoData:[B

    move-object v4, v0

    .line 629
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/tmn/CardReaderHelper;->startCardReader$impl_release(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionType;Ljava/lang/String;J[B)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 632
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getBleRate()Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 633
    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v3

    const-string v4, "it"

    .line 634
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->getConnectionIntervalMillis()I

    move-result v4

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->getMtuSize()I

    move-result v2

    .line 633
    invoke-virtual {v3, v4, v2}, Lcom/squareup/tmn/TmnTimings;->params(II)V

    .line 637
    :cond_0
    new-instance v2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;

    invoke-direct {v2, v1, v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;-><init>(Lcom/squareup/cardreader/CardReader;Ljava/lang/String;)V

    invoke-static {v2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;->call()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
