.class public final Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;
.super Ljava/lang/Object;
.source "SetupGuideCompletionDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupGuideCompletionDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J.\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\n\u0008\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0011J\u0013\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0008\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;",
        "",
        "titleId",
        "",
        "messageId",
        "iconId",
        "(ILjava/lang/Integer;I)V",
        "getIconId",
        "()I",
        "getMessageId",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getTitleId",
        "component1",
        "component2",
        "component3",
        "copy",
        "(ILjava/lang/Integer;I)Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final iconId:I

.field private final messageId:Ljava/lang/Integer;

.field private final titleId:I


# direct methods
.method public constructor <init>(ILjava/lang/Integer;I)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    iput-object p2, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    iput p3, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;ILjava/lang/Integer;IILjava/lang/Object;)Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->copy(ILjava/lang/Integer;I)Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    return v0
.end method

.method public final component2()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    return v0
.end method

.method public final copy(ILjava/lang/Integer;I)Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;
    .locals 1

    new-instance v0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;-><init>(ILjava/lang/Integer;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    iget v1, p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    iget p1, p1, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIconId()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    return v0
.end method

.method public final getMessageId()Ljava/lang/Integer;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getTitleId()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->titleId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", messageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->messageId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", iconId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/setupguide/SetupGuideCompletionDialog$ScreenData;->iconId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
