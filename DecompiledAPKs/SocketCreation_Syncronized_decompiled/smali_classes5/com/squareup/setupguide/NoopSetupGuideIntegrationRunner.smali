.class public final Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner;
.super Ljava/lang/Object;
.source "NoopSetupGuideIntegrationRunner.kt"

# interfaces
.implements Lcom/squareup/setupguide/SetupGuideIntegrationRunner;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner;",
        "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
        "()V",
        "handleCompletion",
        "Lio/reactivex/Completable;",
        "name",
        "Lcom/squareup/protos/checklist/common/ActionItemName;",
        "parentTreeKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "doIfCompleted",
        "Lkotlin/Function0;",
        "",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner;

    invoke-direct {v0}, Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner;-><init>()V

    sput-object v0, Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner;->INSTANCE:Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "parentTreeKey"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "doIfCompleted"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    new-instance p1, Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner$sam$io_reactivex_functions_Action$0;

    invoke-direct {p1, p3}, Lcom/squareup/setupguide/NoopSetupGuideIntegrationRunner$sam$io_reactivex_functions_Action$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lio/reactivex/functions/Action;

    invoke-static {p1}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    const-string p2, "Completable.fromAction(doIfCompleted)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
