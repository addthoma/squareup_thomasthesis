.class public interface abstract Lcom/squareup/setupguide/SetupGuideConfiguration;
.super Ljava/lang/Object;
.source "SetupGuideConfiguration.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0012\u0010\u000e\u001a\u00020\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0012\u0010\u0012\u001a\u00020\u0013X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideConfiguration;",
        "",
        "actionBarGlyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getActionBarGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "content",
        "",
        "Lcom/squareup/setupguide/SetupGuideContent;",
        "getContent",
        "()Ljava/util/List;",
        "linkedContent",
        "getLinkedContent",
        "()Lcom/squareup/setupguide/SetupGuideContent;",
        "showBusinessIntro",
        "",
        "getShowBusinessIntro",
        "()Z",
        "variant",
        "Lcom/squareup/protos/checklist/service/Variant;",
        "getVariant",
        "()Lcom/squareup/protos/checklist/service/Variant;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getActionBarGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
.end method

.method public abstract getContent()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/setupguide/SetupGuideContent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLinkedContent()Lcom/squareup/setupguide/SetupGuideContent;
.end method

.method public abstract getShowBusinessIntro()Z
.end method

.method public abstract getVariant()Lcom/squareup/protos/checklist/service/Variant;
.end method
