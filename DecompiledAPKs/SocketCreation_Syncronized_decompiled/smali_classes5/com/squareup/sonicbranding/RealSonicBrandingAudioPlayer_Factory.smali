.class public final Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;
.super Ljava/lang/Object;
.source "RealSonicBrandingAudioPlayer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/brandaudio/BrandAudioPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/brandaudio/BrandAudioPlayer;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/brandaudio/BrandAudioPlayer;",
            ">;)",
            "Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/brandaudio/BrandAudioPlayer;)Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;

    invoke-direct {v0, p0}, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;-><init>(Lcom/squareup/brandaudio/BrandAudioPlayer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/brandaudio/BrandAudioPlayer;

    invoke-static {v0}, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;->newInstance(Lcom/squareup/brandaudio/BrandAudioPlayer;)Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer_Factory;->get()Lcom/squareup/sonicbranding/RealSonicBrandingAudioPlayer;

    move-result-object v0

    return-object v0
.end method
