.class public final Lcom/squareup/thread/executor/Executors;
.super Ljava/lang/Object;
.source "Executors.java"


# static fields
.field private static final DEFAULT_CACHE_TIMEOUT_SECONDS:J = 0x3cL

.field private static final DEFAULT_MAX_THREAD_COUNT:I = 0x5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newBoundedCachedThreadPool(IJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 9

    .line 65
    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 67
    new-instance v8, Ljava/util/concurrent/ThreadPoolExecutor;

    move-object v0, v8

    move v1, p0

    move v2, p0

    move-wide v3, p1

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    const/4 p0, 0x1

    .line 72
    invoke-virtual {v8, p0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 74
    invoke-static {v8}, Lcom/squareup/thread/executor/ExecutorRegistry;->register(Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/ThreadPoolExecutor;

    return-object p0
.end method

.method public static newBoundedCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 4

    .line 36
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v1, 0x5

    const-wide/16 v2, 0x3c

    invoke-static {v1, v2, v3, v0, p0}, Lcom/squareup/thread/executor/Executors;->newBoundedCachedThreadPool(IJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object p0

    return-object p0
.end method

.method public static newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    .line 79
    invoke-static {p0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    .line 78
    invoke-static {p0}, Lcom/squareup/thread/executor/ExecutorRegistry;->register(Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    return-object p0
.end method

.method public static newSingleThreadExecutor(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    .line 84
    invoke-static {p0}, Lcom/squareup/thread/Threads;->namedThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object p0

    invoke-static {p0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    .line 83
    invoke-static {p0}, Lcom/squareup/thread/executor/ExecutorRegistry;->register(Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    return-object p0
.end method

.method public static stoppableHandlerExecutor(Landroid/os/Handler;Z)Lcom/squareup/thread/executor/StoppableHandlerExecutor;
    .locals 1

    .line 102
    new-instance v0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;-><init>(Landroid/os/Handler;Z)V

    invoke-static {v0}, Lcom/squareup/thread/executor/ExecutorRegistry;->register(Lcom/squareup/thread/executor/StoppableSerialExecutor;)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object p0

    check-cast p0, Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    return-object p0
.end method

.method public static stoppableMainThreadExecutor()Lcom/squareup/thread/executor/StoppableHandlerExecutor;
    .locals 2

    .line 97
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/thread/executor/Executors;->stoppableHandlerExecutor(Landroid/os/Handler;Z)Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    move-result-object v0

    return-object v0
.end method

.method public static stoppableNamedThreadExecutor(Ljava/lang/String;IZ)Lcom/squareup/thread/executor/StoppableSerialExecutor;
    .locals 3

    .line 89
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sq-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 91
    invoke-virtual {v0, p2}, Landroid/os/HandlerThread;->setDaemon(Z)V

    .line 92
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 93
    new-instance p0, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const/4 p1, 0x1

    invoke-static {p0, p1}, Lcom/squareup/thread/executor/Executors;->stoppableHandlerExecutor(Landroid/os/Handler;Z)Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    move-result-object p0

    return-object p0
.end method
