.class public final Lcom/squareup/thread/Rx2Schedulers;
.super Ljava/lang/Object;
.source "Rx2Schedulers.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006R\u001c\u0010\t\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\n\u0010\u0002\u001a\u0004\u0008\u000b\u0010\u0006R\u0011\u0010\u000c\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/thread/Rx2Schedulers;",
        "",
        "()V",
        "computation",
        "Lio/reactivex/Scheduler;",
        "getComputation",
        "()Lio/reactivex/Scheduler;",
        "io",
        "getIo",
        "legacyMainScheduler",
        "legacyMainScheduler$annotations",
        "getLegacyMainScheduler",
        "main",
        "getMain",
        "impl-rx2_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/thread/Rx2Schedulers;

.field private static final legacyMainScheduler:Lio/reactivex/Scheduler;

.field private static final main:Lio/reactivex/Scheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 8
    new-instance v0, Lcom/squareup/thread/Rx2Schedulers;

    invoke-direct {v0}, Lcom/squareup/thread/Rx2Schedulers;-><init>()V

    sput-object v0, Lcom/squareup/thread/Rx2Schedulers;->INSTANCE:Lcom/squareup/thread/Rx2Schedulers;

    .line 20
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lio/reactivex/android/schedulers/AndroidSchedulers;->from(Landroid/os/Looper;Z)Lio/reactivex/Scheduler;

    move-result-object v0

    const-string v1, "AndroidSchedulers.from(L\u2026er.getMainLooper(), true)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/thread/Rx2Schedulers;->main:Lio/reactivex/Scheduler;

    .line 26
    invoke-static {}, Lcom/squareup/thread/RxSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v0

    const-string v1, "RxSchedulers.mainThread()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/thread/Rx2Schedulers;->legacyMainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic legacyMainScheduler$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Immediate scheduling in RxJava could lead to stackoverflows."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "main"
            imports = {}
        .end subannotation
    .end annotation

    return-void
.end method


# virtual methods
.method public final getComputation()Lio/reactivex/Scheduler;
    .locals 2

    .line 10
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v0

    const-string v1, "Schedulers.computation()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getIo()Lio/reactivex/Scheduler;
    .locals 2

    .line 9
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v0

    const-string v1, "Schedulers.io()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getLegacyMainScheduler()Lio/reactivex/Scheduler;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/thread/Rx2Schedulers;->legacyMainScheduler:Lio/reactivex/Scheduler;

    return-object v0
.end method

.method public final getMain()Lio/reactivex/Scheduler;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/thread/Rx2Schedulers;->main:Lio/reactivex/Scheduler;

    return-object v0
.end method
