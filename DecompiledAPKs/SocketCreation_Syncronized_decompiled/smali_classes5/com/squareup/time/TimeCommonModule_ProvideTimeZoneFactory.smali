.class public final Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;
.super Ljava/lang/Object;
.source "TimeCommonModule_ProvideTimeZoneFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/TimeZone;",
        ">;"
    }
.end annotation


# instance fields
.field private final currentTimeZoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;->currentTimeZoneProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;)",
            "Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;

    invoke-direct {v0, p0}, Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTimeZone(Lcom/squareup/time/CurrentTimeZone;)Ljava/util/TimeZone;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/time/TimeCommonModule;->provideTimeZone(Lcom/squareup/time/CurrentTimeZone;)Ljava/util/TimeZone;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/TimeZone;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;->get()Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/TimeZone;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;->currentTimeZoneProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/time/CurrentTimeZone;

    invoke-static {v0}, Lcom/squareup/time/TimeCommonModule_ProvideTimeZoneFactory;->provideTimeZone(Lcom/squareup/time/CurrentTimeZone;)Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method
