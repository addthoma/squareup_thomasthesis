.class public final Lcom/squareup/time/CurrentTime$DefaultImpls;
.super Ljava/lang/Object;
.source "CurrentTime.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/time/CurrentTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static instant(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/Instant;
    .locals 1

    .line 13
    invoke-interface {p0}, Lcom/squareup/time/CurrentTime;->clock()Lorg/threeten/bp/Clock;

    move-result-object p0

    invoke-virtual {p0}, Lorg/threeten/bp/Clock;->instant()Lorg/threeten/bp/Instant;

    move-result-object p0

    const-string v0, "clock().instant()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static localDate(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 14
    invoke-interface {p0}, Lcom/squareup/time/CurrentTime;->clock()Lorg/threeten/bp/Clock;

    move-result-object p0

    invoke-static {p0}, Lorg/threeten/bp/LocalDate;->now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "LocalDate.now(clock())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static localDateTime(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/LocalDateTime;
    .locals 1

    .line 16
    invoke-interface {p0}, Lcom/squareup/time/CurrentTime;->clock()Lorg/threeten/bp/Clock;

    move-result-object p0

    invoke-static {p0}, Lorg/threeten/bp/LocalDateTime;->now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDateTime;

    move-result-object p0

    const-string v0, "LocalDateTime.now(clock())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static localTime(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 15
    invoke-interface {p0}, Lcom/squareup/time/CurrentTime;->clock()Lorg/threeten/bp/Clock;

    move-result-object p0

    invoke-static {p0}, Lorg/threeten/bp/LocalTime;->now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalTime;

    move-result-object p0

    const-string v0, "LocalTime.now(clock())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static zonedDateTime(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .line 17
    invoke-interface {p0}, Lcom/squareup/time/CurrentTime;->clock()Lorg/threeten/bp/Clock;

    move-result-object p0

    invoke-static {p0}, Lorg/threeten/bp/ZonedDateTime;->now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p0

    const-string v0, "ZonedDateTime.now(clock())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
