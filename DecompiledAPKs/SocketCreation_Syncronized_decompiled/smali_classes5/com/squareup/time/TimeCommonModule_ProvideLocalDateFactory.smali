.class public final Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;
.super Ljava/lang/Object;
.source "TimeCommonModule_ProvideLocalDateFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lorg/threeten/bp/LocalDate;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lorg/threeten/bp/Clock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lorg/threeten/bp/Clock;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lorg/threeten/bp/Clock;",
            ">;)",
            "Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;

    invoke-direct {v0, p0}, Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLocalDate(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/time/TimeCommonModule;->INSTANCE:Lcom/squareup/time/TimeCommonModule;

    invoke-virtual {v0, p0}, Lcom/squareup/time/TimeCommonModule;->provideLocalDate(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/threeten/bp/LocalDate;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;->get()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public get()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/Clock;

    invoke-static {v0}, Lcom/squareup/time/TimeCommonModule_ProvideLocalDateFactory;->provideLocalDate(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method
