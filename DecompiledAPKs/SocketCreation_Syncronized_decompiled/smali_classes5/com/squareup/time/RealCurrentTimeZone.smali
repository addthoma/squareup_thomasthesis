.class public final Lcom/squareup/time/RealCurrentTimeZone;
.super Ljava/lang/Object;
.source "RealCurrentTimeZone.kt"

# interfaces
.implements Lcom/squareup/time/CurrentTimeZone;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0007H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/time/RealCurrentTimeZone;",
        "Lcom/squareup/time/CurrentTimeZone;",
        "timeInfoChangedMonitor",
        "Lcom/squareup/time/TimeInfoChangedMonitor;",
        "(Lcom/squareup/time/TimeInfoChangedMonitor;)V",
        "zoneId",
        "Lio/reactivex/Observable;",
        "Lorg/threeten/bp/ZoneId;",
        "getZoneId",
        "()Lio/reactivex/Observable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final timeInfoChangedMonitor:Lcom/squareup/time/TimeInfoChangedMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/time/TimeInfoChangedMonitor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "timeInfoChangedMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/time/RealCurrentTimeZone;->timeInfoChangedMonitor:Lcom/squareup/time/TimeInfoChangedMonitor;

    return-void
.end method


# virtual methods
.method public getZoneId()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lorg/threeten/bp/ZoneId;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/time/RealCurrentTimeZone;->timeInfoChangedMonitor:Lcom/squareup/time/TimeInfoChangedMonitor;

    invoke-virtual {v0}, Lcom/squareup/time/TimeInfoChangedMonitor;->getTimeZoneId()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public zoneId()Lorg/threeten/bp/ZoneId;
    .locals 2

    .line 14
    invoke-static {}, Lorg/threeten/bp/ZoneId;->systemDefault()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    const-string v1, "ZoneId.systemDefault()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
