.class public final Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;
.super Ljava/lang/Object;
.source "TenderSettingsManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/TenderSettingsManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineStoreRestrictionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->onlineStoreRestrictionsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)",
            "Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/tenderpayment/TenderSettingsManager;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/TenderSettingsManager;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->onlineStoreRestrictionsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-static {v0, v1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/tenderpayment/TenderSettingsManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/TenderSettingsManager_Factory;->get()Lcom/squareup/tenderpayment/TenderSettingsManager;

    move-result-object v0

    return-object v0
.end method
