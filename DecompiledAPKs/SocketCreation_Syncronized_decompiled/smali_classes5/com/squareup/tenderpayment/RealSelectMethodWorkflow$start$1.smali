.class final synthetic Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function6;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->start(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;Lcom/squareup/workflow/legacy/Screen$Key;ZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function6<",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
        "Lcom/squareup/protos/common/Money;",
        "Lcom/squareup/connectivity/InternetState;",
        "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
        "Ljava/util/EnumSet<",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0015\u0010\u0002\u001a\u00110\u0003\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00062\u0015\u0010\u0007\u001a\u00110\u0008\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\t2\u0015\u0010\n\u001a\u00110\u000b\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u000c2\u0015\u0010\r\u001a\u00110\u000e\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u000f2\u001b\u0010\u0010\u001a\u0017\u0012\u0004\u0012\u00020\u00120\u0011\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00132\u0015\u0010\u0014\u001a\u00110\u0015\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u0016\u00a2\u0006\u0002\u0008\u0017"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "p1",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
        "Lkotlin/ParameterName;",
        "name",
        "nfcState",
        "p2",
        "Lcom/squareup/protos/common/Money;",
        "displayedAmount",
        "p3",
        "Lcom/squareup/connectivity/InternetState;",
        "internetState",
        "p4",
        "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
        "toast",
        "p5",
        "Ljava/util/EnumSet;",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        "readerCapabilities",
        "p6",
        "",
        "buyerCheckoutEnabled",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 1

    const/4 v0, 0x6

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "updateSelectMethodScreen"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "updateSelectMethodScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;Lcom/squareup/connectivity/InternetState;Lcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;"

    return-object v0
.end method

.method public final invoke(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;Lcom/squareup/connectivity/InternetState;Lcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/connectivity/InternetState;",
            "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;Z)",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$1;->receiver:Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    .line 745
    invoke-static/range {v1 .. v7}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$updateSelectMethodScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;Lcom/squareup/connectivity/InternetState;Lcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .line 229
    move-object v1, p1

    check-cast v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    move-object v2, p2

    check-cast v2, Lcom/squareup/protos/common/Money;

    move-object v3, p3

    check-cast v3, Lcom/squareup/connectivity/InternetState;

    move-object v4, p4

    check-cast v4, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    move-object v5, p5

    check-cast v5, Ljava/util/EnumSet;

    check-cast p6, Ljava/lang/Boolean;

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$1;->invoke(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;Lcom/squareup/connectivity/InternetState;Lcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p1

    return-object p1
.end method
