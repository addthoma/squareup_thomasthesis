.class synthetic Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;
.super Ljava/lang/Object;
.source "SelectMethodWorkflowRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

.field static final synthetic $SwitchMap$com$squareup$tenderpayment$RealSelectMethodWorkflow$NfcState:[I

.field static final synthetic $SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderAvailability:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 656
    invoke-static {}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->values()[Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$RealSelectMethodWorkflow$NfcState:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$RealSelectMethodWorkflow$NfcState:[I

    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->DISABLED:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$RealSelectMethodWorkflow$NfcState:[I

    sget-object v3, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE_WITHOUT_TAP:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$RealSelectMethodWorkflow$NfcState:[I

    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v4}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$RealSelectMethodWorkflow$NfcState:[I

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->TIMED_OUT:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v5}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 225
    :catch_3
    invoke-static {}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->values()[Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    :try_start_4
    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    sget-object v5, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v5}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    sget-object v5, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v5}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v5

    aput v1, v4, v5
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    sget-object v5, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v5}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v5

    aput v2, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    sget-object v5, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v5}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v3, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$protos$client$devicesettings$TenderSettings$TenderType:[I

    sget-object v4, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v4

    const/4 v5, 0x5

    aput v5, v3, v4
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    .line 185
    :catch_8
    invoke-static {}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->values()[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderAvailability:[I

    :try_start_9
    sget-object v3, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderAvailability:[I

    sget-object v4, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ALWAYS:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    invoke-virtual {v4}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderAvailability:[I

    sget-object v3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->OFFLINE:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowRenderer$1;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderAvailability:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ONLINE_ONLY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderAvailability;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    return-void
.end method
