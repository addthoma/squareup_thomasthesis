.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;
.super Lcom/squareup/tenderpayment/SelectMethodWorkflowState;
.source "SelectMethodWorkflowState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectMethodState"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008+\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0083\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010\u0012\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\u0005\u0012\u0006\u0010\u0018\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0019J\t\u0010/\u001a\u00020\u0003H\u00c6\u0003J\u000f\u00100\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u00c6\u0003J\t\u00101\u001a\u00020\u0005H\u00c6\u0003J\t\u00102\u001a\u00020\u0005H\u00c6\u0003J\t\u00103\u001a\u00020\u0005H\u00c6\u0003J\t\u00104\u001a\u00020\u0005H\u00c6\u0003J\t\u00105\u001a\u00020\u0005H\u00c6\u0003J\u000b\u00106\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\t\u00107\u001a\u00020\tH\u00c6\u0003J\t\u00108\u001a\u00020\u0007H\u00c6\u0003J\t\u00109\u001a\u00020\u0005H\u00c6\u0003J\t\u0010:\u001a\u00020\u0005H\u00c6\u0003J\t\u0010;\u001a\u00020\u000eH\u00c6\u0003J\u000f\u0010<\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010H\u00c6\u0003J\u00a3\u0001\u0010=\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00052\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e2\u000e\u0008\u0002\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u000e\u0008\u0002\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00132\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010>\u001a\u00020\u00052\u0008\u0010?\u001a\u0004\u0018\u00010@H\u00d6\u0003J\t\u0010A\u001a\u00020BH\u00d6\u0001J\t\u0010C\u001a\u00020DH\u00d6\u0001R\u0011\u0010\u0015\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0017\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u001bR\u0011\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u001bR\u0011\u0010\u0018\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u001bR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\u0016\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u001bR\u0017\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R\u0014\u0010\'\u001a\u00020\u0000X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010\u001bR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010\u001fR\u0011\u0010\u0017\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010\u001bR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.\u00a8\u0006E"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowState;",
        "readerSessionId",
        "Ljava/util/UUID;",
        "showSecondaryMethods",
        "",
        "splitTenderAmount",
        "Lcom/squareup/protos/common/Money;",
        "nfcState",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
        "displayedAmount",
        "isConnectedToNetwork",
        "isInOfflineMode",
        "toastData",
        "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
        "readerCapabilities",
        "Ljava/util/EnumSet;",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "buyerCheckoutEnabled",
        "preAuthTipRequired",
        "tipEnabled",
        "isRunningState",
        "(Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZ)V",
        "getBuyerCheckoutEnabled",
        "()Z",
        "getCompletedTenders",
        "()Ljava/util/List;",
        "getDisplayedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getNfcState",
        "()Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
        "getPreAuthTipRequired",
        "getReaderCapabilities",
        "()Ljava/util/EnumSet;",
        "getReaderSessionId",
        "()Ljava/util/UUID;",
        "selectMethodState",
        "getSelectMethodState",
        "()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;",
        "getShowSecondaryMethods",
        "getSplitTenderAmount",
        "getTipEnabled",
        "getToastData",
        "()Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerCheckoutEnabled:Z

.field private final completedTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final displayedAmount:Lcom/squareup/protos/common/Money;

.field private final isConnectedToNetwork:Z

.field private final isInOfflineMode:Z

.field private final isRunningState:Z

.field private final nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

.field private final preAuthTipRequired:Z

.field private final readerCapabilities:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation
.end field

.field private final readerSessionId:Ljava/util/UUID;

.field private final selectMethodState:Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

.field private final showSecondaryMethods:Z

.field private final splitTenderAmount:Lcom/squareup/protos/common/Money;

.field private final tipEnabled:Z

.field private final toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;


# direct methods
.method public constructor <init>(Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
            "Lcom/squareup/protos/common/Money;",
            "ZZ",
            "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;ZZZZ)V"
        }
    .end annotation

    const-string v0, "readerSessionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nfcState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayedAmount"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toastData"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerCapabilities"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    iput-boolean p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    iput-boolean p6, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    iput-boolean p7, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    iput-object p10, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    iput-boolean p11, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    iput-boolean p12, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    iput-boolean p13, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    iput-boolean p14, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    .line 30
    move-object p1, p0

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->selectMethodState:Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZILjava/lang/Object;)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;
    .locals 15

    move-object v0, p0

    move/from16 v1, p15

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-boolean v12, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-boolean v13, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    goto :goto_b

    :cond_b
    move/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-boolean v14, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    iget-boolean v1, v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    goto :goto_d

    :cond_d
    move/from16 v1, p14

    :goto_d
    move-object/from16 p1, v2

    move/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v13

    move/from16 p13, v14

    move/from16 p14, v1

    invoke-virtual/range {p0 .. p14}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->copy(Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZ)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/util/UUID;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    return-object v0
.end method

.method public final component10()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final component11()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    return v0
.end method

.method public final component12()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    return v0
.end method

.method public final component13()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    return v0
.end method

.method public final component14()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    return v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    return v0
.end method

.method public final component8()Lcom/squareup/tenderpayment/SelectMethod$ToastData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    return-object v0
.end method

.method public final component9()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    return-object v0
.end method

.method public final copy(Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZ)Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
            "Lcom/squareup/protos/common/Money;",
            "ZZ",
            "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;ZZZZ)",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;"
        }
    .end annotation

    const-string v0, "readerSessionId"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nfcState"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayedAmount"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toastData"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerCapabilities"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    move-object v1, v0

    move/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move/from16 v15, p14

    invoke-direct/range {v1 .. v15}, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;-><init>(Ljava/util/UUID;ZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Ljava/util/List;ZZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    iget-boolean p1, p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBuyerCheckoutEnabled()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    return v0
.end method

.method public final getCompletedTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final getDisplayedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getNfcState()Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    return-object v0
.end method

.method public final getPreAuthTipRequired()Z
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    return v0
.end method

.method public final getReaderCapabilities()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    return-object v0
.end method

.method public final getReaderSessionId()Ljava/util/UUID;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    return-object v0
.end method

.method public getSelectMethodState()Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->selectMethodState:Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;

    return-object v0
.end method

.method public final getShowSecondaryMethods()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    return v0
.end method

.method public final getSplitTenderAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getTipEnabled()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    return v0
.end method

.method public final getToastData()Lcom/squareup/tenderpayment/SelectMethod$ToastData;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_8
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :cond_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :cond_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    if-eqz v1, :cond_c

    const/4 v1, 0x1

    :cond_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    :cond_d
    add-int/2addr v0, v1

    return v0
.end method

.method public final isConnectedToNetwork()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    return v0
.end method

.method public final isInOfflineMode()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    return v0
.end method

.method public final isRunningState()Z
    .locals 1

    .line 28
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectMethodState(readerSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerSessionId:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSecondaryMethods="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->showSecondaryMethods:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", splitTenderAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", nfcState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->nfcState:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", displayedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->displayedAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isConnectedToNetwork="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isConnectedToNetwork:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isInOfflineMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isInOfflineMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", toastData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", readerCapabilities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->readerCapabilities:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", completedTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->completedTenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", buyerCheckoutEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->buyerCheckoutEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", preAuthTipRequired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->preAuthTipRequired:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", tipEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->tipEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isRunningState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowState$SelectMethodState;->isRunningState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
