.class public final enum Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;
.super Ljava/lang/Enum;
.source "TenderCompleter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderCompleter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CancelPaymentResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

.field public static final enum ENDED:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

.field public static final enum ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

.field public static final enum ENDED_INVOICE_PAYMENT:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 29
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    const/4 v1, 0x0

    const-string v2, "ENDED_INVOICE_PAYMENT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_INVOICE_PAYMENT:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    .line 30
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    const/4 v2, 0x1

    const-string v3, "ENDED_API_TRANSACTION"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    .line 31
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    const/4 v3, 0x2

    const-string v4, "ENDED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    .line 28
    sget-object v4, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_INVOICE_PAYMENT:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->$VALUES:[Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->$VALUES:[Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    invoke-virtual {v0}, [Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    return-object v0
.end method
