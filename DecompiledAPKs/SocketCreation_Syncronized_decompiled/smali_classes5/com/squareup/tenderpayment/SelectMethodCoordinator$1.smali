.class Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;
.super Ljava/lang/Object;
.source "SelectMethodCoordinator.java"

# interfaces
.implements Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodCoordinator;->lambda$null$7(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodCoordinator;

.field final synthetic val$scrollView:Lcom/squareup/widgets/InteractiveScrollView;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;Lcom/squareup/widgets/InteractiveScrollView;)V
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodCoordinator;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;->val$scrollView:Lcom/squareup/widgets/InteractiveScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollTopNotReached()V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;->val$scrollView:Lcom/squareup/widgets/InteractiveScrollView;

    invoke-virtual {v0}, Lcom/squareup/widgets/InteractiveScrollView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;->val$scrollView:Lcom/squareup/widgets/InteractiveScrollView;

    sget v1, Lcom/squareup/noho/R$drawable;->noho_border_light:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/InteractiveScrollView;->setBackgroundResource(I)V

    .line 193
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;->val$scrollView:Lcom/squareup/widgets/InteractiveScrollView;

    invoke-virtual {v0}, Lcom/squareup/widgets/InteractiveScrollView;->invalidate()V

    :cond_0
    return-void
.end method

.method public onScrollTopReached()V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;->val$scrollView:Lcom/squareup/widgets/InteractiveScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/InteractiveScrollView;->setBackgroundResource(I)V

    .line 187
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;->val$scrollView:Lcom/squareup/widgets/InteractiveScrollView;

    invoke-virtual {v0}, Lcom/squareup/widgets/InteractiveScrollView;->invalidate()V

    return-void
.end method
