.class public final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final changeHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final completerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final gateKeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentEventHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final screenDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final selectMethodBuyerCheckoutEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            ">;"
        }
    .end annotation
.end field

.field private final selectMethodScreensFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final showModalListProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final swipeValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final touchEventMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 126
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 127
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->completerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 128
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 129
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 130
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->paymentEventHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 131
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 132
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 133
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 134
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 135
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 136
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 137
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 138
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->gateKeeperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 139
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->busProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 140
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 141
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 142
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 143
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 144
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 145
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 146
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 147
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 148
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 149
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 150
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->showModalListProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 151
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 152
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 153
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->selectMethodBuyerCheckoutEnabledProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 154
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->selectMethodScreensFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 155
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->screenDataRendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;
    .locals 32
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ">;)",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    .line 188
    new-instance v31, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;

    move-object/from16 v0, v31

    invoke-direct/range {v0 .. v30}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v31
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;
    .locals 32
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/tender/TenderFactory;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/TouchEventMonitor;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lrx/Scheduler;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            "Lcom/squareup/swipe/SwipeValidator;",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ")",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    .line 207
    new-instance v31, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    move-object/from16 v0, v31

    invoke-direct/range {v0 .. v30}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;-><init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)V

    return-object v31
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;
    .locals 32

    move-object/from16 v0, p0

    .line 160
    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->completerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->paymentEventHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->gateKeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/tenderpayment/ChangeHudToaster;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/ui/TouchEventMonitor;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lrx/Scheduler;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->showModalListProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/swipe/SwipeValidator;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->selectMethodBuyerCheckoutEnabledProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->selectMethodScreensFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->screenDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

    invoke-static/range {v2 .. v31}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow_Factory;->get()Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    move-result-object v0

    return-object v0
.end method
