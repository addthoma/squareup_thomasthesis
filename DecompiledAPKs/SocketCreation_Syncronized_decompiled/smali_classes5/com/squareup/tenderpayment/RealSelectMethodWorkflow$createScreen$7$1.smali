.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;->call(Lkotlin/Pair;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7$1;->invoke(Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1394
    instance-of v0, p1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;

    if-eqz v0, :cond_0

    .line 1395
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;

    iget-object v0, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getAnalytics$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->COLLECT_CASH_DIALOG_OKAY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 1396
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;

    iget-object v0, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    check-cast p1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->processCashPayment(Lcom/squareup/protos/common/Money;)V

    goto :goto_0

    .line 1398
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$DismissCollectCash;->INSTANCE:Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$DismissCollectCash;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;

    iget-object p1, p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onDismissCollectCash()V

    :cond_1
    :goto_0
    return-void
.end method
