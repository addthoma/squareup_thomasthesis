.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u00012\u000e\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "selectMethodData",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;->call(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 1353
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getSelectMethodScreensFactory$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    move-result-object v0

    const-string v1, "selectMethodData"

    .line 1354
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1355
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getShowSecondaryMethods$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Z

    move-result v1

    .line 1353
    invoke-virtual {v0, p1, v1}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSelectMethodScreenForBackground(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Z)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 1357
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$getSelectMethodScreensFactory$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    move-result-object v0

    .line 1358
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 1357
    invoke-virtual {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->createSplitTenderWarningDialogScreen(Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    .line 1352
    invoke-static {p1, v0}, Lcom/squareup/tenderpayment/SelectMethod;->dialogStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
