.class public Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;
.super Ljava/lang/Object;
.source "SelectMethodCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final hudToaster:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentHudToaster:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManager:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Ldagger/Lazy;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Ldagger/Lazy;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Ldagger/Lazy<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 82
    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 83
    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->res:Lcom/squareup/util/Res;

    .line 84
    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->hudToaster:Ldagger/Lazy;

    .line 85
    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->readerHudManager:Ldagger/Lazy;

    .line 86
    iput-object p6, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->paymentHudToaster:Ldagger/Lazy;

    .line 87
    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->transaction:Lcom/squareup/payment/Transaction;

    .line 88
    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->settings:Ldagger/Lazy;

    .line 89
    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method


# virtual methods
.method build(Lio/reactivex/Observable;)Lcom/squareup/tenderpayment/SelectMethodCoordinator;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;>;)",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator;"
        }
    .end annotation

    .line 93
    new-instance v12, Lcom/squareup/tenderpayment/SelectMethodCoordinator;

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->hudToaster:Ldagger/Lazy;

    .line 94
    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->readerHudManager:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->paymentHudToaster:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/PaymentHudToaster;

    iget-object v8, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->settings:Ldagger/Lazy;

    .line 95
    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v10, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const/4 v11, 0x0

    move-object v0, v12

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;)V

    return-object v12
.end method
