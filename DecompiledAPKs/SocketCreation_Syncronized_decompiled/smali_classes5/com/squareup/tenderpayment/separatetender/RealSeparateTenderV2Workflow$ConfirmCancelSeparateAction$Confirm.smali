.class public final Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;
.super Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;
.source "RealSeparateTenderV2Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Confirm"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u0007\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u0008\u001a\u00020\u0005H\u00c2\u0003J\u001d\u0010\t\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u0013*\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;",
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "tenderToCancel",
        "Lcom/squareup/payment/tender/BaseTender;",
        "(Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/tender/BaseTender;)V",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/tender/BaseTender;)V
    .locals 1

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderToCancel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 265
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method

.method private final component1()Lcom/squareup/x2/MaybeX2SellerScreenRunner;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-object v0
.end method

.method private final component2()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/tender/BaseTender;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->copy(Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    .line 268
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getCompletedTenders()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->minus(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    .line 269
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getCancelledTenders()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    .line 270
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 271
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplitAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v8

    .line 276
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->removeCompletedTender(Lcom/squareup/protos/common/Money;)Z

    .line 278
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    const-string v0, "amountRemaining"

    .line 279
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxSplitAmount"

    .line 282
    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    move-object v11, v0

    check-cast v11, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v12, 0xe

    const/4 v13, 0x0

    .line 278
    invoke-static/range {v2 .. v13}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->copy$default(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 262
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public final copy(Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;
    .locals 1

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderToCancel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;-><init>(Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/tender/BaseTender;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    iget-object p1, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Confirm(x2SellerScreenRunner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderToCancel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction$Confirm;->tenderToCancel:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
