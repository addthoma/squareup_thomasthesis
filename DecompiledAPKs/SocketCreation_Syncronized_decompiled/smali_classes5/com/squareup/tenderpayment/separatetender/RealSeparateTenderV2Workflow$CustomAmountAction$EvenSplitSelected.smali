.class public final Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;
.super Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;
.source "RealSeparateTenderV2Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EvenSplitSelected"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSeparateTenderV2Workflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSeparateTenderV2Workflow.kt\ncom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected\n*L\n1#1,298:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u0007\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u0008\u001a\u00020\u0005H\u00c2\u0003J\u001d\u0010\t\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u0013*\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;",
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "numberOfSplits",
        "",
        "(Lcom/squareup/analytics/Analytics;J)V",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final numberOfSplits:J


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;J)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 196
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    iput-wide p2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    return-void
.end method

.method private final component1()Lcom/squareup/analytics/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method private final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    return-wide v0
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;Lcom/squareup/analytics/Analytics;JILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-wide p2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->copy(Lcom/squareup/analytics/Analytics;J)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplits()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 201
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/SplitTenderEvenSplitEvent;

    iget-wide v2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    invoke-direct {v1, v2, v3}, Lcom/squareup/tenderpayment/events/SplitTenderEvenSplitEvent;-><init>(J)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 202
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;

    .line 203
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getCancelledTenders()Ljava/util/List;

    move-result-object p1

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    .line 202
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;-><init>(Ljava/util/List;J)V

    check-cast v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;

    return-object v0

    .line 199
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tried to enter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " when max is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplits()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 193
    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public final copy(Lcom/squareup/analytics/Analytics;J)Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;-><init>(Lcom/squareup/analytics/Analytics;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    iget-wide v2, p1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EvenSplitSelected(analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfSplits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;->numberOfSplits:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
