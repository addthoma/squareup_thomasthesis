.class final Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSeparateTenderV2Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->render(Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSeparateTenderV2Workflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSeparateTenderV2Workflow.kt\ncom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1\n*L\n1#1,298:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actionSink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->this$0:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->$actionSink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->invoke(Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;)V
    .locals 5

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomAmountDoneSelected;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomAmountDoneSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->$actionSink:Lcom/squareup/workflow/Sink;

    .line 91
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CustomAmountDoneSelected;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->this$0:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->access$getAnalytics$p(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CustomAmountDoneSelected;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 90
    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomSplitSelected;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomSplitSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->$actionSink:Lcom/squareup/workflow/Sink;

    .line 94
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CustomSplitSelected;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CustomSplitSelected;

    .line 93
    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CancelSelected;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CancelSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->$actionSink:Lcom/squareup/workflow/Sink;

    .line 97
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CancelSelected;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->this$0:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->access$getAnalytics$p(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CancelSelected;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 96
    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    :cond_2
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomAmountChanged;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->$actionSink:Lcom/squareup/workflow/Sink;

    .line 100
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CustomAmountChanged;

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomAmountChanged;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomAmountChanged;->getNewAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$CustomAmountChanged;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 99
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_3
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$EvenSplitSelected;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->$actionSink:Lcom/squareup/workflow/Sink;

    .line 103
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->this$0:Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;

    invoke-static {v2}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->access$getAnalytics$p(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v2

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$EvenSplitSelected;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$EvenSplitSelected;->getNumberOfSplits()J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$EvenSplitSelected;-><init>(Lcom/squareup/analytics/Analytics;J)V

    .line 102
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :cond_4
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$TenderToCancelSelected;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;->$actionSink:Lcom/squareup/workflow/Sink;

    .line 106
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$TenderToCancelSelected;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$TenderToCancelSelected;->getCancelledTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction$TenderToCancelSelected;-><init>(Lcom/squareup/payment/tender/BaseTender;)V

    .line 105
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 108
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Event of type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " not allowed."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
