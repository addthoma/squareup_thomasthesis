.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTender;
.super Ljava/lang/Object;
.source "SeparateTenderScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00060\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00060\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender;",
        "",
        "()V",
        "CONFIRMING_CANCEL_SPLIT_KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "EVEN_SPLIT_KEY",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "KEY",
        "CustomScreenState",
        "SeparateTenderData",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CONFIRMING_CANCEL_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lkotlin/Unit;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVEN_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTender;

.field public static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 16
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;

    invoke-direct {v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTender;-><init>()V

    sput-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTender;

    .line 17
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const-string v3, "SeparateCustom"

    invoke-direct {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 18
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    const-string v3, "SeparateEven"

    invoke-direct {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->EVEN_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 20
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    const-string v3, "CancelingTender"

    invoke-direct {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->CONFIRMING_CANCEL_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
