.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;
.super Ljava/lang/Object;
.source "SeparateTenderInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003JK\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020#H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000eR\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;",
        "",
        "amountRemaining",
        "Lcom/squareup/protos/common/Money;",
        "maxSplits",
        "",
        "amountEntered",
        "billAmount",
        "maxSplitAmount",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;)V",
        "getAmountEntered",
        "()Lcom/squareup/protos/common/Money;",
        "getAmountRemaining",
        "getBillAmount",
        "getCompletedTenders",
        "()Ljava/util/List;",
        "getMaxSplitAmount",
        "getMaxSplits",
        "()J",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountEntered:Lcom/squareup/protos/common/Money;

.field private final amountRemaining:Lcom/squareup/protos/common/Money;

.field private final billAmount:Lcom/squareup/protos/common/Money;

.field private final completedTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final maxSplitAmount:Lcom/squareup/protos/common/Money;

.field private final maxSplits:J


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)V"
        }
    .end annotation

    const-string v0, "amountRemaining"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountEntered"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxSplitAmount"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    iput-wide p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    iput-object p4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    iput-object p6, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    iput-object p7, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-wide p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    :cond_2
    move-object p9, p4

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p5, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    :cond_3
    move-object v2, p5

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p6, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    :cond_4
    move-object v3, p6

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p7, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    :cond_5
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    move-object p6, p9

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->copy(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    return-wide v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;"
        }
    .end annotation

    const-string v0, "amountRemaining"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountEntered"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billAmount"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxSplitAmount"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;

    move-object v1, v0

    move-wide v3, p2

    invoke-direct/range {v1 .. v8}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;-><init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    iget-wide v2, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountEntered()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getAmountRemaining()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getBillAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getCompletedTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    return-object v0
.end method

.method public final getMaxSplitAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getMaxSplits()J
    .locals 2

    .line 8
    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    invoke-static {v2, v3}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SeparateTenderInput(amountRemaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountRemaining:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxSplits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplits:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", amountEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->amountEntered:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->billAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxSplitAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->maxSplitAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", completedTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->completedTenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
