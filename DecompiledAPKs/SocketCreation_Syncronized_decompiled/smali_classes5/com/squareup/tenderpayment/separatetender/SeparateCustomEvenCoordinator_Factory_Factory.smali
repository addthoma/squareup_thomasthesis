.class public final Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "SeparateCustomEvenCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;-><init>(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;->newInstance(Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator_Factory_Factory;->get()Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
