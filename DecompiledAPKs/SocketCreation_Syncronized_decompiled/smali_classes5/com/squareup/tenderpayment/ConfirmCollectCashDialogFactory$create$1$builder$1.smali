.class final Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$create$1$builder$1;
.super Ljava/lang/Object;
.source "ConfirmCollectCashDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$create$1$builder$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 34
    iget-object p1, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$create$1$builder$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance p2, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;

    iget-object v0, p0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogFactory$create$1$builder$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event$ConfirmCollectCash;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
