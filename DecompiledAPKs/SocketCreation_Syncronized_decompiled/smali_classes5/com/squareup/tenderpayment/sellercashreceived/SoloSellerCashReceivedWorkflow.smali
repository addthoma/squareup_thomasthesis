.class public final Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "SoloSellerCashReceivedWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;",
        "Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedDone;",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSoloSellerCashReceivedWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SoloSellerCashReceivedWorkflow.kt\ncom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,47:1\n149#2,5:48\n*E\n*S KotlinDebug\n*F\n+ 1 SoloSellerCashReceivedWorkflow.kt\ncom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow\n*L\n35#1,5:48\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0018\u0000 \u000c2 \u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0001:\u0001\u000cB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J0\u0010\u0007\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00052\u0006\u0010\u0008\u001a\u00020\u00022\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00030\nH\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;",
        "Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedDone;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "Companion",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final AUTO_CONTINUE_WORKER_KEY:Ljava/lang/String; = "AUTO_CONTINUE_WORKER"

.field public static final Companion:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$Companion;

.field public static final SELLER_CASH_RECEIVED_DURATION_MILLISECONDS:J = 0xfa0L


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;->Companion:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;->getCashReceived()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 20
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 23
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    const-wide/16 v3, 0xfa0

    const-string v5, "AUTO_CONTINUE_WORKER"

    invoke-virtual {v2, v3, v4, v5}, Lcom/squareup/workflow/Worker$Companion;->timer(JLjava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v7

    const/4 v8, 0x0

    .line 24
    sget-object v2, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$render$1;->INSTANCE:Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$render$1;

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v6, p2

    .line 22
    invoke-static/range {v6 .. v11}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 28
    new-instance p2, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;

    .line 30
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 31
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;->getCashReceived()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string v3, "MoneyMath.subtract(props\u2026ived, props.tenderAmount)"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v3, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$render$2;

    invoke-direct {v3, v1}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 28
    invoke-direct {p2, v0, v2, p1, v3}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;-><init>(ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 49
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 50
    const-class v0, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 51
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 49
    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedWorkflow;->render(Lcom/squareup/tenderpayment/sellercashreceived/SellerCashReceivedInput;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
