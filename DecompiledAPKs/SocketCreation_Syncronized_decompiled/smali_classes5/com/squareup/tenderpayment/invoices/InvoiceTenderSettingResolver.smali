.class public Lcom/squareup/tenderpayment/invoices/InvoiceTenderSettingResolver;
.super Ljava/lang/Object;
.source "InvoiceTenderSettingResolver.java"

# interfaces
.implements Lcom/squareup/tenderpayment/InvoiceTenderSetting;


# instance fields
.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/tenderpayment/invoices/InvoiceTenderSettingResolver;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public canUseInvoiceAsTender()Z
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/tenderpayment/invoices/InvoiceTenderSettingResolver;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/invoices/InvoiceTenderSettingResolver;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 19
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->canUseMobileInvoices()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
