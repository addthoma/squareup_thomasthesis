.class public final Lcom/squareup/tenderpayment/SelectMethod$TextData;
.super Ljava/lang/Object;
.source "SelectMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TextData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B9\u0008\u0016\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\nBY\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0013\u001a\u00020\u0014R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u00020\u00108\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
        "",
        "phraseId",
        "",
        "replacementString",
        "",
        "placeHolder",
        "action",
        "Lcom/squareup/fsm/SideEffect;",
        "serverDrivenString",
        "(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/fsm/SideEffect;Ljava/lang/String;)V",
        "color",
        "weight",
        "Lcom/squareup/marketfont/MarketFont$Weight;",
        "visibility",
        "enabled",
        "",
        "(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V",
        "string",
        "res",
        "Lcom/squareup/util/Res;",
        "Companion",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;


# instance fields
.field public final action:Lcom/squareup/fsm/SideEffect;

.field public final color:I

.field public final enabled:Z

.field public final phraseId:I

.field public final placeHolder:Ljava/lang/String;

.field public final replacementString:Ljava/lang/String;

.field public final serverDrivenString:Ljava/lang/String;

.field public final visibility:I

.field public final weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->Companion:Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "weight"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->phraseId:I

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->replacementString:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->placeHolder:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->color:I

    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    iput p6, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->visibility:I

    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->action:Lcom/squareup/fsm/SideEffect;

    iput-boolean p8, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->enabled:Z

    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->serverDrivenString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/fsm/SideEffect;Ljava/lang/String;)V
    .locals 10

    .line 209
    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string v0, "DEFAULT"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v7, p4

    move-object v9, p5

    .line 204
    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;ILcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/fsm/SideEffect;ZLjava/lang/String;)V

    return-void
.end method

.method public static final empty()Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->Companion:Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethod$TextData$Companion;->empty()Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final string(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->serverDrivenString:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 220
    :cond_0
    iget v0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->phraseId:I

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->replacementString:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    iget v0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->phraseId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 225
    :cond_2
    iget v0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->phraseId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 226
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->placeHolder:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->replacementString:Ljava/lang/String;

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 227
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
