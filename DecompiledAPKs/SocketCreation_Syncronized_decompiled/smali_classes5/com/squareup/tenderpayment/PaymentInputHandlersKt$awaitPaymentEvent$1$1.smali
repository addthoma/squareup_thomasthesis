.class final Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;
.super Ljava/lang/Object;
.source "PaymentInputHandlers.kt"

# interfaces
.implements Lio/reactivex/functions/Cancellable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "cancel"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $disposable:Lio/reactivex/disposables/SerialDisposable;

.field final synthetic $shouldCancelPaymentOnCancelled:Lkotlin/jvm/internal/Ref$BooleanRef;

.field final synthetic this$0:Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;Lkotlin/jvm/internal/Ref$BooleanRef;Lio/reactivex/disposables/SerialDisposable;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;->this$0:Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;

    iput-object p2, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;->$shouldCancelPaymentOnCancelled:Lkotlin/jvm/internal/Ref$BooleanRef;

    iput-object p3, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;->$disposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;->$shouldCancelPaymentOnCancelled:Lkotlin/jvm/internal/Ref$BooleanRef;

    iget-boolean v0, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;->this$0:Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;

    iget-object v0, v0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$this_awaitPaymentEvent:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cancelPaymentAndDestroy()V

    goto :goto_0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;->this$0:Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;

    iget-object v0, v0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->$this_awaitPaymentEvent:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->destroy()V

    .line 47
    :goto_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$1;->$disposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method
