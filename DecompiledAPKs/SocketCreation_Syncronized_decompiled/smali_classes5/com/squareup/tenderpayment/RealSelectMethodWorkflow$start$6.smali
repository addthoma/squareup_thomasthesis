.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$6;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->start(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;Lcom/squareup/workflow/legacy/Screen$Key;ZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "isRunning",
        "",
        "kotlin.jvm.PlatformType",
        "call",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;)V
    .locals 1

    .line 790
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 791
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-static {p1, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    goto :goto_0

    .line 793
    :cond_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$6;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-static {p1, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 229
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$6;->call(Ljava/lang/Boolean;)V

    return-void
.end method
