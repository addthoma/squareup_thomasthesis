.class public final Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;
.super Ljava/lang/Object;
.source "ConfirmChargeCardOnFileDialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0008R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
        "",
        "amountDue",
        "Lcom/squareup/protos/common/Money;",
        "customerName",
        "",
        "cardNameAndNumber",
        "instrumentToken",
        "(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final amountDue:Lcom/squareup/protos/common/Money;

.field public final cardNameAndNumber:Ljava/lang/String;

.field public final customerName:Ljava/lang/String;

.field public final instrumentToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "amountDue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardNameAndNumber"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentToken"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->customerName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->cardNameAndNumber:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;->instrumentToken:Ljava/lang/String;

    return-void
.end method
