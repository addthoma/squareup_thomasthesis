.class public interface abstract Lcom/squareup/tenderpayment/TenderScopeRunner;
.super Ljava/lang/Object;
.source "TenderScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0014\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007H&J\u0008\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\u000bH&J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0003H&J\u0008\u0010\u0010\u001a\u00020\u000eH&J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0003H&J\u0018\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u0003H&J\u0010\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0003H\'J\u0008\u0010\u0015\u001a\u00020\u0012H&J\u0008\u0010\u0016\u001a\u00020\u0003H&J\u0008\u0010\u0017\u001a\u00020\u000bH&J\u0008\u0010\u0018\u001a\u00020\u000bH&\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderScopeRunner;",
        "Lmortar/Scoped;",
        "advanceToNextFlow",
        "",
        "completeTenderResult",
        "Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;",
        "asCancelSplitTenderDialogScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
        "availabilityOffline",
        "",
        "availabilityOnline",
        "buildActionBarConfigSplittable",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "upAsX",
        "buildTenderActionBarConfig",
        "title",
        "",
        "completeTenderAndAdvance",
        "shouldAuthorize",
        "getFormattedTotal",
        "onBackPressedOnFirstTenderScreen",
        "onGiftCardOnFileSelected",
        "onOrderNameCommitted",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z
    .annotation runtime Lkotlin/Deprecated;
        message = "Use TenderCompleter#completeTenderAndAuthorize(boolean)"
    .end annotation
.end method

.method public abstract asCancelSplitTenderDialogScreen()Lcom/squareup/workflow/legacy/Screen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            ">;"
        }
    .end annotation
.end method

.method public abstract availabilityOffline()V
.end method

.method public abstract availabilityOnline()V
.end method

.method public abstract buildActionBarConfigSplittable(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end method

.method public abstract buildTenderActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end method

.method public abstract buildTenderActionBarConfig(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end method

.method public abstract buildTenderActionBarConfig(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end method

.method public abstract completeTenderAndAdvance(Z)Z
    .annotation runtime Lkotlin/Deprecated;
        message = "Use TenderCompleter#completeTenderAndAuthorize(boolean)"
    .end annotation
.end method

.method public abstract getFormattedTotal()Ljava/lang/CharSequence;
.end method

.method public abstract onBackPressedOnFirstTenderScreen()Z
.end method

.method public abstract onGiftCardOnFileSelected()V
.end method

.method public abstract onOrderNameCommitted()V
.end method
