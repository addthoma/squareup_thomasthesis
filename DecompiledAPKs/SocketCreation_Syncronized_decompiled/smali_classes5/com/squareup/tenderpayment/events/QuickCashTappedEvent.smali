.class public Lcom/squareup/tenderpayment/events/QuickCashTappedEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "QuickCashTappedEvent.java"


# instance fields
.field private final event_id:Ljava/lang/String;

.field private final tendered_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 15
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_TYPE_QUICK_CASH:Lcom/squareup/analytics/RegisterTapName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/squareup/tenderpayment/events/QuickCashTappedEvent;->event_id:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/squareup/tenderpayment/events/QuickCashTappedEvent;->tendered_amount:Lcom/squareup/protos/common/Money;

    return-void
.end method
