.class public Lcom/squareup/squarewave/Averager;
.super Ljava/lang/Object;
.source "Averager.java"


# instance fields
.field private average:I

.field private final divisionShift:I

.field private position:I

.field private total:J

.field private final window:[I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p1, v0}, Lcom/squareup/squarewave/Averager;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/squareup/squarewave/MathUtil;->isPowerOfTwo(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 30
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/squareup/squarewave/Averager;->window:[I

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x2

    if-le p1, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    shr-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 38
    :cond_0
    iput v0, p0, Lcom/squareup/squarewave/Averager;->divisionShift:I

    if-eqz p2, :cond_1

    .line 41
    invoke-virtual {p0, p2}, Lcom/squareup/squarewave/Averager;->reset(I)V

    :cond_1
    return-void

    .line 27
    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "size must be a power of 2: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method


# virtual methods
.method public add(I)Lcom/squareup/squarewave/Averager;
    .locals 6

    .line 55
    iget-wide v0, p0, Lcom/squareup/squarewave/Averager;->total:J

    iget-object v2, p0, Lcom/squareup/squarewave/Averager;->window:[I

    iget v3, p0, Lcom/squareup/squarewave/Averager;->position:I

    aget v4, v2, v3

    int-to-long v4, v4

    sub-long/2addr v0, v4

    iput-wide v0, p0, Lcom/squareup/squarewave/Averager;->total:J

    add-int/lit8 v0, v3, 0x1

    .line 56
    iput v0, p0, Lcom/squareup/squarewave/Averager;->position:I

    aput p1, v2, v3

    .line 59
    iget v0, p0, Lcom/squareup/squarewave/Averager;->position:I

    array-length v1, v2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/squareup/squarewave/Averager;->position:I

    .line 61
    :cond_0
    iget-wide v0, p0, Lcom/squareup/squarewave/Averager;->total:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/squarewave/Averager;->total:J

    .line 62
    iget-wide v0, p0, Lcom/squareup/squarewave/Averager;->total:J

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-gez p1, :cond_1

    neg-long v0, v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iget p1, p0, Lcom/squareup/squarewave/Averager;->divisionShift:I

    shr-long/2addr v0, p1

    neg-long v0, v0

    long-to-int p1, v0

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/squareup/squarewave/Averager;->window:[I

    array-length v2, p1

    shr-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    add-long/2addr v0, v2

    long-to-int v1, v0

    array-length p1, p1

    div-int p1, v1, p1

    :goto_0
    iput p1, p0, Lcom/squareup/squarewave/Averager;->average:I

    return-object p0
.end method

.method public average()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/squareup/squarewave/Averager;->average:I

    return v0
.end method

.method public reset(I)V
    .locals 3

    const/4 v0, 0x0

    .line 46
    :goto_0
    iget-object v1, p0, Lcom/squareup/squarewave/Averager;->window:[I

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 47
    aput p1, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    array-length v0, v1

    mul-int v0, v0, p1

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/squarewave/Averager;->total:J

    .line 50
    iput p1, p0, Lcom/squareup/squarewave/Averager;->average:I

    return-void
.end method
