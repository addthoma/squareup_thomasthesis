.class public Lcom/squareup/squarewave/o1/O1Swipe;
.super Ljava/lang/Object;
.source "O1Swipe.java"

# interfaces
.implements Lcom/squareup/squarewave/Swipe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/o1/O1Swipe$Builder;
    }
.end annotation


# static fields
.field public static final DEVICE_BAUD:I = 0x960

.field public static final WORD_SIZE:I = 0x9


# instance fields
.field private final derivative:[S

.field private final readings:[Lcom/squareup/squarewave/gen2/Reading;

.field private final signal:Lcom/squareup/squarewave/Signal;

.field private threshold:Lcom/squareup/squarewave/o1/Threshold;


# direct methods
.method private constructor <init>(Lcom/squareup/squarewave/Signal;[SLcom/squareup/squarewave/o1/Threshold;[Lcom/squareup/squarewave/gen2/Reading;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->signal:Lcom/squareup/squarewave/Signal;

    .line 28
    iput-object p2, p0, Lcom/squareup/squarewave/o1/O1Swipe;->derivative:[S

    .line 29
    iput-object p3, p0, Lcom/squareup/squarewave/o1/O1Swipe;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    .line 30
    iput-object p4, p0, Lcom/squareup/squarewave/o1/O1Swipe;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/Signal;[SLcom/squareup/squarewave/o1/Threshold;[Lcom/squareup/squarewave/gen2/Reading;Lcom/squareup/squarewave/o1/O1Swipe$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/squarewave/o1/O1Swipe;-><init>(Lcom/squareup/squarewave/Signal;[SLcom/squareup/squarewave/o1/Threshold;[Lcom/squareup/squarewave/gen2/Reading;)V

    return-void
.end method

.method public static fromSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/o1/O1Swipe;
    .locals 1

    .line 93
    new-instance v0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    invoke-direct {v0}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->build()Lcom/squareup/squarewave/o1/O1Swipe;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/squarewave/o1/O1Swipe$Builder;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    invoke-direct {v0}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->signal:Lcom/squareup/squarewave/Signal;

    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->derivative:[S

    .line 87
    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setDerivative([S)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    .line 88
    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setThreshold(Lcom/squareup/squarewave/o1/Threshold;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    .line 89
    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setReadings([Lcom/squareup/squarewave/gen2/Reading;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getDerivative()[S
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Swipe;->derivative:[S

    return-object v0
.end method

.method public getReadings()[Lcom/squareup/squarewave/gen2/Reading;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Swipe;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    return-object v0
.end method

.method public getSignal()Lcom/squareup/squarewave/Signal;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Swipe;->signal:Lcom/squareup/squarewave/Signal;

    return-object v0
.end method

.method public getThreshold()Lcom/squareup/squarewave/o1/Threshold;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Swipe;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    return-object v0
.end method

.method public samplesPerCycle()I
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Swipe;->signal:Lcom/squareup/squarewave/Signal;

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->sampleRate()I

    move-result v0

    div-int/lit16 v0, v0, 0x960

    return v0
.end method

.method public setThreshold(Lcom/squareup/squarewave/o1/Threshold;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    return-void
.end method

.method public withDerivative()Lcom/squareup/squarewave/o1/O1Swipe;
    .locals 5

    .line 72
    invoke-virtual {p0}, Lcom/squareup/squarewave/o1/O1Swipe;->getSignal()Lcom/squareup/squarewave/Signal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->samples()[S

    move-result-object v0

    .line 73
    array-length v1, v0

    new-array v1, v1, [S

    const/4 v2, 0x1

    .line 75
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_1

    .line 76
    aget-short v3, v0, v2

    add-int/lit8 v4, v2, -0x1

    aget-short v4, v0, v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-short v3, v3

    if-gez v3, :cond_0

    const/16 v3, 0x7fff

    .line 78
    :cond_0
    aput-short v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/squarewave/o1/O1Swipe;->buildUpon()Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setDerivative([S)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->build()Lcom/squareup/squarewave/o1/O1Swipe;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/squareup/squarewave/o1/VariableThreshold;

    invoke-direct {v1, v0}, Lcom/squareup/squarewave/o1/VariableThreshold;-><init>(Lcom/squareup/squarewave/o1/O1Swipe;)V

    iput-object v1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    .line 82
    invoke-virtual {v0}, Lcom/squareup/squarewave/o1/O1Swipe;->buildUpon()Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/squarewave/o1/O1Swipe;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setThreshold(Lcom/squareup/squarewave/o1/Threshold;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->build()Lcom/squareup/squarewave/o1/O1Swipe;

    move-result-object v0

    return-object v0
.end method

.method public zeroize()V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Swipe;->derivative:[S

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([SS)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1Swipe;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
