.class Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application;
.super Ljava/lang/Object;
.source "DaggerSquarewaveLibraryComponent.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Landroid/app/Application;",
        ">;"
    }
.end annotation


# instance fields
.field private final parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;


# direct methods
.method constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    return-void
.end method


# virtual methods
.method public get()Landroid/app/Application;
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    invoke-interface {v0}, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;->application()Landroid/app/Application;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable component method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 225
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_application;->get()Landroid/app/Application;

    move-result-object v0

    return-object v0
.end method
