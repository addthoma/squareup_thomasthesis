.class public final Lcom/squareup/squarewave/library/SquarewaveLibraryModule;
.super Ljava/lang/Object;
.source "SquarewaveLibraryModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule;,
        Lcom/squareup/wavpool/swipe/AsyncDecoderModule;,
        Lcom/squareup/wavpool/swipe/AudioModule$Real;,
        Lcom/squareup/wavpool/swipe/AudioModule;,
        Lcom/squareup/wavpool/swipe/DecoderModule$Prod;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\t\u001a\u00020\nH\u0001J\u0008\u0010\u000b\u001a\u00020\u000cH\u0001J\u0008\u0010\r\u001a\u00020\u000eH\u0001J\u0008\u0010\u000f\u001a\u00020\u0003H\u0001J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0001J\u0008\u0010\u0014\u001a\u00020\u0007H\u0001J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0001J\u0008\u0010\u0019\u001a\u00020\u001aH\u0001R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/squarewave/library/SquarewaveLibraryModule;",
        "",
        "messenger",
        "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
        "cardreaderConnectionId",
        "Lcom/squareup/cardreader/CardreaderConnectionId;",
        "readerTypeProvider",
        "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
        "(Lcom/squareup/cardreader/SingleCardreaderMessenger;Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;)V",
        "provideBadEventSink",
        "Lcom/squareup/badbus/BadEventSink;",
        "provideCardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "provideCrashnado",
        "Lcom/squareup/crashnado/Crashnado;",
        "provideMessenger",
        "provideR4Decoder",
        "Lcom/squareup/squarewave/m1/R4Decoder;",
        "decoder",
        "Lcom/squareup/squarewave/m1/MessengerR4Decoder;",
        "provideReaderTypeProvider",
        "provideSampleProcessor",
        "Lcom/squareup/squarewave/gum/SampleProcessor;",
        "processor",
        "Lcom/squareup/squarewave/gum/MessengerSampleProcessor;",
        "provideSwipeEventLogger",
        "Lcom/squareup/logging/SwipeEventLogger;",
        "squarewave_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderConnectionId:Lcom/squareup/cardreader/CardreaderConnectionId;

.field private final messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

.field private final readerTypeProvider:Lcom/squareup/wavpool/swipe/ReaderTypeProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SingleCardreaderMessenger;Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;)V
    .locals 1

    const-string v0, "messenger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderConnectionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerTypeProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    iput-object p2, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->cardreaderConnectionId:Lcom/squareup/cardreader/CardreaderConnectionId;

    iput-object p3, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->readerTypeProvider:Lcom/squareup/wavpool/swipe/ReaderTypeProvider;

    return-void
.end method


# virtual methods
.method public final provideBadEventSink()Lcom/squareup/badbus/BadEventSink;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 75
    sget-object v0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule$provideBadEventSink$1;->INSTANCE:Lcom/squareup/squarewave/library/SquarewaveLibraryModule$provideBadEventSink$1;

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    return-object v0
.end method

.method public final provideCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/cardreader/CardReaderId;

    iget-object v1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->cardreaderConnectionId:Lcom/squareup/cardreader/CardreaderConnectionId;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardreaderConnectionId;->getId()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/CardReaderId;-><init>(I)V

    return-object v0
.end method

.method public final provideCrashnado()Lcom/squareup/crashnado/Crashnado;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 57
    new-instance v0, Lcom/squareup/crashnado/Crashnado$ToolsCrashnado;

    invoke-direct {v0}, Lcom/squareup/crashnado/Crashnado$ToolsCrashnado;-><init>()V

    check-cast v0, Lcom/squareup/crashnado/Crashnado;

    return-object v0
.end method

.method public final provideMessenger()Lcom/squareup/cardreader/SingleCardreaderMessenger;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->messenger:Lcom/squareup/cardreader/SingleCardreaderMessenger;

    return-object v0
.end method

.method public final provideR4Decoder(Lcom/squareup/squarewave/m1/MessengerR4Decoder;)Lcom/squareup/squarewave/m1/R4Decoder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "decoder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    check-cast p1, Lcom/squareup/squarewave/m1/R4Decoder;

    return-object p1
.end method

.method public final provideReaderTypeProvider()Lcom/squareup/wavpool/swipe/ReaderTypeProvider;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->readerTypeProvider:Lcom/squareup/wavpool/swipe/ReaderTypeProvider;

    return-object v0
.end method

.method public final provideSampleProcessor(Lcom/squareup/squarewave/gum/MessengerSampleProcessor;)Lcom/squareup/squarewave/gum/SampleProcessor;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "processor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    check-cast p1, Lcom/squareup/squarewave/gum/SampleProcessor;

    return-object p1
.end method

.method public final provideSwipeEventLogger()Lcom/squareup/logging/SwipeEventLogger;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 84
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule$provideSwipeEventLogger$1;

    invoke-direct {v0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule$provideSwipeEventLogger$1;-><init>()V

    check-cast v0, Lcom/squareup/logging/SwipeEventLogger;

    return-object v0
.end method
