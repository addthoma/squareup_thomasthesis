.class public Lcom/squareup/squarewave/gen2/Denoiser;
.super Ljava/lang/Object;
.source "Denoiser.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/gen2/Denoiser$Order;
    }
.end annotation


# instance fields
.field private final nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

.field private final order:Lcom/squareup/squarewave/gen2/Denoiser$Order;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;Lcom/squareup/squarewave/gen2/Denoiser$Order;)V
    .locals 0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Denoiser;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    .line 108
    iput-object p2, p0, Lcom/squareup/squarewave/gen2/Denoiser;->order:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    return-void
.end method


# virtual methods
.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 18

    move-object/from16 v0, p0

    .line 113
    iget-object v1, v0, Lcom/squareup/squarewave/gen2/Denoiser;->order:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    sget-object v2, Lcom/squareup/squarewave/gen2/Denoiser$Order;->ORDER_NONE:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    if-ne v1, v2, :cond_0

    .line 114
    iget-object v1, v0, Lcom/squareup/squarewave/gen2/Denoiser;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    move-object/from16 v2, p1

    invoke-interface {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    const/4 v1, 0x0

    return-object v1

    :cond_0
    move-object/from16 v2, p1

    .line 154
    iget-object v1, v0, Lcom/squareup/squarewave/gen2/Denoiser;->order:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    invoke-static {v1}, Lcom/squareup/squarewave/gen2/Denoiser$Order;->access$000(Lcom/squareup/squarewave/gen2/Denoiser$Order;)I

    move-result v1

    .line 155
    iget-object v3, v0, Lcom/squareup/squarewave/gen2/Denoiser;->order:Lcom/squareup/squarewave/gen2/Denoiser$Order;

    invoke-static {v3}, Lcom/squareup/squarewave/gen2/Denoiser$Order;->access$100(Lcom/squareup/squarewave/gen2/Denoiser$Order;)[I

    move-result-object v3

    .line 156
    array-length v4, v3

    shr-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v4, 0x1

    .line 158
    new-array v6, v5, [S

    .line 163
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->samples()[S

    move-result-object v7

    .line 164
    array-length v8, v7

    .line 165
    new-array v9, v8, [S

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_0
    if-ge v11, v8, :cond_6

    sub-int v13, v11, v4

    move/from16 v16, v11

    move v14, v13

    const-wide/16 v10, 0x0

    const/4 v13, 0x0

    .line 171
    :goto_1
    array-length v15, v3

    if-ge v13, v15, :cond_2

    .line 172
    aget v15, v3, v13

    if-ltz v14, :cond_1

    if-ge v14, v8, :cond_1

    .line 174
    aget-short v17, v7, v14

    mul-int v15, v15, v17

    move-object/from16 v17, v3

    int-to-long v2, v15

    add-long/2addr v10, v2

    goto :goto_2

    :cond_1
    move-object/from16 v17, v3

    :goto_2
    add-int/lit8 v14, v14, 0x1

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v2, p1

    move-object/from16 v3, v17

    goto :goto_1

    :cond_2
    move-object/from16 v17, v3

    const-wide/16 v2, 0x0

    cmp-long v13, v10, v2

    if-gez v13, :cond_3

    neg-long v2, v10

    shr-int/lit8 v10, v1, 0x1

    int-to-long v10, v10

    add-long/2addr v2, v10

    int-to-long v10, v1

    .line 181
    div-long/2addr v2, v10

    neg-long v2, v2

    long-to-int v3, v2

    int-to-short v2, v3

    goto :goto_3

    :cond_3
    shr-int/lit8 v2, v1, 0x1

    int-to-long v2, v2

    add-long/2addr v10, v2

    int-to-long v2, v1

    div-long/2addr v10, v2

    long-to-int v2, v10

    int-to-short v2, v2

    :goto_3
    move/from16 v10, v16

    if-le v10, v4, :cond_4

    sub-int v11, v10, v5

    .line 188
    aget-short v3, v6, v12

    aput-short v3, v9, v11

    .line 190
    :cond_4
    aput-short v2, v6, v12

    add-int/lit8 v12, v12, 0x1

    .line 195
    array-length v2, v6

    if-ne v12, v2, :cond_5

    const/4 v12, 0x0

    :cond_5
    add-int/lit8 v11, v10, 0x1

    move-object/from16 v2, p1

    move-object/from16 v3, v17

    goto :goto_0

    .line 199
    :cond_6
    array-length v1, v6

    sub-int v1, v8, v1

    :goto_4
    if-ge v1, v8, :cond_8

    add-int/lit8 v10, v12, 0x1

    .line 201
    aget-short v2, v6, v12

    aput-short v2, v9, v1

    .line 202
    array-length v2, v6

    if-ne v10, v2, :cond_7

    const/4 v12, 0x0

    goto :goto_5

    :cond_7
    move v12, v10

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 206
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->getSignal()Lcom/squareup/squarewave/Signal;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/squarewave/Signal;->buildUpon()Lcom/squareup/squarewave/Signal$Builder;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/squareup/squarewave/Signal$Builder;->samples([S)Lcom/squareup/squarewave/Signal$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/squarewave/Signal$Builder;->build()Lcom/squareup/squarewave/Signal;

    move-result-object v1

    .line 207
    iget-object v2, v0, Lcom/squareup/squarewave/gen2/Denoiser;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v1

    return-object v1
.end method
