.class public Lcom/squareup/squarewave/gen2/Peak;
.super Ljava/lang/Object;
.source "Peak.java"


# instance fields
.field public final sample:S

.field public final sampleIndex:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    int-to-short p2, p2

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/squarewave/gen2/Peak;-><init>(IS)V

    return-void
.end method

.method public constructor <init>(IS)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    .line 22
    iput-short p2, p0, Lcom/squareup/squarewave/gen2/Peak;->sample:S

    return-void
.end method


# virtual methods
.method public absoluteAmplitude()S
    .locals 1

    .line 46
    iget-short v0, p0, Lcom/squareup/squarewave/gen2/Peak;->sample:S

    if-gez v0, :cond_0

    neg-int v0, v0

    int-to-short v0, v0

    :cond_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 27
    :cond_0
    instance-of v1, p1, Lcom/squareup/squarewave/gen2/Peak;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 28
    check-cast p1, Lcom/squareup/squarewave/gen2/Peak;

    .line 29
    iget v1, p0, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iget v3, p1, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    if-ne v1, v3, :cond_1

    iget-short v1, p0, Lcom/squareup/squarewave/gen2/Peak;->sample:S

    iget-short p1, p1, Lcom/squareup/squarewave/gen2/Peak;->sample:S

    if-ne v1, p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public hashCode()I
    .locals 2

    .line 35
    iget v0, p0, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    mul-int/lit8 v0, v0, 0x1f

    .line 36
    iget-short v1, p0, Lcom/squareup/squarewave/gen2/Peak;->sample:S

    add-int/2addr v0, v1

    return v0
.end method

.method public polarity()I
    .locals 1

    .line 52
    iget-short v0, p0, Lcom/squareup/squarewave/gen2/Peak;->sample:S

    if-gez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v1, p0, Lcom/squareup/squarewave/gen2/Peak;->sample:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
