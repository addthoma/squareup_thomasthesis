.class public Lcom/squareup/squarewave/gen2/CardDataStart;
.super Ljava/lang/Object;
.source "CardDataStart.java"


# instance fields
.field private final decoder:Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;

.field private final forward:Z

.field private final startSentinelIndex:I


# direct methods
.method constructor <init>(Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;IZ)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/CardDataStart;->decoder:Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;

    .line 22
    iput p2, p0, Lcom/squareup/squarewave/gen2/CardDataStart;->startSentinelIndex:I

    .line 23
    iput-boolean p3, p0, Lcom/squareup/squarewave/gen2/CardDataStart;->forward:Z

    return-void
.end method

.method public static find([Lcom/squareup/squarewave/gen2/Reading;)[Lcom/squareup/squarewave/gen2/CardDataStart;
    .locals 7

    .line 32
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 33
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 34
    aget-object v3, p0, v2

    .line 35
    iget v3, v3, Lcom/squareup/squarewave/gen2/Reading;->level:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->reverse()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 45
    invoke-static {}, Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;->instance()Lcom/squareup/squarewave/gen2/Gen2Track2Decoder;

    move-result-object v3

    .line 46
    invoke-interface {v3, p0}, Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;->startSentinelIndex(Ljava/lang/String;)I

    move-result p0

    const/4 v4, -0x1

    if-le p0, v4, :cond_1

    .line 48
    new-instance v5, Lcom/squareup/squarewave/gen2/CardDataStart;

    const/4 v6, 0x1

    invoke-direct {v5, v3, p0, v6}, Lcom/squareup/squarewave/gen2/CardDataStart;-><init>(Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;IZ)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_1
    invoke-interface {v3, v0}, Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;->startSentinelIndex(Ljava/lang/String;)I

    move-result p0

    if-le p0, v4, :cond_2

    .line 53
    new-instance v0, Lcom/squareup/squarewave/gen2/CardDataStart;

    invoke-direct {v0, v3, p0, v1}, Lcom/squareup/squarewave/gen2/CardDataStart;-><init>(Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;IZ)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p0

    new-array p0, p0, [Lcom/squareup/squarewave/gen2/CardDataStart;

    invoke-interface {v2, p0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/squarewave/gen2/CardDataStart;

    return-object p0
.end method


# virtual methods
.method public getDecoder()Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/CardDataStart;->decoder:Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;

    return-object v0
.end method

.method public getStartSentinelIndex()I
    .locals 1

    .line 69
    iget v0, p0, Lcom/squareup/squarewave/gen2/CardDataStart;->startSentinelIndex:I

    return v0
.end method

.method public isForward()Z
    .locals 1

    .line 64
    iget-boolean v0, p0, Lcom/squareup/squarewave/gen2/CardDataStart;->forward:Z

    return v0
.end method
