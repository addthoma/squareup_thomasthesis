.class public Lcom/squareup/squarewave/gen2/Gen2Swipe;
.super Ljava/lang/Object;
.source "Gen2Swipe.java"

# interfaces
.implements Lcom/squareup/squarewave/Swipe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    }
.end annotation


# instance fields
.field private final characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

.field private final peaks:[Lcom/squareup/squarewave/gen2/Peak;

.field private volatile peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

.field private final readings:[Lcom/squareup/squarewave/gen2/Reading;

.field private final signal:Lcom/squareup/squarewave/Signal;

.field private final streaks:[Lcom/squareup/squarewave/gen2/Streak;


# direct methods
.method private constructor <init>(Lcom/squareup/squarewave/Signal;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Reading;[Lcom/squareup/squarewave/gen2/CharacterReading;[Lcom/squareup/squarewave/gen2/Streak;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->signal:Lcom/squareup/squarewave/Signal;

    const/4 p1, 0x0

    if-nez p2, :cond_0

    new-array p2, p1, [Lcom/squareup/squarewave/gen2/Peak;

    .line 38
    :cond_0
    iput-object p2, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    .line 39
    iput-object p3, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    if-nez p4, :cond_1

    new-array p4, p1, [Lcom/squareup/squarewave/gen2/Reading;

    .line 40
    :cond_1
    iput-object p4, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    if-nez p5, :cond_2

    new-array p5, p1, [Lcom/squareup/squarewave/gen2/CharacterReading;

    .line 41
    :cond_2
    iput-object p5, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    .line 43
    iput-object p6, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->streaks:[Lcom/squareup/squarewave/gen2/Streak;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/Signal;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Reading;[Lcom/squareup/squarewave/gen2/CharacterReading;[Lcom/squareup/squarewave/gen2/Streak;Lcom/squareup/squarewave/gen2/Gen2Swipe$1;)V
    .locals 0

    .line 16
    invoke-direct/range {p0 .. p6}, Lcom/squareup/squarewave/gen2/Gen2Swipe;-><init>(Lcom/squareup/squarewave/Signal;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Reading;[Lcom/squareup/squarewave/gen2/CharacterReading;[Lcom/squareup/squarewave/gen2/Streak;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/Signal;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->signal:Lcom/squareup/squarewave/Signal;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Peak;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Peak;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Reading;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/CharacterReading;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Streak;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->streaks:[Lcom/squareup/squarewave/gen2/Streak;

    return-object p0
.end method

.method public static fromSignal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2Swipe;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;-><init>(Lcom/squareup/squarewave/Signal;)V

    invoke-virtual {v0}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;-><init>(Lcom/squareup/squarewave/gen2/Gen2Swipe;Lcom/squareup/squarewave/gen2/Gen2Swipe$1;)V

    return-object v0
.end method

.method public characters()[Lcom/squareup/squarewave/gen2/CharacterReading;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    return-object v0
.end method

.method public getSampleRate()I
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->signal:Lcom/squareup/squarewave/Signal;

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->sampleRate()I

    move-result v0

    return v0
.end method

.method public getSignal()Lcom/squareup/squarewave/Signal;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->signal:Lcom/squareup/squarewave/Signal;

    return-object v0
.end method

.method public peaks()[Lcom/squareup/squarewave/gen2/Peak;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    return-object v0
.end method

.method public peaksBySampleIndex()[Lcom/squareup/squarewave/gen2/Peak;
    .locals 5

    .line 112
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    if-nez v0, :cond_2

    .line 113
    monitor-enter p0

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    if-nez v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->signal:Lcom/squareup/squarewave/Signal;

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->samples()[S

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/squareup/squarewave/gen2/Peak;

    .line 116
    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    const/4 v2, 0x0

    .line 117
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 118
    aget-object v3, v1, v2

    .line 119
    iget v4, v3, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    aput-object v3, v0, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 121
    :cond_0
    iput-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    .line 123
    :cond_1
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 125
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    return-object v0
.end method

.method public readings()[Lcom/squareup/squarewave/gen2/Reading;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    return-object v0
.end method

.method public reversedPeaks()Lcom/squareup/squarewave/gen2/Gen2Swipe;
    .locals 4

    .line 75
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    array-length v1, v0

    new-array v1, v1, [Lcom/squareup/squarewave/gen2/Peak;

    .line 76
    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    invoke-static {v1}, Lcom/squareup/util/Arrays;->reverse([Ljava/lang/Object;)V

    .line 79
    invoke-virtual {p0}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object v0

    return-object v0
.end method

.method public samples()[S
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->signal:Lcom/squareup/squarewave/Signal;

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->samples()[S

    move-result-object v0

    return-object v0
.end method

.method public streaks()[Lcom/squareup/squarewave/gen2/Streak;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe;->streaks:[Lcom/squareup/squarewave/gen2/Streak;

    return-object v0
.end method

.method public withPeaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p1

    return-object p1
.end method
