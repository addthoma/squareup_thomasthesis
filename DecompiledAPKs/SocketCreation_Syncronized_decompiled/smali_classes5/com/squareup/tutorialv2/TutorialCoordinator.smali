.class public abstract Lcom/squareup/tutorialv2/TutorialCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TutorialCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/TutorialCoordinator$NoOp;,
        Lcom/squareup/tutorialv2/TutorialCoordinator$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u0000 \u00072\u00020\u0001:\u0002\u0007\u0008B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "()V",
        "setRootView",
        "",
        "rootView",
        "Landroid/view/View;",
        "Companion",
        "NoOp",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tutorialv2/TutorialCoordinator$Companion;

.field public static final TUTORIAL_TAPPED:Ljava/lang/String; = "Tutorial content tapped"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tutorialv2/TutorialCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tutorialv2/TutorialCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialCoordinator;->Companion:Lcom/squareup/tutorialv2/TutorialCoordinator$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract setRootView(Landroid/view/View;)V
.end method
