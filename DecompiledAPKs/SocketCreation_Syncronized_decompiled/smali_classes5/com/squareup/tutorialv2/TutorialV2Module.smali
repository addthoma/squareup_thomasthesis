.class public abstract Lcom/squareup/tutorialv2/TutorialV2Module;
.super Ljava/lang/Object;
.source "TutorialV2Module.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideTutorialContainer(Lcom/squareup/tutorialv2/RealTutorialContainer;)Lcom/squareup/tutorialv2/TutorialContainer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTutorialCore(Lcom/squareup/tutorialv2/RealTutorialCore;)Lcom/squareup/tutorialv2/TutorialCore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
