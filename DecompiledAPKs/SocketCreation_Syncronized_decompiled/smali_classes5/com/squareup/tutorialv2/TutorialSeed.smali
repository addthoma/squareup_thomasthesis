.class public abstract Lcom/squareup/tutorialv2/TutorialSeed;
.super Ljava/lang/Object;
.source "TutorialSeed.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/TutorialSeed$Priority;,
        Lcom/squareup/tutorialv2/TutorialSeed$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTutorialSeed.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TutorialSeed.kt\ncom/squareup/tutorialv2/TutorialSeed\n*L\n1#1,88:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008&\u0018\u0000 \u00102\u00020\u0001:\u0002\u0010\u0011B\u0011\u0008\u0007\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u000c\u001a\u00020\rH$J\r\u0010\u000e\u001a\u00020\rH\u0000\u00a2\u0006\u0002\u0008\u000fR\u001e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006@BX\u0080\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "",
        "priority",
        "Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
        "(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V",
        "<set-?>",
        "",
        "isAvailable",
        "isAvailable$tutorial_release",
        "()Z",
        "getPriority",
        "()Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
        "doCreate",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "plant",
        "plant$tutorial_release",
        "Companion",
        "Priority",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tutorialv2/TutorialSeed$Companion;

.field public static final NONE:Lcom/squareup/tutorialv2/TutorialSeed;


# instance fields
.field private isAvailable:Z

.field private final priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tutorialv2/TutorialSeed$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tutorialv2/TutorialSeed$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialSeed;->Companion:Lcom/squareup/tutorialv2/TutorialSeed$Companion;

    .line 70
    new-instance v0, Lcom/squareup/tutorialv2/TutorialSeed$Companion$NONE$1;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->LOWEST_INTERNAL:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    invoke-direct {v0, v1}, Lcom/squareup/tutorialv2/TutorialSeed$Companion$NONE$1;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    check-cast v0, Lcom/squareup/tutorialv2/TutorialSeed;

    sput-object v0, Lcom/squareup/tutorialv2/TutorialSeed;->NONE:Lcom/squareup/tutorialv2/TutorialSeed;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V
    .locals 1

    const-string v0, "priority"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialSeed;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 23
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->MERCHANT_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    return-void
.end method

.method public static final synthetic access$isAvailable$p(Lcom/squareup/tutorialv2/TutorialSeed;)Z
    .locals 0

    .line 12
    iget-boolean p0, p0, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable:Z

    return p0
.end method

.method public static final synthetic access$setAvailable$p(Lcom/squareup/tutorialv2/TutorialSeed;Z)V
    .locals 0

    .line 12
    iput-boolean p1, p0, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable:Z

    return-void
.end method


# virtual methods
.method protected abstract doCreate()Lcom/squareup/tutorialv2/Tutorial;
.end method

.method public final getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialSeed;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    return-object v0
.end method

.method public final isAvailable$tutorial_release()Z
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable:Z

    return v0
.end method

.method public final plant$tutorial_release()Lcom/squareup/tutorialv2/Tutorial;
    .locals 2

    .line 53
    iget-boolean v0, p0, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 57
    iput-boolean v0, p0, Lcom/squareup/tutorialv2/TutorialSeed;->isAvailable:Z

    .line 59
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/TutorialSeed;->doCreate()Lcom/squareup/tutorialv2/Tutorial;

    move-result-object v0

    return-object v0

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This seed has already sprouted a tutorial!"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
