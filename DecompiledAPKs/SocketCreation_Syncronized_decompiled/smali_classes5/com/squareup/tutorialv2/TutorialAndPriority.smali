.class public final Lcom/squareup/tutorialv2/TutorialAndPriority;
.super Ljava/lang/Object;
.source "TutorialAndPriority.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0080\u0008\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialAndPriority;",
        "",
        "tutorial",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "priority",
        "Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
        "(Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V",
        "getPriority",
        "()Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
        "getTutorial",
        "()Lcom/squareup/tutorialv2/Tutorial;",
        "asTutorial",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;

.field private static final NO_TUTORIAL:Lcom/squareup/tutorialv2/Tutorial;

.field private static NO_TUTORIAL_PAIR:Lcom/squareup/tutorialv2/TutorialAndPriority;


# instance fields
.field private final priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

.field private final tutorial:Lcom/squareup/tutorialv2/Tutorial;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialAndPriority;->Companion:Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;

    .line 23
    new-instance v0, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion$NO_TUTORIAL$1;

    invoke-direct {v0}, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion$NO_TUTORIAL$1;-><init>()V

    check-cast v0, Lcom/squareup/tutorialv2/Tutorial;

    sput-object v0, Lcom/squareup/tutorialv2/TutorialAndPriority;->NO_TUTORIAL:Lcom/squareup/tutorialv2/Tutorial;

    .line 48
    new-instance v0, Lcom/squareup/tutorialv2/TutorialAndPriority;

    .line 49
    sget-object v1, Lcom/squareup/tutorialv2/TutorialAndPriority;->NO_TUTORIAL:Lcom/squareup/tutorialv2/Tutorial;

    .line 50
    sget-object v2, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->LOWEST_INTERNAL:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    .line 48
    invoke-direct {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialAndPriority;-><init>(Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialAndPriority;->NO_TUTORIAL_PAIR:Lcom/squareup/tutorialv2/TutorialAndPriority;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V
    .locals 1

    const-string/jumbo v0, "tutorial"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priority"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    iput-object p2, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    return-void
.end method

.method public static final synthetic access$getNO_TUTORIAL_PAIR$cp()Lcom/squareup/tutorialv2/TutorialAndPriority;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/tutorialv2/TutorialAndPriority;->NO_TUTORIAL_PAIR:Lcom/squareup/tutorialv2/TutorialAndPriority;

    return-object v0
.end method

.method public static final synthetic access$setNO_TUTORIAL_PAIR$cp(Lcom/squareup/tutorialv2/TutorialAndPriority;)V
    .locals 0

    .line 10
    sput-object p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->NO_TUTORIAL_PAIR:Lcom/squareup/tutorialv2/TutorialAndPriority;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tutorialv2/TutorialAndPriority;Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;ILjava/lang/Object;)Lcom/squareup/tutorialv2/TutorialAndPriority;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tutorialv2/TutorialAndPriority;->copy(Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)Lcom/squareup/tutorialv2/TutorialAndPriority;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final asTutorial()Lcom/squareup/tutorialv2/Tutorial;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    return-object v0
.end method

.method public final component1()Lcom/squareup/tutorialv2/Tutorial;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    return-object v0
.end method

.method public final component2()Lcom/squareup/tutorialv2/TutorialSeed$Priority;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    return-object v0
.end method

.method public final copy(Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)Lcom/squareup/tutorialv2/TutorialAndPriority;
    .locals 1

    const-string/jumbo v0, "tutorial"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priority"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tutorialv2/TutorialAndPriority;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tutorialv2/TutorialAndPriority;-><init>(Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tutorialv2/TutorialAndPriority;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tutorialv2/TutorialAndPriority;

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    iget-object p1, p1, Lcom/squareup/tutorialv2/TutorialAndPriority;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    return-object v0
.end method

.method public final getTutorial()Lcom/squareup/tutorialv2/Tutorial;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->priority:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialAndPriority;->tutorial:Lcom/squareup/tutorialv2/Tutorial;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialAndPriority;->NO_TUTORIAL:Lcom/squareup/tutorialv2/Tutorial;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "NO_TUTORIAL_PAIR"

    goto :goto_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
