.class public final Lcom/squareup/tutorialv2/TutorialV2Dialog;
.super Lcom/squareup/dialog/GlassDialog;
.source "TutorialV2Dialog.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTutorialV2Dialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TutorialV2Dialog.kt\ncom/squareup/tutorialv2/TutorialV2Dialog\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,59:1\n1103#2,7:60\n1103#2,7:67\n*E\n*S KotlinDebug\n*F\n+ 1 TutorialV2Dialog.kt\ncom/squareup/tutorialv2/TutorialV2Dialog\n*L\n40#1,7:60\n50#1,7:67\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialV2Dialog;",
        "Lcom/squareup/dialog/GlassDialog;",
        "context",
        "Landroid/content/Context;",
        "core",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "prompt",
        "Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;",
        "(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "core"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prompt"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget v0, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 23
    sget p1, Lcom/squareup/common/tutorial/R$layout;->tutorial2_dialog:I

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/TutorialV2Dialog;->setContentView(I)V

    const/4 p1, 0x0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/TutorialV2Dialog;->setCancelable(Z)V

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/TutorialV2Dialog;->setCanceledOnTouchOutside(Z)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/TutorialV2Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string/jumbo v0, "window!!"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 29
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "window.decorView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 31
    sget v2, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_primary:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 32
    sget v3, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_secondary:I

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 33
    sget v4, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_title:I

    invoke-static {p1, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/widgets/MessageView;

    .line 34
    sget v5, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_icon:I

    invoke-static {p1, v5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 36
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getTitleId$tutorial_release()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 37
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getContentId$tutorial_release()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 39
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getPrimaryButtonId$tutorial_release()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    .line 40
    check-cast v2, Landroid/view/View;

    .line 60
    new-instance v0, Lcom/squareup/tutorialv2/TutorialV2Dialog$$special$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/tutorialv2/TutorialV2Dialog$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/tutorialv2/TutorialV2Dialog;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getSecondaryButtonId$tutorial_release()I

    move-result v0

    if-ne v0, v1, :cond_1

    const/16 p2, 0x8

    .line 47
    invoke-virtual {v3, p2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(I)V

    .line 50
    check-cast v3, Landroid/view/View;

    .line 67
    new-instance v0, Lcom/squareup/tutorialv2/TutorialV2Dialog$$special$$inlined$onClickDebounced$2;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/tutorialv2/TutorialV2Dialog$$special$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/tutorialv2/TutorialV2Dialog;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    :goto_0
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getIconId$tutorial_release()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method
