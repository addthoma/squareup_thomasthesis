.class public final Lcom/squareup/tutorialv2/TutorialSeed$Companion$NONE$1;
.super Lcom/squareup/tutorialv2/TutorialSeed;
.source "TutorialSeed.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialSeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTutorialSeed.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TutorialSeed.kt\ncom/squareup/tutorialv2/TutorialSeed$Companion$NONE$1\n*L\n1#1,88:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0014J\u0008\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/tutorialv2/TutorialSeed$Companion$NONE$1",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "doCreate",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "toString",
        "",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V
    .locals 0

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    .line 72
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed;->Companion:Lcom/squareup/tutorialv2/TutorialSeed$Companion;

    invoke-static {p1, p0}, Lcom/squareup/tutorialv2/TutorialSeed$Companion;->access$clearAvailable(Lcom/squareup/tutorialv2/TutorialSeed$Companion;Lcom/squareup/tutorialv2/TutorialSeed;)V

    return-void
.end method


# virtual methods
.method protected doCreate()Lcom/squareup/tutorialv2/Tutorial;
    .locals 2

    .line 76
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot create a tutorial with NONE"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "NONE"

    return-object v0
.end method
