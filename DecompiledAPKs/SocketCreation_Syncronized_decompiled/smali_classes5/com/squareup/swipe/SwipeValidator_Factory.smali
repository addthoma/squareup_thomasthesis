.class public final Lcom/squareup/swipe/SwipeValidator_Factory;
.super Ljava/lang/Object;
.source "SwipeValidator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/swipe/SwipeValidator;",
        ">;"
    }
.end annotation


# instance fields
.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/swipe/SwipeValidator_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/swipe/SwipeValidator_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/swipe/SwipeValidator_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/swipe/SwipeValidator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/swipe/SwipeValidator_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/swipe/SwipeValidator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/swipe/SwipeValidator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/giftcard/GiftCards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;)Lcom/squareup/swipe/SwipeValidator;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/swipe/SwipeValidator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/swipe/SwipeValidator;-><init>(Lcom/squareup/giftcard/GiftCards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/swipe/SwipeValidator;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/swipe/SwipeValidator_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/giftcard/GiftCards;

    iget-object v1, p0, Lcom/squareup/swipe/SwipeValidator_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/swipe/SwipeValidator_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1, v2}, Lcom/squareup/swipe/SwipeValidator_Factory;->newInstance(Lcom/squareup/giftcard/GiftCards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;)Lcom/squareup/swipe/SwipeValidator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/swipe/SwipeValidator_Factory;->get()Lcom/squareup/swipe/SwipeValidator;

    move-result-object v0

    return-object v0
.end method
