.class public final Lcom/squareup/server/felica/FelicaService$DefaultImpls;
.super Ljava/lang/Object;
.source "FelicaService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/felica/FelicaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic proxyMessage$default(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/protos/client/felica/ProxyMessageRequest;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 32
    check-cast p3, Ljava/lang/Boolean;

    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/server/felica/FelicaService;->proxyMessage(Lcom/squareup/protos/client/felica/ProxyMessageRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: proxyMessage"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic setTransactionStatus$default(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 39
    check-cast p3, Ljava/lang/Boolean;

    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/server/felica/FelicaService;->setTransactionStatus(Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: setTransactionStatus"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
