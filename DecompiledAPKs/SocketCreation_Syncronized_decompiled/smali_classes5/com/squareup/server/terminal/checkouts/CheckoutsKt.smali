.class public final Lcom/squareup/server/terminal/checkouts/CheckoutsKt;
.super Ljava/lang/Object;
.source "Checkouts.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckouts.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Checkouts.kt\ncom/squareup/server/terminal/checkouts/CheckoutsKt\n*L\n1#1,36:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\"\u0017\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\",\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00062\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00018F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0003\u0010\u0007\"\u0004\u0008\u0008\u0010\t\"\u0017\u0010\n\u001a\u0004\u0018\u00010\u000b*\u00020\u000c8F\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\",\u0010\n\u001a\u0004\u0018\u00010\u000b*\u00020\u000f2\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u000b8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\r\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "cancel_state",
        "Lcom/squareup/server/terminal/checkouts/CancelReason;",
        "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;",
        "getCancel_state",
        "(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;)Lcom/squareup/server/terminal/checkouts/CancelReason;",
        "value",
        "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;",
        "(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;)Lcom/squareup/server/terminal/checkouts/CancelReason;",
        "setCancel_state",
        "(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;Lcom/squareup/server/terminal/checkouts/CancelReason;)V",
        "state",
        "Lcom/squareup/server/terminal/checkouts/CheckoutState;",
        "Lcom/squareup/protos/connect/v2/TerminalCheckout;",
        "getState",
        "(Lcom/squareup/protos/connect/v2/TerminalCheckout;)Lcom/squareup/server/terminal/checkouts/CheckoutState;",
        "Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;",
        "(Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;)Lcom/squareup/server/terminal/checkouts/CheckoutState;",
        "setState",
        "(Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;Lcom/squareup/server/terminal/checkouts/CheckoutState;)V",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getCancel_state(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;)Lcom/squareup/server/terminal/checkouts/CancelReason;
    .locals 1

    const-string v0, "$this$cancel_state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;->cancel_reason:Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/squareup/server/terminal/checkouts/CancelReason;->valueOf(Ljava/lang/String;)Lcom/squareup/server/terminal/checkouts/CancelReason;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getCancel_state(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;)Lcom/squareup/server/terminal/checkouts/CancelReason;
    .locals 1

    const-string v0, "$this$cancel_state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;->cancel_reason:Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/squareup/server/terminal/checkouts/CancelReason;->valueOf(Ljava/lang/String;)Lcom/squareup/server/terminal/checkouts/CancelReason;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getState(Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;)Lcom/squareup/server/terminal/checkouts/CheckoutState;
    .locals 1

    const-string v0, "$this$state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->status:Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/squareup/server/terminal/checkouts/CheckoutState;->valueOf(Ljava/lang/String;)Lcom/squareup/server/terminal/checkouts/CheckoutState;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getState(Lcom/squareup/protos/connect/v2/TerminalCheckout;)Lcom/squareup/server/terminal/checkouts/CheckoutState;
    .locals 1

    const-string v0, "$this$state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/squareup/server/terminal/checkouts/CheckoutState;->valueOf(Ljava/lang/String;)Lcom/squareup/server/terminal/checkouts/CheckoutState;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final setCancel_state(Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;Lcom/squareup/server/terminal/checkouts/CancelReason;)V
    .locals 1

    const-string v0, "$this$cancel_state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 34
    invoke-virtual {p1}, Lcom/squareup/server/terminal/checkouts/CancelReason;->name()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;->cancel_reason:Ljava/lang/String;

    return-void
.end method

.method public static final setState(Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;Lcom/squareup/server/terminal/checkouts/CheckoutState;)V
    .locals 1

    const-string v0, "$this$state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 25
    invoke-virtual {p1}, Lcom/squareup/server/terminal/checkouts/CheckoutState;->name()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->status:Ljava/lang/String;

    return-void
.end method
