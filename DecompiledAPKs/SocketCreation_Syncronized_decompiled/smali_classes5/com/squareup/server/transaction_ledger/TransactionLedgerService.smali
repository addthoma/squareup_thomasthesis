.class public interface abstract Lcom/squareup/server/transaction_ledger/TransactionLedgerService;
.super Ljava/lang/Object;
.source "TransactionLedgerService.java"


# virtual methods
.method public abstract uploadTransactionLedger(Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/transaction-ledger/upload"
    .end annotation
.end method
