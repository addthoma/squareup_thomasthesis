.class public abstract Lcom/squareup/server/analytics/EventStreamModule;
.super Ljava/lang/Object;
.source "EventStreamModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/analytics/EventStreamModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static diagonalBucket(Landroid/util/DisplayMetrics;)I
    .locals 4

    .line 166
    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int v0, v0, v1

    iget v1, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int v1, v1, v2

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iget p0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v2, p0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int p0, v0

    return p0
.end method

.method private static getBuildType(Lcom/squareup/util/PosBuild;Z)Lcom/squareup/eventstream/BaseEventstream$BuildType;
    .locals 1

    if-eqz p1, :cond_0

    .line 149
    sget-object p0, Lcom/squareup/eventstream/BaseEventstream$BuildType;->RELEASE:Lcom/squareup/eventstream/BaseEventstream$BuildType;

    return-object p0

    .line 151
    :cond_0
    sget-object p1, Lcom/squareup/server/analytics/EventStreamModule$1;->$SwitchMap$com$squareup$util$BuildFlavor:[I

    invoke-interface {p0}, Lcom/squareup/util/PosBuild;->getBuildFlavor()Lcom/squareup/util/BuildFlavor;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/BuildFlavor;->ordinal()I

    move-result p0

    aget p0, p1, p0

    const/4 p1, 0x1

    if-eq p0, p1, :cond_3

    const/4 p1, 0x2

    if-eq p0, p1, :cond_2

    const/4 p1, 0x3

    if-ne p0, p1, :cond_1

    .line 157
    sget-object p0, Lcom/squareup/eventstream/BaseEventstream$BuildType;->DEVELOPMENT:Lcom/squareup/eventstream/BaseEventstream$BuildType;

    return-object p0

    .line 159
    :cond_1
    new-instance p0, Ljava/lang/EnumConstantNotPresentException;

    const-class p1, Lcom/squareup/util/BuildFlavor;

    const-string v0, "Couldn\'t match build type."

    invoke-direct {p0, p1, v0}, Ljava/lang/EnumConstantNotPresentException;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    throw p0

    .line 155
    :cond_2
    sget-object p0, Lcom/squareup/eventstream/BaseEventstream$BuildType;->BETA:Lcom/squareup/eventstream/BaseEventstream$BuildType;

    return-object p0

    .line 153
    :cond_3
    sget-object p0, Lcom/squareup/eventstream/BaseEventstream$BuildType;->RELEASE:Lcom/squareup/eventstream/BaseEventstream$BuildType;

    return-object p0
.end method

.method static provideEventStream(Landroid/app/Application;Lcom/squareup/server/analytics/Es1BatchUploader;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Landroid/view/WindowManager;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v1/EventStream;
    .locals 9
    .annotation runtime Ldagger/Provides;
    .end annotation

    move/from16 v0, p10

    .line 60
    sget v1, Lcom/squareup/analytics/impl/wiring/R$string;->eventstream_product_name:I

    move-object/from16 v2, p9

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p13

    .line 61
    invoke-static {v1, v0}, Lcom/squareup/server/analytics/EventStreamModule;->getBuildType(Lcom/squareup/util/PosBuild;Z)Lcom/squareup/eventstream/BaseEventstream$BuildType;

    move-result-object v5

    .line 62
    new-instance v6, Lcom/squareup/eventstream/es1/gson/GsonEs1JsonSerializer;

    move-object v2, p5

    invoke-direct {v6, p5}, Lcom/squareup/eventstream/es1/gson/GsonEs1JsonSerializer;-><init>(Lcom/google/gson/Gson;)V

    .line 64
    new-instance v8, Lcom/squareup/eventstream/v1/EventStream$Builder;

    move-object v2, v8

    move-object v3, p0

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/eventstream/v1/EventStream$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v1/Es1JsonSerializer;Lcom/squareup/eventstream/EventBatchUploader;)V

    if-eqz v0, :cond_0

    move/from16 v2, p12

    .line 69
    invoke-virtual {v8, v2}, Lcom/squareup/eventstream/v1/EventStream$Builder;->versionCode(I)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v2

    check-cast v2, Lcom/squareup/eventstream/v1/EventStream$Builder;

    move-object/from16 v3, p11

    .line 70
    invoke-virtual {v2, v3}, Lcom/squareup/eventstream/v1/EventStream$Builder;->versionName(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    .line 73
    :cond_0
    invoke-interface/range {p13 .. p13}, Lcom/squareup/util/PosBuild;->getGitSha()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/squareup/eventstream/v1/EventStream$Builder;->gitSha(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStream$Builder;

    move-object v2, p6

    .line 74
    invoke-virtual {v1, p6}, Lcom/squareup/eventstream/v1/EventStream$Builder;->installationId(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStream$Builder;

    move-object v2, p3

    .line 75
    invoke-virtual {v1, p3}, Lcom/squareup/eventstream/v1/EventStream$Builder;->userAgent(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStream$Builder;

    move-object v2, p2

    .line 76
    invoke-virtual {v1, p2}, Lcom/squareup/eventstream/v1/EventStream$Builder;->log(Lcom/squareup/eventstream/EventStreamLog;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStream$Builder;

    .line 77
    invoke-virtual/range {p14 .. p14}, Lcom/squareup/analytics/ProcessUniqueId;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/eventstream/v1/EventStream$Builder;->sessionToken(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStream$Builder;

    .line 78
    invoke-virtual {v1}, Lcom/squareup/eventstream/v1/EventStream$Builder;->build()Lcom/squareup/eventstream/BaseEventstream;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStream;

    .line 80
    invoke-interface/range {p7 .. p7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 81
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 82
    invoke-virtual {v2, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 83
    invoke-virtual {v1}, Lcom/squareup/eventstream/v1/EventStream;->state()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/eventstream/v1/EventStream$CurrentState;

    .line 84
    iget v4, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "density_dpi"

    invoke-virtual {v2, v5, v4}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-static {v3}, Lcom/squareup/server/analytics/EventStreamModule;->diagonalBucket(Landroid/util/DisplayMetrics;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "diagonal_bucket"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-interface/range {p8 .. p8}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    const-string v4, "is_tablet"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v4, "build_device"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v4, "build_product"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    sget-object v3, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string v4, "cpu_abi"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    sget-object v3, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    const-string v4, "cpu_abi2"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v3, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    const-string v4, "build_incremental_version"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_1

    .line 99
    sget-object v3, Landroid/os/Build$VERSION;->SECURITY_PATCH:Ljava/lang/String;

    const-string v4, "build_security_patch"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_1
    invoke-static {p0}, Lcom/facebook/device/yearclass/YearClass;->get(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "year_class"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_2

    .line 104
    sget-object v0, Lcom/squareup/sdk/reader/Client;->CLIENT_ID:Ljava/lang/String;

    const-string v3, "sq_app_id"

    invoke-virtual {v2, v3, v0}, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-object v1
.end method

.method static provideEventStreamV2(Landroid/app/Application;Lcom/squareup/server/analytics/Es2BatchUploader;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v2/EventstreamV2;
    .locals 9
    .annotation runtime Ldagger/Provides;
    .end annotation

    move/from16 v0, p9

    .line 116
    sget v1, Lcom/squareup/analytics/impl/wiring/R$string;->eventstream_product_name:I

    move-object/from16 v2, p8

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p12

    .line 117
    invoke-static {v1, v0}, Lcom/squareup/server/analytics/EventStreamModule;->getBuildType(Lcom/squareup/util/PosBuild;Z)Lcom/squareup/eventstream/BaseEventstream$BuildType;

    move-result-object v5

    .line 118
    new-instance v6, Lcom/squareup/eventstream/es2/gson/GsonEs2JsonSerializer;

    move-object v2, p5

    invoke-direct {v6, p5}, Lcom/squareup/eventstream/es2/gson/GsonEs2JsonSerializer;-><init>(Lcom/google/gson/Gson;)V

    .line 120
    new-instance v8, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;

    move-object v2, v8

    move-object v3, p0

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Lcom/squareup/eventstream/EventBatchUploader;)V

    if-eqz v0, :cond_0

    move/from16 v2, p11

    .line 125
    invoke-virtual {v8, v2}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->versionCode(I)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v2

    check-cast v2, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;

    move-object/from16 v3, p10

    .line 126
    invoke-virtual {v2, v3}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->versionName(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    .line 129
    :cond_0
    invoke-interface/range {p12 .. p12}, Lcom/squareup/util/PosBuild;->getGitSha()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->gitSha(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;

    move-object v2, p6

    .line 130
    invoke-virtual {v1, p6}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->installationId(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;

    move-object v2, p3

    .line 131
    invoke-virtual {v1, p3}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->userAgent(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;

    move-object v2, p2

    .line 132
    invoke-virtual {v1, p2}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->log(Lcom/squareup/eventstream/EventStreamLog;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;

    .line 133
    invoke-virtual/range {p13 .. p13}, Lcom/squareup/analytics/ProcessUniqueId;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->sessionToken(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;

    .line 134
    invoke-virtual {v1}, Lcom/squareup/eventstream/v2/EventstreamV2$Builder;->build()Lcom/squareup/eventstream/BaseEventstream;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2;

    .line 136
    invoke-virtual {v1}, Lcom/squareup/eventstream/v2/EventstreamV2;->state()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    .line 137
    invoke-interface/range {p7 .. p7}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    const-string v4, "android_device_is_tablet"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-static {p0}, Lcom/facebook/device/yearclass/YearClass;->get(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "android_device_year_class"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    .line 141
    sget-object v0, Lcom/squareup/sdk/reader/Client;->CLIENT_ID:Ljava/lang/String;

    const-string v3, "sq_app_id"

    invoke-virtual {v2, v3, v0}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v1
.end method
