.class synthetic Lcom/squareup/server/analytics/EventStreamModule$1;
.super Ljava/lang/Object;
.source "EventStreamModule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/analytics/EventStreamModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$util$BuildFlavor:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 151
    invoke-static {}, Lcom/squareup/util/BuildFlavor;->values()[Lcom/squareup/util/BuildFlavor;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/server/analytics/EventStreamModule$1;->$SwitchMap$com$squareup$util$BuildFlavor:[I

    :try_start_0
    sget-object v0, Lcom/squareup/server/analytics/EventStreamModule$1;->$SwitchMap$com$squareup$util$BuildFlavor:[I

    sget-object v1, Lcom/squareup/util/BuildFlavor;->RELEASE:Lcom/squareup/util/BuildFlavor;

    invoke-virtual {v1}, Lcom/squareup/util/BuildFlavor;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/server/analytics/EventStreamModule$1;->$SwitchMap$com$squareup$util$BuildFlavor:[I

    sget-object v1, Lcom/squareup/util/BuildFlavor;->BETA:Lcom/squareup/util/BuildFlavor;

    invoke-virtual {v1}, Lcom/squareup/util/BuildFlavor;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/server/analytics/EventStreamModule$1;->$SwitchMap$com$squareup$util$BuildFlavor:[I

    sget-object v1, Lcom/squareup/util/BuildFlavor;->DEVELOPMENT:Lcom/squareup/util/BuildFlavor;

    invoke-virtual {v1}, Lcom/squareup/util/BuildFlavor;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
