.class public interface abstract Lcom/squareup/server/analytics/EventStreamService;
.super Ljava/lang/Object;
.source "EventStreamService.java"


# static fields
.field public static final ES1_PATH:Ljava/lang/String; = "/1.0/log/eventstream"

.field public static final ES2_PATH:Ljava/lang/String; = "/2.0/log/eventstream"


# virtual methods
.method public abstract logEvents(Lcom/squareup/protos/sawmill/LogEventStreamRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/sawmill/LogEventStreamRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/sawmill/LogEventStreamRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/common/messages/Empty;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/log/eventstream"
    .end annotation
.end method

.method public abstract logEvents(Lcom/squareup/protos/sawmill/LogEventStreamV2Request;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/sawmill/LogEventStreamV2Request;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/sawmill/LogEventStreamV2Request;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/common/messages/Empty;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/2.0/log/eventstream"
    .end annotation
.end method
