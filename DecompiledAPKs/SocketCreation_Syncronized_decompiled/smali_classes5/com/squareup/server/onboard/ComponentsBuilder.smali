.class public final Lcom/squareup/server/onboard/ComponentsBuilder;
.super Ljava/lang/Object;
.source "Panels.kt"


# annotations
.annotation runtime Lcom/squareup/server/onboard/PanelDsl;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Panels.kt\ncom/squareup/server/onboard/ComponentsBuilder\n*L\n1#1,379:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0000\u00a2\u0006\u0002\u0010\u0002J@\u0010\u0008\u001a\u00020\t\"\u0008\u0008\u0000\u0010\n*\u00020\u000b2\u0006\u0010\u000c\u001a\u0002H\n2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011H\u0002\u00a2\u0006\u0002\u0010\u0012J\'\u0010\u0013\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010\u0015\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010\u0017\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010\u0019\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010\u001b\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010\u001d\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010\u001f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010!\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010#\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010%\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010\'\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010)\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010+\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020,\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011J\'\u0010-\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\u0017\u0010\u000f\u001a\u0013\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020\t0\u0010\u00a2\u0006\u0002\u0008\u0011R\u001a\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/server/onboard/ComponentsBuilder;",
        "",
        "()V",
        "components",
        "",
        "Lcom/squareup/protos/client/onboard/Component;",
        "getComponents$public_release",
        "()Ljava/util/List;",
        "addComponent",
        "",
        "T",
        "Lcom/squareup/server/onboard/ComponentBuilder;",
        "component",
        "componentName",
        "",
        "configure",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V",
        "address",
        "Lcom/squareup/server/onboard/Address;",
        "bankAccount",
        "Lcom/squareup/server/onboard/BankAccount;",
        "button",
        "Lcom/squareup/server/onboard/Button;",
        "datePicker",
        "Lcom/squareup/server/onboard/DatePicker;",
        "dropdown",
        "Lcom/squareup/server/onboard/Dropdown;",
        "hardwareList",
        "Lcom/squareup/server/onboard/HardwareList;",
        "image",
        "Lcom/squareup/server/onboard/Image;",
        "multiList",
        "Lcom/squareup/server/onboard/MultiList;",
        "paragraph",
        "Lcom/squareup/server/onboard/Paragraph;",
        "personName",
        "Lcom/squareup/server/onboard/PersonName;",
        "phoneNumber",
        "Lcom/squareup/server/onboard/PhoneNumber;",
        "radioList",
        "Lcom/squareup/server/onboard/RadioList;",
        "text",
        "Lcom/squareup/server/onboard/Text;",
        "title",
        "Lcom/squareup/server/onboard/Title;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Component;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/server/onboard/ComponentsBuilder;->components:Ljava/util/List;

    return-void
.end method

.method private final addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/server/onboard/ComponentBuilder;",
            ">(TT;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 100
    invoke-virtual {p1}, Lcom/squareup/server/onboard/ComponentBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Component$Builder;

    move-result-object v0

    iput-object p2, v0, Lcom/squareup/protos/client/onboard/Component$Builder;->name:Ljava/lang/String;

    .line 101
    invoke-interface {p3, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object p2, p0, Lcom/squareup/server/onboard/ComponentsBuilder;->components:Ljava/util/List;

    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/squareup/server/onboard/ComponentBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Component$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/Component$Builder;->build()Lcom/squareup/protos/client/onboard/Component;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final address(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/Address;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    new-instance v0, Lcom/squareup/server/onboard/Address;

    invoke-direct {v0}, Lcom/squareup/server/onboard/Address;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final bankAccount(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/BankAccount;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    new-instance v0, Lcom/squareup/server/onboard/BankAccount;

    invoke-direct {v0}, Lcom/squareup/server/onboard/BankAccount;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final button(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/Button;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    new-instance v0, Lcom/squareup/server/onboard/Button;

    invoke-direct {v0}, Lcom/squareup/server/onboard/Button;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final datePicker(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/DatePicker;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    new-instance v0, Lcom/squareup/server/onboard/DatePicker;

    invoke-direct {v0}, Lcom/squareup/server/onboard/DatePicker;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final dropdown(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/Dropdown;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    new-instance v0, Lcom/squareup/server/onboard/Dropdown;

    invoke-direct {v0}, Lcom/squareup/server/onboard/Dropdown;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final getComponents$public_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Component;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/server/onboard/ComponentsBuilder;->components:Ljava/util/List;

    return-object v0
.end method

.method public final hardwareList(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/HardwareList;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    new-instance v0, Lcom/squareup/server/onboard/HardwareList;

    invoke-direct {v0}, Lcom/squareup/server/onboard/HardwareList;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final image(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/Image;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    new-instance v0, Lcom/squareup/server/onboard/Image;

    invoke-direct {v0}, Lcom/squareup/server/onboard/Image;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final multiList(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/MultiList;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    new-instance v0, Lcom/squareup/server/onboard/MultiList;

    invoke-direct {v0}, Lcom/squareup/server/onboard/MultiList;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final paragraph(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/Paragraph;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    new-instance v0, Lcom/squareup/server/onboard/Paragraph;

    invoke-direct {v0}, Lcom/squareup/server/onboard/Paragraph;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final personName(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/PersonName;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    new-instance v0, Lcom/squareup/server/onboard/PersonName;

    invoke-direct {v0}, Lcom/squareup/server/onboard/PersonName;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final phoneNumber(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/PhoneNumber;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    new-instance v0, Lcom/squareup/server/onboard/PhoneNumber;

    invoke-direct {v0}, Lcom/squareup/server/onboard/PhoneNumber;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final radioList(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/RadioList;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/squareup/server/onboard/RadioList;

    invoke-direct {v0}, Lcom/squareup/server/onboard/RadioList;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final text(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/Text;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    new-instance v0, Lcom/squareup/server/onboard/Text;

    invoke-direct {v0}, Lcom/squareup/server/onboard/Text;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final title(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/Title;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/squareup/server/onboard/Title;

    invoke-direct {v0}, Lcom/squareup/server/onboard/Title;-><init>()V

    check-cast v0, Lcom/squareup/server/onboard/ComponentBuilder;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/server/onboard/ComponentsBuilder;->addComponent(Lcom/squareup/server/onboard/ComponentBuilder;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
