.class public Lcom/squareup/server/activation/ActivationStatus;
.super Lcom/squareup/server/SimpleResponse;
.source "ActivationStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/activation/ActivationStatus$State;
    }
.end annotation


# instance fields
.field private final questions:[Lcom/squareup/server/activation/QuizQuestion;

.field private final retryable:Z

.field private final state:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/server/activation/ActivationStatus$State;Z[Lcom/squareup/server/activation/QuizQuestion;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 22
    iput-boolean p5, p0, Lcom/squareup/server/activation/ActivationStatus;->retryable:Z

    .line 23
    iput-object p6, p0, Lcom/squareup/server/activation/ActivationStatus;->questions:[Lcom/squareup/server/activation/QuizQuestion;

    .line 24
    invoke-virtual {p4}, Lcom/squareup/server/activation/ActivationStatus$State;->getJsonString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/activation/ActivationStatus;->state:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getQuestions()[Lcom/squareup/server/activation/QuizQuestion;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationStatus;->questions:[Lcom/squareup/server/activation/QuizQuestion;

    return-object v0
.end method

.method public getRawState()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationStatus;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/squareup/server/activation/ActivationStatus$State;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationStatus;->state:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/server/activation/ActivationStatus$State;->access$000(Ljava/lang/String;)Lcom/squareup/server/activation/ActivationStatus$State;

    move-result-object v0

    return-object v0
.end method

.method public isRetryable()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lcom/squareup/server/activation/ActivationStatus;->retryable:Z

    return v0
.end method
