.class public interface abstract Lcom/squareup/server/StandardResponse$Factory;
.super Ljava/lang/Object;
.source "StandardResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/StandardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u0000*\u0008\u0008\u0001\u0010\u0001*\u00020\u00022\u00020\u0002J\"\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u00070\u0006H&J\"\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00010\t2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u00070\u0006H&J(\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00040\u000b2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u00070\u0006H&J(\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\t0\u000b2\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u00070\u0006H&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/server/StandardResponse$Factory;",
        "R",
        "",
        "blocking",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "successOrFailure",
        "Lkotlin/Function1;",
        "",
        "blockingSuccessOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "receivedResponse",
        "Lio/reactivex/Single;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract blocking(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TR;>;"
        }
    .end annotation
.end method

.method public abstract blockingSuccessOrFailure(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TR;>;"
        }
    .end annotation
.end method

.method public abstract receivedResponse(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TR;>;>;"
        }
    .end annotation
.end method

.method public abstract successOrFailure(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TR;>;>;"
        }
    .end annotation
.end method
