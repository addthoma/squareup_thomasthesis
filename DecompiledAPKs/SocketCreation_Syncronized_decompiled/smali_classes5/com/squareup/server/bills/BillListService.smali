.class public interface abstract Lcom/squareup/server/bills/BillListService;
.super Ljava/lang/Object;
.source "BillListService.java"


# virtual methods
.method public abstract getBill(Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bills/get"
    .end annotation
.end method

.method public abstract getBillFamilies(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bills/get-families"
    .end annotation
.end method

.method public abstract getResidualBill(Lcom/squareup/protos/client/bills/GetResidualBillRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bills/GetResidualBillRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bills/get-residual-bill"
    .end annotation
.end method
