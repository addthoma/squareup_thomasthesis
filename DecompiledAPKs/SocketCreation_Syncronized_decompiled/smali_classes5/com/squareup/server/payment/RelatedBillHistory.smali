.class public Lcom/squareup/server/payment/RelatedBillHistory;
.super Ljava/lang/Object;
.source "RelatedBillHistory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/payment/RelatedBillHistory$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRelatedBillHistory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RelatedBillHistory.kt\ncom/squareup/server/payment/RelatedBillHistory\n*L\n1#1,284:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0016\n\u0002\u0010%\n\u0002\u0008\u0008\u0008\u0016\u0018\u0000 @2\u00020\u0001:\u0001@B\u0091\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013\u0012\u000e\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0013\u0012\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u00a2\u0006\u0002\u0010\u0019J\u0014\u0010<\u001a\u00020\u00002\u000c\u0010=\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013J\u000e\u0010>\u001a\u00020\u00002\u0006\u0010?\u001a\u00020\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001dR\u0011\u0010!\u001a\u00020\"8F\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010$R\u0011\u0010%\u001a\u00020\"8F\u00a2\u0006\u0006\u001a\u0004\u0008&\u0010$R\u0011\u0010\'\u001a\u00020\"8F\u00a2\u0006\u0006\u001a\u0004\u0008\'\u0010$R\u0014\u0010(\u001a\u00020\"8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008(\u0010$R\u0014\u0010)\u001a\u00020\"8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008)\u0010$R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010\u001dR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u00100R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u0010\u001dR\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u00103R\u0019\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00084\u00105R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00086\u0010\u001dR\u0019\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00087\u00105R\u001d\u00108\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\r09\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008:\u0010;\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/server/payment/RelatedBillHistory;",
        "",
        "billId",
        "Lcom/squareup/protos/client/IdPair;",
        "refundId",
        "",
        "reason",
        "reasonOption",
        "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        "tenderId",
        "employeeToken",
        "createdAt",
        "refundedMoney",
        "Lcom/squareup/protos/common/Money;",
        "refundHistoryState",
        "Lcom/squareup/server/payment/RefundHistoryState;",
        "purchaseLineItems",
        "Lcom/squareup/protos/client/bills/Cart$LineItems;",
        "returnedLineItems",
        "",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
        "tenders",
        "Lcom/squareup/protos/client/bills/Tender;",
        "destination",
        "Lcom/squareup/protos/client/bills/Refund$Destination;",
        "(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V",
        "getBillId",
        "()Lcom/squareup/protos/client/IdPair;",
        "getCreatedAt",
        "()Ljava/lang/String;",
        "getDestination",
        "()Lcom/squareup/protos/client/bills/Refund$Destination;",
        "getEmployeeToken",
        "hasReturn",
        "",
        "getHasReturn",
        "()Z",
        "hasSale",
        "getHasSale",
        "isExchange",
        "isFailedOrRejected",
        "isRefund",
        "getPurchaseLineItems",
        "()Lcom/squareup/protos/client/bills/Cart$LineItems;",
        "getReason",
        "getReasonOption",
        "()Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        "getRefundHistoryState",
        "()Lcom/squareup/server/payment/RefundHistoryState;",
        "getRefundId",
        "getRefundedMoney",
        "()Lcom/squareup/protos/common/Money;",
        "getReturnedLineItems",
        "()Ljava/util/List;",
        "getTenderId",
        "getTenders",
        "tendersRefundMoney",
        "",
        "getTendersRefundMoney",
        "()Ljava/util/Map;",
        "copyWithReturnLineItems",
        "returnLineItems",
        "merge",
        "otherRelatedBill",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

.field private static final FUTURE_TIME:J = 0xe677d21fdc00L


# instance fields
.field private final billId:Lcom/squareup/protos/client/IdPair;

.field private final createdAt:Ljava/lang/String;

.field private final destination:Lcom/squareup/protos/client/bills/Refund$Destination;

.field private final employeeToken:Ljava/lang/String;

.field private final purchaseLineItems:Lcom/squareup/protos/client/bills/Cart$LineItems;

.field private final reason:Ljava/lang/String;

.field private final reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field private final refundHistoryState:Lcom/squareup/server/payment/RefundHistoryState;

.field private final refundId:Ljava/lang/String;

.field private final refundedMoney:Lcom/squareup/protos/common/Money;

.field private final returnedLineItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderId:Ljava/lang/String;

.field private final tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation
.end field

.field private final tendersRefundMoney:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/server/payment/RefundHistoryState;",
            "Lcom/squareup/protos/client/bills/Cart$LineItems;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Lcom/squareup/protos/client/bills/Refund$Destination;",
            ")V"
        }
    .end annotation

    const-string v0, "billId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/server/payment/RelatedBillHistory;->billId:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/server/payment/RelatedBillHistory;->reason:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/server/payment/RelatedBillHistory;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    iput-object p5, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tenderId:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/server/payment/RelatedBillHistory;->employeeToken:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/server/payment/RelatedBillHistory;->createdAt:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundedMoney:Lcom/squareup/protos/common/Money;

    iput-object p9, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundHistoryState:Lcom/squareup/server/payment/RefundHistoryState;

    iput-object p10, p0, Lcom/squareup/server/payment/RelatedBillHistory;->purchaseLineItems:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iput-object p11, p0, Lcom/squareup/server/payment/RelatedBillHistory;->returnedLineItems:Ljava/util/List;

    iput-object p12, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tenders:Ljava/util/List;

    iput-object p13, p0, Lcom/squareup/server/payment/RelatedBillHistory;->destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    .line 51
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tendersRefundMoney:Ljava/util/Map;

    .line 88
    iget-object p1, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tenderId:Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundedMoney:Lcom/squareup/protos/common/Money;

    if-eqz p2, :cond_0

    .line 89
    iget-object p3, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tendersRefundMoney:Ljava/util/Map;

    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static final fromBill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->fromBill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object p0

    return-object p0
.end method

.method public static final fromRefund(Lcom/squareup/protos/client/bills/Refund;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->fromRefund(Lcom/squareup/protos/client/bills/Refund;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object p0

    return-object p0
.end method

.method public static final fromRefundV1(Lcom/squareup/protos/client/bills/RefundV1;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->fromRefundV1(Lcom/squareup/protos/client/bills/RefundV1;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object p0

    return-object p0
.end method

.method public static final getCreatedByDescendingComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->getCreatedByDescendingComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public static final of(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->of(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->of(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;)Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copyWithReturnLineItems(Ljava/util/List;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;)",
            "Lcom/squareup/server/payment/RelatedBillHistory;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "returnLineItems"

    move-object/from16 v13, p1

    invoke-static {v13, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    new-instance v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 100
    iget-object v4, v0, Lcom/squareup/server/payment/RelatedBillHistory;->refundId:Ljava/lang/String;

    .line 101
    iget-object v5, v0, Lcom/squareup/server/payment/RelatedBillHistory;->reason:Ljava/lang/String;

    .line 102
    iget-object v6, v0, Lcom/squareup/server/payment/RelatedBillHistory;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 103
    iget-object v7, v0, Lcom/squareup/server/payment/RelatedBillHistory;->tenderId:Ljava/lang/String;

    .line 104
    iget-object v8, v0, Lcom/squareup/server/payment/RelatedBillHistory;->employeeToken:Ljava/lang/String;

    .line 105
    iget-object v9, v0, Lcom/squareup/server/payment/RelatedBillHistory;->createdAt:Ljava/lang/String;

    .line 106
    iget-object v10, v0, Lcom/squareup/server/payment/RelatedBillHistory;->refundedMoney:Lcom/squareup/protos/common/Money;

    .line 107
    iget-object v11, v0, Lcom/squareup/server/payment/RelatedBillHistory;->refundHistoryState:Lcom/squareup/server/payment/RefundHistoryState;

    .line 108
    iget-object v3, v0, Lcom/squareup/server/payment/RelatedBillHistory;->billId:Lcom/squareup/protos/client/IdPair;

    .line 109
    iget-object v12, v0, Lcom/squareup/server/payment/RelatedBillHistory;->purchaseLineItems:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 111
    iget-object v14, v0, Lcom/squareup/server/payment/RelatedBillHistory;->tenders:Ljava/util/List;

    .line 112
    iget-object v15, v0, Lcom/squareup/server/payment/RelatedBillHistory;->destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    move-object v2, v1

    .line 99
    invoke-direct/range {v2 .. v15}, Lcom/squareup/server/payment/RelatedBillHistory;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/server/payment/RefundHistoryState;Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Refund$Destination;)V

    return-object v1
.end method

.method public final getBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->billId:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final getCreatedAt()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->createdAt:Ljava/lang/String;

    return-object v0
.end method

.method public final getDestination()Lcom/squareup/protos/client/bills/Refund$Destination;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    return-object v0
.end method

.method public final getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->employeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getHasReturn()Z
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->returnedLineItems:Ljava/util/List;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final getHasSale()Z
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->purchaseLineItems:Lcom/squareup/protos/client/bills/Cart$LineItems;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    const-string v2, "purchaseLineItems.itemization"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final getPurchaseLineItems()Lcom/squareup/protos/client/bills/Cart$LineItems;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->purchaseLineItems:Lcom/squareup/protos/client/bills/Cart$LineItems;

    return-object v0
.end method

.method public final getReason()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->reason:Ljava/lang/String;

    return-object v0
.end method

.method public final getReasonOption()Lcom/squareup/protos/client/bills/Refund$ReasonOption;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object v0
.end method

.method public final getRefundHistoryState()Lcom/squareup/server/payment/RefundHistoryState;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundHistoryState:Lcom/squareup/server/payment/RefundHistoryState;

    return-object v0
.end method

.method public final getRefundId()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundId:Ljava/lang/String;

    return-object v0
.end method

.method public final getRefundedMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundedMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getReturnedLineItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->returnedLineItems:Ljava/util/List;

    return-object v0
.end method

.method public final getTenderId()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tenderId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tenders:Ljava/util/List;

    return-object v0
.end method

.method public final getTendersRefundMoney()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tendersRefundMoney:Ljava/util/Map;

    return-object v0
.end method

.method public final isExchange()Z
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/server/payment/RelatedBillHistory;->getHasSale()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/server/payment/RelatedBillHistory;->getHasReturn()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isFailedOrRejected()Z
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundHistoryState:Lcom/squareup/server/payment/RefundHistoryState;

    sget-object v1, Lcom/squareup/server/payment/RefundHistoryState;->FAILED:Lcom/squareup/server/payment/RefundHistoryState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundHistoryState:Lcom/squareup/server/payment/RefundHistoryState;

    sget-object v1, Lcom/squareup/server/payment/RefundHistoryState;->REJECTED:Lcom/squareup/server/payment/RefundHistoryState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isRefund()Z
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->refundHistoryState:Lcom/squareup/server/payment/RefundHistoryState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final merge(Lcom/squareup/server/payment/RelatedBillHistory;)Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 1

    const-string v0, "otherRelatedBill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/server/payment/RelatedBillHistory;->tendersRefundMoney:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/server/payment/RelatedBillHistory;->tendersRefundMoney:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-object p0
.end method
