.class public final Lcom/squareup/server/payment/RefundHistoryState$Companion;
.super Ljava/lang/Object;
.source "RefundHistoryState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/payment/RefundHistoryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundHistoryState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundHistoryState.kt\ncom/squareup/server/payment/RefundHistoryState$Companion\n*L\n1#1,51:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0008J\u000e\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/server/payment/RefundHistoryState$Companion;",
        "",
        "()V",
        "fromRefundState",
        "Lcom/squareup/server/payment/RefundHistoryState;",
        "state",
        "Lcom/squareup/protos/client/bills/Refund$State;",
        "fromRefundV1State",
        "Lcom/squareup/protos/client/bills/RefundV1$State;",
        "fromString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/server/payment/RefundHistoryState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromRefundState(Lcom/squareup/protos/client/bills/Refund$State;)Lcom/squareup/server/payment/RefundHistoryState;
    .locals 1

    if-nez p1, :cond_0

    goto :goto_0

    .line 34
    :cond_0
    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Refund$State;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_5

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/4 v0, 0x5

    if-eq p1, v0, :cond_1

    :goto_0
    const/4 p1, 0x0

    return-object p1

    .line 39
    :cond_1
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->FAILED:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 38
    :cond_2
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->SUCCESS:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 37
    :cond_3
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->PENDING:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 36
    :cond_4
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->NEW:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 35
    :cond_5
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->UNKNOWN:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1
.end method

.method public final fromRefundV1State(Lcom/squareup/protos/client/bills/RefundV1$State;)Lcom/squareup/server/payment/RefundHistoryState;
    .locals 1

    if-nez p1, :cond_0

    goto :goto_0

    .line 22
    :cond_0
    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundV1$State;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 28
    :pswitch_0
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->FAILED:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 27
    :pswitch_1
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->REJECTED:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 26
    :pswitch_2
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->COMPLETED:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 25
    :pswitch_3
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->APPROVED:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 24
    :pswitch_4
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->REQUESTED:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    .line 23
    :pswitch_5
    sget-object p1, Lcom/squareup/server/payment/RefundHistoryState;->UNKNOWN:Lcom/squareup/server/payment/RefundHistoryState;

    return-object p1

    :goto_0
    const/4 p1, 0x0

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final fromString(Ljava/lang/String;)Lcom/squareup/server/payment/RefundHistoryState;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Locale.US"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/server/payment/RefundHistoryState;->valueOf(Ljava/lang/String;)Lcom/squareup/server/payment/RefundHistoryState;

    move-result-object p1

    return-object p1
.end method
