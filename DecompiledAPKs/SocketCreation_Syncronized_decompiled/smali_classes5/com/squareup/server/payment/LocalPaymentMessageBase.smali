.class public Lcom/squareup/server/payment/LocalPaymentMessageBase;
.super Ljava/lang/Object;
.source "LocalPaymentMessageBase.java"


# instance fields
.field public final adjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final currency_code:Ljava/lang/String;

.field public final description:Ljava/lang/String;

.field public final items_model:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public final line_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final no_sale:Z

.field public final timestamp:Ljava/lang/String;

.field public final unique_key:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->adjustments:Ljava/util/List;

    .line 26
    iput-object p2, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->line_items:Ljava/util/List;

    .line 27
    iput-object p3, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->description:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->unique_key:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->timestamp:Ljava/lang/String;

    .line 30
    iput-object p6, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->currency_code:Ljava/lang/String;

    .line 31
    iput-boolean p7, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->no_sale:Z

    .line 32
    iput-object p8, p0, Lcom/squareup/server/payment/LocalPaymentMessageBase;->items_model:Ljava/util/List;

    return-void
.end method
