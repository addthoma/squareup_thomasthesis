.class public Lcom/squareup/server/payment/RefundRequestBuilder;
.super Ljava/lang/Object;
.source "RefundRequestBuilder.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static newRefundIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 3

    .line 65
    new-instance v0, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static of(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/RefundRequest;
    .locals 7

    .line 37
    iget-object v0, p4, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "RefundRequest reader type is null"

    invoke-static {v0, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 38
    iget-object v0, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-ltz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RefundRequest refund amount is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " but should not be negative"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/Refund$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Refund$Builder;-><init>()V

    .line 43
    invoke-static {}, Lcom/squareup/server/payment/RefundRequestBuilder;->newRefundIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Refund$Builder;->refund_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/bills/Refund$State;->NEW:Lcom/squareup/protos/client/bills/Refund$State;

    .line 44
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Refund$Builder;->state(Lcom/squareup/protos/client/bills/Refund$State;)Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object v1

    .line 45
    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object p2

    .line 46
    invoke-virtual {p2, p0}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object p0

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_tender_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object p0

    .line 48
    invoke-virtual {p0, p3}, Lcom/squareup/protos/client/bills/Refund$Builder;->reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object p0

    .line 49
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_requested_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Refund$Builder;

    move-result-object p0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$Builder;->build()Lcom/squareup/protos/client/bills/Refund;

    move-result-object p0

    .line 42
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->refund(Lcom/squareup/protos/client/bills/Refund;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    move-result-object p0

    new-instance p1, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;-><init>()V

    .line 52
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    move-result-object p1

    .line 53
    invoke-virtual {p1, p5}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails$Builder;->build()Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    move-result-object p1

    .line 51
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->presented_card_details(Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;

    move-result-object p0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->build()Lcom/squareup/protos/client/bills/RefundRequest;

    move-result-object p0

    return-object p0
.end method
