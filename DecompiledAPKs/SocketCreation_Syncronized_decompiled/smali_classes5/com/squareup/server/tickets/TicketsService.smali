.class public interface abstract Lcom/squareup/server/tickets/TicketsService;
.super Ljava/lang/Object;
.source "TicketsService.java"


# virtual methods
.method public abstract listTickets(Lcom/squareup/protos/client/tickets/v2/ListRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/tickets/v2/ListRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/tickets/v2/ListRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/tickets/v2/ListResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/tickets/v2/list"
    .end annotation
.end method

.method public abstract updateTicket(Lcom/squareup/protos/client/tickets/v2/UpdateRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/tickets/v2/UpdateRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/tickets/v2/UpdateRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/tickets/v2/UpdateResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/tickets/v2/update"
    .end annotation
.end method
