.class public final Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;
.super Ljava/lang/Object;
.source "AuthenticationService.kt"

# interfaces
.implements Lcom/squareup/server/account/AuthenticationService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/AuthenticationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoAuthenticationService"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthenticationService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthenticationService.kt\ncom/squareup/server/account/AuthenticationService$NoAuthenticationService\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,90:1\n151#2,2:91\n*E\n*S KotlinDebug\n*F\n+ 1 AuthenticationService.kt\ncom/squareup/server/account/AuthenticationService$NoAuthenticationService\n*L\n68#1,2:91\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J#\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0097\u0001J\u0019\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0008\u0008\u0001\u0010\u0008\u001a\u00020\rH\u0097\u0001J#\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0010H\u0097\u0001J\u001d\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0013H\u0097\u0001J#\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0016H\u0097\u0001\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;",
        "Lcom/squareup/server/account/AuthenticationService;",
        "()V",
        "enrollTwoFactor",
        "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "sessionHeader",
        "",
        "request",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;",
        "login",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "Lcom/squareup/protos/register/api/LoginRequest;",
        "selectUnit",
        "Lcom/squareup/protos/register/api/SelectUnitResponse;",
        "Lcom/squareup/protos/register/api/SelectUnitRequest;",
        "sendVerificationCodeTwoFactor",
        "Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;",
        "upgradeSessionTwoFactor",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/server/account/AuthenticationService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    new-instance v0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;

    invoke-direct {v0}, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;-><init>()V

    sput-object v0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;->INSTANCE:Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 91
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 92
    const-class v2, Lcom/squareup/server/account/AuthenticationService;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/AuthenticationService;

    iput-object v0, p0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;->$$delegate_0:Lcom/squareup/server/account/AuthenticationService;

    return-void
.end method


# virtual methods
.method public enrollTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;",
            ")",
            "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse<",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/EnrollTwoFactor"
    .end annotation

    const-string v0, "sessionHeader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;->$$delegate_0:Lcom/squareup/server/account/AuthenticationService;

    invoke-interface {v0, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->enrollTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public login(Lcom/squareup/protos/register/api/LoginRequest;)Lcom/squareup/server/AcceptedResponse;
    .locals 1
    .param p1    # Lcom/squareup/protos/register/api/LoginRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/register/api/LoginRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/Login"
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;->$$delegate_0:Lcom/squareup/server/account/AuthenticationService;

    invoke-interface {v0, p1}, Lcom/squareup/server/account/AuthenticationService;->login(Lcom/squareup/protos/register/api/LoginRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    return-object p1
.end method

.method public selectUnit(Ljava/lang/String;Lcom/squareup/protos/register/api/SelectUnitRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/SelectUnitRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/register/api/SelectUnitRequest;",
            ")",
            "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse<",
            "Lcom/squareup/protos/register/api/SelectUnitResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/SelectUnit"
    .end annotation

    const-string v0, "sessionHeader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;->$$delegate_0:Lcom/squareup/server/account/AuthenticationService;

    invoke-interface {v0, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->selectUnit(Ljava/lang/String;Lcom/squareup/protos/register/api/SelectUnitRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;

    move-result-object p1

    return-object p1
.end method

.method public sendVerificationCodeTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/SendVerificationCodeTwoFactor"
    .end annotation

    const-string v0, "sessionHeader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;->$$delegate_0:Lcom/squareup/server/account/AuthenticationService;

    invoke-interface {v0, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->sendVerificationCodeTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;

    move-result-object p1

    return-object p1
.end method

.method public upgradeSessionTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;",
            ")",
            "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse<",
            "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/UpgradeSessionTwoFactor"
    .end annotation

    const-string v0, "sessionHeader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;->$$delegate_0:Lcom/squareup/server/account/AuthenticationService;

    invoke-interface {v0, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->upgradeSessionTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;

    move-result-object p1

    return-object p1
.end method
