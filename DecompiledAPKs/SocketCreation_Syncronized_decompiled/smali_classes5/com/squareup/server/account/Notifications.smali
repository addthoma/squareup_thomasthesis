.class public final Lcom/squareup/server/account/Notifications;
.super Ljava/lang/Object;
.source "Notifications.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static getButtons(Lcom/squareup/server/account/protos/Notification;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/Notification;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    if-eqz v0, :cond_0

    .line 12
    iget-object p0, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0

    .line 14
    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    return-object p0
.end method
