.class public Lcom/squareup/server/account/CreateBody;
.super Ljava/lang/Object;
.source "CreateBody.java"


# instance fields
.field public final country_code:Ljava/lang/String;

.field public final email:Ljava/lang/String;

.field public final incentive_offer_token:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final password:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/server/account/CreateBody;->email:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/squareup/server/account/CreateBody;->password:Ljava/lang/String;

    .line 14
    iput-object p3, p0, Lcom/squareup/server/account/CreateBody;->name:Ljava/lang/String;

    .line 15
    iput-object p4, p0, Lcom/squareup/server/account/CreateBody;->country_code:Ljava/lang/String;

    .line 16
    iput-object p5, p0, Lcom/squareup/server/account/CreateBody;->incentive_offer_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public withPassword(Ljava/lang/String;)Lcom/squareup/server/account/CreateBody;
    .locals 7

    .line 20
    new-instance v6, Lcom/squareup/server/account/CreateBody;

    iget-object v1, p0, Lcom/squareup/server/account/CreateBody;->email:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/CreateBody;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/server/account/CreateBody;->country_code:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/server/account/CreateBody;->incentive_offer_token:Ljava/lang/String;

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/CreateBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method
