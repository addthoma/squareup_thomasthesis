.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OrderHub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$ProtoAdapter_OrderHub;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_BAZAAR_ORDERS:Ljava/lang/Boolean;

.field public static final DEFAULT_AUTOMATIC_ACTION_MAX_AGE_SECONDS:Ljava/lang/Long;

.field public static final DEFAULT_CAN_SEARCH_ORDERS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SHOW_QUICK_ACTIONS_SETTING:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SUPPORT_ECOM_DELIVERY_ORDERS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SUPPORT_UPCOMING_ORDERS:Ljava/lang/Boolean;

.field public static final DEFAULT_DEFAULT_SEARCH_RANGE_MILLIS:Ljava/lang/Long;

.field public static final DEFAULT_THROTTLED_SEARCH_RANGE_MILLIS:Ljava/lang/Long;

.field public static final DEFAULT_THROTTLE_ORDER_SEARCH:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_EMPLOYEE_PERMISSION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_bazaar_orders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final automatic_action_max_age_seconds:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final can_search_orders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final can_show_quick_actions_setting:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final can_support_ecom_delivery_orders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final can_support_upcoming_orders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final default_search_range_millis:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x9
    .end annotation
.end field

.field public final throttle_order_search:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final throttled_search_range_millis:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x8
    .end annotation
.end field

.field public final use_employee_permission:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 18084
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$ProtoAdapter_OrderHub;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$ProtoAdapter_OrderHub;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 18086
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 18090
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SUPPORT_UPCOMING_ORDERS:Ljava/lang/Boolean;

    .line 18092
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_USE_EMPLOYEE_PERMISSION:Ljava/lang/Boolean;

    .line 18094
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SHOW_QUICK_ACTIONS_SETTING:Ljava/lang/Boolean;

    .line 18096
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_ALLOW_BAZAAR_ORDERS:Ljava/lang/Boolean;

    const-wide/16 v1, 0x0

    .line 18098
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_AUTOMATIC_ACTION_MAX_AGE_SECONDS:Ljava/lang/Long;

    .line 18100
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SEARCH_ORDERS:Ljava/lang/Boolean;

    .line 18102
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_THROTTLE_ORDER_SEARCH:Ljava/lang/Boolean;

    .line 18104
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_THROTTLED_SEARCH_RANGE_MILLIS:Ljava/lang/Long;

    .line 18106
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_DEFAULT_SEARCH_RANGE_MILLIS:Ljava/lang/Long;

    .line 18108
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SUPPORT_ECOM_DELIVERY_ORDERS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 12

    .line 18186
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 18195
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 18196
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    .line 18197
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    .line 18198
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    .line 18199
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    .line 18200
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    .line 18201
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    .line 18202
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    .line 18203
    iput-object p8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    .line 18204
    iput-object p9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    .line 18205
    iput-object p10, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 18312
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 18228
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 18229
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    .line 18230
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    .line 18231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    .line 18232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    .line 18233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    .line 18234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    .line 18235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    .line 18236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    .line 18237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    .line 18238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    .line 18239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    .line 18240
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 18245
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_a

    .line 18247
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 18248
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18249
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18250
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18251
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18252
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18253
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18254
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18255
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18256
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18257
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 18258
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 2

    .line 18210
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;-><init>()V

    .line 18211
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_upcoming_orders:Ljava/lang/Boolean;

    .line 18212
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->use_employee_permission:Ljava/lang/Boolean;

    .line 18213
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    .line 18214
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->allow_bazaar_orders:Ljava/lang/Boolean;

    .line 18215
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->automatic_action_max_age_seconds:Ljava/lang/Long;

    .line 18216
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_search_orders:Ljava/lang/Boolean;

    .line 18217
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttle_order_search:Ljava/lang/Boolean;

    .line 18218
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttled_search_range_millis:Ljava/lang/Long;

    .line 18219
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->default_search_range_millis:Ljava/lang/Long;

    .line 18220
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    .line 18221
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 18083
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;
    .locals 2

    .line 18298
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_upcoming_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18299
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->use_employee_permission(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18300
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_show_quick_actions_setting(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18301
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->allow_bazaar_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18302
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->automatic_action_max_age_seconds(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18303
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_search_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18304
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttle_order_search(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18305
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttled_search_range_millis(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18306
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->default_search_range_millis(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18307
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_ecom_delivery_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    move-object p1, p0

    goto :goto_0

    .line 18308
    :cond_a
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 18083
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;
    .locals 2

    .line 18282
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SUPPORT_UPCOMING_ORDERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_upcoming_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18283
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_USE_EMPLOYEE_PERMISSION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->use_employee_permission(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18284
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SHOW_QUICK_ACTIONS_SETTING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_show_quick_actions_setting(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18285
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_ALLOW_BAZAAR_ORDERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->allow_bazaar_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18286
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_AUTOMATIC_ACTION_MAX_AGE_SECONDS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->automatic_action_max_age_seconds(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18287
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SEARCH_ORDERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_search_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18288
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_THROTTLE_ORDER_SEARCH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttle_order_search(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18289
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_THROTTLED_SEARCH_RANGE_MILLIS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttled_search_range_millis(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18290
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_DEFAULT_SEARCH_RANGE_MILLIS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->default_search_range_millis(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    .line 18291
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->DEFAULT_CAN_SUPPORT_ECOM_DELIVERY_ORDERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_ecom_delivery_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    move-object v0, p0

    goto :goto_0

    .line 18292
    :cond_a
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 18083
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 18265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18266
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", can_support_upcoming_orders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_upcoming_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18267
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", use_employee_permission="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->use_employee_permission:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18268
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_show_quick_actions_setting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18269
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", allow_bazaar_orders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->allow_bazaar_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18270
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", automatic_action_max_age_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18271
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", can_search_orders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_search_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18272
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", throttle_order_search="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18273
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    if-eqz v1, :cond_7

    const-string v1, ", throttled_search_range_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18274
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    if-eqz v1, :cond_8

    const-string v1, ", default_search_range_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18275
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", can_support_ecom_delivery_orders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OrderHub{"

    .line 18276
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
