.class final Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AccountStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccountStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AccountStatusResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1240
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1345
    new-instance v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;-><init>()V

    .line 1346
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1347
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1395
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1393
    :pswitch_1
    sget-object v3, Lcom/squareup/server/account/protos/PrivacyFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1392
    :pswitch_2
    sget-object v3, Lcom/squareup/server/account/protos/ProductIntent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProductIntent;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent(Lcom/squareup/server/account/protos/ProductIntent;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1391
    :pswitch_3
    sget-object v3, Lcom/squareup/server/account/protos/AppointmentSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/AppointmentSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings(Lcom/squareup/server/account/protos/AppointmentSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1390
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->frozen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1389
    :pswitch_5
    sget-object v3, Lcom/squareup/server/account/protos/InstallmentsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/InstallmentsSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings(Lcom/squareup/server/account/protos/InstallmentsSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1388
    :pswitch_6
    sget-object v3, Lcom/squareup/server/account/protos/Disputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/Disputes;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes(Lcom/squareup/server/account/protos/Disputes;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1387
    :pswitch_7
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/Subscription;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1386
    :pswitch_8
    sget-object v3, Lcom/squareup/server/account/protos/BusinessBanking;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking(Lcom/squareup/server/account/protos/BusinessBanking;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1385
    :pswitch_9
    sget-object v3, Lcom/squareup/server/account/protos/LoyaltyProgram;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto :goto_0

    .line 1384
    :pswitch_a
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->forced_offline_mode_server_urls:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1383
    :pswitch_b
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url_list:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1382
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->r12_getting_started_video_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1381
    :pswitch_d
    sget-object v3, Lcom/squareup/server/account/protos/DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/DeviceCredential;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential(Lcom/squareup/server/account/protos/DeviceCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1380
    :pswitch_e
    sget-object v3, Lcom/squareup/server/account/protos/OpenTickets;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/OpenTickets;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets(Lcom/squareup/server/account/protos/OpenTickets;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1379
    :pswitch_f
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_authorized_application_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1378
    :pswitch_10
    sget-object v3, Lcom/squareup/server/account/protos/EmployeesEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1377
    :pswitch_11
    sget-object v3, Lcom/squareup/server/account/protos/Tutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1376
    :pswitch_12
    sget-object v3, Lcom/squareup/server/account/protos/Tipping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/Tipping;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping(Lcom/squareup/server/account/protos/Tipping;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1375
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->server_time(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1374
    :pswitch_14
    sget-object v3, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1373
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_payment_expiration_seconds(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1372
    :pswitch_16
    sget-object v3, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1371
    :pswitch_17
    sget-object v3, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1370
    :pswitch_18
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1369
    :pswitch_19
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/TaxId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1368
    :pswitch_1a
    sget-object v3, Lcom/squareup/server/account/protos/InstantDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1367
    :pswitch_1b
    sget-object v3, Lcom/squareup/server/account/protos/GiftCards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/GiftCards;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards(Lcom/squareup/server/account/protos/GiftCards;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1366
    :pswitch_1c
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/MerchantUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1365
    :pswitch_1d
    sget-object v3, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings(Lcom/squareup/server/account/protos/MerchantRegisterSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1364
    :pswitch_1e
    sget-object v3, Lcom/squareup/server/account/protos/FeeTypesProto;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1363
    :pswitch_1f
    sget-object v3, Lcom/squareup/server/account/protos/ProcessingFees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1362
    :pswitch_20
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->bank_account_status(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1361
    :pswitch_21
    sget-object v3, Lcom/squareup/server/account/protos/Preferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/Preferences;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1360
    :pswitch_22
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/Notification;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1359
    :pswitch_23
    sget-object v3, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/OtherTenderType;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1358
    :pswitch_24
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1357
    :pswitch_25
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1356
    :pswitch_26
    sget-object v3, Lcom/squareup/server/account/protos/Limits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/Limits;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits(Lcom/squareup/server/account/protos/Limits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1355
    :pswitch_27
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->return_policy(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1354
    :pswitch_28
    sget-object v3, Lcom/squareup/server/account/protos/User;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/User;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user(Lcom/squareup/server/account/protos/User;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1353
    :pswitch_29
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1352
    :pswitch_2a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_title(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1351
    :pswitch_2b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->message(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1350
    :pswitch_2c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->title(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1349
    :pswitch_2d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    goto/16 :goto_0

    .line 1399
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1400
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_0
        :pswitch_1f
        :pswitch_1e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_0
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1238
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1295
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1296
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1297
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1298
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1299
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1300
    sget-object v0, Lcom/squareup/server/account/protos/User;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1301
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1302
    sget-object v0, Lcom/squareup/server/account/protos/Limits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1303
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1304
    sget-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1305
    sget-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1306
    sget-object v0, Lcom/squareup/server/account/protos/Notification;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1307
    sget-object v0, Lcom/squareup/server/account/protos/Preferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1308
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1309
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1310
    sget-object v0, Lcom/squareup/server/account/protos/FeeTypesProto;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1311
    sget-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1312
    sget-object v0, Lcom/squareup/server/account/protos/MerchantUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1313
    sget-object v0, Lcom/squareup/server/account/protos/GiftCards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1314
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1315
    sget-object v0, Lcom/squareup/server/account/protos/TaxId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1316
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1317
    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1318
    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1319
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    const/16 v2, 0x1e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1320
    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1321
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    const/16 v2, 0x20

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1322
    sget-object v0, Lcom/squareup/server/account/protos/Tipping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    const/16 v2, 0x21

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1323
    sget-object v0, Lcom/squareup/server/account/protos/Tutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    const/16 v2, 0x22

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1324
    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    const/16 v2, 0x23

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1325
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    const/16 v2, 0x27

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1326
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    const/16 v2, 0x28

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1327
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    const/16 v2, 0x2a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1328
    sget-object v0, Lcom/squareup/server/account/protos/BusinessBanking;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    const/16 v2, 0x2b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1329
    sget-object v0, Lcom/squareup/server/account/protos/Subscription;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    const/16 v2, 0x2c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1330
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    const/16 v2, 0x24

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1331
    sget-object v0, Lcom/squareup/server/account/protos/OpenTickets;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    const/16 v2, 0x25

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1332
    sget-object v0, Lcom/squareup/server/account/protos/DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    const/16 v2, 0x26

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1333
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    const/16 v2, 0x29

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1334
    sget-object v0, Lcom/squareup/server/account/protos/Disputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    const/16 v2, 0x2d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1335
    sget-object v0, Lcom/squareup/server/account/protos/InstallmentsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    const/16 v2, 0x2e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1336
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    const/16 v2, 0x2f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1337
    sget-object v0, Lcom/squareup/server/account/protos/AppointmentSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    const/16 v2, 0x30

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1338
    sget-object v0, Lcom/squareup/server/account/protos/ProductIntent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    const/16 v2, 0x31

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1339
    sget-object v0, Lcom/squareup/server/account/protos/PrivacyFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    const/16 v2, 0x32

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1340
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/AccountStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1238
    check-cast p2, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/AccountStatusResponse;)I
    .locals 4

    .line 1245
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->title:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1246
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->message:Ljava/lang/String;

    const/4 v3, 0x3

    .line 1247
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_title:Ljava/lang/String;

    const/4 v3, 0x4

    .line 1248
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x5

    .line 1249
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/User;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    const/4 v3, 0x6

    .line 1250
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->return_policy:Ljava/lang/String;

    const/4 v3, 0x7

    .line 1251
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->limits:Lcom/squareup/server/account/protos/Limits;

    const/16 v3, 0x8

    .line 1252
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    const/16 v3, 0x9

    .line 1253
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1254
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->other_tenders:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    const/16 v3, 0xb

    .line 1255
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Notification;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1256
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    const/16 v3, 0xc

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    const/16 v3, 0xd

    .line 1257
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->bank_account_status:Ljava/lang/String;

    const/16 v3, 0xe

    .line 1258
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    const/16 v3, 0x10

    .line 1259
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/FeeTypesProto;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    const/16 v3, 0x11

    .line 1260
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    const/16 v3, 0x15

    .line 1261
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/MerchantUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1262
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_units:Ljava/util/List;

    const/16 v3, 0x16

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/GiftCards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    const/16 v3, 0x17

    .line 1263
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    const/16 v3, 0x18

    .line 1264
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/TaxId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1265
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tax_ids:Ljava/util/List;

    const/16 v3, 0x19

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url:Ljava/lang/String;

    const/16 v3, 0x1b

    .line 1266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    const/16 v3, 0x1c

    .line 1267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    const/16 v3, 0x1d

    .line 1268
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    const/16 v3, 0x1e

    .line 1269
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    const/16 v3, 0x1f

    .line 1270
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->server_time:Ljava/lang/String;

    const/16 v3, 0x20

    .line 1271
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Tipping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tipping:Lcom/squareup/server/account/protos/Tipping;

    const/16 v3, 0x21

    .line 1272
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Tutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    const/16 v3, 0x22

    .line 1273
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/EmployeesEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    const/16 v3, 0x23

    .line 1274
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->r12_getting_started_video_url:Ljava/lang/String;

    const/16 v3, 0x27

    .line 1275
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1276
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    const/16 v3, 0x28

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    const/16 v3, 0x2a

    .line 1277
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/BusinessBanking;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    const/16 v3, 0x2b

    .line 1278
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Subscription;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1279
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->subscriptions:Ljava/util/List;

    const/16 v3, 0x2c

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1280
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_authorized_application_ids:Ljava/util/List;

    const/16 v3, 0x24

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/OpenTickets;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    const/16 v3, 0x25

    .line 1281
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    const/16 v3, 0x26

    .line 1282
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1283
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    const/16 v3, 0x29

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Disputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->disputes:Lcom/squareup/server/account/protos/Disputes;

    const/16 v3, 0x2d

    .line 1284
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/InstallmentsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    const/16 v3, 0x2e

    .line 1285
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->frozen:Ljava/lang/Boolean;

    const/16 v3, 0x2f

    .line 1286
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/AppointmentSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    const/16 v3, 0x30

    .line 1287
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/ProductIntent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    const/16 v3, 0x31

    .line 1288
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/PrivacyFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    const/16 v3, 0x32

    .line 1289
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1290
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1238
    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;->encodedSize(Lcom/squareup/server/account/protos/AccountStatusResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 2

    .line 1405
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object p1

    .line 1406
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user:Lcom/squareup/server/account/protos/User;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/User;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user:Lcom/squareup/server/account/protos/User;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/User;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user:Lcom/squareup/server/account/protos/User;

    .line 1407
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits:Lcom/squareup/server/account/protos/Limits;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/account/protos/Limits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits:Lcom/squareup/server/account/protos/Limits;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Limits;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits:Lcom/squareup/server/account/protos/Limits;

    .line 1408
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 1409
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1410
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/OtherTenderType;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 1411
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/Notification;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1412
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/server/account/protos/Preferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences:Lcom/squareup/server/account/protos/Preferences;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Preferences;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences:Lcom/squareup/server/account/protos/Preferences;

    .line 1413
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProcessingFees;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    .line 1414
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/server/account/protos/FeeTypesProto;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FeeTypesProto;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    .line 1415
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    .line 1416
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/MerchantUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1417
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/server/account/protos/GiftCards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/GiftCards;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    .line 1418
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/InstantDeposits;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    .line 1419
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/TaxId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1420
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 1421
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 1422
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    .line 1423
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping:Lcom/squareup/server/account/protos/Tipping;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/server/account/protos/Tipping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping:Lcom/squareup/server/account/protos/Tipping;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Tipping;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping:Lcom/squareup/server/account/protos/Tipping;

    .line 1424
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/server/account/protos/Tutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Tutorial;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    .line 1425
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/server/account/protos/EmployeesEntity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/EmployeesEntity;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    .line 1426
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/LoyaltyProgram;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    .line 1427
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/server/account/protos/BusinessBanking;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/BusinessBanking;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    .line 1428
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/Subscription;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1429
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/server/account/protos/OpenTickets;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/OpenTickets;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    .line 1430
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/server/account/protos/DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/DeviceCredential;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    .line 1431
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes:Lcom/squareup/server/account/protos/Disputes;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/squareup/server/account/protos/Disputes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes:Lcom/squareup/server/account/protos/Disputes;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Disputes;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes:Lcom/squareup/server/account/protos/Disputes;

    .line 1432
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/squareup/server/account/protos/InstallmentsSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/InstallmentsSettings;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    .line 1433
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    if-eqz v0, :cond_16

    sget-object v0, Lcom/squareup/server/account/protos/AppointmentSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AppointmentSettings;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    .line 1434
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/server/account/protos/ProductIntent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProductIntent;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    .line 1435
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/squareup/server/account/protos/PrivacyFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/PrivacyFeatures;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    .line 1436
    :cond_18
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1437
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1238
    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$ProtoAdapter_AccountStatusResponse;->redact(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    return-object p1
.end method
