.class public final Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyProgram.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public one:Ljava/lang/String;

.field public other:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 533
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;
    .locals 4

    .line 554
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->one:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->other:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 528
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    move-result-object v0

    return-object v0
.end method

.method public one(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;
    .locals 0

    .line 540
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->one:Ljava/lang/String;

    return-object p0
.end method

.method public other(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;
    .locals 0

    .line 548
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology$Builder;->other:Ljava/lang/String;

    return-object p0
.end method
