.class public final Lcom/squareup/server/account/protos/User$MerchantKey$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$MerchantKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/User$MerchantKey;",
        "Lcom/squareup/server/account/protos/User$MerchantKey$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public key:Ljava/lang/String;

.field public master_key_id:Ljava/lang/Integer;

.field public timestamp:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1357
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/User$MerchantKey;
    .locals 5

    .line 1386
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantKey;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->master_key_id:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->timestamp:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/User$MerchantKey;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1350
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object v0

    return-object v0
.end method

.method public key(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;
    .locals 0

    .line 1380
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public master_key_id(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;
    .locals 0

    .line 1364
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->master_key_id:Ljava/lang/Integer;

    return-object p0
.end method

.method public timestamp(Ljava/lang/Long;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;
    .locals 0

    .line 1372
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->timestamp:Ljava/lang/Long;

    return-object p0
.end method
