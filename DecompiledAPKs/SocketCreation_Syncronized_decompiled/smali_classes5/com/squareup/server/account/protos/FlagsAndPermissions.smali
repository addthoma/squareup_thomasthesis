.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCEPT_THIRD_PARTY_GIFT_CARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_ACTIVATION_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_ALLOW_PAYMENT_FOR_INVOICE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_AUTO_CAPTURE_PAYMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_ALLOW_SWIPE_FOR_CHIP_CARDS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_BULK_DELETE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_ENABLE_TIPPING:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_ONBOARD:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_PUBLISH_MERCHANT_CARD:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_REFER_FRIEND_IN_APP:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SKIP_RECEIPT_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_TRANSFER_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_ANDROID_ECR:Ljava/lang/Boolean;

.field public static final DEFAULT_CARDHOLDER_NAME_ON_DIP_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_COLLECT_TIP_BEFORE_AUTHORIZATION_PREFERRED:Ljava/lang/Boolean;

.field public static final DEFAULT_COLLECT_TIP_BEFORE_AUTHORIZATION_REQUIRED:Ljava/lang/Boolean;

.field public static final DEFAULT_CONTACT_SUPPORT_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_CONTACT_SUPPORT_TWITTER:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY_PREFERS_CONTACTLESS_CARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_COUNTRY_PREFERS_EMV:Ljava/lang/Boolean;

.field public static final DEFAULT_COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_DISABLE_ITEM_EDITING:Ljava/lang/Boolean;

.field public static final DEFAULT_DISABLE_MOBILE_REPORTS_APPLET_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_DISABLE_REGISTER_API:Ljava/lang/Boolean;

.field public static final DEFAULT_DISPLAY_LEARN_MORE_R4:Ljava/lang/Boolean;

.field public static final DEFAULT_ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Ljava/lang/Boolean;

.field public static final DEFAULT_ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Ljava/lang/Boolean;

.field public static final DEFAULT_EMAIL_TRANSACTION_LEDGER:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_CANCEL_TRANSACTION:Ljava/lang/Boolean;

.field public static final DEFAULT_HAS_BAZAAR_ONLINE_STORE:Ljava/lang/Boolean;

.field public static final DEFAULT_HELP_CENTER_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_HIDE_MODIFIERS_ON_RECEIPTS_IN_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_INVOICE_TIPPING:Ljava/lang/Boolean;

.field public static final DEFAULT_ITEMS_BATCH_SIZE:Ljava/lang/Long;

.field public static final DEFAULT_LOGIN_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_MERGE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_OPT_IN_TO_OPEN_TICKETS_WITH_BETA_UI_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDER_HUB_SYNC_PERIOD_WITHOUT_NOTIFICATIONS:Ljava/lang/Long;

.field public static final DEFAULT_ORDER_HUB_SYNC_PERIOD_WITH_NOTIFICATIONS:Ljava/lang/Long;

.field public static final DEFAULT_PAYMENT_CARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_PLACEHOLDER:Ljava/lang/Boolean;

.field public static final DEFAULT_PRINT_LOGO_ON_PAPER_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_PRIVACY_POLICY_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_PROTECT_EDIT_TAX:Ljava/lang/Boolean;

.field public static final DEFAULT_REDUCE_PRINTING_WASTE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUIRES_TRACK_2_IF_NOT_AMEX:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Ljava/lang/Boolean;

.field public static final DEFAULT_ROUNDING_TYPE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

.field public static final DEFAULT_SEE_ADVANCED_MODIFIERS:Ljava/lang/Boolean;

.field public static final DEFAULT_SEE_PAYROLL_REGISTER_FEATURE_TOUR:Ljava/lang/Boolean;

.field public static final DEFAULT_SEE_PAYROLL_REGISTER_LEARN_MORE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_SELLER_AGREEMENT_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_SHOW_FEATURE_TOUR:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_INCLUSIVE_TAXES_IN_CART:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_O1_JP_DEPRECATION_WARNING_IN_APP:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_MODIFIER_DETAIL_SCREEN_IN_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_SMS:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORT_RESTAURANT_ITEMS:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORT_RETAIL_ITEMS:Ljava/lang/Boolean;

.field public static final DEFAULT_SWIPE_TO_CREATE_OPEN_TICKET_WITH_NAME:Ljava/lang/Boolean;

.field public static final DEFAULT_TRANSFER_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_UPLOAD_SUPPORT_LEDGER:Ljava/lang/Boolean;

.field public static final DEFAULT_USES_DEVICE_PROFILE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_A10:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ANDROID_BARCODE_SCANNING:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ANDROID_EMPLOYEE_MANAGEMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ANDROID_PRINTER_STATIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_AUSTRALIAN_GROSS_SALES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CARD_ASSOCIATED_COUPONS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CARD_ON_FILE_IN_ANDROID_REGISTER:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CARD_ON_FILE_IN_ANDROID_REGISTER_ON_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CONDITIONAL_TAXES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CRM_CUSTOM_ATTRIBUTES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CRM_MESSAGING:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CRM_NOTES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CUSTOMER_DIRECTORY_WITH_INVOICES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CUSTOMER_LIST_ACTIVITY_LIST_IN_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CUSTOMER_LIST_APPLET_IN_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CUSTOMER_LIST_APPLET_IN_ANDROID_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CUSTOMER_LIST_IN_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CUSTOMER_LIST_IN_ANDROID_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DINING_OPTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_EMPLOYEE_FILTERING_FOR_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_EMPLOYEE_FILTERING_FOR_PAPER_SIGNATURE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_EMPLOYEE_TIMECARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ENFORCE_LOCATION_FIX:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_GIFT_CARDS_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_INVENTORY_PLUS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_INVOICE_APPLET_IN_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_IN_APP_TIMECARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_MAGSTRIPE_ONLY_READERS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_MOBILE_INVOICES:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_MOBILE_INVOICES_WITH_DISCOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_MOBILE_INVOICES_WITH_MODIFIERS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_OPEN_TICKETS_MOBILE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PAPER_SIGNATURE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PREDEFINED_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PRE_TAX_TIPPING_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_R12:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_R12_PAIRING_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_R6:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_REPORTS_CHARTS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SAFETYNET:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SPLIT_TENDER:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SPLIT_TICKETS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_V3_CATALOG:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_VOID_COMP_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ZERO_AMOUNT_TENDER:Ljava/lang/Boolean;

.field public static final DEFAULT_USING_TIME_TRACKING:Ljava/lang/Boolean;

.field public static final DEFAULT_ZERO_AMOUNT_CONTACTLESS_ARQC_EXPERIMENT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final accept_third_party_gift_cards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6c
    .end annotation
.end field

.field public final activation_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x66
    .end annotation
.end field

.field public final allow_payment_for_invoice_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7c
    .end annotation
.end field

.field public final appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Appointments#ADAPTER"
        tag = 0xa8
    .end annotation
.end field

.field public final auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Auth#ADAPTER"
        tag = 0xc2
    .end annotation
.end field

.field public final auto_capture_payments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Bfd#ADAPTER"
        tag = 0x98
    .end annotation
.end field

.field public final billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Billing#ADAPTER"
        tag = 0x9e
    .end annotation
.end field

.field public final bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Bizbank#ADAPTER"
        tag = 0x99
    .end annotation
.end field

.field public final can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x69
    .end annotation
.end field

.field public final can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x67
    .end annotation
.end field

.field public final can_enable_tipping:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final can_onboard:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final can_publish_merchant_card:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final can_refer_friend_in_app:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final can_skip_receipt_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final can_transfer_open_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final can_use_android_ecr:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa1
    .end annotation
.end field

.field public final capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Capital#ADAPTER"
        tag = 0xbe
    .end annotation
.end field

.field public final cardholder_name_on_dip_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x61
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final cardreaders:Lcom/squareup/server/account/protos/Cardreaders;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Cardreaders#ADAPTER"
        tag = 0x82
    .end annotation
.end field

.field public final catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Catalog#ADAPTER"
        tag = 0x7f
    .end annotation
.end field

.field public final collect_tip_before_authorization_preferred:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8a
    .end annotation
.end field

.field public final collect_tip_before_authorization_required:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8b
    .end annotation
.end field

.field public final contact_support_phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final contact_support_twitter:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final country_prefers_contactless_cards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7a
    .end annotation
.end field

.field public final country_prefers_emv:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8f
    .end annotation
.end field

.field public final country_prefers_no_contactless_cards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7b
    .end annotation
.end field

.field public final crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Crm#ADAPTER"
        tag = 0x85
    .end annotation
.end field

.field public final deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Deposits#ADAPTER"
        tag = 0x89
    .end annotation
.end field

.field public final devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Devplatmobile#ADAPTER"
        tag = 0x95
    .end annotation
.end field

.field public final disable_item_editing:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final disable_mobile_reports_applet_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final disable_register_api:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Discount#ADAPTER"
        tag = 0xa9
    .end annotation
.end field

.field public final display_learn_more_r4:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbf
    .end annotation
.end field

.field public final ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Ecom#ADAPTER"
        tag = 0xbc
    .end annotation
.end field

.field public final eligible_for_square_card_processing:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final eligible_for_third_party_card_processing:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final email_transaction_ledger:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Employee#ADAPTER"
        tag = 0x8e
    .end annotation
.end field

.field public final employee_cancel_transaction:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x70
    .end annotation
.end field

.field public final employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$EmployeeJobs#ADAPTER"
        tag = 0xa2
    .end annotation
.end field

.field public final fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Fees#ADAPTER"
        tag = 0x9a
    .end annotation
.end field

.field public final giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Giftcard#ADAPTER"
        tag = 0x8c
    .end annotation
.end field

.field public final has_bazaar_online_store:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xba
    .end annotation
.end field

.field public final help_center_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x15
    .end annotation
.end field

.field public final hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final invoice_tipping:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Invoices#ADAPTER"
        tag = 0x88
    .end annotation
.end field

.field public final items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Items#ADAPTER"
        tag = 0x9f
    .end annotation
.end field

.field public final items_batch_size:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x18
    .end annotation
.end field

.field public final login_type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x92
    .end annotation
.end field

.field public final loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Loyalty#ADAPTER"
        tag = 0x81
    .end annotation
.end field

.field public final marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Marketing#ADAPTER"
        tag = 0xc0
    .end annotation
.end field

.field public final merge_open_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7d
    .end annotation
.end field

.field public final onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Onboard#ADAPTER"
        tag = 0x84
    .end annotation
.end field

.field public final opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final order_hub_sync_period_with_notifications:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0xae
    .end annotation
.end field

.field public final order_hub_sync_period_without_notifications:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0xaf
    .end annotation
.end field

.field public final orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$OrderHub#ADAPTER"
        tag = 0xb5
    .end annotation
.end field

.field public final payment_cards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1c
    .end annotation
.end field

.field public final paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$PaymentFlow#ADAPTER"
        tag = 0x8d
    .end annotation
.end field

.field public final payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Payments#ADAPTER"
        tag = 0xbb
    .end annotation
.end field

.field public final placeholder:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x62
    .end annotation
.end field

.field public final pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Pos#ADAPTER"
        tag = 0x94
    .end annotation
.end field

.field public final posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Posfeatures#ADAPTER"
        tag = 0xc1
    .end annotation
.end field

.field public final prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Prices#ADAPTER"
        tag = 0xb2
    .end annotation
.end field

.field public final print_logo_on_paper_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Printers#ADAPTER"
        tag = 0xbd
    .end annotation
.end field

.field public final privacy_policy_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1d
    .end annotation
.end field

.field public final protect_edit_tax:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x78
    .end annotation
.end field

.field public final readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$ReaderFw#ADAPTER"
        tag = 0x9c
    .end annotation
.end field

.field public final reduce_printing_waste_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1e
    .end annotation
.end field

.field public final regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$RegEx#ADAPTER"
        tag = 0x87
    .end annotation
.end field

.field public final reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Reports#ADAPTER"
        tag = 0x86
    .end annotation
.end field

.field public final require_secure_session_for_r6_swipe:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6a
    .end annotation
.end field

.field public final requires_track_2_if_not_amex:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1f
    .end annotation
.end field

.field public final restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Restaurants#ADAPTER"
        tag = 0xa4
    .end annotation
.end field

.field public final retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Retailer#ADAPTER"
        tag = 0xb1
    .end annotation
.end field

.field public final rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$RoundingType#ADAPTER"
        tag = 0x20
    .end annotation
.end field

.field public final see_advanced_modifiers:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x21
    .end annotation
.end field

.field public final see_payroll_register_feature_tour:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x22
    .end annotation
.end field

.field public final see_payroll_register_learn_more_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x23
    .end annotation
.end field

.field public final seller_agreement_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x24
    .end annotation
.end field

.field public final show_feature_tour:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x27
    .end annotation
.end field

.field public final show_inclusive_taxes_in_cart:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x28
    .end annotation
.end field

.field public final show_items_library_after_login:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2a
    .end annotation
.end field

.field public final show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x29
    .end annotation
.end field

.field public final skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x25
    .end annotation
.end field

.field public final skip_signatures_for_small_payments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x26
    .end annotation
.end field

.field public final sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2c
    .end annotation
.end field

.field public final spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$SPoC#ADAPTER"
        tag = 0xb0
    .end annotation
.end field

.field public final squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$SquareDevice#ADAPTER"
        tag = 0xa0
    .end annotation
.end field

.field public final support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Support#ADAPTER"
        tag = 0x9d
    .end annotation
.end field

.field public final support_restaurant_items:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x63
    .end annotation
.end field

.field public final support_retail_items:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x64
    .end annotation
.end field

.field public final supported_card_brands_offline:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2d
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final supported_card_brands_online:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2e
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2f
    .end annotation
.end field

.field public final terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Terminal#ADAPTER"
        tag = 0x91
    .end annotation
.end field

.field public final timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FlagsAndPermissions$Timecards#ADAPTER"
        tag = 0x93
    .end annotation
.end field

.field public final transfer_open_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6b
    .end annotation
.end field

.field public final upload_support_ledger:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x30
    .end annotation
.end field

.field public final use_a10:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x72
    .end annotation
.end field

.field public final use_android_barcode_scanning:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x32
    .end annotation
.end field

.field public final use_android_employee_management:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x33
    .end annotation
.end field

.field public final use_android_printer_stations:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x31
    .end annotation
.end field

.field public final use_australian_gross_sales:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x34
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final use_card_associated_coupons:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x37
    .end annotation
.end field

.field public final use_card_on_file_in_android_register:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5a
    .end annotation
.end field

.field public final use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x77
    .end annotation
.end field

.field public final use_conditional_taxes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x38
    .end annotation
.end field

.field public final use_crm_custom_attributes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x80
    .end annotation
.end field

.field public final use_crm_messaging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6e
    .end annotation
.end field

.field public final use_crm_notes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6f
    .end annotation
.end field

.field public final use_customer_directory_with_invoices:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6d
    .end annotation
.end field

.field public final use_customer_list_activity_list_in_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x39
    .end annotation
.end field

.field public final use_customer_list_applet_in_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3a
    .end annotation
.end field

.field public final use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3b
    .end annotation
.end field

.field public final use_customer_list_in_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3c
    .end annotation
.end field

.field public final use_customer_list_in_android_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3d
    .end annotation
.end field

.field public final use_dining_options:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3f
    .end annotation
.end field

.field public final use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5d
    .end annotation
.end field

.field public final use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x41
    .end annotation
.end field

.field public final use_employee_timecards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x42
    .end annotation
.end field

.field public final use_enforce_location_fix:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x43
    .end annotation
.end field

.field public final use_gift_cards_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x44
    .end annotation
.end field

.field public final use_in_app_timecards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x73
    .end annotation
.end field

.field public final use_inventory_plus:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x97
    .end annotation
.end field

.field public final use_invoice_applet_in_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x76
    .end annotation
.end field

.field public final use_magstripe_only_readers:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x46
    .end annotation
.end field

.field public final use_mobile_invoices:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x47
    .end annotation
.end field

.field public final use_mobile_invoices_with_discounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x48
    .end annotation
.end field

.field public final use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x49
    .end annotation
.end field

.field public final use_open_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4b
    .end annotation
.end field

.field public final use_open_tickets_mobile_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4c
    .end annotation
.end field

.field public final use_paper_signature_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4d
    .end annotation
.end field

.field public final use_pre_tax_tipping_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x83
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final use_predefined_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4e
    .end annotation
.end field

.field public final use_r12:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x50
    .end annotation
.end field

.field public final use_r12_pairing_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x51
    .end annotation
.end field

.field public final use_r6:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x52
    .end annotation
.end field

.field public final use_reports_charts_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4f
    .end annotation
.end field

.field public final use_safetynet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x53
    .end annotation
.end field

.field public final use_split_tender:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x55
    .end annotation
.end field

.field public final use_split_tickets_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x54
    .end annotation
.end field

.field public final use_v3_catalog:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x57
    .end annotation
.end field

.field public final use_void_comp_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x58
    .end annotation
.end field

.field public final use_zero_amount_tender:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x59
    .end annotation
.end field

.field public final uses_device_profile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa3
    .end annotation
.end field

.field public final using_time_tracking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x96
    .end annotation
.end field

.field public final zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x68
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 42
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ProtoAdapter_FlagsAndPermissions;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 44
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 48
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ACCEPT_THIRD_PARTY_GIFT_CARDS:Ljava/lang/Boolean;

    .line 52
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ALLOW_PAYMENT_FOR_INVOICE_ANDROID:Ljava/lang/Boolean;

    const/4 v1, 0x1

    .line 54
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_AUTO_CAPTURE_PAYMENTS:Ljava/lang/Boolean;

    .line 56
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_ALLOW_SWIPE_FOR_CHIP_CARDS_ANDROID:Ljava/lang/Boolean;

    .line 58
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_BULK_DELETE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 60
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_ENABLE_TIPPING:Ljava/lang/Boolean;

    .line 62
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_ONBOARD:Ljava/lang/Boolean;

    .line 64
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PRINT_LOGO_ON_PAPER_ANDROID:Ljava/lang/Boolean;

    .line 66
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_PUBLISH_MERCHANT_CARD:Ljava/lang/Boolean;

    .line 68
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_REFER_FRIEND_IN_APP:Ljava/lang/Boolean;

    .line 70
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_SKIP_RECEIPT_SCREEN:Ljava/lang/Boolean;

    .line 72
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_TRANSFER_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 74
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_USE_ANDROID_ECR:Ljava/lang/Boolean;

    .line 76
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CARDHOLDER_NAME_ON_DIP_ANDROID:Ljava/lang/Boolean;

    .line 78
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COLLECT_TIP_BEFORE_AUTHORIZATION_PREFERRED:Ljava/lang/Boolean;

    .line 80
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COLLECT_TIP_BEFORE_AUTHORIZATION_REQUIRED:Ljava/lang/Boolean;

    .line 86
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COUNTRY_PREFERS_CONTACTLESS_CARDS:Ljava/lang/Boolean;

    .line 88
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Ljava/lang/Boolean;

    .line 90
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COUNTRY_PREFERS_EMV:Ljava/lang/Boolean;

    .line 92
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISABLE_ITEM_EDITING:Ljava/lang/Boolean;

    .line 94
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISABLE_MOBILE_REPORTS_APPLET_ANDROID:Ljava/lang/Boolean;

    .line 96
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Ljava/lang/Boolean;

    .line 98
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Ljava/lang/Boolean;

    .line 100
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_EMAIL_TRANSACTION_LEDGER:Ljava/lang/Boolean;

    .line 102
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISABLE_REGISTER_API:Ljava/lang/Boolean;

    .line 106
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_HIDE_MODIFIERS_ON_RECEIPTS_IN_ANDROID:Ljava/lang/Boolean;

    .line 108
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_INVOICE_TIPPING:Ljava/lang/Boolean;

    const-wide/16 v2, 0x0

    .line 110
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sput-object v2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ITEMS_BATCH_SIZE:Ljava/lang/Long;

    .line 114
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_MERGE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 116
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_OPT_IN_TO_OPEN_TICKETS_WITH_BETA_UI_ANDROID:Ljava/lang/Boolean;

    .line 118
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Ljava/lang/Boolean;

    .line 120
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PAYMENT_CARDS:Ljava/lang/Boolean;

    .line 122
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PROTECT_EDIT_TAX:Ljava/lang/Boolean;

    .line 126
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_REDUCE_PRINTING_WASTE_ANDROID:Ljava/lang/Boolean;

    .line 128
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Ljava/lang/Boolean;

    .line 130
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_REQUIRES_TRACK_2_IF_NOT_AMEX:Ljava/lang/Boolean;

    .line 132
    sget-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->BANKERS:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    sput-object v3, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ROUNDING_TYPE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 134
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SEE_ADVANCED_MODIFIERS:Ljava/lang/Boolean;

    .line 136
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SEE_PAYROLL_REGISTER_FEATURE_TOUR:Ljava/lang/Boolean;

    .line 138
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SEE_PAYROLL_REGISTER_LEARN_MORE_ANDROID:Ljava/lang/Boolean;

    .line 142
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SKIP_MODIFIER_DETAIL_SCREEN_IN_ANDROID:Ljava/lang/Boolean;

    .line 144
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Ljava/lang/Boolean;

    .line 146
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_FEATURE_TOUR:Ljava/lang/Boolean;

    .line 148
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_INCLUSIVE_TAXES_IN_CART:Ljava/lang/Boolean;

    .line 150
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_O1_JP_DEPRECATION_WARNING_IN_APP:Ljava/lang/Boolean;

    .line 152
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Ljava/lang/Boolean;

    .line 154
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SMS:Ljava/lang/Boolean;

    .line 156
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PLACEHOLDER:Ljava/lang/Boolean;

    .line 158
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SUPPORT_RESTAURANT_ITEMS:Ljava/lang/Boolean;

    .line 160
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SUPPORT_RETAIL_ITEMS:Ljava/lang/Boolean;

    .line 162
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SWIPE_TO_CREATE_OPEN_TICKET_WITH_NAME:Ljava/lang/Boolean;

    .line 164
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_TRANSFER_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 166
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_UPLOAD_SUPPORT_LEDGER:Ljava/lang/Boolean;

    .line 168
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_A10:Ljava/lang/Boolean;

    .line 170
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ANDROID_PRINTER_STATIONS:Ljava/lang/Boolean;

    .line 172
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ANDROID_BARCODE_SCANNING:Ljava/lang/Boolean;

    .line 174
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ANDROID_EMPLOYEE_MANAGEMENT:Ljava/lang/Boolean;

    .line 176
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_AUSTRALIAN_GROSS_SALES:Ljava/lang/Boolean;

    .line 178
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CARD_ASSOCIATED_COUPONS:Ljava/lang/Boolean;

    .line 180
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CARD_ON_FILE_IN_ANDROID_REGISTER:Ljava/lang/Boolean;

    .line 182
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CARD_ON_FILE_IN_ANDROID_REGISTER_ON_MOBILE:Ljava/lang/Boolean;

    .line 184
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CONDITIONAL_TAXES:Ljava/lang/Boolean;

    .line 186
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CRM_CUSTOM_ATTRIBUTES:Ljava/lang/Boolean;

    .line 188
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CRM_MESSAGING:Ljava/lang/Boolean;

    .line 190
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CRM_NOTES:Ljava/lang/Boolean;

    .line 192
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_DIRECTORY_WITH_INVOICES:Ljava/lang/Boolean;

    .line 194
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_ACTIVITY_LIST_IN_ANDROID:Ljava/lang/Boolean;

    .line 196
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_APPLET_IN_ANDROID:Ljava/lang/Boolean;

    .line 198
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_APPLET_IN_ANDROID_MOBILE:Ljava/lang/Boolean;

    .line 200
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_IN_ANDROID:Ljava/lang/Boolean;

    .line 202
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_IN_ANDROID_MOBILE:Ljava/lang/Boolean;

    .line 204
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_DINING_OPTIONS:Ljava/lang/Boolean;

    .line 206
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_EMPLOYEE_CANCEL_TRANSACTION:Ljava/lang/Boolean;

    .line 208
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_EMPLOYEE_FILTERING_FOR_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 210
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_EMPLOYEE_FILTERING_FOR_PAPER_SIGNATURE_ANDROID:Ljava/lang/Boolean;

    .line 212
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_EMPLOYEE_TIMECARDS:Ljava/lang/Boolean;

    .line 214
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ENFORCE_LOCATION_FIX:Ljava/lang/Boolean;

    .line 216
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_GIFT_CARDS_V2:Ljava/lang/Boolean;

    .line 218
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_IN_APP_TIMECARDS:Ljava/lang/Boolean;

    .line 220
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_INVENTORY_PLUS:Ljava/lang/Boolean;

    .line 222
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_INVOICE_APPLET_IN_ANDROID:Ljava/lang/Boolean;

    .line 224
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MAGSTRIPE_ONLY_READERS:Ljava/lang/Boolean;

    .line 226
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MOBILE_INVOICES:Ljava/lang/Boolean;

    .line 228
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MOBILE_INVOICES_WITH_DISCOUNTS:Ljava/lang/Boolean;

    .line 230
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MOBILE_INVOICES_WITH_MODIFIERS:Ljava/lang/Boolean;

    .line 232
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 234
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_OPEN_TICKETS_MOBILE_ANDROID:Ljava/lang/Boolean;

    .line 236
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_PAPER_SIGNATURE_ANDROID:Ljava/lang/Boolean;

    .line 238
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_PRE_TAX_TIPPING_ANDROID:Ljava/lang/Boolean;

    .line 240
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_PREDEFINED_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 242
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_REPORTS_CHARTS_ANDROID:Ljava/lang/Boolean;

    .line 244
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_R12:Ljava/lang/Boolean;

    .line 246
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_R12_PAIRING_V2:Ljava/lang/Boolean;

    .line 248
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_R6:Ljava/lang/Boolean;

    .line 250
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_SAFETYNET:Ljava/lang/Boolean;

    .line 252
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_SPLIT_TICKETS_ANDROID:Ljava/lang/Boolean;

    .line 254
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_SPLIT_TENDER:Ljava/lang/Boolean;

    .line 256
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_V3_CATALOG:Ljava/lang/Boolean;

    .line 258
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_VOID_COMP_ANDROID:Ljava/lang/Boolean;

    .line 260
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ZERO_AMOUNT_TENDER:Ljava/lang/Boolean;

    .line 262
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USES_DEVICE_PROFILE:Ljava/lang/Boolean;

    .line 264
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USING_TIME_TRACKING:Ljava/lang/Boolean;

    .line 266
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ZERO_AMOUNT_CONTACTLESS_ARQC_EXPERIMENT:Ljava/lang/Boolean;

    .line 268
    sput-object v2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ORDER_HUB_SYNC_PERIOD_WITH_NOTIFICATIONS:Ljava/lang/Long;

    .line 270
    sput-object v2, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ORDER_HUB_SYNC_PERIOD_WITHOUT_NOTIFICATIONS:Ljava/lang/Long;

    .line 272
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_HAS_BAZAAR_ONLINE_STORE:Ljava/lang/Boolean;

    .line 274
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISPLAY_LEARN_MORE_R4:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;Lokio/ByteString;)V
    .locals 1

    .line 1395
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1396
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    .line 1397
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->activation_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    .line 1398
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    .line 1399
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auto_capture_payments:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    .line 1400
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    .line 1401
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    .line 1402
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_enable_tipping:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    .line 1403
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_onboard:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    .line 1404
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->print_logo_on_paper_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    .line 1405
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_publish_merchant_card:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    .line 1406
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_refer_friend_in_app:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    .line 1407
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_skip_receipt_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    .line 1408
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    .line 1409
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_use_android_ecr:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    .line 1410
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    .line 1411
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    .line 1412
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    .line 1413
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    .line 1414
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_phone_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    .line 1415
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_twitter:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    .line 1416
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    .line 1417
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    .line 1418
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_emv:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    .line 1419
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_item_editing:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    .line 1420
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    .line 1421
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    .line 1422
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    .line 1423
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->email_transaction_ledger:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    .line 1424
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_register_api:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    .line 1425
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->help_center_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    .line 1426
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    .line 1427
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoice_tipping:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    .line 1428
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items_batch_size:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    .line 1429
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->login_type:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    .line 1430
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->merge_open_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    .line 1431
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    .line 1432
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    .line 1433
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payment_cards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    .line 1434
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->protect_edit_tax:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    .line 1435
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->privacy_policy_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    .line 1436
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reduce_printing_waste_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    .line 1437
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    .line 1438
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    .line 1439
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 1440
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_advanced_modifiers:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    .line 1441
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    .line 1442
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    .line 1443
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->seller_agreement_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    .line 1444
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    .line 1445
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    .line 1446
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_feature_tour:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    .line 1447
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    .line 1448
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    .line 1449
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_items_library_after_login:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    .line 1450
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->sms:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    .line 1451
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->placeholder:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    .line 1452
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_restaurant_items:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    .line 1453
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_retail_items:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    .line 1454
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_offline:Ljava/util/List;

    const-string v0, "supported_card_brands_offline"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    .line 1455
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_online:Ljava/util/List;

    const-string v0, "supported_card_brands_online"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    .line 1456
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    .line 1457
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->transfer_open_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    .line 1458
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->upload_support_ledger:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    .line 1459
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_a10:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    .line 1460
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_printer_stations:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    .line 1461
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_barcode_scanning:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    .line 1462
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_employee_management:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    .line 1463
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_australian_gross_sales:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    .line 1464
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_associated_coupons:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    .line 1465
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    .line 1466
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    .line 1467
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_conditional_taxes:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    .line 1468
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_custom_attributes:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    .line 1469
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_messaging:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    .line 1470
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_notes:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    .line 1471
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    .line 1472
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    .line 1473
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    .line 1474
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    .line 1475
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    .line 1476
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    .line 1477
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_dining_options:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    .line 1478
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee_cancel_transaction:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    .line 1479
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    .line 1480
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    .line 1481
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_timecards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    .line 1482
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_enforce_location_fix:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    .line 1483
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_gift_cards_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    .line 1484
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_in_app_timecards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    .line 1485
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_inventory_plus:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    .line 1486
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    .line 1487
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_magstripe_only_readers:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    .line 1488
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    .line 1489
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    .line 1490
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    .line 1491
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    .line 1492
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    .line 1493
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_paper_signature_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    .line 1494
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    .line 1495
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_predefined_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    .line 1496
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_reports_charts_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    .line 1497
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    .line 1498
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12_pairing_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    .line 1499
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r6:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    .line 1500
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_safetynet:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    .line 1501
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tickets_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    .line 1502
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tender:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    .line 1503
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_v3_catalog:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    .line 1504
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_void_comp_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    .line 1505
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_zero_amount_tender:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    .line 1506
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->uses_device_profile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    .line 1507
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->using_time_tracking:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    .line 1508
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    .line 1509
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    .line 1510
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    .line 1511
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->has_bazaar_online_store:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    .line 1512
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->display_learn_more_r4:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    .line 1513
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    .line 1514
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    .line 1515
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    .line 1516
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    .line 1517
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    .line 1518
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    .line 1519
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    .line 1520
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    .line 1521
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    .line 1522
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    .line 1523
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    .line 1524
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    .line 1525
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    .line 1526
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    .line 1527
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    .line 1528
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    .line 1529
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    .line 1530
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    .line 1531
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    .line 1532
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    .line 1533
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    .line 1534
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    .line 1535
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    .line 1536
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    .line 1537
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    .line 1538
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    .line 1539
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    .line 1540
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    .line 1541
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    .line 1542
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    .line 1543
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    .line 1544
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    .line 1545
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    .line 1546
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    .line 1547
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    .line 1548
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    .line 1549
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    .line 1550
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 2635
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1718
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1719
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 1720
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    .line 1721
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    .line 1722
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    .line 1723
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    .line 1724
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    .line 1725
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    .line 1726
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    .line 1727
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    .line 1728
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    .line 1729
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    .line 1730
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    .line 1731
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    .line 1732
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    .line 1733
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    .line 1734
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    .line 1735
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    .line 1736
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    .line 1737
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    .line 1738
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    .line 1739
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    .line 1740
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    .line 1741
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    .line 1742
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    .line 1743
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    .line 1744
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    .line 1745
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    .line 1746
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    .line 1747
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    .line 1748
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    .line 1749
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    .line 1750
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    .line 1751
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    .line 1752
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    .line 1753
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    .line 1754
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    .line 1755
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    .line 1756
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    .line 1757
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    .line 1758
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    .line 1759
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    .line 1760
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    .line 1761
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    .line 1762
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    .line 1763
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 1764
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    .line 1765
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    .line 1766
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    .line 1767
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    .line 1768
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    .line 1769
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    .line 1770
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    .line 1771
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    .line 1772
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    .line 1773
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    .line 1774
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    .line 1775
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    .line 1776
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    .line 1777
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    .line 1778
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    .line 1779
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    .line 1780
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    .line 1781
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    .line 1782
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    .line 1783
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    .line 1784
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    .line 1785
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    .line 1786
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    .line 1787
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    .line 1788
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    .line 1789
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    .line 1790
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    .line 1791
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    .line 1792
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    .line 1793
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    .line 1794
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    .line 1795
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    .line 1796
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    .line 1797
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    .line 1798
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    .line 1799
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    .line 1800
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    .line 1801
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    .line 1802
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    .line 1803
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    .line 1804
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    .line 1805
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    .line 1806
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    .line 1807
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    .line 1808
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    .line 1809
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    .line 1810
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    .line 1811
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    .line 1812
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    .line 1813
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    .line 1814
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    .line 1815
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    .line 1816
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    .line 1817
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    .line 1818
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    .line 1819
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    .line 1820
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    .line 1821
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    .line 1822
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    .line 1823
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    .line 1824
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    .line 1825
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    .line 1826
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    .line 1827
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    .line 1828
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    .line 1829
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    .line 1830
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    .line 1831
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    .line 1832
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    .line 1833
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    .line 1834
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    .line 1835
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    .line 1836
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    .line 1837
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    .line 1838
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    .line 1839
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    .line 1840
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    .line 1841
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    .line 1842
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    .line 1843
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    .line 1844
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    .line 1845
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    .line 1846
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    .line 1847
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    .line 1848
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    .line 1849
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    .line 1850
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    .line 1851
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    .line 1852
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    .line 1853
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    .line 1854
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    .line 1855
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    .line 1856
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    .line 1857
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    .line 1858
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    .line 1859
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    .line 1860
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    .line 1861
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    .line 1862
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    .line 1863
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    .line 1864
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    .line 1865
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    .line 1866
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    .line 1867
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    .line 1868
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    .line 1869
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    .line 1870
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    .line 1871
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    .line 1872
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    .line 1873
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    .line 1874
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    .line 1875
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1880
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_99

    .line 1882
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1883
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1884
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1885
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1886
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1887
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1888
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1889
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1890
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1891
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1892
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1893
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1894
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1895
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1896
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1897
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1898
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Cardreaders;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1899
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1900
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1901
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1902
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1903
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1904
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1905
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1906
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1907
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1908
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1909
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1910
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1911
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1912
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1913
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1914
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1915
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1916
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1917
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1918
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1919
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1920
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1921
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1922
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1923
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_28

    :cond_28
    const/4 v1, 0x0

    :goto_28
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1924
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_29

    :cond_29
    const/4 v1, 0x0

    :goto_29
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1925
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    if-eqz v1, :cond_2a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2a

    :cond_2a
    const/4 v1, 0x0

    :goto_2a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1926
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->hashCode()I

    move-result v1

    goto :goto_2b

    :cond_2b
    const/4 v1, 0x0

    :goto_2b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1927
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2c

    :cond_2c
    const/4 v1, 0x0

    :goto_2c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1928
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    if-eqz v1, :cond_2d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2d

    :cond_2d
    const/4 v1, 0x0

    :goto_2d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1929
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2e

    :cond_2e
    const/4 v1, 0x0

    :goto_2e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1930
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2f

    :cond_2f
    const/4 v1, 0x0

    :goto_2f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1931
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_30

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_30

    :cond_30
    const/4 v1, 0x0

    :goto_30
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1932
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_31

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_31

    :cond_31
    const/4 v1, 0x0

    :goto_31
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1933
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    if-eqz v1, :cond_32

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_32

    :cond_32
    const/4 v1, 0x0

    :goto_32
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1934
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    if-eqz v1, :cond_33

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_33

    :cond_33
    const/4 v1, 0x0

    :goto_33
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1935
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_34

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_34

    :cond_34
    const/4 v1, 0x0

    :goto_34
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1936
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    if-eqz v1, :cond_35

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_35

    :cond_35
    const/4 v1, 0x0

    :goto_35
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1937
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_36

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_36

    :cond_36
    const/4 v1, 0x0

    :goto_36
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1938
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    if-eqz v1, :cond_37

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_37

    :cond_37
    const/4 v1, 0x0

    :goto_37
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1939
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_38

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_38

    :cond_38
    const/4 v1, 0x0

    :goto_38
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1940
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_39

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_39

    :cond_39
    const/4 v1, 0x0

    :goto_39
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1941
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1942
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1943
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-eqz v1, :cond_3a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3a

    :cond_3a
    const/4 v1, 0x0

    :goto_3a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1944
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3b

    :cond_3b
    const/4 v1, 0x0

    :goto_3b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1945
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    if-eqz v1, :cond_3c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3c

    :cond_3c
    const/4 v1, 0x0

    :goto_3c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1946
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    if-eqz v1, :cond_3d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3d

    :cond_3d
    const/4 v1, 0x0

    :goto_3d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1947
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    if-eqz v1, :cond_3e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3e

    :cond_3e
    const/4 v1, 0x0

    :goto_3e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1948
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    if-eqz v1, :cond_3f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3f

    :cond_3f
    const/4 v1, 0x0

    :goto_3f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1949
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    if-eqz v1, :cond_40

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_40

    :cond_40
    const/4 v1, 0x0

    :goto_40
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1950
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    if-eqz v1, :cond_41

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_41

    :cond_41
    const/4 v1, 0x0

    :goto_41
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1951
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    if-eqz v1, :cond_42

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_42

    :cond_42
    const/4 v1, 0x0

    :goto_42
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1952
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    if-eqz v1, :cond_43

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_43

    :cond_43
    const/4 v1, 0x0

    :goto_43
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1953
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_44

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_44

    :cond_44
    const/4 v1, 0x0

    :goto_44
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1954
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    if-eqz v1, :cond_45

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_45

    :cond_45
    const/4 v1, 0x0

    :goto_45
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1955
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    if-eqz v1, :cond_46

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_46

    :cond_46
    const/4 v1, 0x0

    :goto_46
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1956
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    if-eqz v1, :cond_47

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_47

    :cond_47
    const/4 v1, 0x0

    :goto_47
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1957
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_48

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_48

    :cond_48
    const/4 v1, 0x0

    :goto_48
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1958
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    if-eqz v1, :cond_49

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_49

    :cond_49
    const/4 v1, 0x0

    :goto_49
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1959
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4a

    :cond_4a
    const/4 v1, 0x0

    :goto_4a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1960
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4b

    :cond_4b
    const/4 v1, 0x0

    :goto_4b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1961
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_4c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4c

    :cond_4c
    const/4 v1, 0x0

    :goto_4c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1962
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4d

    :cond_4d
    const/4 v1, 0x0

    :goto_4d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1963
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_4e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4e

    :cond_4e
    const/4 v1, 0x0

    :goto_4e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1964
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_4f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4f

    :cond_4f
    const/4 v1, 0x0

    :goto_4f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1965
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_50

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_50

    :cond_50
    const/4 v1, 0x0

    :goto_50
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1966
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_51

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_51

    :cond_51
    const/4 v1, 0x0

    :goto_51
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1967
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_52

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_52

    :cond_52
    const/4 v1, 0x0

    :goto_52
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1968
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    if-eqz v1, :cond_53

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_53

    :cond_53
    const/4 v1, 0x0

    :goto_53
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1969
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    if-eqz v1, :cond_54

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_54

    :cond_54
    const/4 v1, 0x0

    :goto_54
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1970
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_55

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_55

    :cond_55
    const/4 v1, 0x0

    :goto_55
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1971
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    if-eqz v1, :cond_56

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_56

    :cond_56
    const/4 v1, 0x0

    :goto_56
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1972
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    if-eqz v1, :cond_57

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_57

    :cond_57
    const/4 v1, 0x0

    :goto_57
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1973
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_58

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_58

    :cond_58
    const/4 v1, 0x0

    :goto_58
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1974
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    if-eqz v1, :cond_59

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_59

    :cond_59
    const/4 v1, 0x0

    :goto_59
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1975
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    if-eqz v1, :cond_5a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5a

    :cond_5a
    const/4 v1, 0x0

    :goto_5a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1976
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_5b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5b

    :cond_5b
    const/4 v1, 0x0

    :goto_5b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1977
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    if-eqz v1, :cond_5c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5c

    :cond_5c
    const/4 v1, 0x0

    :goto_5c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1978
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5d

    :cond_5d
    const/4 v1, 0x0

    :goto_5d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1979
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5e

    :cond_5e
    const/4 v1, 0x0

    :goto_5e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1980
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5f

    :cond_5f
    const/4 v1, 0x0

    :goto_5f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1981
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_60

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_60

    :cond_60
    const/4 v1, 0x0

    :goto_60
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1982
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_61

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_61

    :cond_61
    const/4 v1, 0x0

    :goto_61
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1983
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_62

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_62

    :cond_62
    const/4 v1, 0x0

    :goto_62
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1984
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    if-eqz v1, :cond_63

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_63

    :cond_63
    const/4 v1, 0x0

    :goto_63
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1985
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_64

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_64

    :cond_64
    const/4 v1, 0x0

    :goto_64
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1986
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    if-eqz v1, :cond_65

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_65

    :cond_65
    const/4 v1, 0x0

    :goto_65
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1987
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    if-eqz v1, :cond_66

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_66

    :cond_66
    const/4 v1, 0x0

    :goto_66
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1988
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_67

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_67

    :cond_67
    const/4 v1, 0x0

    :goto_67
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1989
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    if-eqz v1, :cond_68

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_68

    :cond_68
    const/4 v1, 0x0

    :goto_68
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1990
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    if-eqz v1, :cond_69

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_69

    :cond_69
    const/4 v1, 0x0

    :goto_69
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1991
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6a

    :cond_6a
    const/4 v1, 0x0

    :goto_6a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1992
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    if-eqz v1, :cond_6b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6b

    :cond_6b
    const/4 v1, 0x0

    :goto_6b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1993
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_6c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6c

    :cond_6c
    const/4 v1, 0x0

    :goto_6c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1994
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    if-eqz v1, :cond_6d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6d

    :cond_6d
    const/4 v1, 0x0

    :goto_6d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1995
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    if-eqz v1, :cond_6e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6e

    :cond_6e
    const/4 v1, 0x0

    :goto_6e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1996
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    if-eqz v1, :cond_6f

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_6f

    :cond_6f
    const/4 v1, 0x0

    :goto_6f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1997
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    if-eqz v1, :cond_70

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_70

    :cond_70
    const/4 v1, 0x0

    :goto_70
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1998
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    if-eqz v1, :cond_71

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_71

    :cond_71
    const/4 v1, 0x0

    :goto_71
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1999
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    if-eqz v1, :cond_72

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_72

    :cond_72
    const/4 v1, 0x0

    :goto_72
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2000
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    if-eqz v1, :cond_73

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->hashCode()I

    move-result v1

    goto :goto_73

    :cond_73
    const/4 v1, 0x0

    :goto_73
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2001
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    if-eqz v1, :cond_74

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->hashCode()I

    move-result v1

    goto :goto_74

    :cond_74
    const/4 v1, 0x0

    :goto_74
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2002
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    if-eqz v1, :cond_75

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->hashCode()I

    move-result v1

    goto :goto_75

    :cond_75
    const/4 v1, 0x0

    :goto_75
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2003
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    if-eqz v1, :cond_76

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->hashCode()I

    move-result v1

    goto :goto_76

    :cond_76
    const/4 v1, 0x0

    :goto_76
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2004
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    if-eqz v1, :cond_77

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hashCode()I

    move-result v1

    goto :goto_77

    :cond_77
    const/4 v1, 0x0

    :goto_77
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2005
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    if-eqz v1, :cond_78

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hashCode()I

    move-result v1

    goto :goto_78

    :cond_78
    const/4 v1, 0x0

    :goto_78
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2006
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    if-eqz v1, :cond_79

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->hashCode()I

    move-result v1

    goto :goto_79

    :cond_79
    const/4 v1, 0x0

    :goto_79
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2007
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    if-eqz v1, :cond_7a

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->hashCode()I

    move-result v1

    goto :goto_7a

    :cond_7a
    const/4 v1, 0x0

    :goto_7a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2008
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    if-eqz v1, :cond_7b

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->hashCode()I

    move-result v1

    goto :goto_7b

    :cond_7b
    const/4 v1, 0x0

    :goto_7b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2009
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    if-eqz v1, :cond_7c

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->hashCode()I

    move-result v1

    goto :goto_7c

    :cond_7c
    const/4 v1, 0x0

    :goto_7c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2010
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    if-eqz v1, :cond_7d

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->hashCode()I

    move-result v1

    goto :goto_7d

    :cond_7d
    const/4 v1, 0x0

    :goto_7d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2011
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    if-eqz v1, :cond_7e

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->hashCode()I

    move-result v1

    goto :goto_7e

    :cond_7e
    const/4 v1, 0x0

    :goto_7e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2012
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    if-eqz v1, :cond_7f

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->hashCode()I

    move-result v1

    goto :goto_7f

    :cond_7f
    const/4 v1, 0x0

    :goto_7f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2013
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    if-eqz v1, :cond_80

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->hashCode()I

    move-result v1

    goto :goto_80

    :cond_80
    const/4 v1, 0x0

    :goto_80
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2014
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    if-eqz v1, :cond_81

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->hashCode()I

    move-result v1

    goto :goto_81

    :cond_81
    const/4 v1, 0x0

    :goto_81
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2015
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    if-eqz v1, :cond_82

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->hashCode()I

    move-result v1

    goto :goto_82

    :cond_82
    const/4 v1, 0x0

    :goto_82
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2016
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    if-eqz v1, :cond_83

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;->hashCode()I

    move-result v1

    goto :goto_83

    :cond_83
    const/4 v1, 0x0

    :goto_83
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2017
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    if-eqz v1, :cond_84

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->hashCode()I

    move-result v1

    goto :goto_84

    :cond_84
    const/4 v1, 0x0

    :goto_84
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2018
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    if-eqz v1, :cond_85

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->hashCode()I

    move-result v1

    goto :goto_85

    :cond_85
    const/4 v1, 0x0

    :goto_85
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2019
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    if-eqz v1, :cond_86

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->hashCode()I

    move-result v1

    goto :goto_86

    :cond_86
    const/4 v1, 0x0

    :goto_86
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2020
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    if-eqz v1, :cond_87

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hashCode()I

    move-result v1

    goto :goto_87

    :cond_87
    const/4 v1, 0x0

    :goto_87
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2021
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    if-eqz v1, :cond_88

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;->hashCode()I

    move-result v1

    goto :goto_88

    :cond_88
    const/4 v1, 0x0

    :goto_88
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2022
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    if-eqz v1, :cond_89

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->hashCode()I

    move-result v1

    goto :goto_89

    :cond_89
    const/4 v1, 0x0

    :goto_89
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2023
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    if-eqz v1, :cond_8a

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->hashCode()I

    move-result v1

    goto :goto_8a

    :cond_8a
    const/4 v1, 0x0

    :goto_8a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2024
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    if-eqz v1, :cond_8b

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->hashCode()I

    move-result v1

    goto :goto_8b

    :cond_8b
    const/4 v1, 0x0

    :goto_8b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2025
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    if-eqz v1, :cond_8c

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->hashCode()I

    move-result v1

    goto :goto_8c

    :cond_8c
    const/4 v1, 0x0

    :goto_8c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2026
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    if-eqz v1, :cond_8d

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->hashCode()I

    move-result v1

    goto :goto_8d

    :cond_8d
    const/4 v1, 0x0

    :goto_8d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2027
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    if-eqz v1, :cond_8e

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;->hashCode()I

    move-result v1

    goto :goto_8e

    :cond_8e
    const/4 v1, 0x0

    :goto_8e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2028
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    if-eqz v1, :cond_8f

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->hashCode()I

    move-result v1

    goto :goto_8f

    :cond_8f
    const/4 v1, 0x0

    :goto_8f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2029
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    if-eqz v1, :cond_90

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->hashCode()I

    move-result v1

    goto :goto_90

    :cond_90
    const/4 v1, 0x0

    :goto_90
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2030
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    if-eqz v1, :cond_91

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->hashCode()I

    move-result v1

    goto :goto_91

    :cond_91
    const/4 v1, 0x0

    :goto_91
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2031
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v1, :cond_92

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->hashCode()I

    move-result v1

    goto :goto_92

    :cond_92
    const/4 v1, 0x0

    :goto_92
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2032
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    if-eqz v1, :cond_93

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->hashCode()I

    move-result v1

    goto :goto_93

    :cond_93
    const/4 v1, 0x0

    :goto_93
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2033
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    if-eqz v1, :cond_94

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->hashCode()I

    move-result v1

    goto :goto_94

    :cond_94
    const/4 v1, 0x0

    :goto_94
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2034
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    if-eqz v1, :cond_95

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->hashCode()I

    move-result v1

    goto :goto_95

    :cond_95
    const/4 v1, 0x0

    :goto_95
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2035
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    if-eqz v1, :cond_96

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;->hashCode()I

    move-result v1

    goto :goto_96

    :cond_96
    const/4 v1, 0x0

    :goto_96
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2036
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    if-eqz v1, :cond_97

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->hashCode()I

    move-result v1

    goto :goto_97

    :cond_97
    const/4 v1, 0x0

    :goto_97
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2037
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    if-eqz v1, :cond_98

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->hashCode()I

    move-result v2

    :cond_98
    add-int/2addr v0, v2

    .line 2038
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_99
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;
    .locals 2

    .line 1555
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;-><init>()V

    .line 1556
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    .line 1557
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->activation_url:Ljava/lang/String;

    .line 1558
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    .line 1559
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auto_capture_payments:Ljava/lang/Boolean;

    .line 1560
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    .line 1561
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    .line 1562
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_enable_tipping:Ljava/lang/Boolean;

    .line 1563
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_onboard:Ljava/lang/Boolean;

    .line 1564
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->print_logo_on_paper_android:Ljava/lang/Boolean;

    .line 1565
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_publish_merchant_card:Ljava/lang/Boolean;

    .line 1566
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_refer_friend_in_app:Ljava/lang/Boolean;

    .line 1567
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_skip_receipt_screen:Ljava/lang/Boolean;

    .line 1568
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    .line 1569
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_use_android_ecr:Ljava/lang/Boolean;

    .line 1570
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    .line 1571
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    .line 1572
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    .line 1573
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    .line 1574
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_phone_number:Ljava/lang/String;

    .line 1575
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_twitter:Ljava/lang/String;

    .line 1576
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    .line 1577
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    .line 1578
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_emv:Ljava/lang/Boolean;

    .line 1579
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_item_editing:Ljava/lang/Boolean;

    .line 1580
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    .line 1581
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    .line 1582
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    .line 1583
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->email_transaction_ledger:Ljava/lang/Boolean;

    .line 1584
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_register_api:Ljava/lang/Boolean;

    .line 1585
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->help_center_url:Ljava/lang/String;

    .line 1586
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    .line 1587
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoice_tipping:Ljava/lang/Boolean;

    .line 1588
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items_batch_size:Ljava/lang/Long;

    .line 1589
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->login_type:Ljava/lang/String;

    .line 1590
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->merge_open_tickets_android:Ljava/lang/Boolean;

    .line 1591
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    .line 1592
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    .line 1593
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payment_cards:Ljava/lang/Boolean;

    .line 1594
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->protect_edit_tax:Ljava/lang/Boolean;

    .line 1595
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->privacy_policy_url:Ljava/lang/String;

    .line 1596
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reduce_printing_waste_android:Ljava/lang/Boolean;

    .line 1597
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    .line 1598
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    .line 1599
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    .line 1600
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_advanced_modifiers:Ljava/lang/Boolean;

    .line 1601
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    .line 1602
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    .line 1603
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->seller_agreement_url:Ljava/lang/String;

    .line 1604
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    .line 1605
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    .line 1606
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_feature_tour:Ljava/lang/Boolean;

    .line 1607
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    .line 1608
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    .line 1609
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_items_library_after_login:Ljava/lang/Boolean;

    .line 1610
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->sms:Ljava/lang/Boolean;

    .line 1611
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->placeholder:Ljava/lang/Boolean;

    .line 1612
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_restaurant_items:Ljava/lang/Boolean;

    .line 1613
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_retail_items:Ljava/lang/Boolean;

    .line 1614
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_offline:Ljava/util/List;

    .line 1615
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_online:Ljava/util/List;

    .line 1616
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    .line 1617
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->transfer_open_tickets_android:Ljava/lang/Boolean;

    .line 1618
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->upload_support_ledger:Ljava/lang/Boolean;

    .line 1619
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_a10:Ljava/lang/Boolean;

    .line 1620
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_printer_stations:Ljava/lang/Boolean;

    .line 1621
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_barcode_scanning:Ljava/lang/Boolean;

    .line 1622
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_employee_management:Ljava/lang/Boolean;

    .line 1623
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_australian_gross_sales:Ljava/lang/Boolean;

    .line 1624
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_associated_coupons:Ljava/lang/Boolean;

    .line 1625
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    .line 1626
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    .line 1627
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_conditional_taxes:Ljava/lang/Boolean;

    .line 1628
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_custom_attributes:Ljava/lang/Boolean;

    .line 1629
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_messaging:Ljava/lang/Boolean;

    .line 1630
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_notes:Ljava/lang/Boolean;

    .line 1631
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    .line 1632
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    .line 1633
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    .line 1634
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    .line 1635
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android:Ljava/lang/Boolean;

    .line 1636
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    .line 1637
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_dining_options:Ljava/lang/Boolean;

    .line 1638
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee_cancel_transaction:Ljava/lang/Boolean;

    .line 1639
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    .line 1640
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    .line 1641
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_timecards:Ljava/lang/Boolean;

    .line 1642
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_enforce_location_fix:Ljava/lang/Boolean;

    .line 1643
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_gift_cards_v2:Ljava/lang/Boolean;

    .line 1644
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_in_app_timecards:Ljava/lang/Boolean;

    .line 1645
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_inventory_plus:Ljava/lang/Boolean;

    .line 1646
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    .line 1647
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_magstripe_only_readers:Ljava/lang/Boolean;

    .line 1648
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices:Ljava/lang/Boolean;

    .line 1649
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    .line 1650
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    .line 1651
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_android:Ljava/lang/Boolean;

    .line 1652
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    .line 1653
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_paper_signature_android:Ljava/lang/Boolean;

    .line 1654
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    .line 1655
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_predefined_tickets_android:Ljava/lang/Boolean;

    .line 1656
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_reports_charts_android:Ljava/lang/Boolean;

    .line 1657
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12:Ljava/lang/Boolean;

    .line 1658
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12_pairing_v2:Ljava/lang/Boolean;

    .line 1659
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r6:Ljava/lang/Boolean;

    .line 1660
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_safetynet:Ljava/lang/Boolean;

    .line 1661
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tickets_android:Ljava/lang/Boolean;

    .line 1662
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tender:Ljava/lang/Boolean;

    .line 1663
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_v3_catalog:Ljava/lang/Boolean;

    .line 1664
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_void_comp_android:Ljava/lang/Boolean;

    .line 1665
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_zero_amount_tender:Ljava/lang/Boolean;

    .line 1666
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->uses_device_profile:Ljava/lang/Boolean;

    .line 1667
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->using_time_tracking:Ljava/lang/Boolean;

    .line 1668
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    .line 1669
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    .line 1670
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    .line 1671
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->has_bazaar_online_store:Ljava/lang/Boolean;

    .line 1672
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->display_learn_more_r4:Ljava/lang/Boolean;

    .line 1673
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    .line 1674
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    .line 1675
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    .line 1676
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    .line 1677
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    .line 1678
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    .line 1679
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    .line 1680
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    .line 1681
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    .line 1682
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    .line 1683
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    .line 1684
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    .line 1685
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    .line 1686
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    .line 1687
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    .line 1688
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    .line 1689
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    .line 1690
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    .line 1691
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    .line 1692
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    .line 1693
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    .line 1694
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    .line 1695
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    .line 1696
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    .line 1697
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    .line 1698
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    .line 1699
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    .line 1700
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    .line 1701
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    .line 1702
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    .line 1703
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    .line 1704
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    .line 1705
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    .line 1706
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    .line 1707
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    .line 1708
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    .line 1709
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    .line 1710
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    .line 1711
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/FlagsAndPermissions;
    .locals 2

    .line 2476
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->accept_third_party_gift_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2477
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->activation_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2478
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->allow_payment_for_invoice_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2479
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auto_capture_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2480
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_allow_swipe_for_chip_cards_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2481
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_bulk_delete_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2482
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_enable_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2483
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_onboard(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2484
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->print_logo_on_paper_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2485
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_publish_merchant_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2486
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_refer_friend_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2487
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2488
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2489
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_use_android_ecr(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2490
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardholder_name_on_dip_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2491
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders(Lcom/squareup/server/account/protos/Cardreaders;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2492
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_preferred(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2493
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_required(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2494
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_phone_number(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2495
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->contact_support_twitter(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2496
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2497
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_no_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2498
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    if-eqz v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_emv(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2499
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    if-eqz v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_item_editing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2500
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_mobile_reports_applet_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2501
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    if-eqz v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_square_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2502
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_third_party_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2503
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->email_transaction_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2504
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_register_api(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2505
    :cond_1c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    if-eqz v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->help_center_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2506
    :cond_1d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->hide_modifiers_on_receipts_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2507
    :cond_1e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    if-eqz v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoice_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2508
    :cond_1f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    if-eqz v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items_batch_size(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2509
    :cond_20
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    if-eqz v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->login_type(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2510
    :cond_21
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->merge_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2511
    :cond_22
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_open_tickets_with_beta_ui_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2512
    :cond_23
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    if-eqz v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_store_and_forward_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2513
    :cond_24
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    if-eqz v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payment_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2514
    :cond_25
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    if-eqz v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->protect_edit_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2515
    :cond_26
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    if-eqz v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->privacy_policy_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2516
    :cond_27
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reduce_printing_waste_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2517
    :cond_28
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    if-eqz v0, :cond_29

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->require_secure_session_for_r6_swipe(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2518
    :cond_29
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    if-eqz v0, :cond_2a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->requires_track_2_if_not_amex(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2519
    :cond_2a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    if-eqz v0, :cond_2b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->rounding_type(Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2520
    :cond_2b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    if-eqz v0, :cond_2c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_advanced_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2521
    :cond_2c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    if-eqz v0, :cond_2d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2522
    :cond_2d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_2e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_learn_more_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2523
    :cond_2e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    if-eqz v0, :cond_2f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->seller_agreement_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2524
    :cond_2f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_30

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_modifier_detail_screen_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2525
    :cond_30
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    if-eqz v0, :cond_31

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_signatures_for_small_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2526
    :cond_31
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    if-eqz v0, :cond_32

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2527
    :cond_32
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    if-eqz v0, :cond_33

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_inclusive_taxes_in_cart(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2528
    :cond_33
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    if-eqz v0, :cond_34

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_o1_jp_deprecation_warning_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2529
    :cond_34
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    if-eqz v0, :cond_35

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_items_library_after_login(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2530
    :cond_35
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    if-eqz v0, :cond_36

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2531
    :cond_36
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    if-eqz v0, :cond_37

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->placeholder(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2532
    :cond_37
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    if-eqz v0, :cond_38

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_restaurant_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2533
    :cond_38
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    if-eqz v0, :cond_39

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_retail_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2534
    :cond_39
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_offline(Ljava/util/List;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2535
    :cond_3a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->supported_card_brands_online(Ljava/util/List;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2536
    :cond_3b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-eqz v0, :cond_3c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->swipe_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2537
    :cond_3c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_3d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2538
    :cond_3d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    if-eqz v0, :cond_3e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->upload_support_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2539
    :cond_3e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    if-eqz v0, :cond_3f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_a10(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2540
    :cond_3f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    if-eqz v0, :cond_40

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_printer_stations(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2541
    :cond_40
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    if-eqz v0, :cond_41

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_barcode_scanning(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2542
    :cond_41
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    if-eqz v0, :cond_42

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_employee_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2543
    :cond_42
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    if-eqz v0, :cond_43

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_australian_gross_sales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2544
    :cond_43
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    if-eqz v0, :cond_44

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_associated_coupons(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2545
    :cond_44
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    if-eqz v0, :cond_45

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2546
    :cond_45
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_46

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register_on_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2547
    :cond_46
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    if-eqz v0, :cond_47

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_conditional_taxes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2548
    :cond_47
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    if-eqz v0, :cond_48

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_custom_attributes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2549
    :cond_48
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    if-eqz v0, :cond_49

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_messaging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2550
    :cond_49
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    if-eqz v0, :cond_4a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2551
    :cond_4a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    if-eqz v0, :cond_4b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_directory_with_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2552
    :cond_4b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_4c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_activity_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2553
    :cond_4c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_4d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2554
    :cond_4d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_4e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2555
    :cond_4e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_4f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2556
    :cond_4f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_50

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2557
    :cond_50
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    if-eqz v0, :cond_51

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_dining_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2558
    :cond_51
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    if-eqz v0, :cond_52

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee_cancel_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2559
    :cond_52
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_53

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2560
    :cond_53
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_54

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2561
    :cond_54
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    if-eqz v0, :cond_55

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2562
    :cond_55
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    if-eqz v0, :cond_56

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_enforce_location_fix(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2563
    :cond_56
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_57

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_gift_cards_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2564
    :cond_57
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    if-eqz v0, :cond_58

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_in_app_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2565
    :cond_58
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    if-eqz v0, :cond_59

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_inventory_plus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2566
    :cond_59
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_5a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_invoice_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2567
    :cond_5a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    if-eqz v0, :cond_5b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_magstripe_only_readers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2568
    :cond_5b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    if-eqz v0, :cond_5c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2569
    :cond_5c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    if-eqz v0, :cond_5d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2570
    :cond_5d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    if-eqz v0, :cond_5e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2571
    :cond_5e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_5f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2572
    :cond_5f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_60

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_mobile_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2573
    :cond_60
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_61

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2574
    :cond_61
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_62

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2575
    :cond_62
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_63

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_predefined_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2576
    :cond_63
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_64

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_reports_charts_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2577
    :cond_64
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    if-eqz v0, :cond_65

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2578
    :cond_65
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_66

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12_pairing_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2579
    :cond_66
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    if-eqz v0, :cond_67

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r6(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2580
    :cond_67
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    if-eqz v0, :cond_68

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_safetynet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2581
    :cond_68
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_69

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2582
    :cond_69
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    if-eqz v0, :cond_6a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2583
    :cond_6a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    if-eqz v0, :cond_6b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_v3_catalog(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2584
    :cond_6b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_6c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_void_comp_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2585
    :cond_6c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    if-eqz v0, :cond_6d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_zero_amount_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2586
    :cond_6d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    if-eqz v0, :cond_6e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->uses_device_profile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2587
    :cond_6e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    if-eqz v0, :cond_6f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2588
    :cond_6f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    if-eqz v0, :cond_70

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->zero_amount_contactless_arqc_experiment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2589
    :cond_70
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    if-eqz v0, :cond_71

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_with_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2590
    :cond_71
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    if-eqz v0, :cond_72

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_without_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2591
    :cond_72
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    if-eqz v0, :cond_73

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->has_bazaar_online_store(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2592
    :cond_73
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    if-eqz v0, :cond_74

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->display_learn_more_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2593
    :cond_74
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    if-eqz v0, :cond_75

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2594
    :cond_75
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    if-eqz v0, :cond_76

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2595
    :cond_76
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    if-eqz v0, :cond_77

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2596
    :cond_77
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    if-eqz v0, :cond_78

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2597
    :cond_78
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    if-eqz v0, :cond_79

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2598
    :cond_79
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    if-eqz v0, :cond_7a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2599
    :cond_7a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    if-eqz v0, :cond_7b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2600
    :cond_7b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    if-eqz v0, :cond_7c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2601
    :cond_7c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    if-eqz v0, :cond_7d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2602
    :cond_7d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    if-eqz v0, :cond_7e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2603
    :cond_7e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    if-eqz v0, :cond_7f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2604
    :cond_7f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    if-eqz v0, :cond_80

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2605
    :cond_80
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    if-eqz v0, :cond_81

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2606
    :cond_81
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    if-eqz v0, :cond_82

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2607
    :cond_82
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    if-eqz v0, :cond_83

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2608
    :cond_83
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    if-eqz v0, :cond_84

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2609
    :cond_84
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    if-eqz v0, :cond_85

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2610
    :cond_85
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    if-eqz v0, :cond_86

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2611
    :cond_86
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    if-eqz v0, :cond_87

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees(Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2612
    :cond_87
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    if-eqz v0, :cond_88

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2613
    :cond_88
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    if-eqz v0, :cond_89

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2614
    :cond_89
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    if-eqz v0, :cond_8a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2615
    :cond_8a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    if-eqz v0, :cond_8b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2616
    :cond_8b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    if-eqz v0, :cond_8c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2617
    :cond_8c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    if-eqz v0, :cond_8d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs(Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2618
    :cond_8d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    if-eqz v0, :cond_8e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2619
    :cond_8e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    if-eqz v0, :cond_8f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2620
    :cond_8f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    if-eqz v0, :cond_90

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount(Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2621
    :cond_90
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    if-eqz v0, :cond_91

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2622
    :cond_91
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    if-eqz v0, :cond_92

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2623
    :cond_92
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    if-eqz v0, :cond_93

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2624
    :cond_93
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v0, :cond_94

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2625
    :cond_94
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    if-eqz v0, :cond_95

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2626
    :cond_95
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    if-eqz v0, :cond_96

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2627
    :cond_96
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    if-eqz v0, :cond_97

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2628
    :cond_97
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    if-eqz v0, :cond_98

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital(Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2629
    :cond_98
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    if-eqz v0, :cond_99

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2630
    :cond_99
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    if-eqz v0, :cond_9a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth(Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    :cond_9a
    if-nez v1, :cond_9b

    move-object p1, p0

    goto :goto_0

    .line 2631
    :cond_9b
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions;
    .locals 3

    .line 2207
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ACCEPT_THIRD_PARTY_GIFT_CARDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->accept_third_party_gift_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2208
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ALLOW_PAYMENT_FOR_INVOICE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->allow_payment_for_invoice_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2209
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_AUTO_CAPTURE_PAYMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auto_capture_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2210
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_ALLOW_SWIPE_FOR_CHIP_CARDS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_allow_swipe_for_chip_cards_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2211
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_BULK_DELETE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_bulk_delete_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2212
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_ENABLE_TIPPING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_enable_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2213
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_ONBOARD:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_onboard(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2214
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PRINT_LOGO_ON_PAPER_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->print_logo_on_paper_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2215
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_PUBLISH_MERCHANT_CARD:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_publish_merchant_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2216
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_REFER_FRIEND_IN_APP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_refer_friend_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2217
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_SKIP_RECEIPT_SCREEN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2218
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_TRANSFER_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2219
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CAN_USE_ANDROID_ECR:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->can_use_android_ecr(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2220
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_CARDHOLDER_NAME_ON_DIP_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardholder_name_on_dip_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2221
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    if-eqz v0, :cond_e

    .line 2222
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Cardreaders;->populateDefaults()Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object v0

    .line 2223
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    if-eq v0, v2, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->cardreaders(Lcom/squareup/server/account/protos/Cardreaders;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2225
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COLLECT_TIP_BEFORE_AUTHORIZATION_PREFERRED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_preferred(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2226
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COLLECT_TIP_BEFORE_AUTHORIZATION_REQUIRED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->collect_tip_before_authorization_required(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2227
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COUNTRY_PREFERS_CONTACTLESS_CARDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2228
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_no_contactless_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2229
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_COUNTRY_PREFERS_EMV:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->country_prefers_emv(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2230
    :cond_13
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    if-nez v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISABLE_ITEM_EDITING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_item_editing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2231
    :cond_14
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISABLE_MOBILE_REPORTS_APPLET_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_mobile_reports_applet_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2232
    :cond_15
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    if-nez v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_square_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2233
    :cond_16
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    if-nez v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ELIGIBLE_FOR_THIRD_PARTY_CARD_PROCESSING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->eligible_for_third_party_card_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2234
    :cond_17
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    if-nez v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_EMAIL_TRANSACTION_LEDGER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->email_transaction_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2235
    :cond_18
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    if-nez v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISABLE_REGISTER_API:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->disable_register_api(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2236
    :cond_19
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_HIDE_MODIFIERS_ON_RECEIPTS_IN_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->hide_modifiers_on_receipts_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2237
    :cond_1a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    if-nez v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_INVOICE_TIPPING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoice_tipping(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2238
    :cond_1b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    if-nez v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ITEMS_BATCH_SIZE:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items_batch_size(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2239
    :cond_1c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_MERGE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->merge_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2240
    :cond_1d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_OPT_IN_TO_OPEN_TICKETS_WITH_BETA_UI_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_open_tickets_with_beta_ui_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2241
    :cond_1e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    if-nez v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_OPT_IN_TO_STORE_AND_FORWARD_PAYMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->opt_in_to_store_and_forward_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2242
    :cond_1f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    if-nez v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PAYMENT_CARDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payment_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2243
    :cond_20
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    if-nez v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PROTECT_EDIT_TAX:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->protect_edit_tax(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2244
    :cond_21
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    if-nez v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_REDUCE_PRINTING_WASTE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reduce_printing_waste_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2245
    :cond_22
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    if-nez v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->require_secure_session_for_r6_swipe(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2246
    :cond_23
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    if-nez v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_REQUIRES_TRACK_2_IF_NOT_AMEX:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->requires_track_2_if_not_amex(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2247
    :cond_24
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    if-nez v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ROUNDING_TYPE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->rounding_type(Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2248
    :cond_25
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    if-nez v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SEE_ADVANCED_MODIFIERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_advanced_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2249
    :cond_26
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    if-nez v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SEE_PAYROLL_REGISTER_FEATURE_TOUR:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2250
    :cond_27
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    if-nez v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SEE_PAYROLL_REGISTER_LEARN_MORE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->see_payroll_register_learn_more_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2251
    :cond_28
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    if-nez v0, :cond_29

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SKIP_MODIFIER_DETAIL_SCREEN_IN_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_modifier_detail_screen_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2252
    :cond_29
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    if-nez v0, :cond_2a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SKIP_SIGNATURES_FOR_SMALL_PAYMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->skip_signatures_for_small_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2253
    :cond_2a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    if-nez v0, :cond_2b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_FEATURE_TOUR:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_feature_tour(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2254
    :cond_2b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    if-nez v0, :cond_2c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_INCLUSIVE_TAXES_IN_CART:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_inclusive_taxes_in_cart(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2255
    :cond_2c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    if-nez v0, :cond_2d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_O1_JP_DEPRECATION_WARNING_IN_APP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_o1_jp_deprecation_warning_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2256
    :cond_2d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    if-nez v0, :cond_2e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SHOW_ITEMS_LIBRARY_AFTER_LOGIN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->show_items_library_after_login(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2257
    :cond_2e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    if-nez v0, :cond_2f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2258
    :cond_2f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    if-nez v0, :cond_30

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_PLACEHOLDER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->placeholder(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2259
    :cond_30
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    if-nez v0, :cond_31

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SUPPORT_RESTAURANT_ITEMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_restaurant_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2260
    :cond_31
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    if-nez v0, :cond_32

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SUPPORT_RETAIL_ITEMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support_retail_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2261
    :cond_32
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-nez v0, :cond_33

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_SWIPE_TO_CREATE_OPEN_TICKET_WITH_NAME:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->swipe_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2262
    :cond_33
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_34

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_TRANSFER_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->transfer_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2263
    :cond_34
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    if-nez v0, :cond_35

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_UPLOAD_SUPPORT_LEDGER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->upload_support_ledger(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2264
    :cond_35
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    if-nez v0, :cond_36

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_A10:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_a10(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2265
    :cond_36
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    if-nez v0, :cond_37

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ANDROID_PRINTER_STATIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_printer_stations(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2266
    :cond_37
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    if-nez v0, :cond_38

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ANDROID_BARCODE_SCANNING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_barcode_scanning(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2267
    :cond_38
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    if-nez v0, :cond_39

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ANDROID_EMPLOYEE_MANAGEMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_android_employee_management(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2268
    :cond_39
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    if-nez v0, :cond_3a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_AUSTRALIAN_GROSS_SALES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_australian_gross_sales(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2269
    :cond_3a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    if-nez v0, :cond_3b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CARD_ASSOCIATED_COUPONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_associated_coupons(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2270
    :cond_3b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    if-nez v0, :cond_3c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CARD_ON_FILE_IN_ANDROID_REGISTER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2271
    :cond_3c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_3d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CARD_ON_FILE_IN_ANDROID_REGISTER_ON_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_card_on_file_in_android_register_on_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2272
    :cond_3d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    if-nez v0, :cond_3e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CONDITIONAL_TAXES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_conditional_taxes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2273
    :cond_3e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    if-nez v0, :cond_3f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CRM_CUSTOM_ATTRIBUTES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_custom_attributes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2274
    :cond_3f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    if-nez v0, :cond_40

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CRM_MESSAGING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_messaging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2275
    :cond_40
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    if-nez v0, :cond_41

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CRM_NOTES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_crm_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2276
    :cond_41
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    if-nez v0, :cond_42

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_DIRECTORY_WITH_INVOICES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_directory_with_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2277
    :cond_42
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    if-nez v0, :cond_43

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_ACTIVITY_LIST_IN_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_activity_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2278
    :cond_43
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    if-nez v0, :cond_44

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_APPLET_IN_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2279
    :cond_44
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_45

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_APPLET_IN_ANDROID_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_applet_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2280
    :cond_45
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    if-nez v0, :cond_46

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_IN_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2281
    :cond_46
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_47

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_CUSTOMER_LIST_IN_ANDROID_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_customer_list_in_android_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2282
    :cond_47
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    if-nez v0, :cond_48

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_DINING_OPTIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_dining_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2283
    :cond_48
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    if-nez v0, :cond_49

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_EMPLOYEE_CANCEL_TRANSACTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee_cancel_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2284
    :cond_49
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_4a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_EMPLOYEE_FILTERING_FOR_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2285
    :cond_4a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    if-nez v0, :cond_4b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_EMPLOYEE_FILTERING_FOR_PAPER_SIGNATURE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_filtering_for_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2286
    :cond_4b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    if-nez v0, :cond_4c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_EMPLOYEE_TIMECARDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_employee_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2287
    :cond_4c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    if-nez v0, :cond_4d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ENFORCE_LOCATION_FIX:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_enforce_location_fix(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2288
    :cond_4d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_4e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_GIFT_CARDS_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_gift_cards_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2289
    :cond_4e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    if-nez v0, :cond_4f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_IN_APP_TIMECARDS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_in_app_timecards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2290
    :cond_4f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    if-nez v0, :cond_50

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_INVENTORY_PLUS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_inventory_plus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2291
    :cond_50
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    if-nez v0, :cond_51

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_INVOICE_APPLET_IN_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_invoice_applet_in_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2292
    :cond_51
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    if-nez v0, :cond_52

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MAGSTRIPE_ONLY_READERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_magstripe_only_readers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2293
    :cond_52
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    if-nez v0, :cond_53

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MOBILE_INVOICES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2294
    :cond_53
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    if-nez v0, :cond_54

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MOBILE_INVOICES_WITH_DISCOUNTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2295
    :cond_54
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    if-nez v0, :cond_55

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_MOBILE_INVOICES_WITH_MODIFIERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_mobile_invoices_with_modifiers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2296
    :cond_55
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_56

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_OPEN_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2297
    :cond_56
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    if-nez v0, :cond_57

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_OPEN_TICKETS_MOBILE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_open_tickets_mobile_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2298
    :cond_57
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    if-nez v0, :cond_58

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_PAPER_SIGNATURE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_paper_signature_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2299
    :cond_58
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-nez v0, :cond_59

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_PRE_TAX_TIPPING_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2300
    :cond_59
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_5a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_PREDEFINED_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_predefined_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2301
    :cond_5a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    if-nez v0, :cond_5b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_REPORTS_CHARTS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_reports_charts_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2302
    :cond_5b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    if-nez v0, :cond_5c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_R12:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2303
    :cond_5c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_5d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_R12_PAIRING_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r12_pairing_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2304
    :cond_5d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    if-nez v0, :cond_5e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_R6:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_r6(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2305
    :cond_5e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    if-nez v0, :cond_5f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_SAFETYNET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_safetynet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2306
    :cond_5f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    if-nez v0, :cond_60

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_SPLIT_TICKETS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tickets_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2307
    :cond_60
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    if-nez v0, :cond_61

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_SPLIT_TENDER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_split_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2308
    :cond_61
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    if-nez v0, :cond_62

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_V3_CATALOG:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_v3_catalog(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2309
    :cond_62
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    if-nez v0, :cond_63

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_VOID_COMP_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_void_comp_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2310
    :cond_63
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    if-nez v0, :cond_64

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USE_ZERO_AMOUNT_TENDER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->use_zero_amount_tender(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2311
    :cond_64
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    if-nez v0, :cond_65

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USES_DEVICE_PROFILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->uses_device_profile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2312
    :cond_65
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    if-nez v0, :cond_66

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USING_TIME_TRACKING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2313
    :cond_66
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    if-nez v0, :cond_67

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ZERO_AMOUNT_CONTACTLESS_ARQC_EXPERIMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->zero_amount_contactless_arqc_experiment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2314
    :cond_67
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    if-nez v0, :cond_68

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ORDER_HUB_SYNC_PERIOD_WITH_NOTIFICATIONS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_with_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2315
    :cond_68
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    if-nez v0, :cond_69

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_ORDER_HUB_SYNC_PERIOD_WITHOUT_NOTIFICATIONS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->order_hub_sync_period_without_notifications(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2316
    :cond_69
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    if-nez v0, :cond_6a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_HAS_BAZAAR_ONLINE_STORE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->has_bazaar_online_store(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2317
    :cond_6a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    if-nez v0, :cond_6b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_DISPLAY_LEARN_MORE_R4:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->display_learn_more_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2318
    :cond_6b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    if-eqz v0, :cond_6c

    .line 2319
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object v0

    .line 2320
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    if-eq v0, v2, :cond_6c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->catalog(Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2322
    :cond_6c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    if-eqz v0, :cond_6d

    .line 2323
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object v0

    .line 2324
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    if-eq v0, v2, :cond_6d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->loyalty(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2326
    :cond_6d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    if-eqz v0, :cond_6e

    .line 2327
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object v0

    .line 2328
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    if-eq v0, v2, :cond_6e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->onboard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2330
    :cond_6e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    if-eqz v0, :cond_6f

    .line 2331
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    move-result-object v0

    .line 2332
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    if-eq v0, v2, :cond_6f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->crm(Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2334
    :cond_6f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    if-eqz v0, :cond_70

    .line 2335
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    .line 2336
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    if-eq v0, v2, :cond_70

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->regex(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2338
    :cond_70
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    if-eqz v0, :cond_71

    .line 2339
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object v0

    .line 2340
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    if-eq v0, v2, :cond_71

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->reports(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2342
    :cond_71
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    if-eqz v0, :cond_72

    .line 2343
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object v0

    .line 2344
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    if-eq v0, v2, :cond_72

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->invoices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2346
    :cond_72
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    if-eqz v0, :cond_73

    .line 2347
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object v0

    .line 2348
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    if-eq v0, v2, :cond_73

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->deposits(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2350
    :cond_73
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    if-eqz v0, :cond_74

    .line 2351
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object v0

    .line 2352
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    if-eq v0, v2, :cond_74

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->giftcard(Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2354
    :cond_74
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    if-eqz v0, :cond_75

    .line 2355
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object v0

    .line 2356
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    if-eq v0, v2, :cond_75

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->paymentflow(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2358
    :cond_75
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    if-eqz v0, :cond_76

    .line 2359
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    move-result-object v0

    .line 2360
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    if-eq v0, v2, :cond_76

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->pos(Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2362
    :cond_76
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    if-eqz v0, :cond_77

    .line 2363
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    move-result-object v0

    .line 2364
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    if-eq v0, v2, :cond_77

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->posfeatures(Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2366
    :cond_77
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    if-eqz v0, :cond_78

    .line 2367
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    move-result-object v0

    .line 2368
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    if-eq v0, v2, :cond_78

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employee(Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2370
    :cond_78
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    if-eqz v0, :cond_79

    .line 2371
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object v0

    .line 2372
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    if-eq v0, v2, :cond_79

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->terminal(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2374
    :cond_79
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    if-eqz v0, :cond_7a

    .line 2375
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object v0

    .line 2376
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    if-eq v0, v2, :cond_7a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->timecards(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2378
    :cond_7a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    if-eqz v0, :cond_7b

    .line 2379
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    move-result-object v0

    .line 2380
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    if-eq v0, v2, :cond_7b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->devplatmobile(Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2382
    :cond_7b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    if-eqz v0, :cond_7c

    .line 2383
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    move-result-object v0

    .line 2384
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    if-eq v0, v2, :cond_7c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bfd(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2386
    :cond_7c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    if-eqz v0, :cond_7d

    .line 2387
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    move-result-object v0

    .line 2388
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    if-eq v0, v2, :cond_7d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->bizbank(Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2390
    :cond_7d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    if-eqz v0, :cond_7e

    .line 2391
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    move-result-object v0

    .line 2392
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    if-eq v0, v2, :cond_7e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->fees(Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2394
    :cond_7e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    if-eqz v0, :cond_7f

    .line 2395
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object v0

    .line 2396
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    if-eq v0, v2, :cond_7f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->readerfw(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2398
    :cond_7f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    if-eqz v0, :cond_80

    .line 2399
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object v0

    .line 2400
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    if-eq v0, v2, :cond_80

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->support(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2402
    :cond_80
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    if-eqz v0, :cond_81

    .line 2403
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    move-result-object v0

    .line 2404
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    if-eq v0, v2, :cond_81

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->billing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2406
    :cond_81
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    if-eqz v0, :cond_82

    .line 2407
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object v0

    .line 2408
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    if-eq v0, v2, :cond_82

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->items(Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2410
    :cond_82
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    if-eqz v0, :cond_83

    .line 2411
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object v0

    .line 2412
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    if-eq v0, v2, :cond_83

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->squaredevice(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2414
    :cond_83
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    if-eqz v0, :cond_84

    .line 2415
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    move-result-object v0

    .line 2416
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    if-eq v0, v2, :cond_84

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->employeejobs(Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2418
    :cond_84
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    if-eqz v0, :cond_85

    .line 2419
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    move-result-object v0

    .line 2420
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    if-eq v0, v2, :cond_85

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->restaurants(Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2422
    :cond_85
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    if-eqz v0, :cond_86

    .line 2423
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    move-result-object v0

    .line 2424
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    if-eq v0, v2, :cond_86

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->appointments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2426
    :cond_86
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    if-eqz v0, :cond_87

    .line 2427
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    move-result-object v0

    .line 2428
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    if-eq v0, v2, :cond_87

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->discount(Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2430
    :cond_87
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    if-eqz v0, :cond_88

    .line 2431
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    move-result-object v0

    .line 2432
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    if-eq v0, v2, :cond_88

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->spoc(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2434
    :cond_88
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    if-eqz v0, :cond_89

    .line 2435
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object v0

    .line 2436
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    if-eq v0, v2, :cond_89

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->retailer(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2438
    :cond_89
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    if-eqz v0, :cond_8a

    .line 2439
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object v0

    .line 2440
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    if-eq v0, v2, :cond_8a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->prices(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2442
    :cond_8a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v0, :cond_8b

    .line 2443
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object v0

    .line 2444
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eq v0, v2, :cond_8b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->orderhub(Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2446
    :cond_8b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    if-eqz v0, :cond_8c

    .line 2447
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object v0

    .line 2448
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    if-eq v0, v2, :cond_8c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->payments(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2450
    :cond_8c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    if-eqz v0, :cond_8d

    .line 2451
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object v0

    .line 2452
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    if-eq v0, v2, :cond_8d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->ecom(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2454
    :cond_8d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    if-eqz v0, :cond_8e

    .line 2455
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object v0

    .line 2456
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    if-eq v0, v2, :cond_8e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->printers(Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2458
    :cond_8e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    if-eqz v0, :cond_8f

    .line 2459
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    move-result-object v0

    .line 2460
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    if-eq v0, v2, :cond_8f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->capital(Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2462
    :cond_8f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    if-eqz v0, :cond_90

    .line 2463
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    move-result-object v0

    .line 2464
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    if-eq v0, v2, :cond_90

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->marketing(Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    .line 2466
    :cond_90
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    if-eqz v0, :cond_91

    .line 2467
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    move-result-object v0

    .line 2468
    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    if-eq v0, v2, :cond_91

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->auth(Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v1

    :cond_91
    if-nez v1, :cond_92

    move-object v0, p0

    goto :goto_0

    .line 2470
    :cond_92
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2045
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2046
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", accept_third_party_gift_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->accept_third_party_gift_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2047
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", activation_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2048
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", allow_payment_for_invoice_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->allow_payment_for_invoice_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2049
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", auto_capture_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auto_capture_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2050
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", can_allow_swipe_for_chip_cards_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_allow_swipe_for_chip_cards_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2051
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", can_bulk_delete_open_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_bulk_delete_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2052
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", can_enable_tipping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_enable_tipping:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2053
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", can_onboard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2054
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", print_logo_on_paper_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->print_logo_on_paper_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2055
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", can_publish_merchant_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_publish_merchant_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2056
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", can_refer_friend_in_app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_refer_friend_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2057
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", can_skip_receipt_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_skip_receipt_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2058
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", can_transfer_open_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_transfer_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2059
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", can_use_android_ecr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_use_android_ecr:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2060
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", cardholder_name_on_dip_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardholder_name_on_dip_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2061
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    if-eqz v1, :cond_f

    const-string v1, ", cardreaders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->cardreaders:Lcom/squareup/server/account/protos/Cardreaders;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2062
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", collect_tip_before_authorization_preferred="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_preferred:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2063
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", collect_tip_before_authorization_required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->collect_tip_before_authorization_required:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2064
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_12

    const-string v1, ", contact_support_phone_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2065
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    if-eqz v1, :cond_13

    const-string v1, ", contact_support_twitter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->contact_support_twitter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2066
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", country_prefers_contactless_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_contactless_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2067
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    const-string v1, ", country_prefers_no_contactless_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_no_contactless_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2068
    :cond_15
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    const-string v1, ", country_prefers_emv="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->country_prefers_emv:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2069
    :cond_16
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    const-string v1, ", disable_item_editing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_item_editing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2070
    :cond_17
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    const-string v1, ", disable_mobile_reports_applet_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_mobile_reports_applet_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2071
    :cond_18
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    const-string v1, ", eligible_for_square_card_processing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2072
    :cond_19
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", eligible_for_third_party_card_processing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_third_party_card_processing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2073
    :cond_1a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", email_transaction_ledger="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->email_transaction_ledger:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2074
    :cond_1b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    const-string v1, ", disable_register_api="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->disable_register_api:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2075
    :cond_1c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    if-eqz v1, :cond_1d

    const-string v1, ", help_center_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->help_center_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2076
    :cond_1d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    const-string v1, ", hide_modifiers_on_receipts_in_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->hide_modifiers_on_receipts_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2077
    :cond_1e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    const-string v1, ", invoice_tipping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoice_tipping:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2078
    :cond_1f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    if-eqz v1, :cond_20

    const-string v1, ", items_batch_size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items_batch_size:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2079
    :cond_20
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    if-eqz v1, :cond_21

    const-string v1, ", login_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->login_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2080
    :cond_21
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    const-string v1, ", merge_open_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->merge_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2081
    :cond_22
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    const-string v1, ", opt_in_to_open_tickets_with_beta_ui_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_open_tickets_with_beta_ui_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2082
    :cond_23
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    const-string v1, ", opt_in_to_store_and_forward_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->opt_in_to_store_and_forward_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2083
    :cond_24
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    const-string v1, ", payment_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2084
    :cond_25
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    const-string v1, ", protect_edit_tax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->protect_edit_tax:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2085
    :cond_26
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    if-eqz v1, :cond_27

    const-string v1, ", privacy_policy_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->privacy_policy_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2086
    :cond_27
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    const-string v1, ", reduce_printing_waste_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reduce_printing_waste_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2087
    :cond_28
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    const-string v1, ", require_secure_session_for_r6_swipe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->require_secure_session_for_r6_swipe:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2088
    :cond_29
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    if-eqz v1, :cond_2a

    const-string v1, ", requires_track_2_if_not_amex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->requires_track_2_if_not_amex:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2089
    :cond_2a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    if-eqz v1, :cond_2b

    const-string v1, ", rounding_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->rounding_type:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2090
    :cond_2b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    if-eqz v1, :cond_2c

    const-string v1, ", see_advanced_modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_advanced_modifiers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2091
    :cond_2c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    if-eqz v1, :cond_2d

    const-string v1, ", see_payroll_register_feature_tour="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_feature_tour:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2092
    :cond_2d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2e

    const-string v1, ", see_payroll_register_learn_more_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->see_payroll_register_learn_more_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2093
    :cond_2e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    if-eqz v1, :cond_2f

    const-string v1, ", seller_agreement_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->seller_agreement_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2094
    :cond_2f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_30

    const-string v1, ", skip_modifier_detail_screen_in_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_modifier_detail_screen_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2095
    :cond_30
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_31

    const-string v1, ", skip_signatures_for_small_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->skip_signatures_for_small_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2096
    :cond_31
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    if-eqz v1, :cond_32

    const-string v1, ", show_feature_tour="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_feature_tour:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2097
    :cond_32
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    if-eqz v1, :cond_33

    const-string v1, ", show_inclusive_taxes_in_cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_inclusive_taxes_in_cart:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2098
    :cond_33
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_34

    const-string v1, ", show_o1_jp_deprecation_warning_in_app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_o1_jp_deprecation_warning_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2099
    :cond_34
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    if-eqz v1, :cond_35

    const-string v1, ", show_items_library_after_login="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->show_items_library_after_login:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2100
    :cond_35
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_36

    const-string v1, ", sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2101
    :cond_36
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    if-eqz v1, :cond_37

    const-string v1, ", placeholder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->placeholder:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2102
    :cond_37
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_38

    const-string v1, ", support_restaurant_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_restaurant_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2103
    :cond_38
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_39

    const-string v1, ", support_retail_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support_retail_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2104
    :cond_39
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3a

    const-string v1, ", supported_card_brands_offline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_offline:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2105
    :cond_3a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3b

    const-string v1, ", supported_card_brands_online="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->supported_card_brands_online:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2106
    :cond_3b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-eqz v1, :cond_3c

    const-string v1, ", swipe_to_create_open_ticket_with_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->swipe_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2107
    :cond_3c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3d

    const-string v1, ", transfer_open_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->transfer_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2108
    :cond_3d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    if-eqz v1, :cond_3e

    const-string v1, ", upload_support_ledger="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->upload_support_ledger:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2109
    :cond_3e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    if-eqz v1, :cond_3f

    const-string v1, ", use_a10="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_a10:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2110
    :cond_3f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    if-eqz v1, :cond_40

    const-string v1, ", use_android_printer_stations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_printer_stations:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2111
    :cond_40
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    if-eqz v1, :cond_41

    const-string v1, ", use_android_barcode_scanning="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_barcode_scanning:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2112
    :cond_41
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    if-eqz v1, :cond_42

    const-string v1, ", use_android_employee_management="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_android_employee_management:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2113
    :cond_42
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    if-eqz v1, :cond_43

    const-string v1, ", use_australian_gross_sales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_australian_gross_sales:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2114
    :cond_43
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    if-eqz v1, :cond_44

    const-string v1, ", use_card_associated_coupons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_associated_coupons:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2115
    :cond_44
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    if-eqz v1, :cond_45

    const-string v1, ", use_card_on_file_in_android_register="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2116
    :cond_45
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_46

    const-string v1, ", use_card_on_file_in_android_register_on_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_card_on_file_in_android_register_on_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2117
    :cond_46
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    if-eqz v1, :cond_47

    const-string v1, ", use_conditional_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_conditional_taxes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2118
    :cond_47
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    if-eqz v1, :cond_48

    const-string v1, ", use_crm_custom_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_custom_attributes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2119
    :cond_48
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    if-eqz v1, :cond_49

    const-string v1, ", use_crm_messaging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_messaging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2120
    :cond_49
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_4a

    const-string v1, ", use_crm_notes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_crm_notes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2121
    :cond_4a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    if-eqz v1, :cond_4b

    const-string v1, ", use_customer_directory_with_invoices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_directory_with_invoices:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2122
    :cond_4b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4c

    const-string v1, ", use_customer_list_activity_list_in_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_activity_list_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2123
    :cond_4c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4d

    const-string v1, ", use_customer_list_applet_in_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2124
    :cond_4d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_4e

    const-string v1, ", use_customer_list_applet_in_android_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_applet_in_android_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2125
    :cond_4e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4f

    const-string v1, ", use_customer_list_in_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2126
    :cond_4f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_50

    const-string v1, ", use_customer_list_in_android_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_customer_list_in_android_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2127
    :cond_50
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_51

    const-string v1, ", use_dining_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_dining_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2128
    :cond_51
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_52

    const-string v1, ", employee_cancel_transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee_cancel_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2129
    :cond_52
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_53

    const-string v1, ", use_employee_filtering_for_open_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2130
    :cond_53
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_54

    const-string v1, ", use_employee_filtering_for_paper_signature_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_filtering_for_paper_signature_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2131
    :cond_54
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    if-eqz v1, :cond_55

    const-string v1, ", use_employee_timecards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_employee_timecards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2132
    :cond_55
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    if-eqz v1, :cond_56

    const-string v1, ", use_enforce_location_fix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_enforce_location_fix:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2133
    :cond_56
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_57

    const-string v1, ", use_gift_cards_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_gift_cards_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2134
    :cond_57
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    if-eqz v1, :cond_58

    const-string v1, ", use_in_app_timecards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_in_app_timecards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2135
    :cond_58
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    if-eqz v1, :cond_59

    const-string v1, ", use_inventory_plus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_inventory_plus:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2136
    :cond_59
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5a

    const-string v1, ", use_invoice_applet_in_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_invoice_applet_in_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2137
    :cond_5a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    if-eqz v1, :cond_5b

    const-string v1, ", use_magstripe_only_readers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_magstripe_only_readers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2138
    :cond_5b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    if-eqz v1, :cond_5c

    const-string v1, ", use_mobile_invoices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2139
    :cond_5c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_5d

    const-string v1, ", use_mobile_invoices_with_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_discounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2140
    :cond_5d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    if-eqz v1, :cond_5e

    const-string v1, ", use_mobile_invoices_with_modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_mobile_invoices_with_modifiers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2141
    :cond_5e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5f

    const-string v1, ", use_open_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2142
    :cond_5f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_60

    const-string v1, ", use_open_tickets_mobile_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_open_tickets_mobile_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2143
    :cond_60
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_61

    const-string v1, ", use_paper_signature_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_paper_signature_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2144
    :cond_61
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_62

    const-string v1, ", use_pre_tax_tipping_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2145
    :cond_62
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_63

    const-string v1, ", use_predefined_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_predefined_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2146
    :cond_63
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_64

    const-string v1, ", use_reports_charts_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_reports_charts_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2147
    :cond_64
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    if-eqz v1, :cond_65

    const-string v1, ", use_r12="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2148
    :cond_65
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_66

    const-string v1, ", use_r12_pairing_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r12_pairing_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2149
    :cond_66
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    if-eqz v1, :cond_67

    const-string v1, ", use_r6="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_r6:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2150
    :cond_67
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    if-eqz v1, :cond_68

    const-string v1, ", use_safetynet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_safetynet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2151
    :cond_68
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_69

    const-string v1, ", use_split_tickets_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tickets_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2152
    :cond_69
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    if-eqz v1, :cond_6a

    const-string v1, ", use_split_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_split_tender:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2153
    :cond_6a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    if-eqz v1, :cond_6b

    const-string v1, ", use_v3_catalog="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_v3_catalog:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2154
    :cond_6b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6c

    const-string v1, ", use_void_comp_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_void_comp_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2155
    :cond_6c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    if-eqz v1, :cond_6d

    const-string v1, ", use_zero_amount_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->use_zero_amount_tender:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2156
    :cond_6d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_6e

    const-string v1, ", uses_device_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2157
    :cond_6e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    if-eqz v1, :cond_6f

    const-string v1, ", using_time_tracking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->using_time_tracking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2158
    :cond_6f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    if-eqz v1, :cond_70

    const-string v1, ", zero_amount_contactless_arqc_experiment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->zero_amount_contactless_arqc_experiment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2159
    :cond_70
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    if-eqz v1, :cond_71

    const-string v1, ", order_hub_sync_period_with_notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2160
    :cond_71
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    if-eqz v1, :cond_72

    const-string v1, ", order_hub_sync_period_without_notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2161
    :cond_72
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    if-eqz v1, :cond_73

    const-string v1, ", has_bazaar_online_store="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->has_bazaar_online_store:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2162
    :cond_73
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    if-eqz v1, :cond_74

    const-string v1, ", display_learn_more_r4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->display_learn_more_r4:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2163
    :cond_74
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    if-eqz v1, :cond_75

    const-string v1, ", catalog="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->catalog:Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2164
    :cond_75
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    if-eqz v1, :cond_76

    const-string v1, ", loyalty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->loyalty:Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2165
    :cond_76
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    if-eqz v1, :cond_77

    const-string v1, ", onboard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->onboard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2166
    :cond_77
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    if-eqz v1, :cond_78

    const-string v1, ", crm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->crm:Lcom/squareup/server/account/protos/FlagsAndPermissions$Crm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2167
    :cond_78
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    if-eqz v1, :cond_79

    const-string v1, ", regex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2168
    :cond_79
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    if-eqz v1, :cond_7a

    const-string v1, ", reports="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->reports:Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2169
    :cond_7a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    if-eqz v1, :cond_7b

    const-string v1, ", invoices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->invoices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2170
    :cond_7b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    if-eqz v1, :cond_7c

    const-string v1, ", deposits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->deposits:Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2171
    :cond_7c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    if-eqz v1, :cond_7d

    const-string v1, ", giftcard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->giftcard:Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2172
    :cond_7d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    if-eqz v1, :cond_7e

    const-string v1, ", paymentflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->paymentflow:Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2173
    :cond_7e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    if-eqz v1, :cond_7f

    const-string v1, ", pos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->pos:Lcom/squareup/server/account/protos/FlagsAndPermissions$Pos;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2174
    :cond_7f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    if-eqz v1, :cond_80

    const-string v1, ", posfeatures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->posfeatures:Lcom/squareup/server/account/protos/FlagsAndPermissions$Posfeatures;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2175
    :cond_80
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    if-eqz v1, :cond_81

    const-string v1, ", employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employee:Lcom/squareup/server/account/protos/FlagsAndPermissions$Employee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2176
    :cond_81
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    if-eqz v1, :cond_82

    const-string v1, ", terminal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->terminal:Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2177
    :cond_82
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    if-eqz v1, :cond_83

    const-string v1, ", timecards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->timecards:Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2178
    :cond_83
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    if-eqz v1, :cond_84

    const-string v1, ", devplatmobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->devplatmobile:Lcom/squareup/server/account/protos/FlagsAndPermissions$Devplatmobile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2179
    :cond_84
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    if-eqz v1, :cond_85

    const-string v1, ", bfd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bfd:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bfd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2180
    :cond_85
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    if-eqz v1, :cond_86

    const-string v1, ", bizbank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->bizbank:Lcom/squareup/server/account/protos/FlagsAndPermissions$Bizbank;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2181
    :cond_86
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    if-eqz v1, :cond_87

    const-string v1, ", fees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->fees:Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2182
    :cond_87
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    if-eqz v1, :cond_88

    const-string v1, ", readerfw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->readerfw:Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2183
    :cond_88
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    if-eqz v1, :cond_89

    const-string v1, ", support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->support:Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2184
    :cond_89
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    if-eqz v1, :cond_8a

    const-string v1, ", billing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->billing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2185
    :cond_8a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    if-eqz v1, :cond_8b

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->items:Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2186
    :cond_8b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    if-eqz v1, :cond_8c

    const-string v1, ", squaredevice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->squaredevice:Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2187
    :cond_8c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    if-eqz v1, :cond_8d

    const-string v1, ", employeejobs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->employeejobs:Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2188
    :cond_8d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    if-eqz v1, :cond_8e

    const-string v1, ", restaurants="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->restaurants:Lcom/squareup/server/account/protos/FlagsAndPermissions$Restaurants;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2189
    :cond_8e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    if-eqz v1, :cond_8f

    const-string v1, ", appointments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->appointments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Appointments;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2190
    :cond_8f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    if-eqz v1, :cond_90

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->discount:Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2191
    :cond_90
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    if-eqz v1, :cond_91

    const-string v1, ", spoc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->spoc:Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2192
    :cond_91
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    if-eqz v1, :cond_92

    const-string v1, ", retailer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->retailer:Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2193
    :cond_92
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    if-eqz v1, :cond_93

    const-string v1, ", prices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->prices:Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2194
    :cond_93
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v1, :cond_94

    const-string v1, ", orderhub="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2195
    :cond_94
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    if-eqz v1, :cond_95

    const-string v1, ", payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payments:Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2196
    :cond_95
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    if-eqz v1, :cond_96

    const-string v1, ", ecom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->ecom:Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2197
    :cond_96
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    if-eqz v1, :cond_97

    const-string v1, ", printers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->printers:Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2198
    :cond_97
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    if-eqz v1, :cond_98

    const-string v1, ", capital="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->capital:Lcom/squareup/server/account/protos/FlagsAndPermissions$Capital;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2199
    :cond_98
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    if-eqz v1, :cond_99

    const-string v1, ", marketing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->marketing:Lcom/squareup/server/account/protos/FlagsAndPermissions$Marketing;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2200
    :cond_99
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    if-eqz v1, :cond_9a

    const-string v1, ", auth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->auth:Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FlagsAndPermissions{"

    .line 2201
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
