.class final Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FeeTypesProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FeeTypesProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FeeTypesProto"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/FeeTypesProto;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 130
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FeeTypesProto;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 147
    new-instance v0, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;-><init>()V

    .line 148
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 149
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 153
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 151
    :cond_0
    iget-object v3, v0, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 158
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->build()Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 128
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FeeTypesProto;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 141
    sget-object v0, Lcom/squareup/server/account/protos/FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 142
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/FeeTypesProto;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 128
    check-cast p2, Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FeeTypesProto;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/FeeTypesProto;)I
    .locals 3

    .line 135
    sget-object v0, Lcom/squareup/server/account/protos/FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 136
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FeeTypesProto;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 128
    check-cast p1, Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;->encodedSize(Lcom/squareup/server/account/protos/FeeTypesProto;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/FeeTypesProto;
    .locals 2

    .line 163
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FeeTypesProto;->newBuilder()Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    move-result-object p1

    .line 164
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 165
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 166
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->build()Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 128
    check-cast p1, Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;->redact(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object p1

    return-object p1
.end method
