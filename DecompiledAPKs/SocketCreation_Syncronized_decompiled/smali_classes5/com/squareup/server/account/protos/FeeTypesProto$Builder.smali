.class public final Lcom/squareup/server/account/protos/FeeTypesProto$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FeeTypesProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FeeTypesProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FeeTypesProto;",
        "Lcom/squareup/server/account/protos/FeeTypesProto$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 113
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FeeTypesProto;
    .locals 3

    .line 124
    new-instance v0, Lcom/squareup/server/account/protos/FeeTypesProto;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FeeTypesProto;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->build()Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object v0

    return-object v0
.end method

.method public types(Ljava/util/List;)Lcom/squareup/server/account/protos/FeeTypesProto$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;)",
            "Lcom/squareup/server/account/protos/FeeTypesProto$Builder;"
        }
    .end annotation

    .line 117
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 118
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types:Ljava/util/List;

    return-object p0
.end method
