.class public final Lcom/squareup/server/account/protos/User$MerchantProfile;
.super Lcom/squareup/wire/AndroidMessage;
.source "User.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MerchantProfile"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;,
        Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/User$MerchantProfile;",
        "Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/User$MerchantProfile;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/User$MerchantProfile;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/User$MerchantProfile;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/User$MerchantProfile;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURATED_IMAGE_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOM_RECEIPT_TEXT:Ljava/lang/String; = ""

.field public static final DEFAULT_PRINTED_RECEIPT_IMAGE_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final curated_image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final custom_receipt_text:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final printed_receipt_image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1042
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/User$MerchantProfile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1044
    sget-object v0, Lcom/squareup/server/account/protos/User$MerchantProfile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User$MerchantProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1077
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/User$MerchantProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 1083
    sget-object v0, Lcom/squareup/server/account/protos/User$MerchantProfile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1084
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    .line 1085
    iput-object p2, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    .line 1086
    iput-object p3, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 1148
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->newBuilder()Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1102
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1103
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantProfile;

    .line 1104
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    .line 1105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    .line 1106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    .line 1107
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1112
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 1114
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1115
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1116
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1117
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 1118
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;
    .locals 2

    .line 1091
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;-><init>()V

    .line 1092
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->curated_image_url:Ljava/lang/String;

    .line 1093
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->printed_receipt_image_url:Ljava/lang/String;

    .line 1094
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->custom_receipt_text:Ljava/lang/String;

    .line 1095
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1041
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->newBuilder()Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/User$MerchantProfile;)Lcom/squareup/server/account/protos/User$MerchantProfile;
    .locals 2

    .line 1141
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->curated_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object v1

    .line 1142
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->printed_receipt_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object v1

    .line 1143
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->custom_receipt_text(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 1144
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 1041
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantProfile;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->overlay(Lcom/squareup/server/account/protos/User$MerchantProfile;)Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/User$MerchantProfile;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 1041
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantProfile;->populateDefaults()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1126
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", curated_image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1127
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", printed_receipt_image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1128
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", custom_receipt_text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MerchantProfile{"

    .line 1129
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
