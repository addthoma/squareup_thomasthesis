.class public final Lcom/squareup/server/account/protos/Tipping$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tipping.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Tipping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Tipping;",
        "Lcom/squareup/server/account/protos/Tipping$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_manual_tip_entry:Ljava/lang/Boolean;

.field public manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

.field public manual_tip_entry_max_percentage:Ljava/lang/Double;

.field public manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

.field public smart_tipping_over_threshold_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

.field public smart_tipping_under_threshold_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 217
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 218
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    .line 219
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->allow_manual_tip_entry:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/Tipping;
    .locals 10

    .line 264
    new-instance v9, Lcom/squareup/server/account/protos/Tipping;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->allow_manual_tip_entry:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/Tipping;-><init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 202
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tipping$Builder;->build()Lcom/squareup/server/account/protos/Tipping;

    move-result-object v0

    return-object v0
.end method

.method public manual_tip_entry_largest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public manual_tip_entry_max_percentage(Ljava/lang/Double;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    return-object p0
.end method

.method public manual_tip_entry_smallest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public smart_tipping_over_threshold_options(Ljava/util/List;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)",
            "Lcom/squareup/server/account/protos/Tipping$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 237
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 238
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    return-object p0
.end method

.method public smart_tipping_threshold_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public smart_tipping_under_threshold_options(Ljava/util/List;)Lcom/squareup/server/account/protos/Tipping$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)",
            "Lcom/squareup/server/account/protos/Tipping$Builder;"
        }
    .end annotation

    .line 229
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 230
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tipping$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    return-object p0
.end method
