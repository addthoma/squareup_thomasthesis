.class final Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Terminal"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 12646
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12703
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;-><init>()V

    .line 12704
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 12705
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 12729
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 12727
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_terminal_api_spm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12726
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cp_pricing_change(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12725
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_banner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12724
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_notifications(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12723
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12722
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cnp_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12721
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cof_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12720
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_connected_terminal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12719
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes_challenge_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto :goto_0

    .line 12718
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12717
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_first_payment_tutorial_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12716
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_x2_dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12715
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r41(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12714
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12713
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12712
    :pswitch_f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_order_reader_menu(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12711
    :pswitch_10
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_dip_cof_post_payment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12710
    :pswitch_11
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12709
    :pswitch_12
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->can_show_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12708
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_fee_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12707
    :pswitch_14
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cnp_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    goto/16 :goto_0

    .line 12733
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 12734
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12644
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12677
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12678
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12679
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12680
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12681
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12682
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12683
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12684
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12685
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12686
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12687
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12688
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12689
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12690
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12691
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12692
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12693
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12694
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12695
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12696
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12697
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12698
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12644
    check-cast p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)I
    .locals 4

    .line 12651
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 12652
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 12653
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 12654
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 12655
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 12656
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 12657
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 12658
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    const/16 v3, 0x9

    .line 12659
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 12660
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    const/16 v3, 0xb

    .line 12661
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    const/16 v3, 0xc

    .line 12662
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    const/16 v3, 0xd

    .line 12663
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    const/16 v3, 0xe

    .line 12664
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    const/16 v3, 0xf

    .line 12665
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    const/16 v3, 0x10

    .line 12666
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    const/16 v3, 0x11

    .line 12667
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    const/16 v3, 0x12

    .line 12668
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    const/16 v3, 0x13

    .line 12669
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    const/16 v3, 0x14

    .line 12670
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    const/16 v3, 0x15

    .line 12671
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12672
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 12644
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;->encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
    .locals 0

    .line 12739
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object p1

    .line 12740
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 12741
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12644
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;->redact(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    return-object p1
.end method
