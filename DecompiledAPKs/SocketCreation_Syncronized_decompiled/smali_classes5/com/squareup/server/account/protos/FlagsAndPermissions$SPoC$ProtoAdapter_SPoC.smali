.class final Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$ProtoAdapter_SPoC;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SPoC"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 17665
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17682
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;-><init>()V

    .line 17683
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 17684
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 17688
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 17686
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->show_spoc_version_number(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;

    goto :goto_0

    .line 17692
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 17693
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17663
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$ProtoAdapter_SPoC;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17676
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->show_spoc_version_number:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 17677
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17663
    check-cast p2, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$ProtoAdapter_SPoC;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)I
    .locals 3

    .line 17670
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->show_spoc_version_number:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 17671
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 17663
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$ProtoAdapter_SPoC;->encodedSize(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;
    .locals 0

    .line 17698
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;

    move-result-object p1

    .line 17699
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 17700
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17663
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$ProtoAdapter_SPoC;->redact(Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    move-result-object p1

    return-object p1
.end method
