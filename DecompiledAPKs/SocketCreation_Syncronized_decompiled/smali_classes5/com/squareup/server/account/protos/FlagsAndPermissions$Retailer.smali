.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Retailer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$ProtoAdapter_Retailer;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADJUST_INVENTORY_AFTER_ORDER_CANCELLATION:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SMALL_RED_SEARCH:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final can_small_red_search:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17706
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$ProtoAdapter_Retailer;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$ProtoAdapter_Retailer;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 17708
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 17712
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->DEFAULT_ADJUST_INVENTORY_AFTER_ORDER_CANCELLATION:Ljava/lang/Boolean;

    .line 17714
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->DEFAULT_CAN_SMALL_RED_SEARCH:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 17732
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 17737
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 17738
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    .line 17739
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 17798
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 17754
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 17755
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    .line 17756
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    .line 17757
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    .line 17758
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 17763
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 17765
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 17766
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 17767
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 17768
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;
    .locals 2

    .line 17744
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;-><init>()V

    .line 17745
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    .line 17746
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->can_small_red_search:Ljava/lang/Boolean;

    .line 17747
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 17705
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;
    .locals 2

    .line 17792
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->adjust_inventory_after_order_cancellation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v1

    .line 17793
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->can_small_red_search(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 17794
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 17705
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;
    .locals 2

    .line 17784
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->DEFAULT_ADJUST_INVENTORY_AFTER_ORDER_CANCELLATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->adjust_inventory_after_order_cancellation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v1

    .line 17785
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->DEFAULT_CAN_SMALL_RED_SEARCH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->can_small_red_search(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 17786
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 17705
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 17775
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 17776
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", adjust_inventory_after_order_cancellation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17777
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", can_small_red_search="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;->can_small_red_search:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Retailer{"

    .line 17778
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
