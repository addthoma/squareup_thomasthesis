.class public final Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstantDeposits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;",
        "Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fee_amount:Lcom/squareup/protos/common/Money;

.field public fee_basis_points:Ljava/lang/Integer;

.field public minimum_fee_amount:Lcom/squareup/protos/common/Money;

.field public minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 758
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;
    .locals 7

    .line 795
    new-instance v6, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_basis_points:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 749
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    move-result-object v0

    return-object v0
.end method

.method public fee_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
    .locals 0

    .line 773
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public fee_basis_points(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
    .locals 0

    .line 765
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->fee_basis_points:Ljava/lang/Integer;

    return-object p0
.end method

.method public minimum_fee_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
    .locals 0

    .line 781
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public minimum_fee_deposit_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;
    .locals 0

    .line 789
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure$Builder;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
