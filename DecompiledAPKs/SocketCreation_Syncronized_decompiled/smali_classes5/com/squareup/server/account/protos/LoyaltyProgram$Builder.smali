.class public final Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyProgram.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyProgram;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram;",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

.field public expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

.field public points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

.field public program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

.field public program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

.field public qualifying_purchase_description:Ljava/lang/String;

.field public reward_tiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 245
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 246
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accrual_rules(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/LoyaltyProgram;
    .locals 10

    .line 303
    new-instance v9, Lcom/squareup/server/account/protos/LoyaltyProgram;

    iget-object v1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    iget-object v2, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->qualifying_purchase_description:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    iget-object v4, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    iget-object v5, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    iget-object v6, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/LoyaltyProgram;-><init>(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;Ljava/lang/String;Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;Lcom/squareup/server/account/protos/AccrualRules;Ljava/util/List;Lcom/squareup/server/account/protos/ExpirationPolicy;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 230
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object v0

    return-object v0
.end method

.method public expiration_policy(Lcom/squareup/server/account/protos/ExpirationPolicy;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    return-object p0
.end method

.method public points_terminology(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    return-object p0
.end method

.method public program_status(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    return-object p0
.end method

.method public program_type(Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    return-object p0
.end method

.method public qualifying_purchase_description(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->qualifying_purchase_description:Ljava/lang/String;

    return-object p0
.end method

.method public reward_tiers(Ljava/util/List;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;)",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;"
        }
    .end annotation

    .line 288
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 289
    iput-object p1, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers:Ljava/util/List;

    return-object p0
.end method
