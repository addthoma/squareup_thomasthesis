.class public final Lcom/squareup/server/account/protos/ProcessingFee;
.super Lcom/squareup/wire/AndroidMessage;
.source "ProcessingFee.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;,
        Lcom/squareup/server/account/protos/ProcessingFee$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/ProcessingFee;",
        "Lcom/squareup/server/account/protos/ProcessingFee$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/ProcessingFee;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/ProcessingFee;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/ProcessingFee;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/ProcessingFee;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISCOUNT_BASIS_POINTS:Ljava/lang/Integer;

.field public static final DEFAULT_INTERCHANGE_CENTS:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final discount_basis_points:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final interchange_cents:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->DEFAULT_DISCOUNT_BASIS_POINTS:Ljava/lang/Integer;

    .line 37
    sput-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->DEFAULT_INTERCHANGE_CENTS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 55
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/ProcessingFee;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    .line 62
    iput-object p2, p0, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/ProcessingFee$Builder;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFee;->newBuilder()Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 77
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 78
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFee;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    .line 81
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 91
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/ProcessingFee$Builder;
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->discount_basis_points:Ljava/lang/Integer;

    .line 69
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->interchange_cents:Ljava/lang/Integer;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFee;->newBuilder()Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 2

    .line 115
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFee;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFee$Builder;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->discount_basis_points(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v1

    .line 116
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFee;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFee$Builder;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->interchange_cents(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFee;->overlay(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFee;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFee$Builder;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFee;->DEFAULT_DISCOUNT_BASIS_POINTS:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->discount_basis_points(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v1

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFee;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFee$Builder;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFee;->DEFAULT_INTERCHANGE_CENTS:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->interchange_cents(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 109
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFee;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", discount_basis_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", interchange_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ProcessingFee{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
