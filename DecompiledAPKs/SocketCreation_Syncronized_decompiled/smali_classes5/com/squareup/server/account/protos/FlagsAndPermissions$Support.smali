.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Support"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$ProtoAdapter_Support;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAN_DISPLAY_SUPPORT_MESSAGING:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_DISPLAY_SUPPORT_MESSAGING_ANDROID_TABLET:Ljava/lang/Boolean;

.field public static final DEFAULT_HIDE_ANNOUNCEMENTS_SECTION:Ljava/lang/Boolean;

.field public static final DEFAULT_STREAMING_LEDGER_UPLOAD:Ljava/lang/Boolean;

.field public static final DEFAULT_UPLOAD_LEDGER_AND_DIAGNOSTICS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_JEDI_WORKFLOW:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final can_display_support_messaging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final can_display_support_messaging_android_tablet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final hide_announcements_section:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final streaming_ledger_upload:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final upload_ledger_and_diagnostics:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final use_jedi_workflow:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14312
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$ProtoAdapter_Support;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$ProtoAdapter_Support;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 14314
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 14318
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_UPLOAD_LEDGER_AND_DIAGNOSTICS:Ljava/lang/Boolean;

    .line 14320
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_STREAMING_LEDGER_UPLOAD:Ljava/lang/Boolean;

    .line 14322
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_CAN_DISPLAY_SUPPORT_MESSAGING:Ljava/lang/Boolean;

    .line 14324
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_CAN_DISPLAY_SUPPORT_MESSAGING_ANDROID_TABLET:Ljava/lang/Boolean;

    .line 14326
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_HIDE_ANNOUNCEMENTS_SECTION:Ljava/lang/Boolean;

    .line 14328
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_USE_JEDI_WORKFLOW:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 8

    .line 14394
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 14402
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 14403
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    .line 14404
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    .line 14405
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    .line 14406
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    .line 14407
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    .line 14408
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 14491
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 14427
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 14428
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    .line 14429
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    .line 14430
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    .line 14431
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    .line 14432
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    .line 14433
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    .line 14434
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    .line 14435
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 14440
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_6

    .line 14442
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 14443
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14444
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14445
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14446
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14447
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14448
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 14449
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;
    .locals 2

    .line 14413
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;-><init>()V

    .line 14414
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    .line 14415
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->streaming_ledger_upload:Ljava/lang/Boolean;

    .line 14416
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging:Ljava/lang/Boolean;

    .line 14417
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    .line 14418
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->hide_announcements_section:Ljava/lang/Boolean;

    .line 14419
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->use_jedi_workflow:Ljava/lang/Boolean;

    .line 14420
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 14311
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;
    .locals 2

    .line 14481
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->upload_ledger_and_diagnostics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14482
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->streaming_ledger_upload(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14483
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14484
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging_android_tablet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14485
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->hide_announcements_section(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14486
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->use_jedi_workflow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object p1, p0

    goto :goto_0

    .line 14487
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 14311
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;
    .locals 2

    .line 14469
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_UPLOAD_LEDGER_AND_DIAGNOSTICS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->upload_ledger_and_diagnostics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14470
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_STREAMING_LEDGER_UPLOAD:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->streaming_ledger_upload(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14471
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_CAN_DISPLAY_SUPPORT_MESSAGING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14472
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_CAN_DISPLAY_SUPPORT_MESSAGING_ANDROID_TABLET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->can_display_support_messaging_android_tablet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14473
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_HIDE_ANNOUNCEMENTS_SECTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->hide_announcements_section(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    .line 14474
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->DEFAULT_USE_JEDI_WORKFLOW:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->use_jedi_workflow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object v0, p0

    goto :goto_0

    .line 14475
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 14311
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 14456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14457
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", upload_ledger_and_diagnostics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->upload_ledger_and_diagnostics:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14458
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", streaming_ledger_upload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->streaming_ledger_upload:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14459
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_display_support_messaging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14460
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", can_display_support_messaging_android_tablet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->can_display_support_messaging_android_tablet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14461
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", hide_announcements_section="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->hide_announcements_section:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14462
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", use_jedi_workflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Support;->use_jedi_workflow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Support{"

    .line 14463
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
