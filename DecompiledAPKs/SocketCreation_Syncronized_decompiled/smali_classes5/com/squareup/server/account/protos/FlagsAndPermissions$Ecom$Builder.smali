.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enable_buy_links_silent_auth:Ljava/lang/Boolean;

.field public enable_online_checkout_settings_donations:Ljava/lang/Boolean;

.field public enable_pay_links_on_checkout:Ljava/lang/Boolean;

.field public show_buy_button:Ljava/lang/Boolean;

.field public show_checkout_links_settings:Ljava/lang/Boolean;

.field public show_online_checkout_settings_v2:Ljava/lang/Boolean;

.field public show_pay_online_tender_option:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18909
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;
    .locals 10

    .line 18950
    new-instance v9, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_buy_button:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_checkout_links_settings:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_pay_online_tender_option:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 18894
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object v0

    return-object v0
.end method

.method public enable_buy_links_silent_auth(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    .line 18928
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_online_checkout_settings_donations(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    .line 18944
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_pay_links_on_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    .line 18933
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_buy_button(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    .line 18913
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_buy_button:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_checkout_links_settings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    .line 18918
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_checkout_links_settings:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_online_checkout_settings_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    .line 18938
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_pay_online_tender_option(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    .line 18923
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_pay_online_tender_option:Ljava/lang/Boolean;

    return-object p0
.end method
