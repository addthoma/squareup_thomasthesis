.class public final Lcom/squareup/server/account/protos/RewardTier;
.super Lcom/squareup/wire/AndroidMessage;
.source "RewardTier.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/RewardTier$ProtoAdapter_RewardTier;,
        Lcom/squareup/server/account/protos/RewardTier$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/RewardTier;",
        "Lcom/squareup/server/account/protos/RewardTier$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/RewardTier;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/RewardTier;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUPON_DEFINITION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_POINTS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final coupon_definition_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final points:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/server/account/protos/RewardTier$ProtoAdapter_RewardTier;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/RewardTier$ProtoAdapter_RewardTier;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 34
    sget-object v0, Lcom/squareup/server/account/protos/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/RewardTier;->CREATOR:Landroid/os/Parcelable$Creator;

    const-wide/16 v0, 0x0

    .line 42
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/RewardTier;->DEFAULT_POINTS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .line 75
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/RewardTier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/server/account/protos/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/RewardTier$Builder;)Lcom/squareup/server/account/protos/RewardTier$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier;->newBuilder()Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 99
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/RewardTier;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 100
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/RewardTier;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/RewardTier;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    .line 104
    invoke-virtual {v1, p1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 111
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/RewardTier$Builder;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/server/account/protos/RewardTier$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/RewardTier$Builder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/RewardTier$Builder;->coupon_definition_token:Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/RewardTier$Builder;->name:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/RewardTier$Builder;->points:Ljava/lang/Long;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier;->newBuilder()Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/RewardTier;)Lcom/squareup/server/account/protos/RewardTier;
    .locals 2

    .line 139
    iget-object v0, p1, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/RewardTier;->requireBuilder(Lcom/squareup/server/account/protos/RewardTier$Builder;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->coupon_definition_token(Ljava/lang/String;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v1

    .line 140
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/RewardTier;->requireBuilder(Lcom/squareup/server/account/protos/RewardTier$Builder;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v1

    .line 141
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/RewardTier;->requireBuilder(Lcom/squareup/server/account/protos/RewardTier$Builder;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 142
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->build()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/server/account/protos/RewardTier;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/RewardTier;->overlay(Lcom/squareup/server/account/protos/RewardTier;)Lcom/squareup/server/account/protos/RewardTier;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/RewardTier;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/RewardTier;->requireBuilder(Lcom/squareup/server/account/protos/RewardTier$Builder;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/RewardTier;->DEFAULT_POINTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/RewardTier$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/RewardTier$Builder;->build()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier;->populateDefaults()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", coupon_definition_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, ", points="

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RewardTier{"

    .line 126
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
