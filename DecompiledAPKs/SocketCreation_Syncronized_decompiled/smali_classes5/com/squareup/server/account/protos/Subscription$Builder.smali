.class public final Lcom/squareup/server/account/protos/Subscription$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Subscription.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Subscription;",
        "Lcom/squareup/server/account/protos/Subscription$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public product_key:Ljava/lang/String;

.field public status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/Subscription;
    .locals 4

    .line 137
    new-instance v0, Lcom/squareup/server/account/protos/Subscription;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Subscription$Builder;->product_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/Subscription$Builder;->status:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/Subscription;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Subscription$Builder;->build()Lcom/squareup/server/account/protos/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public product_key(Ljava/lang/String;)Lcom/squareup/server/account/protos/Subscription$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/server/account/protos/Subscription$Builder;->product_key:Ljava/lang/String;

    return-object p0
.end method

.method public status(Ljava/lang/String;)Lcom/squareup/server/account/protos/Subscription$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/server/account/protos/Subscription$Builder;->status:Ljava/lang/String;

    return-object p0
.end method
