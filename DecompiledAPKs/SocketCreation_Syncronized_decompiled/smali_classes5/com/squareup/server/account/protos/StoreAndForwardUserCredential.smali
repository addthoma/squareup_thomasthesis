.class public final Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;
.super Lcom/squareup/wire/AndroidMessage;
.source "StoreAndForwardUserCredential.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$ProtoAdapter_StoreAndForwardUserCredential;,
        Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;",
        "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SIGNED_LOGGED_IN_USER:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final signed_logged_in_user:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$ProtoAdapter_StoreAndForwardUserCredential;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$ProtoAdapter_StoreAndForwardUserCredential;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->DEFAULT_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1

    .line 55
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;-><init>(Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    .line 62
    iput-object p2, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->newBuilder()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 77
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 78
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    .line 81
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 91
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->version:Ljava/lang/Integer;

    .line 69
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->signed_logged_in_user:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->newBuilder()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;
    .locals 2

    .line 114
    iget-object v0, p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object v1

    .line 115
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->signed_logged_in_user(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->build()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->overlay(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->DEFAULT_VERSION:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 108
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->build()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->populateDefaults()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", signed_logged_in_user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;->signed_logged_in_user:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StoreAndForwardUserCredential{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
