.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public pricing_engine_register_android:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14707
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;
    .locals 3

    .line 14721
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing$Builder;->pricing_engine_register_android:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 14704
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing;

    move-result-object v0

    return-object v0
.end method

.method public pricing_engine_register_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing$Builder;
    .locals 0

    .line 14715
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Billing$Builder;->pricing_engine_register_android:Ljava/lang/Boolean;

    return-object p0
.end method
