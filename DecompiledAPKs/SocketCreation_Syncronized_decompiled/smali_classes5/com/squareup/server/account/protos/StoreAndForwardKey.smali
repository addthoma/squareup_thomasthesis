.class public final Lcom/squareup/server/account/protos/StoreAndForwardKey;
.super Lcom/squareup/wire/AndroidMessage;
.source "StoreAndForwardKey.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;,
        Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/StoreAndForwardKey;",
        "Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/StoreAndForwardKey;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/StoreAndForwardKey;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/StoreAndForwardKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/StoreAndForwardKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BLETCHLEY_KEY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_EXPIRATION:Ljava/lang/String; = ""

.field public static final DEFAULT_RAW_CERTIFICATE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bletchley_key_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final expiration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final raw_certificate:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/StoreAndForwardKey$ProtoAdapter_StoreAndForwardKey;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 63
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    .line 70
    iput-object p2, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    .line 71
    iput-object p3, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->newBuilder()Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 87
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 88
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    .line 92
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 97
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 99
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 103
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;
    .locals 2

    .line 76
    new-instance v0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;-><init>()V

    .line 77
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->bletchley_key_id:Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->expiration:Ljava/lang/String;

    .line 79
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->raw_certificate:Ljava/lang/String;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->newBuilder()Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .locals 2

    .line 126
    iget-object v0, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->bletchley_key_id(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object v1

    .line 127
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->expiration(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object v1

    .line 128
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->requireBuilder(Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->raw_certificate(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 129
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/StoreAndForwardKey$Builder;->build()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/server/account/protos/StoreAndForwardKey;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->overlay(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardKey;->populateDefaults()Lcom/squareup/server/account/protos/StoreAndForwardKey;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", bletchley_key_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->bletchley_key_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", expiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->expiration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", raw_certificate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardKey;->raw_certificate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StoreAndForwardKey{"

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
