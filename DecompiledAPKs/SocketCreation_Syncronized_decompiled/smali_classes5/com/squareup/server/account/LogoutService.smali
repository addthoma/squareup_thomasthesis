.class public interface abstract Lcom/squareup/server/account/LogoutService;
.super Ljava/lang/Object;
.source "LogoutService.java"


# virtual methods
.method public abstract logOut(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/logout"
    .end annotation
.end method
