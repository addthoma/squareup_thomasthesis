.class public abstract Lcom/squareup/server/SquareCallbackObserver;
.super Ljava/lang/Object;
.source "SquareCallbackObserver.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<U:",
        "Lcom/squareup/server/SimpleResponse;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/squareup/server/SquareCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/server/SquareCallback<",
            "TU;>;"
        }
    .end annotation
.end field

.field private response:Lcom/squareup/server/SimpleResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TU;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/server/SquareCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "TU;>;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/server/SquareCallbackObserver;->callback:Lcom/squareup/server/SquareCallback;

    return-void
.end method


# virtual methods
.method public abstract handleResponse(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TU;"
        }
    .end annotation
.end method

.method public onCompleted()V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/server/SquareCallbackObserver;->callback:Lcom/squareup/server/SquareCallback;

    iget-object v1, p0, Lcom/squareup/server/SquareCallbackObserver;->response:Lcom/squareup/server/SimpleResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 27
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/squareup/server/SquareCallbackObserver;->callback:Lcom/squareup/server/SquareCallback;

    check-cast p1, Lretrofit/RetrofitError;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/SquareCallbackObserver;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 37
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallbackObserver;->handleResponse(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/SquareCallbackObserver;->response:Lcom/squareup/server/SimpleResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallbackObserver;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
