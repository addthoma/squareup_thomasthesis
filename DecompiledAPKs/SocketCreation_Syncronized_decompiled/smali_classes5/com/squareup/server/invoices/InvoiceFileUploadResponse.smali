.class public Lcom/squareup/server/invoices/InvoiceFileUploadResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "InvoiceFileUploadResponse.java"


# instance fields
.field public final token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/server/invoices/InvoiceFileUploadResponse;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    .line 17
    iput-object p1, p0, Lcom/squareup/server/invoices/InvoiceFileUploadResponse;->token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final isSuccessful()Z
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/server/invoices/InvoiceFileUploadResponse;->token:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
