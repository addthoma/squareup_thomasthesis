.class public final Lcom/squareup/server/DefaultStandardResponseFactory;
.super Ljava/lang/Object;
.source "StandardResponse.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardResponse.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardResponse.kt\ncom/squareup/server/DefaultStandardResponseFactory\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,213:1\n1203#2,2:214\n*E\n*S KotlinDebug\n*F\n+ 1 StandardResponse.kt\ncom/squareup/server/DefaultStandardResponseFactory\n*L\n206#1,2:214\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0085\u0001\u0010\u0003\u001a\u0002H\u0004\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0001\"\u000e\u0008\u0001\u0010\u0004*\u0008\u0012\u0004\u0012\u0002H\u00050\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u000e\u0010\u0016\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00050\u0017\u00a2\u0006\u0002\u0010\u0018J2\u0010\u0019\u001a\n\u0012\u0004\u0012\u0002H\u0004\u0018\u00010\u0008\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0001\"\u000e\u0008\u0001\u0010\u0004*\u0008\u0012\u0004\u0012\u0002H\u00050\u0006*\u0008\u0012\u0004\u0012\u0002H\u00040\u001a\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/server/DefaultStandardResponseFactory;",
        "",
        "()V",
        "create",
        "S",
        "R",
        "Lcom/squareup/server/StandardResponse;",
        "constructor",
        "Ljava/lang/reflect/Constructor;",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "rxCallAdapter",
        "Lretrofit2/CallAdapter;",
        "networkResponseType",
        "Ljava/lang/reflect/Type;",
        "retrofit",
        "Lretrofit2/Retrofit;",
        "annotations",
        "",
        "",
        "call",
        "Lretrofit2/Call;",
        "(Ljava/lang/reflect/Constructor;Lcom/squareup/receiving/StandardReceiver;Lio/reactivex/Scheduler;Lretrofit2/CallAdapter;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Lretrofit2/Call;)Lcom/squareup/server/StandardResponse;",
        "findConstructor",
        "Ljava/lang/Class;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/server/DefaultStandardResponseFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 175
    new-instance v0, Lcom/squareup/server/DefaultStandardResponseFactory;

    invoke-direct {v0}, Lcom/squareup/server/DefaultStandardResponseFactory;-><init>()V

    sput-object v0, Lcom/squareup/server/DefaultStandardResponseFactory;->INSTANCE:Lcom/squareup/server/DefaultStandardResponseFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/reflect/Constructor;Lcom/squareup/receiving/StandardReceiver;Lio/reactivex/Scheduler;Lretrofit2/CallAdapter;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Lretrofit2/Call;)Lcom/squareup/server/StandardResponse;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "S:",
            "Lcom/squareup/server/StandardResponse<",
            "TR;>;>(",
            "Ljava/lang/reflect/Constructor<",
            "TS;>;",
            "Lcom/squareup/receiving/StandardReceiver;",
            "Lio/reactivex/Scheduler;",
            "Lretrofit2/CallAdapter<",
            "TR;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/reflect/Type;",
            "Lretrofit2/Retrofit;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lretrofit2/Call<",
            "TR;>;)TS;"
        }
    .end annotation

    move-object v0, p1

    const-string v1, "constructor"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "standardReceiver"

    move-object v3, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mainScheduler"

    move-object v4, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rxCallAdapter"

    move-object v5, p4

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "networkResponseType"

    move-object v6, p5

    invoke-static {p5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "retrofit"

    move-object/from16 v7, p6

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "annotations"

    move-object/from16 v8, p7

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "call"

    move-object/from16 v9, p8

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    new-instance v1, Lcom/squareup/server/DefaultFactory;

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lcom/squareup/server/DefaultFactory;-><init>(Lcom/squareup/receiving/StandardReceiver;Lio/reactivex/Scheduler;Lretrofit2/CallAdapter;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Lretrofit2/Call;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 196
    invoke-virtual {p1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "constructor.newInstance(factory)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/server/StandardResponse;

    return-object v0
.end method

.method public final findConstructor(Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "S:",
            "Lcom/squareup/server/StandardResponse<",
            "TR;>;>(",
            "Ljava/lang/Class<",
            "TS;>;)",
            "Ljava/lang/reflect/Constructor<",
            "TS;>;"
        }
    .end annotation

    const-string v0, "$this$findConstructor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p1}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 206
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object p1

    const-string v0, "declaredConstructors"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    array-length v0, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_3

    aget-object v4, p1, v3

    const-string v5, "it"

    .line 209
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v5

    array-length v5, v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 208
    invoke-virtual {v4}, Ljava/lang/reflect/Constructor;->getModifiers()I

    move-result v5

    invoke-static {v5}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v5

    aget-object v5, v5, v2

    const-class v7, Lcom/squareup/server/StandardResponse$Factory;

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    move-object v4, v1

    .line 215
    :goto_2
    instance-of p1, v4, Ljava/lang/reflect/Constructor;

    if-nez p1, :cond_4

    goto :goto_3

    :cond_4
    move-object v1, v4

    :goto_3
    return-object v1
.end method
