.class public interface abstract Lcom/squareup/server/catalog/CatalogService;
.super Ljava/lang/Object;
.source "CatalogService.java"


# virtual methods
.method public abstract get(Lsquareup/items/merchant/GetRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lsquareup/items/merchant/GetRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/GetRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lsquareup/items/merchant/GetResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v3/items/get"
    .end annotation
.end method

.method public abstract put(Lsquareup/items/merchant/PutRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lsquareup/items/merchant/PutRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/PutRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lsquareup/items/merchant/PutResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v3/items/put"
    .end annotation
.end method

.method public abstract sync(Lsquareup/items/merchant/BatchRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lsquareup/items/merchant/BatchRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/BatchRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lsquareup/items/merchant/BatchResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v3/items/sync"
    .end annotation
.end method
