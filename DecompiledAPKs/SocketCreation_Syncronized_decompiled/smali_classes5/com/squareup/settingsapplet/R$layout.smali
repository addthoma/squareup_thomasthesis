.class public final Lcom/squareup/settingsapplet/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settingsapplet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final applet_header_list:I = 0x7f0d005c

.field public static final bank_account_settings_view:I = 0x7f0d0086

.field public static final barcode_scanners_settings_view:I = 0x7f0d0089

.field public static final card_reader_detail_screen_view:I = 0x7f0d00bd

.field public static final card_readers_screen_view:I = 0x7f0d00c0

.field public static final cardreaders_learn_more:I = 0x7f0d00c3

.field public static final cash_drawer_settings_view:I = 0x7f0d00d1

.field public static final cash_management_settings_section_view:I = 0x7f0d00d5

.field public static final check_row:I = 0x7f0d00e1

.field public static final close_of_day_dialog_layout:I = 0x7f0d00eb

.field public static final crm_customer_management_settings_view:I = 0x7f0d0131

.field public static final crm_email_collection_settings_view:I = 0x7f0d0141

.field public static final delegate_lockout_view:I = 0x7f0d01b9

.field public static final deposit_schedule_view:I = 0x7f0d01bd

.field public static final deposit_settings_view:I = 0x7f0d01be

.field public static final deposit_settings_view_content:I = 0x7f0d01bf

.field public static final device_name_view:I = 0x7f0d01d9

.field public static final edit_tax_tax_exempt_all_row:I = 0x7f0d0226

.field public static final edit_ticket_group_ticket_counter:I = 0x7f0d0228

.field public static final edit_ticket_group_view:I = 0x7f0d0229

.field public static final edit_ticket_group_view_custom_header:I = 0x7f0d022a

.field public static final edit_ticket_group_view_delete_button:I = 0x7f0d022b

.field public static final edit_ticket_group_view_header:I = 0x7f0d022c

.field public static final editor_dialog_percentage:I = 0x7f0d0231

.field public static final egiftcard_add_design_settings_screen:I = 0x7f0d0234

.field public static final egiftcard_crop_custom_design_settings_screen:I = 0x7f0d023c

.field public static final egiftcard_settings_design_image:I = 0x7f0d023e

.field public static final egiftcard_view_design_settings_screen:I = 0x7f0d023f

.field public static final employee_management_settings_view:I = 0x7f0d024d

.field public static final employees_upsell_screen:I = 0x7f0d024f

.field public static final giftcards_settings_screen:I = 0x7f0d027f

.field public static final hardware_peripheral_empty_row:I = 0x7f0d0284

.field public static final hardware_printer_select_view:I = 0x7f0d0285

.field public static final hardware_settings_message:I = 0x7f0d0286

.field public static final learn_more_contactless:I = 0x7f0d0321

.field public static final learn_more_magstripe:I = 0x7f0d0322

.field public static final learn_more_reader_view:I = 0x7f0d0323

.field public static final loyalty_settings_screen:I = 0x7f0d0341

.field public static final merchant_profile_business_address_view:I = 0x7f0d0353

.field public static final merchant_profile_content:I = 0x7f0d0354

.field public static final merchant_profile_edit_logo_view:I = 0x7f0d0355

.field public static final merchant_profile_view:I = 0x7f0d0356

.field public static final open_tickets_settings_create_ticket_group_button:I = 0x7f0d03d2

.field public static final open_tickets_settings_ticket_group_header:I = 0x7f0d03d3

.field public static final open_tickets_settings_toggle_as_home_screen:I = 0x7f0d03d4

.field public static final open_tickets_settings_toggle_open_tickets:I = 0x7f0d03d5

.field public static final open_tickets_settings_toggle_predefined_tickets:I = 0x7f0d03d6

.field public static final open_tickets_settings_view:I = 0x7f0d03d7

.field public static final orderhub_alert_settings_view:I = 0x7f0d03f4

.field public static final orderhub_printing_settings_view:I = 0x7f0d0415

.field public static final orderhub_quick_actions_settings_view:I = 0x7f0d0416

.field public static final passcodes_settings:I = 0x7f0d0423

.field public static final passcodes_settings_create_or_edit_passcode:I = 0x7f0d0424

.field public static final passcodes_settings_create_or_edit_passcode_success:I = 0x7f0d0425

.field public static final passcodes_settings_timeout:I = 0x7f0d0426

.field public static final payment_types_settings_header_row:I = 0x7f0d0442

.field public static final payment_types_settings_info_row:I = 0x7f0d0443

.field public static final payment_types_settings_view:I = 0x7f0d0444

.field public static final predefined_tickets_opt_in_view:I = 0x7f0d044e

.field public static final printer_detail_category_row:I = 0x7f0d0455

.field public static final printer_station_create_station_row:I = 0x7f0d0456

.field public static final printer_station_detail_view:I = 0x7f0d0457

.field public static final printer_stations_list_view:I = 0x7f0d0458

.field public static final scales_settings_view:I = 0x7f0d04b4

.field public static final settings_applet_list_row:I = 0x7f0d04c6

.field public static final settings_sections_view:I = 0x7f0d04c8

.field public static final shared_settings_view:I = 0x7f0d04cd

.field public static final sign_out_button_list:I = 0x7f0d04cf

.field public static final sign_out_button_sidebar:I = 0x7f0d04d0

.field public static final signature_settings_view:I = 0x7f0d04d3

.field public static final store_and_forward_settings_enable_view:I = 0x7f0d0508

.field public static final store_and_forward_settings_view:I = 0x7f0d0509

.field public static final swipe_chip_cards_settings_enable_view:I = 0x7f0d050c

.field public static final swipe_chip_cards_settings_view:I = 0x7f0d050d

.field public static final tax_applicable_items_view:I = 0x7f0d0512

.field public static final tax_detail_view:I = 0x7f0d0513

.field public static final tax_fee_type_view:I = 0x7f0d0514

.field public static final tax_item_pricing_view:I = 0x7f0d0515

.field public static final tax_settings_add_tax_row:I = 0x7f0d0516

.field public static final taxes_list_view:I = 0x7f0d0517

.field public static final ticket_template_row:I = 0x7f0d053a

.field public static final tile_appearance_view:I = 0x7f0d053d

.field public static final time_tracking_settings:I = 0x7f0d053f

.field public static final tip_settings_view:I = 0x7f0d0553


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
