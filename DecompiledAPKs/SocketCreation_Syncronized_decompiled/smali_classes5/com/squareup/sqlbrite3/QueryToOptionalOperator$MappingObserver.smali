.class final Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;
.super Lio/reactivex/observers/DisposableObserver;
.source "QueryToOptionalOperator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/QueryToOptionalOperator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "MappingObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/observers/DisposableObserver<",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;"
    }
.end annotation


# instance fields
.field private final downstream:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer<",
            "-",
            "Ljava/util/Optional<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final mapper:Lio/reactivex/functions/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/Observer;Lio/reactivex/functions/Function;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer<",
            "-",
            "Ljava/util/Optional<",
            "TT;>;>;",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Lio/reactivex/observers/DisposableObserver;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    .line 47
    iput-object p2, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->mapper:Lio/reactivex/functions/Function;

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-interface {v0}, Lio/reactivex/Observer;->onComplete()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-static {p1}, Lio/reactivex/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-interface {v0, p1}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Lcom/squareup/sqlbrite3/SqlBrite$Query;)V
    .locals 3

    const/4 v0, 0x0

    .line 57
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->run()Landroid/database/Cursor;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz p1, :cond_3

    .line 60
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 61
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->mapper:Lio/reactivex/functions/Function;

    invoke-interface {v0, p1}, Lio/reactivex/functions/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "QueryToOne mapper returned null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :try_start_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 66
    :cond_0
    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 67
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cursor returned more than 1 row"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 71
    :cond_2
    :goto_0
    :try_start_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 74
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->isDisposed()Z

    move-result p1

    if-nez p1, :cond_4

    .line 75
    iget-object p1, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-static {v0}, Ljava/util/Optional;->ofNullable(Ljava/lang/Object;)Ljava/util/Optional;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception p1

    .line 78
    invoke-static {p1}, Lio/reactivex/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    .line 79
    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->onError(Ljava/lang/Throwable;)V

    :cond_4
    :goto_2
    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/sqlbrite3/SqlBrite$Query;

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->onNext(Lcom/squareup/sqlbrite3/SqlBrite$Query;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-interface {v0, p0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
