.class public final Lcom/squareup/sqlbrite3/SqlBrite;
.super Ljava/lang/Object;
.source "SqlBrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqlbrite3/SqlBrite$Logger;,
        Lcom/squareup/sqlbrite3/SqlBrite$Query;,
        Lcom/squareup/sqlbrite3/SqlBrite$Builder;
    }
.end annotation


# static fields
.field static final DEFAULT_LOGGER:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

.field static final DEFAULT_TRANSFORMER:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

.field final queryTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/sqlbrite3/SqlBrite$1;

    invoke-direct {v0}, Lcom/squareup/sqlbrite3/SqlBrite$1;-><init>()V

    sput-object v0, Lcom/squareup/sqlbrite3/SqlBrite;->DEFAULT_LOGGER:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    .line 48
    new-instance v0, Lcom/squareup/sqlbrite3/SqlBrite$2;

    invoke-direct {v0}, Lcom/squareup/sqlbrite3/SqlBrite$2;-><init>()V

    sput-object v0, Lcom/squareup/sqlbrite3/SqlBrite;->DEFAULT_TRANSFORMER:Lio/reactivex/ObservableTransformer;

    return-void
.end method

.method constructor <init>(Lcom/squareup/sqlbrite3/SqlBrite$Logger;Lio/reactivex/ObservableTransformer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/sqlbrite3/SqlBrite$Logger;",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)V"
        }
    .end annotation

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/squareup/sqlbrite3/SqlBrite;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    .line 84
    iput-object p2, p0, Lcom/squareup/sqlbrite3/SqlBrite;->queryTransformer:Lio/reactivex/ObservableTransformer;

    return-void
.end method


# virtual methods
.method public wrapContentProvider(Landroid/content/ContentResolver;Lio/reactivex/Scheduler;)Lcom/squareup/sqlbrite3/BriteContentResolver;
    .locals 3

    .line 112
    new-instance v0, Lcom/squareup/sqlbrite3/BriteContentResolver;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/SqlBrite;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    iget-object v2, p0, Lcom/squareup/sqlbrite3/SqlBrite;->queryTransformer:Lio/reactivex/ObservableTransformer;

    invoke-direct {v0, p1, v1, p2, v2}, Lcom/squareup/sqlbrite3/BriteContentResolver;-><init>(Landroid/content/ContentResolver;Lcom/squareup/sqlbrite3/SqlBrite$Logger;Lio/reactivex/Scheduler;Lio/reactivex/ObservableTransformer;)V

    return-object v0
.end method

.method public wrapDatabaseHelper(Landroidx/sqlite/db/SupportSQLiteOpenHelper;Lio/reactivex/Scheduler;)Lcom/squareup/sqlbrite3/BriteDatabase;
    .locals 3

    .line 101
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/SqlBrite;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    iget-object v2, p0, Lcom/squareup/sqlbrite3/SqlBrite;->queryTransformer:Lio/reactivex/ObservableTransformer;

    invoke-direct {v0, p1, v1, p2, v2}, Lcom/squareup/sqlbrite3/BriteDatabase;-><init>(Landroidx/sqlite/db/SupportSQLiteOpenHelper;Lcom/squareup/sqlbrite3/SqlBrite$Logger;Lio/reactivex/Scheduler;Lio/reactivex/ObservableTransformer;)V

    return-object v0
.end method
