.class Lcom/squareup/sqlbrite3/BriteDatabase$1;
.super Ljava/lang/Object;
.source "BriteDatabase.java"

# interfaces
.implements Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/BriteDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqlbrite3/BriteDatabase;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteDatabase;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .line 103
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase$1;->end()V

    return-void
.end method

.method public end()V
    .locals 4

    .line 88
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object v0, v0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    if-eqz v0, :cond_2

    .line 92
    iget-object v1, v0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->parent:Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    .line 93
    iget-object v2, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object v2, v2, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 94
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-boolean v1, v1, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v3, "TXN END %s"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v1

    invoke-interface {v1}, Landroidx/sqlite/db/SupportSQLiteDatabase;->endTransaction()V

    .line 97
    iget-boolean v1, v0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->commit:Z

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {v1, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    :cond_1
    return-void

    .line 90
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not in transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public markSuccessful()V
    .locals 4

    .line 75
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-boolean v0, v0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, v0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "TXN SUCCESS %s"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->setTransactionSuccessful()V

    return-void
.end method

.method public yieldIfContendedSafely()Z
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->yieldIfContendedSafely()Z

    move-result v0

    return v0
.end method

.method public yieldIfContendedSafely(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$1;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p1

    invoke-interface {v0, p1, p2}, Landroidx/sqlite/db/SupportSQLiteDatabase;->yieldIfContendedSafely(J)Z

    move-result p1

    return p1
.end method
