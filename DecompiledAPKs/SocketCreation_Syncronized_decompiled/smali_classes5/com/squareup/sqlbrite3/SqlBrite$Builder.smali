.class public final Lcom/squareup/sqlbrite3/SqlBrite$Builder;
.super Ljava/lang/Object;
.source "SqlBrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/SqlBrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

.field private queryTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget-object v0, Lcom/squareup/sqlbrite3/SqlBrite;->DEFAULT_LOGGER:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    iput-object v0, p0, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    .line 57
    sget-object v0, Lcom/squareup/sqlbrite3/SqlBrite;->DEFAULT_TRANSFORMER:Lio/reactivex/ObservableTransformer;

    iput-object v0, p0, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->queryTransformer:Lio/reactivex/ObservableTransformer;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/sqlbrite3/SqlBrite;
    .locals 3

    .line 75
    new-instance v0, Lcom/squareup/sqlbrite3/SqlBrite;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    iget-object v2, p0, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->queryTransformer:Lio/reactivex/ObservableTransformer;

    invoke-direct {v0, v1, v2}, Lcom/squareup/sqlbrite3/SqlBrite;-><init>(Lcom/squareup/sqlbrite3/SqlBrite$Logger;Lio/reactivex/ObservableTransformer;)V

    return-object v0
.end method

.method public logger(Lcom/squareup/sqlbrite3/SqlBrite$Logger;)Lcom/squareup/sqlbrite3/SqlBrite$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 62
    iput-object p1, p0, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    return-object p0

    .line 61
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "logger == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public queryTransformer(Lio/reactivex/ObservableTransformer;)Lcom/squareup/sqlbrite3/SqlBrite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)",
            "Lcom/squareup/sqlbrite3/SqlBrite$Builder;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 69
    iput-object p1, p0, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->queryTransformer:Lio/reactivex/ObservableTransformer;

    return-object p0

    .line 68
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "queryTransformer == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
