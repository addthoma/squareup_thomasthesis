.class final Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setUpHistoryRecycler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;

    invoke-direct {v0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;-><init>()V

    sput-object v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;)J
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 79
    check-cast p1, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setUpHistoryRecycler$1$1;->invoke(Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method
