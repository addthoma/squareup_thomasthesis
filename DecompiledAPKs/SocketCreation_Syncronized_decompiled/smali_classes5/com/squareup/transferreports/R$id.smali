.class public final Lcom/squareup/transferreports/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final active_sales_row:I = 0x7f0a0150

.field public static final badge:I = 0x7f0a01ef

.field public static final badged_glyph_block:I = 0x7f0a01f1

.field public static final card_payments_title:I = 0x7f0a02be

.field public static final collected:I = 0x7f0a0364

.field public static final current_balance_title:I = 0x7f0a051b

.field public static final data_recycler_view:I = 0x7f0a0545

.field public static final deposit_title:I = 0x7f0a057b

.field public static final deposits_report_detail_bank_or_card:I = 0x7f0a057c

.field public static final deposits_report_detail_card_payments_title:I = 0x7f0a057d

.field public static final deposits_report_detail_collected:I = 0x7f0a057e

.field public static final deposits_report_detail_date_time:I = 0x7f0a057f

.field public static final deposits_report_detail_deposit_number:I = 0x7f0a0580

.field public static final deposits_report_detail_error_section:I = 0x7f0a0581

.field public static final deposits_report_detail_net_total:I = 0x7f0a0582

.field public static final deposits_report_detail_root_view:I = 0x7f0a0583

.field public static final deposits_report_detail_spinner:I = 0x7f0a0584

.field public static final deposits_report_detail_toggles:I = 0x7f0a0585

.field public static final deposits_report_detail_view:I = 0x7f0a0586

.field public static final deposits_report_title:I = 0x7f0a0587

.field public static final deposits_report_view:I = 0x7f0a0588

.field public static final error_section:I = 0x7f0a0729

.field public static final glyph:I = 0x7f0a07ae

.field public static final history_row_title:I = 0x7f0a07dd

.field public static final load_more_spinner:I = 0x7f0a094e

.field public static final net_total:I = 0x7f0a0a12

.field public static final spinner:I = 0x7f0a0ebc

.field public static final subtitle:I = 0x7f0a0f49

.field public static final summary_title:I = 0x7f0a0f4f

.field public static final swipe_refresh:I = 0x7f0a0f54

.field public static final title:I = 0x7f0a103f

.field public static final value:I = 0x7f0a10de


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
