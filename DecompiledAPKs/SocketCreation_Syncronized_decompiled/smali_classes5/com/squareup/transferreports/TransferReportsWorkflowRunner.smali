.class public final Lcom/squareup/transferreports/TransferReportsWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "TransferReportsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;,
        Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/transferreports/TransferReportsProps;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00102\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0010\u0011B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/transferreports/TransferReportsProps;",
        "",
        "viewFactory",
        "Lcom/squareup/transferreports/TransferReportsViewFactory;",
        "workflow",
        "Lcom/squareup/transferreports/TransferReportsWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/transferreports/TransferReportsViewFactory;Lcom/squareup/transferreports/TransferReportsWorkflow;Lcom/squareup/ui/main/PosContainer;)V",
        "getWorkflow",
        "()Lcom/squareup/transferreports/TransferReportsWorkflow;",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "Companion",
        "Factory",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/transferreports/TransferReportsWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->Companion:Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;

    .line 41
    const-class v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TransferReportsWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsViewFactory;Lcom/squareup/transferreports/TransferReportsWorkflow;Lcom/squareup/ui/main/PosContainer;)V
    .locals 9

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v2, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->NAME:Ljava/lang/String;

    .line 17
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 18
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 15
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->workflow:Lcom/squareup/transferreports/TransferReportsWorkflow;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;)V
    .locals 0

    .line 11
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getProps$p(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;)Lcom/squareup/transferreports/TransferReportsProps;
    .locals 0

    .line 11
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->getProps()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/transferreports/TransferReportsProps;

    return-object p0
.end method

.method public static final synthetic access$setProps$p(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;Lcom/squareup/transferreports/TransferReportsProps;)V
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->setProps(Ljava/lang/Object;)V

    return-void
.end method

.method public static final startWorkflow(Lmortar/MortarScope;Lcom/squareup/transferreports/TransferReportsProps;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->Companion:Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;->startWorkflow(Lmortar/MortarScope;Lcom/squareup/transferreports/TransferReportsProps;)V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/transferreports/TransferReportsWorkflow;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->workflow:Lcom/squareup/transferreports/TransferReportsWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->getWorkflow()Lcom/squareup/transferreports/TransferReportsWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 26
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
