.class public final Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;
.super Ljava/lang/Object;
.source "TransferReportsLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rR\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "shortDateFormatter",
        "Ljava/text/DateFormat;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)V",
        "create",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner;",
        "view",
        "Landroid/view/View;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final shortDateFormatter:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/ShortForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortDateFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;->shortDateFormatter:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/transferreports/TransferReportsLayoutRunner;
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    .line 493
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;->shortDateFormatter:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 492
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method
