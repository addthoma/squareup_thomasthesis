.class final Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsWorkflow;->transferReportsScreen(Lcom/squareup/workflow/Sink;ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "depositType",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "settlementReportWrapper",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "token",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

.field final synthetic $showCurrentBalanceAndActiveSales:Z

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/workflow/Sink;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;->$reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    iput-boolean p3, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;->$showCurrentBalanceAndActiveSales:Z

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    check-cast p2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;->invoke(Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)V
    .locals 2

    const-string v0, "depositType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    new-instance v0, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    .line 135
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;->$reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    .line 134
    invoke-direct {v0, v1, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)V

    .line 140
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    .line 141
    new-instance p2, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;

    iget-boolean p3, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;->$showCurrentBalanceAndActiveSales:Z

    invoke-direct {p2, v0, p3}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;-><init>(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Z)V

    .line 140
    invoke-interface {p1, p2}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
