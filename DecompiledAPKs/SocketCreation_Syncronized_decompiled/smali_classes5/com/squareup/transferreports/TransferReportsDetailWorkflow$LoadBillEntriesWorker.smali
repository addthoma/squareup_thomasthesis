.class public final Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "TransferReportsDetailWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsDetailWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "LoadBillEntriesWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0080\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0007\u001a\u00020\u00082\n\u0010\t\u001a\u0006\u0012\u0002\u0008\u00030\nH\u0016J\u0010\u0010\u000b\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u000cH\u0016R\u0011\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "reportsSnapshot",
        "(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V",
        "getReportsSnapshot",
        "()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lorg/reactivestreams/Publisher;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

.field final synthetic this$0:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")V"
        }
    .end annotation

    const-string v0, "reportsSnapshot"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;->this$0:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    .line 149
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    instance-of p1, p1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;

    return p1
.end method

.method public final getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-object v0
.end method

.method public runPublisher()Lorg/reactivestreams/Publisher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/reactivestreams/Publisher<",
            "+",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;->this$0:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    invoke-static {v0}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->access$getTransferReportsLoader$p(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)Lcom/squareup/transferreports/TransferReportsLoader;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-interface {v0, v1}, Lcom/squareup/transferreports/TransferReportsLoader;->loadBillEntries(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v1, "transferReportsLoader.lo\u2026)\n          .toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
