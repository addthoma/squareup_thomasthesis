.class public final Lcom/squareup/transferreports/TransferReportsScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "TransferReportsScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsScope$Creator;,
        Lcom/squareup/transferreports/TransferReportsScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsScope.kt\ncom/squareup/transferreports/TransferReportsScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,29:1\n35#2:30\n*E\n*S KotlinDebug\n*F\n+ 1 TransferReportsScope.kt\ncom/squareup/transferreports/TransferReportsScope\n*L\n18#1:30\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u0013B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u000e\u0008\u0002\u0010\u0003\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016J\t\u0010\n\u001a\u00020\u000bH\u00d6\u0001J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0019\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000bH\u00d6\u0001R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parentKey",
        "section",
        "Ljava/lang/Class;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/Class;)V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "describeContents",
        "",
        "getParentKey",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "ParentComponent",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final section:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/transferreports/TransferReportsScope$Creator;

    invoke-direct {v0}, Lcom/squareup/transferreports/TransferReportsScope$Creator;-><init>()V

    sput-object v0, Lcom/squareup/transferreports/TransferReportsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsScope;->section:Ljava/lang/Class;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/Class;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 11
    check-cast p2, Ljava/lang/Class;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-super {p0, p1}, Lcom/squareup/ui/main/RegisterTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 30
    const-class v1, Lcom/squareup/transferreports/TransferReportsScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/transferreports/TransferReportsScope$ParentComponent;

    .line 19
    invoke-interface {p1}, Lcom/squareup/transferreports/TransferReportsScope$ParentComponent;->transferReportsWorkflowRunnerFactory()Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;

    move-result-object p1

    .line 20
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScope;->section:Ljava/lang/Class;

    invoke-virtual {p1, v1}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Factory;->create(Ljava/lang/Class;)Lcom/squareup/transferreports/TransferReportsWorkflowRunner;

    move-result-object p1

    const-string v1, "builder"

    .line 21
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsScope;->section:Ljava/lang/Class;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
