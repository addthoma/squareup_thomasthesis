.class public final Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;
.super Ljava/lang/Object;
.source "TransferReportsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/transferreports/TransferReportsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final detailWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsDetailWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final instantTransferRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final transferReportsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsDetailWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->transferReportsLoaderProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->instantTransferRunnerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->detailWorkflowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsDetailWorkflow;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)Lcom/squareup/transferreports/TransferReportsWorkflow;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/transferreports/TransferReportsWorkflow;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/transferreports/TransferReportsWorkflow;-><init>(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/transferreports/TransferReportsWorkflow;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->transferReportsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transferreports/TransferReportsLoader;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->instantTransferRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/instantdeposit/InstantDepositRunner;

    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/BrowserLauncher;

    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->detailWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->newInstance(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)Lcom/squareup/transferreports/TransferReportsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsWorkflow_Factory;->get()Lcom/squareup/transferreports/TransferReportsWorkflow;

    move-result-object v0

    return-object v0
.end method
