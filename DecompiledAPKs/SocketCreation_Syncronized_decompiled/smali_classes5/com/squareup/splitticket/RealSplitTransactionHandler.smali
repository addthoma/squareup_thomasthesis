.class public final Lcom/squareup/splitticket/RealSplitTransactionHandler;
.super Ljava/lang/Object;
.source "RealSplitTransactionHandler.kt"

# interfaces
.implements Lcom/squareup/splitticket/SplitTransactionHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u001a\u0010\u0011\u001a\u00020\u00102\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0013\u001a\u00020\nH\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/splitticket/RealSplitTransactionHandler;",
        "Lcom/squareup/splitticket/SplitTransactionHandler;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/payment/Transaction;)V",
        "currentTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "getCurrentTicket",
        "()Lcom/squareup/tickets/OpenTicket;",
        "order",
        "Lcom/squareup/payment/Order;",
        "getOrder",
        "()Lcom/squareup/payment/Order;",
        "getTransaction",
        "()Lcom/squareup/payment/Transaction;",
        "reset",
        "",
        "setTicketFromSplit",
        "splitTicket",
        "splitTicketOrder",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/splitticket/RealSplitTransactionHandler;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method


# virtual methods
.method public getCurrentTicket()Lcom/squareup/tickets/OpenTicket;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitTransactionHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    return-object v0
.end method

.method public getOrder()Lcom/squareup/payment/Order;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitTransactionHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method public final getTransaction()Lcom/squareup/payment/Transaction;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitTransactionHandler;->transaction:Lcom/squareup/payment/Transaction;

    return-object v0
.end method

.method public reset()V
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitTransactionHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void
.end method

.method public setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V
    .locals 1

    const-string v0, "splitTicketOrder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/splitticket/RealSplitTransactionHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/Transaction;->setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V

    return-void
.end method
