.class public final Lcom/squareup/splitticket/RealTicketSplitter;
.super Ljava/lang/Object;
.source "RealTicketSplitter.kt"

# interfaces
.implements Lcom/squareup/splitticket/TicketSplitter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/splitticket/RealTicketSplitter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTicketSplitter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTicketSplitter.kt\ncom/squareup/splitticket/RealTicketSplitter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,695:1\n1642#2,2:696\n950#2:698\n959#2:699\n1360#2:700\n1429#2,3:701\n1099#2,2:704\n1127#2,4:706\n704#2:710\n777#2,2:711\n1642#2,2:713\n704#2:715\n777#2,2:716\n1360#2:718\n1429#2,3:719\n704#2:722\n777#2,2:723\n1642#2,2:725\n704#2:727\n777#2,2:728\n1360#2:730\n1429#2,3:731\n1360#2:734\n1429#2,3:735\n704#2:738\n777#2,2:739\n1360#2:741\n1429#2,3:742\n704#2:745\n777#2,2:746\n1801#2,14:748\n*E\n*S KotlinDebug\n*F\n+ 1 RealTicketSplitter.kt\ncom/squareup/splitticket/RealTicketSplitter\n*L\n117#1,2:696\n143#1:698\n146#1:699\n155#1:700\n155#1,3:701\n182#1,2:704\n182#1,4:706\n192#1:710\n192#1,2:711\n193#1,2:713\n216#1:715\n216#1,2:716\n217#1:718\n217#1,3:719\n304#1:722\n304#1,2:723\n305#1,2:725\n329#1:727\n329#1,2:728\n330#1:730\n330#1,3:731\n332#1:734\n332#1,3:735\n567#1:738\n567#1,2:739\n567#1:741\n567#1,3:742\n592#1:745\n592#1,2:746\n592#1,14:748\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u001e\n\u0002\u0008\u0003\n\u0002\u0010%\n\u0002\u0008\u0010\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\"\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 p2\u00020\u0001:\u0001pBw\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0018\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00120\t\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0002\u0010\u001bJ\u0008\u0010H\u001a\u00020\u0014H\u0016J\u0008\u0010I\u001a\u00020\u0013H\u0002J\n\u0010J\u001a\u0004\u0018\u00010\u0014H\u0002J\n\u0010K\u001a\u0004\u0018\u00010\u0013H\u0016J\u0008\u0010L\u001a\u000205H\u0002J\u0008\u0010M\u001a\u000205H\u0016J\u0008\u0010N\u001a\u000205H\u0016J\u0008\u0010O\u001a\u00020PH\u0002J\u0010\u0010Q\u001a\u00020\u001d2\u0006\u0010R\u001a\u00020\u0013H\u0002J\u0010\u0010S\u001a\u00020\u00142\u0006\u0010R\u001a\u00020\u0013H\u0016J\u0012\u0010T\u001a\u00020P2\u0008\u0010U\u001a\u0004\u0018\u00010VH\u0016J\u0008\u0010W\u001a\u00020PH\u0016J\u0012\u0010X\u001a\u00020P2\u0008\u0010Y\u001a\u0004\u0018\u00010ZH\u0016J\u0010\u0010[\u001a\u00020P2\u0006\u0010\\\u001a\u00020ZH\u0016J\u0008\u0010]\u001a\u00020PH\u0016J\u0010\u0010^\u001a\u00020P2\u0006\u0010R\u001a\u00020\u0013H\u0016J\u0016\u0010_\u001a\u00020P2\u000c\u0010`\u001a\u0008\u0012\u0004\u0012\u00020\u00130aH\u0002J\u0010\u0010b\u001a\u00020\u001d2\u0006\u0010R\u001a\u00020\u0013H\u0016J\u0008\u0010c\u001a\u000205H\u0016J\u0010\u0010d\u001a\u00020P2\u0006\u0010R\u001a\u00020\u0013H\u0002J\u0010\u0010e\u001a\u00020P2\u0006\u0010f\u001a\u00020\u0013H\u0002J\u0018\u0010g\u001a\u00020\u001d2\u0006\u0010R\u001a\u00020\u00132\u0006\u0010h\u001a\u00020\u001dH\u0016J\u0014\u0010i\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u0002050\u0012H\u0002J\u0010\u0010j\u001a\u00020\u00142\u0006\u0010R\u001a\u00020\u0013H\u0016J,\u0010k\u001a\u00020\u001d2\u0006\u0010R\u001a\u00020\u00132\u0006\u0010l\u001a\u00020\u00132\u0008\u0010m\u001a\u0004\u0018\u00010\u00132\u0008\u0010n\u001a\u0004\u0018\u00010oH\u0016R\u0014\u0010\u001c\u001a\u00020\u001d8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010 \u001a\u0004\u0018\u00010\nX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008!\u0010\"\"\u0004\u0008#\u0010$R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020)0(8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008*\u0010+R\u001c\u0010,\u001a\u0010\u0012\u000c\u0012\n .*\u0004\u0018\u00010)0)0-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010/\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00100\u001a\u00020\u00138VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00081\u00102R\u0010\u00103\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u000205X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u000205X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00107\u001a\u0002058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u00088\u00109R\u001a\u0010:\u001a\u0008\u0012\u0004\u0012\u00020\u00140;8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008<\u0010=R \u0010>\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140?X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008@\u0010AR\u001a\u0010B\u001a\u0008\u0012\u0004\u0012\u00020\u00140;8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008C\u0010=R\u001a\u0010D\u001a\u0008\u0012\u0004\u0012\u00020\u00140;8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008E\u0010=R \u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00120\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u000205X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010G\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006q"
    }
    d2 = {
        "Lcom/squareup/splitticket/RealTicketSplitter;",
        "Lcom/squareup/splitticket/TicketSplitter;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "fosterStateKey",
        "Lcom/squareup/BundleKey;",
        "Lcom/squareup/splitticket/FosterState;",
        "orderPrintingDispatcher",
        "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
        "pricingEngineService",
        "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
        "res",
        "Lcom/squareup/util/Res;",
        "splitStatesKey",
        "",
        "",
        "Lcom/squareup/splitticket/RealSplitState;",
        "tickets",
        "Lcom/squareup/tickets/Tickets;",
        "transactionHandler",
        "Lcom/squareup/splitticket/SplitTransactionHandler;",
        "printerStations",
        "Lcom/squareup/print/PrinterStations;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/BundleKey;Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;Lcom/squareup/splitticket/PricingEngineServiceWrapper;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/tickets/Tickets;Lcom/squareup/splitticket/SplitTransactionHandler;Lcom/squareup/print/PrinterStations;)V",
        "allTicketsAreSaved",
        "",
        "getAllTicketsAreSaved",
        "()Z",
        "fosterState",
        "getFosterState$impl_release",
        "()Lcom/squareup/splitticket/FosterState;",
        "setFosterState$impl_release",
        "(Lcom/squareup/splitticket/FosterState;)V",
        "fosterTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "onAsyncStateChange",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/splitticket/SplitState;",
        "getOnAsyncStateChange",
        "()Lio/reactivex/Observable;",
        "onAsyncStateChangeRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "kotlin.jvm.PlatformType",
        "parentBaseName",
        "parentTicketBaseName",
        "getParentTicketBaseName",
        "()Ljava/lang/String;",
        "parentTicketId",
        "printedTicketCount",
        "",
        "savedTicketCount",
        "selectedEntriesCountAcrossAllTickets",
        "getSelectedEntriesCountAcrossAllTickets",
        "()I",
        "splitStates",
        "",
        "getSplitStates",
        "()Ljava/util/Collection;",
        "splitStatesById",
        "",
        "getSplitStatesById$impl_release",
        "()Ljava/util/Map;",
        "splitStatesByOrdinalDescending",
        "getSplitStatesByOrdinalDescending",
        "splitStatesByViewIndex",
        "getSplitStatesByViewIndex",
        "splitTicketsCount",
        "usePrinters",
        "createNewSplitTicket",
        "fetchAndIncrementTicketName",
        "getLowestUnsavedOrdinalState",
        "getMortarBundleKey",
        "getNextStateViewIndex",
        "getSavedTicketsCount",
        "getSplitTicketsCount",
        "initializeDataFromCart",
        "",
        "isLincolnTicket",
        "ticketId",
        "moveSelectedItemsTo",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onSave",
        "outState",
        "printAllTickets",
        "printOneTicket",
        "refreshAutoDiscounts",
        "dirtyStateIds",
        "",
        "removeOneTicket",
        "saveAllTickets",
        "saveLincolnAndCreateNewFoster",
        "saveNonLincolnAndUpdateFoster",
        "nonFosterId",
        "saveOneTicket",
        "removeStateAfterSave",
        "seatCountByTicketId",
        "splitStateForTicket",
        "updateTicket",
        "name",
        "note",
        "predefinedTicket",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BASE_NAME_KEY:Ljava/lang/String; = "parentTicketBaseBane"

.field public static final Companion:Lcom/squareup/splitticket/RealTicketSplitter$Companion;

.field private static final INITIAL_TICKET_VIEW_INDEX:I = 0x0

.field private static final MASTER_TICKET_KEY:Ljava/lang/String; = "fosterTicketKey"

.field private static final PARENT_TICKET_ID_KEY:Ljava/lang/String; = "parentTicketId"

.field private static final PRINTED_TICKET_COUNT_KEY:Ljava/lang/String; = "printedTicketCount"

.field private static final SAVED_TICKET_COUNT_KEY:Ljava/lang/String; = "savedTicketCount"

.field private static final SPLIT_TICKET_COUNT_KEY:Ljava/lang/String; = "splitTicketCountKey"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private fosterState:Lcom/squareup/splitticket/FosterState;

.field private final fosterStateKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;"
        }
    .end annotation
.end field

.field private fosterTicket:Lcom/squareup/tickets/OpenTicket;

.field private final onAsyncStateChangeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcher:Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;

.field private parentBaseName:Ljava/lang/String;

.field private parentTicketId:Ljava/lang/String;

.field private final pricingEngineService:Lcom/squareup/splitticket/PricingEngineServiceWrapper;

.field private printedTicketCount:I

.field private final res:Lcom/squareup/util/Res;

.field private savedTicketCount:I

.field private final splitStatesById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;"
        }
    .end annotation
.end field

.field private final splitStatesKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;>;"
        }
    .end annotation
.end field

.field private splitTicketsCount:I

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final transactionHandler:Lcom/squareup/splitticket/SplitTransactionHandler;

.field private final usePrinters:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/splitticket/RealTicketSplitter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/splitticket/RealTicketSplitter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/splitticket/RealTicketSplitter;->Companion:Lcom/squareup/splitticket/RealTicketSplitter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/BundleKey;Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;Lcom/squareup/splitticket/PricingEngineServiceWrapper;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/tickets/Tickets;Lcom/squareup/splitticket/SplitTransactionHandler;Lcom/squareup/print/PrinterStations;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;",
            "Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;",
            "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;>;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/splitticket/SplitTransactionHandler;",
            "Lcom/squareup/print/PrinterStations;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fosterStateKey"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pricingEngineService"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "splitStatesKey"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tickets"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionHandler"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerStations"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterStateKey:Lcom/squareup/BundleKey;

    iput-object p5, p0, Lcom/squareup/splitticket/RealTicketSplitter;->orderPrintingDispatcher:Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;

    iput-object p6, p0, Lcom/squareup/splitticket/RealTicketSplitter;->pricingEngineService:Lcom/squareup/splitticket/PricingEngineServiceWrapper;

    iput-object p7, p0, Lcom/squareup/splitticket/RealTicketSplitter;->res:Lcom/squareup/util/Res;

    iput-object p8, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesKey:Lcom/squareup/BundleKey;

    iput-object p9, p0, Lcom/squareup/splitticket/RealTicketSplitter;->tickets:Lcom/squareup/tickets/Tickets;

    iput-object p10, p0, Lcom/squareup/splitticket/RealTicketSplitter;->transactionHandler:Lcom/squareup/splitticket/SplitTransactionHandler;

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/squareup/print/PrinterStation$Role;

    .line 53
    sget-object p2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 p3, 0x0

    aput-object p2, p1, p3

    invoke-interface {p11, p1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->usePrinters:Z

    .line 59
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    .line 65
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<SplitState>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->onAsyncStateChangeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/splitticket/RealTicketSplitter;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getOnAsyncStateChangeRelay$p(Lcom/squareup/splitticket/RealTicketSplitter;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->onAsyncStateChangeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method private final fetchAndIncrementTicketName()Ljava/lang/String;
    .locals 3

    .line 575
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    .line 576
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    if-ne v0, v1, :cond_0

    .line 577
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentBaseName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/splitticket/impl/R$string;->split_ticket_name_and_ordinal:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 580
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentBaseName:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    const-string/jumbo v2, "ticket_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 581
    iget v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    const-string v2, "ordinal"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 582
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 583
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final getAllTicketsAreSaved()Z
    .locals 1

    .line 152
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getLowestUnsavedOrdinalState()Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getLowestUnsavedOrdinalState()Lcom/squareup/splitticket/RealSplitState;
    .locals 5

    .line 592
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 745
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 746
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/splitticket/RealSplitState;

    .line 592
    invoke-virtual {v3}, Lcom/squareup/splitticket/RealSplitState;->getHasBeenSaved()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 747
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 748
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 749
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 750
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 751
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_1

    .line 752
    :cond_3
    move-object v2, v1

    check-cast v2, Lcom/squareup/splitticket/RealSplitState;

    .line 592
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getOrdinal()I

    move-result v2

    .line 754
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 755
    move-object v4, v3

    check-cast v4, Lcom/squareup/splitticket/RealSplitState;

    .line 592
    invoke-virtual {v4}, Lcom/squareup/splitticket/RealSplitState;->getOrdinal()I

    move-result v4

    if-le v2, v4, :cond_5

    move-object v1, v3

    move v2, v4

    .line 760
    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 761
    :goto_1
    check-cast v1, Lcom/squareup/splitticket/RealSplitState;

    return-object v1
.end method

.method private final getNextStateViewIndex()I
    .locals 4

    .line 566
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 738
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 739
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/splitticket/RealSplitState;

    .line 567
    invoke-virtual {v3}, Lcom/squareup/splitticket/RealSplitState;->hasAnySelectedEntries$impl_release()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 740
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 741
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 742
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 743
    check-cast v2, Lcom/squareup/splitticket/RealSplitState;

    .line 567
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 744
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 567
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->max(Ljava/lang/Iterable;)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    :cond_3
    const/4 v0, -0x1

    :goto_2
    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final getSplitStatesByOrdinalDescending()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;"
        }
    .end annotation

    .line 146
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 699
    new-instance v1, Lcom/squareup/splitticket/RealTicketSplitter$splitStatesByOrdinalDescending$$inlined$sortedByDescending$1;

    invoke-direct {v1}, Lcom/squareup/splitticket/RealTicketSplitter$splitStatesByOrdinalDescending$$inlined$sortedByDescending$1;-><init>()V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method private final initializeDataFromCart()V
    .locals 6

    const/4 v0, 0x0

    .line 596
    iput v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    .line 600
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->transactionHandler:Lcom/squareup/splitticket/SplitTransactionHandler;

    invoke-interface {v1}, Lcom/squareup/splitticket/SplitTransactionHandler;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 601
    invoke-virtual {v1}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v1

    const-string v2, "parentOrder"

    .line 602
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentBaseName:Ljava/lang/String;

    .line 603
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentTicketId:Ljava/lang/String;

    .line 605
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->popUninterestingItem()Lcom/squareup/checkout/CartItem;

    .line 608
    iget-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->transactionHandler:Lcom/squareup/splitticket/SplitTransactionHandler;

    invoke-interface {v2}, Lcom/squareup/splitticket/SplitTransactionHandler;->getCurrentTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    .line 610
    invoke-static {v1}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v2

    .line 611
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->fetchAndIncrementTicketName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/payment/Order$Builder;->openTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v2

    .line 612
    invoke-virtual {v2}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v2

    .line 613
    new-instance v3, Lcom/squareup/splitticket/RealFosterState;

    const-string v4, "fosterOrder"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v2}, Lcom/squareup/splitticket/RealFosterState;-><init>(Lcom/squareup/payment/Order;)V

    check-cast v3, Lcom/squareup/splitticket/FosterState;

    iput-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    .line 616
    iget-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 617
    iget-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v1

    const-string v4, "parentOrder.ticketId"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/squareup/splitticket/RealSplitState;

    .line 618
    invoke-static {v2}, Lcom/squareup/splitticket/OrdersKt;->withoutVoidedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/splitticket/OrdersKt;->explodeStackedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object v2

    .line 619
    iget v5, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    .line 617
    invoke-direct {v4, v2, v5, v0}, Lcom/squareup/splitticket/RealSplitState;-><init>(Lcom/squareup/payment/Order;II)V

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final isLincolnTicket(Ljava/lang/String;)Z
    .locals 1

    .line 626
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/tickets/OpenTicket;->getClientId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private final refreshAutoDiscounts(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 360
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 361
    invoke-virtual {p0, v0}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v1

    .line 365
    invoke-virtual {v1}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/splitticket/OrdersKt;->rejoinExplodedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object v2

    .line 366
    iget-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->pricingEngineService:Lcom/squareup/splitticket/PricingEngineServiceWrapper;

    invoke-interface {v3, v2}, Lcom/squareup/splitticket/PricingEngineServiceWrapper;->rulesForOrder(Lcom/squareup/payment/Order;)Lrx/Single;

    move-result-object v3

    .line 367
    new-instance v4, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;

    invoke-direct {v4, p0, v1, v0, v2}, Lcom/squareup/splitticket/RealTicketSplitter$refreshAutoDiscounts$1;-><init>(Lcom/squareup/splitticket/RealTicketSplitter;Lcom/squareup/splitticket/RealSplitState;Ljava/lang/String;Lcom/squareup/payment/Order;)V

    check-cast v4, Lrx/functions/Action1;

    invoke-virtual {v3, v4}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final saveLincolnAndCreateNewFoster(Ljava/lang/String;)V
    .locals 10

    .line 422
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const-string v4, "[Ticket_Split_Save_Lincoln] Saving lincoln ticket with id %s"

    .line 424
    invoke-static {v4, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v2

    .line 427
    iget-object v4, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    if-eqz v4, :cond_6

    .line 429
    invoke-interface {v4}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v4

    .line 430
    invoke-virtual {v4}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v4

    const-string v5, "previousFosterOrder"

    .line 435
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 437
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v6

    .line 435
    invoke-static {v4, p1, v6}, Lcom/squareup/splitticket/OrdersKt;->mergeStates(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)Lcom/squareup/splitticket/FosterState;

    move-result-object v6

    .line 439
    invoke-interface {v6}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v7

    .line 440
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    invoke-interface {v6}, Lcom/squareup/splitticket/FosterState;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v6

    .line 443
    iget-object v8, p0, Lcom/squareup/splitticket/RealTicketSplitter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v8}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v0

    .line 440
    invoke-static {v4, v7, v6, v0}, Lcom/squareup/splitticket/OrdersKt;->pruneOrder(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;Ljava/util/List;Lcom/squareup/protos/client/Employee;)Ljava/util/Map;

    new-array v0, v3, [Lkotlin/Pair;

    .line 445
    invoke-static {v0}, Lkotlin/collections/MapsKt;->sortedMapOf([Lkotlin/Pair;)Ljava/util/SortedMap;

    move-result-object v0

    .line 446
    check-cast v0, Ljava/util/Map;

    invoke-static {v7, v0}, Lcom/squareup/splitticket/OrdersKt;->flashNewItemIds(Lcom/squareup/payment/Order;Ljava/util/Map;)Lcom/squareup/payment/Order;

    move-result-object v6

    .line 447
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v7

    invoke-static {v0, p1, v7}, Lcom/squareup/splitticket/RealTicketSplitterKt;->swizzleAllItemIds(Ljava/util/Map;Ljava/lang/String;Ljava/util/Collection;)V

    .line 448
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/squareup/splitticket/OrdersKt;->withSortedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object v0

    .line 449
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/payment/Order;->setOpenTicketName(Ljava/lang/String;)V

    .line 450
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getNote()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/payment/Order;->setOpenTicketNote(Ljava/lang/String;)V

    .line 451
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/payment/Order;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    .line 452
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getHoldsCustomer()Lcom/squareup/payment/crm/HoldsCustomer;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)V

    .line 453
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->seatCountByTicketId()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    .line 454
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 455
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v5

    .line 457
    sget-object v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 458
    iget-object v8, p0, Lcom/squareup/splitticket/RealTicketSplitter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v8}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/squareup/permissions/Employee;->toEmployeeProto()Lcom/squareup/protos/client/Employee;

    move-result-object v8

    goto :goto_0

    :cond_0
    move-object v8, v4

    .line 459
    :goto_0
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 455
    invoke-virtual {v5, p1, v7, v8, v9}, Lcom/squareup/payment/OrderSeatingHandler;->setSeatCount(ILcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V

    .line 462
    :cond_1
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz p1, :cond_2

    .line 464
    iget-object v5, p0, Lcom/squareup/splitticket/RealTicketSplitter;->features:Lcom/squareup/settings/server/Features;

    .line 465
    sget-object v7, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 464
    invoke-interface {v5, v7}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v5

    .line 462
    invoke-virtual {p1, v0, v5}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    .line 468
    :cond_2
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->orderPrintingDispatcher:Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v0, v5}, Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 469
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-interface {p1, v0}, Lcom/squareup/tickets/Tickets;->updateTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V

    .line 470
    invoke-virtual {v2, v1}, Lcom/squareup/splitticket/RealSplitState;->setHasBeenSaved$impl_release(Z)V

    .line 472
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getLowestUnsavedOrdinalState()Lcom/squareup/splitticket/RealSplitState;

    move-result-object p1

    if-nez p1, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_4

    .line 475
    move-object p1, v4

    check-cast p1, Lcom/squareup/splitticket/FosterState;

    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    .line 476
    check-cast v4, Lcom/squareup/tickets/OpenTicket;

    iput-object v4, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    return-void

    .line 480
    :cond_4
    invoke-static {v6}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 481
    new-instance v2, Lcom/squareup/protos/client/IdPair;

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/splitticket/RealSplitState;->getTicketId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/squareup/payment/Order$Builder;->ticketIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 482
    invoke-virtual {p1}, Lcom/squareup/splitticket/RealSplitState;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order$Builder;->openTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object p1

    .line 483
    invoke-virtual {p1}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p1

    const-string v0, "Order.Builder.fromOrder(\u2026te.name)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 484
    new-instance v0, Lcom/squareup/splitticket/RealFosterState;

    invoke-direct {v0, p1}, Lcom/squareup/splitticket/RealFosterState;-><init>(Lcom/squareup/payment/Order;)V

    check-cast v0, Lcom/squareup/splitticket/FosterState;

    iput-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    .line 487
    invoke-static {p1}, Lcom/squareup/splitticket/OrdersKt;->removeUnlockedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object p1

    new-array v0, v1, [Ljava/lang/Object;

    .line 490
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "[Ticket_Split_Save_Lincoln] Creating new foster ticket with id %s"

    .line 488
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 493
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->tickets:Lcom/squareup/tickets/Tickets;

    .line 494
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 495
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/payment/Order;->getCartProtoForTicket(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 493
    invoke-interface {v0, v1, p1}, Lcom/squareup/tickets/Tickets;->addTicketAndLock(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    return-void

    .line 427
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final saveNonLincolnAndUpdateFoster(Ljava/lang/String;)V
    .locals 10

    .line 508
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    .line 509
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v1

    .line 510
    invoke-virtual {v1}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v2

    .line 511
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->seatCountByTicketId()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    .line 512
    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 513
    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v5

    .line 515
    sget-object v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 516
    iget-object v7, p0, Lcom/squareup/splitticket/RealTicketSplitter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v7}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/squareup/permissions/Employee;->toEmployeeProto()Lcom/squareup/protos/client/Employee;

    move-result-object v7

    goto :goto_0

    :cond_0
    move-object v7, v4

    .line 517
    :goto_0
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 513
    invoke-virtual {v5, v3, v6, v7, v8}, Lcom/squareup/payment/OrderSeatingHandler;->setSeatCount(ILcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V

    .line 520
    :cond_1
    invoke-static {v2}, Lcom/squareup/splitticket/OrdersKt;->rejoinExplodedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object v2

    .line 521
    iget-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    const-string v5, "Required value was null."

    if-eqz v3, :cond_5

    invoke-interface {v3}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    .line 524
    invoke-virtual {v1}, Lcom/squareup/splitticket/RealSplitState;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v6

    .line 525
    iget-object v7, p0, Lcom/squareup/splitticket/RealTicketSplitter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v7}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v0

    .line 523
    invoke-static {v3, v2, v6, v0}, Lcom/squareup/splitticket/OrdersKt;->pruneOrder(Lcom/squareup/payment/Order;Lcom/squareup/payment/Order;Ljava/util/List;Lcom/squareup/protos/client/Employee;)Ljava/util/Map;

    move-result-object v0

    .line 527
    new-instance v6, Lcom/squareup/splitticket/RealFosterState;

    invoke-static {v3}, Lcom/squareup/splitticket/OrdersKt;->withSortedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object v3

    invoke-direct {v6, v3}, Lcom/squareup/splitticket/RealFosterState;-><init>(Lcom/squareup/payment/Order;)V

    check-cast v6, Lcom/squareup/splitticket/FosterState;

    iput-object v6, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    .line 532
    invoke-static {v2, v4}, Lcom/squareup/splitticket/OrdersKt;->flashNewItemIds(Lcom/squareup/payment/Order;Ljava/util/Map;)Lcom/squareup/payment/Order;

    move-result-object v2

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Object;

    const-string v6, "Ticket_Split_Save_NonLincoln"

    const/4 v7, 0x0

    aput-object v6, v4, v7

    const/4 v8, 0x1

    aput-object p1, v4, v8

    const-string v9, "[%s] Saving split ticket with id %s"

    .line 533
    invoke-static {v9, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 536
    iget-object v4, p0, Lcom/squareup/splitticket/RealTicketSplitter;->orderPrintingDispatcher:Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;

    .line 537
    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    .line 536
    invoke-interface {v4, v2, v9}, Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 539
    iget-object v4, p0, Lcom/squareup/splitticket/RealTicketSplitter;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getCartProtoForTicket()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v2

    invoke-interface {v4, v9, v2}, Lcom/squareup/tickets/Tickets;->addTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)V

    .line 540
    invoke-virtual {v1, v8}, Lcom/squareup/splitticket/RealSplitState;->setHasBeenSaved$impl_release(Z)V

    .line 544
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/splitticket/RealTicketSplitterKt;->swizzleAllItemIds(Ljava/util/Map;Ljava/lang/String;Ljava/util/Collection;)V

    .line 547
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    if-eqz p1, :cond_4

    invoke-interface {p1}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/splitticket/OrdersKt;->removeUnlockedItems(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Object;

    aput-object v6, v0, v7

    .line 550
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    const-string v1, "[%s] Updating foster ticket with id %s"

    .line 548
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 552
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v0, :cond_2

    .line 554
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->features:Lcom/squareup/settings/server/Features;

    .line 555
    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 554
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 552
    invoke-virtual {v0, p1, v1}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    .line 558
    :cond_2
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    invoke-interface {p1, v0}, Lcom/squareup/tickets/Tickets;->updateTicketAndMaintainLock(Lcom/squareup/tickets/OpenTicket;)V

    return-void

    .line 550
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 547
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 521
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final seatCountByTicketId()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 629
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 630
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    .line 631
    iget-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/squareup/tickets/OpenTicket;->getEnabledSeatCount()I

    move-result v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_2

    .line 633
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    rem-int v5, v4, v5

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/splitticket/RealSplitState;

    invoke-virtual {v5}, Lcom/squareup/splitticket/RealSplitState;->getTicketId()Ljava/lang/String;

    move-result-object v5

    .line 634
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    :goto_2
    add-int/lit8 v6, v6, 0x1

    .line 635
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    return-object v0
.end method


# virtual methods
.method public createNewSplitTicket()Lcom/squareup/splitticket/RealSplitState;
    .locals 9

    .line 173
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UUID.randomUUID()\n        .toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/splitticket/RealSplitState;

    invoke-virtual {v1}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/payment/Order$Builder;->cloneState(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 180
    new-instance v2, Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->ticketIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 181
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->fetchAndIncrementTicketName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->openTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 182
    iget-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    check-cast v2, Ljava/lang/Iterable;

    const/16 v4, 0xa

    .line 704
    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-static {v4}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v4

    .line 705
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5, v4}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v5, Ljava/util/Map;

    .line 706
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 707
    move-object v6, v4

    check-cast v6, Lcom/squareup/checkout/Surcharge;

    .line 182
    invoke-virtual {v6}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v5

    :cond_1
    invoke-virtual {v1, v5}, Lcom/squareup/payment/Order$Builder;->surcharges(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 183
    iget-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/squareup/payment/OrderSeatingHandler;->getSeating()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object v3

    :cond_2
    invoke-virtual {v1, v3}, Lcom/squareup/payment/Order$Builder;->seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 184
    invoke-virtual {v1}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v1

    .line 185
    new-instance v2, Lcom/squareup/splitticket/RealSplitState;

    const-string v3, "newTicketOrder"

    .line 186
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    iget v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    .line 188
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getNextStateViewIndex()I

    move-result v4

    .line 185
    invoke-direct {v2, v1, v3, v4}, Lcom/squareup/splitticket/RealSplitState;-><init>(Lcom/squareup/payment/Order;II)V

    .line 192
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 710
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 711
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Lcom/squareup/splitticket/RealSplitState;

    .line 192
    invoke-virtual {v7}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v7

    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v8

    if-le v7, v8, :cond_4

    const/4 v5, 0x1

    :cond_4
    if-eqz v5, :cond_3

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 712
    :cond_5
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 713
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/splitticket/RealSplitState;

    .line 194
    invoke-virtual {v3}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v4

    add-int/2addr v4, v6

    invoke-virtual {v3, v4}, Lcom/squareup/splitticket/RealSplitState;->setViewIndex$impl_release(I)V

    goto :goto_2

    .line 198
    :cond_6
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array v1, v6, [Ljava/lang/Object;

    aput-object v0, v1, v5

    const-string v2, "[Ticket_Split_New] Created new split state with id %s"

    .line 199
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    invoke-virtual {p0, v0}, Lcom/squareup/splitticket/RealTicketSplitter;->moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 203
    new-instance v2, Lcom/squareup/splitticket/analytics/SplitTicketNewActionEvent;

    .line 204
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->hasAtLeastOneUnitPricedItem$impl_release()Z

    move-result v3

    .line 203
    invoke-direct {v2, v3}, Lcom/squareup/splitticket/analytics/SplitTicketNewActionEvent;-><init>(Z)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 202
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-object v0
.end method

.method public bridge synthetic createNewSplitTicket()Lcom/squareup/splitticket/SplitState;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->createNewSplitTicket()Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    check-cast v0, Lcom/squareup/splitticket/SplitState;

    return-object v0
.end method

.method public final getFosterState$impl_release()Lcom/squareup/splitticket/FosterState;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 98
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOnAsyncStateChange()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->onAsyncStateChangeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public getParentTicketBaseName()Ljava/lang/String;
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentBaseName:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getSavedTicketsCount()I
    .locals 1

    .line 159
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->savedTicketCount:I

    return v0
.end method

.method public getSelectedEntriesCountAcrossAllTickets()I
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 700
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 701
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 702
    check-cast v2, Lcom/squareup/splitticket/RealSplitState;

    .line 155
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getSelectedEntriesCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 703
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 155
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->sumOfInt(Ljava/lang/Iterable;)I

    move-result v0

    return v0
.end method

.method public getSplitStates()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final getSplitStatesById$impl_release()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    return-object v0
.end method

.method public getSplitStatesByViewIndex()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;"
        }
    .end annotation

    .line 143
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 698
    new-instance v1, Lcom/squareup/splitticket/RealTicketSplitter$splitStatesByViewIndex$$inlined$sortedBy$1;

    invoke-direct {v1}, Lcom/squareup/splitticket/RealTicketSplitter$splitStatesByViewIndex$$inlined$sortedBy$1;-><init>()V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method

.method public getSplitTicketsCount()I
    .locals 1

    .line 157
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    return v0
.end method

.method public moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;
    .locals 7

    const-string/jumbo v0, "ticketId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_MOVE_ITEMS:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 326
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getHasBeenSaved()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_4

    .line 329
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 727
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 728
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/splitticket/RealSplitState;

    .line 329
    invoke-virtual {v5}, Lcom/squareup/splitticket/RealSplitState;->hasAnySelectedEntries$impl_release()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 729
    :cond_1
    check-cast v3, Ljava/util/List;

    .line 330
    check-cast v3, Ljava/lang/Iterable;

    .line 730
    new-instance v1, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v3, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 731
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 732
    check-cast v6, Lcom/squareup/splitticket/RealSplitState;

    .line 330
    invoke-virtual {v6}, Lcom/squareup/splitticket/RealSplitState;->getTicketId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 733
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 330
    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 734
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v3, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 735
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 736
    check-cast v4, Lcom/squareup/splitticket/RealSplitState;

    .line 333
    invoke-virtual {v4}, Lcom/squareup/splitticket/RealSplitState;->getConfigurationForMoveAndInvalidate$impl_release()Lcom/squareup/splitticket/MoveConfiguration;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 737
    :cond_3
    check-cast v5, Ljava/util/List;

    .line 335
    invoke-static {v5}, Lcom/squareup/splitticket/MoveConfigurationKt;->merge(Ljava/util/List;)Lcom/squareup/splitticket/MoveConfiguration;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 339
    invoke-virtual {v3}, Lcom/squareup/splitticket/MoveConfiguration;->getItems()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3}, Lcom/squareup/splitticket/MoveConfiguration;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    aput-object p1, v4, v2

    const-string v2, "[Ticket_Split_Move] Moving %d items and %d discounts to ticket with id %s"

    .line 337
    invoke-static {v2, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342
    invoke-virtual {v0, v3}, Lcom/squareup/splitticket/RealSplitState;->withMoveConfiguration$impl_release(Lcom/squareup/splitticket/MoveConfiguration;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    .line 343
    iget-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->refreshAutoDiscounts(Ljava/util/Set;)V

    return-object v0

    .line 327
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot add to "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ". It has been saved"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object p1

    check-cast p1, Lcom/squareup/splitticket/SplitState;

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    .line 106
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->initializeDataFromCart()V

    goto/16 :goto_1

    :cond_0
    const-string v0, "splitTicketCountKey"

    .line 108
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    const-string v0, "savedTicketCount"

    .line 109
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->savedTicketCount:I

    const-string v0, "parentTicketBaseBane"

    .line 110
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentBaseName:Ljava/lang/String;

    const-string v0, "parentTicketId"

    .line 111
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentTicketId:Ljava/lang/String;

    const-string v0, "printedTicketCount"

    .line 112
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->printedTicketCount:I

    .line 113
    sget-object v0, Lcom/squareup/tickets/OpenTicket;->Companion:Lcom/squareup/tickets/OpenTicket$Companion;

    const-string v1, "fosterTicketKey"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/tickets/OpenTicket$Companion;->loadFromBundle(Ljava/lang/String;Landroid/os/Bundle;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    .line 114
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterStateKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/splitticket/FosterState;

    iput-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    .line 115
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->init()V

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesKey:Lcom/squareup/BundleKey;

    invoke-virtual {v1, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    const-string v1, "splitStatesKey.get(savedInstanceState)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 696
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/splitticket/RealSplitState;

    .line 117
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->init()V

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitTicketsCount:I

    const-string v1, "splitTicketCountKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 123
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->savedTicketCount:I

    const-string v1, "savedTicketCount"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentBaseName:Ljava/lang/String;

    const-string v1, "parentTicketBaseBane"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->parentTicketId:Ljava/lang/String;

    const-string v1, "parentTicketId"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->printedTicketCount:I

    const-string v1, "printedTicketCount"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    if-eqz v0, :cond_0

    const-string v1, "fosterTicketKey"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/tickets/OpenTicket;->saveToBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterStateKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method public printAllTickets()V
    .locals 6

    .line 278
    iget-boolean v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->usePrinters:Z

    if-eqz v0, :cond_2

    .line 279
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->saveAllTickets()I

    .line 281
    iget v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->printedTicketCount:I

    const/4 v1, 0x0

    .line 283
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/splitticket/RealSplitState;

    .line 285
    invoke-virtual {v3}, Lcom/squareup/splitticket/RealSplitState;->hasItems()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 286
    iget-object v4, p0, Lcom/squareup/splitticket/RealTicketSplitter;->orderPrintingDispatcher:Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;

    invoke-virtual {v3}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v5

    invoke-virtual {v3}, Lcom/squareup/splitticket/RealSplitState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;->printBill(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 291
    :cond_1
    iget-object v2, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 292
    new-instance v3, Lcom/squareup/splitticket/analytics/SplitTicketPrintAllEvent;

    invoke-direct {v3, v1, v0}, Lcom/squareup/splitticket/analytics/SplitTicketPrintAllEvent;-><init>(II)V

    check-cast v3, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 291
    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void

    .line 278
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should not be allowed to print with no enabled bill printers!"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public printOneTicket(Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "ticketId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    iget-boolean v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->usePrinters:Z

    if-eqz v0, :cond_3

    .line 262
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->hasItems()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_PRINT:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 267
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getHasBeenSaved()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 268
    invoke-virtual {p0, p1, v1}, Lcom/squareup/splitticket/RealTicketSplitter;->saveOneTicket(Ljava/lang/String;Z)Z

    .line 272
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->hasItems()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 273
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->orderPrintingDispatcher:Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;

    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getOrder$impl_release()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/squareup/splitticket/OrderPrintingDispatcherWrapper;->printBill(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    :cond_1
    return-void

    .line 263
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot print a bill for a ticket with no items!"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 260
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should not be allowed to print with no enabled bill printers!"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public removeOneTicket(Ljava/lang/String;)Z
    .locals 6

    const-string/jumbo v0, "ticketId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Lcom/squareup/splitticket/RealSplitState;

    .line 303
    invoke-virtual {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 722
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 723
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/squareup/splitticket/RealSplitState;

    .line 304
    invoke-virtual {v4}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v4

    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v5

    if-le v4, v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 724
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 725
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/splitticket/RealSplitState;

    .line 305
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getViewIndex()I

    move-result v1

    sub-int/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/squareup/splitticket/RealSplitState;->setViewIndex$impl_release(I)V

    goto :goto_2

    .line 306
    :cond_3
    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v3

    return p1

    .line 300
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Shouldn\'t try to remove "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " from "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 299
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public saveAllTickets()I
    .locals 4

    .line 214
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getSplitStatesByOrdinalDescending()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 715
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 716
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/splitticket/RealSplitState;

    .line 216
    invoke-virtual {v3}, Lcom/squareup/splitticket/RealSplitState;->getHasBeenSaved()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 717
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 718
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 719
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 720
    check-cast v2, Lcom/squareup/splitticket/RealSplitState;

    .line 217
    invoke-virtual {v2}, Lcom/squareup/splitticket/RealSplitState;->getTicketId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/squareup/splitticket/RealTicketSplitter;->saveOneTicket(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 721
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 218
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 219
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 220
    new-instance v2, Lcom/squareup/splitticket/analytics/SplitTicketSaveAllEvent;

    .line 221
    iget v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->savedTicketCount:I

    .line 220
    invoke-direct {v2, v0, v3}, Lcom/squareup/splitticket/analytics/SplitTicketSaveAllEvent;-><init>(II)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 219
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return v0
.end method

.method public saveOneTicket(Ljava/lang/String;Z)Z
    .locals 5

    const-string/jumbo v0, "ticketId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_SAVE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 233
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getHasBeenSaved()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_4

    .line 235
    invoke-direct {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->isLincolnTicket(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    invoke-direct {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->saveLincolnAndCreateNewFoster(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->saveNonLincolnAndUpdateFoster(Ljava/lang/String;)V

    .line 240
    :goto_0
    iget v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->savedTicketCount:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->savedTicketCount:I

    .line 241
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/log/tickets/OpenTicketSaved;

    invoke-virtual {v0}, Lcom/squareup/splitticket/RealSplitState;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/squareup/log/tickets/OpenTicketSaved;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    check-cast v3, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->transactionHandler:Lcom/squareup/splitticket/SplitTransactionHandler;

    invoke-interface {v0}, Lcom/squareup/splitticket/SplitTransactionHandler;->reset()V

    .line 246
    invoke-direct {p0}, Lcom/squareup/splitticket/RealTicketSplitter;->getAllTicketsAreSaved()Z

    move-result v0

    if-nez v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->transactionHandler:Lcom/squareup/splitticket/SplitTransactionHandler;

    .line 248
    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterTicket:Lcom/squareup/tickets/OpenTicket;

    .line 249
    iget-object v3, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    if-eqz v3, :cond_1

    invoke-interface {v3}, Lcom/squareup/splitticket/FosterState;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v3

    const-string v4, "Order.Builder.fromOrder(\u2026sterState).order).build()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    invoke-interface {v0, v1, v3}, Lcom/squareup/splitticket/SplitTransactionHandler;->setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V

    goto :goto_1

    .line 249
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    :goto_1
    if-eqz p2, :cond_3

    .line 253
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->removeOneTicket(Ljava/lang/String;)Z

    move-result v2

    :cond_3
    return v2

    .line 234
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Cannot save ticket "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " twice! :("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final setFosterState$impl_release(Lcom/squareup/splitticket/FosterState;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->fosterState:Lcom/squareup/splitticket/FosterState;

    return-void
.end method

.method public splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;
    .locals 2

    const-string/jumbo v0, "ticketId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/splitticket/RealSplitState;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No state in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/splitticket/RealTicketSplitter;->splitStatesById:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object p1

    check-cast p1, Lcom/squareup/splitticket/SplitState;

    return-object p1
.end method

.method public updateTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Z
    .locals 4

    const-string/jumbo v0, "ticketId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    invoke-virtual {p0, p1}, Lcom/squareup/splitticket/RealTicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/RealSplitState;

    move-result-object p1

    .line 316
    invoke-virtual {p1}, Lcom/squareup/splitticket/RealSplitState;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 317
    invoke-virtual {p1}, Lcom/squareup/splitticket/RealSplitState;->getNote()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v1

    .line 318
    invoke-virtual {p1}, Lcom/squareup/splitticket/RealSplitState;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v3

    invoke-static {v3, p4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v1

    .line 319
    invoke-virtual {p1, p2, p3, p4}, Lcom/squareup/splitticket/RealSplitState;->updateTicketDetails(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method
