.class public final Lcom/squareup/tenderworkflow/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderworkflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final confirm_and_pay_button_height:I = 0x7f0700d4

.field public static final confirm_and_pay_button_text_size:I = 0x7f0700d5

.field public static final payment_options_container_margin_bottom:I = 0x7f07040c

.field public static final payment_options_row_text_size:I = 0x7f070411

.field public static final payment_options_scroll_view_shadow_length:I = 0x7f070412

.field public static final payment_options_split_tender_cancel_padding:I = 0x7f070413

.field public static final payment_prompt_card_inserted_offset:I = 0x7f070414

.field public static final payment_prompt_card_removed_offset:I = 0x7f070415

.field public static final payment_prompt_card_slot_padding_top:I = 0x7f070416

.field public static final payment_prompt_charge_text_size:I = 0x7f070417

.field public static final payment_prompt_header_buttons_margin:I = 0x7f070418

.field public static final payment_prompt_nfc_icon_top_offset:I = 0x7f070419

.field public static final payment_prompt_nfc_margin_top:I = 0x7f07041a

.field public static final payment_prompt_payment_language_switcher_textSize:I = 0x7f07041b

.field public static final payment_prompt_payment_type_margin_top:I = 0x7f07041c

.field public static final payment_prompt_payment_type_margin_top_no_transaction_type:I = 0x7f07041d

.field public static final payment_prompt_swipe_insert_tap:I = 0x7f07041e

.field public static final payment_prompt_transaction_total_margin_top:I = 0x7f07041f

.field public static final payment_prompt_transaction_total_text_size:I = 0x7f070420

.field public static final payment_prompt_transaction_type_text_size:I = 0x7f070421

.field public static final split_tender_banner_height:I = 0x7f0704de

.field public static final split_tender_banner_text_size:I = 0x7f0704df

.field public static final split_tender_edit_amount_internal_padding:I = 0x7f0704e0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
