.class public final Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;
.super Ljava/lang/Object;
.source "ProcessedTransactionSummary.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008&\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 A2\u00020\u0001:\u0001AB\u008b\u0001\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\u0006\u0010\u0013\u001a\u00020\u0011\u0012\u0006\u0010\u0014\u001a\u00020\u0011\u0012\u0006\u0010\u0015\u001a\u00020\u0011\u0012\u0006\u0010\u0016\u001a\u00020\u0011\u0012\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000e\u00a2\u0006\u0002\u0010\u0019J\t\u0010,\u001a\u00020\u0003H\u00c6\u0003J\t\u0010-\u001a\u00020\u0011H\u00c6\u0003J\t\u0010.\u001a\u00020\u0011H\u00c6\u0003J\t\u0010/\u001a\u00020\u0011H\u00c6\u0003J\t\u00100\u001a\u00020\u0011H\u00c6\u0003J\t\u00101\u001a\u00020\u0011H\u00c6\u0003J\u000f\u00102\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000eH\u00c6\u0003J\t\u00103\u001a\u00020\u0003H\u00c6\u0003J\t\u00104\u001a\u00020\u0006H\u00c6\u0003J\t\u00105\u001a\u00020\u0008H\u00c6\u0003J\t\u00106\u001a\u00020\nH\u00c6\u0003J\t\u00107\u001a\u00020\u0003H\u00c6\u0003J\t\u00108\u001a\u00020\u0003H\u00c6\u0003J\u000f\u00109\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u00c6\u0003J\t\u0010:\u001a\u00020\u0011H\u00c6\u0003J\u00ab\u0001\u0010;\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00032\u000e\u0008\u0002\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u00112\u000e\u0008\u0002\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000eH\u00c6\u0001J\u0013\u0010<\u001a\u00020\u00112\u0008\u0010=\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010>\u001a\u00020?H\u00d6\u0001J\t\u0010@\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001dR\u0011\u0010\u0015\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\u0012\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\"R\u0011\u0010\u0016\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\"R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\"R\u0011\u0010\u0014\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\"R\u0011\u0010\u0013\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\"R\u0017\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010\u001dR\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u001dR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010&R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+\u00a8\u0006B"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
        "",
        "clientId",
        "",
        "serverId",
        "date",
        "Ljava/util/Date;",
        "transactionType",
        "Lcom/squareup/transactionhistory/TransactionType;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "description",
        "tenderId",
        "tenderInfo",
        "",
        "Lcom/squareup/transactionhistory/TenderInfo;",
        "isAwaitingTip",
        "",
        "hasExpiringTip",
        "isNoSale",
        "isFullyVoided",
        "hasError",
        "hasRelatedTransactions",
        "searchMatches",
        "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;)V",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getClientId",
        "()Ljava/lang/String;",
        "getDate",
        "()Ljava/util/Date;",
        "getDescription",
        "getHasError",
        "()Z",
        "getHasExpiringTip",
        "getHasRelatedTransactions",
        "getSearchMatches",
        "()Ljava/util/List;",
        "getServerId",
        "getTenderId",
        "getTenderInfo",
        "getTransactionType",
        "()Lcom/squareup/transactionhistory/TransactionType;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final clientId:Ljava/lang/String;

.field private final date:Ljava/util/Date;

.field private final description:Ljava/lang/String;

.field private final hasError:Z

.field private final hasExpiringTip:Z

.field private final hasRelatedTransactions:Z

.field private final isAwaitingTip:Z

.field private final isFullyVoided:Z

.field private final isNoSale:Z

.field private final searchMatches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
            ">;"
        }
    .end annotation
.end field

.field private final serverId:Ljava/lang/String;

.field private final tenderId:Ljava/lang/String;

.field private final tenderInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionType:Lcom/squareup/transactionhistory/TransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->Companion:Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Lcom/squareup/transactionhistory/TransactionType;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;ZZZZZZ",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    iput-object p4, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    iput-object p5, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    iput-object p6, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    iput-boolean p9, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    iput-boolean p10, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    iput-boolean p11, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    iput-boolean p12, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    iput-boolean p13, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    iput-boolean p14, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    iput-object p15, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 43
    invoke-direct/range {p0 .. p15}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p16

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-boolean v10, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-boolean v11, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-boolean v12, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-boolean v13, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    goto :goto_b

    :cond_b
    move/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-boolean v14, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-boolean v15, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    goto :goto_d

    :cond_d
    move/from16 v15, p14

    :goto_d
    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_e

    iget-object v1, v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    goto :goto_e

    :cond_e
    move-object/from16 v1, p15

    :goto_e
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v13

    move/from16 p13, v14

    move/from16 p14, v15

    move-object/from16 p15, v1

    invoke-virtual/range {p0 .. p15}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;)Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    return v0
.end method

.method public final component11()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    return v0
.end method

.method public final component12()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    return v0
.end method

.method public final component13()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    return v0
.end method

.method public final component14()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    return v0
.end method

.method public final component15()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    return-object v0
.end method

.method public final component4()Lcom/squareup/transactionhistory/TransactionType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    return-object v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;)Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Lcom/squareup/transactionhistory/TransactionType;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;ZZZZZZ",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
            ">;)",
            "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;"
        }
    .end annotation

    const-string v0, "clientId"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serverId"

    move-object/from16 v3, p2

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "date"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionType"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderId"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInfo"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchMatches"

    move-object/from16 v15, p15

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;

    move-object v1, v0

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move/from16 v15, p14

    move-object/from16 v16, p15

    invoke-direct/range {v1 .. v16}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    iget-boolean v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    iget-boolean v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    iget-boolean v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    iget-boolean v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    iget-boolean v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    iget-boolean v1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getClientId()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getDate()Ljava/util/Date;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final getHasError()Z
    .locals 1

    .line 115
    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    return v0
.end method

.method public final getHasExpiringTip()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    return v0
.end method

.method public final getHasRelatedTransactions()Z
    .locals 1

    .line 122
    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    return v0
.end method

.method public final getSearchMatches()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
            ">;"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    return-object v0
.end method

.method public final getServerId()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTenderId()Ljava/lang/String;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTenderInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    return-object v0
.end method

.method public final getTransactionType()Lcom/squareup/transactionhistory/TransactionType;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :cond_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :cond_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :cond_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    :cond_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_e
    add-int/2addr v0, v1

    return v0
.end method

.method public final isAwaitingTip()Z
    .locals 1

    .line 96
    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    return v0
.end method

.method public final isFullyVoided()Z
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    return v0
.end method

.method public final isNoSale()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProcessedTransactionSummary(clientId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", serverId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->date:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->transactionType:Lcom/squareup/transactionhistory/TransactionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->tenderInfo:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isAwaitingTip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasExpiringTip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasExpiringTip:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isNoSale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isFullyVoided="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasRelatedTransactions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->hasRelatedTransactions:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", searchMatches="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->searchMatches:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
