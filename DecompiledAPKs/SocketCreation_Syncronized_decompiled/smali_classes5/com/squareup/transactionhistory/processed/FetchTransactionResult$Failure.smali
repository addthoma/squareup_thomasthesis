.class public final Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;
.super Lcom/squareup/transactionhistory/processed/FetchTransactionResult;
.source "ProcessedTransactionsStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transactionhistory/processed/FetchTransactionResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failure"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;",
        "Lcom/squareup/transactionhistory/processed/FetchTransactionResult;",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "(Lcom/squareup/receiving/FailureMessage;)V",
        "getFailureMessage",
        "()Lcom/squareup/receiving/FailureMessage;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final failureMessage:Lcom/squareup/receiving/FailureMessage;


# direct methods
.method public constructor <init>(Lcom/squareup/receiving/FailureMessage;)V
    .locals 1

    const-string v0, "failureMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 86
    invoke-direct {p0, v0}, Lcom/squareup/transactionhistory/processed/FetchTransactionResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-void
.end method


# virtual methods
.method public final getFailureMessage()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method
