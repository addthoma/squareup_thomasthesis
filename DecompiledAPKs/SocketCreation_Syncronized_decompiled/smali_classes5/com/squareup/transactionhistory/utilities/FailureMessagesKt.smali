.class public final Lcom/squareup/transactionhistory/utilities/FailureMessagesKt;
.super Ljava/lang/Object;
.source "FailureMessages.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0016\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u001a\u0018\u0010\u0006\u001a\u00020\u0001*\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u001a\u0018\u0010\u000b\u001a\u00020\u0001*\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\t\u00a8\u0006\r"
    }
    d2 = {
        "defaultFailureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "res",
        "Lcom/squareup/util/Res;",
        "retryable",
        "",
        "defaultFailureMessageFor",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "showFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
        "statusFailureMessageFor",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final defaultFailureMessage(Lcom/squareup/util/Res;Z)Lcom/squareup/receiving/FailureMessage;
    .locals 3

    const-string v0, "res"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/receiving/FailureMessage;

    .line 21
    sget v1, Lcom/squareup/common/strings/R$string;->failed:I

    invoke-interface {p0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 22
    sget v2, Lcom/squareup/common/strings/R$string;->server_error_message:I

    invoke-interface {p0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 20
    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static final defaultFailureMessageFor(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
            ">;)",
            "Lcom/squareup/receiving/FailureMessage;"
        }
    .end annotation

    const-string v0, "$this$defaultFailureMessageFor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    sget v0, Lcom/squareup/common/strings/R$string;->failed:I

    sget-object v1, Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$defaultFailureMessageFor$1;->INSTANCE:Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$defaultFailureMessageFor$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p0

    return-object p0
.end method

.method public static final statusFailureMessageFor(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;)",
            "Lcom/squareup/receiving/FailureMessage;"
        }
    .end annotation

    const-string v0, "$this$statusFailureMessageFor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget v0, Lcom/squareup/common/strings/R$string;->failed:I

    sget-object v1, Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;->INSTANCE:Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p0

    return-object p0
.end method
