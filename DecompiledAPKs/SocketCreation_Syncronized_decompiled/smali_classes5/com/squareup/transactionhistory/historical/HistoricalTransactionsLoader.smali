.class public interface abstract Lcom/squareup/transactionhistory/historical/HistoricalTransactionsLoader;
.super Ljava/lang/Object;
.source "HistoricalTransactionsLoader.kt"

# interfaces
.implements Lcom/squareup/transactionhistory/TransactionsLoader;
.implements Lcom/squareup/transactionhistory/historical/HistoricalTransactionsUpdater;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/transactionhistory/TransactionsLoader<",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        ">;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionsUpdater;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003J\u0014\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u00060\u0005H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J \u0010\t\u001a\u00020\u00082\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u000bj\u0008\u0012\u0004\u0012\u00020\u0002`\u000cH&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionsLoader;",
        "Lcom/squareup/transactionhistory/TransactionsLoader;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionsUpdater;",
        "cumulativeSummaries",
        "Lio/reactivex/Observable;",
        "",
        "loadNext",
        "",
        "setSortOrder",
        "comparator",
        "Ljava/util/Comparator;",
        "Lkotlin/Comparator;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cumulativeSummaries()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract loadNext()V
.end method

.method public abstract setSortOrder(Ljava/util/Comparator;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;)V"
        }
    .end annotation
.end method
