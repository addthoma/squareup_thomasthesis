.class public final Lcom/squareup/transactionhistory/mappings/FelicaBrandMappingKt;
.super Ljava/lang/Object;
.source "FelicaBrandMapping.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFelicaBrandMapping.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FelicaBrandMapping.kt\ncom/squareup/transactionhistory/mappings/FelicaBrandMappingKt\n*L\n1#1,38:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0004\u0018\u00010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toFelicaBrand",
        "Lcom/squareup/transactionhistory/FelicaBrand;",
        "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toFelicaBrand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/transactionhistory/FelicaBrand;
    .locals 7

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    .line 34
    invoke-static {}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->values()[Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;

    move-result-object v1

    .line 35
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_2

    aget-object v5, v1, v4

    invoke-virtual {v5}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->getProtoFelicaBrand$public_release()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    move-result-object v6

    if-ne v6, p0, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_1

    move-object v0, v5

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 36
    invoke-virtual {v0}, Lcom/squareup/transactionhistory/mappings/FelicaBrandMapping;->getTransactionFelicaBrand$public_release()Lcom/squareup/transactionhistory/FelicaBrand;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized FeliCa brand: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    :goto_3
    return-object v0
.end method
