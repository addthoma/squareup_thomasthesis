.class public final Lcom/squareup/transactionhistory/Configuration;
.super Ljava/lang/Object;
.source "Configuration.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConfiguration.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Configuration.kt\ncom/squareup/transactionhistory/Configuration\n*L\n1#1,123:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B)\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J-\u0010\u0012\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/Configuration;",
        "",
        "timeInterval",
        "Lcom/squareup/transactionhistory/TimeInterval;",
        "queryString",
        "",
        "searchInstrument",
        "Lcom/squareup/protos/transactionsfe/SearchInstrument;",
        "(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)V",
        "getQueryString",
        "()Ljava/lang/String;",
        "getSearchInstrument",
        "()Lcom/squareup/protos/transactionsfe/SearchInstrument;",
        "getTimeInterval",
        "()Lcom/squareup/transactionhistory/TimeInterval;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final queryString:Ljava/lang/String;

.field private final searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

.field private final timeInterval:Lcom/squareup/transactionhistory/TimeInterval;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/transactionhistory/Configuration;-><init>(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    iput-object p2, p0, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    .line 43
    iget-object p1, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    if-eqz p1, :cond_1

    .line 45
    iget-object p2, p1, Lcom/squareup/protos/transactionsfe/SearchInstrument;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    if-eqz p2, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/transactionsfe/SearchInstrument;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string p2, "If set, search instrument must have a non-null payment instrument and entry method."

    .line 44
    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    :cond_1
    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 19
    move-object p1, v0

    check-cast p1, Lcom/squareup/transactionhistory/TimeInterval;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 30
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 40
    move-object p3, v0

    check-cast p3, Lcom/squareup/protos/transactionsfe/SearchInstrument;

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transactionhistory/Configuration;-><init>(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/transactionhistory/Configuration;Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;ILjava/lang/Object;)Lcom/squareup/transactionhistory/Configuration;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/transactionhistory/Configuration;->copy(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)Lcom/squareup/transactionhistory/Configuration;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/transactionhistory/TimeInterval;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/transactionsfe/SearchInstrument;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    return-object v0
.end method

.method public final copy(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)Lcom/squareup/transactionhistory/Configuration;
    .locals 1

    new-instance v0, Lcom/squareup/transactionhistory/Configuration;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/transactionhistory/Configuration;-><init>(Lcom/squareup/transactionhistory/TimeInterval;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transactionhistory/Configuration;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transactionhistory/Configuration;

    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    iget-object v1, p1, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    iget-object p1, p1, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getQueryString()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    return-object v0
.end method

.method public final getSearchInstrument()Lcom/squareup/protos/transactionsfe/SearchInstrument;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    return-object v0
.end method

.method public final getTimeInterval()Lcom/squareup/transactionhistory/TimeInterval;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Configuration(timeInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/Configuration;->timeInterval:Lcom/squareup/transactionhistory/TimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", queryString="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/Configuration;->queryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", searchInstrument="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/Configuration;->searchInstrument:Lcom/squareup/protos/transactionsfe/SearchInstrument;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
