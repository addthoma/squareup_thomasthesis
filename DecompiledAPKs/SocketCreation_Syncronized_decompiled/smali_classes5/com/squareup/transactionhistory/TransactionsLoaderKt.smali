.class public final Lcom/squareup/transactionhistory/TransactionsLoaderKt;
.super Ljava/lang/Object;
.source "TransactionsLoader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0017\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "NO_ERROR",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/receiving/FailureMessage;",
        "getNO_ERROR",
        "()Lcom/squareup/util/Optional;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final NO_ERROR:Lcom/squareup/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 134
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    sput-object v0, Lcom/squareup/transactionhistory/TransactionsLoaderKt;->NO_ERROR:Lcom/squareup/util/Optional;

    return-void
.end method

.method public static final getNO_ERROR()Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;"
        }
    .end annotation

    .line 134
    sget-object v0, Lcom/squareup/transactionhistory/TransactionsLoaderKt;->NO_ERROR:Lcom/squareup/util/Optional;

    return-object v0
.end method
