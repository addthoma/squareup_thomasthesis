.class public Lcom/squareup/timessquare/CalendarCellView;
.super Landroid/widget/FrameLayout;
.source "CalendarCellView.java"


# static fields
.field private static final STATE_CURRENT_MONTH:[I

.field private static final STATE_HIGHLIGHTED:[I

.field private static final STATE_RANGE_FIRST:[I

.field private static final STATE_RANGE_LAST:[I

.field private static final STATE_RANGE_MIDDLE:[I

.field private static final STATE_SELECTABLE:[I

.field private static final STATE_TODAY:[I


# instance fields
.field private dayOfMonthTextView:Landroid/widget/TextView;

.field private isCurrentMonth:Z

.field private isHighlighted:Z

.field private isSelectable:Z

.field private isToday:Z

.field private rangeState:Lcom/squareup/timessquare/RangeState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [I

    .line 11
    sget v2, Lcom/squareup/timessquare/R$attr;->tsquare_state_selectable:I

    const/4 v3, 0x0

    aput v2, v1, v3

    sput-object v1, Lcom/squareup/timessquare/CalendarCellView;->STATE_SELECTABLE:[I

    new-array v1, v0, [I

    .line 14
    sget v2, Lcom/squareup/timessquare/R$attr;->tsquare_state_current_month:I

    aput v2, v1, v3

    sput-object v1, Lcom/squareup/timessquare/CalendarCellView;->STATE_CURRENT_MONTH:[I

    new-array v1, v0, [I

    .line 17
    sget v2, Lcom/squareup/timessquare/R$attr;->tsquare_state_today:I

    aput v2, v1, v3

    sput-object v1, Lcom/squareup/timessquare/CalendarCellView;->STATE_TODAY:[I

    new-array v1, v0, [I

    .line 20
    sget v2, Lcom/squareup/timessquare/R$attr;->tsquare_state_highlighted:I

    aput v2, v1, v3

    sput-object v1, Lcom/squareup/timessquare/CalendarCellView;->STATE_HIGHLIGHTED:[I

    new-array v1, v0, [I

    .line 23
    sget v2, Lcom/squareup/timessquare/R$attr;->tsquare_state_range_first:I

    aput v2, v1, v3

    sput-object v1, Lcom/squareup/timessquare/CalendarCellView;->STATE_RANGE_FIRST:[I

    new-array v1, v0, [I

    .line 26
    sget v2, Lcom/squareup/timessquare/R$attr;->tsquare_state_range_middle:I

    aput v2, v1, v3

    sput-object v1, Lcom/squareup/timessquare/CalendarCellView;->STATE_RANGE_MIDDLE:[I

    new-array v0, v0, [I

    .line 29
    sget v1, Lcom/squareup/timessquare/R$attr;->tsquare_state_range_last:I

    aput v1, v0, v3

    sput-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_RANGE_LAST:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 33
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isSelectable:Z

    .line 34
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isCurrentMonth:Z

    .line 35
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isToday:Z

    .line 36
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isHighlighted:Z

    .line 37
    sget-object p1, Lcom/squareup/timessquare/RangeState;->NONE:Lcom/squareup/timessquare/RangeState;

    iput-object p1, p0, Lcom/squareup/timessquare/CalendarCellView;->rangeState:Lcom/squareup/timessquare/RangeState;

    return-void
.end method


# virtual methods
.method public getDayOfMonthTextView()Landroid/widget/TextView;
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarCellView;->dayOfMonthTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    return-object v0

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You have to setDayOfMonthTextView in your custom DayViewAdapter."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getRangeState()Lcom/squareup/timessquare/RangeState;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarCellView;->rangeState:Lcom/squareup/timessquare/RangeState;

    return-object v0
.end method

.method public isCurrentMonth()Z
    .locals 1

    .line 81
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isCurrentMonth:Z

    return v0
.end method

.method public isHighlighted()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isHighlighted:Z

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isSelectable:Z

    return v0
.end method

.method public isToday()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isToday:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    add-int/lit8 p1, p1, 0x5

    .line 101
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object p1

    .line 103
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isSelectable:Z

    if-eqz v0, :cond_0

    .line 104
    sget-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_SELECTABLE:[I

    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->mergeDrawableStates([I[I)[I

    .line 107
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isCurrentMonth:Z

    if-eqz v0, :cond_1

    .line 108
    sget-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_CURRENT_MONTH:[I

    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->mergeDrawableStates([I[I)[I

    .line 111
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isToday:Z

    if-eqz v0, :cond_2

    .line 112
    sget-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_TODAY:[I

    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->mergeDrawableStates([I[I)[I

    .line 115
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isHighlighted:Z

    if-eqz v0, :cond_3

    .line 116
    sget-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_HIGHLIGHTED:[I

    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->mergeDrawableStates([I[I)[I

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarCellView;->rangeState:Lcom/squareup/timessquare/RangeState;

    sget-object v1, Lcom/squareup/timessquare/RangeState;->FIRST:Lcom/squareup/timessquare/RangeState;

    if-ne v0, v1, :cond_4

    .line 120
    sget-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_RANGE_FIRST:[I

    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 121
    :cond_4
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarCellView;->rangeState:Lcom/squareup/timessquare/RangeState;

    sget-object v1, Lcom/squareup/timessquare/RangeState;->MIDDLE:Lcom/squareup/timessquare/RangeState;

    if-ne v0, v1, :cond_5

    .line 122
    sget-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_RANGE_MIDDLE:[I

    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 123
    :cond_5
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarCellView;->rangeState:Lcom/squareup/timessquare/RangeState;

    sget-object v1, Lcom/squareup/timessquare/RangeState;->LAST:Lcom/squareup/timessquare/RangeState;

    if-ne v0, v1, :cond_6

    .line 124
    sget-object v0, Lcom/squareup/timessquare/CalendarCellView;->STATE_RANGE_LAST:[I

    invoke-static {p1, v0}, Lcom/squareup/timessquare/CalendarCellView;->mergeDrawableStates([I[I)[I

    :cond_6
    :goto_0
    return-object p1
.end method

.method public setCurrentMonth(Z)V
    .locals 1

    .line 53
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isCurrentMonth:Z

    if-eq v0, p1, :cond_0

    .line 54
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isCurrentMonth:Z

    .line 55
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarCellView;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public setDayOfMonthTextView(Landroid/widget/TextView;)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarCellView;->dayOfMonthTextView:Landroid/widget/TextView;

    return-void
.end method

.method public setHighlighted(Z)V
    .locals 1

    .line 74
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isHighlighted:Z

    if-eq v0, p1, :cond_0

    .line 75
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isHighlighted:Z

    .line 76
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarCellView;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public setRangeState(Lcom/squareup/timessquare/RangeState;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarCellView;->rangeState:Lcom/squareup/timessquare/RangeState;

    if-eq v0, p1, :cond_0

    .line 68
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarCellView;->rangeState:Lcom/squareup/timessquare/RangeState;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarCellView;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public setSelectable(Z)V
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isSelectable:Z

    if-eq v0, p1, :cond_0

    .line 47
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isSelectable:Z

    .line 48
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarCellView;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public setToday(Z)V
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarCellView;->isToday:Z

    if-eq v0, p1, :cond_0

    .line 61
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarCellView;->isToday:Z

    .line 62
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarCellView;->refreshDrawableState()V

    :cond_0
    return-void
.end method
