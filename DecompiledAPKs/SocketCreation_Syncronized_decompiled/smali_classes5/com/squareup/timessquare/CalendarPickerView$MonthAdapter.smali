.class Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;
.super Landroid/widget/BaseAdapter;
.source "CalendarPickerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/timessquare/CalendarPickerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MonthAdapter"
.end annotation


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/squareup/timessquare/CalendarPickerView;


# direct methods
.method private constructor <init>(Lcom/squareup/timessquare/CalendarPickerView;)V
    .locals 0

    .line 883
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 884
    invoke-virtual {p1}, Lcom/squareup/timessquare/CalendarPickerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->inflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/timessquare/CalendarPickerView;Lcom/squareup/timessquare/CalendarPickerView$1;)V
    .locals 0

    .line 880
    invoke-direct {p0, p1}, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;-><init>(Lcom/squareup/timessquare/CalendarPickerView;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 893
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v0, v0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 897
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v0, v0, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19

    move-object/from16 v0, p0

    .line 905
    move-object/from16 v1, p2

    check-cast v1, Lcom/squareup/timessquare/MonthView;

    if-eqz v1, :cond_1

    .line 906
    sget v2, Lcom/squareup/timessquare/R$id;->day_view_adapter_class:I

    .line 907
    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/MonthView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v3}, Lcom/squareup/timessquare/CalendarPickerView;->access$1600(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/DayViewAdapter;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 915
    :cond_0
    iget-object v2, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v2}, Lcom/squareup/timessquare/CalendarPickerView;->access$2500(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/MonthView;->setDecorators(Ljava/util/List;)V

    goto :goto_1

    .line 908
    :cond_1
    :goto_0
    iget-object v4, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->inflater:Landroid/view/LayoutInflater;

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    .line 909
    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$600(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/text/DateFormat;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v6, v1, Lcom/squareup/timessquare/CalendarPickerView;->listener:Lcom/squareup/timessquare/MonthView$Listener;

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v7, v1, Lcom/squareup/timessquare/CalendarPickerView;->today:Ljava/util/Calendar;

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$1700(Lcom/squareup/timessquare/CalendarPickerView;)I

    move-result v8

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    .line 910
    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$1800(Lcom/squareup/timessquare/CalendarPickerView;)I

    move-result v9

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$1900(Lcom/squareup/timessquare/CalendarPickerView;)I

    move-result v10

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$2000(Lcom/squareup/timessquare/CalendarPickerView;)I

    move-result v11

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$2100(Lcom/squareup/timessquare/CalendarPickerView;)Z

    move-result v12

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    .line 911
    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$2200(Lcom/squareup/timessquare/CalendarPickerView;)I

    move-result v13

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$2300(Lcom/squareup/timessquare/CalendarPickerView;)Z

    move-result v14

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$2400(Lcom/squareup/timessquare/CalendarPickerView;)Z

    move-result v15

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    .line 912
    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$2500(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/List;

    move-result-object v16

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$500(Lcom/squareup/timessquare/CalendarPickerView;)Ljava/util/Locale;

    move-result-object v17

    iget-object v1, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v1}, Lcom/squareup/timessquare/CalendarPickerView;->access$1600(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/DayViewAdapter;

    move-result-object v18

    move-object/from16 v3, p3

    .line 909
    invoke-static/range {v3 .. v18}, Lcom/squareup/timessquare/MonthView;->create(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Ljava/text/DateFormat;Lcom/squareup/timessquare/MonthView$Listener;Ljava/util/Calendar;IIIIZIZZLjava/util/List;Ljava/util/Locale;Lcom/squareup/timessquare/DayViewAdapter;)Lcom/squareup/timessquare/MonthView;

    move-result-object v1

    .line 913
    sget v2, Lcom/squareup/timessquare/R$id;->day_view_adapter_class:I

    iget-object v3, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v3}, Lcom/squareup/timessquare/CalendarPickerView;->access$1600(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/DayViewAdapter;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/timessquare/MonthView;->setTag(ILjava/lang/Object;)V

    .line 917
    :goto_1
    iget-object v2, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v2}, Lcom/squareup/timessquare/CalendarPickerView;->access$800(Lcom/squareup/timessquare/CalendarPickerView;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 918
    iget-object v2, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v2, v2, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v2, v2, p1

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_2
    move/from16 v2, p1

    .line 920
    :goto_2
    iget-object v3, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget-object v3, v3, Lcom/squareup/timessquare/CalendarPickerView;->months:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/squareup/timessquare/MonthDescriptor;

    iget-object v3, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v3}, Lcom/squareup/timessquare/CalendarPickerView;->access$2600(Lcom/squareup/timessquare/CalendarPickerView;)Lcom/squareup/timessquare/IndexedLinkedHashMap;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/timessquare/IndexedLinkedHashMap;->getValueAtIndex(I)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Ljava/util/List;

    iget-object v2, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v2}, Lcom/squareup/timessquare/CalendarPickerView;->access$700(Lcom/squareup/timessquare/CalendarPickerView;)Z

    move-result v7

    iget-object v2, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    .line 921
    invoke-static {v2}, Lcom/squareup/timessquare/CalendarPickerView;->access$2700(Lcom/squareup/timessquare/CalendarPickerView;)Landroid/graphics/Typeface;

    move-result-object v8

    iget-object v2, v0, Lcom/squareup/timessquare/CalendarPickerView$MonthAdapter;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    invoke-static {v2}, Lcom/squareup/timessquare/CalendarPickerView;->access$2800(Lcom/squareup/timessquare/CalendarPickerView;)Landroid/graphics/Typeface;

    move-result-object v9

    move-object v4, v1

    .line 920
    invoke-virtual/range {v4 .. v9}, Lcom/squareup/timessquare/MonthView;->init(Lcom/squareup/timessquare/MonthDescriptor;Ljava/util/List;ZLandroid/graphics/Typeface;Landroid/graphics/Typeface;)V

    return-object v1
.end method

.method public isEnabled(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
