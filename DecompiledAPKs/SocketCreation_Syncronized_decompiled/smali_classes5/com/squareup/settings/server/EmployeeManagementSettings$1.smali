.class synthetic Lcom/squareup/settings/server/EmployeeManagementSettings$1;
.super Ljava/lang/Object;
.source "EmployeeManagementSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/EmployeeManagementSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$FieldName:[I

.field static final synthetic $SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$PasscodeAccess:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 329
    invoke-static {}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->values()[Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$FieldName:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$FieldName:[I

    sget-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v2}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$FieldName:[I

    sget-object v3, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TIME_TRACKING:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v3}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$FieldName:[I

    sget-object v3, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->GUEST_MODE:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v3}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->ordinal()I

    move-result v3

    const/4 v4, 0x3

    aput v4, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$FieldName:[I

    sget-object v3, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TRANSACTION_LOCK:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v3}, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->ordinal()I

    move-result v3

    const/4 v4, 0x4

    aput v4, v2, v3
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 312
    :catch_3
    invoke-static {}, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->values()[Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$PasscodeAccess:[I

    :try_start_4
    sget-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$PasscodeAccess:[I

    sget-object v3, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v3}, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$PasscodeAccess:[I

    sget-object v2, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v2}, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    return-void
.end method
