.class public final enum Lcom/squareup/settings/server/ProductIntent;
.super Ljava/lang/Enum;
.source "ProductIntent.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/server/ProductIntent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0012\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/settings/server/ProductIntent;",
        "",
        "(Ljava/lang/String;I)V",
        "PRODUCT_INTENT_APPOINTMENTS",
        "PRODUCT_INTENT_CAPITAL",
        "PRODUCT_INTENT_DEVELOPERS",
        "PRODUCT_INTENT_ECOMMERCE",
        "PRODUCT_INTENT_EMPLOYEES",
        "PRODUCT_INTENT_INVOICES",
        "PRODUCT_INTENT_MOBILE_POS",
        "PRODUCT_INTENT_PARTNER_POS",
        "PRODUCT_INTENT_PAYROLL",
        "PRODUCT_INTENT_POS",
        "PRODUCT_INTENT_RESTAURANTS",
        "PRODUCT_INTENT_RETAIL",
        "PRODUCT_INTENT_SERVICES_POS",
        "PRODUCT_INTENT_TERMINAL",
        "PRODUCT_INTENT_UNKNOWN",
        "PRODUCT_INTENT_VIRTUAL_TERMINAL",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_APPOINTMENTS:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_CAPITAL:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_DEVELOPERS:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_ECOMMERCE:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_EMPLOYEES:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_INVOICES:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_MOBILE_POS:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_PARTNER_POS:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_PAYROLL:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_POS:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_RESTAURANTS:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_RETAIL:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_SERVICES_POS:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_TERMINAL:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_UNKNOWN:Lcom/squareup/settings/server/ProductIntent;

.field public static final enum PRODUCT_INTENT_VIRTUAL_TERMINAL:Lcom/squareup/settings/server/ProductIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/settings/server/ProductIntent;

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x0

    const-string v3, "PRODUCT_INTENT_APPOINTMENTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_APPOINTMENTS:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x1

    const-string v3, "PRODUCT_INTENT_CAPITAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_CAPITAL:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x2

    const-string v3, "PRODUCT_INTENT_DEVELOPERS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_DEVELOPERS:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x3

    const-string v3, "PRODUCT_INTENT_ECOMMERCE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_ECOMMERCE:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x4

    const-string v3, "PRODUCT_INTENT_EMPLOYEES"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_EMPLOYEES:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x5

    const-string v3, "PRODUCT_INTENT_INVOICES"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_INVOICES:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x6

    const-string v3, "PRODUCT_INTENT_MOBILE_POS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_MOBILE_POS:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/4 v2, 0x7

    const-string v3, "PRODUCT_INTENT_PARTNER_POS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_PARTNER_POS:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0x8

    const-string v3, "PRODUCT_INTENT_PAYROLL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_PAYROLL:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0x9

    const-string v3, "PRODUCT_INTENT_POS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_POS:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0xa

    const-string v3, "PRODUCT_INTENT_RESTAURANTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_RESTAURANTS:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0xb

    const-string v3, "PRODUCT_INTENT_RETAIL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_RETAIL:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0xc

    const-string v3, "PRODUCT_INTENT_SERVICES_POS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_SERVICES_POS:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0xd

    const-string v3, "PRODUCT_INTENT_TERMINAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_TERMINAL:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0xe

    const-string v3, "PRODUCT_INTENT_UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_UNKNOWN:Lcom/squareup/settings/server/ProductIntent;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/settings/server/ProductIntent;

    const-string v2, "PRODUCT_INTENT_VIRTUAL_TERMINAL"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/settings/server/ProductIntent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/settings/server/ProductIntent;->PRODUCT_INTENT_VIRTUAL_TERMINAL:Lcom/squareup/settings/server/ProductIntent;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/settings/server/ProductIntent;->$VALUES:[Lcom/squareup/settings/server/ProductIntent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/server/ProductIntent;
    .locals 1

    const-class v0, Lcom/squareup/settings/server/ProductIntent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/server/ProductIntent;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/server/ProductIntent;
    .locals 1

    sget-object v0, Lcom/squareup/settings/server/ProductIntent;->$VALUES:[Lcom/squareup/settings/server/ProductIntent;

    invoke-virtual {v0}, [Lcom/squareup/settings/server/ProductIntent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/server/ProductIntent;

    return-object v0
.end method
