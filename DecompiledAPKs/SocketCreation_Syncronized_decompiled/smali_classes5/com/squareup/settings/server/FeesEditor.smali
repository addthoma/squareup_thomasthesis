.class public interface abstract Lcom/squareup/settings/server/FeesEditor;
.super Ljava/lang/Object;
.source "FeesEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;
    }
.end annotation


# virtual methods
.method public abstract deleteTax(Lcom/squareup/shared/catalog/models/CatalogTax;Ljava/lang/Runnable;)V
.end method

.method public abstract getDiscounts()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTaxRules()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTaxes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end method

.method public abstract read(Ljava/lang/Runnable;)V
.end method

.method public abstract setInForeground()V
.end method

.method public abstract writeTax(Lcom/squareup/shared/catalog/models/CatalogTax;Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;)V
.end method
