.class public Lcom/squareup/settings/server/MerchantProfileSettings;
.super Ljava/lang/Object;
.source "MerchantProfileSettings.java"


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final useCuratedImageForReceipt:Z


# direct methods
.method protected constructor <init>(Ljava/lang/Boolean;Lcom/squareup/accountstatus/AccountStatusProvider;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p2, p0, Lcom/squareup/settings/server/MerchantProfileSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    if-eqz p1, :cond_0

    .line 18
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/settings/server/MerchantProfileSettings;->useCuratedImageForReceipt:Z

    return-void
.end method


# virtual methods
.method public set(Z)V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/settings/server/MerchantProfileSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance v1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 32
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_curated_image_for_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 31
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method

.method public shouldUseCuratedImageForReceipt()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/settings/server/MerchantProfileSettings;->useCuratedImageForReceipt:Z

    return v0
.end method
