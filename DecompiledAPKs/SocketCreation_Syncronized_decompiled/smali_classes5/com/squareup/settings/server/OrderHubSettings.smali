.class public final Lcom/squareup/settings/server/OrderHubSettings;
.super Ljava/lang/Object;
.source "OrderHubSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0002\u0008\u0015\u0018\u00002\u00020\u0001B\u0017\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\nR\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\nR\u0011\u0010\u0013\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\nR\u0011\u0010\u0015\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0017\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\nR\u0013\u0010\u0019\u001a\u0004\u0018\u00010\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u0010R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u0010R\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u0010R\u0013\u0010\u001f\u001a\u0004\u0018\u00010\u000e8F\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010\u0010R\u0011\u0010!\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\n\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/settings/server/OrderHubSettings;",
        "",
        "accountStatusResponse",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V",
        "allowBazaarOrders",
        "",
        "getAllowBazaarOrders",
        "()Z",
        "allowOrdersSearch",
        "getAllowOrdersSearch",
        "automaticActionMaxAgeSeconds",
        "",
        "getAutomaticActionMaxAgeSeconds",
        "()Ljava/lang/Long;",
        "canShowQuickActionsSetting",
        "getCanShowQuickActionsSetting",
        "canSupportEcomDeliveryOrders",
        "getCanSupportEcomDeliveryOrders",
        "canSupportUpcomingOrders",
        "getCanSupportUpcomingOrders",
        "hasBazaarOnlineStore",
        "getHasBazaarOnlineStore",
        "orderHubSyncPeriodWithNotifications",
        "getOrderHubSyncPeriodWithNotifications",
        "orderHubSyncPeriodWithoutNotifications",
        "getOrderHubSyncPeriodWithoutNotifications",
        "ordersSearchRange",
        "getOrdersSearchRange",
        "ordersSearchRangeThrottled",
        "getOrdersSearchRangeThrottled",
        "throttleOrdersSearch",
        "getThrottleOrdersSearch",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "accountStatusResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/settings/server/OrderHubSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iput-object p2, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public final getAllowBazaarOrders()Z
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_ALLOW_BAZAAR_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public final getAllowOrdersSearch()Z
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_CAN_SEARCH_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public final getAutomaticActionMaxAgeSeconds()Ljava/lang/Long;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->automatic_action_max_age_seconds:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getCanShowQuickActionsSetting()Z
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SHOW_QUICK_ACTIONS_SETTING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getCanSupportEcomDeliveryOrders()Z
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_ECOM_DELIVERY_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public final getCanSupportUpcomingOrders()Z
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_SUPPORTS_UPCOMING_ORDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public final getHasBazaarOnlineStore()Z
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HAS_BAZAAR_ONLINE_STORE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public final getOrderHubSyncPeriodWithNotifications()Ljava/lang/Long;
    .locals 7

    .line 19
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_with_notifications:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    .line 24
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    move-object v0, v1

    :cond_2
    :goto_1
    return-object v0
.end method

.method public final getOrderHubSyncPeriodWithoutNotifications()Ljava/lang/Long;
    .locals 7

    .line 33
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->order_hub_sync_period_without_notifications:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    .line 38
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    move-object v0, v1

    :cond_2
    :goto_1
    return-object v0
.end method

.method public final getOrdersSearchRange()Ljava/lang/Long;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->default_search_range_millis:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getOrdersSearchRangeThrottled()Ljava/lang/Long;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttled_search_range_millis:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getThrottleOrdersSearch()Z
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/settings/server/OrderHubSettings;->accountStatusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->orderhub:Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;->throttle_order_search:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
