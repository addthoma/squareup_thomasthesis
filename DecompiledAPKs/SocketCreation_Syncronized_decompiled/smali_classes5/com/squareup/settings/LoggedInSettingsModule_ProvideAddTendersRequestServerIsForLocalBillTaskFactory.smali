.class public final Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;
.super Ljava/lang/Object;
.source "LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Lcom/squareup/settings/AddTendersRequestServerIds;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAddTendersRequestServerIsForLocalBillTask(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/settings/LoggedInSettingsModule;->provideAddTendersRequestServerIsForLocalBillTask(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/Gson;

    invoke-static {v0, v1}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;->provideAddTendersRequestServerIsForLocalBillTask(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideAddTendersRequestServerIsForLocalBillTaskFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
