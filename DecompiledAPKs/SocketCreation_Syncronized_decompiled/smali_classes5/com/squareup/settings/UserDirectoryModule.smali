.class public Lcom/squareup/settings/UserDirectoryModule;
.super Ljava/lang/Object;
.source "UserDirectoryModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static provideUserDataDirectory(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 15
    invoke-static {p0, p1}, Lcom/squareup/user/Users;->getUserDirectory(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    return-object p0
.end method
