.class public Lcom/squareup/settings/LoggedInSettingsPreferenceModule;
.super Ljava/lang/Object;
.source "LoggedInSettingsPreferenceModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideRx2UserPreferences(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    invoke-static {p0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object p0

    return-object p0
.end method

.method static provideUserPreferences(Landroid/app/Application;Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 18
    invoke-static {p0, p1}, Lcom/squareup/user/Users;->getUserPreferences(Landroid/app/Application;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method
