.class public interface abstract Lcom/squareup/sku/DuplicateSkuResultScreen$Component;
.super Ljava/lang/Object;
.source "DuplicateSkuResultScreen.kt"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sku/DuplicateSkuResultScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/sku/DuplicateSkuResultScreen$Component;",
        "",
        "duplicateSkuResultCoordinator",
        "Lcom/squareup/sku/DuplicateSkuResultCoordinator;",
        "sku_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract duplicateSkuResultCoordinator()Lcom/squareup/sku/DuplicateSkuResultCoordinator;
.end method
