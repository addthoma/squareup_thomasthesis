.class public final Lcom/squareup/sku/DuplicateSkuResultCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DuplicateSkuResultCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDuplicateSkuResultCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DuplicateSkuResultCoordinator.kt\ncom/squareup/sku/DuplicateSkuResultCoordinator\n*L\n1#1,97:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0016\u0010\u0017\u001a\u00020\u00132\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019H\u0002J\u000c\u0010\u001b\u001a\u00020\u0013*\u00020\u0015H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/sku/DuplicateSkuResultCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "controller",
        "Lcom/squareup/sku/DuplicateSkuResultController;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/sku/DuplicateSkuResultController;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "getController",
        "()Lcom/squareup/sku/DuplicateSkuResultController;",
        "getMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "rowLayoutParams",
        "Landroid/view/ViewGroup$LayoutParams;",
        "variationList",
        "Landroid/widget/LinearLayout;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "configureActionBar",
        "inflateVariationList",
        "variations",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "bindViews",
        "sku_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final controller:Lcom/squareup/sku/DuplicateSkuResultController;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private rowLayoutParams:Landroid/view/ViewGroup$LayoutParams;

.field private variationList:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/sku/DuplicateSkuResultController;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/sku/DuplicateSkuResultController;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "controller"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->controller:Lcom/squareup/sku/DuplicateSkuResultController;

    return-void
.end method

.method public static final synthetic access$configureActionBar(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Landroid/view/View;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->configureActionBar(Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$inflateVariationList(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Ljava/util/List;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->inflateVariationList(Ljava/util/List;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 54
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 55
    sget v0, Lcom/squareup/sku/R$id;->duplicate_sku_result_variation_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->variationList:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final configureActionBar(Landroid/view/View;)V
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 61
    new-instance v2, Lcom/squareup/sku/DuplicateSkuResultCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Landroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 62
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v3, Lcom/squareup/sku/R$string;->multiple_sku_results:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 63
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 65
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final inflateVariationList(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 70
    iget-object v1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->variationList:Landroid/widget/LinearLayout;

    const-string/jumbo v2, "variationList"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v1}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForLayout(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    .line 71
    iget-object v3, v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationPrice:Lcom/squareup/protos/common/Money;

    const-string v4, "row"

    .line 72
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->rowLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    if-nez v4, :cond_1

    const-string v5, "rowLayoutParams"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v4, v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemName:Ljava/lang/String;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 75
    move-object v4, v1

    check-cast v4, Landroid/view/View;

    sget v5, Lcom/squareup/sku/R$string;->multiple_sku_results_variation_row_subtitle:I

    invoke-static {v4, v5}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 76
    iget-object v6, v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationName:Ljava/lang/String;

    check-cast v6, Ljava/lang/CharSequence;

    const-string/jumbo v7, "variation_name"

    invoke-virtual {v5, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 77
    iget-object v6, v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->sku:Ljava/lang/String;

    check-cast v6, Ljava/lang/CharSequence;

    const-string v7, "sku"

    invoke-virtual {v5, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 78
    invoke-virtual {v5}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v5

    .line 74
    invoke-virtual {v1, v5}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    const/4 v5, 0x1

    .line 80
    invoke-virtual {v1, v5}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    if-nez v3, :cond_2

    .line 82
    invoke-virtual {v1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v6, Lcom/squareup/sku/R$string;->variable_price:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 84
    :cond_2
    iget-object v6, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v6, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 86
    :goto_1
    sget v3, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v1, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 87
    invoke-virtual {v1, v5}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 88
    new-instance v3, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;

    invoke-direct {v3, p0, v0}, Lcom/squareup/sku/DuplicateSkuResultCoordinator$inflateVariationList$1;-><init>(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->variationList:Landroid/widget/LinearLayout;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->bindViews(Landroid/view/View;)V

    .line 35
    new-instance v0, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$1;

    iget-object v1, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->controller:Lcom/squareup/sku/DuplicateSkuResultController;

    invoke-direct {v0, v1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$1;-><init>(Lcom/squareup/sku/DuplicateSkuResultController;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 34
    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 39
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 41
    sget v2, Lcom/squareup/marin/R$dimen;->marin_tall_list_row_height:I

    .line 40
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    const/4 v2, -0x1

    .line 39
    invoke-direct {v0, v2, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->rowLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    .line 45
    iget-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->controller:Lcom/squareup/sku/DuplicateSkuResultController;

    invoke-interface {v0}, Lcom/squareup/sku/DuplicateSkuResultController;->variationsWithDuplicateSku()Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/sku/DuplicateSkuResultCoordinator$attach$2;-><init>(Lcom/squareup/sku/DuplicateSkuResultCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/BiConsumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final getController()Lcom/squareup/sku/DuplicateSkuResultController;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->controller:Lcom/squareup/sku/DuplicateSkuResultController;

    return-object v0
.end method

.method public final getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/sku/DuplicateSkuResultCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method
