.class public Lcom/squareup/ui/ReorientingLinearLayout;
.super Landroid/widget/LinearLayout;
.source "ReorientingLinearLayout.java"


# instance fields
.field private preDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ReorientingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance p1, Lcom/squareup/ui/-$$Lambda$ReorientingLinearLayout$hy-OYGo-rVnQ98F8ptmBq-EzLck;

    invoke-direct {p1, p0}, Lcom/squareup/ui/-$$Lambda$ReorientingLinearLayout$hy-OYGo-rVnQ98F8ptmBq-EzLck;-><init>(Lcom/squareup/ui/ReorientingLinearLayout;)V

    iput-object p1, p0, Lcom/squareup/ui/ReorientingLinearLayout;->preDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/ReorientingLinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/ReorientingLinearLayout;->preDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return-void
.end method

.method private textIsTooTall(Landroid/widget/TextView;)Z
    .locals 1

    .line 61
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineCount()I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public synthetic lambda$new$0$ReorientingLinearLayout()Z
    .locals 7

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/ReorientingLinearLayout;->getChildCount()I

    move-result v0

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/ReorientingLinearLayout;->getOrientation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    return v2

    :cond_0
    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    .line 25
    invoke-virtual {p0, v1}, Lcom/squareup/ui/ReorientingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 26
    instance-of v5, v4, Landroid/widget/TextView;

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    goto :goto_1

    .line 27
    :cond_1
    check-cast v4, Landroid/widget/TextView;

    .line 28
    invoke-direct {p0, v4}, Lcom/squareup/ui/ReorientingLinearLayout;->textIsTooTall(Landroid/widget/TextView;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    if-eqz v3, :cond_4

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/ReorientingLinearLayout;->reorientVertical()V

    :cond_4
    xor-int/lit8 v0, v3, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 56
    invoke-virtual {p0}, Lcom/squareup/ui/ReorientingLinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ReorientingLinearLayout;->preDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public reorientVertical()V
    .locals 5

    const/4 v0, 0x1

    .line 42
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ReorientingLinearLayout;->setOrientation(I)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/ReorientingLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 45
    invoke-virtual {p0, v2}, Lcom/squareup/ui/ReorientingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 46
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    .line 47
    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 48
    iput v1, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iput v1, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/ReorientingLinearLayout;->requestLayout()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
