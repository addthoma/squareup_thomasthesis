.class public interface abstract Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;
.super Ljava/lang/Object;
.source "NfcProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/NfcProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NfcErrorHandler"
.end annotation


# virtual methods
.method public abstract handleActionRequired()V
.end method

.method public abstract handleCardBlocked()V
.end method

.method public abstract handleCardDeclined()V
.end method

.method public abstract handleCardTapAgain()V
.end method

.method public abstract handleCollisionDetected()V
.end method

.method public abstract handleInterfaceUnavailable()V
.end method

.method public abstract handleLimitExceededInsertCard()V
.end method

.method public abstract handleLimitExceededTryAnotherCard()V
.end method

.method public abstract handleOnRequestTapCard()V
.end method

.method public abstract handleProcessingError()V
.end method

.method public abstract handleTryAnotherCard()V
.end method

.method public abstract handleUnlockDevice()V
.end method
