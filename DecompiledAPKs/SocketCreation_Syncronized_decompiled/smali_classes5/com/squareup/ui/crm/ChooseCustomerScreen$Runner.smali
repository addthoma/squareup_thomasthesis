.class public interface abstract Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;
.super Ljava/lang/Object;
.source "ChooseCustomerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/ChooseCustomerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract actionBarGlyphIsX()Ljava/lang/Boolean;
.end method

.method public abstract cancelChooseCustomer()V
.end method

.method public abstract getCustomerListItems()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/BaseCustomerListItem;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getScreenLoadingState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getScrollPosition()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSearchTerm()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract onNearEndOfListChanged(Ljava/lang/Boolean;)V
.end method

.method public abstract setContactListScrollPosition(I)V
.end method

.method public abstract setSearchTerm(Ljava/lang/String;)V
.end method

.method public abstract showCreateCustomerScreen()V
.end method
