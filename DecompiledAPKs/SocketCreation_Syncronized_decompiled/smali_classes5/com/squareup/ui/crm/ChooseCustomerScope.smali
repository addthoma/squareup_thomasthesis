.class public Lcom/squareup/ui/crm/ChooseCustomerScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ChooseCustomerScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/ChooseCustomerScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/ChooseCustomerScope$Component;,
        Lcom/squareup/ui/crm/ChooseCustomerScope$Module;,
        Lcom/squareup/ui/crm/ChooseCustomerScope$ParentComponent;,
        Lcom/squareup/ui/crm/ChooseCustomerScope$ChooseCustomerRecentRolodexContactLoader;,
        Lcom/squareup/ui/crm/ChooseCustomerScope$ChooseCustomerRolodexContactLoader;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/ChooseCustomerScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field public final createNewCustomerValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

.field public final crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public final isFirstCardScreen:Z

.field public final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field public final sortByRecentContacts:Z

.field public final titleResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 140
    sget-object v0, Lcom/squareup/ui/crm/-$$Lambda$ChooseCustomerScope$VDCxuA3Tmv6J9Ds3xjuMbZPnLSI;->INSTANCE:Lcom/squareup/ui/crm/-$$Lambda$ChooseCustomerScope$VDCxuA3Tmv6J9Ds3xjuMbZPnLSI;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    .line 64
    iput p3, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->titleResId:I

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->createNewCustomerValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    .line 66
    iput-object p5, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 67
    iput-boolean p6, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->isFirstCardScreen:Z

    .line 68
    iput-boolean p7, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->sortByRecentContacts:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/ChooseCustomerScope;
    .locals 10

    .line 141
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 143
    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerScope;

    .line 145
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lcom/squareup/ui/crm/ChooseCustomerFlowKt;->toChooseCustomerResultKey(I)Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object v3

    .line 146
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 147
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlowKt;->toContactValidationType(I)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    move-result-object v5

    .line 148
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->toCrmScopeType(I)Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v6

    .line 149
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-ne v1, v8, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    .line 150
    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    if-ne p0, v8, :cond_1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    move-object v1, v0

    move v7, v9

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/crm/ChooseCustomerScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 129
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 132
    iget-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    invoke-static {p2}, Lcom/squareup/ui/crm/ChooseCustomerFlowKt;->toInt(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->titleResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    iget-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->createNewCustomerValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    invoke-static {p2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlowKt;->toInt(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {p2}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->toInt(Lcom/squareup/ui/crm/flow/CrmScopeType;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-boolean p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->isFirstCardScreen:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget-boolean p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->sortByRecentContacts:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 124
    const-class v0, Lcom/squareup/ui/crm/ChooseCustomerScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/ChooseCustomerScope$Component;

    .line 125
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    invoke-interface {v0}, Lcom/squareup/ui/crm/ChooseCustomerScope$Component;->runner()Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
