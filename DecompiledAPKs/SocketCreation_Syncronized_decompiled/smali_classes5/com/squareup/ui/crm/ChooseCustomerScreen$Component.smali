.class public interface abstract Lcom/squareup/ui/crm/ChooseCustomerScreen$Component;
.super Ljava/lang/Object;
.source "ChooseCustomerScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/ChooseCustomerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract coordinator()Lcom/squareup/ui/crm/ChooseCustomerCoordinator;
.end method

.method public abstract runner()Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;
.end method
