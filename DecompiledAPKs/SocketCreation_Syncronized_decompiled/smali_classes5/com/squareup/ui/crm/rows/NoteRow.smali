.class public Lcom/squareup/ui/crm/rows/NoteRow;
.super Landroid/widget/LinearLayout;
.source "NoteRow.java"


# instance fields
.field private final creatorTimestamp:Lcom/squareup/widgets/MessageView;

.field private final note:Landroid/view/View;

.field private final noteText:Landroid/widget/TextView;

.field private final reminder:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 21
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 22
    sget v0, Lcom/squareup/crm/R$layout;->crm_note_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/rows/NoteRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 24
    sget p1, Lcom/squareup/crm/R$id;->crm_note_row_note:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/NoteRow;->noteText:Landroid/widget/TextView;

    .line 25
    sget p1, Lcom/squareup/crm/R$id;->crm_note_row_creator_timestamp:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/NoteRow;->creatorTimestamp:Lcom/squareup/widgets/MessageView;

    .line 26
    sget p1, Lcom/squareup/crm/R$id;->crm_note_row_contents:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/NoteRow;->note:Landroid/view/View;

    .line 27
    sget p1, Lcom/squareup/crm/R$id;->crm_note_row_reminder:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/NoteRow;->reminder:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public setNoteOnClick(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/NoteRow;->note:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public showCreatorTimestamp(Ljava/lang/String;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/NoteRow;->creatorTimestamp:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showNote(Ljava/lang/String;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/NoteRow;->noteText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showReminder()V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/NoteRow;->reminder:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method
