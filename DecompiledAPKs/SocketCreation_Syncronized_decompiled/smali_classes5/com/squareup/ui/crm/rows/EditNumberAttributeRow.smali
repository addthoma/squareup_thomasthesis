.class public Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;
.super Lcom/squareup/marketfont/MarketEditText;
.source "EditNumberAttributeRow.java"

# interfaces
.implements Lcom/squareup/ui/crm/rows/HasAttribute;


# instance fields
.field private attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

.field private onTextChanged:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 3

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 49
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexContactHelper;->toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    .line 51
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number_text(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v0

    .line 50
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 26
    invoke-super {p0}, Lcom/squareup/marketfont/MarketEditText;->onFinishInflate()V

    .line 27
    invoke-static {p0}, Lcom/squareup/util/RxViews;->debouncedOnChanged(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->onTextChanged:Lrx/Observable;

    return-void
.end method

.method public onTextChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->onTextChanged:Lrx/Observable;

    return-object v0
.end method

.method public showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 1

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    .line 33
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->setHint(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 34
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->setText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    const/16 p2, 0x8

    :goto_1
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->setVisibility(I)V

    .line 36
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/EditNumberAttributeRow;->setFocusable(Z)V

    return-void
.end method
