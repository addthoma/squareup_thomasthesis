.class public final Lcom/squareup/ui/crm/BaseCustomerListItem$ErrorItem;
.super Lcom/squareup/ui/crm/BaseCustomerListItem;
.source "BaseCustomerListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/BaseCustomerListItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ErrorItem"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/crm/BaseCustomerListItem$ErrorItem;",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "()V",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/BaseCustomerListItem;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
