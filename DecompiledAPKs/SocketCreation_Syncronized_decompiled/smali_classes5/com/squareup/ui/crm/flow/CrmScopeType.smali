.class public final enum Lcom/squareup/ui/crm/flow/CrmScopeType;
.super Ljava/lang/Enum;
.source "CrmScopeType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/flow/CrmScopeType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0019\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/crm/flow/CrmScopeType;",
        "",
        "(Ljava/lang/String;I)V",
        "ADD_CARD_TO_CUSTOMER",
        "ADD_CUSTOMER_TO_APPOINTMENT",
        "ADD_CUSTOMER_TO_INVOICE",
        "ADD_CUSTOMER_TO_ESTIMATE",
        "ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET",
        "ADD_CUSTOMER_TO_SALE_IN_TRANSACTION",
        "ADD_CUSTOMER_TO_SALE_POST_TRANSACTION",
        "ADD_CUSTOMER_TO_SALE_IN_RETAIL_HOME_APPLET",
        "CREATE_CUSTOMER_FOR_INVOICE",
        "VIEW_CUSTOMER_PROFILE_IN_APPLET",
        "VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD",
        "VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET",
        "VIEW_CUSTOMER_ADDED_TO_APPOINTMENT",
        "VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE",
        "VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE",
        "VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL",
        "VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET",
        "VIEW_CUSTOMER_ADDED_TO_SALE_IN_TRANSACTION",
        "VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION",
        "X2_ADD_CUSTOMER_TO_SALE_POST_TRANSACTION",
        "X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION",
        "SAVE_CARD_POST_TRANSACTION",
        "X2_SAVE_CARD_POST_TRANSACTION",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CARD_TO_CUSTOMER:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CUSTOMER_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CUSTOMER_TO_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CUSTOMER_TO_SALE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CUSTOMER_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum CREATE_CUSTOMER_FOR_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_ADDED_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_ADDED_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum X2_ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum X2_SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public static final enum X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/squareup/ui/crm/flow/CrmScopeType;

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x0

    const-string v3, "ADD_CARD_TO_CUSTOMER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CARD_TO_CUSTOMER:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x1

    const-string v3, "ADD_CUSTOMER_TO_APPOINTMENT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x2

    const-string v3, "ADD_CUSTOMER_TO_INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x3

    const-string v3, "ADD_CUSTOMER_TO_ESTIMATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x4

    const-string v3, "ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x5

    const-string v3, "ADD_CUSTOMER_TO_SALE_IN_TRANSACTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x6

    const-string v3, "ADD_CUSTOMER_TO_SALE_POST_TRANSACTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v2, 0x7

    const-string v3, "ADD_CUSTOMER_TO_SALE_IN_RETAIL_HOME_APPLET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x8

    const-string v3, "CREATE_CUSTOMER_FOR_INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->CREATE_CUSTOMER_FOR_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x9

    const-string v3, "VIEW_CUSTOMER_PROFILE_IN_APPLET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0xa

    const-string v3, "VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0xb

    const-string v3, "VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0xc

    const-string v3, "VIEW_CUSTOMER_ADDED_TO_APPOINTMENT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0xd

    const-string v3, "VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0xe

    const-string v3, "VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "VIEW_CUSTOMER_ADDED_TO_SALE_IN_TRANSACTION"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "X2_ADD_CUSTOMER_TO_SALE_POST_TRANSACTION"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION"

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "SAVE_CARD_POST_TRANSACTION"

    const/16 v3, 0x15

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/crm/flow/CrmScopeType;

    const-string v2, "X2_SAVE_CARD_POST_TRANSACTION"

    const/16 v3, 0x16

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScopeType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->$VALUES:[Lcom/squareup/ui/crm/flow/CrmScopeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/flow/CrmScopeType;
    .locals 1

    const-class v0, Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/CrmScopeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/flow/CrmScopeType;
    .locals 1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->$VALUES:[Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/flow/CrmScopeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/flow/CrmScopeType;

    return-object v0
.end method
