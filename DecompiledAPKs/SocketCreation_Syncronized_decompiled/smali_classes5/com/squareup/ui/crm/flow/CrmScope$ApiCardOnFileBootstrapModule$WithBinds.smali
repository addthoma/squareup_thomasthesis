.class public abstract Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule$WithBinds;
.super Ljava/lang/Object;
.source "CrmScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "WithBinds"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;)V
    .locals 0

    .line 1043
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule$WithBinds;->this$0:Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract baseRunner(Lcom/squareup/ui/crm/flow/ReaderSdkCrmRunner;)Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideDippedCardSpinnerScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSaveCardCustomerEmailScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSaveCardSpinnerScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSaveCardVerifyPostalCodeRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract saveCardScreenRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
