.class public final Lcom/squareup/ui/crm/flow/UpdateCustomerModule;
.super Ljava/lang/Object;
.source "UpdateCustomerModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0001\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/crm/flow/UpdateCustomerModule;",
        "",
        "()V",
        "bindUpdateCustomerFlow",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "updateFlow",
        "Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;",
        "editFlow",
        "Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;",
        "crm-update-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/crm/flow/UpdateCustomerModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule;

    invoke-direct {v0}, Lcom/squareup/ui/crm/flow/UpdateCustomerModule;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule;->INSTANCE:Lcom/squareup/ui/crm/flow/UpdateCustomerModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final bindUpdateCustomerFlow(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "features"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateFlow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editFlow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 20
    check-cast p2, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    goto :goto_0

    .line 22
    :cond_0
    move-object p2, p1

    check-cast p2, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    :goto_0
    return-object p2
.end method
