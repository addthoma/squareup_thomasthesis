.class public abstract Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Module;
.super Ljava/lang/Object;
.source "UpdateCustomerScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/UpdateCustomerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Module;->this$0:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract ChooseDateAttributeDialogScreenRunner(Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;)Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract ChooseEnumAttributeScreenV2Runner(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract UpdateCustomerScreenV2Runner(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideChooseGroupsScreenRunner(Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;)Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCreateGroupScreenRunner(Lcom/squareup/ui/crm/flow/CreateGroupFlow;)Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePickAddressBookContactScreen(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
