.class public final enum Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;
.super Ljava/lang/Enum;
.source "CofDippedCardInfoProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

.field public static final enum CANCELED:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

.field public static final enum FAILURE:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

.field public static final enum SUCCESS:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 52
    new-instance v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    const/4 v1, 0x0

    const-string v2, "CANCELED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->CANCELED:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    new-instance v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->SUCCESS:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    new-instance v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    const/4 v3, 0x2

    const-string v4, "FAILURE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->FAILURE:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    sget-object v4, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->CANCELED:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->SUCCESS:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->FAILURE:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->$VALUES:[Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;
    .locals 1

    .line 52
    const-class v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->$VALUES:[Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    return-object v0
.end method
