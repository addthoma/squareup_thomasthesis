.class public Lcom/squareup/ui/crm/applet/CustomersApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "CustomersApplet.java"


# static fields
.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "CUSTOMERS"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/crm/applet/CustomersApplet;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    const-string v0, "customers"

    return-object v0
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 50
    sget-object p1, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getInitialDetailScreen()Lflow/path/Path;
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;

    return-object v0
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "CUSTOMERS"

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 54
    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 46
    sget v0, Lcom/squareup/crm/applet/R$string;->crm_titlecase_customers:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public visibility()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_APPLET_TABLET:Lcom/squareup/settings/server/Features$Feature;

    .line 60
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 59
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
