.class public final Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;
.super Ljava/lang/Object;
.source "CustomersApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/applet/CustomersApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/crm/applet/CustomersApplet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/crm/applet/CustomersApplet;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/applet/CustomersApplet;-><init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/applet/CustomersApplet;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/crm/applet/CustomersApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersApplet_Factory;->get()Lcom/squareup/ui/crm/applet/CustomersApplet;

    move-result-object v0

    return-object v0
.end method
