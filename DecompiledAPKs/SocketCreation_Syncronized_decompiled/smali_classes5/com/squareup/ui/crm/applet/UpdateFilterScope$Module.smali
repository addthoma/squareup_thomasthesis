.class public abstract Lcom/squareup/ui/crm/applet/UpdateFilterScope$Module;
.super Ljava/lang/Object;
.source "UpdateFilterScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/UpdateFilterScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract editFilter(Lcom/squareup/ui/crm/flow/UpdateFilterFlow;)Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
