.class public Lcom/squareup/ui/crm/applet/UpdateFilterScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "UpdateFilterScope.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/UpdateFilterScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/UpdateFilterScope$Component;,
        Lcom/squareup/ui/crm/applet/UpdateFilterScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/UpdateFilterScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/ui/crm/applet/-$$Lambda$UpdateFilterScope$hhXTa8zNsnypAbBeIfEAEdn1Wb8;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$UpdateFilterScope$hhXTa8zNsnypAbBeIfEAEdn1Wb8;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/UpdateFilterScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/crm/applet/UpdateFilterScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/applet/UpdateFilterScope;
    .locals 1

    .line 42
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 43
    new-instance v0, Lcom/squareup/ui/crm/applet/UpdateFilterScope;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/applet/UpdateFilterScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 37
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/UpdateFilterScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/UpdateFilterScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method
