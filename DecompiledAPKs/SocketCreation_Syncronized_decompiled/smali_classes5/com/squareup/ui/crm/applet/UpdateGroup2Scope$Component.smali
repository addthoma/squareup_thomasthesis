.class public interface abstract Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Component;
.super Ljava/lang/Object;
.source "UpdateGroup2Scope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract createFilterFlow()Lcom/squareup/ui/crm/flow/CreateFilterFlow;
.end method

.method public abstract createFilterScope()Lcom/squareup/ui/crm/applet/CreateFilterScope$Component;
.end method

.method public abstract updateFilterFlow()Lcom/squareup/ui/crm/flow/UpdateFilterFlow;
.end method

.method public abstract updateFilterScope()Lcom/squareup/ui/crm/applet/UpdateFilterScope$Component;
.end method

.method public abstract updateGroup2Flow()Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;
.end method

.method public abstract updateGroup2Screen()Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Component;
.end method
