.class public Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;
.super Landroid/widget/LinearLayout;
.source "CardOnFileSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;
    }
.end annotation


# instance fields
.field private final rows:Landroid/widget/LinearLayout;

.field private final sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

.field private final unlinkInstrument:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->unlinkInstrument:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 46
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_profile_section_card_on_file:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->setOrientation(I)V

    .line 49
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_section_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->uppercase_card_on_file:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 51
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_section_rows:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->rows:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method public addCardClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->onActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setViewData$0$CardOnFileSectionView(Lcom/squareup/ui/crm/rows/CardOnFileRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CardOnFileRow;->onUnlinkClicked()Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->unlinkInstrument:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 78
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public setViewData(Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;)V
    .locals 3

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_cardonfile_addcard_button:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;->headerActionButtonState:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 69
    iget-boolean v0, p1, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;->visible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 70
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;->lineData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    .line 73
    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView$ViewData;->lineData:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;

    .line 74
    new-instance v1, Lcom/squareup/ui/crm/rows/CardOnFileRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/rows/CardOnFileRow;-><init>(Landroid/content/Context;)V

    .line 75
    invoke-virtual {v1, v0}, Lcom/squareup/ui/crm/rows/CardOnFileRow;->setViewData(Lcom/squareup/ui/crm/rows/CardOnFileRow$ViewData;)V

    .line 76
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$CardOnFileSectionView$BQ7uHb_MgSQ4avjVOuP_RdXV6O4;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$CardOnFileSectionView$BQ7uHb_MgSQ4avjVOuP_RdXV6O4;-><init>(Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;Lcom/squareup/ui/crm/rows/CardOnFileRow;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    .line 82
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public unlinkInstrumentClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/CardOnFileSectionView;->unlinkInstrument:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method
