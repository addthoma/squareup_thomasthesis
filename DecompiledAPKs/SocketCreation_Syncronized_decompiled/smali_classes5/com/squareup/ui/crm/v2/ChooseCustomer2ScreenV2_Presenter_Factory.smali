.class public final Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;
.super Ljava/lang/Object;
.source "ChooseCustomer2ScreenV2_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final recentContactsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexRecentContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexRecentContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->recentContactsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexRecentContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->recentContactsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/crm/RolodexRecentContactLoader;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->newInstance(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2_Presenter_Factory;->get()Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method
