.class Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;
.super Ljava/lang/Object;
.source "ViewGroupsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Item"
.end annotation


# instance fields
.field final group:Lcom/squareup/protos/client/rolodex/Group;

.field final name:Ljava/lang/String;

.field final type:I


# direct methods
.method constructor <init>(ILjava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;->type:I

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;->name:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;->group:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method
