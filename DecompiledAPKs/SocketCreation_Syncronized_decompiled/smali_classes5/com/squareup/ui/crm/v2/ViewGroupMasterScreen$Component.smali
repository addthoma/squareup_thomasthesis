.class public interface abstract Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen$Component;
.super Ljava/lang/Object;
.source "ViewGroupMasterScreen.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$Component;
.implements Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;
.implements Lcom/squareup/ui/crm/v2/FiltersLayout$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewGroupMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract coordinator()Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;
.end method
