.class public interface abstract Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;
.super Ljava/lang/Object;
.source "CreateManualGroupCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelCreateManualGroup()V
.end method

.method public abstract isFirstGroup()Z
.end method

.method public abstract onCreateGroupError()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract saveManualGroup(Ljava/lang/String;)V
.end method
