.class public final Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 RewardRecyclerViewHelper.kt\ncom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,87:1\n71#2,22:88\n93#2,13:117\n107#2:137\n108#2,4:145\n1103#3,7:110\n1103#3,7:130\n1103#3,7:138\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$bind$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $counterView$inlined:Lcom/squareup/loyalty/ui/CounterView;

.field final synthetic $redeemText$inlined:Landroid/widget/TextView;

.field final synthetic $subtitle$inlined:Landroid/widget/TextView;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic $title$inlined:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/squareup/loyalty/ui/CounterView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$title$inlined:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$subtitle$inlined:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$redeemText$inlined:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$counterView$inlined:Lcom/squareup/loyalty/ui/CounterView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$title$inlined:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$subtitle$inlined:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getFormattedPoints()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$redeemText$inlined:Landroid/widget/TextView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$counterView$inlined:Lcom/squareup/loyalty/ui/CounterView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$counterView$inlined:Lcom/squareup/loyalty/ui/CounterView;

    .line 99
    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v1, p1, 0x1

    goto :goto_0

    :cond_0
    move v1, p1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move v1, p1

    .line 97
    invoke-static/range {v0 .. v5}, Lcom/squareup/loyalty/ui/CounterView;->update$default(Lcom/squareup/loyalty/ui/CounterView;ILjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Z

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    .line 110
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$1;

    invoke-direct {v1}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$1;-><init>()V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$counterView$inlined:Lcom/squareup/loyalty/ui/CounterView;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;

    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$2;-><init>(ILcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/ui/CounterView;->setCountChangeListener(Lkotlin/jvm/functions/Function1;)V

    goto :goto_2

    .line 126
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$redeemText$inlined:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getEnabled()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$counterView$inlined:Lcom/squareup/loyalty/ui/CounterView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 128
    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    .line 130
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$3;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$3;-><init>(Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 137
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    .line 138
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$4;

    invoke-direct {p2}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1$4;-><init>()V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$$special$$inlined$create$1$lambda$1;->$counterView$inlined:Lcom/squareup/loyalty/ui/CounterView;

    sget-object p2, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$createRecyclerFor$1$1$1$1$5;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper$createRecyclerFor$1$1$1$1$5;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/loyalty/ui/CounterView;->setCountChangeListener(Lkotlin/jvm/functions/Function1;)V

    :goto_2
    return-void
.end method
