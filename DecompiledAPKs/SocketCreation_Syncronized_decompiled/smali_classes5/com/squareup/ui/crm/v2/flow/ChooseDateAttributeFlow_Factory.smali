.class public final Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;
.super Ljava/lang/Object;
.source "ChooseDateAttributeFlow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Ljava/util/Locale;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;-><init>(Lflow/Flow;Ljava/util/Locale;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->newInstance(Lflow/Flow;Ljava/util/Locale;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow_Factory;->get()Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

    move-result-object v0

    return-object v0
.end method
