.class Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ExpiringPointsCoordinator.java"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;Landroid/view/ViewGroup;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;

    .line 102
    sget p1, Lcom/squareup/crmscreens/R$layout;->crm_v2_expiring_points_row:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getEdges()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 0

    .line 110
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method
