.class public final Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ViewCustomerCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CUSTOMER_PROFILE_CARD:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;

    .line 49
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;->coordinator()Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 57
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_view_customer:I

    return v0
.end method
