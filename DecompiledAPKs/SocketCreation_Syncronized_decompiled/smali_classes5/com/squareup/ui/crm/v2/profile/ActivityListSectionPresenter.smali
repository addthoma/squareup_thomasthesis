.class Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;
.super Lmortar/ViewPresenter;
.source "ActivityListSectionPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SHOW_VIEW_ALL_ACTIVITY_SIZE:Ljava/lang/Integer;


# instance fields
.field private final contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final eventLoader:Lcom/squareup/crm/RolodexEventLoader;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final onConversationClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final onPaymentClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final resultsForContact:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x7

    .line 65
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->SHOW_VIEW_ALL_ACTIVITY_SIZE:Ljava/lang/Integer;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerActivityHelper;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexEventLoader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 70
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 56
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 58
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onConversationClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 59
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onPaymentClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    .line 72
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    .line 73
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->dateFormatter:Ljava/text/DateFormat;

    .line 74
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 75
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->locale:Ljava/util/Locale;

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->features:Lcom/squareup/settings/server/Features;

    const/16 p1, 0x14

    .line 80
    invoke-virtual {p6, p1}, Lcom/squareup/crm/RolodexEventLoader;->setDefaultPageSize(I)V

    .line 85
    invoke-virtual {p6}, Lcom/squareup/crm/RolodexEventLoader;->results()Lio/reactivex/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$clQ3J5RnjgON0RIhMYR1ZslxbPI;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$clQ3J5RnjgON0RIhMYR1ZslxbPI;

    .line 84
    invoke-static {p1, p2, p3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    .line 90
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->resultsForContact:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onConversationClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onPaymentClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/squareup/ui/crm/cards/CustomerActivityHelper;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    return-object p0
.end method

.method private bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Event;)V
    .locals 2

    .line 147
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 148
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 150
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->getDateString(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 152
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTag(Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->isActivityClickable(Lcom/squareup/protos/client/rolodex/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setClickable(Z)V

    .line 156
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 157
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;-><init>(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;Lcom/squareup/protos/client/rolodex/Event;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 169
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setClickable(Z)V

    .line 170
    sget-object p2, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    :goto_0
    return-void
.end method

.method private getDateString(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 3

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->timeFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->locale:Ljava/util/Locale;

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/crm/DateTimeFormatHelper;->formatDateForTransaction(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$new$0(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 87
    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getInput()Ljava/lang/Object;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 88
    invoke-static {p0}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p0

    goto :goto_0

    .line 89
    :cond_0
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private onEvents(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;)V"
        }
    .end annotation

    .line 137
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->clearRows()V

    .line 139
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Event;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->addRow()Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Event;)V

    goto :goto_0

    .line 143
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->setVisible(Z)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$2$ActivityListSectionPresenter(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 111
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 113
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onEvents(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;Ljava/util/List;)V

    goto :goto_0

    .line 118
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object p2

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    .line 119
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    sget-object v2, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->SHOW_VIEW_ALL_ACTIVITY_SIZE:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gt v1, v2, :cond_1

    .line 120
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onEvents(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;Ljava/util/List;)V

    goto :goto_0

    .line 122
    :cond_1
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->SHOW_VIEW_ALL_ACTIVITY_SIZE:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onEvents(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;Ljava/util/List;)V

    .line 123
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->showViewAll()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$ActivityListSectionPresenter(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/RolodexEventLoader;->setContactToken(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$ActivityListSectionPresenter(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->resultsForContact:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$dFq2Cw3aCQzr6TQt3CeKG9ZQgEw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$dFq2Cw3aCQzr6TQt3CeKG9ZQgEw;-><init>(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;)V

    .line 110
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method onConversationClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onConversationClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 94
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$3aSXVs-FHCmx8XYd_y-TCnko9Sc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$3aSXVs-FHCmx8XYd_y-TCnko9Sc;-><init>(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)V

    .line 99
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 97
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 104
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$fmHqbJ59KX4EH_j9I__oNoFPcB8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ActivityListSectionPresenter$fmHqbJ59KX4EH_j9I__oNoFPcB8;-><init>(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method onPaymentClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;",
            ">;"
        }
    .end annotation

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onPaymentClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->contacts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
