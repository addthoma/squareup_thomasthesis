.class Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ExpiringPointsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExpiringPointsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private screenData:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 82
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->screenData:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->screenData:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->getSize()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->onBindViewHolder(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;I)V
    .locals 3

    .line 90
    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/crmscreens/R$id;->crm_expiring_points_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoTitleValueRow;

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->screenData:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->getRowTitle(ILjava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoTitleValueRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->access$100(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Lcom/squareup/loyalty/PointsTermsFormatter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->screenData:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    invoke-virtual {v1, p2}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->getRowValue(I)J

    move-result-wide v1

    sget p2, Lcom/squareup/crmscreens/R$string;->crm_loyalty_program_section_view_expiring_points_value:I

    invoke-virtual {v0, v1, v2, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoTitleValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;
    .locals 0

    .line 86
    new-instance p2, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter$ViewHolder;-><init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;Landroid/view/ViewGroup;)V

    return-object p2
.end method

.method updateView(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;)V
    .locals 2

    .line 115
    iget-object v0, p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->errorMessage:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->access$200(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->access$200(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->expirationPolicy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->screenData:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->access$300(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->loading:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->access$400(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-boolean p1, p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->empty:Z

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->notifyDataSetChanged()V

    return-void
.end method
