.class public Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;
.super Landroid/widget/LinearLayout;
.source "ActivityListSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;,
        Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$Component;
    }
.end annotation


# instance fields
.field private final onViewAllClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final rows:Landroid/widget/LinearLayout;

.field private final sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

.field private final viewAll:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const-class p2, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$Component;->inject(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;)V

    .line 52
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_activity_list_section:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 53
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->setOrientation(I)V

    .line 55
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_section_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_activity_list_header_uppercase_v2:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    sget-object p2, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 60
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_section_rows:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->rows:Landroid/widget/LinearLayout;

    .line 61
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_bottom_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->viewAll:Landroid/view/View;

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->viewAll:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->onViewAllClicked:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method addRow()Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 3

    .line 107
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_profile_smartlinerow:I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 108
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 109
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    return-object v0
.end method

.method clearRows()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 67
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onConversationClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onConversationClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->dropView(Ljava/lang/Object;)V

    .line 73
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onPaymentClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;",
            ">;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->onPaymentClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 78
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onViewAllClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->onViewAllClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method setVisible(Z)V
    .locals 0

    .line 98
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showViewAll()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView;->viewAll:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
