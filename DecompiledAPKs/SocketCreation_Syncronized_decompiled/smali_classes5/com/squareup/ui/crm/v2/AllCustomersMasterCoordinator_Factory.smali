.class public final Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;
.super Ljava/lang/Object;
.source "AllCustomersMasterCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final contactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final groupLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final mergeProposalLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->mergeProposalLoaderProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;"
        }
    .end annotation

    .line 84
    new-instance v12, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/BadgePresenter;)Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;
    .locals 13

    .line 92
    new-instance v12, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/BadgePresenter;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/crm/RolodexContactLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/crm/RolodexGroupLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->mergeProposalLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/crm/MergeProposalLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/applet/AppletSelection;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/applet/BadgePresenter;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/BadgePresenter;)Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator_Factory;->get()Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;

    move-result-object v0

    return-object v0
.end method
