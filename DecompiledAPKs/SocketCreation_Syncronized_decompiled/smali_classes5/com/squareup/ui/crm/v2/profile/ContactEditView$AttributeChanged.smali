.class Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;
.super Ljava/lang/Object;
.source "ContactEditView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ContactEditView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AttributeChanged"
.end annotation


# instance fields
.field final attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

.field final attributeKey:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;->attributeKey:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$AttributeChanged;->attribute:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-void
.end method
