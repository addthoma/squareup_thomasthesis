.class public final Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ManageCouponsAndRewardsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;,
        Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;,
        Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;,
        Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 76
    const-class v0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;

    .line 77
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;->coordinator()Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 81
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_v2_manage_coupons_and_rewards_screen:I

    return v0
.end method
