.class Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ActivityListSectionPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Event;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

.field final synthetic val$event:Lcom/squareup/protos/client/rolodex/Event;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;Lcom/squareup/protos/client/rolodex/Event;)V
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->access$000(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/squareup/settings/server/Features;

    move-result-object p1

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/EventType;->FEEDBACK_CONVERSATION:Lcom/squareup/protos/client/rolodex/EventType;

    if-ne p1, v0, :cond_0

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->access$100(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 161
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/EventType;->PAYMENT:Lcom/squareup/protos/client/rolodex/EventType;

    if-ne p1, v0, :cond_1

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->access$200(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionView$PaymentDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 164
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->this$0:Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;->access$300(Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;)Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter$1;->val$event:Lcom/squareup/protos/client/rolodex/Event;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->onActivityClicked(Lcom/squareup/protos/client/rolodex/Event;)V

    :goto_0
    return-void
.end method
