.class public final Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;
.super Ljava/lang/Object;
.source "PersonalInformationViewDataRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPersonalInformationViewDataRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PersonalInformationViewDataRenderer.kt\ncom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,363:1\n1642#2,2:364\n1360#2:366\n1429#2,3:367\n704#2:370\n777#2,2:371\n1642#2,2:373\n*E\n*S KotlinDebug\n*F\n+ 1 PersonalInformationViewDataRenderer.kt\ncom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer\n*L\n151#1,2:364\n238#1:366\n238#1,3:367\n282#1:370\n282#1,2:371\n283#1,2:373\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B_\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\r\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u001e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0016\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0014\u0010!\u001a\u0004\u0018\u00010\"2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u0002J\u0018\u0010%\u001a\u00020\"2\u000e\u0010&\u001a\n\u0012\u0004\u0012\u00020\"\u0018\u00010\u001aH\u0002J\u0016\u0010\'\u001a\u0004\u0018\u00010\"2\n\u0010(\u001a\u0006\u0012\u0002\u0008\u00030)H\u0002J\u0012\u0010\'\u001a\u0004\u0018\u00010\"2\u0006\u0010(\u001a\u00020*H\u0002J\u0008\u0010+\u001a\u00020\rH\u0002J\u001c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020.0-2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010/\u001a\u00020\rJ\"\u00100\u001a\u00020.2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010/\u001a\u00020\r2\n\u0008\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001fJ\u001a\u00101\u001a\u000202*\u0002032\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u00106\u001a\u000202*\u0002032\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u00107\u001a\u000202*\u0002032\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u00108\u001a\u000202*\u00020*2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J!\u00109\u001a\u0004\u0018\u000102*\u00020\u001d2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002\u00a2\u0006\u0002\u0010:J\u001e\u0010;\u001a\u000202*\u0006\u0012\u0002\u0008\u00030)2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u0010<\u001a\u000202*\u0002032\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u0010=\u001a\u000202*\u00020\u001d2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u0010>\u001a\u000202*\u00020\u001d2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J0\u0010?\u001a\u000202*\u0008\u0012\u0004\u0012\u00020\u001b052\u0008\u0008\u0001\u0010@\u001a\u00020A2\u0008\u0010B\u001a\u0004\u0018\u00010\"2\u0008\u0008\u0002\u0010C\u001a\u00020DH\u0002J.\u0010?\u001a\u000202*\u0008\u0012\u0004\u0012\u00020\u001b052\u0006\u0010@\u001a\u00020\"2\u0008\u0010B\u001a\u0004\u0018\u00010\"2\u0008\u0008\u0002\u0010C\u001a\u00020DH\u0002J\u001a\u0010E\u001a\u000202*\u0002032\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u0010F\u001a\u000202*\u0002032\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002J\u001a\u0010G\u001a\u000202*\u0002032\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u001b05H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\n \u0017*\u0004\u0018\u00010\u00160\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0018\u001a\n \u0017*\u0004\u0018\u00010\u00160\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"
    }
    d2 = {
        "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
        "",
        "birthdayFormatter",
        "Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;",
        "dateFormatCustomAttr",
        "Ljava/text/DateFormat;",
        "phoneHelper",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "locale",
        "Ljava/util/Locale;",
        "emailAppAvailable",
        "",
        "phoneAppAvailable",
        "geoAppAvailable",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "rolodexMerchantLoader",
        "Lcom/squareup/crm/RolodexMerchantLoader;",
        "(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Ljava/text/DateFormat;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Ljava/util/Locale;ZZZLcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexMerchantLoader;)V",
        "listPhraseWithAnd",
        "Lcom/squareup/util/ListPhrase;",
        "kotlin.jvm.PlatformType",
        "simpleListPhrase",
        "customOrdering",
        "",
        "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "attributeSchema",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema;",
        "defaultOrdering",
        "formatDate",
        "",
        "date",
        "Lcom/squareup/protos/common/time/DateTime;",
        "formatEnumValues",
        "enumValues",
        "getCustomerAttributeValue",
        "attribute",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "reorderDefaultsEnabled",
        "toViewData",
        "Lrx/Observable;",
        "Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;",
        "editEnabled",
        "viewData",
        "addressRow",
        "",
        "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
        "rows",
        "",
        "birthdayRow",
        "companyRow",
        "customAttributeRow",
        "customAttributesRows",
        "(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lkotlin/Unit;",
        "customTypedAttributeRow",
        "emailRow",
        "fullNameRow",
        "groupsRow",
        "maybeAddNewRow",
        "label",
        "",
        "maybeValue",
        "type",
        "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;",
        "memoRow",
        "phoneRow",
        "referenceIdRow",
        "crm-view-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final birthdayFormatter:Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

.field private final dateFormatCustomAttr:Ljava/text/DateFormat;

.field private final emailAppAvailable:Z

.field private final features:Lcom/squareup/settings/server/Features;

.field private final geoAppAvailable:Z

.field private final listPhraseWithAnd:Lcom/squareup/util/ListPhrase;

.field private final locale:Ljava/util/Locale;

.field private final phoneAppAvailable:Z

.field private final phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

.field private final simpleListPhrase:Lcom/squareup/util/ListPhrase;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Ljava/text/DateFormat;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Ljava/util/Locale;ZZZLcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexMerchantLoader;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/LongForm;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/squareup/ui/crm/annotations/EmailAppAvailable;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/squareup/ui/crm/annotations/PhoneAppAvailable;
        .end annotation
    .end param
    .param p8    # Z
        .annotation runtime Lcom/squareup/ui/crm/annotations/MapAppAvailable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "birthdayFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatCustomAttr"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodexMerchantLoader"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->birthdayFormatter:Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->dateFormatCustomAttr:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->locale:Ljava/util/Locale;

    iput-boolean p6, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->emailAppAvailable:Z

    iput-boolean p7, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->phoneAppAvailable:Z

    iput-boolean p8, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->geoAppAvailable:Z

    iput-object p9, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->features:Lcom/squareup/settings/server/Features;

    iput-object p10, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->list_pattern_long_two_separator:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    .line 87
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/utilities/R$string;->list_pattern_long_final_separator:I

    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    .line 84
    invoke-static {p1, p2, p3}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->listPhraseWithAnd:Lcom/squareup/util/ListPhrase;

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 90
    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->simpleListPhrase:Lcom/squareup/util/ListPhrase;

    return-void
.end method

.method private final addressRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 267
    sget v0, Lcom/squareup/crmscreens/R$string;->crm_address_header:I

    .line 270
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz p1, :cond_0

    .line 269
    invoke-static {p1}, Lcom/squareup/crm/util/RolodexProtoHelper;->toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 270
    invoke-static {p1}, Lcom/squareup/crm/util/RolodexProtoHelper;->formatSingleLine(Lcom/squareup/address/Address;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 271
    :goto_0
    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->geoAppAvailable:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->MAP:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    .line 266
    :goto_1
    invoke-direct {p0, p2, v0, p1, v1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow(Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    return-void
.end method

.method private final birthdayRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 255
    sget v2, Lcom/squareup/crmscreens/R$string;->crm_birthday_header:I

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->birthdayFormatter:Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->birthday:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;->format(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_born_on_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 259
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "birthday"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 260
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 261
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v3, p1

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 254
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final companyRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 228
    sget v2, Lcom/squareup/crmscreens/R$string;->crm_company_hint:I

    .line 229
    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->company_name:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 227
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final customAttributeRow(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 289
    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    const-string v0, "name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->getCustomerAttributeValue(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 288
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final customAttributesRows(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lkotlin/Unit;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .line 283
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    if-eqz p1, :cond_3

    check-cast p1, Ljava/lang/Iterable;

    .line 370
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 371
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    .line 282
    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->is_visible_in_profile:Ljava/lang/Boolean;

    const-string v3, "it.is_visible_in_profile"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 372
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 373
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    const-string v1, "it"

    .line 284
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->customAttributeRow(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Ljava/util/List;)V

    goto :goto_1

    .line 374
    :cond_2
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method

.method private final customOrdering(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema;->version:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexMerchantLoader;->refresh()V

    .line 147
    :cond_0
    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->getOrderedAttributes(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Ljava/util/List;

    move-result-object p2

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 151
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-nez v1, :cond_1

    goto :goto_1

    .line 153
    :cond_1
    check-cast p2, Ljava/lang/Iterable;

    .line 364
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/crm/model/ContactAttribute;

    .line 156
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$NameAttribute;

    if-eqz v3, :cond_3

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->fullNameRow(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)V

    goto :goto_0

    .line 157
    :cond_3
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;

    if-eqz v3, :cond_4

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->phoneRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    goto :goto_0

    .line 158
    :cond_4
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;

    if-eqz v3, :cond_5

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->emailRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    goto :goto_0

    .line 159
    :cond_5
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;

    if-eqz v3, :cond_6

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->addressRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    goto :goto_0

    .line 160
    :cond_6
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    if-eqz v3, :cond_7

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->groupsRow(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)V

    goto :goto_0

    .line 161
    :cond_7
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;

    if-eqz v3, :cond_8

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->birthdayRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    goto :goto_0

    .line 162
    :cond_8
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;

    if-eqz v3, :cond_9

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->companyRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    goto :goto_0

    .line 163
    :cond_9
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;

    if-eqz v3, :cond_a

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->referenceIdRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    goto :goto_0

    .line 164
    :cond_a
    instance-of v3, v2, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    if-eqz v3, :cond_2

    check-cast v2, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;

    invoke-direct {p0, v2, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->customTypedAttributeRow(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;Ljava/util/List;)V

    goto :goto_0

    .line 167
    :cond_b
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->memoRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    :goto_1
    return-object v0
.end method

.method private final customTypedAttributeRow(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute<",
            "*>;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 295
    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;->getName()Ljava/lang/String;

    move-result-object v2

    .line 296
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->getCustomerAttributeValue(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 294
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final defaultOrdering(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;"
        }
    .end annotation

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 176
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-nez v1, :cond_0

    goto :goto_0

    .line 178
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->fullNameRow(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)V

    .line 179
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->emailRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    .line 180
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->phoneRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    .line 181
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->companyRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    .line 182
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->groupsRow(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)V

    .line 183
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->referenceIdRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    .line 184
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->birthdayRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    .line 185
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->addressRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    .line 186
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->memoRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V

    .line 189
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->customAttributesRows(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lkotlin/Unit;

    :cond_1
    return-object v0
.end method

.method private final emailRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 213
    sget v0, Lcom/squareup/crmscreens/R$string;->crm_email_hint:I

    .line 214
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_email_address:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 215
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_2

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    if-nez p1, :cond_3

    iget-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->emailAppAvailable:Z

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->EMAIL:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    goto :goto_3

    :cond_3
    sget-object p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    .line 212
    :goto_3
    invoke-direct {p0, p2, v0, v1, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow(Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    return-void
.end method

.method private final formatDate(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_0

    .line 358
    iget-object v0, p1, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->dateFormatCustomAttr:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->locale:Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final formatEnumValues(Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->simpleListPhrase:Lcom/squareup/util/ListPhrase;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    check-cast p1, Ljava/lang/Iterable;

    invoke-virtual {v0, p1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final fullNameRow(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 203
    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_display_name_label:I

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 202
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final getCustomerAttributeValue(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute<",
            "*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 339
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomUnknownAttribute;->getValue()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 340
    :cond_0
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomNumberAttribute;->getValue()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 341
    :cond_1
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    .line 342
    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomBooleanAttribute;->getValue()Ljava/lang/Boolean;

    move-result-object p1

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget p1, Lcom/squareup/crmviewcustomer/R$string;->yes:I

    goto :goto_0

    :cond_2
    sget p1, Lcom/squareup/crmviewcustomer/R$string;->no:I

    .line 341
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 344
    :cond_3
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 345
    :cond_4
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->formatEnumValues(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 346
    :cond_5
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomPhoneAttribute;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomPhoneAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomPhoneAttribute;->getValue()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 347
    :cond_6
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEmailAttribute;->getValue()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 348
    :cond_7
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;

    if-eqz v0, :cond_9

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;->getValue()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p1

    if-eqz p1, :cond_8

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexProtoHelper;->toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object p1

    if-eqz p1, :cond_8

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexProtoHelper;->formatSingleLine(Lcom/squareup/address/Address;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_8
    const/4 p1, 0x0

    goto :goto_1

    .line 349
    :cond_9
    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomDateAttribute;

    if-eqz v0, :cond_b

    move-object v0, p1

    check-cast v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomDateAttribute;

    invoke-virtual {v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomDateAttribute;->getValue()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->formatDate(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    move-object p1, v0

    goto :goto_1

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getCustomerAttributeValue(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Ljava/lang/String;
    .locals 2

    .line 320
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    if-nez v0, :cond_0

    .line 321
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    goto/16 :goto_3

    .line 323
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    :goto_0
    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 331
    :pswitch_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    goto :goto_3

    .line 330
    :pswitch_1
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->formatDate(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move-object p1, v0

    goto :goto_3

    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    goto :goto_3

    .line 329
    :pswitch_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    goto :goto_3

    .line 328
    :pswitch_3
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    goto :goto_3

    .line 327
    :pswitch_4
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->formatEnumValues(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 326
    :pswitch_5
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    goto :goto_3

    .line 325
    :pswitch_6
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    const-string v1, "attribute.data.boolean_"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_4

    sget p1, Lcom/squareup/crmviewcustomer/R$string;->yes:I

    goto :goto_1

    :cond_4
    sget p1, Lcom/squareup/crmviewcustomer/R$string;->no:I

    :goto_1
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 324
    :pswitch_7
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    goto :goto_3

    .line 332
    :goto_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    :goto_3
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final groupsRow(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 234
    sget v2, Lcom/squareup/crmscreens/R$string;->crm_groups_hint:I

    .line 239
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v1, 0x0

    .line 236
    sget-object v3, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    aput-object v3, v0, v1

    const/4 v1, 0x1

    sget-object v3, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    aput-object v3, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 237
    invoke-static {p1}, Lcom/squareup/util/CollectionsKt;->nullIfEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Iterable;

    .line 366
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 367
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 368
    check-cast v1, Lcom/squareup/protos/client/rolodex/Group;

    .line 238
    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 369
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 240
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_grouped_in_format:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 241
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->listPhraseWithAnd:Lcom/squareup/util/ListPhrase;

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {v1, v0}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "groups"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 242
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 243
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    move-object v3, p1

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 233
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final maybeAddNewRow(Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;I",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;",
            ")V"
        }
    .end annotation

    .line 304
    move-object v0, p3

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 305
    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p2, p3, p4}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method private final maybeAddNewRow(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;",
            ")V"
        }
    .end annotation

    .line 314
    move-object v0, p3

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 315
    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    invoke-direct {v0, p2, p3, p4}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method static synthetic maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    .line 302
    sget-object p4, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow(Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    return-void
.end method

.method static synthetic maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    .line 312
    sget-object p4, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    return-void
.end method

.method private final memoRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 276
    sget v2, Lcom/squareup/crmscreens/R$string;->crm_note_header:I

    .line 277
    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->memo:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 275
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final phoneRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 220
    sget v0, Lcom/squareup/crmscreens/R$string;->crm_phone_hint:I

    .line 221
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {v2, v1}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 222
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->masked_phone_number:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 223
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_2

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    if-nez p1, :cond_3

    iget-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->phoneAppAvailable:Z

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->PHONE:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    goto :goto_3

    :cond_3
    sget-object p1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    .line 219
    :goto_3
    invoke-direct {p0, p2, v0, v1, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow(Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    return-void
.end method

.method private final referenceIdRow(Lcom/squareup/protos/client/rolodex/CustomerProfile;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/CustomerProfile;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;)V"
        }
    .end annotation

    .line 249
    sget v2, Lcom/squareup/crmscreens/R$string;->crm_reference_id_hint:I

    .line 250
    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->merchant_provided_id:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 248
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->maybeAddNewRow$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Ljava/util/List;ILjava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;ILjava/lang/Object;)V

    return-void
.end method

.method private final reorderDefaultsEnabled()Z
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_REORDER_DEFAULT_PROFILE_FIELDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic viewData$default(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Lcom/squareup/protos/client/rolodex/Contact;ZLcom/squareup/protos/client/rolodex/AttributeSchema;ILjava/lang/Object;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 122
    check-cast p3, Lcom/squareup/protos/client/rolodex/AttributeSchema;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->viewData(Lcom/squareup/protos/client/rolodex/Contact;ZLcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final toViewData(Lcom/squareup/protos/client/rolodex/Contact;Z)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;",
            ">;"
        }
    .end annotation

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->reorderDefaultsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->rolodexMerchantLoader:Lcom/squareup/crm/RolodexMerchantLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexMerchantLoader;->merchant()Lio/reactivex/Observable;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer$toViewData$1;-><init>(Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Lcom/squareup/protos/client/rolodex/Contact;Z)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "rolodexMerchantLoader.me\u2026            )\n          }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/ObservableSource;

    .line 107
    sget-object p2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, p2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 109
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->viewData(Lcom/squareup/protos/client/rolodex/Contact;ZLcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const-string p2, "rx.Observable.just(viewD\u2026tact, editEnabled, null))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public final viewData(Lcom/squareup/protos/client/rolodex/Contact;ZLcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
    .locals 6

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->reorderDefaultsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 125
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->customOrdering(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/AttributeSchema;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 127
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->defaultOrdering(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/util/List;

    move-result-object p1

    :goto_0
    move-object v4, p1

    .line 130
    new-instance p1, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    .line 131
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_personal_information_header_uppercase:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 132
    iget-object p3, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->edit:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_1

    .line 133
    sget-object p2, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    goto :goto_1

    :cond_1
    sget-object p2, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    :goto_1
    move-object v3, p2

    const/4 v5, 0x0

    move-object v0, p1

    .line 130
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;Ljava/lang/String;)V

    return-object p1
.end method
