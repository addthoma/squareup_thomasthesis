.class public interface abstract Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;
.super Ljava/lang/Object;
.source "ManageCouponsAndRewardsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelManageCouponsAndRewards()V
.end method

.method public abstract confirmVoid()V
.end method

.method public abstract couponClicked(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;)V
.end method

.method public abstract getConfirmCopy()Ljava/lang/String;
.end method

.method public abstract getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;
.end method

.method public abstract manageCouponsAndRewardsScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showConfirmVoid()V
.end method
