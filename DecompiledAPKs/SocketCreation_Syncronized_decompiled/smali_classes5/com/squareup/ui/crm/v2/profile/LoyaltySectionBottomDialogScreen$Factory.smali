.class public Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;
.super Ljava/lang/Object;
.source "LoyaltySectionBottomDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;
    .locals 0

    .line 145
    invoke-static {p1, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    .line 146
    new-instance p3, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory$1;

    invoke-direct {p3, p0, p2, p4}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory$1;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;Ljava/lang/Runnable;)V

    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method static synthetic lambda$create$0(Landroid/content/Context;Lcom/google/android/material/bottomsheet/BottomSheetDialog;Landroid/content/DialogInterface;)V
    .locals 1

    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget p2, Lcom/squareup/crmviewcustomer/R$dimen;->crm_bottom_card_dialog_width:I

    .line 88
    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 89
    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getWindow()Landroid/view/Window;

    move-result-object p2

    const/4 v0, -0x1

    invoke-virtual {p2, p0, v0}, Landroid/view/Window;->setLayout(II)V

    .line 91
    sget p0, Lcom/google/android/material/R$id;->design_bottom_sheet:I

    .line 92
    invoke-virtual {p1, p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 93
    invoke-static {p0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->from(Landroid/view/View;)Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object p0

    const/4 p1, 0x3

    .line 94
    invoke-virtual {p0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    const/4 p1, 0x0

    .line 95
    invoke-virtual {p0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setPeekHeight(I)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 71
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 72
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 73
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->loyaltySectionDropDownDialogScreen()Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Component;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Component;->runner()Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;

    move-result-object v1

    .line 75
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->loyaltySectionDropDownDialogScreen()Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Component;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Component;->pointsFormatter()Lcom/squareup/loyalty/PointsTermsFormatter;

    move-result-object v0

    .line 77
    new-instance v2, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-direct {v2, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 78
    sget v3, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_loyalty_section_dropdown_dialog:I

    const/4 v4, 0x0

    .line 79
    invoke-static {p1, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 81
    invoke-virtual {v2, v3}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 86
    new-instance v4, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$LoyaltySectionBottomDialogScreen$Factory$KBCBoD1tgs_SQzRDFiDAKKGvbZo;

    invoke-direct {v4, p1, v2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$LoyaltySectionBottomDialogScreen$Factory$KBCBoD1tgs_SQzRDFiDAKKGvbZo;-><init>(Landroid/content/Context;Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V

    invoke-virtual {v2, v4}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 98
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->loyalty_dropdown_adjust_status:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$iCH3ZfSgCCW4L1I07Zc28OeS_dE;

    invoke-direct {v4, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$iCH3ZfSgCCW4L1I07Zc28OeS_dE;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;)V

    invoke-direct {p0, v3, v2, p1, v4}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;->bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;

    move-result-object p1

    .line 101
    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_adjust_status:I

    .line 102
    invoke-virtual {v0, v4}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v4

    .line 101
    invoke-virtual {p1, v4}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 104
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;->canViewExpiringPoints()Z

    move-result p1

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    .line 105
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->loyalty_dropdown_view_expiring_points:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v5, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$w0mPJpwni1MpuCXNXzTmIGdeuqw;

    invoke-direct {v5, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$w0mPJpwni1MpuCXNXzTmIGdeuqw;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;)V

    .line 106
    invoke-direct {p0, v3, v2, p1, v5}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;->bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;

    move-result-object p1

    .line 109
    sget v5, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_view_expiring_points:I

    .line 110
    invoke-virtual {v0, v5}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 111
    invoke-virtual {p1, v4}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 116
    :cond_0
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->loyalty_dropdown_send_status:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$Ja891kblS9tJmusNoKeFP76ardo;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$Ja891kblS9tJmusNoKeFP76ardo;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;)V

    invoke-direct {p0, v3, v2, p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;->bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;

    .line 120
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->loyalty_dropdown_edit_phone:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$MqQRLnrE3U4LQaRVlOgJC4u-Pfo;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$MqQRLnrE3U4LQaRVlOgJC4u-Pfo;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;)V

    invoke-direct {p0, v3, v2, p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;->bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;

    .line 124
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->loyalty_dropdown_delete_account:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$bAXpwWQy1V5JmfW1XbiMzckRo4I;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$bAXpwWQy1V5JmfW1XbiMzckRo4I;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;)V

    invoke-direct {p0, v3, v2, p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;->bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;

    move-result-object p1

    .line 126
    invoke-virtual {p1, v4}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 128
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;->isForTransferringLoyalty()Z

    move-result p1

    if-nez p1, :cond_1

    .line 129
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->loyalty_dropdown_transfer_account:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$3DG-pu7UuPwd4LU2-RCWXWwq5IU;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$3DG-pu7UuPwd4LU2-RCWXWwq5IU;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;)V

    .line 130
    invoke-direct {p0, v3, v2, p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;->bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;

    move-result-object p1

    .line 133
    invoke-virtual {p1, v4}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 136
    :cond_1
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->dismiss_button:I

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$hjW4JuP3-nfOWABpeb8CheY9aLM;

    invoke-direct {v0, v2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$hjW4JuP3-nfOWABpeb8CheY9aLM;-><init>(Lcom/google/android/material/bottomsheet/BottomSheetDialog;)V

    invoke-direct {p0, v3, v2, p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Factory;->bindButton(Landroid/view/View;Lcom/google/android/material/bottomsheet/BottomSheetDialog;ILjava/lang/Runnable;)Lcom/squareup/marketfont/MarketButton;

    .line 140
    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
