.class Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseCustomer3ScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;",
        ">;"
    }
.end annotation


# static fields
.field static final SEARCH_DELAY_MS:J = 0xc8L


# instance fields
.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

.field private screen:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;Lcom/squareup/util/Res;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 122
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 123
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->getContactLoaderForSearch()Lcom/squareup/crm/RolodexContactLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    .line 124
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    .line 125
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setSearchDelayMs(J)V

    return-void
.end method

.method private formatCreateNewButtonLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 256
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_create_new_customer_label:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_create_new_customer_label_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "term"

    .line 260
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 261
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 262
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private update(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 4

    .line 204
    sget-object v0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$1;->$SwitchMap$com$squareup$crm$RolodexContactLoaderHelper$VisualState:[I

    invoke-virtual {p2}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch v0, :pswitch_data_0

    .line 251
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected visual state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 243
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showProgress(Z)V

    .line 244
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showContactList(Z)V

    .line 245
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showTopButtonRow(Z)V

    .line 246
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showMessage(ZLjava/lang/String;)V

    .line 247
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 235
    :pswitch_1
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showProgress(Z)V

    .line 236
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showContactList(Z)V

    .line 237
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->canShowCreateCustomerScreen()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showTopButtonRow(Z)V

    .line 238
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showMessage(ZLjava/lang/String;)V

    .line 239
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 227
    :pswitch_2
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showProgress(Z)V

    .line 228
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showContactList(Z)V

    .line 229
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showTopButtonRow(Z)V

    .line 230
    invoke-virtual {p1, v3, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showMessage(ZLjava/lang/String;)V

    .line 231
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 219
    :pswitch_3
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showProgress(Z)V

    .line 220
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showContactList(Z)V

    .line 221
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->canShowCreateCustomerScreen()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showTopButtonRow(Z)V

    .line 222
    invoke-virtual {p1, v3, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showMessage(ZLjava/lang/String;)V

    .line 223
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 210
    :pswitch_4
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showProgress(Z)V

    .line 211
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showContactList(Z)V

    .line 212
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showTopButtonRow(Z)V

    .line 213
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    .line 214
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 213
    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showMessage(ZLjava/lang/String;)V

    .line 215
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 206
    :pswitch_5
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showProgress(Z)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method bindTopButtonRow(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)V
    .locals 1

    .line 184
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$U65dHPOmXAUgoKPOnLcKQUj64S0;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$U65dHPOmXAUgoKPOnLcKQUj64S0;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    invoke-static {p2, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 191
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$CZ1QknrAL8ZG-GeCJV3Gpb88eGw;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$CZ1QknrAL8ZG-GeCJV3Gpb88eGw;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)V

    invoke-static {p2, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method createNewFromSearchTermClicked()V
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->showCreateCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$bindTopButtonRow$11$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)Lrx/Subscription;
    .locals 1

    .line 192
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->onSearchTermChanged()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    .line 193
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->getSearchTerm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$4ZZoMfC_44KxHgiQJ_HomwT0P1E;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$4ZZoMfC_44KxHgiQJ_HomwT0P1E;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)V

    .line 194
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bindTopButtonRow$9$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)Lrx/Subscription;
    .locals 1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->onCreateNewButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$eJ0ZZ9sjpETnVAoWipy0NG2dal8;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$eJ0ZZ9sjpETnVAoWipy0NG2dal8;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    .line 186
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Ljava/lang/String;)V
    .locals 3

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {v0, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->setSearchTerm(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    new-instance v1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V

    .line 160
    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->formatCreateNewButtonLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->setCreateNewFromSearchTerm(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$10$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;Ljava/lang/String;)V
    .locals 0

    .line 195
    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->formatCreateNewButtonLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->setCreateNewButtonLabel(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$3$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 0

    .line 166
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 167
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->update(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    return-void
.end method

.method public synthetic lambda$null$5$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 173
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 174
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->closeChooseCustomerScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$null$8$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;Lkotlin/Unit;)V
    .locals 0

    .line 187
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 188
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->showCreateCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$0$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V
    .locals 0

    .line 143
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->closeChooseCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$2$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)Lrx/Subscription;
    .locals 2

    .line 155
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->onSearchTermChanged()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    .line 156
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$BkDaO_0L1bzLH58IFte-pErgTms;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$BkDaO_0L1bzLH58IFte-pErgTms;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    .line 157
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)Lrx/Subscription;
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/crm/RolodexContactLoaderHelper;->visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$QWrPnPJmzOAU7P5wl6SF6MD9f2E;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$QWrPnPJmzOAU7P5wl6SF6MD9f2E;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    .line 165
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)Lrx/Subscription;
    .locals 2

    .line 171
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->onContactClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$rD4MpcrsMtW-zcbis1mMIx0apuI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$rD4MpcrsMtW-zcbis1mMIx0apuI;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    .line 172
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$ChooseCustomer3ScreenV2$Presenter(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lrx/Subscription;
    .locals 2

    .line 178
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollPosition()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$BENej-6kapg7lu3QqKXYxbbP7M0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$BENej-6kapg7lu3QqKXYxbbP7M0;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;)V

    .line 179
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 131
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 132
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->screen:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 136
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 137
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;

    .line 138
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->contactList()Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    move-result-object v0

    .line 140
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->screen:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;

    iget-boolean v2, v2, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2;->isFirstScreen:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    .line 142
    invoke-interface {v3}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->getChooseCustomerScreenTitle()Ljava/lang/String;

    move-result-object v3

    .line 141
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$wEaq1RrHt1se-MGVkTzO3TCuCd8;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$wEaq1RrHt1se-MGVkTzO3TCuCd8;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    .line 142
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 140
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 148
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->getContactListScrollPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->init(Lcom/squareup/crm/RolodexContactLoader;I)V

    .line 150
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->setSearchBox(Ljava/lang/String;)V

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Runner;->canShowCreateCustomerScreen()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;->showCreateNewFromSearchTermButton(Z)V

    .line 154
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$39lYQsCY7lP21rVAheR6qNdXWbo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$39lYQsCY7lP21rVAheR6qNdXWbo;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 163
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$q6GI46vHQ06KRktbEC2EVhgJ29g;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$q6GI46vHQ06KRktbEC2EVhgJ29g;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 170
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$comDqrA8OMWdaQkVm3WI3TsdqGw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$comDqrA8OMWdaQkVm3WI3TsdqGw;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer3ViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 177
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$8wCnkzshD3i0c3_c1v0AyzK7gdA;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer3ScreenV2$Presenter$8wCnkzshD3i0c3_c1v0AyzK7gdA;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer3ScreenV2$Presenter;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
