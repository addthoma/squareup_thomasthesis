.class public Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;
.super Ljava/lang/Object;
.source "UpdateCustomerScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenData"
.end annotation


# instance fields
.field public final errorMessage:Ljava/lang/String;

.field public final loading:Z

.field public final saveButtonEnabled:Z

.field public final showContactEdit:Z

.field public final x2DisplayToCustomerEnabled:Z


# direct methods
.method public constructor <init>(ZZZZLjava/lang/String;)V
    .locals 0

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->loading:Z

    .line 141
    iput-boolean p2, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->showContactEdit:Z

    .line 142
    iput-boolean p3, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->saveButtonEnabled:Z

    .line 143
    iput-boolean p4, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->x2DisplayToCustomerEnabled:Z

    .line 144
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->errorMessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 153
    instance-of v0, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 154
    check-cast p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;

    .line 155
    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->loading:Z

    iget-boolean v2, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->loading:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->showContactEdit:Z

    .line 156
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v2, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->showContactEdit:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->saveButtonEnabled:Z

    .line 157
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v2, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->saveButtonEnabled:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->x2DisplayToCustomerEnabled:Z

    .line 158
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v2, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->x2DisplayToCustomerEnabled:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->errorMessage:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->errorMessage:Ljava/lang/String;

    .line 159
    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 148
    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->loading:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->showContactEdit:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->saveButtonEnabled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->x2DisplayToCustomerEnabled:Z

    .line 149
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->errorMessage:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 148
    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 166
    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->loading:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->showContactEdit:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->saveButtonEnabled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->x2DisplayToCustomerEnabled:Z

    .line 167
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;->errorMessage:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "%s_%s_%s_%s_%s"

    .line 166
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
