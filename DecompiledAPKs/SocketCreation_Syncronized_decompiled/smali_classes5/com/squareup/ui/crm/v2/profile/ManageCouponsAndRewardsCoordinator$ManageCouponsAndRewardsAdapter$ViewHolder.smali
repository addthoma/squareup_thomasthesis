.class Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ManageCouponsAndRewardsCoordinator.java"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field item:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

.field row:Lcom/squareup/noho/NohoRow;

.field final synthetic this$1:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;Lcom/squareup/noho/NohoRow;I)V
    .locals 1

    .line 117
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    .line 118
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 119
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/noho/NohoRow;

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 122
    new-instance p3, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder$1;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder$1;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;)V

    invoke-virtual {p2, p3}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 128
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setEnabled(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method bindCoupon(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;)V
    .locals 3

    .line 141
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->item:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->access$100(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/noho/NohoRow;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    iget-object v1, v1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->access$100(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->access$200(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    iget-object v2, v2, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->access$300(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Ljava/util/Locale;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/coupons/Coupon;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/noho/NohoRow;

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iget-boolean p1, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->checked:Z

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    :cond_1
    return-void
.end method

.method public getEdges()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 0

    .line 137
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method
