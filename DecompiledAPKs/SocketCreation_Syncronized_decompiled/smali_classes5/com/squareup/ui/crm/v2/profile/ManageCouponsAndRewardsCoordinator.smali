.class public Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ManageCouponsAndRewardsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private adapter:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

.field private final formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

.field private final holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

.field private final locale:Ljava/util/Locale;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Ljava/util/Locale;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->locale:Ljava/util/Locale;

    .line 53
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;->getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Ljava/util/Locale;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->locale:Ljava/util/Locale;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 97
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 98
    sget v0, Lcom/squareup/crmscreens/R$id;->manage_coupons_and_rewards_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;)Ljava/util/List;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;->coupons:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 57
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_void_action:I

    .line 62
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_manage_title:I

    .line 64
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 63
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$d0Bt64f3tZFslBMwvvnPIHatoQw;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$d0Bt64f3tZFslBMwvvnPIHatoQw;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;)V

    .line 65
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$BwBQOpr7groGePWwh_j6zpKJ7Zo;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$BwBQOpr7groGePWwh_j6zpKJ7Zo;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;)V

    .line 66
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$BwBQOpr7groGePWwh_j6zpKJ7Zo;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$BwBQOpr7groGePWwh_j6zpKJ7Zo;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 71
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;Ljava/util/List;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 76
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$-Ivk-hz9_pz7L0DuJAT4sjb9l3s;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$-Ivk-hz9_pz7L0DuJAT4sjb9l3s;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 82
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$IzyoLh1bg-m5CJJYWrbBOgoFwf8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$IzyoLh1bg-m5CJJYWrbBOgoFwf8;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ManageCouponsAndRewardsCoordinator()Lrx/Subscription;
    .locals 3

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;->manageCouponsAndRewardsScreenData()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$lX74otvgYhfI3mBCWKmu5wgk42s;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$lX74otvgYhfI3mBCWKmu5wgk42s;

    .line 78
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$m3715YMS1j1NmFe7Ogcd9OTdSXk;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$m3715YMS1j1NmFe7Ogcd9OTdSXk;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator$ManageCouponsAndRewardsAdapter;)V

    .line 79
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$3$ManageCouponsAndRewardsCoordinator()Lrx/Subscription;
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;->manageCouponsAndRewardsScreenData()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$wwinalGutARYh_xhj3IcCBqRxtg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ManageCouponsAndRewardsCoordinator$wwinalGutARYh_xhj3IcCBqRxtg;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;)V

    .line 84
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$2$ManageCouponsAndRewardsCoordinator(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;)V
    .locals 1

    .line 86
    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;->coupons:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    .line 87
    iget-boolean v0, v0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->checked:Z

    if-eqz v0, :cond_0

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void

    .line 92
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
