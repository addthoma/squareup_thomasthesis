.class public Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;
.super Ljava/lang/Object;
.source "BuyerSummaryDataRenderer.java"


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final locale:Ljava/util/Locale;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;Ljava/util/Locale;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Clock;",
            "Ljava/util/Locale;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->res:Lcom/squareup/util/Res;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->clock:Lcom/squareup/util/Clock;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->locale:Ljava/util/Locale;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->dateFormatter:Ljava/text/DateFormat;

    return-void
.end method

.method private newLine(ILjava/lang/String;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    return-object v0
.end method

.method private toSummaryLines(Lcom/squareup/protos/client/rolodex/BuyerSummary;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/BuyerSummary;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    return-object v0

    .line 65
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->dateFormatter:Ljava/text/DateFormat;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->locale:Ljava/util/Locale;

    .line 67
    invoke-static {v2, v3}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 68
    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_activity_summary_customer_since:I

    invoke-direct {p0, v2, v1}, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->newLine(ILjava/lang/String;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_1
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    .line 73
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->locale:Ljava/util/Locale;

    invoke-static {v1, v2}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v1

    .line 74
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->clock:Lcom/squareup/util/Clock;

    .line 75
    invoke-interface {v3}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    const/4 v7, 0x1

    sget-object v8, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v9, 0x1

    invoke-static/range {v2 .. v9}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JJZLcom/squareup/util/ShortTimes$MaxUnit;Z)Ljava/lang/String;

    move-result-object v1

    .line 76
    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_activity_summary_last_visited:I

    .line 77
    invoke-direct {p0, v2, v1}, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->newLine(ILjava/lang/String;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    move-result-object v1

    .line 76
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_2
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 82
    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_activity_summary_visit_frequency:I

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->newLine(ILjava/lang/String;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_3
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    .line 89
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_4

    .line 90
    new-instance v1, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 92
    invoke-virtual {v1, p1}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 94
    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_activity_summary_title_avg_spend:I

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 95
    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 94
    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->newLine(ILjava/lang/String;)Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method


# virtual methods
.method public generateViewData(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
    .locals 7

    .line 49
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->toSummaryLines(Lcom/squareup/protos/client/rolodex/BuyerSummary;)Ljava/util/List;

    move-result-object p1

    .line 51
    new-instance v6, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_buyer_summary_title:I

    .line 52
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    .line 54
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;Ljava/lang/String;)V

    return-object v6
.end method
