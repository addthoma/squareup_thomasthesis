.class Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "MessageListViewV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

.field final synthetic val$conversationListItem:Lcom/squareup/protos/client/dialogue/ConversationListItem;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;Lcom/squareup/protos/client/dialogue/ConversationListItem;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter$1;->this$1:Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter$1;->val$conversationListItem:Lcom/squareup/protos/client/dialogue/ConversationListItem;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter$1;->this$1:Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter;->this$0:Lcom/squareup/ui/crm/v2/MessageListViewV2;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2;->access$000(Lcom/squareup/ui/crm/v2/MessageListViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListViewV2$Adapter$1;->val$conversationListItem:Lcom/squareup/protos/client/dialogue/ConversationListItem;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
