.class public interface abstract Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;
.super Ljava/lang/Object;
.source "ChooseDateAttributeDialogScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract getDateAttrViewData()Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;
.end method

.method public abstract getLocale()Ljava/util/Locale;
.end method

.method public abstract onDismiss()V
.end method

.method public abstract onSave(Lcom/squareup/protos/common/time/DateTime;)V
.end method
