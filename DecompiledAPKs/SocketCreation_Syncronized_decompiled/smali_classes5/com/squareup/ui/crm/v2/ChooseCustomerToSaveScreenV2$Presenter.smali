.class Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseCustomerToSaveScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;",
        ">;"
    }
.end annotation


# static fields
.field static final SEARCH_DELAY_MS:J = 0xc8L


# instance fields
.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;Lcom/squareup/util/Res;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 99
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 100
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->getContactLoaderForSearch()Lcom/squareup/crm/RolodexContactLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    .line 102
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setSearchDelayMs(J)V

    return-void
.end method

.method private update(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 4

    .line 166
    sget-object v0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$1;->$SwitchMap$com$squareup$crm$RolodexContactLoaderHelper$VisualState:[I

    invoke-virtual {p2}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch v0, :pswitch_data_0

    .line 213
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected visual state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 205
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showProgress(Z)V

    .line 206
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showContactList(Z)V

    .line 207
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showTopButtonRow(Z)V

    .line 208
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showMessage(ZLjava/lang/String;)V

    .line 209
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 197
    :pswitch_1
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showProgress(Z)V

    .line 198
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showContactList(Z)V

    .line 199
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->canShowCreateCustomerScreen()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showTopButtonRow(Z)V

    .line 200
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showMessage(ZLjava/lang/String;)V

    .line 201
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 189
    :pswitch_2
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showProgress(Z)V

    .line 190
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showContactList(Z)V

    .line 191
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showTopButtonRow(Z)V

    .line 192
    invoke-virtual {p1, v3, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showMessage(ZLjava/lang/String;)V

    .line 193
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 181
    :pswitch_3
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showProgress(Z)V

    .line 182
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showContactList(Z)V

    .line 183
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->canShowCreateCustomerScreen()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showTopButtonRow(Z)V

    .line 184
    invoke-virtual {p1, v3, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showMessage(ZLjava/lang/String;)V

    .line 185
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 172
    :pswitch_4
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showProgress(Z)V

    .line 173
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showContactList(Z)V

    .line 174
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showTopButtonRow(Z)V

    .line 175
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    .line 176
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 175
    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showMessage(ZLjava/lang/String;)V

    .line 177
    invoke-virtual {p1, v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showBottomButtonRow(Z)V

    goto :goto_0

    .line 168
    :pswitch_5
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showProgress(Z)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method bindTopButtonRow(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;)V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_cardonfile_savecard_new_customer:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->setCreateNewButtonLabel(Ljava/lang/String;)V

    .line 157
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$hei9RkcLsDSHCRT32zYcIkAr1BI;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$hei9RkcLsDSHCRT32zYcIkAr1BI;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-static {p2, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public createNewClicked()V
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->showCreateCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$bindTopButtonRow$9$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)Lrx/Subscription;
    .locals 1

    .line 158
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ContactListTopRowCreateCustomer;->onCreateNewButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$dQ1TpLeFIaTU9zRuxJ7ki85piu8;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$dQ1TpLeFIaTU9zRuxJ7ki85piu8;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    .line 159
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$ChooseCustomerToSaveScreenV2$Presenter(Ljava/lang/String;)V
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->setSearchTerm(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    new-instance v1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V

    return-void
.end method

.method public synthetic lambda$null$3$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 0

    .line 136
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 137
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->update(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    return-void
.end method

.method public synthetic lambda$null$5$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 143
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->closeChooseCustomerScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$null$8$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;Lkotlin/Unit;)V
    .locals 0

    .line 160
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->showCreateCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$0$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V
    .locals 0

    .line 115
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->closeChooseCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$2$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)Lrx/Subscription;
    .locals 1

    .line 126
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->searchTermChanged()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    .line 127
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->getSearchTerm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$Z5h3_cSMLYFAwh36nZh1WaBgQSE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$Z5h3_cSMLYFAwh36nZh1WaBgQSE;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;)V

    .line 128
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)Lrx/Subscription;
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/crm/RolodexContactLoaderHelper;->visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$onAW0jeBD1eMLQZZu30-g4whkJo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$onAW0jeBD1eMLQZZu30-g4whkJo;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    .line 135
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)Lrx/Subscription;
    .locals 1

    .line 141
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$2_9frTwW-7NL3LNASnwdsW_-wLU;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$2_9frTwW-7NL3LNASnwdsW_-wLU;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    .line 142
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$ChooseCustomerToSaveScreenV2$Presenter(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lrx/Subscription;
    .locals 2

    .line 148
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollPosition()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$3S1KCcVd52CKJ-Ccv_kM83TyrB0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$3S1KCcVd52CKJ-Ccv_kM83TyrB0;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;)V

    .line 149
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 108
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;

    .line 110
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->contactList()Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    move-result-object v0

    .line 112
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    .line 113
    invoke-interface {v3}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->getChooseCustomerScreenTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$li8hpVCu5066ciJE2s0NWMJVH_E;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$li8hpVCu5066ciJE2s0NWMJVH_E;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    .line 114
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 112
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 120
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->getContactListScrollPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->init(Lcom/squareup/crm/RolodexContactLoader;I)V

    .line 121
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->setSearchBox(Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;->canShowCreateCustomerScreen()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;->showCreateNewButton(Z)V

    .line 125
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$ZVOXdsbTawjUykUUxraJgQg_-x4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$ZVOXdsbTawjUykUUxraJgQg_-x4;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 133
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$IbFcOr1qifQnEM77MW9LLmlyRDc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$IbFcOr1qifQnEM77MW9LLmlyRDc;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 140
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$UobUQrGLptv6RHE-TGNmJ_iyjdg;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$UobUQrGLptv6RHE-TGNmJ_iyjdg;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveCardViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 147
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$U3s2lyCmz98rNfA7d6BLb-bDOII;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomerToSaveScreenV2$Presenter$U3s2lyCmz98rNfA7d6BLb-bDOII;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
