.class public final Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;
.super Ljava/lang/Object;
.source "PersonalInformationViewDataRenderer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
        ">;"
    }
.end annotation


# instance fields
.field private final birthdayFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatCustomAttrProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final emailAppAvailableProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final geoAppAvailableProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneAppAvailableProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexMerchantLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->birthdayFormatterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->dateFormatCustomAttrProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->resProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->emailAppAvailableProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->phoneAppAvailableProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p8, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->geoAppAvailableProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p9, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p10, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->rolodexMerchantLoaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;"
        }
    .end annotation

    .line 76
    new-instance v11, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Ljava/text/DateFormat;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Ljava/util/Locale;ZZZLcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexMerchantLoader;)Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;
    .locals 12

    .line 83
    new-instance v11, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Ljava/text/DateFormat;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Ljava/util/Locale;ZZZLcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexMerchantLoader;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;
    .locals 11

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->birthdayFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->dateFormatCustomAttrProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->emailAppAvailableProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->phoneAppAvailableProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->geoAppAvailableProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->rolodexMerchantLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/crm/RolodexMerchantLoader;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->newInstance(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Ljava/text/DateFormat;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Ljava/util/Locale;ZZZLcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexMerchantLoader;)Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer_Factory;->get()Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    move-result-object v0

    return-object v0
.end method
