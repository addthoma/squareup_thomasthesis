.class public final Lcom/squareup/ui/crm/v2/FiltersLayout_MembersInjector;
.super Ljava/lang/Object;
.source "FiltersLayout_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/v2/FiltersLayout;",
        ">;"
    }
.end annotation


# instance fields
.field private final filterHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/FilterHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/FilterHelper;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FiltersLayout_MembersInjector;->filterHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/FilterHelper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/v2/FiltersLayout;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/crm/v2/FiltersLayout_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/FiltersLayout_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectFilterHelper(Lcom/squareup/ui/crm/v2/FiltersLayout;Lcom/squareup/crm/filters/FilterHelper;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/FiltersLayout;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/v2/FiltersLayout;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/FiltersLayout_MembersInjector;->filterHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/filters/FilterHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/v2/FiltersLayout_MembersInjector;->injectFilterHelper(Lcom/squareup/ui/crm/v2/FiltersLayout;Lcom/squareup/crm/filters/FilterHelper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/crm/v2/FiltersLayout;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/FiltersLayout_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/v2/FiltersLayout;)V

    return-void
.end method
