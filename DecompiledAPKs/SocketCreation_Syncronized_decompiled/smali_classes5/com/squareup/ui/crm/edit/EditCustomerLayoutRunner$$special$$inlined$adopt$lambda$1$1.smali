.class final Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditCustomerLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/noho/NohoInputBox$Error;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/NohoInputBox$Error;",
        "email",
        "",
        "isFocused",
        "",
        "invoke",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$4$1$2",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$create$1$lambda$1",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$lambda$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $inputBox:Lcom/squareup/noho/NohoInputBox;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoInputBox;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$inputBox:Lcom/squareup/noho/NohoInputBox;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/CharSequence;Z)Lcom/squareup/noho/NohoInputBox$Error;
    .locals 2

    const-string v0, "email"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 104
    invoke-static {p1}, Lcom/squareup/text/Emails;->isValidOrPartial(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 105
    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_2

    invoke-static {p1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 106
    :cond_1
    new-instance v0, Lcom/squareup/noho/NohoInputBox$Error;

    iget-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$inputBox:Lcom/squareup/noho/NohoInputBox;

    const-string p2, "inputBox"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/noho/NohoInputBox;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 107
    sget p2, Lcom/squareup/crmeditcustomer/impl/R$string;->crm_add_customer_email_address_validation_error:I

    .line 106
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "inputBox.resources.getSt\u2026ation_error\n            )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/noho/NohoInputBox$Error;-><init>(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1$1;->invoke(Ljava/lang/CharSequence;Z)Lcom/squareup/noho/NohoInputBox$Error;

    move-result-object p1

    return-object p1
.end method
