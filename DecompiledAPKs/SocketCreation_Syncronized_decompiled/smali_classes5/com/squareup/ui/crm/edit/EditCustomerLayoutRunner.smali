.class public final Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;
.super Ljava/lang/Object;
.source "EditCustomerLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/ui/crm/edit/EditCustomerScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditCustomerLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditCustomerLayoutRunner.kt\ncom/squareup/ui/crm/edit/EditCustomerLayoutRunner\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 8 BinderRowSpec.kt\ncom/squareup/cycler/BinderRowSpec\n*L\n1#1,283:1\n49#2:284\n50#2,3:290\n53#2:377\n599#3,4:285\n601#3:289\n43#4,2:293\n310#5,6:295\n310#5,3:301\n313#5,3:310\n310#5,3:313\n313#5,3:322\n310#5,6:325\n310#5,6:331\n342#5,5:340\n344#5,2:345\n347#5:349\n342#5,5:354\n344#5,2:359\n347#5:363\n310#5,3:365\n313#5,3:374\n35#6,6:304\n35#6,6:316\n35#6,6:368\n32#7,3:337\n36#7:350\n32#7,3:351\n36#7:364\n24#8,2:347\n24#8,2:361\n*E\n*S KotlinDebug\n*F\n+ 1 EditCustomerLayoutRunner.kt\ncom/squareup/ui/crm/edit/EditCustomerLayoutRunner\n*L\n75#1:284\n75#1,3:290\n75#1:377\n75#1,4:285\n75#1:289\n75#1,2:293\n75#1,6:295\n75#1,3:301\n75#1,3:310\n75#1,3:313\n75#1,3:322\n75#1,6:325\n75#1,6:331\n75#1,5:340\n75#1,2:345\n75#1:349\n75#1,5:354\n75#1,2:359\n75#1:363\n75#1,3:365\n75#1,3:374\n75#1,6:304\n75#1,6:316\n75#1,6:368\n75#1,3:337\n75#1:350\n75#1,3:351\n75#1:364\n75#1,2:347\n75#1,2:361\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001fB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u001eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n \u000c*\u0004\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \u000c*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R,\u0010\u0014\u001a\u0004\u0018\u00010\u0013*\u00020\u00152\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00138B@BX\u0082\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/ui/crm/edit/EditCustomerScreen;",
        "rootView",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "progressBar",
        "Landroid/widget/ProgressBar;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "value",
        "",
        "label",
        "Lcom/squareup/noho/NohoInputBox;",
        "getLabel",
        "(Lcom/squareup/noho/NohoInputBox;)Ljava/lang/CharSequence;",
        "setLabel",
        "(Lcom/squareup/noho/NohoInputBox;Ljava/lang/CharSequence;)V",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final progressBar:Landroid/widget/ProgressBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final rootView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 4

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->rootView:Landroid/view/View;

    .line 72
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->rootView:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->rootView:Landroid/view/View;

    sget v0, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_progress_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->rootView:Landroid/view/View;

    sget v0, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_recycler_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 285
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 286
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 290
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 291
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 77
    sget-object p2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 293
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 84
    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 293
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 296
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 87
    sget-object v1, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$3$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 95
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 295
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 302
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 99
    sget v2, Lcom/squareup/crmeditcustomer/impl/R$layout;->crm_edit_email_row:I

    .line 304
    new-instance v3, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {v3, v2, p0}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 302
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 301
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 314
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 130
    sget v2, Lcom/squareup/crmeditcustomer/impl/R$layout;->crm_edit_name_row:I

    .line 316
    new-instance v3, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;

    invoke-direct {v3, v2, p0}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;-><init>(ILcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 314
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 313
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 326
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 167
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3;-><init>(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 326
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 325
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 332
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$5;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 184
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$4;-><init>(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 332
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 331
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 338
    sget-object p2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$1;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 341
    new-instance v2, Lcom/squareup/cycler/BinderRowSpec;

    .line 345
    sget-object v3, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 341
    invoke-direct {v2, v3, p2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 347
    new-instance p2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$bind$3;

    invoke-direct {p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$bind$3;-><init>()V

    check-cast p2, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v2, p2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 213
    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 340
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 352
    sget-object p2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$3;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$3;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 355
    new-instance v2, Lcom/squareup/cycler/BinderRowSpec;

    .line 359
    sget-object v3, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$4;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$nohoRow$4;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 355
    invoke-direct {v2, v3, p2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 361
    new-instance p2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$bind$4;

    invoke-direct {p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$bind$4;-><init>()V

    check-cast p2, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v2, p2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 227
    check-cast v2, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 354
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 366
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$6;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 231
    sget v1, Lcom/squareup/crmeditcustomer/impl/R$layout;->crm_edit_address_row:I

    .line 368
    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$5;

    invoke-direct {v2, v1, p0}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$5;-><init>(ILcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 366
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 365
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 288
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    .line 245
    iget-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 246
    new-instance p2, Lcom/squareup/ui/DividerDecoration;

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->rootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->noho_spacing_large:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 246
    invoke-direct {p2, v0}, Lcom/squareup/ui/DividerDecoration;-><init>(I)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    .line 245
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    .line 285
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$getLabel$p(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;Lcom/squareup/noho/NohoInputBox;)Ljava/lang/CharSequence;
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->getLabel(Lcom/squareup/noho/NohoInputBox;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRootView$p(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)Landroid/view/View;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->rootView:Landroid/view/View;

    return-object p0
.end method

.method public static final synthetic access$setLabel$p(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;Lcom/squareup/noho/NohoInputBox;Ljava/lang/CharSequence;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->setLabel(Lcom/squareup/noho/NohoInputBox;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final getLabel(Lcom/squareup/noho/NohoInputBox;)Ljava/lang/CharSequence;
    .locals 1

    .line 280
    sget v0, Lcom/squareup/noho/R$id;->label:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoInputBox;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById<TextView>(nohoR.id.label)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private final setLabel(Lcom/squareup/noho/NohoInputBox;Ljava/lang/CharSequence;)V
    .locals 1

    .line 281
    sget v0, Lcom/squareup/noho/R$id;->label:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoInputBox;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "findViewById<TextView>(nohoR.id.label)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/ui/crm/edit/EditCustomerScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 4

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->rootView:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 257
    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 259
    sget-object v0, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    .line 260
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    .line 258
    invoke-virtual {p2, v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 262
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getTitle()Lcom/squareup/resources/TextModel;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 264
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getContent()Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;

    if-eqz v0, :cond_0

    .line 266
    sget-object v0, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 267
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    .line 268
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getContent()Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;->getSaveEnabled()Z

    move-result v2

    .line 269
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getContent()Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;->getOnSave()Lkotlin/jvm/functions/Function0;

    move-result-object v3

    .line 265
    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getContent()Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;->getData()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 275
    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->progressBar:Landroid/widget/ProgressBar;

    const-string v0, "progressBar"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getContent()Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Loading;

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 276
    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerScreen;->getContent()Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/ui/crm/edit/EditCustomerScreen$Content$Edit;

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;->showRendering(Lcom/squareup/ui/crm/edit/EditCustomerScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
