.class public final Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "TI;TS;TV;>;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$create$1\n+ 2 EditCustomerLayoutRunner.kt\ncom/squareup/ui/crm/edit/EditCustomerLayoutRunner\n+ 3 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox\n+ 4 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,87:1\n131#2,6:88\n137#2,5:95\n143#2:101\n144#2,5:103\n150#2:109\n163#2:112\n172#3:94\n173#3:100\n172#3:102\n173#3:108\n64#4,2:110\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00072\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0002\u0008\n\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "context",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/cycler/StandardRowSpec$create$1",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$create$2",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$lambda$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $layoutId:I

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;


# direct methods
.method public constructor <init>(ILcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V
    .locals 0

    iput p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;->$layoutId:I

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "$receiver"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v9, p0

    .line 37
    iget v3, v9, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;->$layoutId:I

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_name_first_input_box:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/noho/NohoInputBox;

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_name_first_input_scrubber:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_name_second_input_box:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/noho/NohoInputBox;

    .line 91
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_name_second_input_scrubber:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 94
    invoke-virtual {v5}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v1

    const/4 v3, 0x1

    .line 95
    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoEditRow;->setSingleLine(Z)V

    .line 96
    move-object v8, v1

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/squareup/crmscreens/R$integer;->crm_name_max_length:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    invoke-static {v8, v10}, Lcom/squareup/util/Views;->setMaxLength(Landroid/widget/TextView;I)V

    const/16 v8, 0x2001

    .line 97
    invoke-virtual {v1, v8}, Lcom/squareup/noho/NohoEditRow;->setInputType(I)V

    .line 98
    new-instance v17, Lcom/squareup/noho/ClearPlugin;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xe

    const/16 v16, 0x0

    move-object/from16 v10, v17

    invoke-direct/range {v10 .. v16}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v10, v17

    check-cast v10, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v1, v10}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 102
    invoke-virtual {v7}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v1

    .line 103
    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoEditRow;->setSingleLine(Z)V

    .line 104
    move-object v3, v1

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/squareup/crmscreens/R$integer;->crm_name_max_length:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    invoke-static {v3, v10}, Lcom/squareup/util/Views;->setMaxLength(Landroid/widget/TextView;I)V

    .line 105
    invoke-virtual {v1, v8}, Lcom/squareup/noho/NohoEditRow;->setInputType(I)V

    .line 106
    new-instance v3, Lcom/squareup/noho/ClearPlugin;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xe

    const/16 v17, 0x0

    move-object v11, v3

    invoke-direct/range {v11 .. v17}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v3, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 110
    new-instance v1, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2$1;

    move-object v3, v1

    move-object/from16 v8, p0

    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2$1;-><init>(Landroid/view/View;Lcom/squareup/noho/NohoInputBox;Landroid/view/View;Lcom/squareup/noho/NohoInputBox;Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$2;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void

    .line 37
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type V"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
