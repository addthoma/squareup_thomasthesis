.class public final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;
.super Ljava/lang/Object;
.source "EditCustomerEnumLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditCustomerEnumLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditCustomerEnumLayoutRunner.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n*L\n1#1,63:1\n49#2:64\n50#2,3:70\n53#2:90\n599#3,4:65\n601#3:69\n114#4,5:73\n120#4:89\n328#5:78\n342#5,5:79\n344#5,4:84\n329#5:88\n*E\n*S KotlinDebug\n*F\n+ 1 EditCustomerEnumLayoutRunner.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner\n*L\n37#1:64\n37#1,3:70\n37#1:90\n37#1,4:65\n37#1:69\n37#1,5:73\n37#1:89\n37#1:78\n37#1,5:79\n37#1,4:84\n37#1:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "kotlin.jvm.PlatformType",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->view:Landroid/view/View;

    .line 35
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/crmeditcustomer/impl/R$id;->crm_enum_recycler_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 65
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 70
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 71
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 38
    sget-object p2, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast p2, Lcom/squareup/noho/CheckType;

    sget-object v1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$recycler$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function3;

    .line 74
    new-instance v2, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$$special$$inlined$nohoRow$1;

    invoke-direct {v2, p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$$special$$inlined$nohoRow$1;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 80
    new-instance p2, Lcom/squareup/cycler/BinderRowSpec;

    .line 84
    sget-object v3, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$$special$$inlined$nohoRow$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 80
    invoke-direct {p2, v3, v2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 78
    invoke-virtual {p2, v1}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 83
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 79
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 68
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 65
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public showRendering(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 58
    iget-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 52
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->getTitle()Lcom/squareup/resources/TextModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 55
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    .line 56
    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object v2

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 60
    iget-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;->getOptions()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner;->showRendering(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
