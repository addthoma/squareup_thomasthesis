.class public interface abstract Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;
.super Ljava/lang/Object;
.source "SaveCardVerifyPostalCodeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeVerifyPostalCodeScreen()V
.end method

.method public abstract getContactForVerifyPostalCodeScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;
.end method

.method public abstract showSaveCardSpinnerScreen()V
.end method
