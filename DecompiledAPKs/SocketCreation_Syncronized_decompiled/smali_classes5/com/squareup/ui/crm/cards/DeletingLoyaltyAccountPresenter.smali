.class public Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;
.super Lmortar/Presenter;
.source "DeletingLoyaltyAccountPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L

.field static final MIN_LATENCY_SECONDS:J = 0x1L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final received:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 53
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/Long;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 57
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$DeletingLoyaltyAccountPresenter(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->hideProgress()V

    .line 84
    instance-of p2, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p2, :cond_0

    .line 85
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_loyalty_deleting_success:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->showText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;->onDeletingLoyaltyAccountSuccess()V

    goto :goto_0

    .line 90
    :cond_0
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 91
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_loyalty_deleting_failure:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->showText(Ljava/lang/CharSequence;)V

    .line 94
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const-wide/16 v0, 0x3

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$3$DeletingLoyaltyAccountPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;->closeDeletingLoyaltyAccountScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$2$DeletingLoyaltyAccountPresenter(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$VNqaP0XE3jNA8wtTbY2JTrFLYJY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$VNqaP0XE3jNA8wtTbY2JTrFLYJY;-><init>(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;)V

    .line 81
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$DeletingLoyaltyAccountPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$MRd-0afwQHIgI8x39XCWzXDrCsE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$MRd-0afwQHIgI8x39XCWzXDrCsE;-><init>(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;)V

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 61
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;

    .line 65
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;->getLoyaltyAccountToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->deleteLoyaltyAccount(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    .line 68
    invoke-static {v3, v4, v1, v2}, Lio/reactivex/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$RzfRTd4rqcRig1CCEBKd2a1nSvw;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$RzfRTd4rqcRig1CCEBKd2a1nSvw;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 73
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_deleting_account:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->showText(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$Q6SDsnG6aaNa8aSS7UzZroLcbOI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$Q6SDsnG6aaNa8aSS7UzZroLcbOI;-><init>(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$1KoBTBL99LNPhmV0vJDU3NsFVQk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$DeletingLoyaltyAccountPresenter$1KoBTBL99LNPhmV0vJDU3NsFVQk;-><init>(Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
