.class public Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;
.super Lmortar/ViewPresenter;
.source "MultiOptionFilterContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;",
        ">;"
    }
.end annotation


# instance fields
.field private final filter:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private final helper:Lcom/squareup/crm/filters/MultiOptionFilterHelper;


# direct methods
.method constructor <init>(Lcom/squareup/crm/filters/MultiOptionFilterHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 23
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/MultiOptionFilterHelper;

    return-void
.end method

.method private rebind(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)V
    .locals 5

    .line 53
    invoke-static {p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetachNow(Landroid/view/View;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/MultiOptionFilterHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->getAvailableOptions(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 58
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;->addRow()Lcom/squareup/ui/crm/rows/CheckableRow;

    move-result-object v3

    .line 59
    iget-object v4, v2, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/crm/rows/CheckableRow;->showTitle(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v4, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/MultiOptionFilterHelper;

    invoke-virtual {v4, v0, v2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->isSelected(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 62
    invoke-virtual {v3, v4}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 65
    :cond_0
    new-instance v4, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$MultiOptionFilterContentPresenter$qfBSclhhKNGsNw7nlHQTePJvCqA;

    invoke-direct {v4, p0, v3, v2}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$MultiOptionFilterContentPresenter$qfBSclhhKNGsNw7nlHQTePJvCqA;-><init>(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;Lcom/squareup/ui/crm/rows/CheckableRow;Lcom/squareup/protos/client/rolodex/Filter$Option;)V

    invoke-static {v3, v4}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method isValid()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/MultiOptionFilterHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$egLbZi7XBFwrlXyRPH6FsmUYeJc;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$egLbZi7XBFwrlXyRPH6FsmUYeJc;-><init>(Lcom/squareup/crm/filters/MultiOptionFilterHelper;)V

    .line 48
    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$MultiOptionFilterContentPresenter(Lcom/squareup/ui/crm/rows/CheckableRow;Lcom/squareup/protos/client/rolodex/Filter$Option;Lkotlin/Unit;)V
    .locals 1

    .line 68
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->isChecked()Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p3, 0x0

    .line 69
    invoke-virtual {p1, p3}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p3, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/MultiOptionFilterHelper;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {p3, v0, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->unselect(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 p3, 0x1

    .line 72
    invoke-virtual {p1, p3}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p3, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->helper:Lcom/squareup/crm/filters/MultiOptionFilterHelper;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {p3, v0, p2}, Lcom/squareup/crm/filters/MultiOptionFilterHelper;->select(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$rebind$1$MultiOptionFilterContentPresenter(Lcom/squareup/ui/crm/rows/CheckableRow;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lrx/Subscription;
    .locals 2

    .line 66
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->onClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$MultiOptionFilterContentPresenter$hFVCSBrkC9CgWpzZl6jfBcPmj1k;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$MultiOptionFilterContentPresenter$hFVCSBrkC9CgWpzZl6jfBcPmj1k;-><init>(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;Lcom/squareup/ui/crm/rows/CheckableRow;Lcom/squareup/protos/client/rolodex/Filter$Option;)V

    .line 67
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 31
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->rebind(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)V

    return-void
.end method

.method setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;->rebind(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)V

    :cond_0
    return-void
.end method
