.class public Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;
.super Landroid/widget/FrameLayout;
.source "SaveCardVerifyPostalCodeView.java"


# instance fields
.field private agreeButton:Lcom/squareup/marketfont/MarketButton;

.field private helpTextView:Lcom/squareup/widgets/MessageView;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field private onAgreeClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

.field private postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

.field presenter:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private titleView:Landroid/widget/TextView;

.field private upButton:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-class p2, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;->inject(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->togglePostalKeyboardSwitch()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 121
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_postalcode_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->titleView:Landroid/widget/TextView;

    .line 122
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_postalcode_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->messageView:Lcom/squareup/widgets/MessageView;

    .line 123
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_postalcode:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/PostalEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    .line 124
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_postal_keyboard_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    .line 125
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_authorize_add_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->agreeButton:Lcom/squareup/marketfont/MarketButton;

    .line 126
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_postal_disclaimer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->helpTextView:Lcom/squareup/widgets/MessageView;

    .line 127
    sget v0, Lcom/squareup/ui/crm/R$id;->up_button_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method private togglePostalKeyboardSwitch()V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->toggleKeyboard()V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/card/PostalEditText;->getToggleKeyboardGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method


# virtual methods
.method public hideSoftKeyboard()V
    .locals 0

    .line 117
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$setCountryCode$0$SaveCardVerifyPostalCodeView()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method onAgreeClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->onAgreeClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 69
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 75
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 46
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->bindViews()V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->agreeButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->onAgreeClicked:Lrx/Observable;

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView$1;-><init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method postalCode()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedShortText(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public setAgreeEnabled(Z)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->agreeButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method public setCountryCode(Lcom/squareup/CountryCode;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->showDefaultKeyboardForCountry(Lcom/squareup/CountryCode;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalKeyboardSwitch:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/PostalEditText;->getToggleKeyboardGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PostalEditText;->requestFocus()Z

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeView$g2EWfGgElC3btEOaAYroDyzTyKI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeView$g2EWfGgElC3btEOaAYroDyzTyKI;-><init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/PostalEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setHelpText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->helpTextView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHintText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMessageText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setUpButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public showPostalCode(Ljava/lang/String;)V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCodeView:Lcom/squareup/register/widgets/card/PostalEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/PostalEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
