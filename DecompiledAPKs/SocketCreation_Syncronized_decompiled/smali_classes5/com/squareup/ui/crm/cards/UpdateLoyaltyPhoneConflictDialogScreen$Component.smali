.class public interface abstract Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;
.super Ljava/lang/Object;
.source "UpdateLoyaltyPhoneConflictDialogScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract phoneHelper()Lcom/squareup/text/PhoneNumberHelper;
.end method

.method public abstract res()Lcom/squareup/util/Res;
.end method

.method public abstract runner()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;
.end method
