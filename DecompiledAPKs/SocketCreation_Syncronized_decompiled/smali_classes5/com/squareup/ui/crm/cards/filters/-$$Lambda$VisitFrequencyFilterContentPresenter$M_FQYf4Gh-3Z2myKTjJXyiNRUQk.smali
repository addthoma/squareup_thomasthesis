.class public final synthetic Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

.field private final synthetic f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

.field private final synthetic f$2:Ljava/util/concurrent/atomic/AtomicReference;

.field private final synthetic f$3:Lcom/squareup/protos/client/rolodex/Filter$Option;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$0:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$2:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$3:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$0:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$2:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;->f$3:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->lambda$bind$2$VisitFrequencyFilterContentPresenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
