.class public final Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;
.super Ljava/lang/Object;
.source "FrequentItemSubtitleFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFrequentItemSubtitleFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FrequentItemSubtitleFormatter.kt\ncom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter\n*L\n1#1,33:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "getFrequentItemText",
        "",
        "frequentItem",
        "Lcom/squareup/protos/client/rolodex/FrequentItem;",
        "crm-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final getFrequentItemText(Lcom/squareup/protos/client/rolodex/FrequentItem;)Ljava/lang/String;
    .locals 4

    const-string v0, "frequentItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    const-string v1, "frequentItem.category_name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    .line 12
    sget v2, Lcom/squareup/crmscreens/R$string;->crm_frequent_items_uncategorized:I

    .line 11
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 13
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->category_name:Ljava/lang/String;

    .line 15
    :goto_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/FrequentItem;->transaction_count:Ljava/lang/Integer;

    .line 17
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result v1

    if-lez v1, :cond_2

    .line 18
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_frequent_items_subtitle_purchase_plural:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 20
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$string;->crm_frequent_items_subtitle_purchase_singular:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 23
    :goto_2
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    .line 24
    sget v3, Lcom/squareup/crmscreens/R$string;->crm_frequent_items_subtitle_format:I

    .line 23
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 26
    check-cast v0, Ljava/lang/CharSequence;

    const-string v3, "category_name"

    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v2, "purchaseCount"

    .line 27
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const-string/jumbo v2, "transaction_count"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 28
    check-cast v1, Ljava/lang/CharSequence;

    const-string v0, "purchase_terminology"

    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 30
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
