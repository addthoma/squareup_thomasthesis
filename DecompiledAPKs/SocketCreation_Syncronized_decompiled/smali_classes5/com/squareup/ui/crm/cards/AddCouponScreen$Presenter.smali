.class Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "AddCouponScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AddCouponScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/AddCouponView;",
        ">;"
    }
.end annotation


# instance fields
.field private final addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/coupon/AddCouponState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 72
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;

    .line 74
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 75
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    return-void
.end method

.method private discountOrEmpty(Lcom/squareup/ui/crm/cards/AddCouponView;)Lcom/squareup/api/items/Discount;
    .locals 1

    .line 117
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddCouponView;->getCouponValue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-nez p1, :cond_0

    .line 119
    sget-object p1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    return-object p1

    .line 122
    :cond_0
    new-instance v0, Lcom/squareup/api/items/Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$Builder;-><init>()V

    .line 123
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Discount$Builder;->amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/api/items/Discount$Builder;->build()Lcom/squareup/api/items/Discount;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$3(Lcom/squareup/ui/crm/cards/AddCouponView;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 3

    .line 104
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AddCouponView;->getCouponValue()Lcom/squareup/protos/common/Money;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-eqz v2, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$4(Lcom/squareup/ui/crm/cards/AddCouponView;Ljava/lang/Boolean;)V
    .locals 0

    .line 107
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 108
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/AddCouponView;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$onLoad$5(Lcom/squareup/ui/crm/cards/AddCouponView;)Lrx/Subscription;
    .locals 2

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AddCouponView;->couponIsBlank()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$VQidbooRXFYZx4YMPT3NYuYIRuY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$VQidbooRXFYZx4YMPT3NYuYIRuY;-><init>(Lcom/squareup/ui/crm/cards/AddCouponView;)V

    .line 104
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$NB9HOW6XJaxoOu8Udxkok67DvTY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$NB9HOW6XJaxoOu8Udxkok67DvTY;-><init>(Lcom/squareup/ui/crm/cards/AddCouponView;)V

    .line 106
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$onLoad$0$AddCouponScreen$Presenter()V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    sget-object v1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;

    sget-object v1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;->closeAddCouponScreen(Lcom/squareup/api/items/Discount;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$AddCouponScreen$Presenter(Lcom/squareup/ui/crm/cards/AddCouponView;)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->discountOrEmpty(Lcom/squareup/ui/crm/cards/AddCouponView;)Lcom/squareup/api/items/Discount;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->discountOrEmpty(Lcom/squareup/ui/crm/cards/AddCouponView;)Lcom/squareup/api/items/Discount;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;->closeAddCouponScreen(Lcom/squareup/api/items/Discount;)V

    return-void
.end method

.method public synthetic lambda$onLoad$2$AddCouponScreen$Presenter()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->addCouponState:Lcom/squareup/ui/crm/coupon/AddCouponState;

    sget-object v1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/coupon/AddCouponState;->setDiscount(Lcom/squareup/api/items/Discount;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;

    sget-object v1, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;->closeAddCouponScreen(Lcom/squareup/api/items/Discount;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 79
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/AddCouponView;

    .line 82
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_add_coupon:I

    .line 83
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$TSK9KDCRCaitFPHB6Dv6azyNUFU;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$TSK9KDCRCaitFPHB6Dv6azyNUFU;-><init>(Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;)V

    .line 84
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->done:I

    .line 88
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 89
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$OgryaZLivBJ5UZfKjuWsG_NWGNQ;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$OgryaZLivBJ5UZfKjuWsG_NWGNQ;-><init>(Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;Lcom/squareup/ui/crm/cards/AddCouponView;)V

    .line 90
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 94
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/AddCouponView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 96
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$lHKCQHys8eXfVXeqK1naaALKKuM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$lHKCQHys8eXfVXeqK1naaALKKuM;-><init>(Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;)V

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 102
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$SrMDdgsbR3zX6bfj8tgG6DiCeWc;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCouponScreen$Presenter$SrMDdgsbR3zX6bfj8tgG6DiCeWc;-><init>(Lcom/squareup/ui/crm/cards/AddCouponView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_0

    .line 112
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/AddCouponView;->requestTextEditFocus()V

    :cond_0
    return-void
.end method
