.class public Lcom/squareup/ui/crm/cards/CustomerSaveCardView;
.super Landroid/widget/LinearLayout;
.source "CustomerSaveCardView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

.field private final firstName:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private firstNameField:Lcom/squareup/ui/XableEditText;

.field private footer:Landroid/widget/TextView;

.field private final lastName:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private lastNameField:Lcom/squareup/ui/XableEditText;

.field presenter:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->firstName:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 37
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->lastName:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 48
    const-class p2, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Component;->inject(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->firstName:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->lastName:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 159
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 160
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->title:Landroid/widget/TextView;

    .line 161
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_customer_card_row_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/CustomerCardView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    .line 162
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_first_name_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->firstNameField:Lcom/squareup/ui/XableEditText;

    .line 163
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_last_name_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->lastNameField:Lcom/squareup/ui/XableEditText;

    .line 164
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_footer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->footer:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method firstName()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->firstName:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 52
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method hideFooter()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->footer:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method hideTitle()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->title:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method initCardEditor(Lcom/squareup/CountryCode;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/CustomerCardView;->initCardEditor(Lcom/squareup/CountryCode;)V

    return-void
.end method

.method lastName()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->lastName:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->presenter:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method onCardEntryDone()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardEntryDone()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onCardInvalid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card$PanWarning;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardInvalid()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onCardValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardValid()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->presenter:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 80
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->bindViews()V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->firstNameField:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/CustomerSaveCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView$1;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->lastNameField:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/CustomerSaveCardView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView$2;-><init>(Lcom/squareup/ui/crm/cards/CustomerSaveCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->presenter:Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarPrimaryButtonEnabled(Z)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method setCardEditorHint(I)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/CustomerCardView;->setHint(I)V

    return-void
.end method

.method setFirstName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->firstNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setLastName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->lastNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTitle(I)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->showTitle()V

    return-void
.end method

.method showCardEntryField()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->showCardEntry()V

    return-void
.end method

.method showCardToSave(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 116
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->customerCardRowView:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/crm/rows/CustomerCardView;->setCardData(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    return-void
.end method

.method showFooter()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->footer:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method showTitle()V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardView;->title:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
