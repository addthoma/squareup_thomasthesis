.class Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "AddCustomersToGroupScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

.field private final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexGroupLoader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 85
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    .line 87
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 88
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    return-void
.end method

.method private formatMessage(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_customers_will_be_added_to_group_one:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_customers_will_be_added_to_group_many_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 154
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 156
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public synthetic lambda$null$0$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;Lkotlin/Unit;)V
    .locals 1

    .line 128
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->isChecked()Z

    move-result p5

    if-nez p5, :cond_1

    .line 129
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p5

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    .line 130
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/crm/rows/CheckableRow;

    const/4 p5, 0x0

    invoke-virtual {p3, p5}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {p3, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 134
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 136
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    invoke-interface {p1, p4}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;->setGroupToAddTo(Lcom/squareup/protos/client/rolodex/Group;)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$null$1$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;)Lrx/Subscription;
    .locals 8

    .line 126
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->onClicked()Lrx/Observable;

    move-result-object v0

    new-instance v7, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;)V

    .line 127
    invoke-virtual {v0, v7}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 113
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->clearGroupRows()V

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/squareup/protos/client/rolodex/GroupType;

    .line 115
    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p5, v1}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object p5

    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_0
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/protos/client/rolodex/Group;

    .line 116
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->addGroupRow()Lcom/squareup/ui/crm/rows/CheckableRow;

    move-result-object v1

    .line 117
    iget-object v2, v7, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/crm/rows/CheckableRow;->showTitle(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v2, v7, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-static {v2, p2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    invoke-virtual {p3, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 121
    invoke-virtual {v1, v0}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 122
    invoke-virtual {p4, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 125
    :cond_0
    new-instance v8, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;

    move-object v2, v8

    move-object v3, p0

    move-object v4, v1

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$OeZfmdFgLvb6Jw9dMXwd58dWvJ8;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;)V

    invoke-static {v1, v8}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public synthetic lambda$null$4$AddCustomersToGroupScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;->showCreateGroupScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$3$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;)Lio/reactivex/disposables/Disposable;
    .locals 8

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->success()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v7, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$yspGnxRJqt-lWJffDAHNy3aHEAI;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 112
    invoke-virtual {v0, v7}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;)Lrx/Subscription;
    .locals 1

    .line 145
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->onCreateGroupClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$rJBiv56LORsjhZKRgfm_nON2aB4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$rJBiv56LORsjhZKRgfm_nON2aB4;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;)V

    .line 146
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 7

    .line 92
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;

    .line 94
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v4

    .line 96
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/applet/R$string;->crm_select_group_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$eIj5Ldfxxu4NLLk1VdnPP191iCE;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$eIj5Ldfxxu4NLLk1VdnPP191iCE;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;)V

    invoke-virtual {v4, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->done:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 100
    invoke-virtual {v4, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$upLy75k6XW9YuObzOlqO2v1olMw;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$upLy75k6XW9YuObzOlqO2v1olMw;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;)V

    invoke-virtual {v4, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/ContactSet;->contact_count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->formatMessage(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->showMessage(Ljava/lang/String;)V

    .line 105
    new-instance v5, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v5}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;->getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;

    .line 107
    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;->getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v3, v0

    .line 110
    new-instance v6, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-static {p1, v6}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 144
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$WUzw5aNYRUKrqDd9-qVWImaZ62w;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$WUzw5aNYRUKrqDd9-qVWImaZ62w;-><init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
