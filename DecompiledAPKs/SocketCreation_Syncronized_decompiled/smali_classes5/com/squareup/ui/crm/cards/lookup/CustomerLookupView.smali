.class public Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;
.super Landroid/widget/LinearLayout;
.source "CustomerLookupView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$Component;,
        Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$SharedScope;
    }
.end annotation


# instance fields
.field private final onSearchClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchBox:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->onSearchClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 47
    const-class p2, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$Component;->inject(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;)V

    .line 48
    sget p2, Lcom/squareup/crm/R$layout;->crm_customer_lookup_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 49
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->setOrientation(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->onSearchClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 106
    sget v0, Lcom/squareup/crm/R$id;->crm_search_box:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchBox:Lcom/squareup/ui/XableEditText;

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 76
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->presenter:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->presenter:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->dropView(Ljava/lang/Object;)V

    .line 82
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 54
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->bindViews()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchBox:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$1;-><init>(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchBox:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$2;-><init>(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 86
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSearchClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->onSearchClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public searchTerm()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->presenter:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->searchTerm()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchBox:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public setSearchTerm(Ljava/lang/String;)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->presenter:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->setSearchTerm(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Ljava/lang/String;)V

    return-void
.end method
