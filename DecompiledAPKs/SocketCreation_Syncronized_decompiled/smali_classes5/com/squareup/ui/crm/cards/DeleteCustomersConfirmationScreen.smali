.class public Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "DeleteCustomersConfirmationScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Factory;,
        Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;,
        Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$DeleteCustomersConfirmationScreen$CNME_8I1Ha06gjugVdQYZNKfk5c;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$DeleteCustomersConfirmationScreen$CNME_8I1Ha06gjugVdQYZNKfk5c;

    .line 83
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;
    .locals 1

    .line 84
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 85
    new-instance v0, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 78
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_BULK_DELETE_CONFIRM:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method
