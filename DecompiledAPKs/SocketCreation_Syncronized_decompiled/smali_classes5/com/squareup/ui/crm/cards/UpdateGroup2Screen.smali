.class public Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "UpdateGroup2Screen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Component;,
        Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;,
        Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 293
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$xGY5Jx8GrGJSoPA1lo_uuyY6j1Y;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2Screen$xGY5Jx8GrGJSoPA1lo_uuyY6j1Y;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;
    .locals 1

    .line 294
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 295
    new-instance v0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 289
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_GROUPS_UPDATE_GROUP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 71
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_update_group2_view:I

    return v0
.end method
