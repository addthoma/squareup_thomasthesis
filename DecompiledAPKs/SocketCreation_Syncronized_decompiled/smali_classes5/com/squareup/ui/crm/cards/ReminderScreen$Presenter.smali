.class Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ReminderScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ReminderScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/ReminderView;",
        ">;"
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final locale:Ljava/util/Locale;

.field private final reminder:Lcom/squareup/protos/client/rolodex/Reminder;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;Ljava/util/Locale;Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;

    .line 75
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->locale:Ljava/util/Locale;

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 77
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->clock:Lcom/squareup/util/Clock;

    .line 79
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;->getReminder()Lcom/squareup/protos/client/rolodex/Reminder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    return-void
.end method

.method private static toDateTime(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/DateTime;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long v1, v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$null$1$ReminderScreen$Presenter(Lcom/squareup/ui/crm/cards/ReminderView;Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;)V
    .locals 5

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 100
    sget-object v1, Lcom/squareup/ui/crm/cards/ReminderScreen$1;->$SwitchMap$com$squareup$ui$crm$cards$ReminderScreen$ReminderOption:[I

    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v3, 0x3

    const/4 v4, 0x2

    if-eq v1, v4, :cond_1

    if-ne v1, v3, :cond_0

    .line 108
    invoke-virtual {v0, v4, v2}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 111
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unknown option: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 105
    :cond_1
    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    :cond_2
    const/4 p2, 0x6

    .line 102
    invoke-virtual {v0, p2, v2}, Ljava/util/Calendar;->add(II)V

    .line 114
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ReminderView;->getCalendar()Ljava/util/Calendar;

    move-result-object p2

    const/16 v1, 0xb

    .line 115
    invoke-virtual {p2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    .line 116
    invoke-virtual {p2, v1}, Ljava/util/Calendar;->get(I)I

    move-result p2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    const/16 p2, 0xd

    const/4 v1, 0x0

    .line 117
    invoke-virtual {v0, p2, v1}, Ljava/util/Calendar;->set(II)V

    .line 118
    invoke-static {v0}, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->toDateTime(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/ReminderView;->setTime(Ljava/util/Date;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$ReminderScreen$Presenter(Lcom/squareup/ui/crm/cards/ReminderView;)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;

    new-instance v1, Lcom/squareup/protos/client/rolodex/Reminder$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/Reminder$Builder;-><init>()V

    .line 92
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ReminderView;->getCalendar()Ljava/util/Calendar;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->toDateTime(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/Reminder$Builder;->scheduled_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/Reminder$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Reminder$Builder;->build()Lcom/squareup/protos/client/rolodex/Reminder;

    move-result-object p1

    .line 91
    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;->closeReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V

    return-void
.end method

.method public synthetic lambda$onLoad$2$ReminderScreen$Presenter(Lcom/squareup/ui/crm/cards/ReminderView;)Lrx/Subscription;
    .locals 2

    .line 97
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ReminderView;->onReminderOptionClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ReminderScreen$Presenter$JiIBrxTbysHsUGbaSzZIT5VGon4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ReminderScreen$Presenter$JiIBrxTbysHsUGbaSzZIT5VGon4;-><init>(Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;Lcom/squareup/ui/crm/cards/ReminderView;)V

    .line 98
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 83
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/ReminderView;

    .line 86
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/ReminderView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 87
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_reminder:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 88
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$17Lhl5B8brCSMYI9CxC7yh17Gsk;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$17Lhl5B8brCSMYI9CxC7yh17Gsk;-><init>(Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;)V

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 89
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 90
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ReminderScreen$Presenter$Kmc39s1FMyZvlGc4qsY2u7L6ykI;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ReminderScreen$Presenter$Kmc39s1FMyZvlGc4qsY2u7L6ykI;-><init>(Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;Lcom/squareup/ui/crm/cards/ReminderView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 96
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ReminderScreen$Presenter$bJBOVXvKUvO_dbKZ92dkocCPm1A;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ReminderScreen$Presenter$bJBOVXvKUvO_dbKZ92dkocCPm1A;-><init>(Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;Lcom/squareup/ui/crm/cards/ReminderView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_0

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    if-eqz p1, :cond_0

    .line 122
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Reminder;->scheduled_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->locale:Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/ReminderView;->setTime(Ljava/util/Date;)V

    :cond_0
    return-void
.end method

.method onRemoveReminderPressed()V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;->closeReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V

    return-void
.end method
