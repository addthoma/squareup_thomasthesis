.class public final synthetic Lcom/squareup/ui/crm/cards/-$$Lambda$QjzVW86Vdz7Gdmx0501clZmKeP0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$QjzVW86Vdz7Gdmx0501clZmKeP0;->f$0:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$QjzVW86Vdz7Gdmx0501clZmKeP0;->f$0:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;->showMergingDuplicatesScreen(I)V

    return-void
.end method
