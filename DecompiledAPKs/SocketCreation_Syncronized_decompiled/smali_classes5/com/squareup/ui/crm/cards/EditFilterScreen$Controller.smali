.class public interface abstract Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;
.super Ljava/lang/Object;
.source "EditFilterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/EditFilterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract commitEditFilterCard()V
.end method

.method public abstract dismissEditFilterCard()V
.end method

.method public abstract getFilter()Lcom/squareup/protos/client/rolodex/Filter;
.end method

.method public abstract isFilterBeingCreated()Z
.end method

.method public abstract setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
.end method
