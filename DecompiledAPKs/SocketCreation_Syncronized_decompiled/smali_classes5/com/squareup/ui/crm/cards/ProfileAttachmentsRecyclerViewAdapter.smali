.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ProfileAttachmentsRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;)V
    .locals 1

    .line 23
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->attachments:Ljava/util/List;

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;)Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    return-object p0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->attachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->onBindViewHolder(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;I)V
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->attachments:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Attachment;

    .line 35
    iget-object v0, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 37
    iget-object v1, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v2, p2, Lcom/squareup/protos/client/rolodex/Attachment;->file_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 38
    iget-object v1, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    iget-object v3, p2, Lcom/squareup/protos/client/rolodex/Attachment;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-interface {v2, v3}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;->getFormattedDateTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v1, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 40
    iget-object v1, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v1}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v1

    sget v2, Lcom/squareup/crm/R$drawable;->crm_action_overflow:I

    const/4 v3, 0x0

    .line 42
    invoke-static {v0, v2, v3}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 41
    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 43
    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    sget-object v2, Lcom/squareup/protos/client/rolodex/Attachment$FileType;->IMAGE:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    if-ne v1, v2, :cond_0

    .line 44
    iget-object p2, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->getStartGlyphView()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p2

    sget v1, Lcom/squareup/crm/R$drawable;->crm_file_image:I

    .line 46
    invoke-static {v0, v1, v3}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 45
    invoke-virtual {p2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 47
    iget-object p2, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 52
    :cond_0
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Attachment;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment$FileType;->PDF:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    if-ne p2, v1, :cond_1

    .line 53
    iget-object p2, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->getStartGlyphView()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p2

    sget v1, Lcom/squareup/crm/R$drawable;->crm_file_document:I

    .line 55
    invoke-static {v0, v1, v3}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 54
    invoke-virtual {p2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    iget-object p2, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$2;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    :cond_1
    :goto_0
    iget-object p2, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisibility(I)V

    .line 64
    iget-object p2, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 66
    iget-object p2, p1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->smartLineRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$3;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;)V

    invoke-virtual {p2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;
    .locals 0

    .line 29
    invoke-static {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    .line 30
    new-instance p2, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-object p2
.end method

.method public setAttachments(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;)V"
        }
    .end annotation

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->attachments:Ljava/util/List;

    return-void
.end method
