.class public final Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;
.super Ljava/lang/Object;
.source "ContactListPresenterV2_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->resProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;)Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;-><init>(Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/PhoneNumberHelper;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->newInstance(Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;)Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2_Factory;->get()Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    move-result-object v0

    return-object v0
.end method
