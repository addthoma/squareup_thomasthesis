.class Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;
.super Lmortar/Presenter;
.source "VoidingCouponsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L

.field static final MIN_LATENCY_SECONDS:J = 0x1L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final received:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 44
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->runner:Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 54
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method private formatDoneTitle(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_void_result_single:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_void_result_plural:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 121
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/Long;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 58
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$VoidingCouponsPresenter(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 86
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 87
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->runner:Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;

    invoke-interface {p2}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;->getCouponsToBeVoided()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->formatDoneTitle(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->showText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->runner:Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;->success()V

    return-void
.end method

.method public synthetic lambda$null$2$VoidingCouponsPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;

    if-eqz p1, :cond_0

    .line 96
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_void_error:I

    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 102
    :cond_1
    sget-object p3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2, p3}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 103
    invoke-virtual {p2, p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->showText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$3$VoidingCouponsPresenter(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 82
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->hideProgress()V

    .line 84
    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$jN8QaLh-vXPZOa6PgC0T-A6r094;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$jN8QaLh-vXPZOa6PgC0T-A6r094;-><init>(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$Cjrwg1AC3IRuhaBFHft4h4_hGiU;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$Cjrwg1AC3IRuhaBFHft4h4_hGiU;-><init>(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {p1, p2, v1, v2, v0}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$5$VoidingCouponsPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->runner:Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;->closeVoidingCouponsScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$4$VoidingCouponsPresenter(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$p4gMdEruilCQ479DRHaqMqUNrEw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$p4gMdEruilCQ479DRHaqMqUNrEw;-><init>(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;)V

    .line 81
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$VoidingCouponsPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$uLC2aI1dg6XV_E3w2johWv6Hk6k;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$uLC2aI1dg6XV_E3w2johWv6Hk6k;-><init>(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;)V

    .line 113
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 62
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->runner:Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;

    .line 65
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;->getCouponsToBeVoided()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->voidCoupons(Ljava/util/List;)Lio/reactivex/Single;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    .line 68
    invoke-static {v3, v4, v1, v2}, Lio/reactivex/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$0N6zi87um2dq1viGp2IBgVQvEqs;->INSTANCE:Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$0N6zi87um2dq1viGp2IBgVQvEqs;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 73
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_void_voiding:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->showText(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$eTIRMHaKD0lrCIKDqAluZ59EtTQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$eTIRMHaKD0lrCIKDqAluZ59EtTQ;-><init>(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 111
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$FEYI1JDRksyBgzxCPq5oDPMI9yA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$VoidingCouponsPresenter$FEYI1JDRksyBgzxCPq5oDPMI9yA;-><init>(Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
