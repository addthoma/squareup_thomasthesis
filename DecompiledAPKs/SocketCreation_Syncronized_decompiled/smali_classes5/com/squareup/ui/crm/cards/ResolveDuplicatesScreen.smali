.class public Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ResolveDuplicatesScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;,
        Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Component;
    }
.end annotation


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_RESOLVE_DUPLICATES:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 57
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_resolve_duplicates_card_view:I

    return v0
.end method
