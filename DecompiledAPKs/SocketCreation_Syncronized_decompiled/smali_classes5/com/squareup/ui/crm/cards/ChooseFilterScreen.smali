.class public Lcom/squareup/ui/crm/cards/ChooseFilterScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ChooseFilterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;,
        Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/ChooseFilterScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 187
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$f0Y87ejntNtiLYP_h28vgJJjLGo;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$f0Y87ejntNtiLYP_h28vgJJjLGo;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/ChooseFilterScreen;
    .locals 1

    .line 188
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 189
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 183
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 175
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_FILTER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 179
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 171
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_choose_filter_card_view:I

    return v0
.end method
