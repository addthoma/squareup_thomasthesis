.class Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;
.super Lmortar/Presenter;
.source "AddingCustomersToGroupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L

.field static final MIN_LATENCY_SECONDS:J = 0x1L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final response:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 43
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->res:Lcom/squareup/util/Res;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 53
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method private formatAddedCustomersTitle(I)Ljava/lang/String;
    .locals 3

    const-string v0, "group"

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_added_customers_to_group_one_format:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    .line 119
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 120
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/applet/R$string;->crm_added_customers_to_group_many_format:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "number"

    .line 123
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    .line 124
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 125
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formatAddingCustomersTitle(I)Ljava/lang/String;
    .locals 3

    const-string v0, "group"

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_adding_customers_to_group_one_format:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    .line 106
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 107
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/applet/R$string;->crm_adding_customers_to_group_many_format:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "number"

    .line 110
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    .line 111
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/Long;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 57
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$AddingCustomersToGroupPresenter(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 84
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 85
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;->batch_action_status:Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->total_success_count:Ljava/lang/Integer;

    .line 86
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->formatAddedCustomersTitle(I)Ljava/lang/String;

    move-result-object p2

    .line 85
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->showText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->success()V

    return-void
.end method

.method public synthetic lambda$null$2$AddingCustomersToGroupPresenter(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 90
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 91
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_adding_customers_to_group_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->showText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$3$AddingCustomersToGroupPresenter(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 81
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->hideProgress()V

    .line 83
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$wzPrQEOvgYO9v2uGDG5_9q-h7z8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$wzPrQEOvgYO9v2uGDG5_9q-h7z8;-><init>(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$DqQDoRAs_AAZNc-miGQMKJupkpI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$DqQDoRAs_AAZNc-miGQMKJupkpI;-><init>(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {p1, p2, v1, v2, v0}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$5$AddingCustomersToGroupPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->closeAddingCustomersToGroupScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$4$AddingCustomersToGroupPresenter(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$RSATlM9Ioz8ewW9A9EqhetSbnwk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$RSATlM9Ioz8ewW9A9EqhetSbnwk;-><init>(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)V

    .line 80
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$AddingCustomersToGroupPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$Q_Vy_oB5MEZDM4WS0_DvQXVribk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$Q_Vy_oB5MEZDM4WS0_DvQXVribk;-><init>(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;)V

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 61
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    .line 65
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    .line 66
    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    .line 65
    invoke-interface {v0, v1, v2}, Lcom/squareup/crm/RolodexServiceHelper;->addContactsToGroup(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    .line 67
    invoke-static {v3, v4, v1, v2}, Lio/reactivex/Single;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$sPK-ShGrvOjbAJbK_DsY8WtpsRU;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$sPK-ShGrvOjbAJbK_DsY8WtpsRU;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->zipWith(Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 72
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->controller:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/ContactSet;->contact_count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->formatAddingCustomersTitle(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->showText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$saCydh5OLESlITDu53IfUGEBn70;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$saCydh5OLESlITDu53IfUGEBn70;-><init>(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$dlh4xwNW7PLpI_w867fX0ix1WZw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AddingCustomersToGroupPresenter$dlh4xwNW7PLpI_w867fX0ix1WZw;-><init>(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
