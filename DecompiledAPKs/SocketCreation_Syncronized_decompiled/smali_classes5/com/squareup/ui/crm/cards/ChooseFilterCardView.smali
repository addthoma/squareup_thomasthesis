.class public Lcom/squareup/ui/crm/cards/ChooseFilterCardView;
.super Landroid/widget/LinearLayout;
.source "ChooseFilterCardView.java"


# instance fields
.field private filterContainer:Landroid/widget/LinearLayout;

.field presenter:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private searchBar:Lcom/squareup/ui/XableEditText;

.field private searchEmptyMessage:Lcom/squareup/widgets/MessageView;

.field private final searchText:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchText:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 39
    const-class p2, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Component;->inject(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchText:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 69
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method addFilterRow()Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 2

    .line 93
    sget v0, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->filterContainer:Landroid/widget/LinearLayout;

    .line 94
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->filterContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 59
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->presenter:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->presenter:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 65
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 43
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 45
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->progressBar:Landroid/widget/ProgressBar;

    .line 46
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_filter_search_box:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 47
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_filter_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->filterContainer:Landroid/widget/LinearLayout;

    .line 48
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_filter_search_empty:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchEmptyMessage:Lcom/squareup/widgets/MessageView;

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchBar:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/ChooseFilterCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView$1;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchText:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method removeAllFilterRows()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->filterContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method searchText()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchText:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method showFilterContainer(Z)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->filterContainer:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->progressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showSearchEmptyMessage(Z)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchEmptyMessage:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
