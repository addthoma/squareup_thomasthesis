.class Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CustomerInvoiceCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;)V
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$1;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 194
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$1;->this$1:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$300(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;->showInvoiceReadOnlyScreen(Ljava/lang/String;)V

    return-void
.end method
