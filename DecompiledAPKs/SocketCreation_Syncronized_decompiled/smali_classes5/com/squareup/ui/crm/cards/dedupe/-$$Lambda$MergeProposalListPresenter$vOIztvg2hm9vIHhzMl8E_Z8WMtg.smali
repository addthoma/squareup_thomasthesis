.class public final synthetic Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

.field private final synthetic f$1:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;

.field private final synthetic f$2:I

.field private final synthetic f$3:Lcom/squareup/protos/client/rolodex/MergeProposal;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;ILcom/squareup/protos/client/rolodex/MergeProposal;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$0:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$1:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;

    iput p3, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$2:I

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$3:Lcom/squareup/protos/client/rolodex/MergeProposal;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$0:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$1:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;

    iget v2, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$2:I

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;->f$3:Lcom/squareup/protos/client/rolodex/MergeProposal;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->lambda$bind$20$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;ILcom/squareup/protos/client/rolodex/MergeProposal;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
