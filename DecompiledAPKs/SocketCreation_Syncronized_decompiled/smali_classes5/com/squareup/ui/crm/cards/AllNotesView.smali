.class public Lcom/squareup/ui/crm/cards/AllNotesView;
.super Landroid/widget/LinearLayout;
.source "AllNotesView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field presenter:Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rows:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const-class p2, Lcom/squareup/ui/crm/cards/AllNotesScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/AllNotesScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Component;->inject(Lcom/squareup/ui/crm/cards/AllNotesView;)V

    return-void
.end method


# virtual methods
.method addRow()Lcom/squareup/ui/crm/rows/NoteRow;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/ui/crm/rows/NoteRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AllNotesView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/rows/NoteRow;-><init>(Landroid/content/Context;)V

    .line 62
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllNotesView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method clearRows()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesView;->presenter:Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesView;->presenter:Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 33
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 34
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_all_notes_rows:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesView;->rows:Landroid/widget/LinearLayout;

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 49
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
