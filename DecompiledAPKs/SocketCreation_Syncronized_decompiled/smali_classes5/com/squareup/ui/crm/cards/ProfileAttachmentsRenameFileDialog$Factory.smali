.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory;
.super Ljava/lang/Object;
.source "ProfileAttachmentsRenameFileDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/widget/EditText;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 53
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->renameFile(Landroid/content/DialogInterface;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 55
    sget p2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_rename_file_cancelled:I

    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    .line 56
    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    .line 57
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 35
    const-class v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 36
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 37
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;->runner()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-result-object v0

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_rename_file_message:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 41
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getCurrentFilename()Ljava/lang/String;

    move-result-object v2

    const-string v3, "filename"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 42
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 44
    sget v2, Lcom/squareup/profileattachments/R$layout;->crm_profile_attachments_rename_file:I

    const/4 v3, 0x0

    .line 45
    invoke-static {p1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 46
    sget v3, Lcom/squareup/profileattachments/R$id;->rename_edit_text:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 48
    new-instance v4, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v4, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v5, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_rename_file_title:I

    .line 49
    invoke-virtual {v4, v5}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v4

    .line 50
    invoke-virtual {v4, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 51
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_rename:I

    new-instance v4, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsRenameFileDialog$Factory$aAw7GsIQ2cv5OX50Ja40lwxrE3E;

    invoke-direct {v4, v0, v3}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsRenameFileDialog$Factory$aAw7GsIQ2cv5OX50Ja40lwxrE3E;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/widget/EditText;)V

    .line 52
    invoke-virtual {v1, v2, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsRenameFileDialog$Factory$CGTHEbOdyrwKK6nxs40ogOeY5Q4;

    invoke-direct {v2, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsRenameFileDialog$Factory$CGTHEbOdyrwKK6nxs40ogOeY5Q4;-><init>(Landroid/content/Context;)V

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 61
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 64
    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$1;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 71
    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$2;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 80
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
