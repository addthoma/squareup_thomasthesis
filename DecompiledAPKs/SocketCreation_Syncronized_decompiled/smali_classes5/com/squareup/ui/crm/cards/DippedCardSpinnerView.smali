.class public Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;
.super Landroid/widget/LinearLayout;
.source "DippedCardSpinnerView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field presenter:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const-class p2, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Component;->inject(Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 48
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;->presenter:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;->presenter:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;->presenter:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 29
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;->bindViews()V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
