.class public final Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$7;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ChooseCustomerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseCustomerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseCustomerCoordinator.kt\ncom/squareup/ui/crm/ChooseCustomerCoordinator$attach$7\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,167:1\n45#2:168\n17#2,22:169\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseCustomerCoordinator.kt\ncom/squareup/ui/crm/ChooseCustomerCoordinator$attach$7\n*L\n92#1:168\n92#1,22:169\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/crm/ChooseCustomerCoordinator$attach$7",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 8

    const-string v0, "editable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$7;->this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 168
    check-cast p1, Ljava/lang/CharSequence;

    .line 170
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    move v4, v1

    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_0
    if-gt v1, v4, :cond_5

    if-nez v5, :cond_0

    move v6, v1

    goto :goto_1

    :cond_0
    move v6, v4

    .line 175
    :goto_1
    invoke-interface {p1, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    if-gt v6, v7, :cond_1

    const/4 v6, 0x1

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    :goto_2
    if-nez v5, :cond_3

    if-nez v6, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    if-nez v6, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_5
    :goto_3
    add-int/2addr v4, v2

    .line 190
    invoke-interface {p1, v1, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    .line 168
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->setSearchTerm(Ljava/lang/String;)V

    return-void
.end method
