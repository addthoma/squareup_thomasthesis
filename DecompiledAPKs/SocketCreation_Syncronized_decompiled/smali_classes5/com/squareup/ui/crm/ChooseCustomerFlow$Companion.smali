.class public final Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;
.super Ljava/lang/Object;
.source "ChooseCustomerFlow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/ChooseCustomerFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JF\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;",
        "",
        "()V",
        "start",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parentKey",
        "chooseCustomerResultKey",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
        "titleResId",
        "",
        "createNewCustomerValidationType",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;",
        "crmScopeType",
        "Lcom/squareup/ui/crm/flow/CrmScopeType;",
        "isFirstCardScreen",
        "",
        "sortByRecentContacts",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 10
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "parentKey"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseCustomerResultKey"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerScreen;

    .line 116
    new-instance v9, Lcom/squareup/ui/crm/ChooseCustomerScope;

    move-object v1, v9

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/crm/ChooseCustomerScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)V

    .line 115
    invoke-direct {v0, v9}, Lcom/squareup/ui/crm/ChooseCustomerScreen;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScope;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method
