.class public Lcom/squareup/ui/ApiActivity;
.super Lcom/squareup/ui/SquareActivity;
.source "ApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ApiActivity$Component;
    }
.end annotation


# instance fields
.field private final activityInstanceCreatedAt:J

.field apiActivityController:Lcom/squareup/api/ApiActivityController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 43
    sget-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->NO_AUTH_NEEDED:Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-direct {p0, v0}, Lcom/squareup/ui/SquareActivity;-><init>(Lcom/squareup/ui/SquareActivity$Preconditions;)V

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/ApiActivity;->activityInstanceCreatedAt:J

    return-void
.end method

.method static synthetic lambda$showInvalidStartDialog$0(Ljava/lang/Runnable;Landroid/content/DialogInterface;)V
    .locals 0

    .line 89
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method static synthetic lambda$showInvalidStartDialog$1(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 91
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method


# virtual methods
.method protected doBackPressed()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity;->apiActivityController:Lcom/squareup/api/ApiActivityController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiActivityController;->doBackPressed()V

    return-void
.end method

.method protected doCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 52
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doCreate(Landroid/os/Bundle;)V

    .line 53
    sget v0, Lcom/squareup/common/bootstrap/R$layout;->activity_api:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/ApiActivity;->setContentView(I)V

    .line 55
    sget v0, Lcom/squareup/common/bootstrap/R$id;->pos_api_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/ProgressBar;

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/ApiActivity;->apiActivityController:Lcom/squareup/api/ApiActivityController;

    iget-wide v4, p0, Lcom/squareup/ui/ApiActivity;->activityInstanceCreatedAt:J

    move-object v2, p0

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/api/ApiActivityController;->onCreate(Lcom/squareup/ui/ApiActivity;Landroid/widget/ProgressBar;JLandroid/os/Bundle;)V

    return-void
.end method

.method protected doSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 61
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doSaveInstanceState(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity;->apiActivityController:Lcom/squareup/api/ApiActivityController;

    invoke-virtual {v0, p1}, Lcom/squareup/api/ApiActivityController;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected inject(Lmortar/MortarScope;)V
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/ui/ApiActivity$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ApiActivity$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ApiActivity$Component;->inject(Lcom/squareup/ui/ApiActivity;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 76
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onDestroy()V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity;->apiActivityController:Lcom/squareup/api/ApiActivityController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiActivityController;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 66
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onPause()V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity;->apiActivityController:Lcom/squareup/api/ApiActivityController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiActivityController;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 71
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onResume()V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/ApiActivity;->apiActivityController:Lcom/squareup/api/ApiActivityController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiActivityController;->onResume()V

    return-void
.end method

.method public showInvalidStartDialog(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    if-ne p1, v0, :cond_0

    sget p1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_api_version_dialog_title:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_start_method_dialog_title:I

    .line 85
    :goto_0
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 88
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/-$$Lambda$ApiActivity$Zcee2Xcr8oaCr6PFBRWkWloWCqk;

    invoke-direct {p2, p3}, Lcom/squareup/ui/-$$Lambda$ApiActivity$Zcee2Xcr8oaCr6PFBRWkWloWCqk;-><init>(Ljava/lang/Runnable;)V

    .line 89
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v0, Lcom/squareup/ui/-$$Lambda$ApiActivity$pIJSOJCnF4HxrJZM0Kbb-wd4ovY;

    invoke-direct {v0, p3}, Lcom/squareup/ui/-$$Lambda$ApiActivity$pIJSOJCnF4HxrJZM0Kbb-wd4ovY;-><init>(Ljava/lang/Runnable;)V

    .line 90
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method
