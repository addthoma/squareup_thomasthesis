.class public Lcom/squareup/ui/DropDownContainer;
.super Landroid/widget/RelativeLayout;
.source "DropDownContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/DropDownContainer$SavedState;,
        Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;,
        Lcom/squareup/ui/DropDownContainer$DropDownListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_DROP_DOWN_DURATION_MS:I = 0x96


# instance fields
.field protected coveredContent:Landroid/view/View;

.field protected dropDownContent:Landroid/view/ViewGroup;

.field private dropDownDuration:I

.field private final glassCancel:Z

.field private hideDropDown:Landroid/animation/AnimatorSet;

.field private initialMotionX:F

.field private initialMotionY:F

.field protected listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

.field private scrimColor:I

.field private scrimOpacity:F

.field private scrimPaint:Landroid/graphics/Paint;

.field private showDropDown:Landroid/animation/AnimatorSet;

.field private slop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 108
    sget v0, Lcom/squareup/widgets/R$attr;->dropDownContainerStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/DropDownContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 112
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    .line 99
    iput v0, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    .line 113
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->inject()V

    .line 114
    sget-object v0, Lcom/squareup/widgets/R$styleable;->DropDownContainer:[I

    const/4 v1, 0x0

    .line 115
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 116
    sget p2, Lcom/squareup/widgets/R$styleable;->DropDownContainer_dropDownDuration:I

    const/16 p3, 0x96

    .line 117
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/DropDownContainer;->dropDownDuration:I

    .line 118
    sget p2, Lcom/squareup/widgets/R$styleable;->DropDownContainer_glassCancel:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/DropDownContainer;->glassCancel:Z

    .line 119
    sget p2, Lcom/squareup/widgets/R$styleable;->DropDownContainer_scrimColor:I

    const/4 p3, -0x1

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/DropDownContainer;->scrimColor:I

    .line 120
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 122
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimPaint:Landroid/graphics/Paint;

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/ui/DropDownContainer;->scrimColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 124
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/DropDownContainer;->slop:I

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/DropDownContainer;Z)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/DropDownContainer;->sendAccessibilityEventForShown(Z)V

    return-void
.end method

.method private static hasOpaqueBackground(Landroid/view/View;)Z
    .locals 2

    .line 458
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 460
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result p0

    const/4 v1, -0x1

    if-ne p0, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isDropDownClosing()Z
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isDropDownOpening()Z
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private sendAccessibilityEventForShown(Z)V
    .locals 2

    .line 445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 446
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 447
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    .line 448
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 450
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_1

    sget p1, Lcom/squareup/widgets/R$string;->drop_down_accessibility_action_expanded:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/widgets/R$string;->drop_down_accessibility_action_collapsed:I

    :goto_0
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 454
    invoke-static {p0, v0}, Lcom/squareup/accessibility/Accessibility;->announceForAccessibility(Landroid/view/View;Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public closeDropDown()V
    .locals 7

    .line 327
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownClosing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 329
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownOpening()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    .line 334
    iput v0, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setY(F)V

    .line 338
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    new-array v3, v2, [F

    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getDropDownHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    const/4 v5, 0x0

    aput v4, v3, v5

    const-string/jumbo v4, "y"

    invoke-static {v0, v4, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 339
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 v3, 0x2

    new-array v4, v3, [F

    .line 340
    iget v6, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    aput v6, v4, v5

    aput v1, v4, v2

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 341
    new-instance v4, Lcom/squareup/ui/-$$Lambda$DropDownContainer$OHG96Qfzg5eGPb5hVaGv9Efx684;

    invoke-direct {v4, p0}, Lcom/squareup/ui/-$$Lambda$DropDownContainer$OHG96Qfzg5eGPb5hVaGv9Efx684;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 348
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    .line 349
    iget-object v4, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v5

    aput-object v0, v3, v2

    invoke-virtual {v4, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 351
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/squareup/ui/DropDownContainer$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/DropDownContainer$2;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 362
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    iget v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 364
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 9

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 147
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getHeight()I

    move-result v2

    .line 148
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    if-eqz v0, :cond_2

    .line 150
    iget-object v4, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-static {v4}, Lcom/squareup/ui/DropDownContainer;->hasOpaqueBackground(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 152
    iget-object v4, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getY()F

    move-result v4

    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getDropDownHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 154
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getWidth()I

    move-result v5

    invoke-virtual {p1, v1, v4, v5, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    move v1, v4

    .line 157
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p2

    .line 158
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 160
    iget p3, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    const/4 p4, 0x0

    cmpl-float p4, p3, p4

    if-lez p4, :cond_3

    iget p4, p0, Lcom/squareup/ui/DropDownContainer;->scrimColor:I

    const/4 v3, -0x1

    if-eq p4, v3, :cond_3

    if-eqz v0, :cond_3

    const/high16 v0, -0x1000000

    and-int/2addr v0, p4

    ushr-int/lit8 v0, v0, 0x18

    int-to-float v0, v0

    mul-float v0, v0, p3

    float-to-int p3, v0

    shl-int/lit8 p3, p3, 0x18

    const v0, 0xffffff

    and-int/2addr p4, v0

    or-int/2addr p3, p4

    .line 164
    iget-object p4, p0, Lcom/squareup/ui/DropDownContainer;->scrimPaint:Landroid/graphics/Paint;

    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v4, 0x0

    int-to-float v5, v1

    .line 165
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getWidth()I

    move-result p3

    int-to-float v6, p3

    int-to-float v7, v2

    iget-object v8, p0, Lcom/squareup/ui/DropDownContainer;->scrimPaint:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    return p2
.end method

.method protected getDropDownHeight()I
    .locals 1

    .line 437
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    return v0
.end method

.method protected inject()V
    .locals 0

    return-void
.end method

.method protected installGlass()V
    .locals 4

    .line 402
    iget-boolean v0, p0, Lcom/squareup/ui/DropDownContainer;->glassCancel:Z

    if-nez v0, :cond_0

    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getY()F

    move-result v0

    .line 405
    new-instance v1, Lcom/squareup/ui/-$$Lambda$LBLKORISj0QOFeSXw3Y_QUo4vqE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/-$$Lambda$LBLKORISj0QOFeSXw3Y_QUo4vqE;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    .line 407
    iget-object v2, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setY(F)V

    .line 412
    iget-object v2, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    if-gtz v2, :cond_1

    .line 413
    iget-object v2, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    new-instance v3, Lcom/squareup/ui/-$$Lambda$DropDownContainer$QgV96Dphj0ZieRxLgeO8TTkJHBM;

    invoke-direct {v3, p0, v1, v0}, Lcom/squareup/ui/-$$Lambda$DropDownContainer$QgV96Dphj0ZieRxLgeO8TTkJHBM;-><init>(Lcom/squareup/ui/DropDownContainer;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;F)V

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    goto :goto_0

    .line 418
    :cond_1
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v1}, Lcom/squareup/widgets/glass/GlassConfirmController;->installGlass(Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    .line 419
    iget-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setY(F)V

    :goto_0
    return-void
.end method

.method public isDropDownVisible()Z
    .locals 1

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$closeDropDown$1$DropDownContainer(Landroid/animation/ValueAnimator;)V
    .locals 2

    .line 342
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    .line 343
    iput p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownSlide(Landroid/view/View;F)V

    .line 345
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->invalidate()V

    return-void
.end method

.method public synthetic lambda$installGlass$2$DropDownContainer(Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;FLandroid/view/View;II)V
    .locals 0

    .line 414
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object p3

    iget-object p4, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {p3, p4, p1}, Lcom/squareup/widgets/glass/GlassConfirmController;->installGlass(Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    .line 415
    iget-object p1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setY(F)V

    return-void
.end method

.method public synthetic lambda$openDropDown$0$DropDownContainer(Landroid/animation/ValueAnimator;)V
    .locals 2

    .line 299
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    .line 300
    iput p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz v0, :cond_0

    .line 302
    iget-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownSlide(Landroid/view/View;F)V

    .line 304
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->invalidate()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 139
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownClosing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 140
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownOpening()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 141
    :cond_1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 131
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0, v0}, Lcom/squareup/ui/DropDownContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/DropDownContainer;->coveredContent:Landroid/view/View;

    const/4 v1, 0x1

    .line 133
    invoke-virtual {p0, v1}, Lcom/squareup/ui/DropDownContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    .line 134
    invoke-virtual {p0, v0}, Lcom/squareup/ui/DropDownContainer;->setDropDownVisible(Z)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 172
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownVisible()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/squareup/ui/DropDownContainer;->glassCancel:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 174
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 177
    iget-object v3, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getY()F

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getDropDownHeight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 178
    invoke-static {p1}, Landroidx/core/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result p1

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz p1, :cond_3

    if-eq p1, v5, :cond_1

    goto :goto_0

    .line 191
    :cond_1
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    cmpl-float p1, p1, v4

    if-lez p1, :cond_4

    int-to-float p1, v3

    cmpl-float p1, v2, p1

    if-lez p1, :cond_4

    .line 192
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionX:F

    sub-float/2addr v0, p1

    .line 193
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionY:F

    sub-float/2addr v2, p1

    mul-float v0, v0, v0

    mul-float v2, v2, v2

    add-float/2addr v0, v2

    .line 194
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->slop:I

    mul-int p1, p1, p1

    int-to-float p1, p1

    cmpg-float p1, v0, p1

    if-gez p1, :cond_2

    .line 195
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    :cond_2
    return v5

    .line 182
    :cond_3
    iput v0, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionX:F

    .line 183
    iput v2, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionY:F

    .line 184
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    cmpl-float p1, p1, v4

    if-lez p1, :cond_4

    int-to-float p1, v3

    cmpl-float p1, v2, p1

    if-lez p1, :cond_4

    return v5

    :cond_4
    :goto_0
    return v1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 242
    check-cast p1, Lcom/squareup/ui/DropDownContainer$SavedState;

    .line 243
    invoke-virtual {p1}, Lcom/squareup/ui/DropDownContainer$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 244
    invoke-static {p1}, Lcom/squareup/ui/DropDownContainer$SavedState;->access$100(Lcom/squareup/ui/DropDownContainer$SavedState;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->setDropDownOpen()V

    goto :goto_0

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->setDropDownClosed()V

    :goto_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownClosing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 238
    :goto_0
    new-instance v1, Lcom/squareup/ui/DropDownContainer$SavedState;

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/ui/DropDownContainer$SavedState;-><init>(Landroid/os/Parcelable;ZLcom/squareup/ui/DropDownContainer$1;)V

    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/squareup/ui/DropDownContainer;->glassCancel:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 209
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 210
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 212
    iget-object v2, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getY()F

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getDropDownHeight()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 213
    invoke-static {p1}, Landroidx/core/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result p1

    const/4 v3, 0x1

    if-eqz p1, :cond_2

    if-eq p1, v3, :cond_1

    goto :goto_0

    .line 223
    :cond_1
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    const/4 v4, 0x0

    cmpl-float p1, p1, v4

    if-lez p1, :cond_3

    int-to-float p1, v2

    cmpl-float p1, v1, p1

    if-lez p1, :cond_3

    .line 224
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionX:F

    sub-float/2addr v0, p1

    .line 225
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionY:F

    sub-float/2addr v1, p1

    mul-float v0, v0, v0

    mul-float v1, v1, v1

    add-float/2addr v0, v1

    .line 226
    iget p1, p0, Lcom/squareup/ui/DropDownContainer;->slop:I

    mul-int p1, p1, p1

    int-to-float p1, p1

    cmpg-float p1, v0, p1

    if-gez p1, :cond_3

    .line 227
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    goto :goto_0

    .line 217
    :cond_2
    iput v0, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionX:F

    .line 218
    iput v1, p0, Lcom/squareup/ui/DropDownContainer;->initialMotionY:F

    :cond_3
    :goto_0
    return v3

    :cond_4
    :goto_1
    const/4 p1, 0x0

    return p1
.end method

.method public openDropDown()V
    .locals 6

    .line 280
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownOpening()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 282
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownClosing()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->hideDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0

    .line 287
    :cond_1
    iput v1, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getDropDownHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setY(F)V

    .line 291
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->installGlass()V

    const/4 v0, 0x1

    .line 292
    invoke-virtual {p0, v0}, Lcom/squareup/ui/DropDownContainer;->setDropDownVisible(Z)V

    .line 294
    iget-object v2, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    new-array v3, v0, [F

    const/4 v4, 0x0

    aput v1, v3, v4

    const-string/jumbo v1, "y"

    invoke-static {v2, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 295
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 v2, 0x2

    new-array v3, v2, [F

    .line 297
    iget v5, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    aput v5, v3, v4

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v3, v0

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 298
    new-instance v5, Lcom/squareup/ui/-$$Lambda$DropDownContainer$m4CeSXLuc2-Haxwz3G-5HQR2Bbo;

    invoke-direct {v5, p0}, Lcom/squareup/ui/-$$Lambda$DropDownContainer$m4CeSXLuc2-Haxwz3G-5HQR2Bbo;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    invoke-virtual {v3, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 307
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    .line 308
    iget-object v5, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    new-array v2, v2, [Landroid/animation/Animator;

    aput-object v3, v2, v4

    aput-object v1, v2, v0

    invoke-virtual {v5, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/squareup/ui/DropDownContainer$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/DropDownContainer$1;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    iget v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->showDropDown:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public setDropDownBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setDropDownClosed()V
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownHiding(Landroid/view/View;)V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->getDropDownHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setY(F)V

    const/4 v0, 0x0

    .line 270
    iput v0, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    .line 271
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    const/4 v0, 0x0

    .line 272
    invoke-virtual {p0, v0}, Lcom/squareup/ui/DropDownContainer;->setDropDownVisible(Z)V

    .line 273
    invoke-direct {p0, v0}, Lcom/squareup/ui/DropDownContainer;->sendAccessibilityEventForShown(Z)V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownHidden(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public setDropDownDuration(I)V
    .locals 0

    .line 424
    iput p1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownDuration:I

    return-void
.end method

.method public setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V
    .locals 0

    .line 252
    iput-object p1, p0, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    return-void
.end method

.method public setDropDownOpen()V
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownOpening(Landroid/view/View;)V

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setY(F)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 259
    iput v0, p0, Lcom/squareup/ui/DropDownContainer;->scrimOpacity:F

    .line 260
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->installGlass()V

    const/4 v0, 0x1

    .line 261
    invoke-virtual {p0, v0}, Lcom/squareup/ui/DropDownContainer;->setDropDownVisible(Z)V

    .line 262
    invoke-direct {p0, v0}, Lcom/squareup/ui/DropDownContainer;->sendAccessibilityEventForShown(Z)V

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->listener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lcom/squareup/ui/DropDownContainer$DropDownListener;->onDropDownShown(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method protected setDropDownVisible(Z)V
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public setScrimColor(I)V
    .locals 1

    .line 428
    iput p1, p0, Lcom/squareup/ui/DropDownContainer;->scrimColor:I

    .line 429
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->scrimPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public toggleDropDown()V
    .locals 1

    .line 369
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownOpening()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    return-void

    .line 375
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer;->isDropDownClosing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->openDropDown()V

    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/DropDownContainer;->dropDownContent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 381
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    goto :goto_0

    .line 383
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/DropDownContainer;->openDropDown()V

    :goto_0
    return-void
.end method
