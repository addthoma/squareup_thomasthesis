.class public final Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater$DefaultImpls;
.super Ljava/lang/Object;
.source "CartEntryViewsFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static mapToEntryViews(Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;Landroid/content/Context;Lcom/squareup/ui/cart/CartEntryViewModel;)Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-interface {p0, p1}, Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;->inflate(Landroid/content/Context;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p0

    .line 46
    invoke-virtual {p0, p2}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-object p0
.end method
