.class public Lcom/squareup/ui/cart/header/CartHeaderPhoneView;
.super Lcom/squareup/ui/cart/header/CartHeaderBaseView;
.source "CartHeaderPhoneView.java"


# instance fields
.field presenter:Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;->inject(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;)V

    return-void
.end method

.method private initializeSaleAnimations()V
    .locals 21

    move-object/from16 v0, p0

    .line 82
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->fadeOutNoSale:Landroid/view/animation/AlphaAnimation;

    .line 83
    iget-object v1, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->fadeOutNoSale:Landroid/view/animation/AlphaAnimation;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v5, 0x40600000    # 3.5f

    invoke-direct {v4, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v4}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 84
    iget-object v1, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->fadeOutNoSale:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v6, 0x96

    invoke-virtual {v1, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 86
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 87
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 88
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object v8, v4

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 91
    new-instance v8, Landroid/view/animation/OvershootInterpolator;

    const/high16 v9, 0x3fc00000    # 1.5f

    invoke-direct {v8, v9}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    invoke-virtual {v4, v8}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 93
    new-instance v8, Landroid/view/animation/AnimationSet;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 94
    invoke-virtual {v8, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 95
    invoke-virtual {v8, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v10, 0xfa

    .line 96
    invoke-virtual {v8, v10, v11}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 98
    iget-object v1, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->fadeOutNoSale:Landroid/view/animation/AlphaAnimation;

    new-instance v4, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;

    invoke-direct {v4, v0, v8}, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;-><init>(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;Landroid/view/animation/AnimationSet;)V

    invoke-virtual {v1, v4}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 109
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 110
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 111
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v14, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/high16 v20, 0x3f800000    # 1.0f

    move-object v12, v4

    invoke-direct/range {v12 .. v20}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 114
    new-instance v8, Lcom/squareup/ui/ReverseOvershootInterpolator;

    invoke-direct {v8}, Lcom/squareup/ui/ReverseOvershootInterpolator;-><init>()V

    invoke-virtual {v4, v8}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 116
    new-instance v8, Landroid/view/animation/AnimationSet;

    invoke-direct {v8, v9}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v8, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleOutSet:Landroid/view/animation/AnimationSet;

    .line 117
    iget-object v8, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleOutSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {v8, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 118
    iget-object v8, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleOutSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {v8, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 119
    iget-object v4, v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleOutSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 121
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v4, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 122
    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v4, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 123
    invoke-virtual {v4, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 125
    new-instance v2, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;

    invoke-direct {v2, v0, v4}, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$3;-><init>(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 34
    invoke-super {p0}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->onAttachedToWindow()V

    .line 35
    new-instance v0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$1;-><init>(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    invoke-static {p0}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->initializeSaleAnimations()V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->presenter:Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->presenter:Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->dropView(Ljava/lang/Object;)V

    .line 48
    invoke-super {p0}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected showCurrentSale(ZLjava/lang/String;)V
    .locals 1

    .line 52
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p1, :cond_1

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleContainer:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_2

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->noSale:Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->fadeOutNoSale:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {p1, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 62
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleContainer:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->noSale:Landroid/view/View;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method protected showNoSale(Z)V
    .locals 2

    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->setSaleQuantity(I)V

    if-eqz p1, :cond_0

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->noSale:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_1

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleContainer:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleOutSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 76
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleContainer:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->noSale:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method
