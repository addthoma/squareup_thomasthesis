.class public Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;
.super Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;
.source "CartHeaderPhonePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/cart/header/CartHeaderBasePresenter<",
        "Lcom/squareup/ui/cart/header/CartHeaderPhoneView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;Lflow/Flow;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    .line 32
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;)V

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 35
    iput-object p8, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->flow:Lflow/Flow;

    return-void
.end method

.method private onTicketEdited()V
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->updateSaleTextAndQuantity()V

    return-void
.end method


# virtual methods
.method protected getSaleText()Ljava/lang/String;
    .locals 2

    .line 62
    invoke-super {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->getSaleText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->current_sale:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$CartHeaderPhonePresenter(Lcom/squareup/tickets/TicketEdited;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->onTicketEdited()V

    return-void
.end method

.method onCartUpdated()V
    .locals 0

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->updateSaleTextAndQuantity()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 39
    invoke-super {p0, p1}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/tickets/TicketEdited;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderPhonePresenter$1QRv5pJBz_M1QJCnaOO5qG1nou8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderPhonePresenter$1QRv5pJBz_M1QJCnaOO5qG1nou8;-><init>(Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public saleContainerClicked()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/cart/CartContainerScreen;->INSTANCE:Lcom/squareup/ui/cart/CartContainerScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhonePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYMENT_PAD_SHOW_CART:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
