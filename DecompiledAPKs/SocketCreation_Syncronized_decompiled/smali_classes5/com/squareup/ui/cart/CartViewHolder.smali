.class abstract Lcom/squareup/ui/cart/CartViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartViewHolder$NoopViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$TotalViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$SurchargeViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$CompViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;,
        Lcom/squareup/ui/cart/CartViewHolder$TicketNoteViewHolder;
    }
.end annotation


# instance fields
.field protected final cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    return-void
.end method

.method static synthetic access$000(Landroid/view/View;)I
    .locals 0

    .line 25
    invoke-static {p0}, Lcom/squareup/ui/cart/CartViewHolder;->getAvailableWidth(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method private static getAvailableWidth(Landroid/view/View;)I
    .locals 2

    .line 325
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result p0

    sub-int/2addr v0, p0

    return v0
.end method


# virtual methods
.method abstract bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
.end method

.method isClickable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method shouldAllowSwipeToDelete()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
