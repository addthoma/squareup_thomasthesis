.class Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UnappliedCouponViewHolder"
.end annotation


# instance fields
.field private final cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 286
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 287
    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 288
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
    .locals 0

    .line 280
    iget-object p0, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    return-object p0
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 3

    .line 292
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    sget v1, Lcom/squareup/orderentry/R$string;->unapplied_discount_row_title_choose_item:I

    iget-object v2, p1, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;->couponName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->unappliedCoupon(ILjava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    new-instance v1, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;-><init>(Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/CartEntryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method shouldAllowSwipeToDelete()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
