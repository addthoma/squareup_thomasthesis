.class public final Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;
.super Ljava/lang/Object;
.source "CartRecyclerViewPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cartFeesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;"
        }
    .end annotation
.end field

.field private final cartScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyPointsRowSubtitleRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryPagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 78
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->cartFeesProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->cartScreenRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->loyaltyCalculatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->loyaltyPointsRowSubtitleRendererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 115
    new-instance v18, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;Lcom/squareup/util/Res;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 125
    new-instance v18, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;-><init>(Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;Lcom/squareup/util/Res;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
    .locals 19

    move-object/from16 v0, p0

    .line 99
    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/orderentry/pages/OrderEntryPages;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->cartFeesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/cart/CartFeesModel$Session;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->cartScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/cart/CartScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->loyaltyCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/loyalty/LoyaltyCalculator;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->loyaltyPointsRowSubtitleRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    iget-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/util/Res;

    invoke-static/range {v2 .. v18}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->newInstance(Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;Lcom/squareup/util/Res;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter_Factory;->get()Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    move-result-object v0

    return-object v0
.end method
