.class public final Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;
.super Ljava/lang/Object;
.source "CartScreenFinisher_Tablet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;",
        ">;"
    }
.end annotation


# instance fields
.field private final cartDropDownPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;->cartDropDownPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CartDropDownPresenter;",
            ">;)",
            "Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/orderentry/CartDropDownPresenter;)Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;-><init>(Lcom/squareup/orderentry/CartDropDownPresenter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;->cartDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;->newInstance(Lcom/squareup/orderentry/CartDropDownPresenter;)Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartScreenFinisher_Tablet_Factory;->get()Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;

    move-result-object v0

    return-object v0
.end method
