.class public final Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;
.super Ljava/lang/Object;
.source "CartRecyclerView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/cart/CartRecyclerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final cartEntryViewModelFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->cartEntryViewModelFactoryProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartRecyclerViewPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/cart/CartRecyclerView;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCartEntryViewModelFactory(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerView;->presenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/cart/CartRecyclerView;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->cartEntryViewModelFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-static {p1, v0}, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->injectCartEntryViewModelFactory(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->injectPresenter(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->injectFeatures(Lcom/squareup/ui/cart/CartRecyclerView;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerView_MembersInjector;->injectMembers(Lcom/squareup/ui/cart/CartRecyclerView;)V

    return-void
.end method
