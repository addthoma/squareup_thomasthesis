.class public final Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;
.super Ljava/lang/Object;
.source "CartMenuPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/menu/CartMenuPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cartScreenFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCountsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionDiscountAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->cartScreenFinisherProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;)",
            "Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 119
    new-instance v18, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/ui/cart/CartScreenFinisher;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;)Lcom/squareup/ui/cart/menu/CartMenuPresenter;
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 130
    new-instance v18, Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/ui/cart/CartScreenFinisher;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/menu/CartMenuPresenter;
    .locals 19

    move-object/from16 v0, p0

    .line 102
    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/TransactionDiscountAdapter;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/badbus/BadEventSink;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/print/PrinterStations;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->cartScreenFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/cart/CartScreenFinisher;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/tickets/TicketCountsCache;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/ticket/OpenTicketsRunner;

    invoke-static/range {v2 .. v18}, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/ui/cart/CartScreenFinisher;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;)Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter_Factory;->get()Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    move-result-object v0

    return-object v0
.end method
