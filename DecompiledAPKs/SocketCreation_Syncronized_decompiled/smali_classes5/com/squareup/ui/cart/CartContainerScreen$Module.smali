.class public abstract Lcom/squareup/ui/cart/CartContainerScreen$Module;
.super Ljava/lang/Object;
.source "CartContainerScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartContainerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindCartScreenFinisher(Lcom/squareup/ui/cart/CartScreenFinisher$Phone;)Lcom/squareup/ui/cart/CartScreenFinisher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
