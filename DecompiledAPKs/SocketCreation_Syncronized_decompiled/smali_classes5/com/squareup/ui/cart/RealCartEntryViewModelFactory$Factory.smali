.class public final Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;
.super Ljava/lang/Object;
.source "RealCartEntryViewModelFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;",
        "",
        "application",
        "Landroid/app/Application;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)V",
        "create",
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;",
        "config",
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iput-object p4, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;
    .locals 8

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;

    .line 77
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->application:Landroid/app/Application;

    iget-object v3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v5, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->res:Lcom/squareup/util/Res;

    iget-object v7, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    move-object v1, v0

    move-object v6, p1

    .line 76
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;-><init>(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;Lcom/squareup/text/DurationFormatter;)V

    return-object v0
.end method
