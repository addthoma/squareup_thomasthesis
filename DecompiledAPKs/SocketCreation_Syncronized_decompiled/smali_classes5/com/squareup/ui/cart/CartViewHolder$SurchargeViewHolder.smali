.class Lcom/squareup/ui/cart/CartViewHolder$SurchargeViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SurchargeViewHolder"
.end annotation


# instance fields
.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 228
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 229
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$SurchargeViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 3

    .line 233
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$SurchargeRow;

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$SurchargeViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$SurchargeViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    iget-object v2, p1, Lcom/squareup/ui/cart/CartAdapterItem$SurchargeRow;->surchargeName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$SurchargeRow;->surchargeAmount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2, p1}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->surcharge(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-void
.end method
