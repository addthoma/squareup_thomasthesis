.class abstract Lcom/squareup/ui/cart/AbstractCartFeesPresenter;
.super Lmortar/ViewPresenter;
.source "AbstractCartFeesPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/squareup/ui/cart/AbstractCartFeesView;",
        ">",
        "Lmortar/ViewPresenter<",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

.field private final feeFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingDelete:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;"
        }
    .end annotation
.end field

.field protected final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->pendingDelete:Ljava/util/List;

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->res:Lcom/squareup/util/Res;

    .line 29
    iput-object p4, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->feeFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private dismissAnimations()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->flushPendingDeletes()V

    return-void
.end method

.method private flushPendingDeletes()V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->pendingDelete:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    .line 117
    iget-object v2, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/CartFeesModel$Session;->requireEditFeesState()Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->deleteFee(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)V

    goto :goto_0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->pendingDelete:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public static synthetic lambda$PjWRFUbDFhZBPrRhmVuUyLS7MKw(Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->onSavePressed()V

    return-void
.end method

.method private onSavePressed()V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->dismissAnimations()V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->executeDeletes()V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->goBack()V

    return-void
.end method


# virtual methods
.method public commitOneDelete(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->pendingDelete:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->requireEditFeesState()Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->deleteFee(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)V

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->update()V

    :cond_0
    return-void
.end method

.method protected abstract executeDeletes()V
.end method

.method protected getHelpText()Ljava/lang/CharSequence;
    .locals 2

    .line 108
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Override this method to show the help text."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getRow(I)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->requireEditFeesState()Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->getFeeRows()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    return-object p1
.end method

.method public getRowCount()I
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->requireEditFeesState()Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->getFeeRows()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method abstract goBack()V
.end method

.method public initRowView(ILcom/squareup/ui/cart/CartFeeRowView;)V
    .locals 1

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->getRow(I)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    move-result-object p1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->feeFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/cart/CartFeeRowView;->init(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;Lcom/squareup/text/Formatter;)V

    return-void
.end method

.method public isRestoring(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Z
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->requireEditFeesState()Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    move-result-object v0

    .line 83
    iget-object p1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->getLastRestoredFee()Lcom/squareup/checkout/Adjustment;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->clearLastRestoredFee()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onBackPressed()Z
    .locals 1

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->dismissAnimations()V

    const/4 v0, 0x0

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 33
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->title()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/cart/-$$Lambda$TcD2Xzjn-aNqYsB2-xHz-TpDJg0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/-$$Lambda$TcD2Xzjn-aNqYsB2-xHz-TpDJg0;-><init>(Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V

    .line 35
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->save:I

    .line 36
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/cart/-$$Lambda$AbstractCartFeesPresenter$PjWRFUbDFhZBPrRhmVuUyLS7MKw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/-$$Lambda$AbstractCartFeesPresenter$PjWRFUbDFhZBPrRhmVuUyLS7MKw;-><init>(Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V

    .line 37
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method protected onUpPressed()V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->dismissAnimations()V

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->goBack()V

    return-void
.end method

.method public prepDelete(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Z
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->pendingDelete:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected showHelpText()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method abstract title()Ljava/lang/CharSequence;
.end method

.method protected update()V
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/AbstractCartFeesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->updateList()V

    return-void
.end method
