.class public abstract Lcom/squareup/ui/cart/AbstractCartFeesView;
.super Landroid/widget/LinearLayout;
.source "AbstractCartFeesView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;

.field private listView:Landroid/widget/ListView;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected abstract getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;
.end method

.method public onBackPressed()Z
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 33
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 35
    new-instance v0, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;-><init>(Lcom/squareup/ui/cart/AbstractCartFeesView;Lcom/squareup/ui/cart/AbstractCartFeesView$1;)V

    iput-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView;->adapter:Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;

    .line 36
    sget v0, Lcom/squareup/orderentry/R$id;->cart_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView;->listView:Landroid/widget/ListView;

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/cart/AbstractCartFeesView;->adapter:Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method updateList()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/cart/AbstractCartFeesView;->adapter:Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/AbstractCartFeesView$CartFeeListAdapter;->notifyDataSetChanged()V

    return-void
.end method
