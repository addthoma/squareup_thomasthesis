.class public final Lcom/squareup/ui/cart/CartEntryViewModel$Name;
.super Ljava/lang/Object;
.source "CartEntryViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartEntryViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Name"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\t2\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewModel$Name;",
        "",
        "label",
        "",
        "color",
        "Landroid/content/res/ColorStateList;",
        "weight",
        "Lcom/squareup/marketfont/MarketFont$Weight;",
        "enabled",
        "",
        "(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Lcom/squareup/marketfont/MarketFont$Weight;Z)V",
        "getColor",
        "()Landroid/content/res/ColorStateList;",
        "getEnabled",
        "()Z",
        "getLabel",
        "()Ljava/lang/CharSequence;",
        "getWeight",
        "()Lcom/squareup/marketfont/MarketFont$Weight;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final color:Landroid/content/res/ColorStateList;

.field private final enabled:Z

.field private final label:Ljava/lang/CharSequence;

.field private final weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Lcom/squareup/marketfont/MarketFont$Weight;Z)V
    .locals 1

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "color"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    iput-object p3, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    iput-boolean p4, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Lcom/squareup/marketfont/MarketFont$Weight;ZILjava/lang/Object;)Lcom/squareup/ui/cart/CartEntryViewModel$Name;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->copy(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Lcom/squareup/marketfont/MarketFont$Weight;Z)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final component3()Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    return v0
.end method

.method public final copy(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Lcom/squareup/marketfont/MarketFont$Weight;Z)Lcom/squareup/ui/cart/CartEntryViewModel$Name;
    .locals 1

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "color"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;-><init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Lcom/squareup/marketfont/MarketFont$Weight;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    iget-boolean p1, p1, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getColor()Landroid/content/res/ColorStateList;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    return v0
.end method

.method public final getLabel()Ljava/lang/CharSequence;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getWeight()Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Name(label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->label:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->color:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", weight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/cart/CartEntryViewModel$Name;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
