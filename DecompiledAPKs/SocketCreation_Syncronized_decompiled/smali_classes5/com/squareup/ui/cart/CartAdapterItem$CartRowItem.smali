.class public Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartAdapterItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CartRowItem"
.end annotation


# instance fields
.field public final cartItem:Lcom/squareup/checkout/CartItem;

.field public final diningOption:Ljava/lang/String;

.field public final isDiscountEvent:Z

.field public final isKeypadCommittedEvent:Z

.field public final orderItemIndex:I


# direct methods
.method constructor <init>(Lcom/squareup/checkout/CartItem;ILjava/lang/String;ZZ)V
    .locals 0

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->cartItem:Lcom/squareup/checkout/CartItem;

    .line 119
    iput p2, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->orderItemIndex:I

    .line 120
    iput-object p3, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->diningOption:Ljava/lang/String;

    .line 121
    iput-boolean p4, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->isKeypadCommittedEvent:Z

    .line 122
    iput-boolean p5, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->isDiscountEvent:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 141
    :cond_1
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    .line 143
    iget-object v2, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->cartItem:Lcom/squareup/checkout/CartItem;

    iget-object v3, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->cartItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 145
    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->diningOption:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v3, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->diningOption:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 147
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 151
    :cond_3
    iget v2, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->orderItemIndex:I

    iget v3, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->orderItemIndex:I

    if-eq v2, v3, :cond_4

    return v1

    .line 154
    :cond_4
    iget-boolean v2, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->isKeypadCommittedEvent:Z

    iget-boolean p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->isKeypadCommittedEvent:Z

    if-eq v2, p1, :cond_5

    return v1

    :cond_5
    return v0

    :cond_6
    :goto_0
    return v1
.end method

.method public getId()I
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->cartItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 126
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->STANDARD:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->cartItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->hashCode()I

    move-result v0

    return v0
.end method
