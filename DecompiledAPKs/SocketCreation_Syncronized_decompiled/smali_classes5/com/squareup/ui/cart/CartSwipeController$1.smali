.class Lcom/squareup/ui/cart/CartSwipeController$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "CartSwipeController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartSwipeController;-><init>(IILandroidx/recyclerview/widget/RecyclerView;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartSwipeController;

.field final synthetic val$recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartSwipeController;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController$1;->this$0:Lcom/squareup/ui/cart/CartSwipeController;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartSwipeController$1;->val$recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/cart/CartSwipeController$1;->this$0:Lcom/squareup/ui/cart/CartSwipeController;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartSwipeController;->access$000(Lcom/squareup/ui/cart/CartSwipeController;)Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartSwipeController$1;->val$recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartSwipeController$1;->this$0:Lcom/squareup/ui/cart/CartSwipeController;

    .line 66
    invoke-static {v1}, Lcom/squareup/ui/cart/CartSwipeController;->access$100(Lcom/squareup/ui/cart/CartSwipeController;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/cart/CartSwipeController$1;->this$0:Lcom/squareup/ui/cart/CartSwipeController;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartSwipeController;->access$000(Lcom/squareup/ui/cart/CartSwipeController;)Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/cart/CartSwipeController$DeleteButton;->onClick(FF)Z

    move-result p1

    if-nez p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/cart/CartSwipeController$1;->this$0:Lcom/squareup/ui/cart/CartSwipeController;

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartSwipeController;->recoverPreviousSwipe(I)V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method
