.class public Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartAdapterItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TaxRow"
.end annotation


# instance fields
.field public final additionalTaxFees:Lcom/squareup/protos/common/Money;

.field public final hasInterestingTaxState:Z

.field public final taxResId:I


# direct methods
.method public constructor <init>(ZLcom/squareup/protos/common/Money;I)V
    .locals 0

    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    iput-boolean p1, p0, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;->hasInterestingTaxState:Z

    .line 228
    iput-object p2, p0, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;->additionalTaxFees:Lcom/squareup/protos/common/Money;

    .line 229
    iput p3, p0, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;->taxResId:I

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 237
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TAX:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->access$000(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I

    move-result v0

    return v0
.end method

.method public getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 233
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TAX:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method
