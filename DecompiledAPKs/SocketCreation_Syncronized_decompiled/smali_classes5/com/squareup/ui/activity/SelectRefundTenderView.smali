.class public Lcom/squareup/ui/activity/SelectRefundTenderView;
.super Landroid/widget/LinearLayout;
.source "SelectRefundTenderView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/activity/SelectRefundTenderPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private tendersContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const-class p2, Lcom/squareup/ui/activity/SelectRefundTenderScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/SelectRefundTenderScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/activity/SelectRefundTenderScreen$Component;->inject(Lcom/squareup/ui/activity/SelectRefundTenderView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 77
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 78
    sget v0, Lcom/squareup/billhistoryui/R$id;->select_refund_tenders_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->tendersContainer:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method addTender(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/SelectRefundTenderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-virtual {v0, p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    .line 63
    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 64
    invoke-interface {p3, p4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    sget p3, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    .line 65
    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setBackground(I)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    sget-object p3, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 66
    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    .line 67
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p2

    .line 68
    new-instance p3, Lcom/squareup/ui/activity/SelectRefundTenderView$1;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/activity/SelectRefundTenderView$1;-><init>(Lcom/squareup/ui/activity/SelectRefundTenderView;Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->tendersContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method clearTenders()V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->tendersContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->presenter:Lcom/squareup/ui/activity/SelectRefundTenderPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->onBackPressed()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->presenter:Lcom/squareup/ui/activity/SelectRefundTenderPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->dropView(Ljava/lang/Object;)V

    .line 43
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/activity/SelectRefundTenderView;->bindViews()V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->presenter:Lcom/squareup/ui/activity/SelectRefundTenderPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
