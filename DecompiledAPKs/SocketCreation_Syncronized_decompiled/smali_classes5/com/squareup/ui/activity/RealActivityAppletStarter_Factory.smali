.class public final Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;
.super Ljava/lang/Object;
.source "RealActivityAppletStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/RealActivityAppletStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;)",
            "Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/activity/ActivityApplet;)Lcom/squareup/ui/activity/RealActivityAppletStarter;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/ui/activity/RealActivityAppletStarter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RealActivityAppletStarter;-><init>(Lcom/squareup/ui/activity/ActivityApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/RealActivityAppletStarter;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/ActivityApplet;

    invoke-static {v0}, Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;->newInstance(Lcom/squareup/ui/activity/ActivityApplet;)Lcom/squareup/ui/activity/RealActivityAppletStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/activity/RealActivityAppletStarter_Factory;->get()Lcom/squareup/ui/activity/RealActivityAppletStarter;

    move-result-object v0

    return-object v0
.end method
