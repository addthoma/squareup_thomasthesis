.class public Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;
.super Landroid/widget/LinearLayout;
.source "ActivityAppletSearchMessageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$OnTextViewClickedListener;
    }
.end annotation


# instance fields
.field private final clickableTextView:Lcom/squareup/marketfont/MarketTextView;

.field private final messageView:Lcom/squareup/widgets/MessageView;

.field private textViewClickedListener:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$OnTextViewClickedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    sget p2, Lcom/squareup/billhistoryui/R$layout;->activity_applet_sidebar_search_message:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->setOrientation(I)V

    .line 33
    sget p1, Lcom/squareup/billhistoryui/R$id;->activity_applet_search_message:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->messageView:Lcom/squareup/widgets/MessageView;

    .line 34
    sget p1, Lcom/squareup/billhistoryui/R$id;->activity_applet_search_clickable_text:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    new-instance p2, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$1;-><init>(Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->fireOnTextViewClicked()V

    return-void
.end method

.method private fireOnTextViewClicked()V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->textViewClickedListener:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$OnTextViewClickedListener;

    if-eqz v0, :cond_0

    .line 76
    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$OnTextViewClickedListener;->onTextViewClicked()V

    :cond_0
    return-void
.end method


# virtual methods
.method public hideClickableTextView()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method public setClickableText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnTextViewClickedListener(Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$OnTextViewClickedListener;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->textViewClickedListener:Lcom/squareup/ui/activity/ActivityAppletSearchMessageView$OnTextViewClickedListener;

    return-void
.end method

.method public setTextViewClickable(Z)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    if-eqz p1, :cond_0

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 60
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    goto :goto_0

    .line 64
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 64
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletSearchMessageView;->clickableTextView:Lcom/squareup/marketfont/MarketTextView;

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    :goto_0
    return-void
.end method
