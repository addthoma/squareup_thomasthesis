.class public Lcom/squareup/ui/activity/ActivityBadge;
.super Ljava/lang/Object;
.source "ActivityBadge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ActivityBadge$CountSelector;
    }
.end annotation


# instance fields
.field private final countSelector:Lcom/squareup/ui/activity/ActivityBadge$CountSelector;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/ActivityBadge$CountSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityBadge;->countSelector:Lcom/squareup/ui/activity/ActivityBadge$CountSelector;

    return-void
.end method


# virtual methods
.method public count()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityBadge;->countSelector:Lcom/squareup/ui/activity/ActivityBadge$CountSelector;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityBadge$CountSelector;->count()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
