.class public final Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;
.super Ljava/lang/Object;
.source "RefundPinDialog_RefundPinDialogModule_PinPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final listenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final starPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->starPresenterProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->resProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->listenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;",
            ">;)",
            "Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;-><init>(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static pinPresenter(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
    .locals 0

    .line 52
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;->pinPresenter(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

    iget-object v1, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->starPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    iget-object v2, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->listenerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->pinPresenter(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_PinPresenterFactory;->get()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    move-result-object v0

    return-object v0
.end method
