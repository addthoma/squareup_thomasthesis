.class public final Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;
.super Ljava/lang/Object;
.source "TransactionsHistoryListView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/TransactionsHistoryListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->legacyPresenterProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/TransactionsHistoryListView;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/ui/activity/TransactionsHistoryListView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/activity/TransactionsHistoryListView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectLegacyPresenter(Lcom/squareup/ui/activity/TransactionsHistoryListView;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->legacyPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->injectLegacyPresenter(Lcom/squareup/ui/activity/TransactionsHistoryListView;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->injectDevice(Lcom/squareup/ui/activity/TransactionsHistoryListView;Lcom/squareup/util/Device;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->injectFeatures(Lcom/squareup/ui/activity/TransactionsHistoryListView;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryListView_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V

    return-void
.end method
