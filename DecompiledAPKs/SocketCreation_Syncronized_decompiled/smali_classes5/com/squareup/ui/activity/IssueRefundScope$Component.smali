.class public interface abstract Lcom/squareup/ui/activity/IssueRefundScope$Component;
.super Ljava/lang/Object;
.source "IssueRefundScope.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Component;
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope$ParentComponent;
.implements Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;,
        Lcom/squareup/ui/activity/RefundSecureTouchModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/IssueRefundScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/activity/refund/IssueRefundCoordinator;)V
.end method

.method public abstract inject(Lcom/squareup/activity/refund/RefundDoneCoordinator;)V
.end method

.method public abstract inject(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)V
.end method

.method public abstract issueRefundCoordinator()Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;
.end method

.method public abstract issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;
.end method

.method public abstract refundCardPresenceCoordinator()Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$Factory;
.end method

.method public abstract refundDoneCoordinator()Lcom/squareup/activity/refund/RefundDoneCoordinator$Factory;
.end method

.method public abstract refundErrorCoordinator()Lcom/squareup/activity/refund/RefundErrorCoordinator$Factory;
.end method

.method public abstract refundItemizationsCoordinator()Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;
.end method

.method public abstract restockOnItemizedRefundCoordinator()Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;
.end method
