.class public Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;
.super Ljava/lang/Object;
.source "ShowFullHistoryPermissionController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;
    }
.end annotation


# static fields
.field private static final MAX_BILLS_WITHOUT_PERMISSION:I = 0x5


# instance fields
.field private final onPermissionGrantedListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private permissionGranted:Z


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 45
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->onPermissionGrantedListeners:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;Z)Z
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->permissionGranted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->fireOnPermissionGranted()V

    return-void
.end method

.method private fireOnPermissionGranted()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->onPermissionGrantedListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;

    .line 105
    invoke-interface {v1}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;->onShowFullHistoryPermissionGranted()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private hasPermission()Z
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->FULL_SALES_HISTORY:Lcom/squareup/permissions/Permission;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addOnPermissionGrantedListener(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->onPermissionGrantedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getMaxBillsWithoutPermission()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public isPermissionGranted()Z
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->permissionGranted:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->hasPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public removeOnPermissionGrantedListener(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$OnFullHistoryPermissionGrantedListener;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->onPermissionGrantedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public requestPermission()V
    .locals 3

    .line 83
    iget-boolean v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->permissionGranted:Z

    if-eqz v0, :cond_0

    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->FULL_SALES_HISTORY:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController$1;-><init>(Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public unsetPermission()V
    .locals 1

    const/4 v0, 0x0

    .line 72
    iput-boolean v0, p0, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->permissionGranted:Z

    return-void
.end method
