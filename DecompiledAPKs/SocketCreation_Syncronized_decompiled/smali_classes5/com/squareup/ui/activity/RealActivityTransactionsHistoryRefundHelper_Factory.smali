.class public final Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;
.super Ljava/lang/Object;
.source "RealActivityTransactionsHistoryRefundHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final currentBillProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTransactionsHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStatusManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->currentBillProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->tenderStatusManagerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/activity/CurrentBill;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/util/Res;)Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;-><init>(Lcom/squareup/activity/CurrentBill;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->currentBillProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/CurrentBill;

    iget-object v1, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v2, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->tenderStatusManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/papersignature/TenderStatusManager;

    iget-object v3, p0, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->newInstance(Lcom/squareup/activity/CurrentBill;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/util/Res;)Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper_Factory;->get()Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;

    move-result-object v0

    return-object v0
.end method
