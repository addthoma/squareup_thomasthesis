.class public Lcom/squareup/ui/activity/billhistory/BillHistoryView;
.super Landroid/widget/LinearLayout;
.source "BillHistoryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;,
        Lcom/squareup/ui/activity/billhistory/BillHistoryView$SharedScope;
    }
.end annotation


# static fields
.field private static final BUTTON_DISABLED_SECONDS:J = 0x2L


# instance fields
.field private buttonContainer:Landroid/widget/LinearLayout;

.field private contentContainer:Landroid/widget/LinearLayout;

.field private final failurePopup:Lcom/squareup/caller/FailurePopup;

.field private itemsContainer:Landroid/widget/LinearLayout;

.field private itemsTitle:Landroid/widget/TextView;

.field private loyaltyContainer:Landroid/view/ViewGroup;

.field private loyaltyRowSubTitle:Landroid/widget/TextView;

.field private loyaltyRowTitle:Landroid/widget/TextView;

.field private loyaltyRowValue:Landroid/widget/TextView;

.field mainScheduler:Lrx/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private printGiftReceiptButton:Landroid/widget/Button;

.field private progressBar:Landroid/widget/ProgressBar;

.field private receiptButton:Landroid/widget/Button;

.field private refundButton:Landroid/widget/Button;

.field private relatedBillsContainer:Landroid/view/ViewGroup;

.field private reprintTicketButton:Landroid/widget/Button;

.field private taxBreakdownContainer:Landroid/view/ViewGroup;

.field private tendersContainer:Landroid/view/ViewGroup;

.field private ticketNote:Landroid/widget/TextView;

.field private totalContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    const-class p2, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;->inject(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    .line 79
    sget p2, Lcom/squareup/billhistoryui/R$layout;->activity_applet_bill_history_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 81
    new-instance p2, Lcom/squareup/caller/FailurePopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    return-void
.end method

.method static synthetic lambda$null$0(Landroid/widget/Button;Ljava/lang/Long;)V
    .locals 0

    const/4 p1, 0x1

    .line 188
    invoke-virtual {p0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private temporarilyDisableButton(Landroid/widget/Button;)V
    .locals 1

    const/4 v0, 0x0

    .line 184
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 186
    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryView$t845MJqN4v48l6-DlH2AdLTEKyk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryView$t845MJqN4v48l6-DlH2AdLTEKyk;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryView;Landroid/widget/Button;)V

    invoke-static {p0, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method addItemization(Landroid/view/View;)V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method addItemsSection(Ljava/util/List;)Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;"
        }
    .end annotation

    .line 231
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 237
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 238
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method addRelatedBillSection(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;Z)V
    .locals 3

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->relatedBillsContainer:Landroid/view/ViewGroup;

    new-instance v1, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;

    .line 202
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;-><init>(Landroid/content/Context;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;Z)V

    .line 201
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method addTaxBreakdownSection()Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;
    .locals 3

    .line 258
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;-><init>(Landroid/content/Context;)V

    .line 259
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->taxBreakdownContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 260
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->taxBreakdownContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method addTenderSection(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->tendersContainer:Landroid/view/ViewGroup;

    new-instance v1, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;-><init>(Landroid/content/Context;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/protos/client/rolodex/Contact;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method addTotalSection()Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;
    .locals 3

    .line 247
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryTotalSection;-><init>(Landroid/content/Context;)V

    .line 248
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->totalContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 249
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->totalContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method clearItems()V
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method clearRelatedBills()V
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->relatedBillsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method clearTaxBreakdown()V
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->taxBreakdownContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method clearTenders()V
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->tendersContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method clearTotal()V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->totalContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method failurePopup()Lcom/squareup/caller/FailurePopup;
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    return-object v0
.end method

.method hideItemsTitle()V
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsTitle:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method hideLoyalty()V
    .locals 2

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method hideTicketNote()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->ticketNote:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$temporarilyDisableButton$1$BillHistoryView(Landroid/widget/Button;)Lrx/Subscription;
    .locals 4

    .line 187
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->mainScheduler:Lrx/Scheduler;

    const-wide/16 v2, 0x2

    invoke-static {v2, v3, v0, v1}, Lrx/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryView$HLMJJ6-TD5NtnqGjVHjbqmbNppw;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryView$HLMJJ6-TD5NtnqGjVHjbqmbNppw;-><init>(Landroid/widget/Button;)V

    .line 188
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 108
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->presenter:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->presenter:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->dropView(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V

    .line 114
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 85
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 87
    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_view_loading_panel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->progressBar:Landroid/widget/ProgressBar;

    .line 88
    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_view_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->contentContainer:Landroid/widget/LinearLayout;

    .line 89
    sget v0, Lcom/squareup/billhistoryui/R$id;->button_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->buttonContainer:Landroid/widget/LinearLayout;

    .line 90
    sget v0, Lcom/squareup/billhistoryui/R$id;->send_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->receiptButton:Landroid/widget/Button;

    .line 91
    sget v0, Lcom/squareup/billhistoryui/R$id;->issue_refund_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->refundButton:Landroid/widget/Button;

    .line 92
    sget v0, Lcom/squareup/billhistoryui/R$id;->reprint_ticket_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->reprintTicketButton:Landroid/widget/Button;

    .line 93
    sget v0, Lcom/squareup/billhistoryui/R$id;->print_gift_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->printGiftReceiptButton:Landroid/widget/Button;

    .line 94
    sget v0, Lcom/squareup/billhistoryui/R$id;->refunds_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->relatedBillsContainer:Landroid/view/ViewGroup;

    .line 95
    sget v0, Lcom/squareup/billhistoryui/R$id;->tenders_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->tendersContainer:Landroid/view/ViewGroup;

    .line 96
    sget v0, Lcom/squareup/billhistoryui/R$id;->items_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsTitle:Landroid/widget/TextView;

    .line 97
    sget v0, Lcom/squareup/billhistoryui/R$id;->ticket_note:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->ticketNote:Landroid/widget/TextView;

    .line 98
    sget v0, Lcom/squareup/billhistoryui/R$id;->receipt_detail_items_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsContainer:Landroid/widget/LinearLayout;

    .line 99
    sget v0, Lcom/squareup/billhistoryui/R$id;->receipt_detail_total_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->totalContainer:Landroid/view/ViewGroup;

    .line 100
    sget v0, Lcom/squareup/billhistoryui/R$id;->receipt_detail_tax_breakdown_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->taxBreakdownContainer:Landroid/view/ViewGroup;

    .line 101
    sget v0, Lcom/squareup/billhistoryui/R$id;->loyalty_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyContainer:Landroid/view/ViewGroup;

    .line 102
    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_loyalty_section_row_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowTitle:Landroid/widget/TextView;

    .line 103
    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_loyalty_section_row_subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowSubTitle:Landroid/widget/TextView;

    .line 104
    sget v0, Lcom/squareup/billhistoryui/R$id;->bill_history_loyalty_section_row_value:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowValue:Landroid/widget/TextView;

    return-void
.end method

.method public onPrintGiftReceiptButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->printGiftReceiptButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onReceiptButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->receiptButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onRefundButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->refundButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onReprintTicketButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->reprintTicketButton:Landroid/widget/Button;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method setPrintGiftReceiptButtonVisible(Z)V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->printGiftReceiptButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method setReceiptButtonText(I)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->receiptButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method

.method setReceiptEnabled(Z)V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->receiptButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method setRefundButtonText(I)V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->refundButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method

.method setRefundEnabled(Z)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->refundButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method setReprintTicketButtonVisible(Z)V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->reprintTicketButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public show(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->presenter:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->show(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method showButtons(Z)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->buttonContainer:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showContent()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->contentContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method showItemsTitle(Ljava/lang/String;)V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->itemsTitle:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method showLoading()V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->contentContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method showLoyaltySection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowSubTitle:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 283
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowSubTitle:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowValue:Landroid/widget/TextView;

    invoke-static {p3}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 286
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->loyaltyRowValue:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showRefunds(Z)V
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->relatedBillsContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showTicketNote(Ljava/lang/String;)V
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->ticketNote:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->ticketNote:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public temporarilyDisablePrintGiftReceiptButton()V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->printGiftReceiptButton:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisableButton(Landroid/widget/Button;)V

    return-void
.end method

.method public temporarilyDisableReprintTicketButton()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->reprintTicketButton:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisableButton(Landroid/widget/Button;)V

    return-void
.end method
