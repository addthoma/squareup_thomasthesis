.class Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "BillHistoryPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->createGiftCardOnClickListener(Lcom/squareup/checkout/CartItem;)Lcom/squareup/debounce/DebouncedOnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

.field final synthetic val$item:Lcom/squareup/checkout/CartItem;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;Lcom/squareup/checkout/CartItem;)V
    .locals 0

    .line 570
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$1;->this$0:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$1;->val$item:Lcom/squareup/checkout/CartItem;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 572
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$1;->this$0:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    invoke-static {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;->access$000(Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;)Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter$1;->val$item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->getGiftCardServerToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->requestGiftCardBalance(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method
