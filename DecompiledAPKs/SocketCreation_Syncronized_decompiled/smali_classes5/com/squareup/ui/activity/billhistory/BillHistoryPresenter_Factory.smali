.class public final Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;
.super Ljava/lang/Object;
.source "BillHistoryPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final exchangesHostProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardCheckBalanceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final rowFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderWithCustomerInfoCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 75
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 76
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 77
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->giftCardCheckBalanceStarterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 78
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->rowFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->exchangesHostProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->rolodexProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->tenderWithCustomerInfoCacheProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
            ">;)",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 111
    new-instance v17, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Ljava/lang/Object;Lcom/squareup/text/Formatter;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;)Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Ljava/lang/Object;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
            ")",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 122
    new-instance v17, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    move-object/from16 v0, v17

    move-object/from16 v8, p7

    check-cast v8, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/text/Formatter;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;)V

    return-object v17
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;
    .locals 18

    move-object/from16 v0, p0

    .line 95
    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->giftCardCheckBalanceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v6, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->rowFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/print/PrinterStations;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->exchangesHostProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/activity/ExchangesHost;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->tenderWithCustomerInfoCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;

    invoke-static/range {v2 .. v17}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Ljava/lang/Object;Lcom/squareup/text/Formatter;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;)Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter_Factory;->get()Lcom/squareup/ui/activity/billhistory/BillHistoryPresenter;

    move-result-object v0

    return-object v0
.end method
