.class public final Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection_MembersInjector;
.super Ljava/lang/Object;
.source "BillHistoryItemsSection_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectLocaleProvider(Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection_MembersInjector;->injectLocaleProvider(Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;Ljavax/inject/Provider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;)V

    return-void
.end method
