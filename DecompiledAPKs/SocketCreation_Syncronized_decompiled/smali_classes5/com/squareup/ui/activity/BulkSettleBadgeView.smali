.class public Lcom/squareup/ui/activity/BulkSettleBadgeView;
.super Lcom/squareup/widgets/MarinBadgeView;
.source "BulkSettleBadgeView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/activity/BulkSettleBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/widgets/MarinBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public hideBadge()V
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/ui/activity/BulkSettleBadgeView;->shortAnimTimeMs:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    const/4 v0, 0x0

    .line 23
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/BulkSettleBadgeView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    sget v0, Lcom/squareup/billhistoryui/R$drawable;->selector_badge_pending_actionbar:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/BulkSettleBadgeView;->setBackgroundResource(I)V

    return-void
.end method
