.class public Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;
.super Lcom/squareup/ui/activity/InActivityAppletScope;
.source "TipSettlementFailedDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Factory;,
        Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$TipSettlementFailedDialogScreen$FLYf1Cwi8698RA_sQ1df436fzSA;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$TipSettlementFailedDialogScreen$FLYf1Cwi8698RA_sQ1df436fzSA;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/activity/InActivityAppletScope;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;
    .locals 1

    .line 51
    const-class v0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    .line 52
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 51
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    .line 53
    new-instance v0, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;-><init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 45
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/activity/InActivityAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
