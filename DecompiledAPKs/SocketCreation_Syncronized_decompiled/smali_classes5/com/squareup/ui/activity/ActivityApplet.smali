.class public Lcom/squareup/ui/activity/ActivityApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "ActivityApplet.java"


# static fields
.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "SALES_HISTORY"


# instance fields
.field private final activityBadge:Lcom/squareup/ui/activity/ActivityBadge;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/activity/ActivityBadge;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/activity/ActivityBadge;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/activity/ActivityApplet;->activityBadge:Lcom/squareup/ui/activity/ActivityBadge;

    return-void
.end method


# virtual methods
.method public badge()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityApplet;->activityBadge:Lcom/squareup/ui/activity/ActivityBadge;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityBadge;->count()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$JyM0djAmbcEiFah4MVar-ihOwZ0;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$JyM0djAmbcEiFah4MVar-ihOwZ0;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    const-string v0, "activity"

    return-object v0
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 40
    sget-object p1, Lcom/squareup/ui/activity/TransactionsHistoryScreen;->INSTANCE:Lcom/squareup/ui/activity/TransactionsHistoryScreen;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getInitialDetailScreen()Lflow/path/Path;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/activity/BillHistoryDetailScreen;->INSTANCE:Lcom/squareup/ui/activity/BillHistoryDetailScreen;

    return-object v0
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "SALES_HISTORY"

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 36
    sget v0, Lcom/squareup/billhistoryui/R$string;->titlecase_activity:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
