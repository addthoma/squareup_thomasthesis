.class final Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcAuthDelegate$1;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/RealCardPresentRefundKt;->nfcAuthDelegate(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "authData",
        "Lcom/squareup/ui/NfcAuthData;",
        "kotlin.jvm.PlatformType",
        "onNfcAuthorizationRequestReceived"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcAuthDelegate$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V
    .locals 4

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcAuthDelegate$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    .line 150
    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    iget-object v2, p1, Lcom/squareup/ui/NfcAuthData;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const-string v3, "authData.cardReaderInfo"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    const-string v3, "authData.cardReaderInfo.readerType"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    const-string v3, "authData.authorizationData"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p1}, Lcom/squareup/ui/activity/ReaderResult$Companion;->authorizationOf(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object p1

    .line 149
    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method
