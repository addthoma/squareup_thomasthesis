.class public Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;
.super Ljava/lang/Object;
.source "PosActivitySearchPaymentStarter.java"

# interfaces
.implements Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;,
        Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;
    }
.end annotation


# static fields
.field private static final SEARCH_AMOUNT:I = 0x1


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final clock:Lcom/squareup/util/Clock;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private hasStartedPayment:Z

.field private final historyPermissionController:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

.field private searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

.field private final statusDisplay:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 63
    iput-boolean v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->hasStartedPayment:Z

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 72
    iput-object p2, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    .line 73
    iput-object p3, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    .line 74
    iput-object p4, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    .line 75
    iput-object p5, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->clock:Lcom/squareup/util/Clock;

    .line 76
    iput-object p6, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 77
    iput-object p7, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 78
    iput-object p8, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    .line 79
    iput-object p9, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    .line 80
    iput-object p10, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->historyPermissionController:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    .line 81
    new-instance p1, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;-><init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;)V

    iput-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->statusDisplay:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    return-object p1
.end method

.method static synthetic access$500(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/NfcProcessor;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cancelPaymentOnActiveCardReader()V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cantHandlePinRequest()V

    return-void
.end method

.method private becomePinRequestDelegate()V
    .locals 3

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    new-instance v1, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$Listener;-><init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;)V

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    return-void
.end method

.method private canSearchActivities()Z
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v0}, Lcom/squareup/cardreader/DippedCardTracker;->mustReinsertDippedCard()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->historyPermissionController:Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    .line 183
    invoke-virtual {v0}, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;->isPermissionGranted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private cancelPaymentOnActiveCardReader()V
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    .line 116
    iget-boolean v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->hasStartedPayment:Z

    if-eqz v1, :cond_1

    .line 117
    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentTerminated(Lcom/squareup/cardreader/CardReaderId;)V

    const/4 v1, 0x0

    .line 118
    iput-boolean v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->hasStartedPayment:Z

    .line 121
    :cond_1
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 122
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void
.end method

.method private cantHandlePinRequest()V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onCardError()V

    .line 361
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cancelPaymentOnActiveCardReader()V

    return-void
.end method

.method private startPayment()V
    .locals 5

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setDipPaymentStarted()V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    const/4 v1, 0x1

    .line 91
    iput-boolean v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->hasStartedPayment:Z

    .line 92
    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    const-wide/16 v1, 0x1

    .line 95
    iget-object v3, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v3}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReader;->startPayment(JJ)V

    return-void

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to start payment without an active card reader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public disableContactlessField()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->statusDisplay:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/NfcProcessor;->setStatusDisplay(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnAllContactlessReaders()V

    .line 199
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->becomePinRequestDelegate()V

    return-void
.end method

.method public enableContactlessField()V
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iget-object v1, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->statusDisplay:Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$StatusDisplay;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/NfcProcessor;->setStatusDisplay(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/NfcProcessor;->startPaymentOnAllContactlessReadersWithAmount(J)V

    return-void
.end method

.method public onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 204
    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {v0, p1, v1}, Lcom/squareup/activity/ActivitySearchInstrumentConverter;->instrumentSearchForBytes(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    move-result-object p1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V

    .line 207
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cancelPaymentOnActiveCardReader()V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->canSearchActivities()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 131
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->becomePinRequestDelegate()V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onCardInserted()V

    .line 133
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->startPayment()V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/DippedCardTracker;->onEmvTransactionCompleted(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->searchListener:Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;->onCardRemoved()V

    .line 140
    invoke-direct {p0}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->cancelPaymentOnActiveCardReader()V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/DippedCardTracker;->onCardRemoved(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public register(Lmortar/MortarScope;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V
    .locals 1

    .line 146
    new-instance v0, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter$1;-><init>(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
