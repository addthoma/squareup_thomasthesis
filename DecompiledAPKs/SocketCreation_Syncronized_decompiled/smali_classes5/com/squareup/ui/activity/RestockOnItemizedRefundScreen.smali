.class public final Lcom/squareup/ui/activity/RestockOnItemizedRefundScreen;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "RestockOnItemizedRefundScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/activity/RestockOnItemizedRefundScreen;",
        "Lcom/squareup/container/MaybePersistent;",
        "Lcom/squareup/ui/activity/InIssueRefundScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "parentKey",
        "Lcom/squareup/ui/activity/IssueRefundScope;",
        "(Lcom/squareup/ui/activity/IssueRefundScope;)V",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    const-class v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    .line 24
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->restockOnItemizedRefundCoordinator()Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;

    move-result-object v0

    .line 25
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object p1

    const-string v1, "runner"

    .line 26
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->screenData()Lio/reactivex/Observable;

    move-result-object p1

    const-string v2, "runner.screenData()"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$Factory;->create(Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;Lio/reactivex/Observable;)Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 20
    sget v0, Lcom/squareup/activity/R$layout;->activity_applet_restock_on_itemized_refund_view:I

    return v0
.end method
