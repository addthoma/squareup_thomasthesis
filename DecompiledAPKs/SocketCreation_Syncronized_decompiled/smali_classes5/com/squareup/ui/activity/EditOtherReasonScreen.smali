.class public Lcom/squareup/ui/activity/EditOtherReasonScreen;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "EditOtherReasonScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/EditOtherReasonScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/EditOtherReasonScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/EditOtherReasonScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$EditOtherReasonScreen$eTvATDbMELMs1JAQQmWed-UVVdA;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$EditOtherReasonScreen$eTvATDbMELMs1JAQQmWed-UVVdA;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/EditOtherReasonScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/register/widgets/EditTextDialogFactory;Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 0

    .line 20
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/activity/EditOtherReasonScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/EditOtherReasonScreen;)Lcom/squareup/register/widgets/EditTextDialogFactory;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/activity/EditOtherReasonScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/EditOtherReasonScreen;
    .locals 2

    .line 40
    const-class v0, Lcom/squareup/ui/activity/EditOtherReasonScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 41
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/widgets/EditTextDialogFactory;

    .line 42
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/IssueRefundScope;

    .line 43
    new-instance v0, Lcom/squareup/ui/activity/EditOtherReasonScreen;

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/activity/EditOtherReasonScreen;-><init>(Lcom/squareup/register/widgets/EditTextDialogFactory;Lcom/squareup/ui/activity/IssueRefundScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/activity/EditOtherReasonScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/activity/EditOtherReasonScreen;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
