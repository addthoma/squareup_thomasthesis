.class public Lcom/squareup/ui/activity/ViewAllRowViewHolder;
.super Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;
.source "ViewAllRowViewHolder.java"


# instance fields
.field private final listener:Lcom/squareup/debounce/DebouncedOnClickListener;

.field private viewAllButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/ui/activity/BulkSettlePresenter;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;-><init>(Landroid/view/View;)V

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/ViewAllRowViewHolder;->bindViews(Landroid/view/View;)V

    .line 31
    new-instance p1, Lcom/squareup/ui/activity/ViewAllRowViewHolder$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/activity/ViewAllRowViewHolder$1;-><init>(Lcom/squareup/ui/activity/ViewAllRowViewHolder;Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    iput-object p1, p0, Lcom/squareup/ui/activity/ViewAllRowViewHolder;->listener:Lcom/squareup/debounce/DebouncedOnClickListener;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 49
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_view_all_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/activity/ViewAllRowViewHolder;->viewAllButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method public static inflate(Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/view/ViewGroup;)Lcom/squareup/ui/activity/ViewAllRowViewHolder;
    .locals 3

    .line 18
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$layout;->activity_applet_bulk_settle_view_all_row:I

    const/4 v2, 0x0

    .line 19
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    .line 20
    new-instance v0, Lcom/squareup/ui/activity/ViewAllRowViewHolder;

    invoke-direct {v0, p1, p0}, Lcom/squareup/ui/activity/ViewAllRowViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    return-object v0
.end method


# virtual methods
.method onBind(I)V
    .locals 1

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/activity/ViewAllRowViewHolder;->viewAllButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v0, p0, Lcom/squareup/ui/activity/ViewAllRowViewHolder;->listener:Lcom/squareup/debounce/DebouncedOnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method onViewRecycled()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/activity/ViewAllRowViewHolder;->viewAllButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    invoke-super {p0}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;->onViewRecycled()V

    return-void
.end method
