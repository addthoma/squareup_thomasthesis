.class public Lcom/squareup/ui/activity/BulkSettleButtonPresenter;
.super Lmortar/ViewPresenter;
.source "BulkSettleButtonPresenter.java"

# interfaces
.implements Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/activity/BulkSettleButton;",
        ">;",
        "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;"
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final device:Lcom/squareup/util/Device;

.field private final res:Lcom/squareup/util/Res;

.field private final updateScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->device:Lcom/squareup/util/Device;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->res:Lcom/squareup/util/Res;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->updateScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    .line 30
    iput-object p4, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private updateView()V
    .locals 3

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleButton;

    if-nez v0, :cond_0

    return-void

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->updateScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-virtual {v1}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->getLastSeenTenderCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 68
    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleButton;->hideBadge()V

    goto :goto_0

    .line 69
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->updateScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-virtual {v2}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->hasAtLeastOneExpiringTender()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 70
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleButton;->showHighPriorityBadge(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 72
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleButton;->showBadge(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/activity/BulkSettleButton;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->updateScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-virtual {v0, p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->removeOnTenderCountUpdatedListener(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;)V

    .line 41
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/ui/activity/BulkSettleButton;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->dropView(Lcom/squareup/ui/activity/BulkSettleButton;)V

    return-void
.end method

.method public getTooltip()Ljava/lang/CharSequence;
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->bulk_settle_button_hint:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBulkSettleClicked()V
    .locals 2

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lflow/Flow;->get(Landroid/view/View;)Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/BulkSettleScreen;

    invoke-direct {v1}, Lcom/squareup/ui/activity/BulkSettleScreen;-><init>()V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 34
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->updateScheduler:Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    invoke-virtual {p1, p0}, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;->addOnTenderCountUpdatedListener(Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler$OnTenderCountUpdatedListener;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->updateView()V

    return-void
.end method

.method public onTenderCountUpdated()V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->updateView()V

    return-void
.end method

.method public shouldShowButton()Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
