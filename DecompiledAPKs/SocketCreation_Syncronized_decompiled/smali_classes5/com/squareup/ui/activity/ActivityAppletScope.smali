.class public final Lcom/squareup/ui/activity/ActivityAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "ActivityAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/main/EmvSwipePassthroughEnabler$SwipePassthrough;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/ActivityAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ActivityAppletScope$Component;,
        Lcom/squareup/ui/activity/ActivityAppletScope$ParentComponent;,
        Lcom/squareup/ui/activity/ActivityAppletScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/ActivityAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field static final INSTANCE:Lcom/squareup/ui/activity/ActivityAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/activity/ActivityAppletScope;

    invoke-direct {v0}, Lcom/squareup/ui/activity/ActivityAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/activity/ActivityAppletScope;->INSTANCE:Lcom/squareup/ui/activity/ActivityAppletScope;

    .line 97
    sget-object v0, Lcom/squareup/ui/activity/ActivityAppletScope;->INSTANCE:Lcom/squareup/ui/activity/ActivityAppletScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/ActivityAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 36
    const-class v0, Lcom/squareup/ui/activity/ActivityAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/ActivityAppletScope$Component;

    .line 37
    invoke-interface {v0}, Lcom/squareup/ui/activity/ActivityAppletScope$Component;->activityAppletScopeRunner()Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
