.class public Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$Factory;
.super Ljava/lang/Object;
.source "ErrorOnRequestResidualBillScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Landroid/content/res/Resources;Landroid/content/Context;Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$ResidualBillClientErrorRunner;Lcom/squareup/receiving/FailureMessage;)Landroid/app/Dialog;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 43
    invoke-virtual {p3}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-virtual {p3}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p3

    sget v1, Lcom/squareup/activity/R$string;->dialog_dismiss:I

    .line 45
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 42
    invoke-static {v0, p3, p0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p3, Lcom/squareup/ui/activity/-$$Lambda$kA5Mqbcz8uqBm_8A6SyLFwTBmGQ;

    invoke-direct {p3, p2}, Lcom/squareup/ui/activity/-$$Lambda$kA5Mqbcz8uqBm_8A6SyLFwTBmGQ;-><init>(Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$ResidualBillClientErrorRunner;)V

    const/4 p2, 0x0

    .line 49
    invoke-virtual {p0, p1, p3, p2}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p0

    const/4 p1, 0x0

    .line 54
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    return-object p0
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 36
    const-class v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    .line 37
    invoke-interface {v0}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object v0

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 40
    invoke-interface {v0}, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$ResidualBillClientErrorRunner;->failureMessage()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/activity/-$$Lambda$ErrorOnRequestResidualBillScreen$Factory$vnJ2VLEZ0gN-DoHmHX1oX-E2TRU;

    invoke-direct {v3, v1, p1, v0}, Lcom/squareup/ui/activity/-$$Lambda$ErrorOnRequestResidualBillScreen$Factory$vnJ2VLEZ0gN-DoHmHX1oX-E2TRU;-><init>(Landroid/content/res/Resources;Landroid/content/Context;Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$ResidualBillClientErrorRunner;)V

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
