.class public final Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;
.super Ljava/lang/Object;
.source "BulkSettleButton_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/BulkSettleButton;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleButtonPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleButtonPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BulkSettleButtonPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/BulkSettleButton;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCurrency(Lcom/squareup/ui/activity/BulkSettleButton;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleButton;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/activity/BulkSettleButton;Lcom/squareup/ui/activity/BulkSettleButtonPresenter;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleButton;->presenter:Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/BulkSettleButton;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleButtonPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;->injectPresenter(Lcom/squareup/ui/activity/BulkSettleButton;Lcom/squareup/ui/activity/BulkSettleButtonPresenter;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;->injectCurrency(Lcom/squareup/ui/activity/BulkSettleButton;Lcom/squareup/protos/common/CurrencyCode;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/activity/BulkSettleButton;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettleButton_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/BulkSettleButton;)V

    return-void
.end method
