.class public final Lcom/squareup/ui/activity/ActivityApplet_Factory;
.super Ljava/lang/Object;
.source "ActivityApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/ActivityApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityBadgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityBadge;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityBadge;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/activity/ActivityApplet_Factory;->activityBadgeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/ActivityApplet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityBadge;",
            ">;)",
            "Lcom/squareup/ui/activity/ActivityApplet_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/activity/ActivityApplet_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/ActivityApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/ui/activity/ActivityBadge;)Lcom/squareup/ui/activity/ActivityApplet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/activity/ActivityBadge;",
            ")",
            "Lcom/squareup/ui/activity/ActivityApplet;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/activity/ActivityApplet;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/ActivityApplet;-><init>(Ldagger/Lazy;Lcom/squareup/ui/activity/ActivityBadge;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/ActivityApplet;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/ActivityApplet_Factory;->activityBadgeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/ActivityBadge;

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/ActivityApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/ui/activity/ActivityBadge;)Lcom/squareup/ui/activity/ActivityApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/activity/ActivityApplet_Factory;->get()Lcom/squareup/ui/activity/ActivityApplet;

    move-result-object v0

    return-object v0
.end method
