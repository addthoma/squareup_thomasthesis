.class public Lcom/squareup/ui/activity/ActivityIssueRefundStarter;
.super Ljava/lang/Object;
.source "ActivityIssueRefundStarter.java"


# instance fields
.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final flow:Lflow/Flow;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/activity/CurrentBill;Lcom/squareup/activity/LegacyTransactionsHistory;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->flow:Lflow/Flow;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    return-void
.end method

.method private showStartRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/String;Z)V
    .locals 2

    .line 42
    iget-object p3, p0, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    invoke-static {p3, p2, p1, v0, v1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->startIssueRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Flow;Z)V

    return-void
.end method


# virtual methods
.method public showFirstIssueRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 33
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->showStartRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/String;Z)V

    return-void
.end method

.method public showStartRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/activity/ActivityIssueRefundStarter;->showStartRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/lang/String;Z)V

    return-void
.end method
