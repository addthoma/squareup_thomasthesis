.class Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;
.super Lmortar/ViewPresenter;
.source "InvoiceSentSavedPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/invoices/InvoiceSentSavedView;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private invoice:Lcom/squareup/payment/InvoicePayment;

.field private final invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

.field private final invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

.field private final invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/invoices/InvoiceUrlHelper;Lcom/squareup/url/InvoiceShareUrlLauncher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 44
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 48
    iput-object p3, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    .line 49
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    .line 50
    iput-object p5, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->scopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 51
    iput-object p6, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    .line 52
    iput-object p7, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 53
    iput-object p8, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 54
    iput-object p9, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    .line 55
    iput-object p10, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    return-void
.end method

.method public static synthetic lambda$UauaCwosCXnGoPBjjj1Hun1SM7I(Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->startInvoicesApplet()V

    return-void
.end method

.method private startInvoicesApplet()V
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->finish()V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->start()V

    return-void
.end method


# virtual methods
.method finish()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getUniqueKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/TransactionMetrics;->endTransaction(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->invoiceCreated()Z

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->scopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlow(Lcom/squareup/payment/Payment;)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method onCopyLinkClicked()V
    .locals 4

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v1}, Lcom/squareup/payment/InvoicePayment;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    sget-object v2, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/InvoiceUrlHelper;->copyUrlToClipboard(Ljava/lang/String;Lcom/squareup/url/UrlType;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_link_copied:I

    .line 133
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    .line 132
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 59
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->new_sale:I

    .line 60
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setTitleNoUpButton(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v2, Lcom/squareup/permissions/Permission;->INVOICES_APPLET:Lcom/squareup/permissions/Permission;

    invoke-interface {v1, v2}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->view_invoices:I

    .line 65
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/invoices/-$$Lambda$InvoiceSentSavedPresenter$UauaCwosCXnGoPBjjj1Hun1SM7I;

    invoke-direct {v2, p0}, Lcom/squareup/ui/invoices/-$$Lambda$InvoiceSentSavedPresenter$UauaCwosCXnGoPBjjj1Hun1SM7I;-><init>(Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;)V

    .line 66
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 71
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/invoices/InvoiceSentSavedView;

    .line 72
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_saved:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setTitle(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {v1}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->clearSubtitle()V

    goto/16 :goto_2

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->isScheduled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_scheduled:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setTitle(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->getEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setSubtitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->isShareLinkInvoice()Z

    move-result v0

    const-string v2, "email"

    if-eqz v0, :cond_4

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_created:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setTitle(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->getEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_created_subtitle:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    .line 86
    invoke-virtual {v3}, Lcom/squareup/payment/InvoicePayment;->getEmail()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 85
    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 89
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoiceUrlHelper:Lcom/squareup/invoices/InvoiceUrlHelper;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoiceUrlHelper;->canShareInvoiceUrl()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->showShareLinkButtons(Z)V

    .line 90
    invoke-virtual {v1}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->showShareLinkHelper()V

    goto/16 :goto_2

    .line 91
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->isChargedInvoice()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->getInstrumentToCharge()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v0

    .line 93
    iget-object v3, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_charged:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v4, v4, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 94
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "brand"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v0, v0, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v0, v0, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    const-string v4, "pan"

    .line 95
    invoke-virtual {v3, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 97
    iget-object v3, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_charged_receipt_sent:I

    .line 98
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    .line 99
    invoke-virtual {v4}, Lcom/squareup/payment/InvoicePayment;->getBuyerName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "buyer_name"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    .line 100
    invoke-virtual {v4}, Lcom/squareup/payment/InvoicePayment;->getEmail()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 101
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 102
    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setTitle(Ljava/lang/CharSequence;)V

    .line 103
    invoke-virtual {v1, v2}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 105
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->getBuyerName()Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v2, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_sent:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    if-eqz v0, :cond_6

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_detail_customer:I

    .line 109
    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v3, "recipient"

    .line 107
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 106
    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setTitle(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/invoices/InvoiceSentSavedView;->setSubtitle(Ljava/lang/CharSequence;)V

    :cond_7
    :goto_2
    if-nez p1, :cond_8

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v0}, Lcom/squareup/payment/InvoicePayment;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->INVOICE_SENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/payment/Transaction;->closeCurrentTicketBeforeReset(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    :cond_8
    return-void
.end method

.method onMoreOptionsClicked()V
    .locals 4

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    iget-object v1, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v1}, Lcom/squareup/payment/InvoicePayment;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/invoices/InvoiceSentSavedPresenter;->invoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {v2}, Lcom/squareup/payment/InvoicePayment;->getEmail()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/url/InvoiceShareUrlLauncher;->shareUrl(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/url/UrlType;)V

    return-void
.end method
