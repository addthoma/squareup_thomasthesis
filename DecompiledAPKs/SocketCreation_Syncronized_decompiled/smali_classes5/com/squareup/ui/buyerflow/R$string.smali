.class public final Lcom/squareup/ui/buyerflow/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyerflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final amount_charged:I = 0x7f1200cb

.field public static final authorization_failed:I = 0x7f120105

.field public static final buyer_add_tip:I = 0x7f1201d9

.field public static final buyer_auth_declined_message:I = 0x7f1201dc

.field public static final buyer_auth_declined_title:I = 0x7f1201dd

.field public static final buyer_checkout_send_receipt:I = 0x7f1201fe

.field public static final buyer_clear:I = 0x7f1201ff

.field public static final buyer_clear_signature:I = 0x7f120200

.field public static final buyer_copy_link:I = 0x7f120201

.field public static final buyer_coupon_subtitle:I = 0x7f120202

.field public static final buyer_coupon_tip:I = 0x7f120203

.field public static final buyer_custom_tip_amount:I = 0x7f120204

.field public static final buyer_done_label:I = 0x7f120207

.field public static final buyer_message_all_done:I = 0x7f120208

.field public static final buyer_message_all_done_dipped:I = 0x7f120209

.field public static final buyer_message_approved:I = 0x7f12020a

.field public static final buyer_message_approved_dipped:I = 0x7f12020b

.field public static final buyer_message_authorizing_dip:I = 0x7f12020c

.field public static final buyer_message_preparing:I = 0x7f12020d

.field public static final buyer_message_warning_card_removed:I = 0x7f12020e

.field public static final buyer_message_warning_swipe_non_scheme:I = 0x7f12020f

.field public static final buyer_message_warning_swipe_scheme:I = 0x7f120210

.field public static final buyer_order_name_action_bar:I = 0x7f120212

.field public static final buyer_order_name_call_to_action:I = 0x7f120213

.field public static final buyer_please_sign_here:I = 0x7f120215

.field public static final buyer_please_sign_here_manual:I = 0x7f120216

.field public static final buyer_processing:I = 0x7f120246

.field public static final buyer_receipt_text_receipt:I = 0x7f120255

.field public static final buyer_refund_policy_title:I = 0x7f120257

.field public static final buyer_send_receipt_all_done:I = 0x7f12025c

.field public static final buyer_send_receipt_all_done_email:I = 0x7f12025d

.field public static final buyer_send_receipt_all_done_text:I = 0x7f12025e

.field public static final buyer_send_receipt_button_long:I = 0x7f12025f

.field public static final buyer_send_receipt_button_short:I = 0x7f120260

.field public static final buyer_send_receipt_digital_subtitle:I = 0x7f120261

.field public static final buyer_send_receipt_title_cash_change:I = 0x7f120267

.field public static final buyer_send_receipt_title_cash_no_change:I = 0x7f120269

.field public static final buyer_tip_action_bar:I = 0x7f120271

.field public static final buyer_tip_no_tip_amount_plus_auto_grat:I = 0x7f120276

.field public static final buyer_tip_no_tip_amount_plus_service_charge:I = 0x7f120277

.field public static final buyer_tip_no_tip_cap:I = 0x7f120278

.field public static final buyer_tip_plus_amount:I = 0x7f120279

.field public static final buyer_tip_plus_auto_grat_plus_amount:I = 0x7f12027a

.field public static final buyer_tip_plus_service_charge_plus_amount:I = 0x7f12027b

.field public static final buyer_view_refund_policy:I = 0x7f12027c

.field public static final cancel_transaction_content_description:I = 0x7f12029c

.field public static final connection_error_message:I = 0x7f12048f

.field public static final connection_error_title:I = 0x7f120491

.field public static final coupon_applied:I = 0x7f12059c

.field public static final coupon_post_auth_title:I = 0x7f1205a7

.field public static final coupon_use_later:I = 0x7f1205b9

.field public static final crm_cardonfile_header:I = 0x7f120629

.field public static final crm_email_collection_done:I = 0x7f1206af

.field public static final crm_email_collection_enter_email_address:I = 0x7f1206b0

.field public static final crm_email_collection_no_thanks_button:I = 0x7f1206b1

.field public static final crm_email_collection_on_the_list:I = 0x7f1206b2

.field public static final crm_email_collection_prompt:I = 0x7f1206b3

.field public static final crm_email_collection_submit:I = 0x7f1206bc

.field public static final discard_payment_prompt_message:I = 0x7f12087a

.field public static final emv_account_selection_credit:I = 0x7f120a3f

.field public static final emv_account_selection_debit:I = 0x7f120a40

.field public static final emv_account_selection_default:I = 0x7f120a41

.field public static final emv_account_selection_savings:I = 0x7f120a42

.field public static final emv_cancel_payment:I = 0x7f120a46

.field public static final emv_card_removed_msg_not_canceled:I = 0x7f120a4b

.field public static final emv_prepare_payment:I = 0x7f120a5b

.field public static final emv_scheme_fallback_msg:I = 0x7f120a67

.field public static final emv_select_account:I = 0x7f120a6b

.field public static final emv_tech_fallback_msg:I = 0x7f120a6e

.field public static final enable_offline_mode:I = 0x7f120a74

.field public static final enter_offline_mode:I = 0x7f120a7b

.field public static final invoice_paid:I = 0x7f120d3f

.field public static final invoice_paid_remaining_balance:I = 0x7f120d40

.field public static final invoice_paid_transaction_complete:I = 0x7f120d41

.field public static final invoice_unsupported_offline_mode:I = 0x7f120db9

.field public static final loyalty_all_done:I = 0x7f120f0c

.field public static final loyalty_cash_app:I = 0x7f120f10

.field public static final loyalty_enroll_almost_there:I = 0x7f120f26

.field public static final loyalty_enroll_button_check_in:I = 0x7f120f27

.field public static final loyalty_enroll_button_multiple_points:I = 0x7f120f2c

.field public static final loyalty_enroll_button_no_stars:I = 0x7f120f2d

.field public static final loyalty_enroll_button_one_point:I = 0x7f120f2e

.field public static final loyalty_enroll_join_loyalty:I = 0x7f120f2f

.field public static final loyalty_enroll_phone_edit_hint:I = 0x7f120f30

.field public static final loyalty_enroll_phone_edit_prefilled_phone_hint:I = 0x7f120f31

.field public static final loyalty_enroll_qualify:I = 0x7f120f32

.field public static final loyalty_join_loyalty:I = 0x7f120f3a

.field public static final loyalty_no_thanks_button:I = 0x7f120f3b

.field public static final loyalty_points_earned_preview_multiple:I = 0x7f120f41

.field public static final loyalty_points_earned_preview_single:I = 0x7f120f42

.field public static final loyalty_program_name:I = 0x7f120f44

.field public static final loyalty_reward_requirement_no_points:I = 0x7f120f68

.field public static final loyalty_reward_tier_requirement:I = 0x7f120f69

.field public static final loyalty_send_status:I = 0x7f120f77

.field public static final loyalty_status_sent:I = 0x7f120f87

.field public static final loyalty_status_sent_to_phone:I = 0x7f120f88

.field public static final offline_mode_cannot_purchase_gift_cards:I = 0x7f1210cc

.field public static final offline_mode_for_more_information_quick_enable:I = 0x7f1210d3

.field public static final offline_mode_unavailable:I = 0x7f1210db

.field public static final offline_mode_unavailable_hint:I = 0x7f1210dc

.field public static final offline_payment_failed:I = 0x7f1210df

.field public static final offline_payment_failed_message:I = 0x7f1210e0

.field public static final partial_auth_charge_and_continue:I = 0x7f121342

.field public static final partial_auth_charge_and_continue_with_amount:I = 0x7f121343

.field public static final partial_auth_insufficient_funds:I = 0x7f121345

.field public static final partial_auth_message3:I = 0x7f121347

.field public static final saved_card:I = 0x7f121772

.field public static final square_unavailable_message:I = 0x7f1218a5

.field public static final square_unavailable_title:I = 0x7f1218a6

.field public static final tip_message_card_added:I = 0x7f1219ba

.field public static final tip_message_card_inserted:I = 0x7f1219bb

.field public static final tip_message_card_line:I = 0x7f1219bc

.field public static final tip_message_see_cashier:I = 0x7f1219bd

.field public static final try_offline_mode:I = 0x7f121a68

.field public static final try_offline_mode_hint:I = 0x7f121a69

.field public static final view_invoices:I = 0x7f121bab


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
