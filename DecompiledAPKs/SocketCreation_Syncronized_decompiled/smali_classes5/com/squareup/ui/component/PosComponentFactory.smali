.class public Lcom/squareup/ui/component/PosComponentFactory;
.super Ljava/lang/Object;
.source "PosComponentFactory.java"

# interfaces
.implements Lcom/squareup/ui/component/ComponentFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;,
        Lcom/squareup/ui/component/PosComponentFactory$WithComponentFactory;
    }
.end annotation


# static fields
.field private static final NO_GRAPH_FACTORY:Lcom/squareup/ui/WithComponent$Factory;

.field private static final componentFactoryByPathClass:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "+",
            "Lflow/path/Path;",
            ">;",
            "Lcom/squareup/ui/WithComponent$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/squareup/ui/component/PosComponentFactory;->componentFactoryByPathClass:Ljava/util/Map;

    .line 23
    sget-object v0, Lcom/squareup/ui/component/-$$Lambda$PosComponentFactory$9g9CXesfhafs72bX3XfFzlkQ7ro;->INSTANCE:Lcom/squareup/ui/component/-$$Lambda$PosComponentFactory$9g9CXesfhafs72bX3XfFzlkQ7ro;

    sput-object v0, Lcom/squareup/ui/component/PosComponentFactory;->NO_GRAPH_FACTORY:Lcom/squareup/ui/WithComponent$Factory;

    return-void
.end method

.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public createComponent(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 8

    .line 30
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 31
    sget-object v1, Lcom/squareup/ui/component/PosComponentFactory;->componentFactoryByPathClass:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/WithComponent$Factory;

    if-nez v1, :cond_5

    .line 34
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v2

    .line 36
    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_3

    aget-object v6, v2, v5

    .line 37
    instance-of v7, v6, Lcom/squareup/ui/WithComponent$FromFactory;

    if-eqz v7, :cond_0

    .line 38
    check-cast v6, Lcom/squareup/ui/WithComponent$FromFactory;

    .line 41
    :try_start_0
    invoke-interface {v6}, Lcom/squareup/ui/WithComponent$FromFactory;->value()Ljava/lang/Class;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Class;

    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const/4 v2, 0x1

    .line 44
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    new-array v2, v4, [Ljava/lang/Object;

    .line 45
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/WithComponent$Factory;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 56
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-virtual {p1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    .line 54
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_2
    move-exception p1

    .line 52
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_3
    move-exception p1

    .line 50
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    .line 58
    :cond_0
    instance-of v7, v6, Lcom/squareup/ui/WithComponent;

    if-eqz v7, :cond_1

    .line 59
    check-cast v6, Lcom/squareup/ui/WithComponent;

    .line 60
    new-instance v1, Lcom/squareup/ui/component/PosComponentFactory$WithComponentFactory;

    invoke-interface {v6}, Lcom/squareup/ui/WithComponent;->value()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/component/PosComponentFactory$WithComponentFactory;-><init>(Ljava/lang/Class;)V

    goto :goto_1

    .line 62
    :cond_1
    instance-of v7, v6, Lcom/squareup/ui/WithComponent$Responsive;

    if-eqz v7, :cond_2

    .line 63
    check-cast v6, Lcom/squareup/ui/WithComponent$Responsive;

    .line 64
    new-instance v1, Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;

    .line 65
    invoke-interface {v6}, Lcom/squareup/ui/WithComponent$Responsive;->phone()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v6}, Lcom/squareup/ui/WithComponent$Responsive;->tablet()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    if-nez v1, :cond_4

    .line 70
    sget-object v1, Lcom/squareup/ui/component/PosComponentFactory;->NO_GRAPH_FACTORY:Lcom/squareup/ui/WithComponent$Factory;

    .line 72
    :cond_4
    sget-object v2, Lcom/squareup/ui/component/PosComponentFactory;->componentFactoryByPathClass:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_5
    invoke-interface {v1, p1, p2}, Lcom/squareup/ui/WithComponent$Factory;->create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
