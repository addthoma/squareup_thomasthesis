.class public Lcom/squareup/ui/LoadMoreRow;
.super Landroid/widget/ViewAnimator;
.source "LoadMoreRow.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 15
    invoke-direct {p0, p1}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;)V

    .line 18
    sget v0, Lcom/squareup/widgets/pos/R$layout;->load_more_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/LoadMoreRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101004d

    aput v2, v0, v1

    const/4 v2, 0x0

    .line 22
    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 23
    invoke-virtual {p1, v1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/LoadMoreRow;->setMinimumHeight(I)V

    .line 24
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 26
    sget p1, Lcom/squareup/widgets/pos/R$id;->load_more_message:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 27
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->clearGlyph()V

    return-void
.end method


# virtual methods
.method public setLoadMore(Lcom/squareup/ui/LoadMoreState;)V
    .locals 3

    .line 31
    sget-object v0, Lcom/squareup/ui/LoadMoreRow$1;->$SwitchMap$com$squareup$ui$LoadMoreState:[I

    invoke-virtual {p1}, Lcom/squareup/ui/LoadMoreState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 38
    invoke-virtual {p0, v1}, Lcom/squareup/ui/LoadMoreRow;->setDisplayedChild(I)V

    goto :goto_0

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    const/4 p1, 0x0

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/ui/LoadMoreRow;->setDisplayedChild(I)V

    :cond_2
    :goto_0
    return-void
.end method
