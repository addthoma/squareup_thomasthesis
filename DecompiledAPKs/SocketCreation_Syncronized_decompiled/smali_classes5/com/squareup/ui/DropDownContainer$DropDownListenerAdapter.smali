.class public Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;
.super Ljava/lang/Object;
.source "DropDownContainer.java"

# interfaces
.implements Lcom/squareup/ui/DropDownContainer$DropDownListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/DropDownContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DropDownListenerAdapter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDropDownHidden(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onDropDownHiding(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onDropDownOpening(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onDropDownShown(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onDropDownSlide(Landroid/view/View;F)V
    .locals 0

    return-void
.end method
