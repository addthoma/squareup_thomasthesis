.class public Lcom/squareup/ui/items/ItemsAppletSectionList;
.super Lcom/squareup/applet/AppletSectionsList;
.source "ItemsAppletSectionList.java"


# static fields
.field private static final ITEMS_APPLET_ENTRY_POINT_PREF:Ljava/lang/String; = "last-items-applet-screen"


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletSection$AllItems;Lcom/squareup/ui/items/ItemsAppletSection$AllServices;Lcom/squareup/ui/items/ItemsAppletSection$Categories;Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;Lcom/squareup/ui/items/ItemsAppletSection$Discounts;Lcom/squareup/ui/items/ItemsAppletSection$Units;Lcom/squareup/util/Res;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/applet/AppletEntryPoint;

    new-instance v1, Lcom/squareup/settings/StringLocalSetting;

    const-string v2, "last-items-applet-screen"

    invoke-direct {v1, p1, v2}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    const/4 p1, 0x5

    new-array p1, p1, [Lcom/squareup/applet/AppletSection;

    const/4 v2, 0x0

    aput-object p4, p1, v2

    const/4 v2, 0x1

    aput-object p5, p1, v2

    const/4 v2, 0x2

    aput-object p7, p1, v2

    const/4 v2, 0x3

    aput-object p6, p1, v2

    const/4 v2, 0x4

    aput-object p8, p1, v2

    invoke-direct {v0, v1, p2, p3, p1}, Lcom/squareup/applet/AppletEntryPoint;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSectionsList;-><init>(Lcom/squareup/applet/AppletEntryPoint;)V

    .line 36
    invoke-virtual {p3}, Lcom/squareup/ui/items/ItemsAppletSection$AllItems;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSectionList;->visibleEntries:Ljava/util/List;

    new-instance p2, Lcom/squareup/applet/AppletSectionsListEntry;

    iget v0, p3, Lcom/squareup/ui/items/ItemsAppletSection$AllItems;->titleResId:I

    invoke-direct {p2, p3, v0, p9}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_0
    invoke-virtual {p4}, Lcom/squareup/ui/items/ItemsAppletSection$AllServices;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSectionList;->visibleEntries:Ljava/util/List;

    new-instance p2, Lcom/squareup/applet/AppletSectionsListEntry;

    iget p3, p4, Lcom/squareup/ui/items/ItemsAppletSection$AllServices;->titleResId:I

    invoke-direct {p2, p4, p3, p9}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_1
    invoke-virtual {p5}, Lcom/squareup/ui/items/ItemsAppletSection$Categories;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSectionList;->visibleEntries:Ljava/util/List;

    new-instance p2, Lcom/squareup/applet/AppletSectionsListEntry;

    iget p3, p5, Lcom/squareup/ui/items/ItemsAppletSection$Categories;->titleResId:I

    invoke-direct {p2, p5, p3, p9}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_2
    invoke-virtual {p6}, Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSectionList;->visibleEntries:Ljava/util/List;

    new-instance p2, Lcom/squareup/applet/AppletSectionsListEntry;

    iget p3, p6, Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;->titleResId:I

    invoke-direct {p2, p6, p3, p9}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_3
    invoke-virtual {p7}, Lcom/squareup/ui/items/ItemsAppletSection$Discounts;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSectionList;->visibleEntries:Ljava/util/List;

    new-instance p2, Lcom/squareup/applet/AppletSectionsListEntry;

    iget p3, p7, Lcom/squareup/ui/items/ItemsAppletSection$Discounts;->titleResId:I

    invoke-direct {p2, p7, p3, p9}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_4
    invoke-virtual {p8}, Lcom/squareup/ui/items/ItemsAppletSection$Units;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSectionList;->visibleEntries:Ljava/util/List;

    new-instance p2, Lcom/squareup/applet/AppletSectionsListEntry;

    iget p3, p8, Lcom/squareup/ui/items/ItemsAppletSection$Units;->titleResId:I

    invoke-direct {p2, p8, p3, p9}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    return-void
.end method
