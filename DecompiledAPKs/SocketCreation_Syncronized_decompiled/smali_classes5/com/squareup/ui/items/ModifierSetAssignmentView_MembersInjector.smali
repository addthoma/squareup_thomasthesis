.class public final Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;
.super Ljava/lang/Object;
.source "ModifierSetAssignmentView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/ModifierSetAssignmentView;",
        ">;"
    }
.end annotation


# instance fields
.field private final itemPhotosProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/ModifierSetAssignmentView;",
            ">;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectItemPhotos(Lcom/squareup/ui/items/ModifierSetAssignmentView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/ModifierSetAssignmentView;Ljava/lang/Object;)V
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    return-void
.end method

.method public static injectTileAppearanceSettings(Lcom/squareup/ui/items/ModifierSetAssignmentView;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/ModifierSetAssignmentView;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/ModifierSetAssignmentView;Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->injectItemPhotos(Lcom/squareup/ui/items/ModifierSetAssignmentView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->injectTileAppearanceSettings(Lcom/squareup/ui/items/ModifierSetAssignmentView;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/items/ModifierSetAssignmentView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/ModifierSetAssignmentView_MembersInjector;->injectMembers(Lcom/squareup/ui/items/ModifierSetAssignmentView;)V

    return-void
.end method
