.class final Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DetailSearchableListState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListState;->toSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListState;

    .line 81
    instance-of v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    if-eqz v1, :cond_0

    .line 82
    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState$Edit;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/items/DetailSearchableListState;->access$writeDetailSearchableObject(Lcom/squareup/ui/items/DetailSearchableListState;Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    goto :goto_0

    .line 84
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    if-eqz v1, :cond_1

    .line 85
    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/items/DetailSearchableListState;->access$writeDetailSearchableObject(Lcom/squareup/ui/items/DetailSearchableListState;Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    .line 88
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/items/DetailSearchableListState;->access$writeListData(Lcom/squareup/ui/items/DetailSearchableListState;Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    return-void
.end method
