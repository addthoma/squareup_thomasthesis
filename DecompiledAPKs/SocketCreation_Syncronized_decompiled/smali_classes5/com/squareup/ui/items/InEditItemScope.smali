.class public abstract Lcom/squareup/ui/items/InEditItemScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InEditItemScope.java"


# instance fields
.field public final editItemPath:Lcom/squareup/ui/items/EditItemScope;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/items/InEditItemScope;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/items/InEditItemScope;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    return-object v0
.end method

.method getSecondaryButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public getToolTipText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method getUpButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 33
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method getUpButtonText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method isPrimaryButtonEnabled(Lcom/squareup/ui/items/EditItemState;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
