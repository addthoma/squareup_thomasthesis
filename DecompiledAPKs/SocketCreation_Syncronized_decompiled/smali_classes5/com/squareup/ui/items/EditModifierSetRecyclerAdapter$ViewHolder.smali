.class public Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "EditModifierSetRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

.field private existingOptionUpdateWatcher:Landroid/text/TextWatcher;

.field private firedNewOptionWatcher:Z

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private nameHadFocus:Z

.field private newOptionContentWatcher:Lcom/squareup/text/EmptyTextWatcher;

.field private final presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

.field private priceHadfocus:Z

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private row:Landroid/view/View;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final useMultiUnitUI:Z

.field private viewType:I


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;Lcom/squareup/text/Formatter;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;Z)V"
        }
    .end annotation

    .line 82
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    .line 84
    iput p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->viewType:I

    .line 85
    iput-object p6, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 86
    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 87
    iput-object p4, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    .line 88
    iput-object p5, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    .line 89
    iput-object p7, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    .line 90
    iput-object p8, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 91
    iput-boolean p9, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->useMultiUnitUI:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Z
    .locals 0

    .line 61
    iget-boolean p0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->firedNewOptionWatcher:Z

    return p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Z)Z
    .locals 0

    .line 61
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->firedNewOptionWatcher:Z

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/money/PriceLocaleHelper;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Z)Z
    .locals 0

    .line 61
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->priceHadfocus:Z

    return p1
.end method

.method static synthetic access$502(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Z)Z
    .locals 0

    .line 61
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->nameHadFocus:Z

    return p1
.end method

.method static synthetic access$600(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)Landroid/view/View;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public bindDelete()V
    .locals 3

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->delete_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    .line 259
    iget-boolean v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->useMultiUnitUI:Z

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/editbaseobject/R$string;->item_editing_delete_from_location_mod_set:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_set_delete_button:I

    .line 264
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 263
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 267
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$RvHXzj_wL-X79booxdCTJCwYTlk;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/-$$Lambda$RvHXzj_wL-X79booxdCTJCwYTlk;-><init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method public bindModifierOption(Lcom/squareup/api/items/ItemModifierOption$Builder;I)V
    .locals 3

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->getStaticTopRowsCount()I

    move-result v0

    sub-int/2addr p2, v0

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 205
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v2, Lcom/squareup/ui/items/ModifierOptionRow;

    .line 208
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->removeHolderTextChangeListeners()V

    .line 209
    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierOptionRow;->showControls()V

    .line 211
    invoke-virtual {v2, p2, v0}, Lcom/squareup/ui/items/ModifierOptionRow;->setHorizontalBorders(ZZ)V

    .line 212
    iget-object p2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, p1, Lcom/squareup/api/items/ItemModifierOption$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 213
    iget-object v0, p1, Lcom/squareup/api/items/ItemModifierOption$Builder;->name:Ljava/lang/String;

    invoke-virtual {v2, p1, v0, p2}, Lcom/squareup/ui/items/ModifierOptionRow;->setContent(Lcom/squareup/api/items/ItemModifierOption$Builder;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 215
    new-instance p2, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;

    invoke-direct {p2, p0, p1, v2}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$5;-><init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Lcom/squareup/api/items/ItemModifierOption$Builder;Lcom/squareup/ui/items/ModifierOptionRow;)V

    invoke-virtual {v2, p2}, Lcom/squareup/ui/items/ModifierOptionRow;->setDeleteClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    new-instance p1, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;

    invoke-direct {p1, p0, v2}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$6;-><init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Lcom/squareup/ui/items/ModifierOptionRow;)V

    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->existingOptionUpdateWatcher:Landroid/text/TextWatcher;

    .line 237
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->existingOptionUpdateWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/items/ModifierOptionRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 240
    iget-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->priceHadfocus:Z

    if-eqz p1, :cond_1

    .line 241
    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierOptionRow;->getPriceView()Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 242
    iput-boolean v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->priceHadfocus:Z

    goto :goto_1

    .line 243
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->nameHadFocus:Z

    if-eqz p1, :cond_2

    .line 244
    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierOptionRow;->getNameView()Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 245
    iput-boolean v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->nameHadFocus:Z

    :cond_2
    :goto_1
    return-void
.end method

.method public bindStaticNewOptionRow(Z)V
    .locals 3

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/ModifierOptionRow;

    .line 168
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->removeHolderTextChangeListeners()V

    .line 169
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->hideControls()V

    const/4 v1, 0x1

    .line 171
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/items/ModifierOptionRow;->setHorizontalBorders(ZZ)V

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldDisplayModifierInsteadOfOption()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 174
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->setNewModifierContent()V

    goto :goto_0

    .line 176
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->setNewOptionContent()V

    :goto_0
    const/4 p1, 0x0

    .line 178
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->firedNewOptionWatcher:Z

    .line 179
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->priceHadfocus:Z

    .line 181
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->getNameView()Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    .line 182
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierOptionRow;->getPriceView()Lcom/squareup/widgets/SelectableEditText;

    move-result-object v1

    .line 183
    new-instance v2, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;

    invoke-direct {v2, p0, v1, p1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$4;-><init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;Lcom/squareup/widgets/SelectableEditText;Landroid/widget/EditText;)V

    iput-object v2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->newOptionContentWatcher:Lcom/squareup/text/EmptyTextWatcher;

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->newOptionContentWatcher:Lcom/squareup/text/EmptyTextWatcher;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/ModifierOptionRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public bindStaticTopRowContent()V
    .locals 4

    .line 99
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->useMultiUnitUI:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->banner:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AppliedLocationsBanner;

    const/4 v1, 0x0

    .line 101
    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/AppliedLocationsBanner;->setVisibility(I)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldDisplayModifierInsteadOfOption()Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->modifiers_label:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 107
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->uppercase_options:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->name:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketEditText;

    .line 111
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getCurrentName()Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 113
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketEditText;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_2
    new-instance v1, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$1;-><init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    new-instance v1, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$2;-><init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->modifier_set_apply_to_items:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    .line 131
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getNumberOfItemsInSet()I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    .line 142
    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/registerlib/R$string;->modifier_items_count_plural:I

    .line 143
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 142
    invoke-static {v2}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 144
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "item_count"

    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 145
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 138
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_items_count_single:I

    .line 139
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 135
    :cond_4
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_items_count_zero:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 147
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 148
    new-instance v1, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$3;-><init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    sget v1, Lcom/squareup/itemsapplet/R$id;->advanced_modifier_instruction:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 155
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    .line 156
    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_set_advanced_modifier_instruction:I

    const-string v3, "dashboard"

    .line 157
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->dashboard_modifier_url:I

    .line 159
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/configure/item/R$string;->dashboard:I

    .line 160
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 161
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 155
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getViewType()I
    .locals 1

    .line 95
    iget v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->viewType:I

    return v0
.end method

.method public removeHolderTextChangeListeners()V
    .locals 2

    .line 250
    iget v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->viewType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/ModifierOptionRow;

    .line 253
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->existingOptionUpdateWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/ModifierOptionRow;->removeAllTextChangedListener(Landroid/text/TextWatcher;)V

    .line 254
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->newOptionContentWatcher:Lcom/squareup/text/EmptyTextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/ModifierOptionRow;->removeAllTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method
