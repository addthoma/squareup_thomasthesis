.class public final Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;
.super Ljava/lang/Object;
.source "EditServiceMainViewSingleVariationStaticRow_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final intermissionHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->perUnitFormatterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->intermissionHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;",
            ">;"
        }
    .end annotation

    .line 73
    new-instance v10, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static injectAnalytics(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectCatalogIntegrationController(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method public static injectDurationFormatter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/text/DurationFormatter;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method

.method public static injectIntermissionHelper(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/intermission/IntermissionHelper;)V
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPerUnitFormatter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Ljava/lang/Object;)V
    .locals 0

    .line 91
    check-cast p1, Lcom/squareup/ui/items/EditItemMainPresenter;

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    return-void
.end method

.method public static injectPriceLocaleHelper(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/util/Res;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/money/PriceLocaleHelper;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/DurationFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectDurationFormatter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/text/DurationFormatter;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/text/Formatter;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectPerUnitFormatter(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/quantity/PerUnitFormatter;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectCatalogIntegrationController(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectAnalytics(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/analytics/Analytics;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectRes(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/util/Res;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->intermissionHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/intermission/IntermissionHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectIntermissionHelper(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/intermission/IntermissionHelper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow_MembersInjector;->injectMembers(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    return-void
.end method
