.class public final Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;
.super Ljava/lang/Object;
.source "EditDiscountScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditDiscountScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lflow/Flow;)Lcom/squareup/ui/items/EditDiscountScopeRunner;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScopeRunner;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/EditDiscountScopeRunner;-><init>(Lcom/squareup/analytics/Analytics;Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditDiscountScopeRunner;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Flow;

    invoke-static {v0, v1}, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lflow/Flow;)Lcom/squareup/ui/items/EditDiscountScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScopeRunner_Factory;->get()Lcom/squareup/ui/items/EditDiscountScopeRunner;

    move-result-object v0

    return-object v0
.end method
