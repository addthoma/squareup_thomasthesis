.class final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;",
        "parcel",
        "Landroid/os/Parcel;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/os/Parcel;)Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;
    .locals 4

    .line 235
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;

    .line 236
    const-class v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 237
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->Companion:Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v3, "parcel.readString()!!"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object p1

    .line 237
    invoke-virtual {v2, p1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration$Companion;->ofDataType(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object p1

    .line 235
    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 233
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;->invoke(Landroid/os/Parcel;)Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;

    move-result-object p1

    return-object p1
.end method
