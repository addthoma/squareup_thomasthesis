.class public final Lcom/squareup/ui/items/RealImageUploader;
.super Ljava/lang/Object;
.source "RealImageUploader.java"

# interfaces
.implements Lcom/squareup/ui/items/ImageUploader;


# static fields
.field private static final MIME_JPEG:Lokhttp3/MediaType;


# instance fields
.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "image/jpeg"

    .line 14
    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/RealImageUploader;->MIME_JPEG:Lokhttp3/MediaType;

    return-void
.end method

.method constructor <init>(Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/items/RealImageUploader;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method


# virtual methods
.method public uploadImage(Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/items/RealImageUploader;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/ui/library/UploadItemBitmapTask;

    new-instance v2, Lcom/squareup/ui/library/UploadItem;

    sget-object v3, Lcom/squareup/ui/items/RealImageUploader;->MIME_JPEG:Lokhttp3/MediaType;

    invoke-direct {v2, v3, p2}, Lcom/squareup/ui/library/UploadItem;-><init>(Lokhttp3/MediaType;Ljava/io/File;)V

    invoke-direct {v1, p1, v2}, Lcom/squareup/ui/library/UploadItemBitmapTask;-><init>(Ljava/lang/String;Lcom/squareup/ui/library/UploadItem;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method
