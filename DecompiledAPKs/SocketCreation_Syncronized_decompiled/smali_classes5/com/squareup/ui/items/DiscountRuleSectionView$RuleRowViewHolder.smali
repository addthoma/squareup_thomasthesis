.class Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "DiscountRuleSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DiscountRuleSectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RuleRowViewHolder"
.end annotation


# instance fields
.field row:Lcom/squareup/ui/items/DiscountRuleRow;

.field final synthetic this$0:Lcom/squareup/ui/items/DiscountRuleSectionView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DiscountRuleSectionView;Landroid/view/View;)V
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;->this$0:Lcom/squareup/ui/items/DiscountRuleSectionView;

    .line 108
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 109
    check-cast p2, Lcom/squareup/ui/items/DiscountRuleRow;

    iput-object p2, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;->row:Lcom/squareup/ui/items/DiscountRuleRow;

    return-void
.end method


# virtual methods
.method bind(ILcom/squareup/ui/items/RuleRowItem;)V
    .locals 1

    if-nez p1, :cond_0

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;->row:Lcom/squareup/ui/items/DiscountRuleRow;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/DiscountRuleRow;->addBorder(I)V

    .line 116
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;->row:Lcom/squareup/ui/items/DiscountRuleRow;

    iget-object v0, p2, Lcom/squareup/ui/items/RuleRowItem;->key:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/DiscountRuleRow;->setRowKey(Ljava/lang/String;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;->row:Lcom/squareup/ui/items/DiscountRuleRow;

    iget-object v0, p2, Lcom/squareup/ui/items/RuleRowItem;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/DiscountRuleRow;->setRowVal(Ljava/lang/String;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;->row:Lcom/squareup/ui/items/DiscountRuleRow;

    iget v0, p2, Lcom/squareup/ui/items/RuleRowItem;->colorStripColor:I

    if-nez v0, :cond_1

    const/4 p2, -0x1

    goto :goto_0

    :cond_1
    iget p2, p2, Lcom/squareup/ui/items/RuleRowItem;->colorStripColor:I

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/DiscountRuleRow;->setColorStrip(I)V

    return-void
.end method
