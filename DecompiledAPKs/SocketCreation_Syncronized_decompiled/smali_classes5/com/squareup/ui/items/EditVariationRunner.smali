.class public final Lcom/squareup/ui/items/EditVariationRunner;
.super Ljava/lang/Object;
.source "EditVariationRunner.kt"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;
.implements Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;
.implements Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;
.implements Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;
.implements Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;
.implements Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/items/EditItemScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;,
        Lcom/squareup/ui/items/EditVariationRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditVariationRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditVariationRunner.kt\ncom/squareup/ui/items/EditVariationRunner\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,707:1\n92#2,4:708\n1360#3:712\n1429#3,3:713\n*E\n*S KotlinDebug\n*F\n+ 1 EditVariationRunner.kt\ncom/squareup/ui/items/EditVariationRunner\n*L\n256#1,4:708\n512#1:712\n512#1,3:713\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ae\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0008\u000b\n\u0002\u0010\"\n\u0002\u0008\u000e\u0008\u0007\u0018\u0000 \u00ae\u00012\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u0007:\u0004\u00ae\u0001\u00af\u0001B\u009f\u0001\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u0006\u0010,\u001a\u00020-\u00a2\u0006\u0002\u0010.J\u0008\u0010]\u001a\u00020YH\u0016J\u0008\u0010^\u001a\u00020YH\u0016J\u0008\u0010_\u001a\u00020YH\u0016J\u0012\u0010`\u001a\u00020Y2\u0008\u0010a\u001a\u0004\u0018\u00010VH\u0016J\u0010\u0010b\u001a\u00020Y2\u0006\u0010c\u001a\u00020dH\u0016J\u0010\u0010e\u001a\u00020Y2\u0006\u0010f\u001a\u00020dH\u0016J\u0010\u0010g\u001a\u00020Y2\u0006\u0010h\u001a\u00020VH\u0016J$\u0010i\u001a\u00020Y2\u0006\u0010j\u001a\u00020d2\u0008\u0010k\u001a\u0004\u0018\u00010l2\u0008\u0010m\u001a\u0004\u0018\u00010nH\u0002J$\u0010o\u001a\u00020Y2\u0006\u0010j\u001a\u00020d2\u0008\u0010k\u001a\u0004\u0018\u00010l2\u0008\u0010m\u001a\u0004\u0018\u00010nH\u0002J\u001a\u0010p\u001a\u00020Y2\u0008\u0010k\u001a\u0004\u0018\u00010l2\u0008\u0010m\u001a\u0004\u0018\u00010nJ\u0008\u0010q\u001a\u00020YH\u0002J\u0008\u0010r\u001a\u00020YH\u0016J\u0008\u0010s\u001a\u00020YH\u0016J\u000e\u0010t\u001a\u0008\u0012\u0004\u0012\u00020v0uH\u0016J\u000e\u0010w\u001a\u0008\u0012\u0004\u0012\u00020x0uH\u0016J\u0008\u0010y\u001a\u00020YH\u0002J\u0008\u0010z\u001a\u00020YH\u0016J\u0008\u0010{\u001a\u00020YH\u0016J\u0008\u0010|\u001a\u00020YH\u0002J\u0008\u0010}\u001a\u00020YH\u0016J\u0010\u0010~\u001a\u00020Y2\u0006\u0010\u007f\u001a\u00020dH\u0016J\u000c\u0010\u0080\u0001\u001a\u0005\u0018\u00010\u0081\u0001H\u0016J\u000b\u0010\u0082\u0001\u001a\u0004\u0018\u00010VH\u0016J\u0010\u0010\u0083\u0001\u001a\t\u0012\u0005\u0012\u00030\u0084\u00010uH\u0016J\t\u0010\u0085\u0001\u001a\u00020YH\u0016J\u0007\u0010\u0086\u0001\u001a\u00020dJ\t\u0010\u0087\u0001\u001a\u00020dH\u0002J\t\u0010\u0088\u0001\u001a\u00020dH\u0016J\n\u0010\u0089\u0001\u001a\u00030\u0081\u0001H\u0016J\u0013\u0010\u008a\u0001\u001a\u00020Y2\u0008\u0010\u008b\u0001\u001a\u00030\u008c\u0001H\u0016J\t\u0010\u008d\u0001\u001a\u00020YH\u0016J\u0015\u0010\u008e\u0001\u001a\u00020Y2\n\u0010\u008f\u0001\u001a\u0005\u0018\u00010\u0090\u0001H\u0016J\u0013\u0010\u0091\u0001\u001a\u00020Y2\u0008\u0010\u0092\u0001\u001a\u00030\u0090\u0001H\u0016J\"\u0010\u0093\u0001\u001a\u00020Y2\u0007\u0010\u0094\u0001\u001a\u00020V2\u000e\u0010\u0095\u0001\u001a\t\u0012\u0004\u0012\u00020n0\u0096\u0001H\u0016J\u0012\u0010\u0097\u0001\u001a\u00020Y2\u0007\u0010\u0098\u0001\u001a\u00020VH\u0016J\t\u0010\u0099\u0001\u001a\u00020YH\u0016J\t\u0010\u009a\u0001\u001a\u00020YH\u0002J\t\u0010\u009b\u0001\u001a\u00020YH\u0002J\u000f\u0010\u009c\u0001\u001a\u0008\u0012\u0004\u0012\u00020V0uH\u0016J\t\u0010\u009d\u0001\u001a\u00020YH\u0016J\t\u0010\u009e\u0001\u001a\u00020YH\u0016J\t\u0010\u009f\u0001\u001a\u00020YH\u0002J\u0019\u0010\u00a0\u0001\u001a\u00020Y2\u000e\u0010\u00a1\u0001\u001a\t\u0012\u0004\u0012\u00020V0\u00a2\u0001H\u0016J\u0012\u0010\u00a3\u0001\u001a\u00020Y2\u0007\u0010\u00a4\u0001\u001a\u00020dH\u0016J\t\u0010\u00a5\u0001\u001a\u00020YH\u0016J\t\u0010\u00a6\u0001\u001a\u00020YH\u0002J\r\u0010\u00a7\u0001\u001a\u0008\u0012\u0004\u0012\u00020Y0uJ\u0012\u0010\u00a8\u0001\u001a\u00020Y2\u0007\u0010\u00a9\u0001\u001a\u00020VH\u0016J\u0012\u0010\u00aa\u0001\u001a\u00020Y2\u0007\u0010\u00ab\u0001\u001a\u00020VH\u0016J\u0012\u0010\u00ac\u0001\u001a\u00020Y2\u0007\u0010\u00ad\u0001\u001a\u00020VH\u0016R\u000e\u0010,\u001a\u00020-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010/\u001a\u0004\u0018\u0001008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0016\n\u0002\u00107\u0012\u0004\u00081\u00102\u001a\u0004\u00083\u00104\"\u0004\u00085\u00106R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00108\u001a\u000209X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010:\u001a\u0004\u0018\u00010;X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010<\u001a\u00020=X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008>\u0010?\"\u0004\u0008@\u0010AR\u0010\u0010B\u001a\u0004\u0018\u00010CX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010D\u001a\u0004\u0018\u0001008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0016\n\u0002\u00107\u0012\u0004\u0008E\u00102\u001a\u0004\u0008F\u00104\"\u0004\u0008G\u00106R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010H\u001a\u0004\u0018\u0001008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0016\n\u0002\u00107\u0012\u0004\u0008I\u00102\u001a\u0004\u0008J\u00104\"\u0004\u0008K\u00106R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010L\u001a\u0004\u0018\u0001008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0016\n\u0002\u00107\u0012\u0004\u0008M\u00102\u001a\u0004\u0008N\u00104\"\u0004\u0008O\u00106R(\u0010P\u001a\u0004\u0018\u0001008\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0016\n\u0002\u00107\u0012\u0004\u0008Q\u00102\u001a\u0004\u0008R\u00104\"\u0004\u0008S\u00106R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010T\u001a\u0008\u0012\u0004\u0012\u00020V0UX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010W\u001a\u0008\u0012\u0004\u0012\u00020Y0XX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010Z\u001a\u00020[X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\\\u001a\u0008\u0012\u0004\u0012\u00020Y0XX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00b0\u0001"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditVariationRunner;",
        "Lmortar/bundler/Bundler;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;",
        "Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;",
        "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
        "Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;",
        "Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;",
        "editItemState",
        "Lcom/squareup/ui/items/EditItemState;",
        "flow",
        "Lflow/Flow;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "editInventoryStateController",
        "Lcom/squareup/ui/items/EditInventoryStateController;",
        "barcodeScannerTracker",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "durationPickerRunner",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "servicesCustomization",
        "Lcom/squareup/appointmentsapi/ServicesCustomization;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "res",
        "Lcom/squareup/util/Res;",
        "eventLogger",
        "Lcom/squareup/ui/items/EditItemEventLogger;",
        "employeeTextHelper",
        "Lcom/squareup/ui/items/EmployeeTextHelper;",
        "unsupportedItemOptionActionDialogRunner",
        "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
        "intermissionHelper",
        "Lcom/squareup/intermission/IntermissionHelper;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/ui/items/EditItemState;Lflow/Flow;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/ui/items/EmployeeTextHelper;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/text/DurationFormatter;Lcom/squareup/analytics/Analytics;)V",
        "durationToken",
        "",
        "durationToken$annotations",
        "()V",
        "getDurationToken",
        "()Ljava/lang/Long;",
        "setDurationToken",
        "(Ljava/lang/Long;)V",
        "Ljava/lang/Long;",
        "editItemPath",
        "Lcom/squareup/ui/items/EditItemScope;",
        "editItemVariationScreen",
        "Lcom/squareup/ui/items/EditItemVariationScreen;",
        "editItemVariationState",
        "Lcom/squareup/ui/items/EditVariationState;",
        "getEditItemVariationState",
        "()Lcom/squareup/ui/items/EditVariationState;",
        "setEditItemVariationState",
        "(Lcom/squareup/ui/items/EditVariationState;)V",
        "editServiceVariationScreen",
        "Lcom/squareup/ui/items/EditServiceVariationScreen;",
        "extraTimeToken",
        "extraTimeToken$annotations",
        "getExtraTimeToken",
        "setExtraTimeToken",
        "finalDurationToken",
        "finalDurationToken$annotations",
        "getFinalDurationToken",
        "setFinalDurationToken",
        "gapDurationToken",
        "gapDurationToken$annotations",
        "getGapDurationToken",
        "setGapDurationToken",
        "initialDurationToken",
        "initialDurationToken$annotations",
        "getInitialDurationToken",
        "setInitialDurationToken",
        "scannedBarcodeRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "serviceKicker",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "staffSerialSubscription",
        "Lio/reactivex/disposables/SerialDisposable;",
        "variationEditingFinishedRelay",
        "assignEmployeesClicked",
        "assignedEmployeesFinish",
        "backClickedWhenEditingItemVariation",
        "barcodeScanned",
        "barcode",
        "blockExtraTimeToggled",
        "blocked",
        "",
        "bookableByCustomerToggled",
        "bookable",
        "cancellationFeeChanged",
        "newCancellationFee",
        "createOrEditItemVariation",
        "isCreatingNewVariation",
        "variation",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
        "measurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "createOrEditServiceVariation",
        "createOrEditVariation",
        "discardItemVariationEdits",
        "doneClickedWhenEditingItemVariation",
        "durationRowClicked",
        "editItemVariationScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
        "editServiceVariationScreenData",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
        "ensureEditingStarted",
        "extraTimeRowClicked",
        "finalDurationRowClicked",
        "finishEditing",
        "gapDurationRowClicked",
        "gapTimeToggled",
        "checked",
        "getIncludedTax",
        "Lcom/squareup/protos/common/Money;",
        "getMortarBundleKey",
        "getStaffItemState",
        "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;",
        "initialDurationRowClicked",
        "isEditingVariation",
        "isUnitPricedServiceCreationEnabled",
        "isVariablePriceType",
        "maxCancellationFeeMoney",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onSave",
        "outState",
        "onUnitAssignedToVariation",
        "unitId",
        "createdUnits",
        "",
        "priceDescriptionChanged",
        "newPriceDescription",
        "removeButtonClickedWhenEditingItemVariation",
        "removeVariationInEditing",
        "saveItemVariationEdits",
        "scannedBarcode",
        "servicePriceTypeFinish",
        "serviceVariationPriceTypeClicked",
        "setAllStaffAsAssignable",
        "setEmployeeAssigned",
        "employeeIds",
        "",
        "setVariablePriceType",
        "isVariablePrice",
        "unitTypeSelectClicked",
        "updateServiceScreenData",
        "variationEditingFinished",
        "variationNameChanged",
        "newName",
        "variationPriceChanged",
        "newPrice",
        "variationSkuChanged",
        "newSku",
        "Companion",
        "EditVariationScreenData",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/EditVariationRunner$Companion;

.field public static final EDIT_ITEM_VARIATION_STATE_KEY:Ljava/lang/String; = "EDIT_ITEM_VARIATION_STATE_KEY"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

.field private durationToken:Ljava/lang/Long;

.field private final editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

.field private editItemPath:Lcom/squareup/ui/items/EditItemScope;

.field private final editItemState:Lcom/squareup/ui/items/EditItemState;

.field private editItemVariationScreen:Lcom/squareup/ui/items/EditItemVariationScreen;

.field private editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

.field private editServiceVariationScreen:Lcom/squareup/ui/items/EditServiceVariationScreen;

.field private final employeeTextHelper:Lcom/squareup/ui/items/EmployeeTextHelper;

.field private final eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

.field private extraTimeToken:Ljava/lang/Long;

.field private final features:Lcom/squareup/settings/server/Features;

.field private finalDurationToken:Ljava/lang/Long;

.field private final flow:Lflow/Flow;

.field private gapDurationToken:Ljava/lang/Long;

.field private initialDurationToken:Ljava/lang/Long;

.field private final intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final scannedBarcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceKicker:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final staffSerialSubscription:Lio/reactivex/disposables/SerialDisposable;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

.field private final variationEditingFinishedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/EditVariationRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditVariationRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/EditVariationRunner;->Companion:Lcom/squareup/ui/items/EditVariationRunner$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemState;Lflow/Flow;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/ui/items/EmployeeTextHelper;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/text/DurationFormatter;Lcom/squareup/analytics/Analytics;)V
    .locals 16
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "editItemState"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInventoryStateController"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "barcodeScannerTracker"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationPickerRunner"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "servicesCustomization"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventLogger"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeTextHelper"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unsupportedItemOptionActionDialogRunner"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intermissionHelper"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iput-object v2, v0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    iput-object v3, v0, Lcom/squareup/ui/items/EditVariationRunner;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object v4, v0, Lcom/squareup/ui/items/EditVariationRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    iput-object v5, v0, Lcom/squareup/ui/items/EditVariationRunner;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iput-object v6, v0, Lcom/squareup/ui/items/EditVariationRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object v7, v0, Lcom/squareup/ui/items/EditVariationRunner;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    iput-object v8, v0, Lcom/squareup/ui/items/EditVariationRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object v9, v0, Lcom/squareup/ui/items/EditVariationRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    iput-object v10, v0, Lcom/squareup/ui/items/EditVariationRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v11, v0, Lcom/squareup/ui/items/EditVariationRunner;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object v12, v0, Lcom/squareup/ui/items/EditVariationRunner;->features:Lcom/squareup/settings/server/Features;

    iput-object v13, v0, Lcom/squareup/ui/items/EditVariationRunner;->res:Lcom/squareup/util/Res;

    iput-object v14, v0, Lcom/squareup/ui/items/EditVariationRunner;->eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->employeeTextHelper:Lcom/squareup/ui/items/EmployeeTextHelper;

    iput-object v15, v0, Lcom/squareup/ui/items/EditVariationRunner;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    iput-object v2, v0, Lcom/squareup/ui/items/EditVariationRunner;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    move-object/from16 v1, p19

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 99
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.createDefault(Unit)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->serviceKicker:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 100
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    const-string v2, "PublishRelay.create()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->scannedBarcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 101
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.create()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->variationEditingFinishedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 102
    new-instance v1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->staffSerialSubscription:Lio/reactivex/disposables/SerialDisposable;

    .line 117
    sget-object v1, Lcom/squareup/ui/items/EditVariationState;->Companion:Lcom/squareup/ui/items/EditVariationState$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState$Companion;->create()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    return-void
.end method

.method public static final synthetic access$ensureEditingStarted(Lcom/squareup/ui/items/EditVariationRunner;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/ui/items/EditVariationRunner;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getEditItemState$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/ui/items/EditItemState;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    return-object p0
.end method

.method public static final synthetic access$getEmployeeTextHelper$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/ui/items/EmployeeTextHelper;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/ui/items/EditVariationRunner;->employeeTextHelper:Lcom/squareup/ui/items/EmployeeTextHelper;

    return-object p0
.end method

.method public static final synthetic access$getIntermissionHelper$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/intermission/IntermissionHelper;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/ui/items/EditVariationRunner;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    return-object p0
.end method

.method public static final synthetic access$isUnitPricedServiceCreationEnabled(Lcom/squareup/ui/items/EditVariationRunner;)Z
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->isUnitPricedServiceCreationEnabled()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$updateServiceScreenData(Lcom/squareup/ui/items/EditVariationRunner;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    return-void
.end method

.method private final createOrEditItemVariation(ZLcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 207
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->createNewEmptyVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    .line 208
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    const-string p3, "newVariation"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditVariationState;->startCreatingNewVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 210
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object p1

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState()Lrx/Observable;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lrx/Observable;->toSingle()Lrx/Single;

    move-result-object v0

    .line 214
    new-instance v1, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;-><init>(Lcom/squareup/ui/items/EditVariationRunner;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 224
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object p2, p0

    check-cast p2, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {p1, p2}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 225
    new-instance p1, Lcom/squareup/ui/items/EditItemVariationScreen;

    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez p2, :cond_2

    const-string p3, "editItemPath"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p1, p2}, Lcom/squareup/ui/items/EditItemVariationScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationScreen:Lcom/squareup/ui/items/EditItemVariationScreen;

    .line 226
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationScreen:Lcom/squareup/ui/items/EditItemVariationScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final createOrEditServiceVariation(ZLcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 7

    if-eqz p1, :cond_1

    .line 235
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->createNewEmptyVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    const-string p2, "newVariation"

    .line 237
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 p2, 0x1e

    .line 236
    invoke-static {p2, p3}, Lorg/threeten/bp/Duration;->ofMinutes(J)Lorg/threeten/bp/Duration;

    move-result-object p2

    .line 237
    invoke-virtual {p2}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide p2

    invoke-virtual {p1, p2, p3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    const/4 p2, 0x1

    .line 238
    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setAvailableOnOnlineBookingSite(Z)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 239
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->isUnitPricedServiceCreationEnabled()Z

    move-result p2

    if-nez p2, :cond_0

    .line 241
    new-instance p2, Lcom/squareup/protos/common/Money;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p2, p3, v0}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrClearPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 244
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditVariationState;->startCreatingNewVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->setAllStaffAsAssignable()V

    goto :goto_0

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    if-nez p2, :cond_2

    .line 248
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p2

    move-object v4, p3

    .line 247
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/items/EditVariationState;->startEditingExistingVariation$default(Lcom/squareup/ui/items/EditVariationState;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ILjava/lang/Object;)V

    .line 251
    :goto_0
    new-instance p1, Lcom/squareup/ui/items/EditServiceVariationScreen;

    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez p2, :cond_3

    const-string p3, "editItemPath"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p1, p2}, Lcom/squareup/ui/items/EditServiceVariationScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editServiceVariationScreen:Lcom/squareup/ui/items/EditServiceVariationScreen;

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editServiceVariationScreen:Lcom/squareup/ui/items/EditServiceVariationScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final discardItemVariationEdits()V
    .locals 4

    .line 590
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->CREATING_NEW_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v0, v1, :cond_1

    .line 591
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    .line 592
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v1

    .line 591
    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditInventoryStateController;->removeInventoryAssignment(Ljava/lang/String;)V

    goto :goto_0

    .line 595
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    .line 596
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v1

    .line 597
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationState;->getInventoryAssignmentCountOriginal()Ljava/math/BigDecimal;

    move-result-object v2

    .line 598
    iget-object v3, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditVariationState;->getInventoryAssignmentUnitCostOriginal()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 595
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/items/EditInventoryStateController;->updateInventoryAssignment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    :goto_0
    return-void
.end method

.method public static synthetic durationToken$annotations()V
    .locals 0

    return-void
.end method

.method private final ensureEditingStarted()V
    .locals 1

    .line 639
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner;->isEditingVariation()Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method

.method public static synthetic extraTimeToken$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic finalDurationToken$annotations()V
    .locals 0

    return-void
.end method

.method private final finishEditing()V
    .locals 2

    .line 631
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object v1, p0

    check-cast v1, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 632
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->finishEditingVariation()V

    .line 633
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->variationEditingFinishedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 634
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 635
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Item Variation Screen Dismissed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic gapDurationToken$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic initialDurationToken$annotations()V
    .locals 0

    return-void
.end method

.method private final isUnitPricedServiceCreationEnabled()Z
    .locals 2

    .line 663
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final removeVariationInEditing()V
    .locals 3

    .line 604
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->EDITING_EXISTING_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 606
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    .line 607
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/items/EditInventoryStateController;->removeInventoryAssignment(Ljava/lang/String;)V

    .line 609
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->removeItemVariationById(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 610
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState;->pendingDeletions:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method private final saveItemVariationEdits()V
    .locals 3

    .line 616
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez v0, :cond_0

    const-string v1, "editItemPath"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 617
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 620
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->CREATING_NEW_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v0, v1, :cond_3

    .line 621
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemState$ItemData;->addItemVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)V

    goto :goto_0

    .line 623
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    .line 624
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditingOriginal()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    .line 625
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v2

    .line 623
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->replaceItemVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Z

    :goto_0
    return-void
.end method

.method private final setAllStaffAsAssignable()V
    .locals 4

    .line 643
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 645
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->staffSerialSubscription:Lio/reactivex/disposables/SerialDisposable;

    .line 646
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    invoke-interface {v1}, Lcom/squareup/appointmentsapi/ServicesCustomization;->allStaffInfo()Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 647
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v1

    .line 648
    sget-object v2, Lcom/squareup/ui/items/EditVariationRunner$setAllStaffAsAssignable$1;->INSTANCE:Lcom/squareup/ui/items/EditVariationRunner$setAllStaffAsAssignable$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 651
    new-instance v2, Lcom/squareup/ui/items/EditVariationRunner$setAllStaffAsAssignable$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/EditVariationRunner$setAllStaffAsAssignable$2;-><init>(Lcom/squareup/ui/items/EditVariationRunner;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 645
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private final updateServiceScreenData()V
    .locals 2

    .line 659
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->serviceKicker:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public assignEmployeesClicked()V
    .locals 4

    .line 448
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 449
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCardScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez v2, :cond_0

    const-string v3, "editItemPath"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCardScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public assignedEmployeesFinish()V
    .locals 1

    .line 467
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 468
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public backClickedWhenEditingItemVariation()V
    .locals 1

    .line 529
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 530
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->discardItemVariationEdits()V

    .line 531
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->finishEditing()V

    const/4 v0, 0x0

    .line 532
    check-cast v0, Lcom/squareup/ui/items/EditItemVariationScreen;

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationScreen:Lcom/squareup/ui/items/EditItemVariationScreen;

    return-void
.end method

.method public barcodeScanned(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 555
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->scannedBarcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 554
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public blockExtraTimeToggled(Z)V
    .locals 2

    .line 417
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    if-eqz p1, :cond_0

    const/16 p1, 0xf

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    int-to-long v0, p1

    .line 420
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 419
    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMinutes(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 420
    invoke-virtual {v0}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setTransitionTime(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 421
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    return-void
.end method

.method public bookableByCustomerToggled(Z)V
    .locals 1

    .line 437
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 438
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setAvailableOnOnlineBookingSite(Z)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_0
    if-eqz p1, :cond_1

    .line 440
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->setAllStaffAsAssignable()V

    goto :goto_0

    .line 442
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setEmployeeTokens(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 444
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    return-void
.end method

.method public cancellationFeeChanged(Ljava/lang/String;)V
    .locals 1

    const-string v0, "newCancellationFee"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 378
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 379
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 380
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setNoShowFee(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_0
    return-void
.end method

.method public final createOrEditVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 3

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 194
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez v1, :cond_1

    const-string v2, "editItemPath"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v1, v2, :cond_2

    .line 195
    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/ui/items/EditVariationRunner;->createOrEditServiceVariation(ZLcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    goto :goto_1

    .line 197
    :cond_2
    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/ui/items/EditVariationRunner;->createOrEditItemVariation(ZLcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    :goto_1
    return-void
.end method

.method public doneClickedWhenEditingItemVariation()V
    .locals 0

    .line 536
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 537
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->saveItemVariationEdits()V

    .line 538
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->finishEditing()V

    return-void
.end method

.method public durationRowClicked()V
    .locals 6

    .line 366
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 367
    sget-object v0, Lkotlin/random/Random;->Default:Lkotlin/random/Random$Default;

    invoke-virtual {v0}, Lkotlin/random/Random$Default;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->durationToken:Ljava/lang/Long;

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    .line 369
    sget v1, Lcom/squareup/widgets/pos/R$string;->duration_title:I

    .line 370
    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    .line 371
    iget-object v3, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v3

    const-string v4, "Duration.ofMillis(editIt\u2026ionInEditing?.duration!!)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 372
    iget-object v4, p0, Lcom/squareup/ui/items/EditVariationRunner;->durationToken:Ljava/lang/Long;

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 370
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;-><init>(Lorg/threeten/bp/Duration;J)V

    const/4 v3, 0x1

    .line 368
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V

    return-void
.end method

.method public editItemVariationScreenData()Lrx/Observable;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
            ">;"
        }
    .end annotation

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 299
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    goto :goto_0

    :cond_0
    move-object v6, v2

    :goto_0
    if-eqz v1, :cond_1

    .line 300
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v2

    :cond_1
    move-object v10, v2

    .line 302
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditVariationState;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v12

    .line 303
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->isItemAssignedWithIemOptions()Z

    move-result v13

    .line 304
    sget-object v2, Lcom/squareup/ui/items/ItemVariationEditingState;->CREATING_NEW_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v0, v2, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    .line 305
    :goto_1
    sget-object v2, Lcom/squareup/ui/items/ItemVariationEditingState;->EDITING_EXISTING_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v0, v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_4

    .line 306
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v4, :cond_4

    const/4 v2, 0x1

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    if-nez v5, :cond_6

    if-eqz v2, :cond_5

    goto :goto_4

    :cond_5
    const/4 v14, 0x0

    goto :goto_5

    :cond_6
    :goto_4
    const/4 v14, 0x1

    .line 309
    :goto_5
    sget-object v2, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v0, v2, :cond_7

    const/4 v3, 0x1

    :cond_7
    const-string v0, "Should not subscribe to EditItemVariationScreenData when variation editing is not started."

    .line 308
    invoke-static {v3, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 313
    new-instance v0, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;

    if-nez v6, :cond_8

    .line 315
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 316
    :cond_8
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getName()Ljava/lang/String;

    move-result-object v7

    .line 317
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v8

    .line 318
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->hasInclusiveTaxesApplied()Z

    move-result v9

    .line 320
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getSku()Ljava/lang/String;

    move-result-object v11

    move-object v4, v0

    .line 313
    invoke-direct/range {v4 .. v14}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZZ)V

    .line 312
    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-string v1, "rx.Observable.just(\n    \u2026teButton\n\n        )\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public editServiceVariationScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
            ">;"
        }
    .end annotation

    .line 256
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    invoke-interface {v0}, Lcom/squareup/appointmentsapi/ServicesCustomization;->businessInfo()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    invoke-interface {v1}, Lcom/squareup/appointmentsapi/ServicesCustomization;->allStaffInfo()Lio/reactivex/Observable;

    move-result-object v1

    .line 258
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->serviceKicker:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v2, Lio/reactivex/Observable;

    .line 709
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    check-cast v2, Lio/reactivex/ObservableSource;

    .line 710
    new-instance v3, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;-><init>(Lcom/squareup/ui/items/EditVariationRunner;)V

    check-cast v3, Lio/reactivex/functions/Function3;

    .line 708
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026unction(t1, t2, t3) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 711
    check-cast v0, Lio/reactivex/ObservableSource;

    .line 293
    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public extraTimeRowClicked()V
    .locals 6

    .line 425
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 426
    sget-object v0, Lkotlin/random/Random;->Default:Lkotlin/random/Random$Default;

    invoke-virtual {v0}, Lkotlin/random/Random$Default;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->extraTimeToken:Ljava/lang/Long;

    .line 427
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    .line 428
    sget v1, Lcom/squareup/edititem/R$string;->edit_service_extra_time:I

    .line 429
    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    .line 430
    iget-object v3, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getTransitionTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-nez v3, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v3

    const-string v4, "Duration.ofMillis(editIt\u2026diting?.transitionTime!!)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 431
    iget-object v4, p0, Lcom/squareup/ui/items/EditVariationRunner;->extraTimeToken:Ljava/lang/Long;

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 429
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;-><init>(Lorg/threeten/bp/Duration;J)V

    const/4 v3, 0x0

    .line 427
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V

    return-void
.end method

.method public finalDurationRowClicked()V
    .locals 5

    .line 410
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getFinalDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 412
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/squareup/intermission/IntermissionHelper$DefaultImpls;->startDurationPicker$default(Lcom/squareup/intermission/IntermissionHelper;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->finalDurationToken:Ljava/lang/Long;

    .line 413
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    return-void
.end method

.method public gapDurationRowClicked()V
    .locals 5

    .line 403
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 404
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getGapDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 405
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/squareup/intermission/IntermissionHelper$DefaultImpls;->startDurationPicker$default(Lcom/squareup/intermission/IntermissionHelper;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->gapDurationToken:Ljava/lang/Long;

    .line 406
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    return-void
.end method

.method public gapTimeToggled(Z)V
    .locals 2

    .line 388
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->gapTimeToggled(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Z)V

    .line 390
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    .line 391
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez v0, :cond_1

    const-string v1, "editItemPath"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/squareup/appointmentsapi/ServicesCustomization;->maybeShowGapTimeEducation(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    .line 392
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->GAP_TIME_TOGGLE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public final getDurationToken()Ljava/lang/Long;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->durationToken:Ljava/lang/Long;

    return-object v0
.end method

.method public final getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    return-object v0
.end method

.method public final getExtraTimeToken()Ljava/lang/Long;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->extraTimeToken:Ljava/lang/Long;

    return-object v0
.end method

.method public final getFinalDurationToken()Ljava/lang/Long;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->finalDurationToken:Ljava/lang/Long;

    return-object v0
.end method

.method public final getGapDurationToken()Ljava/lang/Long;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->gapDurationToken:Ljava/lang/Long;

    return-object v0
.end method

.method public getIncludedTax()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 546
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    .line 549
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/EditItemState;->calculateIncludedTax(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    :goto_1
    return-object v1
.end method

.method public final getInitialDurationToken()Ljava/lang/Long;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->initialDurationToken:Ljava/lang/Long;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 169
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStaffItemState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;",
            ">;"
        }
    .end annotation

    .line 453
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 454
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    invoke-interface {v0}, Lcom/squareup/appointmentsapi/ServicesCustomization;->allStaffInfo()Lio/reactivex/Observable;

    move-result-object v0

    .line 455
    new-instance v1, Lcom/squareup/ui/items/EditVariationRunner$getStaffItemState$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditVariationRunner$getStaffItemState$1;-><init>(Lcom/squareup/ui/items/EditVariationRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "servicesCustomization.al\u2026ns, staffInfos)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 458
    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public initialDurationRowClicked()V
    .locals 5

    .line 396
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 397
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getInitialDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 398
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/squareup/intermission/IntermissionHelper$DefaultImpls;->startDurationPicker$default(Lcom/squareup/intermission/IntermissionHelper;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->initialDurationToken:Ljava/lang/Long;

    .line 399
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    return-void
.end method

.method public final isEditingVariation()Z
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVariablePriceType()Z
    .locals 1

    .line 562
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 563
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isVariablePricing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public maxCancellationFeeMoney()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 384
    sget-object v0, Lcom/squareup/ui/items/CancellationFeePriceHelper;->INSTANCE:Lcom/squareup/ui/items/CancellationFeePriceHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/CancellationFeePriceHelper;->getMaxCancellationFee(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const-string v1, "RegisterTreeKey.get(scope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/items/EditItemScope;

    iput-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->datePicked()Lrx/Observable;

    move-result-object v0

    .line 123
    new-instance v1, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditVariationRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/items/EditVariationRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->staffSerialSubscription:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_1

    const-string v0, "EDIT_ITEM_VARIATION_STATE_KEY"

    .line 173
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Lcom/squareup/ui/items/EditVariationState;

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    check-cast v0, Landroid/os/Parcelable;

    const-string v1, "EDIT_ITEM_VARIATION_STATE_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onUnitAssignedToVariation(Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "unitId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createdUnits"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 506
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 509
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v4

    .line 510
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    .line 512
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    check-cast p2, Ljava/lang/Iterable;

    .line 712
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p2, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 713
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 714
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 512
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 715
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 512
    invoke-static {v1}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 514
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    move-object v1, v2

    goto :goto_1

    :cond_3
    move-object v1, p1

    :goto_1
    invoke-virtual {p2, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setCatalogMeasurementUnitToken(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 515
    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_4

    move-object v5, v2

    goto :goto_2

    .line 516
    :cond_4
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-object v5, p2

    .line 517
    :goto_2
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p2, v5}, Lcom/squareup/ui/items/EditVariationState;->setMeasurementUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 519
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

    .line 520
    iget-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p2

    if-nez p2, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    const-string p2, "editItemVariationState.variationInEditing!!.id"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, p1

    .line 519
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/items/EditItemEventLogger;->logMeasurementUnitAssignment(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/lang/String;)V

    return-void
.end method

.method public priceDescriptionChanged(Ljava/lang/String;)V
    .locals 1

    const-string v0, "newPriceDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 582
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 583
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPriceDescription(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_0
    return-void
.end method

.method public removeButtonClickedWhenEditingItemVariation()V
    .locals 2

    .line 472
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/ItemVariationEditingState;->EDITING_EXISTING_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 474
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->isItemAssignedWithIemOptions()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result v0

    if-nez v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->showUnsupportedVariationDeleteErrorDialog()V

    goto :goto_1

    .line 477
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->removeVariationInEditing()V

    .line 478
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->finishEditing()V

    :goto_1
    return-void
.end method

.method public scannedBarcode()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 542
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->scannedBarcodeRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public servicePriceTypeFinish()V
    .locals 1

    .line 578
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public serviceVariationPriceTypeClicked()V
    .locals 4

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCardScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez v2, :cond_0

    const-string v3, "editItemPath"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCardScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final setDurationToken(Ljava/lang/Long;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->durationToken:Ljava/lang/Long;

    return-void
.end method

.method public final setEditItemVariationState(Lcom/squareup/ui/items/EditVariationState;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    return-void
.end method

.method public setEmployeeAssigned(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "employeeIds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 462
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 463
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setEmployeeTokens(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_0
    return-void
.end method

.method public final setExtraTimeToken(Ljava/lang/Long;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->extraTimeToken:Ljava/lang/Long;

    return-void
.end method

.method public final setFinalDurationToken(Ljava/lang/Long;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->finalDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public final setGapDurationToken(Ljava/lang/Long;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->gapDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public final setInitialDurationToken(Ljava/lang/Long;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->initialDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public setVariablePriceType(Z)V
    .locals 3

    .line 567
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    if-eqz p1, :cond_0

    .line 569
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->clearPrice()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_0

    .line 571
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    if-eqz p1, :cond_1

    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 574
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditVariationRunner;->servicePriceTypeFinish()V

    return-void
.end method

.method public unitTypeSelectClicked()V
    .locals 11

    .line 483
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationScreen:Lcom/squareup/ui/items/EditItemVariationScreen;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editServiceVariationScreen:Lcom/squareup/ui/items/EditServiceVariationScreen;

    :goto_0
    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    move-object v5, v0

    if-eqz v5, :cond_3

    .line 486
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 487
    :cond_1
    iget-object v9, p0, Lcom/squareup/ui/items/EditVariationRunner;->flow:Lflow/Flow;

    .line 488
    new-instance v10, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;

    .line 489
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editServiceVariationScreen:Lcom/squareup/ui/items/EditServiceVariationScreen;

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/squareup/ui/items/unit/ItemType;->SERVICE:Lcom/squareup/ui/items/unit/ItemType;

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/squareup/ui/items/unit/ItemType;->ITEM:Lcom/squareup/ui/items/unit/ItemType;

    :goto_1
    move-object v2, v1

    .line 490
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v1, "variationInEditing.catalogMeasurementUnitToken"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 491
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v4

    .line 493
    iget-object v6, p0, Lcom/squareup/ui/items/EditVariationRunner;->res:Lcom/squareup/util/Res;

    .line 494
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    .line 495
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/EditInventoryStateController;->doesVariationHaveStockCount(Ljava/lang/String;)Z

    move-result v7

    .line 496
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->shouldShowDefaultUnits()Z

    move-result v8

    move-object v1, v10

    .line 488
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;-><init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/util/Res;ZZ)V

    .line 487
    invoke-virtual {v9, v10}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public final variationEditingFinished()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 559
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->variationEditingFinishedRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public variationNameChanged(Ljava/lang/String;)V
    .locals 1

    const-string v0, "newName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 332
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    return-void
.end method

.method public variationPriceChanged(Ljava/lang/String;)V
    .locals 4

    const-string v0, "newPrice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 340
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 342
    check-cast p1, Ljava/lang/CharSequence;

    .line 343
    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/squareup/ui/items/EditVariationRunner;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v3}, Lcom/squareup/quantity/UnitDisplayDataKt;->getUnitDisplayData(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 341
    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    if-nez v0, :cond_1

    const-string v1, "editItemPath"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->isUnitPricedServiceCreationEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_8

    if-eqz p1, :cond_2

    goto :goto_1

    .line 348
    :cond_2
    new-instance p1, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p1, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 347
    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_4

    .line 351
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 352
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrClearPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_5
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez v2, :cond_6

    const/4 v2, 0x1

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    :goto_2
    if-nez p1, :cond_7

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    :goto_3
    xor-int p1, v2, v0

    if-eqz p1, :cond_8

    .line 355
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->updateServiceScreenData()V

    :cond_8
    :goto_4
    return-void
.end method

.method public variationSkuChanged(Ljava/lang/String;)V
    .locals 1

    const-string v0, "newSku"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 361
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner;->ensureEditingStarted()V

    .line 362
    iget-object v0, p0, Lcom/squareup/ui/items/EditVariationRunner;->editItemVariationState:Lcom/squareup/ui/items/EditVariationState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setSku(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_0
    return-void
.end method
