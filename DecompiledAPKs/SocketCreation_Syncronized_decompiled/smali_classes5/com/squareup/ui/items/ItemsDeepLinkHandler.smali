.class public final Lcom/squareup/ui/items/ItemsDeepLinkHandler;
.super Ljava/lang/Object;
.source "ItemsDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ItemsDeepLinkHandler$CreateItemHistoryFactory;,
        Lcom/squareup/ui/items/ItemsDeepLinkHandler$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemsDeepLinkHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemsDeepLinkHandler.kt\ncom/squareup/ui/items/ItemsDeepLinkHandler\n*L\n1#1,50:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u000b2\u00020\u0001:\u0002\u000b\u000cB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/items/ItemsDeepLinkHandler;",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        "itemsApplet",
        "Lcom/squareup/ui/items/ItemsApplet;",
        "(Lcom/squareup/ui/items/ItemsApplet;)V",
        "getItemsApplet",
        "()Lcom/squareup/ui/items/ItemsApplet;",
        "handleExternal",
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "uri",
        "Landroid/net/Uri;",
        "Companion",
        "CreateItemHistoryFactory",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CREATE:Ljava/lang/String; = "/create"

.field public static final Companion:Lcom/squareup/ui/items/ItemsDeepLinkHandler$Companion;

.field private static final ITEMS:Ljava/lang/String; = "items"


# instance fields
.field private final itemsApplet:Lcom/squareup/ui/items/ItemsApplet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/ItemsDeepLinkHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/ItemsDeepLinkHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/ItemsDeepLinkHandler;->Companion:Lcom/squareup/ui/items/ItemsDeepLinkHandler$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/ItemsApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "itemsApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/ItemsDeepLinkHandler;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    return-void
.end method


# virtual methods
.method public final getItemsApplet()Lcom/squareup/ui/items/ItemsApplet;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsDeepLinkHandler;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    return-object v0
.end method

.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 2

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    goto :goto_1

    .line 23
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsDeepLinkHandler;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto :goto_1

    .line 24
    :cond_3
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    const-string v0, "/create"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/items/ItemsDeepLinkHandler$CreateItemHistoryFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/ItemsDeepLinkHandler$CreateItemHistoryFactory;-><init>(Lcom/squareup/ui/items/ItemsDeepLinkHandler;)V

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto :goto_1

    .line 25
    :cond_4
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    :goto_1
    return-object p1
.end method
