.class public interface abstract Lcom/squareup/ui/items/EditItemGateway;
.super Ljava/lang/Object;
.source "EditItemGateway.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\t\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001:\u0001\u0017J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\"\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005H&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH&J$\u0010\u000c\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\r\u001a\u0004\u0018\u00010\u00052\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0005H&J \u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0010\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0005H&J\u0008\u0010\u0016\u001a\u00020\u0003H&\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditItemGateway;",
        "",
        "startEditDiscount",
        "",
        "id",
        "",
        "startEditItemFlow",
        "itemId",
        "type",
        "Lcom/squareup/api/items/Item$Type;",
        "imageUrl",
        "startEditNewItemFlow",
        "startEditNewItemFlowInCategory",
        "categoryId",
        "categoryName",
        "startEditNewItemFlowWithNameAndAmount",
        "itemAbbreviation",
        "itemName",
        "itemAmount",
        "",
        "startEditNewItemFlowWithSku",
        "sku",
        "startNewDiscount",
        "NoEditItemGateway",
        "edit-item"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract startEditDiscount(Ljava/lang/String;)V
.end method

.method public abstract startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V
.end method

.method public abstract startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V
.end method

.method public abstract startEditNewItemFlowInCategory(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract startEditNewItemFlowWithNameAndAmount(Ljava/lang/String;Ljava/lang/String;J)V
.end method

.method public abstract startEditNewItemFlowWithSku(Ljava/lang/String;)V
.end method

.method public abstract startNewDiscount()V
.end method
