.class public final Lcom/squareup/ui/items/EditItemPhotoScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "EditItemPhotoScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditItemPhotoScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemPhotoScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemPhotoScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoScreen$iFr42AyLR1VGOxo8-Z-r8DupAq4;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemPhotoScreen$iFr42AyLR1VGOxo8-Z-r8DupAq4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditItemPhotoScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemPhotoScreen;
    .locals 1

    .line 46
    const-class v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemScope;

    .line 47
    new-instance v0, Lcom/squareup/ui/items/EditItemPhotoScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemPhotoScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getToolTipText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 32
    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getUpButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method getUpButtonText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 28
    sget v0, Lcom/squareup/registerlib/R$string;->add_photo:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 51
    sget v0, Lcom/squareup/edititem/R$layout;->edit_item_photo_view:I

    return v0
.end method
