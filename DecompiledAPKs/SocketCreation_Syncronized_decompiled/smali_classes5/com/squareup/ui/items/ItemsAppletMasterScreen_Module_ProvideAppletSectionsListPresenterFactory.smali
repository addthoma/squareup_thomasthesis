.class public final Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;
.super Ljava/lang/Object;
.source "ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/applet/AppletSectionsListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->appletProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->flowProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideAppletSectionsListPresenter(Lcom/squareup/ui/items/ItemsApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)Lcom/squareup/applet/AppletSectionsListPresenter;
    .locals 0

    .line 57
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/ui/items/ItemsAppletMasterScreen$Module;->provideAppletSectionsListPresenter(Lcom/squareup/ui/items/ItemsApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)Lcom/squareup/applet/AppletSectionsListPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/applet/AppletSectionsListPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/applet/AppletSectionsListPresenter;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->appletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/ItemsApplet;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Flow;

    iget-object v2, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v4, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->provideAppletSectionsListPresenter(Lcom/squareup/ui/items/ItemsApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)Lcom/squareup/applet/AppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletSectionsListPresenterFactory;->get()Lcom/squareup/applet/AppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method
