.class public final Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "EditModifierSetMainScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appliedLocationCountFetcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final dataProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryDeleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            ">;"
        }
    .end annotation
.end field

.field private final modifierSetEditStateObservableProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->dataProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->modifierSetEditStateObservableProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p7, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p8, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->libraryDeleterProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p9, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p10, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p11, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;"
        }
    .end annotation

    .line 82
    new-instance v12, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditModifierSetScopeRunner;Lcom/squareup/ui/items/ModifierSetEditState;Lcom/squareup/util/Res;Lrx/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lflow/Flow;)Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            "Lcom/squareup/util/Res;",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            "Lflow/Flow;",
            ")",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;"
        }
    .end annotation

    .line 91
    new-instance v12, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditModifierSetScopeRunner;Lcom/squareup/ui/items/ModifierSetEditState;Lcom/squareup/util/Res;Lrx/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lflow/Flow;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;
    .locals 12

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/items/EditModifierSetScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->dataProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/items/ModifierSetEditState;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->modifierSetEditStateObservableProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lrx/Observable;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->libraryDeleterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/items/LibraryDeleter;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lflow/Flow;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditModifierSetScopeRunner;Lcom/squareup/ui/items/ModifierSetEditState;Lcom/squareup/util/Res;Lrx/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lflow/Flow;)Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen_Presenter_Factory;->get()Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
