.class public Lcom/squareup/ui/items/EditDiscountScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "EditDiscountScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditDiscountScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditDiscountScope$Component;,
        Lcom/squareup/ui/items/EditDiscountScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditDiscountScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/items/EditDiscountScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScope;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditDiscountScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScope;->INSTANCE:Lcom/squareup/ui/items/EditDiscountScope;

    .line 44
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScope$g-Y_I4O01sBql8un_ZDaaDRK_Lg;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditDiscountScope$g-Y_I4O01sBql8un_ZDaaDRK_Lg;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditDiscountScope;
    .locals 0

    .line 44
    sget-object p0, Lcom/squareup/ui/items/EditDiscountScope;->INSTANCE:Lcom/squareup/ui/items/EditDiscountScope;

    return-object p0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->RESIZE:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 29
    const-class v0, Lcom/squareup/ui/items/EditDiscountScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountScope$Component;

    .line 30
    invoke-interface {v0}, Lcom/squareup/ui/items/EditDiscountScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditDiscountScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
