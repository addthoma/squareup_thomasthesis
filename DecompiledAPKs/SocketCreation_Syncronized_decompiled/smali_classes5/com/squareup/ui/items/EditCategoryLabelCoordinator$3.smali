.class Lcom/squareup/ui/items/EditCategoryLabelCoordinator$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditCategoryLabelCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->lambda$null$1(Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$3;->this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$3;->this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->access$100(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->isEditingTitle()Z

    move-result p1

    if-nez p1, :cond_0

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$3;->this$0:Lcom/squareup/ui/items/EditCategoryLabelCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->access$300(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V

    :cond_0
    return-void
.end method
