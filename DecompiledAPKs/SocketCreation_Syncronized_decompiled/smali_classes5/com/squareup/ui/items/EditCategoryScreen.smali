.class public final Lcom/squareup/ui/items/EditCategoryScreen;
.super Lcom/squareup/ui/items/InEditCategoryScope;
.source "EditCategoryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditCategoryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditCategoryScreen$Component;,
        Lcom/squareup/ui/items/EditCategoryScreen$Module;,
        Lcom/squareup/ui/items/EditCategoryScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditCategoryScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 373
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$91JEFW9kbTxWCBawBNS55aeJipw;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditCategoryScreen$91JEFW9kbTxWCBawBNS55aeJipw;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditCategoryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditCategoryScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static forNewCategory()Lcom/squareup/ui/items/EditCategoryScreen;
    .locals 2

    .line 58
    new-instance v0, Lcom/squareup/ui/items/EditCategoryScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditCategoryScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditCategoryScreen;
    .locals 1

    .line 374
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 375
    new-instance v0, Lcom/squareup/ui/items/EditCategoryScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditCategoryScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static toEditCategory(Ljava/lang/String;)Lcom/squareup/ui/items/EditCategoryScreen;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/ui/items/EditCategoryScreen;

    const-string v1, "categoryId"

    invoke-static {p0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditCategoryScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 370
    iget-object p2, p0, Lcom/squareup/ui/items/EditCategoryScreen;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 379
    sget v0, Lcom/squareup/itemsapplet/R$layout;->edit_category_view:I

    return v0
.end method
