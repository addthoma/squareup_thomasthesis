.class final Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DetailSearchableListWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflow;->render(Lcom/squareup/ui/items/DetailSearchableListInput;Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "+",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "event",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $behavior:Lcom/squareup/ui/items/DetailSearchableListBehavior;

.field final synthetic $props:Lcom/squareup/ui/items/DetailSearchableListInput;

.field final synthetic $state:Lcom/squareup/ui/items/DetailSearchableListState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListBehavior;Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/ui/items/DetailSearchableListInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$behavior:Lcom/squareup/ui/items/DetailSearchableListBehavior;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$state:Lcom/squareup/ui/items/DetailSearchableListState;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$props:Lcom/squareup/ui/items/DetailSearchableListInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/items/DetailSearchableListScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$Exit;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListScreen$Event$Exit;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$ExitList;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$ExitList;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 142
    :cond_0
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$SetupItemGrid;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListScreen$Event$SetupItemGrid;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditFavGrid;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditFavGrid;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 143
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$behavior:Lcom/squareup/ui/items/DetailSearchableListBehavior;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$state:Lcom/squareup/ui/items/DetailSearchableListState;

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->handlePressingCreateButton(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 144
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$behavior:Lcom/squareup/ui/items/DetailSearchableListBehavior;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$state:Lcom/squareup/ui/items/DetailSearchableListState;

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->handleSelectingObjectFromList(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 145
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$SearchQueryChanged;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 146
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$state:Lcom/squareup/ui/items/DetailSearchableListState;

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    .line 147
    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$SearchQueryChanged;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$SearchQueryChanged;->getNewSearchQuery()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xf7

    const/4 v13, 0x0

    invoke-static/range {v3 .. v13}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object p1

    .line 146
    invoke-virtual {v2, p1}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->copy(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object p1

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 145
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 150
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$BarcodeScanned;

    if-eqz v0, :cond_7

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$props:Lcom/squareup/ui/items/DetailSearchableListInput;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListInput;->getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListDataType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    .line 156
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 153
    :cond_5
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 154
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$BarcodeScanned;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$BarcodeScanned;->getBarcode()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->$behavior:Lcom/squareup/ui/items/DetailSearchableListBehavior;

    invoke-interface {v2}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->getItemTypes()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    invoke-direct {v1, p1, v2}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 153
    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 159
    :cond_7
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ConvertItemButtonPressed;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ConvertItemButtonPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToConvertItemToServiceOnDashboard;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToConvertItemToServiceOnDashboard;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;->invoke(Lcom/squareup/ui/items/DetailSearchableListScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
