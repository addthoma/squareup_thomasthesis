.class public abstract Lcom/squareup/ui/items/InEditModifierSetScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InEditModifierSetScope.java"


# instance fields
.field public final modifierSetId:Ljava/lang/String;

.field private final parent:Lcom/squareup/ui/items/EditModifierSetScope;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/items/InEditModifierSetScope;->modifierSetId:Ljava/lang/String;

    .line 11
    new-instance v0, Lcom/squareup/ui/items/EditModifierSetScope;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/EditModifierSetScope;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/items/InEditModifierSetScope;->parent:Lcom/squareup/ui/items/EditModifierSetScope;

    return-void
.end method


# virtual methods
.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/items/InEditModifierSetScope;->parent:Lcom/squareup/ui/items/EditModifierSetScope;

    return-object v0
.end method
