.class Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;
.super Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;
.source "ModifiersDetailView.java"

# interfaces
.implements Lcom/mobeta/android/dslv/DragSortListView$DragListener;
.implements Lcom/mobeta/android/dslv/DragSortListView$DropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifiersDetailView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ModifiersListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListView<",
        "Lcom/squareup/ui/items/ModifiersDetailView;",
        ">.DetailSearchable",
        "ListAdapter;",
        "Lcom/mobeta/android/dslv/DragSortListView$DragListener;",
        "Lcom/mobeta/android/dslv/DragSortListView$DropListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/ModifiersDetailView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/ModifiersDetailView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;-><init>(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method


# virtual methods
.method protected buildAndBindButtonRow(Landroid/view/View;)Landroid/view/View;
    .locals 6

    if-nez p1, :cond_0

    .line 143
    new-instance p1, Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifiersDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 144
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    iget v0, v0, Lcom/squareup/ui/items/ModifiersDetailView;->gutterHalf:I

    iget-object v3, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    iget v3, v3, Lcom/squareup/ui/items/ModifiersDetailView;->gutterHalf:I

    iget-object v4, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    iget v4, v4, Lcom/squareup/ui/items/ModifiersDetailView;->gutterHalf:I

    iget-object v5, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    iget v5, v5, Lcom/squareup/ui/items/ModifiersDetailView;->gutter:I

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 147
    new-instance v0, Lcom/squareup/marketfont/MarketButton;

    iget-object v3, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    invoke-virtual {v3}, Lcom/squareup/ui/items/ModifiersDetailView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    const/16 v3, 0x11

    .line 148
    invoke-virtual {v0, v3}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    .line 149
    iget-object v3, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    invoke-virtual {v3}, Lcom/squareup/ui/items/ModifiersDetailView;->getPresenter()Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->getButtonText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 150
    new-instance v3, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter$1;-><init>(Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;)V

    invoke-virtual {v0, v3}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-object p1
.end method

.method protected buildAndBindItemRow(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2

    if-nez p1, :cond_0

    .line 104
    sget p1, Lcom/squareup/widgets/pos/R$layout;->items_applet_modifiers_list_row:I

    .line 105
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    :cond_0
    check-cast p1, Lcom/squareup/ui/library/ModifierListRow;

    .line 108
    iget-object p2, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getPosition()I

    move-result p2

    const/4 p3, 0x1

    if-nez p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 109
    :goto_0
    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/library/ModifierListRow;->setHorizontalBorders(ZZ)V

    .line 112
    iget-object p2, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getVariations()I

    move-result p2

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifiersDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    iget-object v1, v1, Lcom/squareup/ui/items/ModifiersDetailView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldDisplayModifierInsteadOfOption()Z

    move-result v1

    if-eqz p2, :cond_5

    if-eq p2, p3, :cond_3

    if-eqz v1, :cond_2

    .line 128
    sget v1, Lcom/squareup/itemsapplet/R$string;->modifier_count_plural:I

    goto :goto_1

    :cond_2
    sget v1, Lcom/squareup/registerlib/R$string;->modifier_options_count_plural:I

    .line 131
    :goto_1
    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 132
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const-string v1, "modifier_count"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 133
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    .line 123
    sget p2, Lcom/squareup/itemsapplet/R$string;->modifier_count_single:I

    .line 124
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_4
    sget p2, Lcom/squareup/registerlib/R$string;->modifier_options_count_single:I

    .line 125
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_5
    if-eqz v1, :cond_6

    .line 118
    sget p2, Lcom/squareup/itemsapplet/R$string;->modifier_count_zero:I

    .line 119
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_6
    sget p2, Lcom/squareup/registerlib/R$string;->modifier_options_count_zero:I

    .line 120
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 136
    :goto_2
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/library/ModifierListRow;->setContent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 137
    iget-object p2, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->backingCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->hasSearchFilter()Z

    move-result p2

    xor-int/2addr p2, p3

    invoke-virtual {p1, p2}, Lcom/squareup/ui/library/ModifierListRow;->setDraggableDisplay(Z)V

    return-object p1
.end method

.method public drag(II)V
    .locals 0

    return-void
.end method

.method public drop(II)V
    .locals 5

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/squareup/ui/items/ModifiersDetailView;->useTemporaryOrdinal:Z

    if-ltz p1, :cond_7

    .line 83
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->getCount()I

    move-result v0

    if-ge p2, v0, :cond_7

    if-gtz p2, :cond_0

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->getStaticTopRowsCount()I

    move-result v2

    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_6

    if-ge v2, p1, :cond_1

    if-lt v2, p2, :cond_2

    :cond_1
    if-le v2, p1, :cond_3

    if-le v2, p2, :cond_3

    .line 88
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->getStaticTopRowsCount()I

    move-result v3

    sub-int v3, v2, v3

    goto :goto_1

    :cond_3
    if-ne v2, p2, :cond_4

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->getStaticTopRowsCount()I

    move-result v3

    sub-int v3, p1, v3

    goto :goto_1

    :cond_4
    if-gt v2, p1, :cond_5

    if-le v2, p2, :cond_5

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->getStaticTopRowsCount()I

    move-result v3

    sub-int v3, v2, v3

    sub-int/2addr v3, v1

    goto :goto_1

    .line 94
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->getStaticTopRowsCount()I

    move-result v3

    sub-int v3, v2, v3

    add-int/2addr v3, v1

    .line 96
    :goto_1
    iget-object v4, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    iget-object v4, v4, Lcom/squareup/ui/items/ModifiersDetailView;->presenter:Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;

    invoke-virtual {v4, v3, v0}, Lcom/squareup/ui/items/ModifiersDetailScreen$Presenter;->positionChanged(II)V

    .line 97
    iget-object v4, p0, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->this$0:Lcom/squareup/ui/items/ModifiersDetailView;

    iget-object v4, v4, Lcom/squareup/ui/items/ModifiersDetailView;->temporaryOrdinalMap:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0, v3}, Landroid/util/SparseIntArray;->put(II)V

    add-int/2addr v0, v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifiersDetailView$ModifiersListAdapter;->notifyDataSetChanged()V

    :cond_7
    :goto_2
    return-void
.end method
