.class Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;
.super Lcom/squareup/cogs/CatalogUpdateTask;
.source "EditModifierSetMainScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->save()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

.field final synthetic val$modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;->this$0:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;->val$modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-direct {p0}, Lcom/squareup/cogs/CatalogUpdateTask;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/squareup/shared/catalog/Catalog$Local;)V
    .locals 9

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168
    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;->val$modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierSet()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    move-result-object v2

    .line 170
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->findModifierItemMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 177
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    .line 178
    iget-object v6, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;->val$modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getItemId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removeAssignedItem(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 179
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 181
    new-instance v6, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    invoke-direct {v6, v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;)V

    .line 183
    invoke-virtual {v6, v5}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    move-result-object v4

    .line 184
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    move-result-object v4

    .line 185
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    if-eqz v6, :cond_0

    .line 186
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_0

    .line 188
    new-instance v5, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    invoke-direct {v5, v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;)V

    const/4 v4, 0x1

    .line 190
    invoke-virtual {v5, v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    move-result-object v4

    .line 191
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    move-result-object v4

    .line 192
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 197
    :cond_2
    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 198
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object v2

    .line 199
    iget-object v3, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;->val$modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-virtual {v3}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getAssignedItemIds()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 200
    new-instance v6, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    iget-object v7, v2, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-direct {v6, v4, v7}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    move-result-object v4

    .line 203
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 206
    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;->this$0:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getModifierOptions()Ljava/util/List;

    move-result-object v3

    .line 207
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_5

    .line 208
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 210
    iget-object v6, v4, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    .line 211
    const-class v7, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 212
    invoke-interface {p1, v7, v6}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    if-eqz v7, :cond_4

    .line 217
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v6

    check-cast v6, Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v6}, Lcom/squareup/api/items/ItemModifierOption;->newBuilder()Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v6

    iget-object v8, v4, Lcom/squareup/api/items/ItemModifierOption$Builder;->name:Ljava/lang/String;

    .line 218
    invoke-virtual {v6, v8}, Lcom/squareup/api/items/ItemModifierOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v6

    iget-object v4, v4, Lcom/squareup/api/items/ItemModifierOption$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 219
    invoke-virtual {v6, v4}, Lcom/squareup/api/items/ItemModifierOption$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v4

    .line 220
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/squareup/api/items/ItemModifierOption$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v4

    .line 221
    invoke-virtual {v4}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v4

    .line 223
    invoke-virtual {v7, v4}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->copy(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    move-result-object v4

    goto :goto_3

    .line 225
    :cond_4
    new-instance v7, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-direct {v7}, Lcom/squareup/api/items/ItemModifierOption$Builder;-><init>()V

    .line 226
    invoke-virtual {v7, v6}, Lcom/squareup/api/items/ItemModifierOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v7

    iget-object v8, v4, Lcom/squareup/api/items/ItemModifierOption$Builder;->name:Ljava/lang/String;

    .line 227
    invoke-virtual {v7, v8}, Lcom/squareup/api/items/ItemModifierOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v7

    iget-object v4, v4, Lcom/squareup/api/items/ItemModifierOption$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 228
    invoke-virtual {v7, v4}, Lcom/squareup/api/items/ItemModifierOption$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v4

    .line 229
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/squareup/api/items/ItemModifierOption$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v4

    .line 230
    invoke-virtual {v4, v2}, Lcom/squareup/api/items/ItemModifierOption$Builder;->modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v4

    .line 231
    invoke-virtual {v4}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v4

    .line 232
    sget-object v7, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v7, v6, v4}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 235
    :goto_3
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 238
    :cond_5
    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;->val$modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getRemovedOptionIds()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 239
    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 240
    invoke-interface {p1, v4, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    if-eqz v3, :cond_6

    .line 242
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 246
    :cond_7
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    return-void
.end method
