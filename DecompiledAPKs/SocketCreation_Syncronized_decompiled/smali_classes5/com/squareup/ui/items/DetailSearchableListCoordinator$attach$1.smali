.class final Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;
.super Ljava/lang/Object;
.source "DetailSearchableListCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Ljava/lang/String;",
        "+",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 72
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreen;

    .line 116
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$BarcodeScanned;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$BarcodeScanned;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
