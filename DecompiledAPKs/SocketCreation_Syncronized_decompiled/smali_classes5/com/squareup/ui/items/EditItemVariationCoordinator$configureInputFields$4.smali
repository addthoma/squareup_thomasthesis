.class final Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;
.super Ljava/lang/Object;
.source "EditItemVariationCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/util/Set<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0000\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $variationId:Ljava/lang/String;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->$variationId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->call(Ljava/util/Set;)V

    return-void
.end method

.method public final call(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->$variationId:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 203
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getSkuEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_0

    .line 205
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getSkuEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;

    move-result-object p1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 205
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    :goto_0
    return-void
.end method
