.class public Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;
.super Ljava/lang/Object;
.source "EditItemScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StaffItemState"
.end annotation


# instance fields
.field public final selectedStaffIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final staffInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/appointmentsapi/StaffInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/appointmentsapi/StaffInfo;",
            ">;)V"
        }
    .end annotation

    .line 1130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1131
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;->selectedStaffIds:Ljava/util/List;

    .line 1132
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;->staffInfos:Ljava/util/List;

    return-void
.end method
