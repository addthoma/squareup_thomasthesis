.class public Lcom/squareup/ui/items/EditModifierSetMainView;
.super Lcom/squareup/widgets/PairLayout;
.source "EditModifierSetMainView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/items/BaseEditObjectView;


# instance fields
.field private adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

.field private layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private overlay:Landroid/widget/ImageView;

.field presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field settings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditModifierSetMainScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditModifierSetMainScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Component;->inject(Lcom/squareup/ui/items/EditModifierSetMainView;)V

    return-void
.end method


# virtual methods
.method public getAdapter()Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    return-object v0
.end method

.method public hideSpinner()V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$EditModifierSetMainView()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->onProgressHidden()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->showConfirmDiscardDialogOrFinish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 67
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .line 42
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 44
    sget v0, Lcom/squareup/itemsapplet/R$id;->modifier_card_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 45
    sget v0, Lcom/squareup/itemsapplet/R$id;->recycler:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditModifierSetMainView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 47
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 50
    new-instance v0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    iget-object v3, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    iget-object v4, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v5, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->res:Lcom/squareup/util/Res;

    iget-object v6, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v7, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;-><init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/text/Formatter;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 54
    sget v0, Lcom/squareup/itemsapplet/R$id;->overlay:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditModifierSetMainView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->overlay:Landroid/widget/ImageView;

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/ui/items/RecyclerViewDragController;

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->overlay:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    sget v4, Lcom/squareup/itemsapplet/R$id;->modifier_option_drag_handle:I

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/squareup/ui/items/RecyclerViewDragController;-><init>(Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/ImageView;Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;I)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainView$yh_RvHy3Wv3C0w7rWLwMN97ICJ0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainView$yh_RvHy3Wv3C0w7rWLwMN97ICJ0;-><init>(Lcom/squareup/ui/items/EditModifierSetMainView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public showContent()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getModifierOptions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->setContentsFromSource(Ljava/util/List;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->adapter:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter;->notifyDataSetChanged()V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    return-void
.end method

.method public showMultiUnitContent()V
    .locals 5

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 93
    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    const/4 v4, 0x0

    .line 92
    invoke-virtual {v0, v1, v4, v2, v3}, Landroidx/recyclerview/widget/RecyclerView;->setPadding(IIII)V

    return-void
.end method
