.class final Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$3;
.super Ljava/lang/Object;
.source "EditItemVariationCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemVariationCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$3;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getSkuEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;

    move-result-object v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
