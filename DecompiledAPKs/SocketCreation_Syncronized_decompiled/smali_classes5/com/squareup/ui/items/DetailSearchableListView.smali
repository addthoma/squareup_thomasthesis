.class public abstract Lcom/squareup/ui/items/DetailSearchableListView;
.super Landroid/widget/LinearLayout;
.source "DetailSearchableListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;,
        Lcom/squareup/ui/items/DetailSearchableListView$TopRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/ui/items/DetailSearchableListView;",
        ">",
        "Landroid/widget/LinearLayout;"
    }
.end annotation


# static fields
.field private static final SEARCH_DELAY_MS:J = 0x64L


# instance fields
.field protected adapter:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>.DetailSearchable",
            "ListAdapter;"
        }
    .end annotation
.end field

.field currencyCode:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field durationFormatter:Lcom/squareup/text/DurationFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected final gutter:I

.field protected final gutterHalf:I

.field itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected listView:Landroid/widget/ListView;

.field private listWrapper:Landroid/view/View;

.field private nullStateSubtitle:Lcom/squareup/widgets/MessageView;

.field private nullStateTitle:Landroid/widget/TextView;

.field percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected searchBar:Lcom/squareup/ui/XableEditText;

.field private final searchRunnable:Ljava/lang/Runnable;

.field public temporaryOrdinalMap:Landroid/util/SparseIntArray;

.field tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public useTemporaryOrdinal:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->inject()V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->gutterHalf:I

    .line 93
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->gutter:I

    .line 95
    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$6VaJBvLPQNbYQBaAZ1o9vfPXSTs;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$6VaJBvLPQNbYQBaAZ1o9vfPXSTs;-><init>(Lcom/squareup/ui/items/DetailSearchableListView;)V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->searchRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/DetailSearchableListView;)Ljava/lang/Runnable;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->searchRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/DetailSearchableListView;)Landroid/widget/TextView;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->nullStateTitle:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/DetailSearchableListView;)Lcom/squareup/widgets/MessageView;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->nullStateSubtitle:Lcom/squareup/widgets/MessageView;

    return-object p0
.end method


# virtual methods
.method bindItemRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 10

    .line 180
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListView;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListView;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListView;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const/4 p2, 0x0

    .line 181
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListView;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 182
    invoke-virtual {p2}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v8

    iget-object v9, p0, Lcom/squareup/ui/items/DetailSearchableListView;->res:Lcom/squareup/util/Res;

    move-object v0, p1

    .line 180
    invoke-virtual/range {v0 .. v9}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCatalogItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;)V

    return-void
.end method

.method protected buildAdapter(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ")",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "TT;>.DetailSearchable",
            "ListAdapter;"
        }
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;-><init>(Lcom/squareup/ui/items/DetailSearchableListView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-object v0
.end method

.method abstract getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
            "TT;>;"
        }
    .end annotation
.end method

.method hideSpinner()V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method protected abstract inject()V
.end method

.method public synthetic lambda$new$0$DetailSearchableListView()V
    .locals 2

    .line 95
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onTextSearched(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$DetailSearchableListView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListView;->adapter:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    .line 130
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->getStaticTopRowsCount()I

    move-result p2

    sub-int/2addr p3, p2

    .line 129
    invoke-virtual {p1, p3}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onRowClicked(I)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$DetailSearchableListView()V
    .locals 1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onProgressHidden()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 166
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->self()Lcom/squareup/ui/items/DetailSearchableListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->dropView(Ljava/lang/Object;)V

    .line 167
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 112
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 113
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 114
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 115
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_list_wrapper:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listWrapper:Landroid/view/View;

    .line 117
    sget v0, Lcom/squareup/itemsapplet/R$id;->null_state_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->nullStateTitle:Landroid/widget/TextView;

    .line 118
    sget v0, Lcom/squareup/itemsapplet/R$id;->null_state_subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->nullStateSubtitle:Lcom/squareup/widgets/MessageView;

    .line 120
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listView:Landroid/widget/ListView;

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->usesDraggableListView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 123
    sget v0, Lcom/squareup/itemsapplet/R$id;->draggable_detail_list_view:I

    .line 124
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 125
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listView:Landroid/widget/ListView;

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listView:Landroid/widget/ListView;

    sget v1, Lcom/squareup/librarylist/R$id;->no_search_results:I

    invoke-virtual {p0, v1}, Lcom/squareup/ui/items/DetailSearchableListView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$AXowwyJ0nqrdsCC7ynGGJj8ib1Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$AXowwyJ0nqrdsCC7ynGGJj8ib1Q;-><init>(Lcom/squareup/ui/items/DetailSearchableListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->searchBar:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListView$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$9zZwJm9RlQV-aUv3XaFgp2-OEbA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListView$9zZwJm9RlQV-aUv3XaFgp2-OEbA;-><init>(Lcom/squareup/ui/items/DetailSearchableListView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    .line 144
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->self()Lcom/squareup/ui/items/DetailSearchableListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->takeView(Ljava/lang/Object;)V

    .line 146
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->rowsHaveThumbnails()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listView:Landroid/widget/ListView;

    iget v1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->gutter:I

    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lcom/squareup/ui/items/DetailSearchableListView;->gutter:I

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    :cond_1
    const/4 v0, 0x0

    .line 150
    iput-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->useTemporaryOrdinal:Z

    .line 151
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->temporaryOrdinalMap:Landroid/util/SparseIntArray;

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 171
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 175
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onTextSearched(Ljava/lang/String;)V

    return-void
.end method

.method protected final self()Lcom/squareup/ui/items/DetailSearchableListView;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    return-object p0
.end method

.method setCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->adapter:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    if-nez v0, :cond_0

    .line 195
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListView;->buildAdapter(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->adapter:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    .line 196
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->adapter:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 198
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->setCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    :goto_0
    return-void
.end method

.method protected setDetailListDivider(I)V
    .locals 2

    .line 161
    sget v0, Lcom/squareup/itemsapplet/R$id;->detail_list_view:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/DetailSearchableListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 162
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method protected setNullState(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->nullStateTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView;->nullStateSubtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showList()V
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListView;->listWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
