.class public abstract Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;
.super Ljava/lang/Object;
.source "EditVariationRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditVariationRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EditVariationScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;,
        Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0018\u0019B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0006R\u001a\u0010\u0008\u001a\u0004\u0018\u00010\tX\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u0012\u0010\u0016\u001a\u00020\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0011\u0082\u0001\u0002\u001a\u001b\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;",
        "",
        "()V",
        "hasInclusiveTaxesApplied",
        "",
        "getHasInclusiveTaxesApplied",
        "()Z",
        "isNewVariation",
        "measurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "getMeasurementUnit",
        "()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "setMeasurementUnit",
        "(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V",
        "name",
        "",
        "getName",
        "()Ljava/lang/String;",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "getPrice",
        "()Lcom/squareup/protos/common/Money;",
        "variationId",
        "getVariationId",
        "EditItemVariationScreenData",
        "EditServiceVariationScreenData",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 666
    invoke-direct {p0}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getHasInclusiveTaxesApplied()Z
.end method

.method public abstract getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPrice()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getVariationId()Ljava/lang/String;
.end method

.method public abstract isNewVariation()Z
.end method

.method public abstract setMeasurementUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
.end method
