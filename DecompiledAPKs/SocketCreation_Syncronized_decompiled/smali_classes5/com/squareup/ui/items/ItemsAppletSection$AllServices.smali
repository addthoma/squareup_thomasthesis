.class public final Lcom/squareup/ui/items/ItemsAppletSection$AllServices;
.super Lcom/squareup/ui/items/ItemsAppletSection;
.source "ItemsAppletSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AllServices"
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 44
    sget-object v0, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;->Companion:Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen$Companion;->getALL_SERVICES_INSTANCE()Lcom/squareup/ui/items/BootstrapDetailSearchableListScreen;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/items/ItemsAppletSection$AllServices$1;

    invoke-direct {v3, p1}, Lcom/squareup/ui/items/ItemsAppletSection$AllServices$1;-><init>(Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    sget v4, Lcom/squareup/itemsapplet/R$string;->items_applet_all_services_title:I

    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_ALL_SERVICES:Lcom/squareup/analytics/RegisterTapName;

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/items/ItemsAppletSection;-><init>(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/SectionAccess;ILcom/squareup/analytics/RegisterTapName;Lcom/squareup/ui/items/ItemsAppletSection$1;)V

    return-void
.end method
