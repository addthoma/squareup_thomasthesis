.class public Lcom/squareup/ui/items/NewCategoryNameScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "NewCategoryNameScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/NewCategoryNameScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/NewCategoryNameScreen$Component;,
        Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/NewCategoryNameScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 112
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$NewCategoryNameScreen$ycqFqpgXDVFR3fIHrbSU8ttyiL4;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$NewCategoryNameScreen$ycqFqpgXDVFR3fIHrbSU8ttyiL4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/NewCategoryNameScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/NewCategoryNameScreen;
    .locals 1

    .line 113
    const-class v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemScope;

    .line 114
    new-instance v0, Lcom/squareup/ui/items/NewCategoryNameScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/NewCategoryNameScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method protected getSecondaryButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getUpButtonText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 44
    sget v0, Lcom/squareup/edititem/R$string;->category_create:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 118
    sget v0, Lcom/squareup/edititem/R$layout;->edit_item_new_category_name_view:I

    return v0
.end method
