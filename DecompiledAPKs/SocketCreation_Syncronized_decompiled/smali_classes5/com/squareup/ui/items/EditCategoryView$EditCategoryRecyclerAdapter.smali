.class public Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "EditCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EditCategoryRecyclerAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final DELETE_BUTTON:I = 0x2

.field private static final ITEM_ROW:I = 0x1

.field private static final STATIC_TOP_ROWS:I = 0x1

.field private static final STATIC_TOP_ROW_CONTENT:I


# instance fields
.field private final categoryNameChangeListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/ui/items/EditCategoryView$CategoryNameChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final showBanner:Z

.field private final showDelete:Z

.field final synthetic this$0:Lcom/squareup/ui/items/EditCategoryView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditCategoryView;ZZ)V
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 106
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->categoryNameChangeListeners:Ljava/util/Set;

    .line 198
    iput-boolean p3, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->showBanner:Z

    .line 199
    iput-boolean p2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->showDelete:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)Ljava/util/Set;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->categoryNameChangeListeners:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)Z
    .locals 0

    .line 97
    iget-boolean p0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->showBanner:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->setUpTile(Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->updateTile(Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)I
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->getCursorCount()I

    move-result p0

    return p0
.end method

.method private getCursorCount()I
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    :goto_0
    return v0
.end method

.method private setUpImageTile(Lcom/squareup/register/widgets/EditCatalogObjectLabel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 306
    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p1, p3}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setName(Ljava/lang/String;)V

    .line 308
    invoke-virtual {p1, p4}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 309
    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    .line 310
    new-instance p2, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$2;-><init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setUpTextTile(Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 294
    invoke-virtual {p1, p3, p4}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p3, 0x0

    .line 295
    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/TextTile;->setVisibility(I)V

    .line 296
    new-instance p3, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$1;

    invoke-direct {p3, p0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$1;-><init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)V

    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/TextTile;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 p1, 0x8

    .line 301
    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setVisibility(I)V

    return-void
.end method

.method private setUpTile(Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V
    .locals 2

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isTextTileMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryNameOrDefault()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    .line 285
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getLabelColorOrDefault()Ljava/lang/String;

    move-result-object v1

    .line 284
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->setUpTextTile(Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getAbbreviationOrDefault()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    .line 288
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryNameOrDefault()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getLabelColorOrDefault()Ljava/lang/String;

    move-result-object v1

    .line 287
    invoke-direct {p0, p2, p1, v0, v1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->setUpImageTile(Lcom/squareup/register/widgets/EditCatalogObjectLabel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private updateTile(Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isTextTileMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget-object p2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object p2, p2, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryNameOrDefault()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/TextTile;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 277
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getCategoryNameOrDefault()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setName(Ljava/lang/String;)V

    .line 278
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->getAbbreviationOrDefault()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public changeCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 204
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getItemCount()I
    .locals 2

    .line 267
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->getCursorCount()I

    move-result v0

    .line 268
    iget-boolean v1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->showDelete:Z

    add-int/lit8 v0, v0, 0x1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 247
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->getCursorCount()I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x2

    return p1

    :cond_1
    return v1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;I)V
    .locals 2

    .line 227
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->getViewType()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p2, 0x2

    if-eq v0, p2, :cond_0

    goto :goto_0

    .line 237
    :cond_0
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->showDelete:Z

    if-eqz p2, :cond_3

    .line 238
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->bindDelete()V

    goto :goto_0

    :cond_1
    sub-int/2addr p2, v1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isTextTileMode()Z

    move-result v1

    invoke-virtual {p1, v0, p2, v1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->bindItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;IZ)V

    goto :goto_0

    .line 229
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->bindStaticTopRowContent()V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 97
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;
    .locals 3

    .line 208
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    const/4 v2, 0x1

    if-eq p2, v2, :cond_1

    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    .line 218
    sget v2, Lcom/squareup/itemsapplet/R$layout;->items_applet_delete_button_row:I

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 221
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "viewType not supported"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 215
    :cond_1
    sget v2, Lcom/squareup/itemsapplet/R$layout;->category_assignment_item_row:I

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 212
    :cond_2
    sget v2, Lcom/squareup/itemsapplet/R$layout;->edit_category_static_top_content:I

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 223
    :goto_0
    new-instance v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;-><init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;Landroid/view/View;I)V

    return-object v0
.end method

.method public bridge synthetic onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->onViewRecycled(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;)V

    return-void
.end method

.method public onViewRecycled(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;)V
    .locals 2

    .line 255
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    .line 256
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->getViewType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 257
    invoke-static {p1}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->access$500(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditCategoryView$CategoryNameChangeListener;

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->categoryNameChangeListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
