.class final Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;
.super Ljava/lang/Object;
.source "DetailSearchableListUnitBehavior.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->apply(Lkotlin/Unit;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListUnitBehavior.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListUnitBehavior.kt\ncom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,102:1\n950#2:103\n1360#2:104\n1429#2,3:105\n704#2:108\n777#2:109\n1642#2,2:110\n778#2:112\n*E\n*S KotlinDebug\n*F\n+ 1 DetailSearchableListUnitBehavior.kt\ncom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1\n*L\n69#1:103\n73#1:104\n73#1,3:105\n76#1:108\n76#1:109\n76#1,2:110\n76#1:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u00020\u00012(\u0010\u0002\u001a$\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "unitList",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "kotlin.jvm.PlatformType",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/List;)Lcom/squareup/ui/items/DetailSearchableListData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListData;"
        }
    .end annotation

    const-string/jumbo v0, "unitList"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 69
    check-cast p1, Ljava/lang/Iterable;

    .line 103
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1$$special$$inlined$sortedBy$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1$$special$$inlined$sortedBy$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;)V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 105
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 106
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 74
    new-instance v3, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    const-string v4, "it"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v2}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 107
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 108
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 109
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    .line 78
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v5

    const-string v6, "it.measurementUnit.measurementUnit"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;

    iget-object v7, v7, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$res:Lcom/squareup/util/Res;

    invoke-static {v5, v7}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v3}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v3

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;

    iget-object v5, v5, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$res:Lcom/squareup/util/Res;

    invoke-static {v3, v5}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 80
    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;

    iget-object v4, v4, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$searchTerm:Ljava/lang/String;

    move-object v5, v4

    check-cast v5, Ljava/lang/CharSequence;

    const-string v4, " "

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 110
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-array v7, v7, [Ljava/lang/Object;

    .line 82
    sget-object v8, Lkotlin/text/Regex;->Companion:Lkotlin/text/Regex$Companion;

    invoke-virtual {v8, v5}, Lkotlin/text/Regex$Companion;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v6

    array-length v5, v7

    invoke-static {v7, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v5

    const-string v7, ".*(\\s|^|-|\\()%s.*"

    invoke-static {v7, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "java.lang.String.format(this, *args)"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget-object v7, Lkotlin/text/RegexOption;->IGNORE_CASE:Lkotlin/text/RegexOption;

    new-instance v8, Lkotlin/text/Regex;

    invoke-direct {v8, v5, v7}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;Lkotlin/text/RegexOption;)V

    .line 84
    move-object v5, v3

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v8, v5}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_2

    :cond_3
    const/4 v6, 0x1

    :goto_2
    if-eqz v6, :cond_1

    .line 89
    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 112
    :cond_4
    check-cast p1, Ljava/util/List;

    .line 91
    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    .line 67
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListData;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/items/DetailSearchableListData;-><init>(ILcom/squareup/cycler/DataSource;)V

    return-object v1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;->apply(Ljava/util/List;)Lcom/squareup/ui/items/DetailSearchableListData;

    move-result-object p1

    return-object p1
.end method
