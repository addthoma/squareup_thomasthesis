.class public interface abstract Lcom/squareup/ui/items/EditItemMainScreen$Component;
.super Ljava/lang/Object;
.source "EditItemMainScreen.java"

# interfaces
.implements Lcom/squareup/ui/items/AppliedLocationsBanner$Component;
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/items/EditItemMainScreen$Module;,
        Lcom/squareup/ui/items/EditItemMainScreen$AssignUnitToVariationWorkflowModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemMainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract duplicateSkuValidator()Lcom/squareup/ui/items/DuplicateSkuValidator;
.end method

.method public abstract inject(Lcom/squareup/ui/items/EditItemEditDetailsView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/items/EditItemMainView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V
.end method

.method public abstract inject(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/items/EditItemMainViewStaticTopView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V
.end method
