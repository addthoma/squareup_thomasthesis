.class Lcom/squareup/ui/items/EditItemLabelView$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditItemLabelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemLabelView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemLabelView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemLabelView;)V
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView$3;->this$0:Lcom/squareup/ui/items/EditItemLabelView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView$3;->this$0:Lcom/squareup/ui/items/EditItemLabelView;

    invoke-static {p1}, Lcom/squareup/ui/items/EditItemLabelView;->access$000(Lcom/squareup/ui/items/EditItemLabelView;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->isEditingTitle()Z

    move-result p1

    if-nez p1, :cond_0

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView$3;->this$0:Lcom/squareup/ui/items/EditItemLabelView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->editLabelClicked()V

    :cond_0
    return-void
.end method
