.class public Lcom/squareup/ui/items/ItemsApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "ItemsApplet.java"


# static fields
.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "ITEMS"


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final sections:Lcom/squareup/ui/items/ItemsAppletSectionList;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/items/ItemsAppletSectionList;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/items/ItemsAppletSectionList;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/items/ItemsApplet;->sections:Lcom/squareup/ui/items/ItemsAppletSectionList;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/items/ItemsApplet;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    const-string v0, "items"

    return-object v0
.end method

.method public getAppletId()Ljava/lang/Integer;
    .locals 1

    .line 52
    sget v0, Lcom/squareup/registerlib/R$id;->applets_drawer_items_applet:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 60
    sget-object p1, Lcom/squareup/ui/items/ItemsAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/items/ItemsAppletMasterScreen;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getInitialDetailScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsApplet;->sections:Lcom/squareup/ui/items/ItemsAppletSectionList;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ItemsAppletSectionList;->getLastSelectedSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getInitialDetailScreen()Lflow/path/Path;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemsApplet;->getInitialDetailScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "ITEMS"

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 64
    sget-object v0, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method getSections()Lcom/squareup/ui/items/ItemsAppletSectionList;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsApplet;->sections:Lcom/squareup/ui/items/ItemsAppletSectionList;

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsApplet;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ItemsApplet;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 46
    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->hasExtraServiceOptions()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/registerlib/R$string;->titlecase_items_appointments:I

    .line 47
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/registerlib/R$string;->titlecase_items:I

    .line 48
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
