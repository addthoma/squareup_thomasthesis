.class public final Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;
.super Lcom/squareup/ui/items/DetailSearchableListRow;
.source "DetailSearchableListRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FooterRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        "screen",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "missingResults",
        "",
        "(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Z)V",
        "getMissingResults",
        "()Z",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreen",
        "()Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final missingResults:Z

.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lcom/squareup/ui/items/DetailSearchableListScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Z)V
    .locals 2

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "footer-row"

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/ui/items/DetailSearchableListRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->res:Lcom/squareup/util/Res;

    iput-boolean p3, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;ZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Z)Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/util/Res;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    return v0
.end method

.method public final copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Z)Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    iget-boolean p1, p1, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMissingResults()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    return v0
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FooterRow(screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", missingResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;->missingResults:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
