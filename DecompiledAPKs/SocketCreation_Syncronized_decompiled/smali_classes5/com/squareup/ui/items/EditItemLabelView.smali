.class public Lcom/squareup/ui/items/EditItemLabelView;
.super Lcom/squareup/widgets/PairLayout;
.source "EditItemLabelView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/picasso/Target;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private choosePhoto:Landroid/widget/Button;

.field private colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

.field private deleteButton:Lcom/squareup/orderentry/DeleteButton;

.field private helpText:Landroid/view/View;

.field private isTextTile:Z

.field private itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

.field private itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

.field private itemTextTile:Lcom/squareup/orderentry/TextTile;

.field private labelHeight:I

.field private labelWidth:I

.field private marinActionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private photoLabel:Landroid/view/View;

.field private photoView:Landroid/view/View;

.field presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private takePhoto:Landroid/widget/Button;

.field toastFactory:Lcom/squareup/util/ToastFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemLabelScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemLabelScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemLabelScreen$Component;->inject(Lcom/squareup/ui/items/EditItemLabelView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditItemLabelView;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditItemLabelView;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->stopEditingLabel()V

    return-void
.end method

.method private colorChanged(Ljava/lang/String;)V
    .locals 1

    .line 183
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->stopEditingLabel()V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/TextTile;->setColor(Ljava/lang/String;)V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->colorChanged(Ljava/lang/String;)V

    .line 191
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->hasItemPhoto()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 192
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->removeItemPhotoAndShowColorAndAbbreviation()V

    :cond_1
    return-void
.end method

.method private hasItemPhoto()Z
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$hmdP6x-ja2iJK9CZkkdsvn1GPmw(Lcom/squareup/ui/items/EditItemLabelView;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemLabelView;->colorChanged(Ljava/lang/String;)V

    return-void
.end method

.method private removeItemPhotoAndShowColorAndAbbreviation()V
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/photo/ItemPhoto;->cancel(Lcom/squareup/picasso/Target;)V

    const/4 v0, 0x0

    .line 260
    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->deleteBitmap()V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->clearItemBitmap()V

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->enableAbbreviationEditing()V

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->deleteButton:Lcom/squareup/orderentry/DeleteButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/DeleteButton;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getLabelColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/PickColorGrid;->setSelectedColor(Ljava/lang/String;)V

    return-void
.end method

.method private setShowingTextTile()V
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    if-eqz v0, :cond_0

    .line 293
    iget-boolean v1, p0, Lcom/squareup/ui/items/EditItemLabelView;->isTextTile:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->helpText:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 296
    iget-boolean v1, p0, Lcom/squareup/ui/items/EditItemLabelView;->isTextTile:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 300
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->isTextTile:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->deleteButton:Lcom/squareup/orderentry/DeleteButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/DeleteButton;->setVisibility(I)V

    .line 302
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iget-boolean v1, p0, Lcom/squareup/ui/items/EditItemLabelView;->isTextTile:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private stopEditingLabel()V
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getAbbreviationText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 250
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->disableAbbreviationEditing()V

    return-void
.end method

.method private static within(Landroid/view/View;II)Z
    .locals 3

    .line 282
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 283
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 284
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    .line 285
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result p0

    if-ge v0, v1, :cond_0

    if-ge v2, p0, :cond_0

    if-lt p1, v0, :cond_0

    if-ge p1, v1, :cond_0

    if-lt p2, v2, :cond_0

    if-ge p2, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->marinActionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getEditItemLabel()Lcom/squareup/register/widgets/EditCatalogObjectLabel;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$EditItemLabelView(Landroid/view/View;)V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->removeItemPhotoAndShowColorAndAbbreviation()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 152
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->stopEditingLabel()V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 210
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/edititem/R$string;->edit_item_image_load_failed:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    .line 211
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showItemBitmapError()V

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 0

    .line 199
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->hasItemPhoto()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 200
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setItemBitmap(Landroid/graphics/Bitmap;)V

    .line 201
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showImage()V

    .line 202
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView;->deleteButton:Lcom/squareup/orderentry/DeleteButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/DeleteButton;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->dropView(Lcom/squareup/ui/items/EditItemLabelView;)V

    .line 147
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 73
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 75
    invoke-virtual {p0, p0}, Lcom/squareup/ui/items/EditItemLabelView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 77
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->marinActionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 78
    sget v0, Lcom/squareup/edititem/R$id;->color_grid:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/PickColorGrid;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    .line 79
    sget v0, Lcom/squareup/edititem/R$id;->edit_label_image_tile:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    .line 80
    sget v0, Lcom/squareup/edititem/R$id;->edit_label_text_tile:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/TextTile;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    .line 82
    sget v0, Lcom/squareup/edititem/R$id;->choose_photo:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->choosePhoto:Landroid/widget/Button;

    .line 83
    sget v0, Lcom/squareup/edititem/R$id;->take_photo:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->takePhoto:Landroid/widget/Button;

    .line 84
    sget v0, Lcom/squareup/edititem/R$id;->delete_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/DeleteButton;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->deleteButton:Lcom/squareup/orderentry/DeleteButton;

    .line 85
    sget v0, Lcom/squareup/edititem/R$id;->photo_label_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->photoLabel:Landroid/view/View;

    .line 86
    sget v0, Lcom/squareup/edititem/R$id;->image_tile_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->helpText:Landroid/view/View;

    .line 87
    sget v0, Lcom/squareup/edititem/R$id;->photo_action_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->photoView:Landroid/view/View;

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->choosePhoto:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/items/EditItemLabelView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemLabelView$1;-><init>(Lcom/squareup/ui/items/EditItemLabelView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->takePhoto:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/items/EditItemLabelView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemLabelView$2;-><init>(Lcom/squareup/ui/items/EditItemLabelView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->deleteButton:Lcom/squareup/orderentry/DeleteButton;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemLabelView$KJMR4S6P9WO_RkFv5-fvxGNf4gc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemLabelView$KJMR4S6P9WO_RkFv5-fvxGNf4gc;-><init>(Lcom/squareup/ui/items/EditItemLabelView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/DeleteButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 107
    sget v1, Lcom/squareup/registerlib/R$dimen;->edit_entry_label_width:I

    .line 108
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/items/EditItemLabelView;->labelWidth:I

    .line 109
    sget v1, Lcom/squareup/registerlib/R$dimen;->edit_entry_label_height:I

    .line 110
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->labelHeight:I

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/PickColorGrid;->addColorsForCatalogObjects()V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemLabelView$hmdP6x-ja2iJK9CZkkdsvn1GPmw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemLabelView$hmdP6x-ja2iJK9CZkkdsvn1GPmw;-><init>(Lcom/squareup/ui/items/EditItemLabelView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/PickColorGrid;->setOnColorChangeListener(Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    new-instance v1, Lcom/squareup/ui/items/EditItemLabelView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemLabelView$3;-><init>(Lcom/squareup/ui/items/EditItemLabelView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    new-instance v1, Lcom/squareup/ui/items/EditItemLabelView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemLabelView$4;-><init>(Lcom/squareup/ui/items/EditItemLabelView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    new-instance v1, Lcom/squareup/ui/items/EditItemLabelView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemLabelView$5;-><init>(Lcom/squareup/ui/items/EditItemLabelView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemLabelPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .line 215
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    float-to-int p1, p1

    .line 216
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 219
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    .line 220
    invoke-virtual {p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->isEditingTitle()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    .line 221
    invoke-static {p2, p1, v0}, Lcom/squareup/ui/items/EditItemLabelView;->within(Landroid/view/View;II)Z

    move-result p1

    if-nez p1, :cond_0

    .line 222
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->stopEditingLabel()V

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Z)V
    .locals 0

    .line 158
    iput-boolean p5, p0, Lcom/squareup/ui/items/EditItemLabelView;->isTextTile:Z

    if-eqz p5, :cond_0

    .line 160
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    invoke-virtual {p2, p3, p1}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/PickColorGrid;->setSelectedColor(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_0
    iget-object p5, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p5, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 164
    iget-object p5, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p5, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    .line 165
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p2, p3}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setName(Ljava/lang/String;)V

    .line 167
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 169
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->hasItemPhoto()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    iget p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->labelWidth:I

    iget p3, p0, Lcom/squareup/ui/items/EditItemLabelView;->labelHeight:I

    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-virtual {p1, p2, p0}, Lcom/squareup/ui/photo/ItemPhoto;->into(ILcom/squareup/picasso/Target;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView;->deleteButton:Lcom/squareup/orderentry/DeleteButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/DeleteButton;->setVisibility(I)V

    goto :goto_0

    .line 173
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    .line 174
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemLabelView;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/PickColorGrid;->setSelectedColor(Ljava/lang/String;)V

    .line 175
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView;->deleteButton:Lcom/squareup/orderentry/DeleteButton;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/DeleteButton;->setVisibility(I)V

    .line 179
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemLabelView;->setShowingTextTile()V

    return-void
.end method

.method setPhotoSectionVisibility(Z)V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->photoLabel:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->photoView:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method startEditingLabel()V
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->getLabelColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/PickColorGrid;->setSelectedColor(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->enableAbbreviationEditing()V

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showSoftInput()V

    return-void
.end method
