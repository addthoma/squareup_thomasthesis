.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "AssignUnitToVariationViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAssignUnitToVariationViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AssignUnitToVariationViewFactory.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationViewFactory\n+ 2 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,20:1\n37#2,2:21\n37#2,2:23\n*E\n*S KotlinDebug\n*F\n+ 1 AssignUnitToVariationViewFactory.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationViewFactory\n*L\n17#1,2:21\n18#1,2:23\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "editUnitViewBindings",
        "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
        "assignUnitViewBindings",
        "Lcom/squareup/ui/items/unit/AssignUnitViewBindings;",
        "(Lcom/squareup/items/unit/ui/EditUnitViewBindings;Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)V",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/items/unit/ui/EditUnitViewBindings;Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editUnitViewBindings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignUnitViewBindings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lkotlin/jvm/internal/SpreadBuilder;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lkotlin/jvm/internal/SpreadBuilder;-><init>(I)V

    .line 17
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;->getBindings()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 22
    invoke-interface {p2, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    const-string v2, "null cannot be cast to non-null type kotlin.Array<T>"

    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lkotlin/jvm/internal/SpreadBuilder;->addSpread(Ljava/lang/Object;)V

    .line 18
    invoke-virtual {p1}, Lcom/squareup/items/unit/ui/EditUnitViewBindings;->getBindings()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    new-array p2, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 24
    invoke-interface {p1, p2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lkotlin/jvm/internal/SpreadBuilder;->addSpread(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lkotlin/jvm/internal/SpreadBuilder;->size()I

    move-result p1

    new-array p1, p1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    invoke-virtual {v0, p1}, Lkotlin/jvm/internal/SpreadBuilder;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void

    .line 24
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 22
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
