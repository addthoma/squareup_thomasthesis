.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;
.super Ljava/lang/Object;
.source "AssignUnitToVariationWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final defaultStandardUnitsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/DefaultStandardUnits;",
            ">;"
        }
    .end annotation
.end field

.field private final editUnitWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/EditUnitWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/EditUnitWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/DefaultStandardUnits;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->editUnitWorkflowProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->defaultStandardUnitsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/EditUnitWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/DefaultStandardUnits;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/items/unit/EditUnitWorkflow;Lcom/squareup/util/Res;Lcom/squareup/ui/items/unit/DefaultStandardUnits;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;-><init>(Lcom/squareup/items/unit/EditUnitWorkflow;Lcom/squareup/util/Res;Lcom/squareup/ui/items/unit/DefaultStandardUnits;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->editUnitWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/unit/EditUnitWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->defaultStandardUnitsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/unit/DefaultStandardUnits;

    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->newInstance(Lcom/squareup/items/unit/EditUnitWorkflow;Lcom/squareup/util/Res;Lcom/squareup/ui/items/unit/DefaultStandardUnits;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow_Factory;->get()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;

    move-result-object v0

    return-object v0
.end method
