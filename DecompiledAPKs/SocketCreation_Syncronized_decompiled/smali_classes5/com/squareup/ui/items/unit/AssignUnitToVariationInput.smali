.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;
.super Ljava/lang/Object;
.source "AssignUnitToVariationWorkflow.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/unit/AssignUnitToVariationInput$CREATOR;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0012\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 (2\u00020\u0001:\u0001(B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001a\u001a\u00020\nH\u00c6\u0003JA\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\nH\u00c6\u0001J\u0008\u0010\u001c\u001a\u00020\u001dH\u0016J\u0013\u0010\u001e\u001a\u00020\n2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\"\u001a\u00020\u0005H\u00d6\u0001J\u0018\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u001dH\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u000e\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;",
        "Landroid/os/Parcelable;",
        "itemType",
        "Lcom/squareup/ui/items/unit/ItemType;",
        "selectedUnitId",
        "",
        "selectableUnits",
        "",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "doesVariationHaveStock",
        "",
        "shouldShowDefaultStandardUnits",
        "(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V",
        "getDoesVariationHaveStock",
        "()Z",
        "getItemType",
        "()Lcom/squareup/ui/items/unit/ItemType;",
        "getSelectableUnits",
        "()Ljava/util/List;",
        "getSelectedUnitId",
        "()Ljava/lang/String;",
        "getShouldShowDefaultStandardUnits",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "CREATOR",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput$CREATOR;


# instance fields
.field private final doesVariationHaveStock:Z

.field private final itemType:Lcom/squareup/ui/items/unit/ItemType;

.field private final selectableUnits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedUnitId:Ljava/lang/String;

.field private final shouldShowDefaultStandardUnits:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput$CREATOR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput$CREATOR;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->CREATOR:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput$CREATOR;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/unit/ItemType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "itemType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedUnitId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectableUnits"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    iput-boolean p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    iput-boolean p5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 52
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;-><init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZILjava/lang/Object;)Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->copy(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/items/unit/ItemType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    return v0
.end method

.method public final copy(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/unit/ItemType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;ZZ)",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;"
        }
    .end annotation

    const-string v0, "itemType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedUnitId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectableUnits"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;-><init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    iget-object v1, p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    iget-boolean v1, p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    iget-boolean p1, p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDoesVariationHaveStock()Z
    .locals 1

    .line 51
    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    return v0
.end method

.method public final getItemType()Lcom/squareup/ui/items/unit/ItemType;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    return-object v0
.end method

.method public final getSelectableUnits()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    return-object v0
.end method

.method public final getSelectedUnitId()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    return-object v0
.end method

.method public final getShouldShowDefaultStandardUnits()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AssignUnitToVariationInput(itemType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedUnitId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", selectableUnits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", doesVariationHaveStock="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowDefaultStandardUnits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/ItemType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectedUnitId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->selectableUnits:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 64
    iget-boolean p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->doesVariationHaveStock:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget-boolean p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->shouldShowDefaultStandardUnits:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
