.class public final Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;->invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator\n*L\n1#1,1322:1\n141#2,7:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0008"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$onClickDebounced$1",
        "com/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$nohoRow$1$lambda$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

.field final synthetic $unit$inlined:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$unit$inlined:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$screen$inlined:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$unit$inlined:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    .line 1324
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerItem;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerService;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    if-eqz v0, :cond_2

    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$screen$inlined:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$UnitSelected;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$unit$inlined:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$UnitSelected;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1325
    :cond_2
    instance-of p1, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$screen$inlined:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 1326
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DefaultStandardUnitSelected;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$unit$inlined:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    check-cast v1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;->getDefaultStandardUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DefaultStandardUnitSelected;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_1
    return-void
.end method
