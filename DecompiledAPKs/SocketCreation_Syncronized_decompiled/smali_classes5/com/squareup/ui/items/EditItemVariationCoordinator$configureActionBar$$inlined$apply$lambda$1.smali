.class final Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "EditItemVariationCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureActionBar(Landroid/view/View;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $isNewVariation$inlined:Z

.field final synthetic $view$inlined:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;ZLandroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    iput-boolean p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;->$isNewVariation$inlined:Z

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;->$view$inlined:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getEditItemVariationRunner$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;->backClickedWhenEditingItemVariation()V

    return-void
.end method
