.class Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ModifierSetAssignmentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifierSetAssignmentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/items/ModifierSetAssignmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static final NEW_ITEMS_KEY:Ljava/lang/String; = "assignment_new_items"

.field private static final REMOVED_ITEMS_KEY:Ljava/lang/String; = "assignment_removed_items"


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private categoryNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final data:Lcom/squareup/ui/items/ModifierSetEditState;

.field private final flow:Lflow/Flow;

.field private itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final newItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final removedItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private stateAlreadyLoaded:Z


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/ModifierSetEditState;Ljavax/inject/Provider;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 62
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    .line 64
    iput-object p2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p3, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->flow:Lflow/Flow;

    .line 66
    iput-object p4, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 67
    iput-object p5, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 68
    iput-object p6, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 70
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    .line 71
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    return-void
.end method

.method public static synthetic lambda$jcuYL4ft5EVNO713usV9_nDh0Gg(Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->saveAndFinish()V

    return-void
.end method

.method private loadCursorAndCategoryMap()V
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Presenter$QkLG4zOmwpTiDwuYTGsS7Rf4BUU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Presenter$QkLG4zOmwpTiDwuYTGsS7Rf4BUU;-><init>(Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;)V

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Presenter$mbMcwRzyN5qWZFY1H-7tHC30T_A;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Presenter$mbMcwRzyN5qWZFY1H-7tHC30T_A;-><init>(Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private saveAndFinish()V
    .locals 3

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 130
    invoke-virtual {v0, v2}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->addAssignedItemId(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 133
    invoke-virtual {v0, v2}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->removeAssignedItem(Ljava/lang/String;)Z

    goto :goto_1

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method private showItemsLoaded()V
    .locals 2

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/ModifierSetAssignmentView;

    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/ModifierSetAssignmentView;->updateCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method


# virtual methods
.method public getCategoryNameForId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->categoryNameMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public itemAssignedToModifier(Ljava/lang/String;)Z
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getAssignedItemIds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public itemSelected(Ljava/lang/String;)V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getAssignedItemIds()Ljava/util/Set;

    move-result-object v0

    .line 187
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->itemAssignedToModifier(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 192
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 199
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$loadCursorAndCategoryMap$0$ModifierSetAssignmentScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;
    .locals 6

    .line 150
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 151
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v2, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object v4, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v4, v2, v3

    .line 155
    invoke-virtual {v1, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v0

    goto :goto_0

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/squareup/api/items/Item$Type;

    sget-object v5, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v5, v4, v3

    sget-object v3, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    aput-object v3, v4, v2

    .line 158
    invoke-virtual {v1, v4}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    .line 157
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v0

    .line 160
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->getCategoryNameMap()Ljava/util/Map;

    move-result-object p1

    .line 161
    new-instance v1, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/Map;)V

    return-object v1
.end method

.method public synthetic lambda$loadCursorAndCategoryMap$1$ModifierSetAssignmentScreen$Presenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 163
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;

    .line 164
    iget-object v0, p1, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;->libraryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 165
    iget-object p1, p1, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;->categoryNameMap:Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->categoryNameMap:Ljava/util/Map;

    .line 166
    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->showItemsLoaded()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 210
    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->saveAndFinish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->loadCursorAndCategoryMap()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 79
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->modifier_assignment_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    .line 84
    invoke-virtual {v1}, Lcom/squareup/ui/items/ModifierSetEditState;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "modifier_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->modifier_assignment_title_no_name:I

    .line 88
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 91
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 92
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 93
    iget-object v2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/registerlib/R$string;->secondary_button_save:I

    .line 94
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v3, v2}, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 96
    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Presenter$jcuYL4ft5EVNO713usV9_nDh0Gg;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$ModifierSetAssignmentScreen$Presenter$jcuYL4ft5EVNO713usV9_nDh0Gg;-><init>(Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    if-eqz p1, :cond_1

    .line 98
    iget-boolean v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->stateAlreadyLoaded:Z

    if-nez v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    const-string v2, "assignment_new_items"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 100
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    const-string v2, "assignment_removed_items"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 102
    :cond_1
    iput-boolean v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->stateAlreadyLoaded:Z

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->itemsCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz p1, :cond_2

    .line 105
    invoke-direct {p0}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->showItemsLoaded()V

    :cond_2
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 6

    .line 110
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 115
    iget-object v2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->newItems:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    add-int/lit8 v5, v3, 0x1

    .line 116
    aput-object v4, v0, v3

    move v3, v5

    goto :goto_0

    .line 119
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->removedItems:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    add-int/lit8 v5, v3, 0x1

    .line 120
    aput-object v4, v1, v3

    move v3, v5

    goto :goto_1

    :cond_1
    const-string v2, "assignment_new_items"

    .line 123
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "assignment_removed_items"

    .line 124
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method
