.class Lcom/squareup/ui/items/EditItemEditDetailsView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditItemEditDetailsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemEditDetailsView;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Ljava/lang/String;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemEditDetailsView;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 106
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    .line 108
    invoke-static {v0}, Lcom/squareup/ui/items/EditItemEditDetailsView;->access$000(Lcom/squareup/ui/items/EditItemEditDetailsView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemEditDetailsView;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/widgets/pos/R$string;->new_item:I

    .line 109
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    .line 110
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-static {v1}, Lcom/squareup/ui/items/EditItemEditDetailsView;->access$100(Lcom/squareup/ui/items/EditItemEditDetailsView;)Lcom/squareup/orderentry/TextTile;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-static {v1}, Lcom/squareup/ui/items/EditItemEditDetailsView;->access$100(Lcom/squareup/ui/items/EditItemEditDetailsView;)Lcom/squareup/orderentry/TextTile;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/TextTile;->setName(Ljava/lang/String;)V

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-static {v1}, Lcom/squareup/ui/items/EditItemEditDetailsView;->access$200(Lcom/squareup/ui/items/EditItemEditDetailsView;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setName(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-static {v1}, Lcom/squareup/ui/items/EditItemEditDetailsView;->access$200(Lcom/squareup/ui/items/EditItemEditDetailsView;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getAbbreviationText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemMainPresenter;->isDefaultAbbreviation(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    invoke-static {p1}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-static {v1}, Lcom/squareup/ui/items/EditItemEditDetailsView;->access$200(Lcom/squareup/ui/items/EditItemEditDetailsView;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->abbreviationChanged(Ljava/lang/String;)V

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView$1;->this$0:Lcom/squareup/ui/items/EditItemEditDetailsView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->nameChanged(Ljava/lang/String;)V

    return-void
.end method
