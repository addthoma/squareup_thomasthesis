.class public Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;
.super Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;
.source "AllItemsDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/AllItemsDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AllItemsDetailPresenter"
.end annotation


# instance fields
.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 15
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v14, p0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    .line 66
    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    move-object/from16 v0, p10

    .line 69
    iput-object v0, v14, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v0, p15

    .line 70
    iput-object v0, v14, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method


# virtual methods
.method protected getActionBarTitle()Ljava/lang/CharSequence;
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/itemsapplet/R$string;->items_applet_all_items_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getAnalyticsTapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method

.method getButtonText(I)Ljava/lang/String;
    .locals 1

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/registerlib/R$string;->create_item:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getItemTypes()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getNullHint(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2

    .line 87
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/itemsapplet/R$string;->items_applet_items_null_message:I

    const-string v1, "learn_more"

    .line 88
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/registerlib/R$string;->items_hint_url:I

    .line 89
    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    .line 90
    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method getNullTitle()Ljava/lang/String;
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/itemsapplet/R$string;->items_applet_items_null_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getScreenInstance()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/ui/items/AllItemsDetailScreen;->INSTANCE:Lcom/squareup/ui/items/AllItemsDetailScreen;

    return-object v0
.end method

.method getSearchHint()Ljava/lang/String;
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->search_library_hint_all_items:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onButtonClicked(I)V
    .locals 1

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    sget-object v0, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    invoke-interface {p1, v0}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 74
    invoke-super {p0, p1}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/items/AllItemsDetailScreen$AllItemsDetailPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "All Items Screen Shown"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method
