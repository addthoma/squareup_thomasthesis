.class Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditServiceMainViewSingleVariationStaticRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$1;->this$0:Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$1;->this$0:Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;

    iget-object v0, v0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$1;->this$0:Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;

    iget-object v0, v0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->onCancellationFeeChanged(Lcom/squareup/protos/common/Money;)V

    return-void
.end method
