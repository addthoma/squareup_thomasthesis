.class public Lcom/squareup/ui/items/AssignInventoryErrorDialogScreen$Factory;
.super Ljava/lang/Object;
.source "AssignInventoryErrorDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/AssignInventoryErrorDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 23
    const-class v0, Lcom/squareup/ui/items/EditItemScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemScope$Component;

    .line 24
    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditItemScopeRunner;

    move-result-object v0

    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 27
    sget v2, Lcom/squareup/edititem/R$string;->assign_stock_error_title:I

    .line 28
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/squareup/edititem/R$string;->assign_stock_error_message:I

    .line 29
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/squareup/common/strings/R$string;->ok:I

    .line 30
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/squareup/cardreader/ui/R$string;->try_again:I

    .line 31
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 27
    invoke-static {v2, v3, v4, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$jSnXe1UfwAMPy3naDkwx_jlg1nY;

    invoke-direct {v2, v0}, Lcom/squareup/ui/items/-$$Lambda$jSnXe1UfwAMPy3naDkwx_jlg1nY;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$VkfSFD1YC3pq9SIhaprqFM19TsM;

    invoke-direct {v3, v0}, Lcom/squareup/ui/items/-$$Lambda$VkfSFD1YC3pq9SIhaprqFM19TsM;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 32
    invoke-virtual {v1, p1, v2, v3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 35
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 37
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
