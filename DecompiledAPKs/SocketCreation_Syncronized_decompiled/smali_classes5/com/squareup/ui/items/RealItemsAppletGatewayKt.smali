.class public final Lcom/squareup/ui/items/RealItemsAppletGatewayKt;
.super Ljava/lang/Object;
.source "RealItemsAppletGateway.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002H\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "isInItemsAppletScope",
        "",
        "",
        "items-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final isInItemsAppletScope(Ljava/lang/Object;)Z
    .locals 2

    .line 49
    instance-of v0, p0, Lcom/squareup/container/ContainerTreeKey;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    const-class v1, Lcom/squareup/ui/items/InItemsAppletScope;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 50
    :cond_0
    instance-of v0, p0, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/container/WorkflowTreeKey;

    iget-object p0, p0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    :cond_1
    const/4 p0, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
