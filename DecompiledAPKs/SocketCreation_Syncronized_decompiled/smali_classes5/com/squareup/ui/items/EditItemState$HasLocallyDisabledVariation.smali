.class final enum Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;
.super Ljava/lang/Enum;
.source "EditItemState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "HasLocallyDisabledVariation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

.field public static final enum NO:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

.field public static final enum UNKNOWN:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

.field public static final enum YES:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1365
    new-instance v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->UNKNOWN:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    .line 1366
    new-instance v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    const/4 v2, 0x1

    const-string v3, "YES"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->YES:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    .line 1367
    new-instance v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    const/4 v3, 0x2

    const-string v4, "NO"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->NO:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    .line 1364
    sget-object v4, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->UNKNOWN:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->YES:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->NO:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->$VALUES:[Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1364
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;
    .locals 1

    .line 1364
    const-class v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;
    .locals 1

    .line 1364
    sget-object v0, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->$VALUES:[Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    return-object v0
.end method
