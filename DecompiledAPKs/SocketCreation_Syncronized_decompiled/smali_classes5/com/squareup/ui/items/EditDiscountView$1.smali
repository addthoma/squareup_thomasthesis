.class Lcom/squareup/ui/items/EditDiscountView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditDiscountView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditDiscountView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditDiscountView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditDiscountView;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView$1;->this$0:Lcom/squareup/ui/items/EditDiscountView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 124
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 125
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView$1;->this$0:Lcom/squareup/ui/items/EditDiscountView;

    invoke-static {p2}, Lcom/squareup/ui/items/EditDiscountView;->access$000(Lcom/squareup/ui/items/EditDiscountView;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 126
    iget-object p3, p0, Lcom/squareup/ui/items/EditDiscountView$1;->this$0:Lcom/squareup/ui/items/EditDiscountView;

    invoke-static {p3}, Lcom/squareup/ui/items/EditDiscountView;->access$100(Lcom/squareup/ui/items/EditDiscountView;)Landroid/widget/TextView;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object p3, p0, Lcom/squareup/ui/items/EditDiscountView$1;->this$0:Lcom/squareup/ui/items/EditDiscountView;

    invoke-static {p3, p2}, Lcom/squareup/ui/items/EditDiscountView;->access$200(Lcom/squareup/ui/items/EditDiscountView;Ljava/lang/String;)V

    .line 128
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView$1;->this$0:Lcom/squareup/ui/items/EditDiscountView;

    iget-object p2, p2, Lcom/squareup/ui/items/EditDiscountView;->presenter:Lcom/squareup/ui/items/EditDiscountScreen$Presenter;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->onNameChanged(Ljava/lang/String;)V

    return-void
.end method
