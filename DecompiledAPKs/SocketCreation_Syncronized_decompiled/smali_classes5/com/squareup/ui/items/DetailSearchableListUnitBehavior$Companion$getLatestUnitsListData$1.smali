.class final Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;
.super Ljava/lang/Object;
.source "DetailSearchableListUnitBehavior.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;->getLatestUnitsListData(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;)Z
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    .line 62
    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->MEASUREMENT_UNIT:Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$1;->test(Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;)Z

    move-result p1

    return p1
.end method
