.class public final Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;
.super Ljava/lang/Object;
.source "DetailSearchableListBehaviorFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogLocalizerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final discountBundleFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final discountCursorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final timeZoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->discountCursorFactoryProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->discountBundleFactoryProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->timeZoneProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->catalogLocalizerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;"
        }
    .end annotation

    .line 75
    new-instance v11, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/badbus/BadBus;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;"
        }
    .end annotation

    .line 84
    new-instance v11, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;
    .locals 11

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/badbus/BadBus;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->resProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->discountCursorFactoryProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->discountBundleFactoryProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->timeZoneProvider:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->catalogLocalizerProvider:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory_Factory;->get()Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

    move-result-object v0

    return-object v0
.end method
