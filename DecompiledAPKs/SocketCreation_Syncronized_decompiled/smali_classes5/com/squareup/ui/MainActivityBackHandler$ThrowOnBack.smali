.class public Lcom/squareup/ui/MainActivityBackHandler$ThrowOnBack;
.super Ljava/lang/Object;
.source "MainActivityBackHandler.java"

# interfaces
.implements Lcom/squareup/ui/MainActivityBackHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/MainActivityBackHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThrowOnBack"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMainActivityBackPressed(Landroid/app/Activity;)V
    .locals 1

    .line 24
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Back not handled"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
