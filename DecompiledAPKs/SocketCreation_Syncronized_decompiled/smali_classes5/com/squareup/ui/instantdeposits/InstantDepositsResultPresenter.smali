.class Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;
.super Lmortar/ViewPresenter;
.source "InstantDepositsResultPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final flow:Lflow/Flow;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/ui/activity/ActivityAppletScopeRunner;Lcom/squareup/util/BrowserLauncher;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            "Lcom/squareup/ui/activity/ActivityAppletScopeRunner;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->res:Lcom/squareup/util/Res;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->scopeRunner:Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    .line 45
    iput-object p6, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->flow:Lflow/Flow;

    .line 46
    iput-object p7, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static synthetic lambda$3LNpAQ8d0fz_TQM309eqNHKrUQk(Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->updateScreen(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    return-void
.end method

.method private onFailure(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_HEADER_DEPOSITING_ERROR:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->instantDepositFailed(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-direct {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->setActionBarTitleFailure()V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;

    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object p3, p3, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-ne p3, v1, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-virtual {v0, p3, p1, p2}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->showLearnMoreButton(ZLjava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private onServerCallFailed(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 3

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_unavailable:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->instant_deposits_network_error_message:I

    .line 119
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->onFailure(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    return-void
.end method

.method private onSuccess(Lcom/squareup/protos/common/Money;)V
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INSTANT_DEPOSIT_HEADER_DEPOSITING_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->instantDepositSucceeded(Lcom/squareup/analytics/RegisterViewName;)V

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->setActionBarTitleAmount(Lcom/squareup/protos/common/Money;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 78
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;

    if-eqz v1, :cond_0

    .line 80
    iget-object v2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v0}, Lcom/squareup/instantdeposit/InstantDepositCountryResources;->instantDepositSuccessTitle(Lcom/squareup/CountryCode;)I

    move-result v0

    invoke-interface {v2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 81
    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v2, "amount"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 80
    invoke-virtual {v1, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->onSuccess(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private setActionBarTitleAmount(Lcom/squareup/protos/common/Money;)V
    .locals 4

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->instant_deposits_deposit_result_card_title:I

    .line 89
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 91
    invoke-interface {v3, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v3, "amount"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 88
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private setActionBarTitleFailure()V
    .locals 4

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->instant_deposits_deposit_failed:I

    .line 99
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private updateScreen(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 2

    .line 59
    sget-object v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter$1;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    iget-object v1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->onServerCallFailed(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositFailedTitle()Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositFailedDescription()Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->onFailure(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->sentDepositAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->onSuccess(Lcom/squareup/protos/common/Money;)V

    :goto_0
    return-void
.end method


# virtual methods
.method finish()V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->scopeRunner:Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->checkIfEligibleForInstantDeposit()V

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lflow/Flow;->get(Landroid/view/View;)Lflow/Flow;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public synthetic lambda$onLoad$0$InstantDepositsResultPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->scopeRunner:Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;->instantDepositResultScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultPresenter$3LNpAQ8d0fz_TQM309eqNHKrUQk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultPresenter$3LNpAQ8d0fz_TQM309eqNHKrUQk;-><init>(Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;)V

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method learnMore(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_HEADER_LEARN_MORE:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tapLearnMore(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object p2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->instant_deposits_unavailable_troubleshooting:I

    .line 132
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 131
    invoke-interface {p1, p2}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;

    invoke-virtual {p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen;

    .line 51
    iget-object p1, p1, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen;->depositAmount:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, p1}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->setActionBarTitleAmount(Lcom/squareup/protos/common/Money;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultPresenter$EJdp-MGfyoRIKwV4fHwIBz2lVBc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultPresenter$EJdp-MGfyoRIKwV4fHwIBz2lVBc;-><init>(Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
