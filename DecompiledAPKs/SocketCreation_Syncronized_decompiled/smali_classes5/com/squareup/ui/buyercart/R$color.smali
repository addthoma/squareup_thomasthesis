.class public final Lcom/squareup/ui/buyercart/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final cart_diff_highlight_background:I = 0x7f06005b

.field public static final header_background:I = 0x7f0600de

.field public static final register_blue:I = 0x7f0602a2

.field public static final register_blue_button_color:I = 0x7f0602a3

.field public static final register_blue_pressed:I = 0x7f0602a4

.field public static final sq_dark_gray:I = 0x7f0602de

.field public static final sq_dark_gray_button_color_pressed:I = 0x7f0602df

.field public static final sq_dark_gray_pressed:I = 0x7f0602e0

.field public static final sq_light_gray:I = 0x7f0602e1

.field public static final sq_light_gray_pressed:I = 0x7f0602e2

.field public static final title_color:I = 0x7f0602f9

.field public static final title_color_default:I = 0x7f0602fa

.field public static final title_color_disabled:I = 0x7f0602fb

.field public static final title_color_purchase:I = 0x7f0602fc

.field public static final title_color_refund:I = 0x7f0602fd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
