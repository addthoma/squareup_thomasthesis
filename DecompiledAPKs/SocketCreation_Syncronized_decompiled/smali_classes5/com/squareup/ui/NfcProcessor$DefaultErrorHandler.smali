.class Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;
.super Ljava/lang/Object;
.source "NfcProcessor.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/NfcProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DefaultErrorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/NfcProcessor;)V
    .locals 0

    .line 956
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleActionRequired()V
    .locals 2

    .line 967
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcActionRequired;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcActionRequired;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleCardBlocked()V
    .locals 2

    .line 983
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCardDeclined;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCardDeclined;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleCardDeclined()V
    .locals 2

    .line 979
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCardDeclined;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCardDeclined;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleCardTapAgain()V
    .locals 2

    .line 987
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcTapAgain;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcTapAgain;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleCollisionDetected()V
    .locals 2

    .line 991
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCollisionDetected;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCollisionDetected;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleInterfaceUnavailable()V
    .locals 2

    .line 959
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcInterfaceUnavailable;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcInterfaceUnavailable;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleLimitExceededInsertCard()V
    .locals 2

    .line 999
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleLimitExceededTryAnotherCard()V
    .locals 2

    .line 995
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededTryAnotherCard;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededTryAnotherCard;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleOnRequestTapCard()V
    .locals 2

    .line 1003
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleProcessingError()V
    .locals 2

    .line 975
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcProcessingError;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcProcessingError;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleTryAnotherCard()V
    .locals 2

    .line 963
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCallYourBank;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCallYourBank;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public handleUnlockDevice()V
    .locals 2

    .line 971
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcUnlockDevice;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcUnlockDevice;

    invoke-static {v0, v1}, Lcom/squareup/ui/NfcProcessor;->access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method
