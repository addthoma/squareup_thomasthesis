.class public Lcom/squareup/ui/account/view/LineRow;
.super Landroid/widget/LinearLayout;
.source "LineRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/account/view/LineRow$Builder;
    }
.end annotation


# instance fields
.field private final chevronView:Lcom/squareup/glyph/SquareGlyphView;

.field private chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

.field private displayValueAsPercent:Z

.field private final glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private final noteView:Lcom/squareup/marketfont/MarketTextView;

.field private final preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

.field private final valueView:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 137
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101004d

    aput v2, v0, v1

    const/4 v2, 0x0

    .line 140
    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 141
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/squareup/ui/account/view/LineRow;->setMinimumHeight(I)V

    .line 142
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 144
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->LineRow:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 145
    sget v0, Lcom/squareup/widgets/pos/R$styleable;->LineRow_lineRowPadding:I

    .line 146
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/LineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 145
    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 148
    invoke-virtual {p0, v0, v1, v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setPadding(IIII)V

    .line 150
    sget v0, Lcom/squareup/widgets/pos/R$layout;->line_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/account/view/LineRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 152
    sget p1, Lcom/squareup/widgets/pos/R$id;->line_row_glyph_view:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 153
    sget p1, Lcom/squareup/widgets/pos/R$id;->preserved_label:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/PreservedLabelView;

    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    .line 154
    sget p1, Lcom/squareup/widgets/pos/R$id;->chevron:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->chevronView:Lcom/squareup/glyph/SquareGlyphView;

    .line 155
    sget p1, Lcom/squareup/widgets/pos/R$id;->value:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    .line 156
    sget p1, Lcom/squareup/widgets/pos/R$id;->note:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->noteView:Lcom/squareup/marketfont/MarketTextView;

    .line 158
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_chevronVisibility:I

    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ChevronVisibility;->ordinal()I

    move-result v0

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    .line 159
    invoke-static {}, Lcom/squareup/marin/widgets/ChevronVisibility;->values()[Lcom/squareup/marin/widgets/ChevronVisibility;

    move-result-object v0

    aget-object p1, v0, p1

    .line 160
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 162
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_displayValueAsPercent:I

    invoke-virtual {p2, p1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    .line 163
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setDisplayValueAsPercent(Z)V

    .line 165
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_weight:I

    const/4 v0, -0x1

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    if-eq p1, v0, :cond_0

    .line 167
    iget-object v3, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-static {}, Lcom/squareup/marketfont/MarketFont$Weight;->values()[Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    aget-object p1, v4, p1

    invoke-virtual {v3, p1}, Lcom/squareup/widgets/PreservedLabelView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 170
    :cond_0
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_valueWeight:I

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    if-eq p1, v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {}, Lcom/squareup/marketfont/MarketFont$Weight;->values()[Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v3

    aget-object p1, v3, p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 175
    :cond_1
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_sq_valueColor:I

    invoke-virtual {p2, p1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 180
    :cond_2
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_glyph:I

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    invoke-static {p2, p1, v0, v2}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz p1, :cond_3

    .line 182
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    .line 184
    :cond_3
    invoke-direct {p0, v1}, Lcom/squareup/ui/account/view/LineRow;->setGlyphVisible(Z)V

    .line 187
    :goto_0
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_titleTextSize:I

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p1

    cmpl-float v0, p1, v0

    if-lez v0, :cond_4

    .line 189
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setTitleTextSize(F)V

    .line 192
    :cond_4
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_titleText:I

    invoke-virtual {p2, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 193
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 195
    :cond_5
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_sq_noteText:I

    invoke-virtual {p2, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 196
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setNote(Ljava/lang/CharSequence;)V

    .line 198
    :cond_6
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_sq_labelText:I

    invoke-virtual {p2, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 199
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabel(Ljava/lang/CharSequence;)V

    .line 201
    :cond_7
    sget p1, Lcom/squareup/widgets/pos/R$styleable;->LineRow_sq_valueText:I

    invoke-virtual {p2, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 202
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 204
    :cond_8
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;ILcom/squareup/marin/widgets/ChevronVisibility;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;I)V
    .locals 1

    const/4 v0, 0x0

    .line 211
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/account/view/LineRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 213
    iput-boolean p9, p0, Lcom/squareup/ui/account/view/LineRow;->displayValueAsPercent:Z

    if-eqz p2, :cond_0

    .line 216
    invoke-virtual {p0, p2}, Lcom/squareup/ui/account/view/LineRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 218
    invoke-direct {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setGlyphVisible(Z)V

    .line 221
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1, p10}, Lcom/squareup/widgets/PreservedLabelView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 222
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->noteView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p11}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p12}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 225
    invoke-virtual {p0, p3}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    if-le p5, p1, :cond_1

    .line 226
    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabel(Ljava/lang/CharSequence;)V

    .line 227
    invoke-virtual {p0, p4}, Lcom/squareup/ui/account/view/LineRow;->setNote(Ljava/lang/CharSequence;)V

    .line 228
    invoke-virtual {p0, p8}, Lcom/squareup/ui/account/view/LineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 229
    invoke-virtual {p0, p6}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    if-lez p13, :cond_2

    .line 231
    invoke-virtual {p0, p13}, Lcom/squareup/ui/account/view/LineRow;->setTitleTextSize(I)V

    :cond_2
    const/4 p1, -0x1

    if-eq p7, p1, :cond_3

    .line 235
    invoke-virtual {p0, p7}, Lcom/squareup/ui/account/view/LineRow;->setBackgroundResource(I)V

    :cond_3
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;ILcom/squareup/marin/widgets/ChevronVisibility;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/ui/account/view/LineRow$1;)V
    .locals 0

    .line 33
    invoke-direct/range {p0 .. p13}, Lcom/squareup/ui/account/view/LineRow;-><init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;ILcom/squareup/marin/widgets/ChevronVisibility;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;I)V

    return-void
.end method

.method private refreshChevron()V
    .locals 4

    .line 260
    sget-object v0, Lcom/squareup/ui/account/view/LineRow$1;->$SwitchMap$com$squareup$marin$widgets$ChevronVisibility:[I

    iget-object v1, p0, Lcom/squareup/ui/account/view/LineRow;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ChevronVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x8

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    const/4 v3, 0x0

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 268
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/LineRow;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->chevronView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v3}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->chevronView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 275
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized chevronVisibility attribute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/account/view/LineRow;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->chevronView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v3}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    goto :goto_0

    .line 262
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->chevronView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private setGlyphVisible(Z)V
    .locals 1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public getApparentPadding()I
    .locals 2

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/account/view/LineRow;->noteView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 382
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/LineRow;->getHeight()I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    return v1
.end method

.method public getPreservedTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/CharSequence;
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 256
    invoke-direct {p0}, Lcom/squareup/ui/account/view/LineRow;->refreshChevron()V

    return-void
.end method

.method public setDisplayValueAsPercent(Z)V
    .locals 0

    .line 327
    iput-boolean p1, p0, Lcom/squareup/ui/account/view/LineRow;->displayValueAsPercent:Z

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 240
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 241
    invoke-direct {p0}, Lcom/squareup/ui/account/view/LineRow;->refreshChevron()V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setSaveEnabled(Z)V

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    const/4 p1, 0x1

    .line 251
    invoke-direct {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setGlyphVisible(Z)V

    return-void
.end method

.method public setNote(Ljava/lang/CharSequence;)V
    .locals 1

    .line 317
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->noteView:Lcom/squareup/marketfont/MarketTextView;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->noteView:Lcom/squareup/marketfont/MarketTextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_0

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->noteView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->noteView:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public setPreservedLabel(Ljava/lang/CharSequence;)V
    .locals 1

    .line 307
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    .line 309
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->hidePreservedLabel()V

    goto :goto_0

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    .line 312
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->showPreservedLabel()V

    :goto_0
    return-void
.end method

.method public setPreservedLabelTextColor(I)V
    .locals 1

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTextColor(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 281
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 283
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->hideTitle()V

    goto :goto_0

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 286
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->showTitle()V

    :goto_0
    return-void
.end method

.method public setTitleTextSize(F)V
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTextSize(F)V

    return-void
.end method

.method public setTitleTextSize(I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTextSize(I)V

    return-void
.end method

.method public setValue(I)V
    .locals 1

    .line 335
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/LineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setValue(Ljava/lang/CharSequence;)V
    .locals 2

    .line 351
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void

    .line 357
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/account/view/LineRow;->displayValueAsPercent:Z

    if-eqz v0, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/squareup/ui/account/view/LineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/widgets/R$string;->percent_character_pattern:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "value"

    .line 360
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 361
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object p1, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method public setValueColor(I)V
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    return-void
.end method

.method public setValueEnabled(Z)V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    return-void
.end method

.method public setValueTextSize(I)V
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/account/view/LineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    return-void
.end method

.method public setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->valueView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method
