.class Lcom/squareup/ui/DropDownContainer$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "DropDownContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/DropDownContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/DropDownContainer$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dropDownOpen:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 466
    new-instance v0, Lcom/squareup/ui/DropDownContainer$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/ui/DropDownContainer$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/DropDownContainer$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 484
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 485
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/DropDownContainer$SavedState;->dropDownOpen:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/ui/DropDownContainer$1;)V
    .locals 0

    .line 465
    invoke-direct {p0, p1}, Lcom/squareup/ui/DropDownContainer$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;Z)V
    .locals 0

    .line 479
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 480
    iput-boolean p2, p0, Lcom/squareup/ui/DropDownContainer$SavedState;->dropDownOpen:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;ZLcom/squareup/ui/DropDownContainer$1;)V
    .locals 0

    .line 465
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DropDownContainer$SavedState;-><init>(Landroid/os/Parcelable;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/DropDownContainer$SavedState;)Z
    .locals 0

    .line 465
    iget-boolean p0, p0, Lcom/squareup/ui/DropDownContainer$SavedState;->dropDownOpen:Z

    return p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 489
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 490
    iget-boolean p2, p0, Lcom/squareup/ui/DropDownContainer$SavedState;->dropDownOpen:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
