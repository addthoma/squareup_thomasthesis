.class public final Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "DiscountEntryMoneyScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final authorizingEmployeeBundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;>;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final discountEntryScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final vibratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation
.end field

.field private final workingDiscountBundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p2, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p3, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p4, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p5, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->discountEntryScreenRunnerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p6, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p7, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p8, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p9, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p10, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->workingDiscountBundleKeyProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p11, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->authorizingEmployeeBundleKeyProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p12, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;"
        }
    .end annotation

    .line 94
    new-instance v13, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/ui/library/DiscountEntryScreenRunner;Landroid/os/Vibrator;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
            "Landroid/os/Vibrator;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;"
        }
    .end annotation

    .line 104
    new-instance v13, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/ui/library/DiscountEntryScreenRunner;Landroid/os/Vibrator;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/Features;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;
    .locals 13

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->discountEntryScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/os/Vibrator;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->workingDiscountBundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->authorizingEmployeeBundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/ui/library/DiscountEntryScreenRunner;Landroid/os/Vibrator;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen_Presenter_Factory;->get()Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
