.class Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;
.super Lcom/squareup/padlock/PercentageKeypadListener;
.source "DiscountEntryPercentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PercentageEntryKeypadListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)V
    .locals 1

    .line 161
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;

    .line 162
    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->access$900(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/DiscountEntryPercentView;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryPercentView;->getKeypad()Lcom/squareup/padlock/Padlock;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->access$1000(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)Landroid/os/Vibrator;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/padlock/PercentageKeypadListener;-><init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;)V

    return-void
.end method


# virtual methods
.method public onPercentUpdated(Ljava/lang/String;Z)V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->access$1100(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$1200(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Ljava/lang/String;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;

    invoke-static {p1, p2}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->access$1300(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;Z)V

    return-void
.end method

.method public onSubmitClicked()V
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->onCommitSelected()Z

    return-void
.end method
