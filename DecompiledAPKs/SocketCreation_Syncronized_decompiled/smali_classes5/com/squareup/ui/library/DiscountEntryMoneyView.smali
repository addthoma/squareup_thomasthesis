.class public Lcom/squareup/ui/library/DiscountEntryMoneyView;
.super Lcom/squareup/register/widgets/KeypadEntryView;
.source "DiscountEntryMoneyView.java"

# interfaces
.implements Lcom/squareup/container/VisualTransitionListener;


# instance fields
.field presenter:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/KeypadEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const-class p2, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Component;->inject(Lcom/squareup/ui/library/DiscountEntryMoneyView;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyView;->presenter:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->onCommitSelected()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyView;->presenter:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 27
    invoke-super {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 21
    invoke-super {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->onFinishInflate()V

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyView;->presenter:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onStartVisualTransition()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyView;->presenter:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->onStartVisualTransition()V

    return-void
.end method
