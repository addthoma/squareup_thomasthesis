.class Lcom/squareup/ui/buyer/BuyerScope$1;
.super Ljava/lang/Object;
.source "BuyerScope.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/BuyerScope;->register(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/BuyerScope;

.field final synthetic val$component:Lcom/squareup/ui/buyer/BuyerScopeComponent;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/ui/buyer/BuyerScopeComponent;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScope$1;->this$0:Lcom/squareup/ui/buyer/BuyerScope;

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerScope$1;->val$component:Lcom/squareup/ui/buyer/BuyerScopeComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScope$1;->val$component:Lcom/squareup/ui/buyer/BuyerScopeComponent;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerScopeComponent;->customLocale()Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->reset()V

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScope$1;->val$component:Lcom/squareup/ui/buyer/BuyerScopeComponent;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerScopeComponent;->changeHudToaster()Lcom/squareup/tenderpayment/ChangeHudToaster;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/tenderpayment/ChangeHudToaster;->toastIfAvailable()V

    return-void
.end method
