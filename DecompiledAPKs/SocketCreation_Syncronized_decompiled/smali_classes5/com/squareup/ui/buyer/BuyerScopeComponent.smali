.class public interface abstract Lcom/squareup/ui/buyer/BuyerScopeComponent;
.super Ljava/lang/Object;
.source "BuyerScopeComponent.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/buyer/BuyerFlowModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/BuyerScopeComponent$ParentComponent;
    }
.end annotation


# virtual methods
.method public abstract authSpinner()Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen$Component;
.end method

.method public abstract buyerOrderTickerName()Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen$Component;
.end method

.method public abstract changeHudToaster()Lcom/squareup/tenderpayment/ChangeHudToaster;
.end method

.method public abstract customLocale()Lcom/squareup/buyer/language/BuyerLocaleOverride;
.end method

.method public abstract displayNameProvider()Lcom/squareup/ui/buyer/DisplayNameProvider;
.end method

.method public abstract emailCollection()Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Component;
.end method

.method public abstract emvApproved()Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Component;
.end method

.method public abstract emvPath(Lcom/squareup/ui/buyer/emv/EmvScope$Module;)Lcom/squareup/ui/buyer/emv/EmvScope$Component;
.end method

.method public abstract invoicePaid()Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen$Component;
.end method

.method public abstract invoiceSentSaved()Lcom/squareup/ui/invoices/InvoiceSentSavedScreen$Component;
.end method

.method public abstract loyalty()Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen$Component;
.end method

.method public abstract partialAuthWarning()Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Component;
.end method

.method public abstract payContactless()Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen$Component;
.end method

.method public abstract paymentError()Lcom/squareup/ui/buyer/error/PaymentErrorScreen$Component;
.end method

.method public abstract permissionPasscodeGatekeeper()Lcom/squareup/permissions/PermissionGatekeeper;
.end method

.method public abstract postAuthCoupon()Lcom/squareup/ui/buyer/coupon/PostAuthCouponScreen$Component;
.end method

.method public abstract receiptPhone()Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$PhoneComponent;
.end method

.method public abstract receiptTablet()Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$TabletComponent;
.end method

.method public abstract retryTender()Lcom/squareup/ui/buyer/retry/RetryNonEmvTenderScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/buyer/BuyerScopeRunner;
.end method

.method public abstract sign()Lcom/squareup/ui/buyer/signature/SignScreen$Component;
.end method
