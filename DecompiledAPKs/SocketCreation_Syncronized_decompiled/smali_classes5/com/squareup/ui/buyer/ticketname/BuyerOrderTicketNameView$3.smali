.class Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$3;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "BuyerOrderTicketNameView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$3;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$3;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->presenter:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->onCommitOrderName()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
