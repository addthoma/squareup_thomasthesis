.class Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;
.super Ljava/lang/Object;
.source "BuyerOrderTicketNamePresenter.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CardholderNameListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)V
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$1;)V
    .locals 0

    .line 324
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;)V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->handleCardholderError()V

    return-void
.end method

.method public onNameReceived(Ljava/lang/String;)V
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->processNameReceived(Ljava/lang/String;)V

    return-void
.end method

.method public onTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-static {v0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->access$600(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method
