.class Lcom/squareup/ui/buyer/signature/RefundPolicy;
.super Ljava/lang/Object;
.source "RefundPolicy.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/signature/RefundPolicy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final policy:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/buyer/signature/RefundPolicy$1;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/signature/RefundPolicy$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/signature/RefundPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/RefundPolicy;->title:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/RefundPolicy;->policy:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 21
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/RefundPolicy;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 22
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/RefundPolicy;->policy:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
