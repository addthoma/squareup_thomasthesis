.class public final Lcom/squareup/ui/buyer/signature/SignScreenRunner;
.super Ljava/lang/Object;
.source "SignScreenRunner.kt"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/buyer/signature/SignScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSignScreenRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SignScreenRunner.kt\ncom/squareup/ui/buyer/signature/SignScreenRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,333:1\n1370#2:334\n1401#2,4:335\n*E\n*S KotlinDebug\n*F\n+ 1 SignScreenRunner.kt\ncom/squareup/ui/buyer/signature/SignScreenRunner\n*L\n170#1:334\n170#1,4:335\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ce\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001:\u0001KB_\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\u0008\u0010 \u001a\u00020!H\u0002J\u0008\u0010\"\u001a\u00020!H\u0002J\u0008\u0010#\u001a\u00020$H\u0016J\u001c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&2\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020)0&H\u0002J\u0008\u0010*\u001a\u00020+H\u0002J\u0010\u0010,\u001a\u00020!2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010-\u001a\n\u0012\u0004\u0012\u00020!\u0018\u00010.H\u0002J\u0010\u0010/\u001a\u00020!2\u0006\u00100\u001a\u00020\u001aH\u0002J\u0010\u00101\u001a\u00020!2\u0006\u00102\u001a\u000203H\u0016J\u0008\u00104\u001a\u00020!H\u0016J\u0012\u00105\u001a\u00020!2\u0008\u00106\u001a\u0004\u0018\u000107H\u0016J\u0012\u00108\u001a\u00020!2\u0008\u00109\u001a\u0004\u0018\u000107H\u0016J\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u00020\u001f0;J\u0010\u0010<\u001a\u00020!2\u0006\u0010=\u001a\u00020>H\u0002J\u0010\u0010?\u001a\u00020!2\u0006\u0010@\u001a\u00020AH\u0002J\u001a\u0010B\u001a\u00020!2\u0006\u0010C\u001a\u00020D2\u0008\u0010E\u001a\u0004\u0018\u00010FH\u0002J\u001e\u0010B\u001a\u00020!2\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020)0&2\u0006\u0010G\u001a\u00020HH\u0002J\u0008\u0010I\u001a\u00020!H\u0002J\u0008\u0010J\u001a\u00020!H\u0002R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006L"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/signature/SignScreenRunner;",
        "Lmortar/bundler/Bundler;",
        "device",
        "Lcom/squareup/util/Device;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "buyerAmountTextProvider",
        "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "buyerScopeRunner",
        "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
        "flow",
        "Lflow/Flow;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "tipDeterminerFactory",
        "Lcom/squareup/payment/TipDeterminerFactory;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lflow/Flow;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/analytics/Analytics;)V",
        "doneButtonAlwaysEnabled",
        "",
        "localeOverrideFactory",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "viewDataRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/ui/buyer/signature/SignScreenData;",
        "clearSignature",
        "",
        "clearTip",
        "getMortarBundleKey",
        "",
        "getTipOptionInfo",
        "",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
        "tipOptions",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        "getTitleSubtitle",
        "Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;",
        "initialSetup",
        "noSigSubmitSignatureHandler",
        "Lkotlin/Function0;",
        "onCancelButtonPressed",
        "canRetreat",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onSave",
        "outState",
        "screenData",
        "Lio/reactivex/Observable;",
        "setCustomTip",
        "tip",
        "",
        "setSignature",
        "signature",
        "Lcom/squareup/signature/SignatureAsJson;",
        "setTip",
        "tipMoney",
        "Lcom/squareup/protos/common/Money;",
        "percentage",
        "Lcom/squareup/util/Percentage;",
        "index",
        "",
        "showReturnPolicy",
        "submitSignature",
        "TitleSubtitle",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final device:Lcom/squareup/util/Device;

.field private doneButtonAlwaysEnabled:Z

.field private final flow:Lflow/Flow;

.field private localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lflow/Flow;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerAmountTextProvider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerScopeRunner"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tipDeterminerFactory"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->device:Lcom/squareup/util/Device;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    iput-object p5, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p6, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p7, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iput-object p8, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->flow:Lflow/Flow;

    iput-object p9, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iput-object p10, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    iput-object p11, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 56
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$clearSignature(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->clearSignature()V

    return-void
.end method

.method public static final synthetic access$clearTip(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->clearTip()V

    return-void
.end method

.method public static final synthetic access$initialSetup(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->initialSetup(Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method public static final synthetic access$onCancelButtonPressed(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Z)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->onCancelButtonPressed(Z)V

    return-void
.end method

.method public static final synthetic access$setCustomTip(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Ljava/lang/CharSequence;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->setCustomTip(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static final synthetic access$setSignature(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Lcom/squareup/signature/SignatureAsJson;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->setSignature(Lcom/squareup/signature/SignatureAsJson;)V

    return-void
.end method

.method public static final synthetic access$setTip(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Ljava/util/List;I)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->setTip(Ljava/util/List;I)V

    return-void
.end method

.method public static final synthetic access$showReturnPolicy(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->showReturnPolicy()V

    return-void
.end method

.method public static final synthetic access$submitSignature(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->submitSignature()V

    return-void
.end method

.method private final clearSignature()V
    .locals 11

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireSignedPayment()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    const/4 v1, 0x0

    .line 228
    invoke-interface {v0, v1}, Lcom/squareup/payment/AcceptsSignature;->setVectorSignature(Lcom/squareup/signature/SignatureAsJson;)V

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/buyer/signature/SignScreenData;

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v1, :cond_1

    .line 233
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 234
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getSignatureConfig()Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 236
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->noSigSubmitSignatureHandler()Lkotlin/jvm/functions/Function0;

    move-result-object v6

    const/4 v8, 0x0

    const/16 v9, 0x9

    const/4 v10, 0x0

    .line 234
    invoke-static/range {v4 .. v10}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ZILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    move-result-object v4

    const/4 v6, 0x0

    const/16 v7, 0x1b

    const/4 v8, 0x0

    .line 233
    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/buyer/signature/SignScreenData;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;ILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData;

    move-result-object v1

    .line 232
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final clearTip()V
    .locals 13

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->clearTip()V

    .line 244
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->getTitleSubtitle()Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;->component1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;->component2()Ljava/lang/String;

    move-result-object v3

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/signature/SignScreenData;

    .line 246
    iget-object v12, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v0, :cond_0

    .line 247
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v8, 0x0

    .line 248
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getHeaderInformation()Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ZILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    move-result-object v6

    const/4 v1, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1d

    const/4 v11, 0x0

    move-object v4, v0

    move-object v5, v8

    move-object v8, v1

    .line 247
    invoke-static/range {v4 .. v11}, Lcom/squareup/ui/buyer/signature/SignScreenData;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;ILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData;

    move-result-object v0

    .line 246
    invoke-virtual {v12, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final getTipOptionInfo(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;",
            ">;"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    const-string v1, "localeOverrideFactory"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/locale/LocaleOverrideFactory;->getPercentageFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    .line 167
    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/locale/LocaleOverrideFactory;->getShortMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 170
    check-cast p1, Ljava/lang/Iterable;

    .line 334
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    const/4 v3, 0x0

    .line 336
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_2

    .line 337
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_2
    check-cast v4, Lcom/squareup/protos/common/tipping/TipOption;

    .line 171
    iget-object v6, v4, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    if-eqz v6, :cond_3

    .line 173
    sget-object v6, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v4, v4, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    const-string/jumbo v7, "tipOption.percentage"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/squareup/util/Percentage$Companion;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_1

    .line 176
    :cond_3
    iget-object v4, v4, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 178
    :goto_1
    new-instance v6, Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;

    const-string v7, "labelText"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v6, v4, v3}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;-><init>(Ljava/lang/CharSequence;I)V

    invoke-interface {v2, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v3, v5

    goto :goto_0

    .line 338
    :cond_4
    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method private final getTitleSubtitle()Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;
    .locals 5

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 184
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    const-string v2, "localeOverrideFactory"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 183
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount(Lcom/squareup/text/Formatter;)Ljava/lang/String;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v1

    .line 189
    iget-object v3, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->hasNonZeroTip()Z

    move-result v3

    const-string v4, ""

    if-eqz v3, :cond_3

    .line 190
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 192
    iget-object v3, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    if-nez v3, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    if-nez v4, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v4}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v2

    .line 191
    invoke-virtual {v1, v3, v2}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 194
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 195
    :cond_3
    invoke-interface {v1}, Lcom/squareup/payment/AcceptsTips;->askForTip()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 196
    invoke-interface {v1}, Lcom/squareup/payment/AcceptsTips;->useSeparateTippingScreen()Z

    move-result v1

    if-nez v1, :cond_5

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTabletAtLeast10Inches()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 199
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/checkout/R$string;->buyer_add_a_tip:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 205
    :cond_5
    :goto_0
    new-instance v1, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;

    invoke-direct {v1, v0, v4}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private final initialSetup(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    .line 85
    iget-object v2, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    instance-of v3, v2, Lcom/squareup/payment/RequiresCardAgreement;

    const/4 v4, 0x0

    if-nez v3, :cond_0

    move-object v2, v4

    :cond_0
    check-cast v2, Lcom/squareup/payment/RequiresCardAgreement;

    if-eqz v2, :cond_6

    .line 89
    iget-object v3, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v3

    .line 90
    invoke-interface {v3}, Lcom/squareup/payment/AcceptsTips;->askForTip()Z

    move-result v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v5, :cond_1

    .line 91
    iget-object v5, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    invoke-virtual {v5}, Lcom/squareup/payment/TipDeterminerFactory;->createForCurrentTender()Lcom/squareup/payment/TipDeterminer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    .line 93
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->getTitleSubtitle()Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;->component1()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;->component2()Ljava/lang/String;

    move-result-object v8

    if-eqz v5, :cond_2

    .line 98
    invoke-interface {v3}, Lcom/squareup/payment/AcceptsTips;->useSeparateTippingScreen()Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v10, 0x1

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    .line 102
    :goto_1
    invoke-interface {v2}, Lcom/squareup/payment/RequiresCardAgreement;->getCard()Lcom/squareup/Card;

    move-result-object v11

    const-string v12, "cardPayment.card"

    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/squareup/Card;->isManual()Z

    move-result v11

    iput-boolean v11, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->doneButtonAlwaysEnabled:Z

    if-eqz v5, :cond_3

    .line 106
    invoke-interface {v3}, Lcom/squareup/payment/AcceptsTips;->useSeparateTippingScreen()Z

    move-result v5

    if-nez v5, :cond_3

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    .line 108
    :goto_2
    sget-object v5, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$NoTip;->INSTANCE:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$NoTip;

    check-cast v5, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    if-eqz v6, :cond_4

    const-string/jumbo v5, "tippingPayment"

    .line 110
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3}, Lcom/squareup/payment/AcceptsTips;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 111
    invoke-interface {v3}, Lcom/squareup/payment/AcceptsTips;->getTipOptions()Ljava/util/List;

    move-result-object v3

    const-string/jumbo v6, "tipOptions"

    .line 112
    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->getTipOptionInfo(Ljava/util/List;)Ljava/util/List;

    move-result-object v17

    .line 114
    new-instance v6, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    .line 115
    new-instance v7, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$1;

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    invoke-direct {v7, v11}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$1;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    move-object v14, v7

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 116
    new-instance v7, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;

    invoke-direct {v7, v0, v3}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Ljava/util/List;)V

    move-object v15, v7

    check-cast v15, Lkotlin/jvm/functions/Function1;

    .line 117
    new-instance v3, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$3;

    invoke-direct {v3, v11}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$3;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    move-object/from16 v16, v3

    check-cast v16, Lkotlin/jvm/functions/Function1;

    .line 118
    iget-object v3, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v18

    .line 119
    iget-object v3, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/squareup/tipping/TippingCalculator;->shouldAskForCustomAmount(Lcom/squareup/settings/server/TipSettings;Lcom/squareup/protos/common/Money;)Z

    move-result v19

    .line 120
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v20

    move-object v13, v6

    move-object/from16 v21, v5

    .line 114
    invoke-direct/range {v13 .. v21}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/List;ZZLcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    move-object v5, v6

    check-cast v5, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    :cond_4
    move-object/from16 v18, v5

    .line 127
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v14

    .line 128
    new-instance v15, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    .line 131
    new-instance v1, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$signScreenData$1;

    invoke-direct {v1, v0, v10}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$signScreenData$1;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Z)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 128
    invoke-direct {v15, v9, v8, v1, v10}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Z)V

    .line 134
    new-instance v1, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    .line 135
    new-instance v3, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$signScreenData$2;

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    invoke-direct {v3, v5}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$signScreenData$2;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 136
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->noSigSubmitSignatureHandler()Lkotlin/jvm/functions/Function0;

    move-result-object v6

    .line 141
    invoke-interface {v2}, Lcom/squareup/payment/RequiresCardAgreement;->getCard()Lcom/squareup/Card;

    move-result-object v7

    invoke-static {v7, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/squareup/Card;->isManual()Z

    move-result v7

    .line 134
    invoke-direct {v1, v3, v6, v4, v7}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Z)V

    .line 144
    iget-object v3, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->getReturnPolicy()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 145
    new-instance v3, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$signScreenData$3$1;

    invoke-direct {v3, v5}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$signScreenData$3$1;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    .line 144
    move-object v4, v3

    check-cast v4, Lkotlin/reflect/KFunction;

    :cond_5
    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 143
    new-instance v3, Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    invoke-direct {v3, v4, v2}, Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;-><init>(Lkotlin/jvm/functions/Function0;Lcom/squareup/payment/RequiresCardAgreement;)V

    .line 126
    new-instance v2, Lcom/squareup/ui/buyer/signature/SignScreenData;

    move-object v13, v2

    move-object/from16 v16, v1

    move-object/from16 v17, v3

    invoke-direct/range {v13 .. v18}, Lcom/squareup/ui/buyer/signature/SignScreenData;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;)V

    .line 152
    iget-object v1, v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 85
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Transaction must have a card agreement payment."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method private final noSigSubmitSignatureHandler()Lkotlin/jvm/functions/Function0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 158
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->doneButtonAlwaysEnabled:Z

    if-eqz v0, :cond_0

    .line 159
    new-instance v0, Lcom/squareup/ui/buyer/signature/SignScreenRunner$noSigSubmitSignatureHandler$1;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$noSigSubmitSignatureHandler$1;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final onCancelButtonPressed(Z)V
    .locals 1

    if-nez p1, :cond_0

    .line 314
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    return-void

    .line 317
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method private final setCustomTip(Ljava/lang/CharSequence;)V
    .locals 6

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-nez p1, :cond_0

    .line 269
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->clearTip()V

    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requirePayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 278
    :goto_0
    sget-object v1, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v2, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-double v4, v4

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/squareup/util/Percentage$Companion;->fromRate(D)Lcom/squareup/util/Percentage;

    move-result-object v0

    .line 279
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->TIP_CUSTOM_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 280
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    return-void
.end method

.method private final setSignature(Lcom/squareup/signature/SignatureAsJson;)V
    .locals 11

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireSignedPayment()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    .line 211
    invoke-interface {v0, p1}, Lcom/squareup/payment/AcceptsSignature;->setVectorSignature(Lcom/squareup/signature/SignatureAsJson;)V

    .line 214
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/buyer/signature/SignScreenData;

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v0, :cond_1

    .line 216
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 217
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getSignatureConfig()Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    move-result-object v3

    const/4 v4, 0x0

    .line 218
    new-instance v5, Lcom/squareup/ui/buyer/signature/SignScreenRunner$setSignature$1;

    move-object v6, p0

    check-cast v6, Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    invoke-direct {v5, v6}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$setSignature$1;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    move-object v7, v5

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 219
    new-instance v5, Lcom/squareup/ui/buyer/signature/SignScreenRunner$setSignature$2;

    invoke-direct {v5, v6}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$setSignature$2;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    const/4 v8, 0x0

    const/16 v9, 0x9

    const/4 v10, 0x0

    move-object v6, v7

    move v7, v8

    move v8, v9

    move-object v9, v10

    .line 217
    invoke-static/range {v3 .. v9}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ZILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    move-result-object v3

    const/4 v5, 0x0

    const/16 v6, 0x1b

    const/4 v7, 0x0

    .line 216
    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/buyer/signature/SignScreenData;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;ILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData;

    move-result-object v0

    .line 215
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
    .locals 11

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    .line 288
    invoke-interface {v0, p1, p2}, Lcom/squareup/payment/AcceptsTips;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    .line 289
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->getTitleSubtitle()Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;->component1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$TitleSubtitle;->component2()Ljava/lang/String;

    move-result-object v2

    .line 290
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/signature/SignScreenData;

    .line 291
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez p1, :cond_0

    .line 292
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v7, 0x0

    .line 293
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getHeaderInformation()Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ZILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    move-result-object v5

    const/4 v0, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1d

    const/4 v10, 0x0

    move-object v3, p1

    move-object v4, v7

    move-object v7, v0

    .line 292
    invoke-static/range {v3 .. v10}, Lcom/squareup/ui/buyer/signature/SignScreenData;->copy$default(Lcom/squareup/ui/buyer/signature/SignScreenData;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;ILjava/lang/Object;)Lcom/squareup/ui/buyer/signature/SignScreenData;

    move-result-object p1

    .line 291
    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final setTip(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;I)V"
        }
    .end annotation

    .line 260
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/tipping/TipOption;

    .line 261
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->TIP_SELECTED_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 262
    iget-object p2, p1, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    const-string/jumbo v0, "tipOption.tip_money"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object p1, p1, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    invoke-virtual {v0, p1}, Lcom/squareup/util/Percentage$Companion;->fromNullableDouble(Ljava/lang/Double;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    return-void
.end method

.method private final showReturnPolicy()V
    .locals 5

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    if-nez v0, :cond_0

    const-string v1, "localeOverrideFactory"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->buyer_refund_policy_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 303
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "settings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "business_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 305
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->flow:Lflow/Flow;

    .line 308
    new-instance v2, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;

    new-instance v3, Lcom/squareup/ui/buyer/signature/RefundPolicy;

    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v4}, Lcom/squareup/settings/server/AccountStatusSettings;->getReturnPolicy()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/squareup/ui/buyer/signature/RefundPolicy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;-><init>(Lcom/squareup/ui/buyer/signature/RefundPolicy;)V

    .line 307
    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final submitSignature()V
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->showReceiptScreen()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 2

    .line 69
    const-class v0, Lcom/squareup/ui/buyer/signature/SignScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SignScreen::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "buyerLocaleOverride.loca\u2026)\n        .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v1, Lcom/squareup/ui/buyer/signature/SignScreenRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown SignScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final screenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/buyer/signature/SignScreenData;",
            ">;"
        }
    .end annotation

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->viewDataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
