.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;
.super Ljava/lang/Object;
.source "SignLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;",
        "",
        "picassoCache",
        "Lcom/squareup/picasso/Cache;",
        "ohSnapLogger",
        "Lcom/squareup/log/OhSnapLogger;",
        "agreementBuilder",
        "Lcom/squareup/ui/buyer/signature/AgreementBuilder;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V",
        "getAgreementBuilder",
        "()Lcom/squareup/ui/buyer/signature/AgreementBuilder;",
        "getCurrencyCode",
        "()Lcom/squareup/protos/common/CurrencyCode;",
        "getMoneyLocaleHelper",
        "()Lcom/squareup/money/MoneyLocaleHelper;",
        "getOhSnapLogger",
        "()Lcom/squareup/log/OhSnapLogger;",
        "getPicassoCache",
        "()Lcom/squareup/picasso/Cache;",
        "create",
        "Lcom/squareup/ui/buyer/signature/SignLayoutRunner;",
        "view",
        "Landroid/view/View;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final agreementBuilder:Lcom/squareup/ui/buyer/signature/AgreementBuilder;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final picassoCache:Lcom/squareup/picasso/Cache;


# direct methods
.method public constructor <init>(Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "picassoCache"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ohSnapLogger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "agreementBuilder"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->picassoCache:Lcom/squareup/picasso/Cache;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->agreementBuilder:Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iput-object p5, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/ui/buyer/signature/SignLayoutRunner;
    .locals 8

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    .line 70
    iget-object v3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->picassoCache:Lcom/squareup/picasso/Cache;

    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    iget-object v5, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->agreementBuilder:Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    iget-object v6, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    .line 71
    iget-object v7, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, v0

    move-object v2, p1

    .line 69
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method public final getAgreementBuilder()Lcom/squareup/ui/buyer/signature/AgreementBuilder;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->agreementBuilder:Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    return-object v0
.end method

.method public final getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public final getMoneyLocaleHelper()Lcom/squareup/money/MoneyLocaleHelper;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    return-object v0
.end method

.method public final getOhSnapLogger()Lcom/squareup/log/OhSnapLogger;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-object v0
.end method

.method public final getPicassoCache()Lcom/squareup/picasso/Cache;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;->picassoCache:Lcom/squareup/picasso/Cache;

    return-object v0
.end method
