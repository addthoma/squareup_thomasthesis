.class final Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SignScreenRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignScreenRunner;->initialSetup(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "index",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $tipOptions:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;->this$0:Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;->$tipOptions:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;->this$0:Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner$initialSetup$2;->$tipOptions:Ljava/util/List;

    const-string/jumbo v2, "tipOptions"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->access$setTip(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Ljava/util/List;I)V

    return-void
.end method
