.class public interface abstract Lcom/squareup/ui/buyer/emv/EmvScope$Component;
.super Ljava/lang/Object;
.source "EmvScope.java"

# interfaces
.implements Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope$ParentComponent;
.implements Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialViewPager$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/buyer/emv/EmvScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract chooseEmvOptionCoordinator()Lcom/squareup/ui/buyer/emv/chooseapplication/ChooseEmvOptionCoordinator;
.end method

.method public abstract emvAuthorizing()Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Component;
.end method

.method public abstract emvFallbackWarning(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;)Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;
.end method

.method public abstract permissionPasscodeGatekeeper()Lcom/squareup/permissions/PermissionGatekeeper;
.end method

.method public abstract pinAuthorizing()Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Component;
.end method

.method public abstract pinPadDialog()Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$Component;
.end method

.method public abstract preparingPayment()Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Component;
.end method

.method public abstract readerInPaymentComponent()Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;
.end method

.method public abstract retryEmvTender()Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/buyer/emv/EmvScope$Runner;
.end method
