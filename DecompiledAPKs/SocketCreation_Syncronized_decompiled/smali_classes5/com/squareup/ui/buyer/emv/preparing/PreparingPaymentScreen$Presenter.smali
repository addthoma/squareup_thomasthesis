.class public Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;
.super Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;
.source "PreparingPaymentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation


# instance fields
.field private final emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

.field private final emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0, p5}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$PreparingPaymentScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 2

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    .line 54
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->showCardRemovalPrompt(ZZ)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->continueMonitoring()V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardPresenceRequired()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/preparing/-$$Lambda$PreparingPaymentScreen$Presenter$daEoW6DGGOprwR9d9hPgQoH9tQQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/preparing/-$$Lambda$PreparingPaymentScreen$Presenter$daEoW6DGGOprwR9d9hPgQoH9tQQ;-><init>(Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->hasDipPaymentStarted()Z

    move-result p1

    if-nez p1, :cond_0

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;->startPaymentOnActiveReader()V

    :cond_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 61
    invoke-super {p0, p1}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    .line 63
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->emv_prepare_payment:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->setSpinnerMessageText(I)V

    return-void
.end method
