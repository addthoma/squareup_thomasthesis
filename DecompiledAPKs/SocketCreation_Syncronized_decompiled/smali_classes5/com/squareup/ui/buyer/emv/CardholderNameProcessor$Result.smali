.class public final enum Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;
.super Ljava/lang/Enum;
.source "CardholderNameProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum ABORTED_PRE_AUTH:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum CARD_ERROR:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum DIP_APPLICATION:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum DIP_FETCHING:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum DIP_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum HARDWARE_PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum NFC_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

.field public static final enum PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 464
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v1, 0x0

    const-string v2, "DIP_SUCCESS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 465
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v2, 0x1

    const-string v3, "DIP_APPLICATION"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_APPLICATION:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 466
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v3, 0x2

    const-string v4, "CARD_ERROR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->CARD_ERROR:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 467
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v4, 0x3

    const-string v5, "DIP_FETCHING"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_FETCHING:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 468
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v5, 0x4

    const-string v6, "PIN_REQUESTED"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 469
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v6, 0x5

    const-string v7, "HARDWARE_PIN_REQUESTED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->HARDWARE_PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 470
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v7, 0x6

    const-string v8, "ABORTED_PRE_AUTH"

    invoke-direct {v0, v8, v7}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->ABORTED_PRE_AUTH:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 471
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/4 v8, 0x7

    const-string v9, "NFC_SUCCESS"

    invoke-direct {v0, v9, v8}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NFC_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 472
    new-instance v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/16 v9, 0x8

    const-string v10, "NONE"

    invoke-direct {v0, v10, v9}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    .line 463
    sget-object v10, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_APPLICATION:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->CARD_ERROR:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_FETCHING:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->HARDWARE_PIN_REQUESTED:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->ABORTED_PRE_AUTH:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NFC_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->$VALUES:[Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 463
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;
    .locals 1

    .line 463
    const-class v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;
    .locals 1

    .line 463
    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->$VALUES:[Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0}, [Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    return-object v0
.end method
