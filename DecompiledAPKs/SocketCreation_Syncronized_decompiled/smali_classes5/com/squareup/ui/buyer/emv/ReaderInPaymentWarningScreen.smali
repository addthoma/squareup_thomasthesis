.class public final Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "ReaderInPaymentWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/main/errors/ReaderWarningScreen;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/ui/buyer/emv/-$$Lambda$ReaderInPaymentWarningScreen$beaDBxlSxekH9GesXWalvS5OslE;->INSTANCE:Lcom/squareup/ui/buyer/emv/-$$Lambda$ReaderInPaymentWarningScreen$beaDBxlSxekH9GesXWalvS5OslE;

    .line 76
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    .line 37
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 38
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;->initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;
    .locals 2

    .line 77
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 78
    const-class v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    .line 79
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 78
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    .line 80
    new-instance v1, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;

    iget-object p0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V

    return-object v1
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 70
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/InEmvScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;->initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;->initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{warningType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;->initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    iget-object v1, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 84
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->reader_warning_view:I

    return v0
.end method
