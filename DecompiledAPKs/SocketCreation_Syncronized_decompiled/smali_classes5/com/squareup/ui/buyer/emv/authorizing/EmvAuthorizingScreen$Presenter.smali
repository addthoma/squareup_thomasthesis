.class public Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;
.super Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;
.source "EmvAuthorizingScreen.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation


# instance fields
.field private authorizeOnArrival:Z

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

.field private isContactless:Z

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    invoke-direct {p0, p5}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$EmvAuthorizingScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 2

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->isContactless:Z

    xor-int/lit8 v1, v1, 0x1

    .line 88
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->showCardRemovalPrompt(ZZ)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 74
    invoke-super {p0, p1}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 75
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    .line 76
    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->access$000(Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->authorizeOnArrival:Z

    .line 77
    iget-object v0, v0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-boolean v0, v0, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    iput-boolean v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->isContactless:Z

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->continueMonitoring()V

    .line 81
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->authorizeOnArrival:Z

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasAuthData()Z

    move-result v0

    const-string v1, "EmvAuthorizingScreen::onEnterScope with authorizeOnArrival doesn\'t have auth data"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardPresenceRequired()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$EmvAuthorizingScreen$Presenter$y2bkd39D9GATX1TfBWz-pX6borw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$EmvAuthorizingScreen$Presenter$y2bkd39D9GATX1TfBWz-pX6borw;-><init>(Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 92
    invoke-super {p0, p1}, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    .line 94
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->authorizeOnArrival:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen$Presenter;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->authorizeContactlessPayment()V

    .line 97
    :cond_0
    sget v0, Lcom/squareup/checkout/R$string;->buyer_authorizing:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->setSpinnerMessageText(I)V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method
