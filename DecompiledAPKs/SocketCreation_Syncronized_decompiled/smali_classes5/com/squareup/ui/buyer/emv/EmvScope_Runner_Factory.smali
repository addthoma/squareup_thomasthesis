.class public final Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;
.super Ljava/lang/Object;
.source "EmvScope_Runner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubScoperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/CardReaderHubScoper;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final currentSecureTouchModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;"
        }
    .end annotation
.end field

.field private final emvPathProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope;",
            ">;"
        }
    .end annotation
.end field

.field private final emvPaymentStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final emvProcessorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final secureTouchWorkflowLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/CardReaderHubScoper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 78
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvProcessorFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->cardReaderHubScoperProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvPathProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvPaymentStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->currentSecureTouchModeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->secureTouchWorkflowLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/CardReaderHubScoper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 117
    new-instance v18, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderInfo;Lflow/Flow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)Lcom/squareup/ui/buyer/emv/EmvScope$Runner;
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 128
    new-instance v18, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderInfo;Lflow/Flow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/EmvScope$Runner;
    .locals 19

    move-object/from16 v0, p0

    .line 99
    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvProcessorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->cardReaderHubScoperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvPathProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->emvPaymentStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->currentSecureTouchModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/securetouch/CurrentSecureTouchMode;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->secureTouchWorkflowLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    invoke-static/range {v2 .. v18}, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->newInstance(Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderInfo;Lflow/Flow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvScope_Runner_Factory;->get()Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    move-result-object v0

    return-object v0
.end method
