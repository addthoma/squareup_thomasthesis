.class public Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;
.super Ljava/lang/Object;
.source "PinPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Params"
.end annotation


# instance fields
.field final canSkip:Z

.field final cardInfo:Lcom/squareup/cardreader/CardInfo;

.field final isFinalRetry:Z

.field final isRetry:Z


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardInfo;ZZZ)V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    .line 87
    iput-boolean p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->canSkip:Z

    .line 88
    iput-boolean p3, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->isFinalRetry:Z

    .line 89
    iput-boolean p4, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->isRetry:Z

    return-void
.end method
