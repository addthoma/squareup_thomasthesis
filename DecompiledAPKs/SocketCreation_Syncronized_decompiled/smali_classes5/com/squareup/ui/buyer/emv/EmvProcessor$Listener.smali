.class public interface abstract Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;
.super Ljava/lang/Object;
.source "EmvProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onAuthorizing(Z)V
.end method

.method public abstract onCardError()V
.end method

.method public abstract onCardRemovedDuringPayment()V
.end method

.method public abstract onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
.end method

.method public abstract onListAccounts([Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
.end method

.method public abstract onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
.end method

.method public abstract onMagSwipeApproved()V
.end method

.method public abstract onPaymentApproved()V
.end method

.method public abstract onPaymentCanceled(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V
.end method

.method public abstract onPaymentDeclined(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V
.end method

.method public abstract onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V
.end method

.method public abstract onSwipeChipCardForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
.end method

.method public abstract onUseChipCardDuringFallback()V
.end method

.method public abstract retryableError()V
.end method
