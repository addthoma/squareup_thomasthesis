.class public Lcom/squareup/ui/buyer/emv/EmvScope$Runner;
.super Ljava/lang/Object;
.source "EmvScope.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lmortar/Scoped;
.implements Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Runner"
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final cardPresenceRequired:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

.field private final emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

.field private final emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

.field private final emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

.field private fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private isPinSubmitted:Z

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private nextScreen:Lcom/squareup/container/ContainerTreeKey;

.field private final pinListener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final secureTouchResultRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;"
        }
    .end annotation
.end field

.field private final secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderInfo;Lflow/Flow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v8, p0

    move-object/from16 v9, p14

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 139
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardPresenceRequired:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 140
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->secureTouchResultRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-object v0, p2

    .line 158
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-object v0, p4

    .line 159
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object/from16 v5, p5

    .line 160
    iput-object v5, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    move-object/from16 v0, p6

    .line 161
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->flow:Lflow/Flow;

    move-object/from16 v0, p7

    .line 162
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    move-object/from16 v0, p9

    .line 163
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    move-object/from16 v0, p10

    .line 164
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    move-object/from16 v0, p11

    .line 165
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    move-object/from16 v0, p12

    .line 166
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v7, p8

    .line 167
    iput-object v7, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    .line 168
    iput-object v9, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    move-object/from16 v0, p15

    .line 169
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    .line 170
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->NONE:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    move-object/from16 v0, p16

    .line 171
    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    move-object/from16 v6, p17

    .line 172
    iput-object v6, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    .line 174
    new-instance v10, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;

    move-object v0, v10

    move-object v1, p0

    move-object/from16 v2, p13

    move-object/from16 v3, p14

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$1;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V

    move-object v0, p1

    invoke-virtual {p1, v10}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->create(Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;)Lcom/squareup/ui/buyer/emv/EmvProcessor;

    move-result-object v0

    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 307
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;

    invoke-direct {v0, p0, v9}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/emv/EmvScope;)V

    iput-object v0, v8, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->pinListener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V
    .locals 0

    .line 123
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->replaceTo(Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V
    .locals 0

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->showEmvErrorScreen(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->pinListener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Z
    .locals 0

    .line 123
    iget-boolean p0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->isPinSubmitted:Z

    return p0
.end method

.method static synthetic access$402(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Z)Z
    .locals 0

    .line 123
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->isPinSubmitted:Z

    return p1
.end method

.method static synthetic access$500(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardPresenceRequired:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/container/ContainerTreeKey;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->nextScreen:Lcom/squareup/container/ContainerTreeKey;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/EmvProcessor;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lflow/Flow;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->flow:Lflow/Flow;

    return-object p0
.end method

.method private goTo(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    .line 369
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->isDialogScreen(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->flow:Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 372
    :cond_0
    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->replaceTo(Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V

    :goto_0
    return-void
.end method

.method private replaceTo(Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->flow:Lflow/Flow;

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method private showEmvErrorScreen(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V
    .locals 2

    .line 422
    new-instance v0, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V

    .line 423
    sget-object p1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->replaceTo(Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Direction;)V

    return-void
.end method


# virtual methods
.method public authorizeContactlessPayment()V
    .locals 1

    .line 427
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->processTenderInEdit()V

    return-void
.end method

.method public cancelPayment()V
    .locals 2

    .line 441
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelPayment()V

    goto :goto_0

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->dropPaymentOrTender(Z)V

    :goto_0
    return-void
.end method

.method public cardPresenceRequired()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardPresenceRequired:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public exit()V
    .locals 2

    .line 381
    new-instance v0, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/emv/CancelEmvPaymentScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public finishWithResult(Lcom/squareup/securetouch/SecureTouchResult;)V
    .locals 1

    .line 498
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->secureTouchResultRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method getFlow()Lflow/Flow;
    .locals 1

    .line 494
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->flow:Lflow/Flow;

    return-object v0
.end method

.method getPinListener()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 1

    .line 490
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->pinListener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    return-object v0
.end method

.method public goToPreparingPaymentScreen()V
    .locals 2

    .line 414
    new-instance v0, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$EmvScope$Runner(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->nextScreen:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 3

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 465
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->transaction:Lcom/squareup/payment/Transaction;

    .line 470
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderWithCaptureArgs()Z

    move-result v0

    if-nez v0, :cond_2

    .line 472
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    sget-object v2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 473
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 475
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 476
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lcom/squareup/cardreader/R$string;->contactless_reader_disconnected_title:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_title:I

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_msg:I

    .line 479
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 480
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 481
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_2
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$xrh1I4r_eGUgywtK2hAfNLNntGU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$xrh1I4r_eGUgywtK2hAfNLNntGU;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$3MBVolioqp0Ya0NzmHUHVGvJ14c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$3MBVolioqp0Ya0NzmHUHVGvJ14c;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->secureTouchResultRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$j3zzF3ehMb7MUixSU35ZpPa-xXI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$j3zzF3ehMb7MUixSU35ZpPa-xXI;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 348
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 349
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    iget-boolean v0, v0, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    invoke-interface {v1, v0}, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;->setUpEmvPayment(Z)V

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$Runner$Q80spXnOCmzVQZxv4a-ecgvEt_w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$EmvScope$Runner$Q80spXnOCmzVQZxv4a-ecgvEt_w;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)V

    .line 351
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 350
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method onSecureTouchResult(Lcom/squareup/securetouch/SecureTouchResult;)V
    .locals 3

    .line 449
    instance-of p1, p1, Lcom/squareup/securetouch/FinishedSecureTouchResult$Completed;

    if-eqz p1, :cond_0

    .line 450
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->flow:Lflow/Flow;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/squareup/container/WorkflowTreeKey;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 452
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cancelPayment()V

    :goto_0
    return-void
.end method

.method onSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 407
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Failed swipe in EMV Flow. Fallback type is %s."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 409
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iget-boolean p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->swipeStraight:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method onSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 389
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Successful swipe in EMV Flow. Fallback type is %s."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    if-eqz v0, :cond_2

    .line 394
    iget-object v0, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-virtual {v0}, Lcom/squareup/Card;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->NONE:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-ne v0, v1, :cond_0

    .line 399
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->submitLegacySwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    goto :goto_0

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->submitFallbackSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    :goto_0
    return-void

    .line 395
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Swiped card is not valid."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 392
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Swiped card is null!"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public reauthorize()V
    .locals 2

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->reauthorizeSmartCardTender(Lcom/squareup/payment/BillPayment;)V

    return-void
.end method

.method public restoreEmvProcessorAsPaymentCompletionListener()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->restoreAsPaymentCompletionListener()V

    return-void
.end method

.method public selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    .line 437
    new-instance p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {p1, v0}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 431
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvProcessor:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    .line 432
    new-instance p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {p1, v0}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goTo(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method setFallbackType(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 0

    .line 486
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    return-void
.end method
