.class Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;
.super Ljava/lang/Object;
.source "EmvScope.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/emv/EmvScope$Runner;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderInfo;Lflow/Flow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/emv/EmvScope;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/securetouch/CurrentSecureTouchMode;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

.field final synthetic val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->exit()V

    return-void
.end method

.method public onClear()V
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$800(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/EmvProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->onPinPadReset()V

    return-void
.end method

.method public onFocusLost()V
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$800(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/EmvProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelPayment()V

    return-void
.end method

.method public onPinEntered(I)V
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$800(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/EmvProcessor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->onPinDigitEntered(I)V

    return-void
.end method

.method public onSkip()V
    .locals 4

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$900(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lflow/Flow;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$800(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/EmvProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->onPinBypass()V

    return-void
.end method

.method public onSubmit()V
    .locals 4

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$402(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Z)Z

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$800(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/EmvProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->submitPinBlock()V

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->this$0:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    new-instance v1, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner$2;->val$emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-boolean v3, v2, Lcom/squareup/ui/buyer/emv/EmvScope;->isContactless:Z

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->access$300(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method
