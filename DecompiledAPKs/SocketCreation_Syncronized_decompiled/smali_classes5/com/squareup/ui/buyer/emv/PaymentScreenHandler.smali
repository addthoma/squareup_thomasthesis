.class public abstract Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.super Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
.source "PaymentScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;,
        Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;,
        Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;,
        Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;,
        Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field protected emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;->cancelPayment()V

    return-void
.end method

.method private cancelPayment()V
    .locals 3

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$2;-><init>(Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method


# virtual methods
.method protected cancelPaymentButton()Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 3

    .line 43
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->emv_cancel_payment:I

    new-instance v2, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$1;-><init>(Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method public handleBackPressed()Z
    .locals 1

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;->cancelPayment()V

    const/4 v0, 0x1

    return v0
.end method
