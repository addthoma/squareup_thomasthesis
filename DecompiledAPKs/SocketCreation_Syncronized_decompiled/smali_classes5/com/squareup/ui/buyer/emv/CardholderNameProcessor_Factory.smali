.class public final Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;
.super Ljava/lang/Object;
.source "CardholderNameProcessor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFetchInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final readerEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;"
        }
    .end annotation

    .line 71
    new-instance v9, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/log/ReaderEventLogger;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;
    .locals 10

    .line 78
    new-instance v9, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;-><init>(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/log/ReaderEventLogger;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->nameFetchInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/log/ReaderEventLogger;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/log/ReaderEventLogger;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor_Factory;->get()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    move-result-object v0

    return-object v0
.end method
