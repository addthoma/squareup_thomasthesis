.class public abstract Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Module;
.super Ljava/lang/Object;
.source "PreparingPaymentScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAbstractEmvProgressPresenter(Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen$Presenter;)Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
