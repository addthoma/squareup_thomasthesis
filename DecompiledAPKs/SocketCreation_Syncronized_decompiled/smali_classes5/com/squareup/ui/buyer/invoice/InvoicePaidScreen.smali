.class public Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "InvoicePaidScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final remainingBalance:Lcom/squareup/protos/common/Money;

.field final smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public final uniqueKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidScreen$zeeAqwx7JuFcun4SIkJkDHWoZCI;->INSTANCE:Lcom/squareup/ui/buyer/invoice/-$$Lambda$InvoicePaidScreen$zeeAqwx7JuFcun4SIkJkDHWoZCI;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, v0, v0, v0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->uniqueKey:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->remainingBalance:Lcom/squareup/protos/common/Money;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;
    .locals 1

    .line 49
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 50
    new-instance v0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 44
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 54
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->invoice_paid_view:I

    return v0
.end method
