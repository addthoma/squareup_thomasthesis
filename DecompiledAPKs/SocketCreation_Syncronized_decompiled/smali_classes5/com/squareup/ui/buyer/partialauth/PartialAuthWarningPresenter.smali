.class public Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;
.super Lmortar/ViewPresenter;
.source "PartialAuthWarningPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final lastAddedTender:Lcom/squareup/payment/tender/BaseCardTender;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/tender/BaseCardTender;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->lastAddedTender:Lcom/squareup/payment/tender/BaseCardTender;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private canChargeRemainingAmountAsNewTender()Z
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->splitTenderSupported()Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$9UHxsU-9-R1uZVvmhhACg9RyhAE(Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->localeUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method private localeUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->lastAddedTender:Lcom/squareup/payment/tender/BaseCardTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseCardTender;->getOriginalAmountInPartialAuth()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->lastAddedTender:Lcom/squareup/payment/tender/BaseCardTender;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseCardTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;

    .line 56
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v3

    .line 58
    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->setTotal(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 61
    iget-object v4, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->lastAddedTender:Lcom/squareup/payment/tender/BaseCardTender;

    invoke-virtual {v4}, Lcom/squareup/payment/tender/BaseCardTender;->getCardBrandResources()Lcom/squareup/text/CardBrandResources;

    move-result-object v4

    .line 62
    iget-object v5, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->lastAddedTender:Lcom/squareup/payment/tender/BaseCardTender;

    invoke-virtual {v5}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v5

    .line 63
    iget v4, v4, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    invoke-static {v0, v4, v5}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 65
    sget v5, Lcom/squareup/ui/buyerflow/R$string;->partial_auth_insufficient_funds:I

    invoke-interface {v0, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 66
    sget v6, Lcom/squareup/ui/buyerflow/R$string;->partial_auth_message3:I

    invoke-interface {v0, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    const-string v7, "brand_and_suffix"

    .line 67
    invoke-virtual {v6, v7, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 68
    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "auth_amount"

    invoke-virtual {v4, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 69
    invoke-virtual {v4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v4

    .line 70
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 71
    invoke-virtual {v2, v5, v4}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->setTitleAndMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->canChargeRemainingAmountAsNewTender()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 73
    sget v4, Lcom/squareup/ui/buyerflow/R$string;->partial_auth_charge_and_continue_with_amount:I

    invoke-interface {v0, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 74
    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 76
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {v2, v0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->setButtonText(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 79
    invoke-virtual {v2, v0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->setButtonVisible(Z)V

    .line 82
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object v0

    .line 83
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    .line 82
    invoke-virtual {v2, v0, p1}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->updateLanguageText(Lcom/squareup/locale/LocaleFormatter;Lcom/squareup/util/Res;)V

    return-void
.end method


# virtual methods
.method changeLanguageButtonClicked()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->showSelectBuyerLanguageScreen()V

    return-void
.end method

.method chargeAndContinue()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->finishPartialAuthWarningScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$0$PartialAuthWarningPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningPresenter$9UHxsU-9-R1uZVvmhhACg9RyhAE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningPresenter$9UHxsU-9-R1uZVvmhhACg9RyhAE;-><init>(Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method onBackPressed()Z
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 42
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningPresenter$BZ8j8sLesIe4K2Y24WjYJkQBpGk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningPresenter$BZ8j8sLesIe4K2Y24WjYJkQBpGk;-><init>(Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningView;->showLanguageSelection()V

    :cond_0
    return-void
.end method
