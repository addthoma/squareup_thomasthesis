.class public Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;
.super Ljava/lang/Object;
.source "PaymentIncompleteStatusBarNotifier.java"

# interfaces
.implements Lcom/squareup/notifications/PaymentIncompleteNotifier;


# static fields
.field static final EVENT_TRANSACTION_INCOMPLETE_SHOWN:Ljava/lang/String; = "Notification: transaction incomplete"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appContext:Landroid/app/Application;

.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;


# direct methods
.method constructor <init>(Landroid/app/NotificationManager;Landroid/app/Application;Lcom/squareup/analytics/Analytics;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->appContext:Landroid/app/Application;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->analytics:Lcom/squareup/analytics/Analytics;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method


# virtual methods
.method public hideNotification()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/payment/notifiers/impl/R$id;->notification_incomplete_transaction:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public showNotification()V
    .locals 6

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Notification: transaction incomplete"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->appContext:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->appContext:Landroid/app/Application;

    invoke-static {v1}, Lcom/squareup/ui/PaymentActivity;->createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 53
    iget-object v2, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v3, Lcom/squareup/payment/notifiers/impl/R$string;->notification_incomplete_transaction_text:I

    .line 54
    invoke-interface {v2, v3}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 55
    iget-object v3, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v4, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->appContext:Landroid/app/Application;

    sget-object v5, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    invoke-virtual {v3, v4, v5}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v3

    sget v4, Lcom/squareup/payment/notifiers/impl/R$string;->notification_incomplete_transaction_title:I

    .line 56
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v3

    sget v4, Lcom/squareup/payment/notifiers/impl/R$string;->notification_incomplete_transaction_title:I

    .line 57
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v2}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v3, 0x2

    .line 59
    invoke-virtual {v0, v3}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    new-instance v1, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 61
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 64
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x22

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;->notificationManager:Landroid/app/NotificationManager;

    sget v2, Lcom/squareup/payment/notifiers/impl/R$id;->notification_incomplete_transaction:I

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
