.class public interface abstract Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;
.super Ljava/lang/Object;
.source "RetryTenderPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RetryTenderStrategy"
.end annotation


# virtual methods
.method public abstract checkBillPaymentForOfflineModeButton()Z
.end method

.method public abstract confirmCancelPayment()V
.end method

.method public abstract maybeAutomaticallyRetryInOfflineMode()V
.end method

.method public abstract onEnterOfflineMode()V
.end method

.method public abstract retryPayment()V
.end method
