.class Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;
.super Ljava/lang/Object;
.source "LoyaltyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Enrollment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    }
.end annotation


# instance fields
.field final claimHelpText:Ljava/lang/String;

.field final claimText:Ljava/lang/String;

.field final defaultPhoneNumber:Ljava/lang/String;

.field final enrollmentLoyaltyStatus:Ljava/lang/String;

.field final phoneEditHint:Ljava/lang/String;

.field final phoneNumber:Ljava/lang/String;

.field final programName:Ljava/lang/String;

.field final receiptPhoneNumber:Ljava/lang/String;

.field final rewardRequirementText:Ljava/lang/String;

.field final rewardTiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation
.end field

.field final rewardTiersLoyaltyStatus:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V
    .locals 1

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$000(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->defaultPhoneNumber:Ljava/lang/String;

    .line 184
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$100(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->receiptPhoneNumber:Ljava/lang/String;

    .line 185
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$200(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneNumber:Ljava/lang/String;

    .line 186
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$300(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->programName:Ljava/lang/String;

    .line 187
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$400(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardTiersLoyaltyStatus:Ljava/lang/String;

    .line 188
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$500(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->enrollmentLoyaltyStatus:Ljava/lang/String;

    .line 189
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$600(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardRequirementText:Ljava/lang/String;

    .line 190
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$700(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneEditHint:Ljava/lang/String;

    .line 191
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$800(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->claimText:Ljava/lang/String;

    .line 192
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$900(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->claimHelpText:Ljava/lang/String;

    .line 193
    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->access$1000(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardTiers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 2

    .line 197
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->defaultPhoneNumber:Ljava/lang/String;

    .line 198
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->defaultPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->receiptPhoneNumber:Ljava/lang/String;

    .line 199
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->receiptPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneNumber:Ljava/lang/String;

    .line 200
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->programName:Ljava/lang/String;

    .line 201
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->programName(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardTiersLoyaltyStatus:Ljava/lang/String;

    .line 202
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiersLoyaltyStatus(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->enrollmentLoyaltyStatus:Ljava/lang/String;

    .line 203
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->enrollmentLoyaltyStatus(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardRequirementText:Ljava/lang/String;

    .line 204
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardRequirementText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneEditHint:Ljava/lang/String;

    .line 205
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneEditHint(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->claimText:Ljava/lang/String;

    .line 206
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->claimHelpText:Ljava/lang/String;

    .line 207
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimHelpText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardTiers:Ljava/util/List;

    .line 208
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiers(Ljava/util/List;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;

    move-result-object v0

    return-object v0
.end method
