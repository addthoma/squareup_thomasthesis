.class public final Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;
.super Ljava/lang/Object;
.source "LoyaltyView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/buyer/loyalty/LoyaltyView;",
        ">;"
    }
.end annotation


# instance fields
.field private final curatedImageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumberScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->curatedImageProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyView;",
            ">;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectCuratedImage(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/merchantimages/CuratedImage;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPhoneNumberScrubber(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/lang/Object;)V
    .locals 0

    .line 59
    check-cast p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Ljava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/InsertingScrubber;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->injectPhoneNumberScrubber(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/text/InsertingScrubber;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->curatedImageProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/merchantimages/CuratedImage;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->injectCuratedImage(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/merchantimages/CuratedImage;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->injectRes(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/util/Res;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->injectDevice(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView_MembersInjector;->injectMembers(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    return-void
.end method
