.class Lcom/squareup/ui/buyer/loyalty/LoyaltyView$5;
.super Ljava/lang/Object;
.source "LoyaltyView.java"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->lambda$setBackgroundImage$7(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V
    .locals 0

    .line 410
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$5;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Failed to load merchant image"

    .line 416
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSuccess()V
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$5;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-static {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->access$100(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    return-void
.end method
