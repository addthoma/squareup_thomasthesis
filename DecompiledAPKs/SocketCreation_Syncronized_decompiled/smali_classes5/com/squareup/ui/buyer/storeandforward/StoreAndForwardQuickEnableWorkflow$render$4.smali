.class final Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "StoreAndForwardQuickEnableWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->render(Lkotlin/Unit;Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "+",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
        "hasPermission",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;->this$0:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableState;",
            "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$StoreAndForwardQuickEnableResult;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;->this$0:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    invoke-static {p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->access$getAnalytics$p(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)Lcom/squareup/analytics/StoreAndForwardAnalytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/StoreAndForwardAnalytics$State;->ENABLED:Lcom/squareup/analytics/StoreAndForwardAnalytics$State;

    invoke-virtual {p1, v0}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logSettingState(Lcom/squareup/analytics/StoreAndForwardAnalytics$State;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;->this$0:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    invoke-static {p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->access$getSettings$p(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->setEnabled(Ljava/lang/Boolean;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;->this$0:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    invoke-static {p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;->access$getOfflineModeMonitor$p(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;)Lcom/squareup/payment/OfflineModeMonitor;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/payment/OfflineModeMonitor;->enterOfflineMode()V

    .line 72
    sget-object p1, Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;->INSTANCE:Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Granted;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 74
    :cond_0
    sget-object p1, Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Denied;->INSTANCE:Lcom/squareup/ui/buyer/storeandforward/RequestingPermissionAction$Denied;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow$render$4;->invoke(Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
