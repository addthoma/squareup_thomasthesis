.class public Lcom/squareup/ui/buyer/TabletTipButton;
.super Landroid/widget/FrameLayout;
.source "TabletTipButton.java"


# instance fields
.field private final bottomLine:Landroid/widget/TextView;

.field private final topLine:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    sget p2, Lcom/squareup/ui/buyerflow/R$layout;->tablet_tip_button_contents:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/buyer/TabletTipButton;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 19
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->top_line:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/TabletTipButton;->topLine:Landroid/widget/TextView;

    .line 20
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->bottom_line:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/TabletTipButton;->bottomLine:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/buyer/TabletTipButton;->topLine:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    iget-object p1, p0, Lcom/squareup/ui/buyer/TabletTipButton;->bottomLine:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/buyer/TabletTipButton;->topLine:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    iget-object p1, p0, Lcom/squareup/ui/buyer/TabletTipButton;->bottomLine:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    iget-object p1, p0, Lcom/squareup/ui/buyer/TabletTipButton;->bottomLine:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
