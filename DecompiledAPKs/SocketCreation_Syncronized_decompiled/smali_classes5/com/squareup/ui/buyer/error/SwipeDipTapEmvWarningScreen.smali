.class public abstract Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "SwipeDipTapEmvWarningScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/ModalBodyScreen;
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;,
        Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;,
        Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$ComponentFactory;
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-void
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 64
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/InEmvScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;->getParentKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected abstract getInitialScreenData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/main/errors/WarningCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/main/errors/WarningCoordinator;
    .locals 2

    .line 51
    const-class v0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;

    .line 52
    new-instance v0, Lcom/squareup/ui/main/errors/WarningCoordinator;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;->workflow()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;->mainScheduler()Lrx/Scheduler;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/main/errors/WarningCoordinator;-><init>(Lcom/squareup/ui/main/errors/WarningWorkflow;Lrx/Scheduler;)V

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 56
    const-class v0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;->workflow()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->register(Lmortar/MortarScope;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->warning_view:I

    return v0
.end method
