.class public final Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;
.super Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;
.source "EmvConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmvTechnicalFallback"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/ui/buyer/error/-$$Lambda$EmvConcreteWarningScreens$EmvTechnicalFallback$ijfC1JAHaLPsizOhQsEZ3wviCUU;->INSTANCE:Lcom/squareup/ui/buyer/error/-$$Lambda$EmvConcreteWarningScreens$EmvTechnicalFallback$ijfC1JAHaLPsizOhQsEZ3wviCUU;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 54
    new-instance v0, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/error/EmvConcreteWarningScreens$EmvTechnicalFallback;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-object v0
.end method


# virtual methods
.method protected getInitialScreenData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 2

    .line 44
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->EMV_SESSION_CANCEL_PAYMENT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->emv_cancel_payment:I

    .line 45
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 46
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->emv_tech_fallback_msg:I

    .line 47
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->emv_fallback_title:I

    .line 48
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
