.class public Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;
.super Lmortar/ViewPresenter;
.source "PaymentErrorPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/error/PaymentErrorView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private screen:Lcom/squareup/ui/buyer/error/PaymentErrorScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    return-void
.end method


# virtual methods
.method onBackPressed()Z
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->screen:Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    iget-boolean v1, v1, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->authFailed:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 38
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    iput-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->screen:Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerDefaultEmvCardInsertRemoveProcessor(Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/error/PaymentErrorView;

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/error/PaymentErrorView;->setTotal(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/error/PaymentErrorView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->screen:Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    iget-object v0, v0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/error/PaymentErrorView;->setSpinnerTitleText(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/PaymentErrorPresenter;->screen:Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    iget-object v0, v0, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/error/PaymentErrorView;->setSpinnerMessageText(Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/error/PaymentErrorView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method
