.class public Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "SwipeDipTapEmvWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 70
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    .line 71
    check-cast p2, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;

    .line 72
    new-instance v0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;-><init>(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;)V

    .line 73
    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Component;->emvFallbackWarning(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;)Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Component;

    move-result-object p1

    return-object p1
.end method
