.class public interface abstract Lcom/squareup/ui/inventory/AdjustInventoryStarter;
.super Ljava/lang/Object;
.source "AdjustInventoryStarter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001JN\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\t2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\tH&JC\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\t2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u00132\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\'\u00a2\u0006\u0002\u0010\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/inventory/AdjustInventoryStarter;",
        "",
        "startAdjustInventoryFlow",
        "",
        "parentPath",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "hasServerStockCount",
        "",
        "variationId",
        "",
        "variationMerchantCatalogToken",
        "currentCount",
        "Ljava/math/BigDecimal;",
        "currentCost",
        "Lcom/squareup/protos/common/Money;",
        "precision",
        "",
        "unitAbbreviation",
        "startAdjustInventoryFlowWithWholeNumberCount",
        "",
        "(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/common/Money;)V",
        "adjust-inventory_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract startAdjustInventoryFlow(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V
.end method

.method public abstract startAdjustInventoryFlowWithWholeNumberCount(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/common/Money;)V
    .annotation runtime Lkotlin/Deprecated;
        message = "Deprecated in favor startAdjustInventoryFlow, which accepts decimal counts"
    .end annotation
.end method
