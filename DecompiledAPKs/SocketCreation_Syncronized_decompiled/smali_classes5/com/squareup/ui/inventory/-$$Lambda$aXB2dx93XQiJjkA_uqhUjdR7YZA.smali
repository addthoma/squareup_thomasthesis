.class public final synthetic Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;

    invoke-direct {v0}, Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;-><init>()V

    sput-object v0, Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;->INSTANCE:Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 2

    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAppliedServerVersion()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method
