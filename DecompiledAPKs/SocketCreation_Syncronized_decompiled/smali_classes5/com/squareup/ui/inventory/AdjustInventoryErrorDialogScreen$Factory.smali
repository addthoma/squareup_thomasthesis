.class public Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen$Factory;
.super Ljava/lang/Object;
.source "AdjustInventoryErrorDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/res/Resources;Landroid/content/Context;Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;)Landroid/app/Dialog;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 30
    iget v0, p3, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;->titleResId:I

    .line 31
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p3, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;->messageResId:I

    .line 32
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 33
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/squareup/cardreader/ui/R$string;->try_again:I

    .line 34
    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 30
    invoke-static {v0, v1, v2, p0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/inventory/-$$Lambda$rAYw2zeIF57AeNTbOUynMJfra20;

    invoke-direct {v0, p2}, Lcom/squareup/ui/inventory/-$$Lambda$rAYw2zeIF57AeNTbOUynMJfra20;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$LoxMRBIgkBAoITZuhFp9c4YzUio;

    invoke-direct {v1, p2, p3}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$LoxMRBIgkBAoITZuhFp9c4YzUio;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;)V

    .line 35
    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;)V
    .locals 0

    .line 37
    iget-object p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;->request:Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController;->onRetryInErrorDialog(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 24
    const-class v0, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;

    .line 25
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;

    .line 26
    invoke-interface {v0}, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;->adjustInventoryController()Lcom/squareup/ui/inventory/AdjustInventoryController;

    move-result-object v0

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 29
    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryErrorDialogScreenData()Lrx/Observable;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;

    invoke-direct {v3, v1, p1, v0}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;-><init>(Landroid/content/res/Resources;Landroid/content/Context;Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    .line 30
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
