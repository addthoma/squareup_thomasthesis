.class synthetic Lcom/squareup/ui/help/HelpAppletScopeRunner$2;
.super Ljava/lang/Object;
.source "HelpAppletScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpAppletScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$analytics$RegisterTapName:[I

.field static final synthetic $SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 491
    invoke-static {}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->values()[Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v2, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v2}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v3, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->MAGSTRIPE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v3}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v4, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->LEGACY:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v4}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v5, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->NONE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v5}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 363
    :catch_3
    invoke-static {}, Lcom/squareup/analytics/RegisterTapName;->values()[Lcom/squareup/analytics/RegisterTapName;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    :try_start_4
    sget-object v4, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v5}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v4, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_WHATS_NEW:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v4}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v4

    aput v1, v0, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FIRST_PAYMENT:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_R12_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_R6_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_INVOICE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_CREATE_ITEM_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_ENROLL_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_ADJUST_POINTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_REDEEM_REWARDS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    return-void
.end method
