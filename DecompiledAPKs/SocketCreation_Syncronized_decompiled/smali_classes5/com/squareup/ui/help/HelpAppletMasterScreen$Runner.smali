.class public interface abstract Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;
.super Ljava/lang/Object;
.source "HelpAppletMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpAppletMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract bannerButtonObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/onboarding/BannerButton;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onBannerClicked(Ljava/lang/String;)V
.end method
