.class public final Lcom/squareup/ui/help/RealHelpBadge;
.super Ljava/lang/Object;
.source "RealHelpBadge.kt"

# interfaces
.implements Lcom/squareup/ui/help/HelpBadge;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealHelpBadge.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealHelpBadge.kt\ncom/squareup/ui/help/RealHelpBadge\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,158:1\n73#2:159\n119#2,4:160\n92#2,4:164\n*E\n*S KotlinDebug\n*F\n+ 1 RealHelpBadge.kt\ncom/squareup/ui/help/RealHelpBadge\n*L\n81#1:159\n122#1,4:160\n133#1,4:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001Bw\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000e\u0008\u0001\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000e\u0008\u0001\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000e\u0008\u0001\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u000e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u001cH\u0002J\u000e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00170\u001eH\u0016J\u0010\u0010!\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020#H\u0016J\u0008\u0010$\u001a\u00020\u001cH\u0016J\u0010\u0010%\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0017H\u0002J\u0008\u0010&\u001a\u00020\u001cH\u0016J\u0010\u0010\'\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020#H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u00170\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001a\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u00170\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001b\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u001c0\u001c0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/help/RealHelpBadge;",
        "Lcom/squareup/ui/help/HelpBadge;",
        "whatsNewBadge",
        "Lcom/squareup/tour/WhatsNewBadge;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "firstInvoiceTutorialViewed",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "loyaltyEnrollTutorialViewed",
        "loyaltyAdjustPointsTutorialViewed",
        "loyaltyRedeemRewardsTutorialViewed",
        "helpAppletSectionsList",
        "Lcom/squareup/ui/help/HelpAppletSectionsList;",
        "helpAppletSettings",
        "Lcom/squareup/ui/help/api/HelpAppletSettings;",
        "tutorialsSections",
        "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
        "(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/tutorials/TutorialsSections;)V",
        "helpBadgeCount",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "includeUnseenWhatsNewItemsInTutorialsCount",
        "newTutorialsCount",
        "refreshRequested",
        "",
        "count",
        "Lio/reactivex/Observable;",
        "maybeDismissBadges",
        "newTutorials",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onSettingsChanged",
        "refresh",
        "setupNewTutorialsCount",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final firstInvoiceTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletSectionsList:Lcom/squareup/ui/help/HelpAppletSectionsList;

.field private final helpBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private includeUnseenWhatsNewItemsInTutorialsCount:Z

.field private final loyaltyAdjustPointsTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyEnrollTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyRedeemRewardsTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final newTutorialsCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialsSections:Lcom/squareup/ui/help/tutorials/TutorialsSections;

.field private final whatsNewBadge:Lcom/squareup/tour/WhatsNewBadge;


# direct methods
.method public constructor <init>(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpAppletSectionsList;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/tutorials/TutorialsSections;)V
    .locals 1
    .param p4    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/FirstInvoiceTutorialViewed;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LoyaltyEnrollTutorialViewed;
        .end annotation
    .end param
    .param p6    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LoyaltyAdjustPointsTutorialViewed;
        .end annotation
    .end param
    .param p7    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LoyaltyRedeemRewardsTutorialViewed;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tour/WhatsNewBadge;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/help/HelpAppletSectionsList;",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "whatsNewBadge"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstInvoiceTutorialViewed"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyEnrollTutorialViewed"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyAdjustPointsTutorialViewed"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyRedeemRewardsTutorialViewed"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helpAppletSectionsList"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helpAppletSettings"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialsSections"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpBadge;->whatsNewBadge:Lcom/squareup/tour/WhatsNewBadge;

    iput-object p2, p0, Lcom/squareup/ui/help/RealHelpBadge;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/ui/help/RealHelpBadge;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/ui/help/RealHelpBadge;->firstInvoiceTutorialViewed:Lcom/squareup/settings/LocalSetting;

    iput-object p5, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyEnrollTutorialViewed:Lcom/squareup/settings/LocalSetting;

    iput-object p6, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyAdjustPointsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    iput-object p7, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyRedeemRewardsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    iput-object p8, p0, Lcom/squareup/ui/help/RealHelpBadge;->helpAppletSectionsList:Lcom/squareup/ui/help/HelpAppletSectionsList;

    iput-object p10, p0, Lcom/squareup/ui/help/RealHelpBadge;->tutorialsSections:Lcom/squareup/ui/help/tutorials/TutorialsSections;

    .line 46
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Int>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpBadge;->newTutorialsCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 47
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p3, "BehaviorRelay.createDefault(Unit)"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpBadge;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 48
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpBadge;->helpBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 54
    invoke-interface {p9}, Lcom/squareup/ui/help/api/HelpAppletSettings;->helpAppletTutorials()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/help/HelpAppletContent;

    .line 55
    instance-of p2, p2, Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    .line 56
    iput-boolean p2, p0, Lcom/squareup/ui/help/RealHelpBadge;->includeUnseenWhatsNewItemsInTutorialsCount:Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static final synthetic access$onSettingsChanged(Lcom/squareup/ui/help/RealHelpBadge;I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/RealHelpBadge;->onSettingsChanged(I)V

    return-void
.end method

.method private final maybeDismissBadges()V
    .locals 3

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISMISS_TUTORIAL_BADGES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->firstInvoiceTutorialViewed:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyEnrollTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyAdjustPointsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyRedeemRewardsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private final onSettingsChanged(I)V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->firstInvoiceTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Booleans;->toInteger(Z)I

    move-result v0

    add-int/2addr p1, v0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_TUTORIALS_ENROLLMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyEnrollTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Booleans;->toInteger(Z)I

    move-result v0

    add-int/2addr p1, v0

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyAdjustPointsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Booleans;->toInteger(Z)I

    move-result v0

    add-int/2addr p1, v0

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->loyaltyRedeemRewardsTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Booleans;->toInteger(Z)I

    move-result v0

    add-int/2addr p1, v0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->newTutorialsCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final setupNewTutorialsCount(Lmortar/MortarScope;)V
    .locals 6

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->tutorialsSections:Lcom/squareup/ui/help/tutorials/TutorialsSections;

    .line 119
    invoke-virtual {v0}, Lcom/squareup/ui/help/tutorials/TutorialsSections;->tutorialsActivationScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 120
    sget-object v1, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$activationCount$1;->INSTANCE:Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$activationCount$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 121
    iget-boolean v1, p0, Lcom/squareup/ui/help/RealHelpBadge;->includeUnseenWhatsNewItemsInTutorialsCount:Z

    const-string v2, "Observables.combineLates\u2026{ onSettingsChanged(it) }"

    const-string v3, "activationCount"

    const-string v4, "accountStatusSettings.settingsAvailable()"

    if-eqz v1, :cond_0

    .line 122
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/help/RealHelpBadge;->whatsNewBadge:Lcom/squareup/tour/WhatsNewBadge;

    invoke-virtual {v1}, Lcom/squareup/tour/WhatsNewBadge;->unseenWhatsNewItems()Lio/reactivex/Observable;

    move-result-object v1

    const-string/jumbo v5, "whatsNewBadge.unseenWhatsNewItems()"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v5, p0, Lcom/squareup/ui/help/RealHelpBadge;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v5}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v5

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iget-object v4, p0, Lcom/squareup/ui/help/RealHelpBadge;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v4, Lio/reactivex/Observable;

    .line 126
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    check-cast v1, Lio/reactivex/ObservableSource;

    check-cast v5, Lio/reactivex/ObservableSource;

    check-cast v4, Lio/reactivex/ObservableSource;

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 162
    new-instance v3, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$$inlined$combineLatest$1;

    invoke-direct {v3}, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$$inlined$combineLatest$1;-><init>()V

    check-cast v3, Lio/reactivex/functions/Function4;

    .line 160
    invoke-static {v1, v5, v4, v0, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ion(t1, t2, t3, t4) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    new-instance v1, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$2;-><init>(Lcom/squareup/ui/help/RealHelpBadge;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    goto :goto_0

    .line 133
    :cond_0
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/help/RealHelpBadge;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-object v4, p0, Lcom/squareup/ui/help/RealHelpBadge;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v4, Lio/reactivex/Observable;

    .line 136
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    check-cast v1, Lio/reactivex/ObservableSource;

    check-cast v4, Lio/reactivex/ObservableSource;

    check-cast v0, Lio/reactivex/ObservableSource;

    .line 166
    new-instance v3, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$$inlined$combineLatest$2;

    invoke-direct {v3}, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$$inlined$combineLatest$2;-><init>()V

    check-cast v3, Lio/reactivex/functions/Function3;

    .line 164
    invoke-static {v1, v4, v0, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026unction(t1, t2, t3) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    new-instance v1, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/RealHelpBadge$setupNewTutorialsCount$4;-><init>(Lcom/squareup/ui/help/RealHelpBadge;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public count()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->helpBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public newTutorials()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->newTutorialsCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "newTutorialsCount.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Lcom/squareup/ui/help/RealHelpBadge;->maybeDismissBadges()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/help/RealHelpBadge;->helpAppletSectionsList:Lcom/squareup/ui/help/HelpAppletSectionsList;

    invoke-virtual {v1}, Lcom/squareup/ui/help/HelpAppletSectionsList;->getVisibleEntries()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/applet/AppletSectionsListEntry;

    if-eqz v3, :cond_1

    .line 67
    check-cast v3, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    .line 68
    invoke-virtual {v3}, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;->badgeCount()Lio/reactivex/Observable;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-virtual {v3}, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v3

    instance-of v3, v3, Lcom/squareup/ui/help/tutorials/TutorialsSection;

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 67
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.ui.help.HelpAppletSectionsListEntry"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    if-eqz v2, :cond_3

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/RealHelpBadge;->setupNewTutorialsCount(Lmortar/MortarScope;)V

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/help/RealHelpBadge;->newTutorialsCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/help/RealHelpBadge;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 81
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 159
    check-cast v0, Ljava/lang/Iterable;

    new-instance v1, Lcom/squareup/ui/help/RealHelpBadge$onEnterScope$$inlined$combineLatest$1;

    invoke-direct {v1}, Lcom/squareup/ui/help/RealHelpBadge$onEnterScope$$inlined$combineLatest$1;-><init>()V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-static {v0, v1}, Lio/reactivex/Observable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ements.map { it as T }) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/squareup/ui/help/RealHelpBadge;->helpBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026subscribe(helpBadgeCount)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    :cond_4
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public refresh()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 108
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpBadge;->refreshRequested:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
