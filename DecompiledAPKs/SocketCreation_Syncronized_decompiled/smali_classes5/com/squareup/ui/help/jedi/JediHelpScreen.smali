.class public final Lcom/squareup/ui/help/jedi/JediHelpScreen;
.super Lcom/squareup/ui/help/jedi/InJediHelpScope;
.source "JediHelpScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediHelpScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediHelpScreen.kt\ncom/squareup/ui/help/jedi/JediHelpScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,64:1\n43#2:65\n24#3,4:66\n*E\n*S KotlinDebug\n*F\n+ 1 JediHelpScreen.kt\ncom/squareup/ui/help/jedi/JediHelpScreen\n*L\n34#1:65\n52#1,4:66\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \'2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001\'B#\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0014J\u0008\u0010\u001e\u001a\u00020\u001fH\u0016J\u0008\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0016J\u0008\u0010&\u001a\u00020\u001dH\u0016R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u0018\u0010\u0014\u001a\u0006\u0012\u0002\u0008\u00030\u00158VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/help/jedi/JediHelpScreen;",
        "Lcom/squareup/ui/help/jedi/InJediHelpScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "input",
        "Lcom/squareup/ui/help/jedi/JediHelpInput;",
        "firstScreen",
        "",
        "screenData",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "(Lcom/squareup/ui/help/jedi/JediHelpInput;ZLcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V",
        "getFirstScreen",
        "()Z",
        "getInput",
        "()Lcom/squareup/ui/help/jedi/JediHelpInput;",
        "getScreenData",
        "()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "setScreenData",
        "(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "getName",
        "",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/jedi/JediHelpScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;


# instance fields
.field private final firstScreen:Z

.field private final input:Lcom/squareup/ui/help/jedi/JediHelpInput;

.field private screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->Companion:Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;

    .line 66
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/help/jedi/JediHelpScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 69
    sput-object v0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpInput;ZLcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/help/jedi/InJediHelpScope;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->input:Lcom/squareup/ui/help/jedi/JediHelpInput;

    iput-boolean p2, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->firstScreen:Z

    iput-object p3, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpInput;ZLcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 19
    check-cast p3, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/help/jedi/JediHelpScreen;-><init>(Lcom/squareup/ui/help/jedi/JediHelpInput;ZLcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V

    return-void
.end method

.method public static final firstScreen()Lcom/squareup/ui/help/jedi/JediHelpScreen;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->Companion:Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jedi/JediHelpScreen$Companion;->firstScreen()Lcom/squareup/ui/help/jedi/JediHelpScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/help/jedi/InJediHelpScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->input:Lcom/squareup/ui/help/jedi/JediHelpInput;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 47
    iget-boolean p2, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->firstScreen:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_HELP_JEDI:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public final getFirstScreen()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->firstScreen:Z

    return v0
.end method

.method public final getInput()Lcom/squareup/ui/help/jedi/JediHelpInput;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->input:Lcom/squareup/ui/help/jedi/JediHelpInput;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .line 38
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-super {p0}, Lcom/squareup/ui/help/jedi/InJediHelpScope;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->input:Lcom/squareup/ui/help/jedi/JediHelpInput;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%s: {input=%s}"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(format, *args)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 23
    const-class v0, Lcom/squareup/ui/help/help/HelpSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    const-class v0, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;

    .line 34
    invoke-interface {p1}, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;->jediHelpCoordinator()Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    move-result-object p1

    const-string/jumbo v0, "view.component<JediHelpS\u2026>().jediHelpCoordinator()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 26
    sget v0, Lcom/squareup/applet/help/R$layout;->jedi_help_section:I

    return v0
.end method

.method public final setScreenData(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScreen;->screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    return-void
.end method
