.class final Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$2;
.super Ljava/lang/Object;
.source "JediHelpScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/protos/jedi/service/PanelRequest;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$2;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/protos/jedi/service/PanelRequest;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/jedi/service/PanelRequest;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/jedi/service/PanelResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$2;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->access$getJediService$p(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;)Lcom/squareup/server/help/JediService;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/server/help/JediService;->getPanel(Lcom/squareup/protos/jedi/service/PanelRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;

    move-result-object p1

    .line 301
    invoke-virtual {p1}, Lcom/squareup/server/help/JediService$JediServiceStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 302
    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$2$1;->INSTANCE:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$2$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 308
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/protos/jedi/service/PanelRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$getPanelForSession$1$2;->apply(Lcom/squareup/protos/jedi/service/PanelRequest;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
