.class final Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;
.super Ljava/lang/Object;
.source "MessagehubState.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/MessagehubState;->singleMessagingCredentialsState()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/help/messages/MessagingCredentialsState;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;

    invoke-direct {v0}, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;->INSTANCE:Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/help/messages/MessagingCredentialsState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
            ">;)",
            "Lcom/squareup/ui/help/messages/MessagingCredentialsState;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 103
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v0, v0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->status:Ljava/lang/Boolean;

    const-string v1, "it.response.status"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    new-instance v0, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;

    .line 105
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v2, v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->api_key:Ljava/lang/String;

    const-string v1, "it.response.api_key"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v3, v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->app_id:Ljava/lang/String;

    const-string v1, "it.response.app_id"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v4, v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->domain_name:Ljava/lang/String;

    const-string v1, "it.response.domain_name"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v5, v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->hmac_token:Ljava/lang/String;

    const-string v1, "it.response.hmac_token"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v1, v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->recent_active:Ljava/lang/Boolean;

    const-string v6, "it.response.recent_active"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 110
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    iget-object v7, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->device_look_up_token:Ljava/lang/String;

    const-string p1, "it.response.device_look_up_token"

    invoke-static {v7, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    .line 104
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Retrieved;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingCredentialsState;

    goto :goto_0

    .line 113
    :cond_0
    sget-object p1, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Invalid;->INSTANCE:Lcom/squareup/ui/help/messages/MessagingCredentialsState$Invalid;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingCredentialsState;

    goto :goto_0

    .line 117
    :cond_1
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_2

    .line 118
    sget-object p1, Lcom/squareup/ui/help/messages/MessagingCredentialsState$Invalid;->INSTANCE:Lcom/squareup/ui/help/messages/MessagingCredentialsState$Invalid;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingCredentialsState;

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/MessagehubState$singleMessagingCredentialsState$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/help/messages/MessagingCredentialsState;

    move-result-object p1

    return-object p1
.end method
