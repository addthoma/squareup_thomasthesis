.class public final Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;
.super Ljava/lang/Object;
.source "BackgroundMessagingActivityHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final foregrounedActivityListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final messagingControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->foregrounedActivityListenerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lcom/squareup/ui/help/MessagingController;Lio/reactivex/Scheduler;)Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;-><init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lcom/squareup/ui/help/MessagingController;Lio/reactivex/Scheduler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->foregrounedActivityListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    iget-object v1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/help/MessagingController;

    iget-object v2, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/Scheduler;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->newInstance(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lcom/squareup/ui/help/MessagingController;Lio/reactivex/Scheduler;)Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler_Factory;->get()Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    move-result-object v0

    return-object v0
.end method
