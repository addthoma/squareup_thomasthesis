.class public final Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;
.super Ljava/lang/Object;
.source "HelpDeepLinksHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/HelpDeepLinksHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final announcementsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/announcements/AnnouncementsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletHistoryBuilderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletHistoryBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final helpSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection;",
            ">;"
        }
    .end annotation
.end field

.field private final ordersVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrdersVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final referralsVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrdersVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletHistoryBuilder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/announcements/AnnouncementsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSection;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->ordersVisibilityProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->referralsVisibilityProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->helpAppletHistoryBuilderProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->helpSectionProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->announcementsSectionProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->tutorialsSectionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrdersVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletHistoryBuilder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/announcements/AnnouncementsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSection;",
            ">;)",
            "Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;"
        }
    .end annotation

    .line 59
    new-instance v7, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/help/orders/OrdersVisibility;Lcom/squareup/ui/help/referrals/ReferralsVisibility;Lcom/squareup/ui/help/HelpAppletHistoryBuilder;Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/ui/help/tutorials/TutorialsSection;)Lcom/squareup/ui/help/HelpDeepLinksHandler;
    .locals 8

    .line 66
    new-instance v7, Lcom/squareup/ui/help/HelpDeepLinksHandler;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/HelpDeepLinksHandler;-><init>(Lcom/squareup/ui/help/orders/OrdersVisibility;Lcom/squareup/ui/help/referrals/ReferralsVisibility;Lcom/squareup/ui/help/HelpAppletHistoryBuilder;Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/ui/help/tutorials/TutorialsSection;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/HelpDeepLinksHandler;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->ordersVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/help/orders/OrdersVisibility;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->referralsVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->helpAppletHistoryBuilderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->helpSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/help/help/HelpSection;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->announcementsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/help/announcements/AnnouncementsSection;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->tutorialsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/tutorials/TutorialsSection;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->newInstance(Lcom/squareup/ui/help/orders/OrdersVisibility;Lcom/squareup/ui/help/referrals/ReferralsVisibility;Lcom/squareup/ui/help/HelpAppletHistoryBuilder;Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/ui/help/tutorials/TutorialsSection;)Lcom/squareup/ui/help/HelpDeepLinksHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpDeepLinksHandler_Factory;->get()Lcom/squareup/ui/help/HelpDeepLinksHandler;

    move-result-object v0

    return-object v0
.end method
