.class public final Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;
.super Lcom/squareup/ui/help/HelpAppletSectionsListEntry;
.source "TutorialsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/tutorials/TutorialsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListEntry"
.end annotation


# instance fields
.field private final tutorialsBadge:Lcom/squareup/ui/help/tutorials/TutorialsBadge;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/tutorials/TutorialsSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/tutorials/TutorialsBadge;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    sget v0, Lcom/squareup/applet/help/R$string;->tutorials:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;->tutorialsBadge:Lcom/squareup/ui/help/tutorials/TutorialsBadge;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;->tutorialsBadge:Lcom/squareup/ui/help/tutorials/TutorialsBadge;

    invoke-virtual {v1}, Lcom/squareup/ui/help/tutorials/TutorialsBadge;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/applet/AppletsBadgeCounter;->getBadgeRowValueText(Lcom/squareup/util/Res;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
