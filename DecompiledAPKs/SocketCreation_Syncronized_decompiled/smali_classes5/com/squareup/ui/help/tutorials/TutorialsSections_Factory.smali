.class public final Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;
.super Ljava/lang/Object;
.source "TutorialsSections_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->helpAppletSettingsProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;)Lcom/squareup/ui/help/tutorials/TutorialsSections;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/help/tutorials/TutorialsSections;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/help/tutorials/TutorialsSections;-><init>(Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/tutorials/TutorialsSections;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->helpAppletSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/api/HelpAppletSettings;

    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v3, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->newInstance(Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;)Lcom/squareup/ui/help/tutorials/TutorialsSections;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/tutorials/TutorialsSections_Factory;->get()Lcom/squareup/ui/help/tutorials/TutorialsSections;

    move-result-object v0

    return-object v0
.end method
