.class public final Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "TroubleshootingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;->INSTANCE:Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;

    .line 42
    sget-object v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;->INSTANCE:Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;

    .line 43
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_TROUBLESHOOTING:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 27
    const-class v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/help/HelpAppletScope$Component;->troubleshootingCoordinator()Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/applet/help/R$layout;->troubleshooting_section:I

    return v0
.end method
