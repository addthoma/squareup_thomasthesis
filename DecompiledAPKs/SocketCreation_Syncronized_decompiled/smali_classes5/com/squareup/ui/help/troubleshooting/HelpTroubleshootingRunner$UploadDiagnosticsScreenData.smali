.class final Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
.super Ljava/lang/Object;
.source "HelpTroubleshootingRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UploadDiagnosticsScreenData"
.end annotation


# instance fields
.field public final response:Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

.field public final sending:Ljava/lang/Boolean;

.field public final throwable:Ljava/lang/Throwable;

.field public final uploadType:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;


# direct methods
.method private constructor <init>(Ljava/lang/Boolean;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;Ljava/lang/Throwable;)V
    .locals 0

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->sending:Ljava/lang/Boolean;

    .line 182
    iput-object p2, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->response:Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    .line 183
    iput-object p3, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->uploadType:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    .line 184
    iput-object p4, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->throwable:Ljava/lang/Throwable;

    return-void
.end method

.method static synthetic access$000()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 1

    .line 171
    invoke-static {}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->idle()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 1

    .line 171
    invoke-static {}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->sending()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/squareup/util/Res;Ljava/lang/Throwable;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 0

    .line 171
    invoke-static {p0, p1}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->forLedgerError(Lcom/squareup/util/Res;Ljava/lang/Throwable;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 0

    .line 171
    invoke-static {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->forLedger(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/util/Res;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 0

    .line 171
    invoke-static {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->forDiagnosticsError(Lcom/squareup/util/Res;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 0

    .line 171
    invoke-static {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->forDiagnostics(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p0

    return-object p0
.end method

.method private static error(Lcom/squareup/util/Res;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;)Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;
    .locals 1

    .line 214
    new-instance v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    iget p1, p1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->failureToast:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    invoke-direct {v0, p1, p0}, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;-><init>(ZLjava/lang/String;)V

    return-object v0
.end method

.method private static forDiagnostics(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 4

    .line 196
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->SEND_DIAGNOSTICS:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;-><init>(Ljava/lang/Boolean;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static forDiagnosticsError(Lcom/squareup/util/Res;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 4

    .line 200
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->SEND_DIAGNOSTICS:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    invoke-static {p0, v2}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->error(Lcom/squareup/util/Res;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;)Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    move-result-object p0

    sget-object v2, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->SEND_DIAGNOSTICS:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;-><init>(Ljava/lang/Boolean;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static forLedger(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 4

    .line 205
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->UPLOAD_LEDGER:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;-><init>(Ljava/lang/Boolean;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static forLedgerError(Lcom/squareup/util/Res;Ljava/lang/Throwable;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 3

    .line 209
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->UPLOAD_LEDGER:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    invoke-static {p0, v2}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->error(Lcom/squareup/util/Res;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;)Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    move-result-object p0

    sget-object v2, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->UPLOAD_LEDGER:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    invoke-direct {v0, v1, p0, v2, p1}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;-><init>(Ljava/lang/Boolean;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static idle()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 3

    .line 192
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;-><init>(Ljava/lang/Boolean;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static sending()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 3

    .line 188
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;-><init>(Ljava/lang/Boolean;Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;Ljava/lang/Throwable;)V

    return-object v0
.end method
