.class public final Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;
.super Ljava/lang/Object;
.source "ReferralNotificationsSource.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationsSource;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "res",
        "Lcom/squareup/util/Res;",
        "referralsVisibility",
        "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "localNotificationAnalytics",
        "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/help/referrals/ReferralsVisibility;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;)V",
        "createReferralsNotification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "notifications",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

.field private final referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/help/referrals/ReferralsVisibility;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "referralsVisibility"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localNotificationAnalytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    iput-object p3, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p4, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    return-void
.end method

.method public static final synthetic access$createReferralsNotification(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;Lcom/squareup/util/Res;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->createReferralsNotification(Lcom/squareup/util/Res;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;)Lcom/squareup/util/Res;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method private final createReferralsNotification(Lcom/squareup/util/Res;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 13

    .line 47
    new-instance v12, Lcom/squareup/notificationcenterdata/Notification;

    .line 48
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "UUID.randomUUID().toString()"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget v0, Lcom/squareup/applet/help/R$string;->referrals_notification_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 51
    sget v0, Lcom/squareup/applet/help/R$string;->referrals_notification_content:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 54
    sget-object v5, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    .line 55
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$Priority$General;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$General;

    move-object v6, p1

    check-cast v6, Lcom/squareup/notificationcenterdata/Notification$Priority;

    .line 56
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    move-object v7, p1

    check-cast v7, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    .line 57
    sget-object v9, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    .line 58
    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    .line 59
    new-instance v0, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 60
    new-instance v2, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->view_free_processing(Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 61
    sget-object v2, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->NONE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object v0

    const-string v2, "ClientAction.Builder()\n \u2026\n                .build()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p1, v0}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    move-object v8, p1

    check-cast v8, Lcom/squareup/notificationcenterdata/Notification$Destination;

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {p1}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object v10

    .line 65
    sget-object p1, Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;->INSTANCE:Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;

    move-object v11, p1

    check-cast v11, Lcom/squareup/communications/Message$Type;

    const-string v2, ""

    move-object v0, v12

    .line 47
    invoke-direct/range {v0 .. v11}, Lcom/squareup/notificationcenterdata/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    invoke-interface {p1, v12}, Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;->logLocalNotificationCreated(Lcom/squareup/notificationcenterdata/Notification;)V

    return-object v12
.end method


# virtual methods
.method public notifications()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    .line 33
    invoke-virtual {v0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->badgeCount()Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    .line 34
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource$notifications$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource$notifications$1;-><init>(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "referralsVisibility\n    \u2026  }\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
