.class final Lcom/squareup/ui/help/referrals/ReferralNotificationsSource$notifications$1;
.super Ljava/lang/Object;
.source "ReferralNotificationsSource.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->notifications()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;",
        "count",
        "",
        "apply",
        "(Ljava/lang/Integer;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource$notifications$1;->this$0:Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Integer;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;
    .locals 2

    const-string v0, "count"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    .line 38
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result p1

    if-lez p1, :cond_0

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource$notifications$1;->this$0:Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;

    invoke-static {p1}, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->access$getRes$p(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;)Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;->access$createReferralsNotification(Lcom/squareup/ui/help/referrals/ReferralNotificationsSource;Lcom/squareup/util/Res;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 41
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 37
    :goto_0
    invoke-direct {v0, p1}, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/referrals/ReferralNotificationsSource$notifications$1;->apply(Ljava/lang/Integer;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    move-result-object p1

    return-object p1
.end method
