.class synthetic Lcom/squareup/ui/help/HelpDeepLinksHandler$1;
.super Ljava/lang/Object;
.source "HelpDeepLinksHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpDeepLinksHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 71
    invoke-static {}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->values()[Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/help/HelpDeepLinksHandler$1;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/help/HelpDeepLinksHandler$1;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->NONE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v1}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/help/HelpDeepLinksHandler$1;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->LEGACY:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v1}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/help/HelpDeepLinksHandler$1;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->MAGSTRIPE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v1}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/help/HelpDeepLinksHandler$1;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    invoke-virtual {v1}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
