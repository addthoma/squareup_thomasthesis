.class public final Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;
.super Ljava/lang/Object;
.source "HelpAppletSectionsList_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/HelpAppletSectionsList;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletEntryPointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletSelectedSectionSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final referralsVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->helpAppletEntryPointProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->helpAppletSettingsProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->helpAppletSelectedSectionSettingProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->referralsVisibilityProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/api/HelpAppletSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsVisibility;",
            ">;)",
            "Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/help/api/HelpAppletEntryPoint;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/help/referrals/ReferralsVisibility;)Lcom/squareup/ui/help/HelpAppletSectionsList;
    .locals 7

    .line 60
    new-instance v6, Lcom/squareup/ui/help/HelpAppletSectionsList;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/help/HelpAppletSectionsList;-><init>(Lcom/squareup/ui/help/api/HelpAppletEntryPoint;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/help/referrals/ReferralsVisibility;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/HelpAppletSectionsList;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->helpAppletEntryPointProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/api/HelpAppletEntryPoint;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->helpAppletSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/help/api/HelpAppletSettings;

    iget-object v2, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->helpAppletSelectedSectionSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;

    iget-object v3, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->referralsVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->newInstance(Lcom/squareup/ui/help/api/HelpAppletEntryPoint;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/ui/help/HelpAppletSelectedSectionSetting;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/help/referrals/ReferralsVisibility;)Lcom/squareup/ui/help/HelpAppletSectionsList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletSectionsList_Factory;->get()Lcom/squareup/ui/help/HelpAppletSectionsList;

    move-result-object v0

    return-object v0
.end method
