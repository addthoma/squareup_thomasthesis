.class public Lcom/squareup/ui/help/announcements/events/ViewAnnouncementDetailEvent;
.super Lcom/squareup/analytics/event/ViewEvent;
.source "ViewAnnouncementDetailEvent.java"


# instance fields
.field final mobile_view_event_tracker_token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/server/messages/Message;)V
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_MESSAGE:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;)V

    .line 12
    iget-object p1, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/help/announcements/events/ViewAnnouncementDetailEvent;->mobile_view_event_tracker_token:Ljava/lang/String;

    return-void
.end method
