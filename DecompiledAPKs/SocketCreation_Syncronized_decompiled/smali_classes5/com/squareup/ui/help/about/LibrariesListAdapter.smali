.class public final Lcom/squareup/ui/help/about/LibrariesListAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "LibrariesListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$LibraryItemViewHolder;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$DividerViewHolder;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$FooterViewHolder;,
        Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder<",
        "+",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibrariesListAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibrariesListAdapter.kt\ncom/squareup/ui/help/about/LibrariesListAdapter\n*L\n1#1,163:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0000\u0018\u00002\u0010\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00020\u0001:\u0007\u001b\u001c\u001d\u001e\u001f !BU\u0012\u001e\u0010\u0004\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00050\u00060\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0002\u0010\u000fJ\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0012H\u0016J \u0010\u0015\u001a\u00020\r2\u000e\u0010\u0016\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010\u0014\u001a\u00020\u0012H\u0016J \u0010\u0017\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R&\u0010\u0004\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00050\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;",
        "libraryGroups",
        "",
        "Lkotlin/Pair;",
        "Lcom/squareup/ui/help/about/License;",
        "Lcom/squareup/ui/help/about/AndroidLibrary;",
        "canFollowLinks",
        "",
        "onLicenseClicked",
        "Lkotlin/Function1;",
        "",
        "onLibraryClicked",
        "(Ljava/util/List;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "items",
        "getItemCount",
        "",
        "getItemViewType",
        "position",
        "onBindViewHolder",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "DividerViewHolder",
        "FooterViewHolder",
        "Item",
        "ItemType",
        "LibrariesListViewHolder",
        "LibraryItemViewHolder",
        "LicenseHeaderViewHolder",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canFollowLinks:Z

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/squareup/ui/help/about/License;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final onLibraryClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onLicenseClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/help/about/License;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/ui/help/about/License;",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            ">;>;>;Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/help/about/License;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "libraryGroups"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLicenseClicked"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLibraryClicked"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->libraryGroups:Ljava/util/List;

    iput-boolean p2, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->canFollowLinks:Z

    iput-object p3, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->onLicenseClicked:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->onLibraryClicked:Lkotlin/jvm/functions/Function1;

    .line 42
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    .line 43
    iget-object p2, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->libraryGroups:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lkotlin/Pair;

    invoke-virtual {p3}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/ui/help/about/License;

    invoke-virtual {p3}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    .line 44
    new-instance v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;

    invoke-direct {v0, p4}, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;-><init>(Lcom/squareup/ui/help/about/License;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 46
    sget-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Divider;->INSTANCE:Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Divider;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LibraryItem;

    invoke-direct {v0, p4}, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LibraryItem;-><init>(Lcom/squareup/ui/help/about/AndroidLibrary;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    :cond_1
    sget-object p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Footer;->INSTANCE:Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Footer;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->items:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;

    invoke-virtual {p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;->getType()Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->getValue()I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/help/about/LibrariesListAdapter;->onBindViewHolder(Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder<",
            "+",
            "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;

    .line 76
    instance-of v0, p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;

    check-cast p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->bindItem(Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;)V

    goto :goto_0

    .line 77
    :cond_0
    instance-of v0, p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LibraryItem;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibraryItemViewHolder;

    check-cast p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LibraryItem;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibraryItemViewHolder;->bindItem(Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LibraryItem;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/help/about/LibrariesListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder<",
            "+",
            "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    sget-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->LicenseHeader:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    invoke-virtual {v0}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->getValue()I

    move-result v0

    if-ne p2, v0, :cond_0

    new-instance p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;

    .line 87
    sget v0, Lcom/squareup/applet/help/R$layout;->license_group_header:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 88
    iget-boolean v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->canFollowLinks:Z

    .line 89
    iget-object v1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->onLicenseClicked:Lkotlin/jvm/functions/Function1;

    .line 86
    invoke-direct {p2, p1, v0, v1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;-><init>(Lcom/squareup/marketfont/MarketTextView;ZLkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;

    goto :goto_0

    .line 91
    :cond_0
    sget-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->LibraryItem:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    invoke-virtual {v0}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->getValue()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 92
    new-instance p2, Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/widgets/list/NameValueRow;-><init>(Landroid/content/Context;)V

    .line 93
    iget-boolean p1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->canFollowLinks:Z

    if-eqz p1, :cond_1

    .line 94
    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_list:I

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/list/NameValueRow;->setBackgroundResource(I)V

    .line 96
    :cond_1
    new-instance p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(II)V

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/list/NameValueRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 101
    iget-boolean p1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->canFollowLinks:Z

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter;->onLibraryClicked:Lkotlin/jvm/functions/Function1;

    .line 91
    new-instance v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibraryItemViewHolder;

    invoke-direct {v1, p2, p1, v0}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibraryItemViewHolder;-><init>(Lcom/squareup/widgets/list/NameValueRow;ZLkotlin/jvm/functions/Function1;)V

    move-object p2, v1

    check-cast p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;

    goto :goto_0

    .line 104
    :cond_2
    sget-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->Divider:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    invoke-virtual {v0}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->getValue()I

    move-result v0

    if-ne p2, v0, :cond_3

    new-instance p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$DividerViewHolder;

    .line 105
    sget v0, Lcom/squareup/applet/help/R$layout;->license_group_divider:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 104
    invoke-direct {p2, p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$DividerViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;

    goto :goto_0

    .line 107
    :cond_3
    sget-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->Footer:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    invoke-virtual {v0}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->getValue()I

    move-result v0

    if-ne p2, v0, :cond_4

    new-instance p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$FooterViewHolder;

    .line 108
    sget v0, Lcom/squareup/applet/help/R$layout;->libraries_footer:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 107
    invoke-direct {p2, p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$FooterViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;

    :goto_0
    return-object p2

    .line 110
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unexpected viewType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
