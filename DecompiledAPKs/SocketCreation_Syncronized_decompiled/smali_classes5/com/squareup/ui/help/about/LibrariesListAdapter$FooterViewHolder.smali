.class public final Lcom/squareup/ui/help/about/LibrariesListAdapter$FooterViewHolder;
.super Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;
.source "LibrariesListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/about/LibrariesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FooterViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder<",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Footer;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$FooterViewHolder;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$Footer;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method
