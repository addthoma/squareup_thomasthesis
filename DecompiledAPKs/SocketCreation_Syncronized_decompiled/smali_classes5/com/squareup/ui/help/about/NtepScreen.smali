.class public final Lcom/squareup/ui/help/about/NtepScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "NtepScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNtepScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NtepScreen.kt\ncom/squareup/ui/help/about/NtepScreen\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,31:1\n24#2,4:32\n*E\n*S KotlinDebug\n*F\n+ 1 NtepScreen.kt\ncom/squareup/ui/help/about/NtepScreen\n*L\n29#1,4:32\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016R\u0016\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/NtepScreen;",
        "Lcom/squareup/ui/help/InHelpAppletScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/about/NtepScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/about/NtepScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/ui/help/about/NtepScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/about/NtepScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/about/NtepScreen;->INSTANCE:Lcom/squareup/ui/help/about/NtepScreen;

    .line 32
    new-instance v0, Lcom/squareup/ui/help/about/NtepScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/help/about/NtepScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 35
    sput-object v0, Lcom/squareup/ui/help/about/NtepScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 20
    const-class v0, Lcom/squareup/ui/help/about/AboutSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/HelpAppletScope$Component;

    .line 26
    invoke-interface {p1}, Lcom/squareup/ui/help/HelpAppletScope$Component;->ntepCoordinator()Lcom/squareup/ui/help/about/NtepCoordinator;

    move-result-object p1

    const-string/jumbo v0, "view.component(HelpApple\u2026       .ntepCoordinator()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 22
    sget v0, Lcom/squareup/applet/help/R$layout;->ntep:I

    return v0
.end method
