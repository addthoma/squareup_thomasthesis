.class public Lcom/squareup/ui/help/orders/OrderHardwareRunner;
.super Ljava/lang/Object;
.source "OrderHardwareRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/util/BrowserLauncher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->flow:Lflow/Flow;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->features:Lcom/squareup/settings/server/Features;

    .line 37
    iput-object p5, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method


# virtual methods
.method public getActionBarConfig(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 47
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 48
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v1, v0}, Lcom/squareup/ui/help/orders/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 49
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method onAllHardwareTapped(Landroid/content/Context;)V
    .locals 1

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_ALL_HARDWARE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderHardwareUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method onContactlessTapped(Landroid/content/Context;)V
    .locals 1

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_R12:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderContactlessUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method onMagstripeTapped(Landroid/content/Context;)V
    .locals 1

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_READER_R4:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;->INSTANCE:Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderReaderUrl()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public state()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;

    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R4:Lcom/squareup/settings/server/Features$Feature;

    .line 42
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/help/orders/OrderHardwareRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CAN_ORDER_R12:Lcom/squareup/settings/server/Features$Feature;

    .line 43
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/help/orders/OrderHardwareRunner$ViewData;-><init>(ZZ)V

    .line 41
    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
