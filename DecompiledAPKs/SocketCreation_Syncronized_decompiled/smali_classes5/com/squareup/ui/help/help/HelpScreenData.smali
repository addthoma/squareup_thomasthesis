.class public final Lcom/squareup/ui/help/help/HelpScreenData;
.super Ljava/lang/Object;
.source "HelpScreenData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/help/HelpScreenData$Builder;
    }
.end annotation


# instance fields
.field public final frequentlyAskedQuestionsContent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation
.end field

.field public final learnMoreContent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation
.end field

.field public final troubleshootingContent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpScreenData;->learnMoreContent:Ljava/util/List;

    .line 16
    iput-object p2, p0, Lcom/squareup/ui/help/help/HelpScreenData;->frequentlyAskedQuestionsContent:Ljava/util/List;

    .line 17
    iput-object p3, p0, Lcom/squareup/ui/help/help/HelpScreenData;->troubleshootingContent:Ljava/util/List;

    return-void
.end method
