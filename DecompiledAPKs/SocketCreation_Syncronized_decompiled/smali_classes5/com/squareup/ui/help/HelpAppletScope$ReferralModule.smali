.class public abstract Lcom/squareup/ui/help/HelpAppletScope$ReferralModule;
.super Ljava/lang/Object;
.source "HelpAppletScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ReferralModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideReferralListener(Lcom/squareup/ui/help/HelpReferralListener;)Lcom/squareup/referrals/ReferralListener;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
