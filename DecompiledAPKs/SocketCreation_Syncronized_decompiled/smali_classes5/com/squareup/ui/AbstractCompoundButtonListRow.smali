.class public abstract Lcom/squareup/ui/AbstractCompoundButtonListRow;
.super Landroid/widget/LinearLayout;
.source "AbstractCompoundButtonListRow.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field protected button:Landroid/widget/CompoundButton;

.field private isChecked:Z

.field protected name:Landroid/widget/TextView;

.field protected options:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .line 64
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/AbstractCompoundButtonListRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_title_subtitle_row_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/AbstractCompoundButtonListRow;->setMinimumHeight(I)V

    .line 67
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/AbstractCompoundButtonListRow;->setBackgroundResource(I)V

    .line 69
    sget v0, Lcom/squareup/widgets/pos/R$layout;->check_box_list_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/AbstractCompoundButtonListRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 71
    sget p1, Lcom/squareup/widgets/pos/R$id;->title:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->name:Landroid/widget/TextView;

    .line 72
    sget p1, Lcom/squareup/widgets/pos/R$id;->subtitle:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->options:Landroid/widget/TextView;

    .line 73
    sget p1, Lcom/squareup/widgets/pos/R$id;->button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CompoundButton;

    iput-object p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->button:Landroid/widget/CompoundButton;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->isChecked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0

    .line 87
    iput-boolean p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->isChecked:Z

    return-void
.end method

.method public showItem(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->name:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->options:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->options:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 82
    iput-boolean p3, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->isChecked:Z

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->button:Landroid/widget/CompoundButton;

    invoke-virtual {p1, p3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method public toggle()V
    .locals 2

    .line 95
    iget-boolean v0, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->isChecked:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->isChecked:Z

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->button:Landroid/widget/CompoundButton;

    iget-boolean v1, p0, Lcom/squareup/ui/AbstractCompoundButtonListRow;->isChecked:Z

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method
