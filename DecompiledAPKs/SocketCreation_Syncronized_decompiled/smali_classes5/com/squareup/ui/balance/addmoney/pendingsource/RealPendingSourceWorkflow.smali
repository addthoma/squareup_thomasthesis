.class public final Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealPendingSourceWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPendingSourceWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPendingSourceWorkflow.kt\ncom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,71:1\n149#2,5:72\n*E\n*S KotlinDebug\n*F\n+ 1 RealPendingSourceWorkflow.kt\ncom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow\n*L\n52#1,5:72\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t0\u0002:\u0001\u001cB\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001f\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00032\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016\u00a2\u0006\u0002\u0010\u0011J2\u0010\u0012\u001a\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t2\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0002JM\u0010\u0016\u001a\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0002`\t2\u0006\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u0019H\u0016\u00a2\u0006\u0002\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0004H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyScreen;",
        "linkDebitCardWorkflow",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflow;",
        "(Lcom/squareup/debitcard/LinkDebitCardWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;",
        "pendingSourceDialog",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$Action;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final linkDebitCardWorkflow:Lcom/squareup/debitcard/LinkDebitCardWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/debitcard/LinkDebitCardWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "linkDebitCardWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;->linkDebitCardWorkflow:Lcom/squareup/debitcard/LinkDebitCardWorkflow;

    return-void
.end method

.method private final pendingSourceDialog(Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$Action;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;

    .line 50
    new-instance v1, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$pendingSourceDialog$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$pendingSourceDialog$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 51
    new-instance v2, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$pendingSourceDialog$2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$pendingSourceDialog$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 49
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 73
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 74
    const-class v1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 75
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 73
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 53
    sget-object v0, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget-object p1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState$DisplayingPendingSourceDialog;->INSTANCE:Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState$DisplayingPendingSourceDialog;

    check-cast p1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;->render(Lkotlin/Unit;Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 38
    sget-object v0, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState$DisplayingPendingSourceDialog;->INSTANCE:Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState$DisplayingPendingSourceDialog;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;->pendingSourceDialog(Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 39
    :cond_0
    sget-object p1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState$ResendingVerificationEmail;->INSTANCE:Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState$ResendingVerificationEmail;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;->linkDebitCardWorkflow:Lcom/squareup/debitcard/LinkDebitCardWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 41
    sget-object v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;

    const/4 v3, 0x0

    .line 42
    sget-object p1, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$render$1;->INSTANCE:Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow$render$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 39
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;->snapshotState(Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
