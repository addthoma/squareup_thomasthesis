.class final Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSubmitAmountWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->render(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
        "+",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        "result",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    instance-of v0, p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Success;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountAddedSuccessfully;

    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Success;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Success;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountAddedSuccessfully;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 81
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Failure;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;

    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Failure;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Failure;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Failure;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$AmountCouldNotBeAdded;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 82
    :cond_1
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$GenericFailure;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$GenericFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$GenericFailure;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action$GenericFailure;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;->invoke(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
