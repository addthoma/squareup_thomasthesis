.class public final Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;
.super Ljava/lang/Object;
.source "EnterAmountScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;,
        Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\u0018\u0000 \u00152\u00020\u0001:\u0002\u0015\u0016BK\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00070\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00070\t\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00070\t\u00a2\u0006\u0002\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00070\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00070\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00070\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0010R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "data",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;",
        "onSubmit",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/common/Money;",
        "",
        "onAddFundingSource",
        "Lkotlin/Function0;",
        "onBack",
        "onDepositSettingsTapped",
        "(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "getData",
        "()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;",
        "getOnAddFundingSource",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnBack",
        "getOnDepositSettingsTapped",
        "getOnSubmit",
        "()Lkotlin/jvm/functions/Function1;",
        "Companion",
        "ScreenData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final data:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

.field private final onAddFundingSource:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onDepositSettingsTapped:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSubmit:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/common/Money;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->Companion:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$Companion;

    .line 32
    const-class v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/common/Money;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSubmit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAddFundingSource"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDepositSettingsTapped"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->data:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onSubmit:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onAddFundingSource:Lkotlin/jvm/functions/Function0;

    iput-object p4, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iput-object p5, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onDepositSettingsTapped:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method


# virtual methods
.method public final getData()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->data:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    return-object v0
.end method

.method public final getOnAddFundingSource()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onAddFundingSource:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnDepositSettingsTapped()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onDepositSettingsTapped:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnSubmit()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/common/Money;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->onSubmit:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method
