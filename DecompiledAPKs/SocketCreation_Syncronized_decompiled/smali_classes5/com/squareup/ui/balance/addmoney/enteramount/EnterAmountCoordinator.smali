.class public final Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EnterAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$Factory;,
        Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;,
        Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnterAmountCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnterAmountCoordinator.kt\ncom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,203:1\n1103#2,7:204\n1103#2,7:211\n*E\n*S KotlinDebug\n*F\n+ 1 EnterAmountCoordinator.kt\ncom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator\n*L\n99#1,7:204\n101#1,7:211\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001:\u0003234BG\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016J\u0010\u0010#\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0002J\u0010\u0010$\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016J\u0010\u0010%\u001a\u00020 2\u0006\u0010&\u001a\u00020\u0005H\u0002J\u0008\u0010\'\u001a\u00020 H\u0002J\u0008\u0010(\u001a\u00020 H\u0002J\u0018\u0010)\u001a\u00020*2\u0006\u0010&\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\nH\u0002J\u0010\u0010,\u001a\u00020 2\u0006\u0010&\u001a\u00020\u0005H\u0002J\u0008\u0010-\u001a\u00020\nH\u0002J\u0010\u0010.\u001a\u00020 2\u0006\u0010&\u001a\u00020\u0005H\u0002J\u0010\u0010/\u001a\u00020 2\u0006\u0010&\u001a\u00020\u0005H\u0002J\u0010\u00100\u001a\u00020 2\u0006\u0010&\u001a\u00020\u0005H\u0002J\u0018\u00101\u001a\u00020 2\u0006\u0010&\u001a\u00020\u00052\u0006\u0010!\u001a\u00020\"H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0008\u0018\u00010\u0019R\u00020\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "addAmountInput",
        "Lcom/squareup/noho/NohoEditText;",
        "addButton",
        "Lcom/squareup/noho/NohoButton;",
        "addSource",
        "Lcom/squareup/noho/NohoRow;",
        "amountTextWatcher",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;",
        "defaultAmount",
        "helpText",
        "Lcom/squareup/widgets/MessageView;",
        "moneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "detach",
        "handleBack",
        "screen",
        "initActionBar",
        "initAmountEditField",
        "isInputValidMoney",
        "",
        "input",
        "notifyBalanceUpdated",
        "readAddAmountField",
        "updateActionBar",
        "updateHelpText",
        "updateInputField",
        "updateUi",
        "AmountTextWatcher",
        "DepositSettingsLinkSpan",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private addAmountInput:Lcom/squareup/noho/NohoEditText;

.field private addButton:Lcom/squareup/noho/NohoButton;

.field private addSource:Lcom/squareup/noho/NohoRow;

.field private amountTextWatcher:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;

.field private final defaultAmount:Lcom/squareup/protos/common/Money;

.field private helpText:Lcom/squareup/widgets/MessageView;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    const-wide/16 p1, 0x0

    .line 59
    invoke-static {p1, p2, p4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->defaultAmount:Lcom/squareup/protos/common/Money;

    .line 60
    new-instance p1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast p2, Lcom/squareup/money/MoneyExtractor;

    iget-object p3, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p4, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->defaultAmount:Lcom/squareup/protos/common/Money;

    invoke-direct {p1, p2, p3, p4}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->handleBack(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    return-void
.end method

.method public static final synthetic access$notifyBalanceUpdated(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->notifyBalanceUpdated(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    return-void
.end method

.method public static final synthetic access$readAddAmountField(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->readAddAmountField()Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateUi(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;Landroid/view/View;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->updateUi(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 183
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 184
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->add_amount_input_field:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.add_amount_input_field)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addAmountInput:Lcom/squareup/noho/NohoEditText;

    .line 185
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->add_amount_source:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.add_amount_source)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    .line 186
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->add_amount_help_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.add_amount_help_text)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->helpText:Lcom/squareup/widgets/MessageView;

    .line 187
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->add_amount_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.add_amount_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final handleBack(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 0

    .line 161
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->getOnBack()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method private final initActionBar()V
    .locals 4

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 146
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 147
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->add_amount_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final initAmountEditField()V
    .locals 4

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addAmountInput:Lcom/squareup/noho/NohoEditText;

    const-string v1, "addAmountInput"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->defaultAmount:Lcom/squareup/protos/common/Money;

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 141
    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addAmountInput:Lcom/squareup/noho/NohoEditText;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v2, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {v0, v2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    check-cast v1, Lcom/squareup/text/Scrubber;

    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    return-void
.end method

.method private final isInputValidMoney(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;Lcom/squareup/protos/common/Money;)Z
    .locals 1

    .line 170
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->getData()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;->getMinAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 171
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->getData()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;->getMaxAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 168
    invoke-static {p2, v0, p1}, Lcom/squareup/money/MoneyMath;->inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method private final notifyBalanceUpdated(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 6

    .line 152
    invoke-direct {p0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->readAddAmountField()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addButton:Lcom/squareup/noho/NohoButton;

    const-string v2, "addButton"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 153
    :cond_0
    iget-object v3, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addButton:Lcom/squareup/noho/NohoButton;

    if-nez v3, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v3, Landroid/view/View;

    sget v4, Lcom/squareup/balance/applet/impl/R$string;->add_amount_button_text:I

    invoke-static {v3, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 154
    iget-object v4, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v4, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "amount"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 155
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addButton:Lcom/squareup/noho/NohoButton;

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->isInputValidMoney(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    return-void
.end method

.method private final readAddAmountField()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addAmountInput:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string v1, "addAmountInput"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 179
    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->defaultAmount:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object v0
.end method

.method private final updateActionBar(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 4

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 117
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 118
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateActionBar$1;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateHelpText(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 5

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->helpText:Lcom/squareup/widgets/MessageView;

    const-string v1, "helpText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 132
    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->helpText:Lcom/squareup/widgets/MessageView;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 124
    :cond_1
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 125
    sget v3, Lcom/squareup/balance/applet/impl/R$string;->add_amount_help_text:I

    const-string v4, "deposit_settings"

    invoke-virtual {v1, v3, v4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 126
    sget v3, Lcom/squareup/balance/applet/impl/R$string;->add_amount_help_text_deposit_settings:I

    invoke-virtual {v1, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 128
    new-instance v3, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;

    const-string v4, "context"

    .line 129
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-direct {v3, v0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;-><init>(Landroid/content/Context;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    check-cast v3, Lcom/squareup/ui/LinkSpan;

    .line 127
    invoke-virtual {v1, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 132
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateInputField(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->getData()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;->getMaxAllowedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->amountTextWatcher:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;

    const-string v1, "addAmountInput"

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addAmountInput:Lcom/squareup/noho/NohoEditText;

    if-nez v2, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 112
    :cond_1
    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    iput-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->amountTextWatcher:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addAmountInput:Lcom/squareup/noho/NohoEditText;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->amountTextWatcher:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final updateUi(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;Landroid/view/View;)V
    .locals 4

    .line 86
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->updateActionBar(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->updateInputField(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->updateHelpText(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    .line 90
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->getData()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData$Sourced;

    const-string v1, "addSource"

    if-eqz v0, :cond_3

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->getData()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData$Sourced;

    invoke-virtual {v2}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen$ScreenData$Sourced;->getDebitCardSource()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Row_Value:I

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    if-nez v2, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Lcom/squareup/noho/NohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->add_amount_add_funding_source:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v2, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Row_ActionLink:I

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addSource:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    .line 204
    new-instance v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateUi$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateUi$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_8

    const-string v1, "addButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    .line 211
    new-instance v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateUi$$inlined$onClickDebounced$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateUi$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateUi$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$updateUi$3;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 104
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->notifyBalanceUpdated(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->bindViews(Landroid/view/View;)V

    .line 71
    invoke-direct {p0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->initActionBar()V

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->initAmountEditField()V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens.map { it.unwrapV2Screen }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$attach$2;-><init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->amountTextWatcher:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->addAmountInput:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string v1, "addAmountInput"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/text/TextWatcher;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    return-void
.end method
