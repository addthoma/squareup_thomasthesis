.class public final Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSubmitAmountWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;,
        Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSubmitAmountWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSubmitAmountWorkflow.kt\ncom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,253:1\n32#2,12:254\n85#3:266\n240#4:267\n276#5:268\n*E\n*S KotlinDebug\n*F\n+ 1 RealSubmitAmountWorkflow.kt\ncom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow\n*L\n67#1,12:254\n178#1:266\n178#1:267\n178#1:268\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n0\u0002:\u000245B-\u0008\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014JB\u0010\u0015\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00172\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J\u001a\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u00032\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016JH\u0010 \u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010\u001d\u001a\u00020\u00032\u0006\u0010!\u001a\u00020\u00042\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050#H\u0016J\u0010\u0010$\u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u0004H\u0016J\u001e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&2\u0006\u0010(\u001a\u00020\r2\u0006\u0010)\u001a\u00020*H\u0002J2\u0010+\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J:\u0010,\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0002`\n2\u0006\u0010-\u001a\u00020\r2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J\u0012\u0010.\u001a\u00020\'*\u0008\u0012\u0004\u0012\u0002000/H\u0002J\u000c\u00101\u001a\u00020\u0017*\u000202H\u0002J\u000c\u00103\u001a\u00020\u0017*\u000202H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "transfersService",
        "Lcom/squareup/balance/core/server/transfers/TransfersService;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "analytics",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;)V",
        "failureScreen",
        "title",
        "Lcom/squareup/util/ViewString;",
        "message",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "submitAddFunds",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;",
        "amount",
        "instrumentToken",
        "",
        "submittingAmountScreen",
        "successScreen",
        "addedAmount",
        "mapFailure",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "toErrorMessage",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen;",
        "toErrorTitle",
        "Action",
        "TransfersResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transfersService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;

    iput-object p3, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p4, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->analytics:Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;)Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->analytics:Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

    return-object p0
.end method

.method public static final synthetic access$mapFailure(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->mapFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    move-result-object p0

    return-object p0
.end method

.method private final failureScreen(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lcom/squareup/util/ViewString;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 120
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 121
    new-instance v8, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 124
    sget v4, Lcom/squareup/common/strings/R$string;->done:I

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p1

    move-object v3, p2

    .line 121
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v8, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 126
    new-instance p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$failureScreen$1;

    invoke-direct {p1, p3}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$failureScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    .line 120
    invoke-direct {v0, v8, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 127
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 128
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final mapFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/deposits/CreateTransferResponse;",
            ">;)",
            "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;"
        }
    .end annotation

    .line 183
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Failure;

    .line 185
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/deposits/CreateTransferResponse;

    iget-object v1, v1, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    iget-object v1, v1, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->title:Ljava/lang/String;

    const-string v2, "response.status_message.title"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferResponse;

    iget-object p1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    iget-object p1, p1, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->description:Ljava/lang/String;

    const-string v2, "response.status_message.description"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    goto :goto_0

    .line 188
    :cond_0
    sget-object p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$GenericFailure;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$GenericFailure;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    :goto_0
    return-object v0
.end method

.method private final submitAddFunds(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;",
            ">;"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "accountStatusSettings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 153
    new-instance v1, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;-><init>()V

    .line 154
    invoke-virtual {v1, p2}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;->instrument_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;

    move-result-object p2

    .line 155
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    move-result-object p2

    .line 157
    new-instance v1, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;-><init>()V

    .line 158
    invoke-virtual {v1, v0}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    move-result-object v1

    .line 161
    new-instance v2, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;-><init>()V

    .line 162
    invoke-virtual {v2, p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 163
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 164
    invoke-virtual {p1, v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 165
    invoke-virtual {p1, p2}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source(Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 166
    invoke-virtual {p1, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination(Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 167
    sget-object p2, Lcom/squareup/protos/deposits/BalanceActivityType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/deposits/BalanceActivityType;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->balance_activity_type(Lcom/squareup/protos/deposits/BalanceActivityType;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 168
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object p1

    .line 170
    iget-object p2, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/core/server/transfers/TransfersService;->createTransfer(Lcom/squareup/protos/deposits/CreateTransferRequest;)Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 172
    new-instance p2, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$1;-><init>(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo p2, "transfersService.createT\u2026e()\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 267
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 268
    const-class p2, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method private final submittingAmountScreen(Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 109
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->add_money_loading_message:I

    invoke-direct {v1, v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 110
    new-instance v2, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submittingAmountScreen$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submittingAmountScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 108
    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 111
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 112
    sget-object v0, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final successScreen(Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$Action;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 135
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 136
    new-instance v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;

    .line 138
    sget v4, Lcom/squareup/balance/applet/impl/R$string;->add_amount_success_message:I

    .line 139
    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "amount"

    invoke-static {v1, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v8

    .line 140
    sget v5, Lcom/squareup/common/strings/R$string;->done:I

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x31

    const/4 v10, 0x0

    move-object v1, v11

    .line 136
    invoke-direct/range {v1 .. v10}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;-><init>(IIIIIILjava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 142
    new-instance p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$successScreen$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$successScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    .line 135
    invoke-direct {v0, v11, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 143
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 144
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final toErrorMessage(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen;)Lcom/squareup/util/ViewString;
    .locals 1

    .line 200
    instance-of v0, p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;->getMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    goto :goto_0

    .line 201
    :cond_0
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$GenericFailure;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$GenericFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_error_message:I

    invoke-direct {p1, v0}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/util/ViewString;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toErrorTitle(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen;)Lcom/squareup/util/ViewString;
    .locals 1

    .line 193
    instance-of v0, p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$WithMessage;->getTitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    goto :goto_0

    .line 194
    :cond_0
    sget-object v0, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$GenericFailure;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen$GenericFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_error_title:I

    invoke-direct {p1, v0}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/util/ViewString;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 254
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 261
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 262
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 263
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 265
    :cond_3
    check-cast v1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 67
    :cond_4
    sget-object p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$Submitting;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$Submitting;

    move-object v1, p1

    check-cast v1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->initialState(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;

    check-cast p2, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->render(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;",
            "-",
            "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 77
    sget-object v1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$Submitting;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$Submitting;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountProps;->getInstrumentToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->submitAddFunds(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v2

    const/4 v3, 0x0

    sget-object p1, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;->INSTANCE:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 86
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->submittingAmountScreen(Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 88
    :cond_0
    instance-of p1, p2, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingSuccessScreen;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    .line 89
    new-instance p1, Lcom/squareup/ui/balance/addmoney/submitamount/LogAnalyticsWorker;

    new-instance v3, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$2;

    invoke-direct {v3, p0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$2;-><init>(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v3}, Lcom/squareup/ui/balance/addmoney/submitamount/LogAnalyticsWorker;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/Worker;

    invoke-static {p3, p1, v2, v1, v2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 91
    check-cast p2, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingSuccessScreen;

    invoke-virtual {p2}, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingSuccessScreen;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->successScreen(Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 93
    :cond_1
    instance-of p1, p2, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen;

    if-eqz p1, :cond_2

    .line 94
    new-instance p1, Lcom/squareup/ui/balance/addmoney/submitamount/LogAnalyticsWorker;

    new-instance v3, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$3;

    invoke-direct {v3, p0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$render$3;-><init>(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v3}, Lcom/squareup/ui/balance/addmoney/submitamount/LogAnalyticsWorker;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/Worker;

    invoke-static {p3, p1, v2, v1, v2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 97
    check-cast p2, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen;

    invoke-direct {p0, p2}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->toErrorTitle(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen;)Lcom/squareup/util/ViewString;

    move-result-object p1

    .line 98
    invoke-direct {p0, p2}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->toErrorMessage(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState$ShowingFailureScreen;)Lcom/squareup/util/ViewString;

    move-result-object p2

    .line 96
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->failureScreen(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->snapshotState(Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
