.class public Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;
.super Lcom/squareup/applet/AppletSectionsListPresenter;
.source "BalanceAppletSectionsListPresenter.java"


# static fields
.field private static final MAX_UNIFIED_ACTIVITY_ROWS:I = 0x3

.field private static final SECTION_POSITION_OFFSET:I = 0x1


# instance fields
.field private final analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

.field private final balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final device:Lcom/squareup/util/Device;

.field private final locale:Ljava/util/Locale;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Landroid/content/res/Resources;

.field private final runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

.field private final shortDateFormatter:Ljava/text/DateFormat;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/applet/AppletSectionsList;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/util/Locale;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
            ")V"
        }
    .end annotation

    .line 86
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/applet/AppletSectionsListPresenter;-><init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;)V

    .line 87
    iput-object p3, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    .line 88
    iput-object p4, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 89
    iput-object p6, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 90
    iput-object p7, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->shortDateFormatter:Ljava/text/DateFormat;

    .line 91
    iput-object p8, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 92
    iput-object p9, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 93
    iput-object p10, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->locale:Ljava/util/Locale;

    .line 94
    iput-object p11, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    .line 95
    iput-object p5, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    .line 96
    iput-object p12, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    .line 97
    iput-object p13, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    return-void
.end method

.method static synthetic access$001(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;I)V
    .locals 0

    .line 63
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    return-void
.end method

.method static synthetic access$101(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;I)V
    .locals 0

    .line 63
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    return-object p0
.end method

.method private convertToItalics(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4

    if-eqz p1, :cond_1

    .line 380
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 382
    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 383
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v0

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private convertToStrikethrough(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .line 390
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    .line 392
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 395
    new-instance v1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method private createRecentCardActivityRows(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;",
            ">;"
        }
    .end annotation

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 226
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    .line 227
    invoke-direct {p0, v1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->fromCardActivityEvent(Lcom/squareup/protos/client/bizbank/CardActivityEvent;)Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private createRecentDepositActivityRows(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;",
            ">;"
        }
    .end annotation

    .line 252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 254
    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;->total_daily_sales:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    sget-object v2, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ACTIVE_SALES:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->recent_activity_active_sales:I

    .line 255
    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->fromSettlementReportWrapper(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;I)Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    move-result-object v1

    .line 254
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    iget-object p1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;->pending_deposit:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    .line 259
    sget-object v2, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->PENDING_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->recent_activity_pending_deposit:I

    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->fromSettlementReportWrapper(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;I)Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private createUnifiedActivityRows(Lcom/squareup/balance/activity/data/BalanceActivity;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;",
            ">;"
        }
    .end annotation

    .line 235
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->parseUnifiedActivityResult(Lcom/squareup/balance/activity/data/UnifiedActivityResult;ZI)Ljava/util/List;

    move-result-object v0

    .line 237
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v1, :cond_0

    return-object v0

    .line 245
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object p1

    const/4 v3, 0x0

    sub-int/2addr v1, v2

    invoke-direct {p0, p1, v3, v1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->parseUnifiedActivityResult(Lcom/squareup/balance/activity/data/UnifiedActivityResult;ZI)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method private fromCardActivityEvent(Lcom/squareup/protos/client/bizbank/CardActivityEvent;)Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;
    .locals 7

    .line 287
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    .line 290
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    sget-object v2, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->PENDING:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    if-ne v0, v2, :cond_0

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->card_activity_pending:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->convertToItalics(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 293
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 295
    iget-object v2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->recent_activity_card_activity_date_format:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->shortDateFormatter:Ljava/text/DateFormat;

    .line 296
    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "date"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 297
    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "time"

    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 298
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 299
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move-object v2, v0

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 303
    new-instance p1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method private fromSettlementReportWrapper(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/protos/common/Money;
    .locals 2

    if-nez p1, :cond_0

    const-wide/16 v0, 0x0

    .line 355
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object p1
.end method

.method private fromSettlementReportWrapper(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;I)Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;
    .locals 8

    .line 339
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 p3, 0x0

    if-eqz p1, :cond_0

    .line 341
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->recent_activity_sending_date:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->shortDateFormatter:Ljava/text/DateFormat;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->locale:Ljava/util/Locale;

    .line 343
    invoke-static {v3, v4}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "date"

    invoke-virtual {v0, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 345
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, p3

    .line 347
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->fromSettlementReportWrapper(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    if-nez p1, :cond_1

    goto :goto_1

    .line 348
    :cond_1
    new-instance p3, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;

    invoke-direct {p3, p0, p1, p2}, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$J0KljeF1iYc-fmkE-92EtLkYGPk;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V

    :goto_1
    move-object v7, p3

    .line 351
    new-instance p1, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method private fromUnifiedActivity(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;
    .locals 7

    .line 307
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    .line 311
    invoke-interface {v1, p1, p2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;->subLabel(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/resources/TextModel;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 315
    invoke-interface {p2, v0}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_0

    :cond_0
    const-string p2, ""

    :goto_0
    move-object v2, p2

    .line 318
    iget-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    invoke-interface {p2, p1}, Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;->transactionAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;

    move-result-object p2

    const/4 v0, 0x0

    .line 322
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    invoke-interface {v1, p1}, Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;->shouldCrossOutAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 323
    invoke-direct {p0, p2}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->convertToStrikethrough(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_1

    .line 324
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    invoke-interface {v1, p1}, Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;->isPositiveAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    move-object v3, p2

    const/4 v5, 0x1

    goto :goto_2

    :cond_2
    :goto_1
    move-object v3, p2

    const/4 v5, 0x0

    .line 328
    :goto_2
    new-instance p2, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    .line 329
    invoke-interface {v0, p1}, Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;->label(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->balanceActivityMapper:Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    .line 332
    invoke-interface {v0, p1}, Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;->runningBalanceAmount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v6, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;)V

    return-object p2
.end method

.method private isRecentActivityEnabled()Z
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$9aIATVVMzjqameFxKn0SwQxuQ9o(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;Lcom/squareup/applet/AppletSection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->onSectionSelected(Lcom/squareup/applet/AppletSection;)V

    return-void
.end method

.method public static synthetic lambda$aP2DjA_7t_-uBeFTgo_cQXfxo-Q(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->onScreenData(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)V

    return-void
.end method

.method private logSettlementReportRowClick(Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V
    .locals 1

    .line 369
    sget-object v0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter$3;->$SwitchMap$com$squareup$transferreports$TransferReportsLoader$DepositType:[I

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 375
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logPendingDepositClick()V

    goto :goto_0

    .line 371
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logActiveSalesClick()V

    :goto_0
    return-void
.end method

.method private maybeShowRecentActivity(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)V
    .locals 2

    .line 200
    invoke-direct {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->isRecentActivityEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 203
    :cond_0
    iget-object p2, p2, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->recentActivity:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    .line 204
    invoke-virtual {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->hasRecentActivity()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 208
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->hasRecentDepositActivity()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 209
    invoke-virtual {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->getDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->createRecentDepositActivityRows(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_2
    invoke-virtual {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->hasRecentCardActivity()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 211
    invoke-virtual {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->getCardActivityEvents()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->createRecentCardActivityRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 213
    :cond_3
    invoke-virtual {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->getUnifiedActivity()Lcom/squareup/balance/activity/data/BalanceActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->createUnifiedActivityRows(Lcom/squareup/balance/activity/data/BalanceActivity;)Ljava/util/List;

    move-result-object v0

    .line 216
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 217
    invoke-virtual {p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->showEmptyRecentActivity()V

    goto :goto_1

    .line 219
    :cond_4
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    invoke-virtual {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->title()I

    move-result p2

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->showRecentActivity(Ljava/lang/CharSequence;Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method private onScreenData(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)V
    .locals 9

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;

    .line 175
    iget-object v8, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->instantDepositSnapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 177
    invoke-virtual {v8}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->loading()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    invoke-virtual {v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->hideRefreshButton()V

    .line 179
    invoke-virtual {v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->showLoading()V

    return-void

    .line 183
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->showRefreshButton()V

    .line 185
    invoke-virtual {v8}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balanceSummaryLoaded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 186
    iget-object v2, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->balanceTitle:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->balance:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->buttonText:Ljava/lang/CharSequence;

    iget-object v5, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->buttonType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    iget-object v6, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->hint:Ljava/lang/CharSequence;

    iget-boolean v7, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->allowAddMoney:Z

    move-object v1, v0

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->setAndShowBalance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Z)V

    .line 189
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->maybeShowRecentActivity(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)V

    .line 192
    :cond_1
    invoke-virtual {v8}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->couldNotLoadBalance()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 193
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->load_balance_error_message_title:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->load_balance_error_message_body:I

    .line 194
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView;->showLoadingError(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private onSectionSelected(Lcom/squareup/applet/AppletSection;)V
    .locals 2

    const/4 v0, 0x0

    .line 160
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->getSectionCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 161
    invoke-virtual {p0, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v1

    if-ne p1, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 162
    invoke-virtual {p0, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->onSectionClicked(I)V

    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private onSettlementReportRowClicked(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V
    .locals 1

    .line 360
    invoke-direct {p0, p2}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->logSettlementReportRowClick(Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v0, p1, p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->onDepositRowClicked(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V

    return-void
.end method

.method private parseUnifiedActivityResult(Lcom/squareup/balance/activity/data/UnifiedActivityResult;ZI)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            "ZI)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;",
            ">;"
        }
    .end annotation

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 270
    instance-of v1, p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    if-eqz v1, :cond_1

    .line 271
    check-cast p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    .line 272
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getActivities()Ljava/util/List;

    move-result-object p1

    .line 274
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/bizbank/UnifiedActivity;

    .line 275
    invoke-direct {p0, v1, p2}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->fromUnifiedActivity(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/ui/balance/BalanceAppletSectionsListView$RecentActivityRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lt v1, p3, :cond_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public synthetic lambda$fromSettlementReportWrapper$2$BalanceAppletSectionsListPresenter(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Landroid/view/View;)V
    .locals 0

    .line 350
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->onSettlementReportRowClicked(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$BalanceAppletSectionsListPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->balanceHeaderScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$aP2DjA_7t_-uBeFTgo_cQXfxo-Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$aP2DjA_7t_-uBeFTgo_cQXfxo-Q;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    .line 139
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$1$BalanceAppletSectionsListPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->onSectionSelected()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$9aIATVVMzjqameFxKn0SwQxuQ9o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$9aIATVVMzjqameFxKn0SwQxuQ9o;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    .line 142
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onAddMoneyClicked()V
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->onAddMoneyClicked()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 136
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$jAB5oAJZC90tqpS3l4uFi-M1ax8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$jAB5oAJZC90tqpS3l4uFi-M1ax8;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 141
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$2grkSFMbUZyYfynphYbFOGaLNj0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/-$$Lambda$BalanceAppletSectionsListPresenter$2grkSFMbUZyYfynphYbFOGaLNj0;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onRefreshBalanceClicked()V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->onRefreshBalanceClicked()V

    return-void
.end method

.method public onSectionClicked(I)V
    .locals 3

    add-int/lit8 p1, p1, -0x1

    .line 105
    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    .line 106
    instance-of v1, v0, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->onCardSpendClicked()V

    .line 109
    :cond_0
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    if-eqz v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->onManageSquareCardClicked()V

    .line 112
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;

    if-eqz v1, :cond_2

    .line 113
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->runner:Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;->onTransferReportsClicked()V

    .line 115
    :cond_2
    instance-of v1, v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;

    if-eqz v1, :cond_3

    .line 116
    move-object v1, v0

    check-cast v1, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;

    invoke-virtual {v1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;->logSectionClick()V

    .line 119
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object v0

    .line 120
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_5

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v2, :cond_4

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :cond_5
    :goto_0
    const-string v1, "Balance sections should require exactly 0 or 1 permission"

    invoke-static {v2, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 123
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 124
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Permission;

    .line 125
    iget-object v1, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v2, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter$1;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;I)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_1

    .line 131
    :cond_6
    invoke-static {p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->access$101(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;I)V

    :goto_1
    return-void
.end method

.method public onTransferToBankClicked(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;)V
    .locals 3

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter$2;-><init>(Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
