.class public final Lcom/squareup/ui/balance/BalanceAppletScopeRunner;
.super Ljava/lang/Object;
.source "BalanceAppletScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/balance/BalanceAppletScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/balance/BalanceAppletScopeRunner;",
        "Lmortar/Scoped;",
        "balance",
        "Lcom/squareup/ui/balance/BalanceApplet;",
        "(Lcom/squareup/ui/balance/BalanceApplet;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balance:Lcom/squareup/ui/balance/BalanceApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/BalanceApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "balance"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletScopeRunner;->balance:Lcom/squareup/ui/balance/BalanceApplet;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    .line 18
    const-class v1, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/balance/BalanceAppletScope$Component;->balanceScopeRunner()Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;

    move-result-object p1

    check-cast p1, Lmortar/bundler/Bundler;

    .line 17
    invoke-virtual {v0, p1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 20
    iget-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletScopeRunner;->balance:Lcom/squareup/ui/balance/BalanceApplet;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/BalanceApplet;->select()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
