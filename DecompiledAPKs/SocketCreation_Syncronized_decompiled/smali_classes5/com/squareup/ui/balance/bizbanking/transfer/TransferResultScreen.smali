.class public Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;
.super Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;
.source "TransferResultScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;,
        Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 45
    const-class v0, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/balance/BalanceAppletScope$Component;->transferResultCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 41
    sget v0, Lcom/squareup/balance/applet/impl/R$layout;->transfer_result_view:I

    return v0
.end method
