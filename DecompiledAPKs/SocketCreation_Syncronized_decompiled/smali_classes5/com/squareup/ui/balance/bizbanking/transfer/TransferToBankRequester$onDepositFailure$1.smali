.class final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferToBankRequester.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->onDepositFailure(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "it",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/deposits/CreateTransferResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    invoke-direct {v0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>()V

    .line 175
    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/receiving/FailureMessage$Parts;->withTitle(Ljava/lang/String;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object v0

    .line 176
    iget-object p1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    if-eqz p1, :cond_1

    iget-object v2, p1, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->description:Ljava/lang/String;

    :cond_1
    invoke-virtual {v0, v2}, Lcom/squareup/receiving/FailureMessage$Parts;->withBody(Ljava/lang/String;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;->invoke(Lcom/squareup/protos/deposits/CreateTransferResponse;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method
