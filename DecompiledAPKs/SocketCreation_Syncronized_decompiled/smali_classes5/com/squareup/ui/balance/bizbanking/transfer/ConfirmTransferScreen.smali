.class public Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;
.super Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;
.source "ConfirmTransferScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;,
        Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 40
    const-class v0, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/BalanceAppletScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/balance/BalanceAppletScope$Component;->confirmTransferCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 36
    sget v0, Lcom/squareup/balance/applet/impl/R$layout;->confirm_transfer_view:I

    return v0
.end method
