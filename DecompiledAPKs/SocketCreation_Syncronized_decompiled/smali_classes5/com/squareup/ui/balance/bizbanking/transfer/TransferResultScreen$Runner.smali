.class public interface abstract Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;
.super Ljava/lang/Object;
.source "TransferResultScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract onDoneFromTransferResultClicked()V
.end method

.method public abstract onExitFromTransferResultClicked()V
.end method

.method public abstract transferResultScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method
