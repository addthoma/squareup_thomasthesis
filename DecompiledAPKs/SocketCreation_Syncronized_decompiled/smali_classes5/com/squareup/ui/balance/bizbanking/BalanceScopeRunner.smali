.class public Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;
.super Ljava/lang/Object;
.source "BalanceScopeRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/transfer/LinkDebitCardDialog$Runner;
.implements Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog$Runner;
.implements Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;
.implements Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;
.implements Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardWorkflowRunner$ManageSquareCardWorkflowResultHandler;
.implements Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;
.implements Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$LinkBankAccountWorkflowResultHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner$DepositsSettingsLinkSpan;
    }
.end annotation


# static fields
.field private static final BALANCE_TRANSACTION_DETAIL_KEY:Ljava/lang/String; = "balanceTransactionDetail"

.field private static final INCLUDE_RECENT_ACTIVITY:Z = true


# instance fields
.field private final accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

.field private final activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

.field private final addMoneyAnalytics:Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

.field private final analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

.field private final application:Landroid/app/Application;

.field private final balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final cardUpseller:Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private goToConfirmTransferScreen:Z

.field private final instantDepositAnalytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

.field private final longDateFormatter:Ljava/text/DateFormat;

.field private final manageSquareCardSection:Lcom/squareup/balance/squarecard/ManageSquareCardSection;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final onBalanceTransactionDetail:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;"
        }
    .end annotation
.end field

.field private final onNextClickedFromTransferToBank:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;",
            ">;>;"
        }
    .end annotation
.end field

.field private final onRefreshBalance:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final orderSquareCardStampsRequester:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final rateFormatter:Lcom/squareup/text/RateFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final sectionSelectedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/applet/AppletSection;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

.field private final squareCardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;

.field private final timeFormatter:Ljava/text/DateFormat;

.field private final transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/ui/settings/SettingsAppletGateway;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/RateFormatter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/activity/data/BalanceActivityRepository;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/RateFormatter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 194
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onBalanceTransactionDetail:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 196
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onNextClickedFromTransferToBank:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 197
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onRefreshBalance:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 198
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->sectionSelectedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    const/4 v1, 0x0

    .line 200
    iput-boolean v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->goToConfirmTransferScreen:Z

    move-object v1, p1

    .line 219
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->application:Landroid/app/Application;

    move-object v1, p2

    .line 220
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->squareCardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    move-object v1, p3

    .line 221
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    move-object v1, p4

    .line 222
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->orderSquareCardStampsRequester:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

    move-object v1, p5

    .line 223
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    move-object v1, p6

    .line 224
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    move-object v1, p7

    .line 225
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p8

    .line 226
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    move-object v1, p9

    .line 227
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    move-object v1, p10

    .line 228
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->timeFormatter:Ljava/text/DateFormat;

    move-object v1, p11

    .line 229
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->longDateFormatter:Ljava/text/DateFormat;

    move-object v1, p12

    .line 230
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object v1, p13

    .line 231
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->rateFormatter:Lcom/squareup/text/RateFormatter;

    move-object/from16 v1, p14

    .line 232
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->device:Lcom/squareup/util/Device;

    move-object/from16 v1, p15

    .line 233
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p16

    .line 234
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    move-object/from16 v1, p17

    .line 235
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    move-object/from16 v1, p18

    .line 236
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    move-object/from16 v1, p19

    .line 237
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object/from16 v1, p20

    .line 238
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositAnalytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    move-object/from16 v1, p21

    .line 239
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v1, p22

    .line 240
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    move-object/from16 v1, p23

    .line 241
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->cardUpseller:Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

    move-object/from16 v1, p24

    .line 242
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->manageSquareCardSection:Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    move-object/from16 v1, p25

    .line 243
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p26

    .line 244
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->addMoneyAnalytics:Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

    move-object/from16 v1, p27

    .line 245
    iput-object v1, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->countryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)Lcom/squareup/ui/settings/SettingsAppletGateway;
    .locals 0

    .line 146
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V
    .locals 0

    .line 146
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->navigateToDepositSettings()V

    return-void
.end method

.method private allowUnifiedActivity()Z
    .locals 2

    .line 1205
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_UNIFIED_ACTIVITY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 1206
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getBusinessBankingSettings()Lcom/squareup/settings/server/BusinessBankingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/BusinessBankingSettings;->showCardSpend()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private clearTransferSpeed()V
    .locals 2

    .line 1201
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->NO_TRANSFER_SPEED:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->setTransferSpeed(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V

    return-void
.end method

.method private closeAllCards()V
    .locals 3

    .line 1146
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    const/4 v2, 0x1

    .line 1147
    invoke-interface {v1, v2}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 1146
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 1148
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/balance/bizbanking/InBalanceAppletScope;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method private eligibleForInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z
    .locals 2

    .line 1167
    iget-object v0, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-eq v0, v1, :cond_0

    iget-object p1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->LOADING:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private formatDepositAmount(ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 1

    .line 729
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 730
    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "amount"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 731
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private handleNextClickedFromTransferToBank(Lkotlin/Pair;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;",
            ">;",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ")",
            "Lkotlin/Unit;"
        }
    .end annotation

    .line 991
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    .line 992
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    .line 993
    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->logTransferAmountClick(Lcom/squareup/protos/common/Money;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V

    .line 994
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {p2, v0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->setTransferAmountAndSpeed(Lcom/squareup/protos/common/Money;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V

    .line 995
    invoke-virtual {p3}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->showPriceChangeModal()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    .line 996
    iput-boolean p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->goToConfirmTransferScreen:Z

    .line 997
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/instantdeposit/PriceChangeDialog;

    sget-object p3, Lcom/squareup/ui/balance/BalanceAppletScope;->INSTANCE:Lcom/squareup/ui/balance/BalanceAppletScope;

    invoke-direct {p2, p3}, Lcom/squareup/instantdeposit/PriceChangeDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 999
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 1002
    :goto_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method private instantDepositButtonText(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 2

    .line 1161
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_button_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 1162
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 1163
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private isEligibleForConfirmScreen(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z
    .locals 1

    .line 1176
    iget-object v0, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->getBalanceSummaryResponse:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static synthetic lambda$7U6cVUfy-kERDiiLIuMqfQNFPxY(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->eligibleForInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result p0

    return p0
.end method

.method public static synthetic lambda$AlpweiCgNjQAkcJzguNJ_AE4S_Q(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->toBalanceMasterScreenData(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$BtHQimYZ9p8pzz-6w9Ctm0EfM2I(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->maybeLogTransferResults(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$FOHiGqKRdagwJXPvJUN7FEVrNeM(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->toTransferToBankScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$LtBbXbktHYEnp0zIsT91lY6DWt4(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->maybeLogCardActivityShown(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$balanceTransactionsScreenData$2(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 304
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static synthetic lambda$bibE2YGPCZtjgxhGZiPw1D1gubg(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->noDepositResult(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result p0

    return p0
.end method

.method public static synthetic lambda$gzt8hBfYu_0Yd1VnxZ3KGOOR_tw(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lkotlin/Unit;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onRefreshBalance(Lkotlin/Unit;)V

    return-void
.end method

.method public static synthetic lambda$mpokApqXlb99O111febQu_y4W6M(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->isEligibleForConfirmScreen(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result p0

    return p0
.end method

.method public static synthetic lambda$n9kSPgmR9bchPyVgimzcYuSUUmU(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->maybeLogBalanceError(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$oD8HUWTZp2COdbE0mp4giSEmJ2c(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lkotlin/Pair;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lkotlin/Unit;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->handleNextClickedFromTransferToBank(Lkotlin/Pair;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lkotlin/Unit;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$osEw2tKAFZaICnWGx2qKO2bF8oU(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lkotlin/Unit;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->showUnableToChangeExpenseWarningDialog(Lkotlin/Unit;)V

    return-void
.end method

.method static synthetic lambda$transferResultScreenData$8(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 375
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static synthetic lambda$z0aVR3HiIIXZvT_svPYafCT5lyw(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->toInstantDepositResultScreenData(Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method private logBalanceHeaderSection()V
    .locals 1

    .line 1013
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logBalanceHeaderView()V

    return-void
.end method

.method private logTransferAmountClick(Lcom/squareup/protos/common/Money;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 1

    .line 1007
    iget-object p2, p2, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    if-ne p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 1008
    invoke-virtual {p3}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object p3

    goto :goto_1

    :cond_1
    invoke-virtual {p3}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 1009
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0, p1, p3, p2}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logTransferRequested(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V

    return-void
.end method

.method private maybeLogBalanceError(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 1

    .line 1024
    iget-object v0, p2, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->instantDepositSnapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-virtual {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->couldNotLoadBalance()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p2

    .line 1032
    :cond_0
    sget-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->LOADING:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    if-eq p1, v0, :cond_1

    iget-object p1, p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->instantDepositSnapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 1033
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->couldNotLoadBalance()Z

    move-result p1

    if-nez p1, :cond_2

    .line 1034
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logBalanceLoadFailure()V

    :cond_2
    return-object p2
.end method

.method private maybeLogCardActivityShown(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;
    .locals 6

    .line 1049
    iget-object v0, p2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getStatus()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->LOADING:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    if-ne v0, v1, :cond_0

    return-object p2

    .line 1053
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    .line 1054
    iget-object v1, p2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->transactions:Ljava/util/List;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-nez v0, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-eq v4, v5, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 1066
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eq v5, v0, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    .line 1068
    :goto_3
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    if-eq p1, v0, :cond_5

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->cardActivityState:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    .line 1069
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getStatus()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->CARD_ACTIVITY_LOADED:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    if-ne p1, v0, :cond_5

    if-nez v4, :cond_5

    if-eqz v2, :cond_6

    .line 1071
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logCardActivityResult(Ljava/util/List;)V

    :cond_6
    return-object p2
.end method

.method private maybeLogTransferResults(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;
    .locals 2

    .line 1085
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1086
    :goto_0
    iget-object v1, p2, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->requestState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    if-nez v0, :cond_1

    .line 1093
    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->requestState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    if-eq v1, p1, :cond_2

    .line 1094
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->maybeLogTransferResults(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;)V

    :cond_2
    return-object p2
.end method

.method private navigateToDepositSettings()V
    .locals 3

    .line 1191
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletGateway;->isInstantDepositsVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1192
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner$1;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :cond_0
    return-void
.end method

.method private noDepositResult(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z
    .locals 2

    .line 1181
    iget-object v0, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_SUCCEEDED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-eq v0, v1, :cond_0

    iget-object p1, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->SERVER_CALL_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private onAllowPartialDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 15

    move-object v0, p0

    .line 833
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->allowUnifiedActivity()Z

    move-result v1

    .line 835
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/balance/applet/impl/R$string;->deposit_to_bank:I

    .line 836
    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    move-object v8, v2

    goto :goto_0

    :cond_0
    move-object v8, v3

    .line 838
    :goto_0
    new-instance v2, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v4, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/balance/applet/impl/R$string;->available_balance_uppercase:I

    .line 840
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .line 841
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v7

    sget-object v9, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->DEPOSIT_TO_BANK:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v10, 0x0

    if-nez v1, :cond_1

    .line 845
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v4

    move-object v11, v4

    goto :goto_1

    :cond_1
    move-object v11, v3

    .line 846
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v12

    if-eqz v1, :cond_2

    move-object/from16 v13, p2

    goto :goto_2

    :cond_2
    move-object v13, v3

    .line 848
    :goto_2
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result v14

    move-object v4, v2

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v14}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v2
.end method

.method private onCanMakeDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 12

    .line 891
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositAnalytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletDisplayedAvailableBalance()V

    .line 892
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->upcoming_deposits_uppercase:I

    .line 894
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 895
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 896
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositButtonText(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v5

    sget-object v6, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->INSTANTLY_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 898
    invoke-interface {v1, p1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->instantDepositHint(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 899
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v8

    .line 900
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v9

    .line 902
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result v11

    const/4 v10, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v0
.end method

.method private onConfirmInstantTransfer(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 12

    .line 876
    new-instance v11, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->upcoming_deposits_uppercase:I

    .line 878
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 879
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->confirm_instant_transfer:I

    .line 880
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    sget-object v5, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->CONFIRM_INSTANT_TRANSFER:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 882
    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->instantDepositHint(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 883
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v7

    .line 884
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v8

    .line 886
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result v10

    const/4 v9, 0x0

    move-object v0, v11

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v11
.end method

.method private onCouldNotLoadBalance(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 12

    .line 793
    new-instance v11, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    sget-object v5, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 800
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v7

    .line 801
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v11
.end method

.method private onDepositFailed(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 8

    .line 702
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_deposit_failed:I

    .line 703
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    sget v2, Lcom/squareup/vectoricons/R$drawable;->circle_alert_96:I

    .line 705
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositFailedTitle()Ljava/lang/String;

    move-result-object v3

    .line 706
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositFailedDescription()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    return-object v7
.end method

.method private onDepositSucceeded(Lcom/squareup/protos/common/Money;ZLcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 9

    .line 680
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositAnalytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletInstantTransferSuccess()V

    .line 681
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 682
    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_deposit_result_card_title:I

    .line 683
    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->formatDepositAmount(ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 687
    invoke-static {v0}, Lcom/squareup/instantdeposit/InstantDepositCountryResources;->instantDepositSuccessTitle(Lcom/squareup/CountryCode;)I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->formatDepositAmount(ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz p2, :cond_1

    .line 689
    invoke-virtual {p3}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTabletLessThan10Inches()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 691
    :goto_1
    new-instance p3, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    if-eqz p1, :cond_2

    sget p1, Lcom/squareup/vectoricons/R$drawable;->circle_check_96:I

    move v4, p1

    goto :goto_2

    :cond_2
    const/4 p1, -0x1

    const/4 v4, -0x1

    :goto_2
    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v2, p3

    move v7, p2

    invoke-direct/range {v2 .. v8}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    return-object p3
.end method

.method private onDepositsFrozen(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 12

    .line 810
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->application:Landroid/app/Application;

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->deposits_frozen_info:I

    const-string v2, "learn_more"

    .line 811
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->deposits_frozen_info_link:I

    .line 812
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->deposits_frozen_help_link_url:I

    .line 813
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$x2dTJddcLiFbzs8ThWw3OvRNe_8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$x2dTJddcLiFbzs8ThWw3OvRNe_8;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 814
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->onClick(Ljava/lang/Runnable;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 815
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v7

    .line 817
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->deposits_frozen_title_uppercase:I

    .line 819
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 820
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->deposits_frozen_verify_account:I

    .line 821
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    sget-object v6, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->ACCOUNT_FROZEN:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 824
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v8

    .line 825
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v0
.end method

.method private onHideInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 12

    .line 939
    new-instance v11, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->upcoming_deposits_uppercase:I

    .line 941
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 942
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    sget-object v5, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 946
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v7

    .line 947
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v8

    .line 949
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result v10

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object v0, v11

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v11
.end method

.method private onLoading(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 2

    .line 722
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_deposit_result_card_title:I

    .line 723
    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->formatDepositAmount(ILcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private onNotEligible(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 25

    move-object/from16 v0, p0

    .line 907
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityBlocker()Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-eq v1, v2, :cond_1

    .line 908
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityBlocker()Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_VERIFICATION_EXPIRED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 923
    :cond_0
    new-instance v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v2, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->upcoming_deposits_uppercase:I

    .line 925
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    .line 926
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 927
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityTitle()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 929
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityDescription()Ljava/lang/String;

    move-result-object v9

    .line 930
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v10

    .line 931
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v11

    const/4 v12, 0x0

    .line 933
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result v13

    move-object v3, v1

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v13}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v1

    .line 909
    :cond_1
    :goto_0
    new-instance v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v2, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/balance/applet/impl/R$string;->upcoming_deposits_uppercase:I

    .line 911
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v16

    .line 912
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v17

    iget-object v2, v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/debitcard/R$string;->instant_deposits_resend_email:I

    .line 913
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v18

    sget-object v19, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->RESEND_EMAIL:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 915
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityDescription()Ljava/lang/String;

    move-result-object v20

    .line 916
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v21

    .line 917
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v22

    const/16 v23, 0x0

    .line 919
    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result v24

    move-object v14, v1

    move-object/from16 v15, p1

    invoke-direct/range {v14 .. v24}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v1
.end method

.method private onRefreshBalance(Lkotlin/Unit;)V
    .locals 0

    .line 653
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->refreshData()V

    .line 655
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->allowUnifiedActivity()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 656
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    invoke-interface {p1}, Lcom/squareup/balance/activity/data/BalanceActivityRepository;->reset()V

    :cond_0
    return-void
.end method

.method private onServerCallFailed()Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 8

    .line 712
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_deposit_failed:I

    .line 713
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    sget v2, Lcom/squareup/vectoricons/R$drawable;->circle_alert_96:I

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->instant_deposits_unavailable:I

    .line 715
    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->instant_deposits_network_error_message:I

    .line 716
    invoke-interface {v0, v4}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;-><init>(Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    return-object v7
.end method

.method private onSetUpInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 12

    .line 854
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->application:Landroid/app/Application;

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->hide_instant_deposit_hint:I

    const-string v2, "deposits_settings"

    .line 855
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->deposits_settings:I

    .line 856
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner$DepositsSettingsLinkSpan;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner$DepositsSettingsLinkSpan;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;Lcom/squareup/util/Res;)V

    .line 857
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 858
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v7

    .line 860
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->upcoming_deposits_uppercase:I

    .line 862
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 863
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 864
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositButtonText(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v5

    sget-object v6, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->SET_UP_INSTANT_DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 867
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentCardActivity()Ljava/util/List;

    move-result-object v8

    .line 868
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->recentDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v9

    .line 870
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z

    move-result v11

    const/4 v10, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v0
.end method

.method private parseBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccount;)Ljava/lang/CharSequence;
    .locals 3

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 1104
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->bank_account:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    const-string v2, "bank_name"

    .line 1105
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    const-string v1, "account_number_suffix"

    .line 1106
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 1107
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private parseCardActivityEvents(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;"
        }
    .end annotation

    .line 1111
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getCardActivities()Ljava/util/List;

    move-result-object v0

    .line 1113
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getStatus()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->COULD_NOT_LOAD_CARD_ACTIVITY:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1117
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1118
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 1121
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    .line 1124
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    iget-object v3, v3, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, v3, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v3}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 1126
    invoke-static {v3}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->asDate(Ljava/util/Date;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1128
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    .line 1129
    iget-object v5, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v5, v5, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v5}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v9

    .line 1130
    invoke-static {v3, v9}, Lcom/squareup/util/Times;->onDifferentDay(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1132
    invoke-static {v9}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->asDate(Ljava/util/Date;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v9

    .line 1135
    :cond_2
    iget-object v5, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    sget-object v6, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->PENDING:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v7, 0x1

    if-ne v5, v6, :cond_3

    const/4 v10, 0x1

    goto :goto_1

    :cond_3
    const/4 v10, 0x0

    .line 1136
    :goto_1
    iget-object v5, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->transaction_state:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    sget-object v6, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DECLINED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    if-ne v5, v6, :cond_4

    const/4 v11, 0x1

    goto :goto_2

    :cond_4
    const/4 v11, 0x0

    .line 1137
    :goto_2
    new-instance v5, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;

    iget-object v7, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->description:Ljava/lang/String;

    iget-object v8, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->amount:Lcom/squareup/protos/common/Money;

    const/4 v12, 0x0

    iget-object v6, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    .line 1139
    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    int-to-long v13, v6

    iget-object v15, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    iget-object v4, v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->is_personal_expense:Ljava/lang/Boolean;

    .line 1140
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16

    move-object v6, v5

    invoke-direct/range {v6 .. v16}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Date;ZZZJLjava/lang/String;Z)V

    .line 1137
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return-object v1
.end method

.method private refreshBalance()V
    .locals 2

    .line 1187
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onRefreshBalance:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private shouldAllowAddMoney(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Z
    .locals 2

    .line 955
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_ALLOW_ADD_MONEY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->allowAddMoney()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 958
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->addMoneyAnalytics:Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

    invoke-interface {v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;->logSeeAddMoneyButton()V

    :cond_1
    return p1
.end method

.method private showUnableToChangeExpenseWarningDialog(Lkotlin/Unit;)V
    .locals 2

    .line 1153
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onBalanceTransactionDetail:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1155
    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->unable_to_change_expense_title:I

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->unable_to_change_expense_body:I

    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 1157
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private toBalanceMasterScreenData(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 4

    .line 737
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->loading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    sget-object p1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->LOADING:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    return-object p1

    .line 742
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->couldNotLoadBalance()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 743
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onCouldNotLoadBalance(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 747
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    invoke-interface {v0}, Lcom/squareup/accountfreeze/AccountFreeze;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 748
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p2}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->onViewedSuspendedDeposits()V

    .line 749
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onDepositsFrozen(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 753
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->allowPartialDeposit()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 754
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onAllowPartialDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 758
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v0, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    invoke-interface {p2, v0}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result p2

    if-eqz p2, :cond_a

    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 759
    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositAllowed()Z

    move-result p2

    if-eqz p2, :cond_a

    .line 760
    iget-object p2, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->CAN_MAKE_DEPOSIT:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-ne p2, v0, :cond_5

    .line 761
    iget-boolean p2, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->isConfirmingInstantTransfer:Z

    if-eqz p2, :cond_4

    .line 762
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onConfirmInstantTransfer(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 765
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onCanMakeDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 766
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->eligibilityBlocker()Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    move-result-object p2

    sget-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_LINKED_BANK_ACCOUNT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne p2, v0, :cond_6

    .line 768
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onHideInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 769
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->noLinkedDebitCard()Z

    move-result p2

    if-eqz p2, :cond_9

    .line 771
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz p2, :cond_8

    .line 772
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p2, v0, v2

    if-nez p2, :cond_7

    goto :goto_0

    .line 781
    :cond_7
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onSetUpInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 776
    :cond_8
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onHideInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 782
    :cond_9
    iget-object p2, p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-ne p2, v0, :cond_a

    .line 783
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onNotEligible(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 788
    :cond_a
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onHideInstantDeposit(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method private toInstantDepositResultScreenData(Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Triplet<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;"
        }
    .end annotation

    .line 662
    iget-object v0, p1, Lcom/squareup/util/tuple/Triplet;->first:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 663
    iget-object v1, p1, Lcom/squareup/util/tuple/Triplet;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 664
    iget-object p1, p1, Lcom/squareup/util/tuple/Triplet;->third:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/util/DeviceScreenSizeInfo;

    .line 666
    sget-object v2, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner$2;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    iget-object v3, v0, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v3}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    const/4 p1, 0x2

    if-eq v2, p1, :cond_1

    const/4 p1, 0x3

    if-eq v2, p1, :cond_0

    .line 674
    invoke-virtual {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onLoading(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 672
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onServerCallFailed()Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 670
    :cond_1
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onDepositFailed(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    move-result-object p1

    return-object p1

    .line 668
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->sentDepositAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onDepositSucceeded(Lcom/squareup/protos/common/Money;ZLcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method private toTransferToBankScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;
    .locals 9

    .line 966
    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 967
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->depositAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 968
    :goto_1
    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->fee:Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    .line 969
    iget-object v4, v1, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;->fee_basis_points:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 970
    iget-object v5, v1, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;->fee_fixed_amount:Lcom/squareup/protos/common/Money;

    .line 971
    iget-object v6, p2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->state:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    sget-object v7, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->CAN_MAKE_DEPOSIT:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    if-eq v6, v7, :cond_3

    .line 972
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->noLinkedBankAccount()Z

    move-result v6

    if-nez v6, :cond_3

    .line 973
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->noLinkedDebitCard()Z

    move-result p2

    if-eqz p2, :cond_2

    goto :goto_2

    :cond_2
    const/4 p2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p2, 0x1

    .line 975
    :goto_3
    new-instance v6, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    invoke-direct {v6}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;-><init>()V

    iget-object v7, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    .line 976
    invoke-interface {v7, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->showInstantDeposit(Z)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object v6

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    .line 977
    invoke-virtual {v6, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->transferSpeed(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object p1

    .line 978
    invoke-virtual {p1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->maxDeposit(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object p1

    iget-object v6, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 979
    invoke-interface {v6, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedMaxDeposit(Ljava/lang/CharSequence;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object p1

    .line 980
    invoke-virtual {p1, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->instantDepositClickable(Z)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object p1

    if-nez v4, :cond_4

    .line 981
    invoke-static {v5}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p2

    if-eqz p2, :cond_4

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    invoke-virtual {p1, v2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->isFeeFixedAmount(Z)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->rateFormatter:Lcom/squareup/text/RateFormatter;

    .line 982
    invoke-static {v4}, Lcom/squareup/util/Percentage;->fromBasisPoints(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {p2, v5, v0}, Lcom/squareup/text/RateFormatter;->format(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedFee(Ljava/lang/CharSequence;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, v1, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;->fee_total_amount:Lcom/squareup/protos/common/Money;

    .line 983
    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedFeeTotalAmount(Ljava/lang/CharSequence;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;

    move-result-object p1

    .line 984
    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->build()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public balanceHeaderScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 327
    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$bibE2YGPCZtjgxhGZiPw1D1gubg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$bibE2YGPCZtjgxhGZiPw1D1gubg;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 331
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->allowUnifiedActivity()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->balanceActivityRepository:Lcom/squareup/balance/activity/data/BalanceActivityRepository;

    sget-object v2, Lcom/squareup/balance/activity/data/BalanceActivityType;->ALL:Lcom/squareup/balance/activity/data/BalanceActivityType;

    .line 333
    invoke-interface {v1, v2}, Lcom/squareup/balance/activity/data/BalanceActivityRepository;->balanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;

    move-result-object v1

    .line 336
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$AlpweiCgNjQAkcJzguNJ_AE4S_Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$AlpweiCgNjQAkcJzguNJ_AE4S_Q;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 337
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPairForFunc(Lkotlin/jvm/functions/Function2;)Lio/reactivex/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0

    .line 339
    :cond_0
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$yxDWjMXxDJRzjor07JI8AMgMvBE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$yxDWjMXxDJRzjor07JI8AMgMvBE;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 340
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 343
    :goto_0
    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->LOADING:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$n9kSPgmR9bchPyVgimzcYuSUUmU;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$n9kSPgmR9bchPyVgimzcYuSUUmU;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 344
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$ZI2UBXyoPWL-DEeyN8xFwtnWUl4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$ZI2UBXyoPWL-DEeyN8xFwtnWUl4;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 345
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public balanceTransactionDetailData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onBalanceTransactionDetail:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$vUEGJTDygvs6bIz9KS1mKXwxFwE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$vUEGJTDygvs6bIz9KS1mKXwxFwE;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public balanceTransactionsScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->state()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$pppaAveEd-Iuuft8wTCny2FJBWA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$pppaAveEd-Iuuft8wTCny2FJBWA;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 301
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$LtBbXbktHYEnp0zIsT91lY6DWt4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$LtBbXbktHYEnp0zIsT91lY6DWt4;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 303
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$9QmiBrPW2cXWOnik_8g30S-Tnq0;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$9QmiBrPW2cXWOnik_8g30S-Tnq0;

    .line 304
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public confirmTransferScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$mpokApqXlb99O111febQu_y4W6M;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$mpokApqXlb99O111febQu_y4W6M;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 355
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 354
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$23NO4sfzCSVWyOxFxZdUz3cMRpc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$23NO4sfzCSVWyOxFxZdUz3cMRpc;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 356
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPairForFunc(Lkotlin/jvm/functions/Function2;)Lio/reactivex/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 282
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public instantDepositResultScreenData()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 620
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 622
    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$7U6cVUfy-kERDiiLIuMqfQNFPxY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$7U6cVUfy-kERDiiLIuMqfQNFPxY;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->cardUpseller:Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

    .line 623
    invoke-interface {v1}, Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;->showUpsellOnInstantDeposit()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->device:Lcom/squareup/util/Device;

    .line 624
    invoke-interface {v2}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v2

    .line 625
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toTriplet()Lio/reactivex/functions/Function3;

    move-result-object v3

    .line 621
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$z0aVR3HiIIXZvT_svPYafCT5lyw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$z0aVR3HiIIXZvT_svPYafCT5lyw;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 627
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$balanceHeaderScreenData$4$BalanceScopeRunner(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 340
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->toBalanceMasterScreenData(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$balanceHeaderScreenData$5$BalanceScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 345
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->logBalanceHeaderSection()V

    return-void
.end method

.method public synthetic lambda$balanceTransactionDetailData$3$BalanceScopeRunner(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 314
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->money:Lcom/squareup/protos/common/Money;

    .line 315
    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->longDateFormatter:Ljava/text/DateFormat;

    iget-object v3, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->date:Ljava/util/Date;

    .line 316
    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->timeFormatter:Ljava/text/DateFormat;

    iget-object v4, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->date:Ljava/util/Date;

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->transactionToken:Ljava/lang/String;

    iget-boolean v6, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->isPersonalExpense:Z

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v7
.end method

.method public synthetic lambda$balanceTransactionsScreenData$1$BalanceScopeRunner(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 301
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;

    .line 302
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->parseCardActivityEvents(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Ljava/util/List;)V

    return-object v0
.end method

.method public synthetic lambda$confirmTransferScreenData$6$BalanceScopeRunner(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;
    .locals 7

    .line 356
    new-instance v6, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    .line 358
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 359
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedBankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->parseBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccount;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 360
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedCard()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v4

    .line 361
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->balance()Lcom/squareup/protos/common/Money;

    move-result-object v5

    move-object v0, v6

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;ZLjava/lang/CharSequence;Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;)V

    return-object v6
.end method

.method public synthetic lambda$onAddMoneyClicked$10$BalanceScopeRunner(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 478
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->debitCardPayInEligibility()Lcom/squareup/protos/client/deposits/EligibilityDetails;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 484
    iget-object v2, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-nez v2, :cond_1

    .line 485
    new-instance v1, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$AvailableCard;

    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedCard()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$AvailableCard;-><init>(Lcom/squareup/protos/client/deposits/CardInfo;)V

    goto :goto_0

    .line 486
    :cond_1
    iget-object p1, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    sget-object v2, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne p1, v2, :cond_2

    .line 487
    sget-object v1, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$NoLinkedCard;->INSTANCE:Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$NoLinkedCard;

    goto :goto_0

    .line 488
    :cond_2
    iget-object p1, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    sget-object v2, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-ne p1, v2, :cond_3

    .line 489
    sget-object v1, Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$PendingVerificationCard;->INSTANCE:Lcom/squareup/ui/balance/addmoney/LinkedCardInfo$PendingVerificationCard;

    .line 494
    :cond_3
    :goto_0
    new-instance p1, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;

    iget-object v2, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->min_amount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->max_amount:Lcom/squareup/protos/common/Money;

    invoke-direct {p1, v1, v2, v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;-><init>(Lcom/squareup/ui/balance/addmoney/LinkedCardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 499
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/AddMoneyBootstrapScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/balance/bizbanking/AddMoneyBootstrapScreen;-><init>(Lcom/squareup/ui/balance/addmoney/AddMoneyProps;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onDepositsFrozen$13$BalanceScopeRunner()V
    .locals 1

    .line 814
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->onFreezeLearnMore()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$BalanceScopeRunner(Lkotlin/Unit;)Lio/reactivex/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 263
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onInstantClicked$11$BalanceScopeRunner(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 562
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->noLinkedBankAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->clearTransferSpeed()V

    .line 564
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 565
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->noLinkedDebitCard()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 566
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->clearTransferSpeed()V

    .line 567
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/LinkDebitCardDialog;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/LinkDebitCardDialog;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onStandardClicked$12$BalanceScopeRunner(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 577
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->noLinkedBankAccount()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 578
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->clearTransferSpeed()V

    .line 579
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onTransferToBankClicked$9$BalanceScopeRunner(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 439
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->showPriceChangeModal()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 440
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/instantdeposit/PriceChangeDialog;

    sget-object v1, Lcom/squareup/ui/balance/BalanceAppletScope;->INSTANCE:Lcom/squareup/ui/balance/BalanceAppletScope;

    invoke-direct {v0, v1}, Lcom/squareup/instantdeposit/PriceChangeDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 442
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->setIsConfirmingInstantTransfer()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$transferResultScreenData$7$BalanceScopeRunner(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;
    .locals 3

    .line 368
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    .line 370
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 371
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedBankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v2

    .line 372
    invoke-virtual {p2}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->linkedCard()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object p2

    invoke-direct {v0, p1, v1, v2, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;)V

    return-object v0
.end method

.method public loadMoreTransactions(Ljava/lang/String;)V
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->loadMoreCardActivity(Ljava/lang/String;)V

    return-void
.end method

.method public onAddMoneyClicked()V
    .locals 3

    .line 470
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->addMoneyAnalytics:Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;

    invoke-interface {v0}, Lcom/squareup/ui/balance/addmoney/AddMoneyAnalytics;->logAddMoneyButtonClick()V

    .line 472
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 475
    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v1

    .line 476
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$-98Kp2dNnchnQOif1yljvtLk60A;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$-98Kp2dNnchnQOif1yljvtLk60A;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 477
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 472
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onAddMoneyResult(Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V
    .locals 3

    .line 397
    instance-of v0, p1, Lcom/squareup/ui/balance/addmoney/AddMoneyResult$ShowDepositSettings;

    if-eqz v0, :cond_0

    .line 398
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->navigateToDepositSettings()V

    goto :goto_0

    .line 399
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/balance/addmoney/AddMoneyResult$MoneyAddedSuccessfully;

    if-eqz v0, :cond_1

    .line 400
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->refreshBalance()V

    goto :goto_0

    .line 401
    :cond_1
    instance-of p1, p1, Lcom/squareup/ui/balance/addmoney/AddMoneyResult$GoToAddFundingSource;

    if-eqz p1, :cond_2

    .line 402
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;

    new-instance v1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    sget-object v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;->DEPOSITS_APPLET:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;

    invoke-direct {v1, v2}, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;)V

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onBackFromBalance()V
    .locals 2

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onBackFromConfirmTransfer()V
    .locals 2

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logBackFromTransferConfirmationClick()V

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onBackFromTransactionDetail()V
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onBackFromTransferToBank()V
    .locals 2

    .line 379
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logBackFromTransferDetailsClick()V

    .line 380
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onBankAccountSettingsClicked()V
    .locals 1

    .line 543
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logBankAccountSettingsClick()V

    .line 544
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletGateway;->activateBankAccountSettings()V

    return-void
.end method

.method public onCardSpendClicked()V
    .locals 1

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logViewCardActivityClick()V

    return-void
.end method

.method public onDepositRowClicked(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V
    .locals 3

    .line 522
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-direct {v1, v2, p2, p1}, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsDetailBootstrapScreen;-><init>(Lcom/squareup/util/Device;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onDepositsSettingsClickedFromConfirmTransfer()V
    .locals 1

    .line 538
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logDepositSettingsClick()V

    .line 539
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletGateway;->activateDepositsSettings()V

    return-void
.end method

.method public onDoneFromInstantDepositResultClicked()V
    .locals 0

    .line 631
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->closeAllCards()V

    return-void
.end method

.method public onDoneFromTransferResultClicked()V
    .locals 1

    .line 548
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logTransferResultsDoneClick()V

    .line 549
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->closeAllCards()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->squareCardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->orderSquareCardStampsRequester:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onNextClickedFromTransferToBank:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    .line 255
    invoke-virtual {v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v2}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v2

    .line 256
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toTriplet()Lio/reactivex/functions/Function3;

    move-result-object v3

    .line 255
    invoke-virtual {v0, v1, v2, v3}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$oD8HUWTZp2COdbE0mp4giSEmJ2c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$oD8HUWTZp2COdbE0mp4giSEmJ2c;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 257
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandTriplet(Lio/reactivex/functions/Function3;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 254
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->onTransferResult()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onRefreshBalance:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->mergeWith(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$njJwhX779XFub2rPQIFsSi5XL18;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$njJwhX779XFub2rPQIFsSi5XL18;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 263
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 262
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 267
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onTransactionCategoryFailure()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$osEw2tKAFZaICnWGx2qKO2bF8oU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$osEw2tKAFZaICnWGx2qKO2bF8oU;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 272
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 271
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onRefreshBalance:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$gzt8hBfYu_0Yd1VnxZ3KGOOR_tw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$gzt8hBfYu_0Yd1VnxZ3KGOOR_tw;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitFromInstantDepositResultClicked()V
    .locals 0

    .line 635
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->closeAllCards()V

    return-void
.end method

.method public onExitFromTransferResultClicked()V
    .locals 1

    .line 553
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logBackFromTransferResultsClick()V

    .line 554
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->closeAllCards()V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onExpenseTypeClicked(Ljava/lang/String;Z)V
    .locals 1

    .line 420
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->activityRequester:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->updateTransactionCategory(Ljava/lang/String;Z)V

    return-void
.end method

.method public onInstantClicked()V
    .locals 3

    .line 558
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->setTransferSpeed(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V

    .line 559
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v1

    .line 560
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$7dUVjI3BUuC61YDlK20ismStmLw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$7dUVjI3BUuC61YDlK20ismStmLw;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 561
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 559
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onLearnMoreFromInstantDepositResultClicked()V
    .locals 3

    .line 646
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->instant_deposits_unavailable_troubleshooting:I

    .line 647
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 646
    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method public onLinkAccount()V
    .locals 3

    .line 590
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    .line 591
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/banklinking/R$string;->link_bank_account_url:I

    .line 594
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 593
    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onLinkBankAccountResult()V
    .locals 0

    .line 416
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->refreshBalance()V

    return-void
.end method

.method public onLinkCard()V
    .locals 4

    .line 585
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;

    new-instance v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    sget-object v3, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;->TRANSFER_TO_BANK_SCREEN:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;

    invoke-direct {v2, v3}, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onLinkDebitCardResult()V
    .locals 0

    .line 412
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->refreshBalance()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "balanceTransactionDetail"

    .line 288
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;

    if-eqz p1, :cond_1

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onBalanceTransactionDetail:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public onManageSquareCardClicked()V
    .locals 1

    .line 512
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->cardUpseller:Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;->upsellAcknowledged()V

    .line 513
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logManageSquareCardClick()V

    return-void
.end method

.method public onMangeSquareCardResult()V
    .locals 0

    .line 408
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->refreshBalance()V

    return-void
.end method

.method public onNextClickedFromTransferToBank(Lcom/squareup/protos/common/Money;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V
    .locals 2

    .line 528
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onNextClickedFromTransferToBank:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, p1, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onPriceChangeDismissed()V
    .locals 2

    .line 600
    iget-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->goToConfirmTransferScreen:Z

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 602
    iput-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->goToConfirmTransferScreen:Z

    goto :goto_0

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->setIsConfirmingInstantTransfer()V

    :goto_0
    return-void
.end method

.method public onRefreshBalanceClicked()V
    .locals 0

    .line 321
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->refreshBalance()V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onBalanceTransactionDetail:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    const-string v1, "balanceTransactionDetail"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onSectionSelected()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/AppletSection;",
            ">;"
        }
    .end annotation

    .line 504
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->sectionSelectedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onSquareCardUpsellClicked()V
    .locals 2

    .line 639
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->closeAllCards()V

    .line 642
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->sectionSelectedRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->manageSquareCardSection:Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onStandardClicked()V
    .locals 3

    .line 573
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->STANDARD:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->setTransferSpeed(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V

    .line 574
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v1

    .line 575
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$PA4vK5qYDt0ISIqvEoDJSlfMp7c;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$PA4vK5qYDt0ISIqvEoDJSlfMp7c;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 576
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 574
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onTransactionRowClicked(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;)V
    .locals 2

    .line 609
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logCardSpendTransactionDetailsClick()V

    .line 610
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->onBalanceTransactionDetail:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 611
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public onTransferClicked(Ljava/lang/String;)V
    .locals 2

    .line 532
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logTransferConfirmationClick()V

    .line 533
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->deposit(Ljava/lang/String;)V

    .line 534
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferScreen;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public onTransferReportsClicked()V
    .locals 1

    .line 517
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logTransferReportsClick()V

    return-void
.end method

.method public onTransferToBankClicked(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;)V
    .locals 3

    .line 424
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner$2;->$SwitchMap$com$squareup$ui$balance$BalanceMasterScreen$ScreenData$ButtonType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 462
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->onFreezeVerify()V

    .line 463
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->deposits_frozen_verification_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;

    sget-object v1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 453
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositAnalytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletSetUpInstantTransfer()V

    .line 454
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;

    new-instance v1, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;

    sget-object v2, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;->DEPOSITS_APPLET:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;

    invoke-direct {v1, v2}, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;)V

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;-><init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 447
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 448
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->sendInstantDeposit()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 437
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositAnalytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletInstantTransferClick()V

    .line 438
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$ViHxZCxI6MIyMMz0XglyOxoYiws;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$ViHxZCxI6MIyMMz0XglyOxoYiws;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 428
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->analytics:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logStartTransferClick()V

    .line 429
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->resetDepositStatus()V

    .line 432
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public transferResultScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 366
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$A-H9wddPRrZTFSMfv73dpF7HroQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$A-H9wddPRrZTFSMfv73dpF7HroQ;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 367
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPairForFunc(Lkotlin/jvm/functions/Function2;)Lio/reactivex/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$BtHQimYZ9p8pzz-6w9Ctm0EfM2I;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$BtHQimYZ9p8pzz-6w9Ctm0EfM2I;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 374
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$tsJH6gXzU2_XQZsvBjrtruv54WE;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$tsJH6gXzU2_XQZsvBjrtruv54WE;

    .line 375
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public transferToBankScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$FOHiGqKRdagwJXPvJUN7FEVrNeM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/-$$Lambda$BalanceScopeRunner$FOHiGqKRdagwJXPvJUN7FEVrNeM;-><init>(Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;)V

    .line 350
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPairForFunc(Lkotlin/jvm/functions/Function2;)Lio/reactivex/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public updateFee(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 615
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;->transferToBankRequester:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->updateFee(Lcom/squareup/protos/common/Money;)V

    return-void
.end method
