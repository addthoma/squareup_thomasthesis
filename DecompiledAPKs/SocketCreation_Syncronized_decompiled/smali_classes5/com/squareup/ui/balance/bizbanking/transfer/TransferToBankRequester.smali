.class public final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;
.super Ljava/lang/Object;
.source "TransferToBankRequester.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;,
        Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;,
        Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0003<=>B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u0019\u001a\u00020\u0015H\u0002J\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u001e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u001e2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\u0015H\u0002J\u0008\u0010 \u001a\u00020!H\u0002J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0012H\u0002J\u0008\u0010%\u001a\u00020\u0015H\u0002J\u001e\u0010&\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u00152\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020)0(H\u0002J\u0010\u0010*\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u0015H\u0002J\u0010\u0010+\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020-H\u0016J\u0008\u0010.\u001a\u00020\u001bH\u0016J\u000c\u0010/\u001a\u0008\u0012\u0004\u0012\u00020\u001b00J\u0006\u00101\u001a\u00020\u001bJ\u0016\u00102\u001a\u00020\u001b2\u0006\u00103\u001a\u00020\u00122\u0006\u00104\u001a\u000205J\u000e\u00106\u001a\u00020\u001b2\u0006\u00104\u001a\u000205J\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u001500J\u0016\u00107\u001a\u0008\u0012\u0004\u0012\u00020#0\u001e2\u0006\u00103\u001a\u00020\u0012H\u0002J\u000e\u00108\u001a\u00020\u001b2\u0006\u00103\u001a\u00020\u0012J\u0014\u00109\u001a\u00020\u001b*\u00020:2\u0006\u0010;\u001a\u00020\u0010H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
        "Lmortar/Scoped;",
        "instantDepositRunner",
        "Lcom/squareup/instantdeposit/InstantDepositRunner;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "transfersService",
        "Lcom/squareup/balance/core/server/transfers/TransfersService;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "messageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/protos/common/CurrencyCode;)V",
        "depositRequestDisposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "minTransfer",
        "Lcom/squareup/protos/common/Money;",
        "state",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
        "kotlin.jvm.PlatformType",
        "updateFeeDisposable",
        "zero",
        "defaultState",
        "deposit",
        "",
        "idempotenceKey",
        "",
        "Lio/reactivex/Single;",
        "currentState",
        "feeBasisPoints",
        "",
        "getDefaultFeeResponse",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
        "depositAmount",
        "latestState",
        "onDepositFailure",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "onDepositSuccess",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onTransferResult",
        "Lio/reactivex/Observable;",
        "resetDepositStatus",
        "setTransferAmountAndSpeed",
        "transferAmount",
        "transferSpeed",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;",
        "setTransferSpeed",
        "tryToUpdateFee",
        "updateFee",
        "withDisposable",
        "Lio/reactivex/disposables/Disposable;",
        "disposable",
        "DepositStatus",
        "State",
        "TransferSpeed",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final depositRequestDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

.field private final messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final minTransfer:Lcom/squareup/protos/common/Money;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            ">;"
        }
    .end annotation
.end field

.field private final transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;

.field private final updateFeeDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final zero:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "instantDepositRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bizbankService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transfersService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;

    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 49
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->updateFeeDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 50
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->depositRequestDisposable:Lio/reactivex/disposables/SerialDisposable;

    const-wide/16 p1, 0x0

    .line 51
    invoke-static {p1, p2, p6}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->zero:Lcom/squareup/protos/common/Money;

    const-wide/16 p1, 0x64

    .line 52
    invoke-static {p1, p2, p6}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->minTransfer:Lcom/squareup/protos/common/Money;

    .line 54
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->defaultState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(defaultState())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$deposit(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lio/reactivex/Single;
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->deposit(Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDefaultFeeResponse(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->getDefaultFeeResponse(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getMinTransfer$p(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->minTransfer:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public static final synthetic access$latestState(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->latestState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onDepositFailure(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->onDepositFailure(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onDepositSuccess(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->onDepositSuccess(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$tryToUpdateFee(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->tryToUpdateFee(Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final defaultState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 10

    .line 211
    new-instance v9, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    .line 212
    new-instance v0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;-><init>()V

    .line 213
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->feeBasisPoints()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_basis_points(Ljava/lang/Integer;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    move-result-object v0

    .line 214
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->zero:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_fixed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->zero:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_total_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->build()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    move-result-object v3

    const-string v0, "GetInstantDepositFeeDeta\u2026ero)\n            .build()"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->zero:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x39

    const/4 v8, 0x0

    move-object v0, v9

    .line 211
    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v9
.end method

.method private final deposit(Ljava/lang/String;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            ">;"
        }
    .end annotation

    .line 146
    iget-object v0, p2, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityType;->INSTANT_PAYOUT:Lcom/squareup/protos/deposits/BalanceActivityType;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityType;->STANDARD_SPEED_PAYOUT:Lcom/squareup/protos/deposits/BalanceActivityType;

    .line 148
    :goto_0
    new-instance v1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;-><init>()V

    .line 149
    invoke-virtual {v1, p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 150
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "settings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 151
    iget-object v1, p2, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 152
    invoke-virtual {p1, v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->balance_activity_type(Lcom/squareup/protos/deposits/BalanceActivityType;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object p1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->transfersService:Lcom/squareup/balance/core/server/transfers/TransfersService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/transfers/TransfersService;->createTransfer(Lcom/squareup/protos/deposits/CreateTransferRequest;)Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 157
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$2;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo p2, "transfersService.createT\u2026it)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final feeBasisPoints()I
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->instantDepositFeeBasisPoints()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getDefaultFeeResponse(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;
    .locals 2

    .line 203
    new-instance v0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;-><init>()V

    .line 204
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->feeBasisPoints()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_basis_points(Ljava/lang/Integer;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->zero:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_fixed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    move-result-object v0

    .line 206
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->feeBasisPoints()I

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/money/MoneyMath;->multiplyByBasisPoints(Lcom/squareup/protos/common/Money;I)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_total_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;

    move-result-object p1

    .line 207
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->build()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    move-result-object p1

    const-string v0, "GetInstantDepositFeeDeta\u2026ints()))\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final latestState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "state.value!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    return-object v0
.end method

.method private final onDepositFailure(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/deposits/CreateTransferResponse;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;"
        }
    .end annotation

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_error_title:I

    sget-object v2, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onDepositFailure$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->component1()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->component2()Ljava/lang/String;

    move-result-object v6

    .line 179
    sget-object v4, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->FAILURE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v0, p1

    .line 178
    invoke-static/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    return-object p1
.end method

.method private final onDepositSuccess(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;
    .locals 9

    .line 166
    sget-object v4, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->SUCCESS:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x37

    const/4 v8, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    return-object p1
.end method

.method private final tryToUpdateFee(Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
            ">;"
        }
    .end annotation

    .line 188
    new-instance v0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;-><init>()V

    .line 189
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;->deposit_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;

    move-result-object p1

    .line 190
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest$Builder;->build()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;

    move-result-object p1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getInstantDepositFeeDetails(Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 194
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$tryToUpdateFee$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService.getInstan\u2026nt)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final withDisposable(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/SerialDisposable;)V
    .locals 0

    .line 229
    invoke-virtual {p2, p1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public final deposit(Ljava/lang/String;)V
    .locals 10

    const-string v0, "idempotenceKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->latestState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object v1

    sget-object v5, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->IN_FLIGHT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x37

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 114
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$deposit$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "state().take(1)\n        \u2026        .subscribe(state)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->depositRequestDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->withDisposable(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/SerialDisposable;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->updateFeeDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->depositRequestDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method

.method public final onTransferResult()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 74
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onTransferResult$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onTransferResult$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onTransferResult$2;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$onTransferResult$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "state\n        .filter { \u2026) }\n        .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final resetDepositStatus()V
    .locals 10

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->latestState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object v1

    sget-object v5, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->BUILDING:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x37

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final setTransferAmountAndSpeed(Lcom/squareup/protos/common/Money;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V
    .locals 10

    const-string/jumbo v0, "transferAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transferSpeed"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->latestState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3c

    const/4 v9, 0x0

    move-object v2, p2

    move-object v3, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final setTransferSpeed(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V
    .locals 10

    const-string/jumbo v0, "transferSpeed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->latestState()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->copy$default(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final state()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final updateFee(Lcom/squareup/protos/common/Money;)V
    .locals 2

    const-string/jumbo v0, "transferAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 88
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$1;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$2;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 99
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$updateFee$3;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "instantDepositRunner.sna\u2026        .subscribe(state)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->updateFeeDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;->withDisposable(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/SerialDisposable;)V

    return-void
.end method
