.class public Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "BalanceTransactionDetailCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private amountDetail:Lcom/squareup/marketfont/MarketTextView;

.field private date:Lcom/squareup/marketfont/MarketTextView;

.field private description:Lcom/squareup/widgets/MessageView;

.field private expenseType:Lcom/squareup/widgets/CheckableGroup;

.field private resources:Landroid/content/res/Resources;

.field private final runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;

.field private time:Lcom/squareup/marketfont/MarketTextView;

.field private total:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 65
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 66
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transaction_total:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->total:Lcom/squareup/marketfont/MarketTextView;

    .line 67
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transaction_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->description:Lcom/squareup/widgets/MessageView;

    .line 68
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transaction_date:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->date:Lcom/squareup/marketfont/MarketTextView;

    .line 69
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transaction_time:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->time:Lcom/squareup/marketfont/MarketTextView;

    .line 70
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transaction_amount_detail:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->amountDetail:Lcom/squareup/marketfont/MarketTextView;

    .line 71
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transaction_expense_type:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->expenseType:Lcom/squareup/widgets/CheckableGroup;

    return-void
.end method

.method private configureActionBar()V
    .locals 3

    .line 42
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->card_spend_detail:I

    .line 43
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$Sgmyzz9j4Gu5JpWAfwuL1Ulluac;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$Sgmyzz9j4Gu5JpWAfwuL1Ulluac;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;)V

    .line 44
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public static synthetic lambda$zVKATB9wtONHIXic763Dsseilu0(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->onScreenData(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;)V

    return-void
.end method

.method private onScreenData(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->total:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->total:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->description:Lcom/squareup/widgets/MessageView;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->date:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->time:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->amountDetail:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->total:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-boolean v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->isPersonalExpense:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/balance/applet/impl/R$id;->expense_type_personal:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->expense_type_business:I

    .line 57
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->expenseType:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->expenseType:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$BalanceTransactionDetailCoordinator$-WsbPgLff_syG9jHzhnghD6p5Qw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$BalanceTransactionDetailCoordinator$-WsbPgLff_syG9jHzhnghD6p5Qw;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 34
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->resources:Landroid/content/res/Resources;

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->bindViews(Landroid/view/View;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->configureActionBar()V

    .line 37
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$BalanceTransactionDetailCoordinator$pFFLFOqWXpHhWU__PJ-UeG4DXzQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$BalanceTransactionDetailCoordinator$pFFLFOqWXpHhWU__PJ-UeG4DXzQ;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$BalanceTransactionDetailCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;->balanceTransactionDetailData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$BalanceTransactionDetailCoordinator$zVKATB9wtONHIXic763Dsseilu0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/-$$Lambda$BalanceTransactionDetailCoordinator$zVKATB9wtONHIXic763Dsseilu0;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;)V

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onScreenData$1$BalanceTransactionDetailCoordinator(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 59
    sget p2, Lcom/squareup/balance/applet/impl/R$id;->expense_type_personal:I

    if-ne p3, p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 60
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$ScreenData;->transactionToken:Ljava/lang/String;

    invoke-interface {p3, p1, p2}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailScreen$Runner;->onExpenseTypeClicked(Ljava/lang/String;Z)V

    return-void
.end method
