.class public final Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;
.super Lcom/squareup/applet/AppletSection;
.source "SquareCardActivitySection.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;",
        "Lcom/squareup/applet/AppletSection;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection$1;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method
