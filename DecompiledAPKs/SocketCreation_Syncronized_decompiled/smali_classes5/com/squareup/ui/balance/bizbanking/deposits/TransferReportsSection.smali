.class public final Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;
.super Lcom/squareup/applet/AppletSection;
.source "TransferReportsSection.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;",
        "Lcom/squareup/applet/AppletSection;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/util/Device;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsBootstrapScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    new-instance v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {v0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;->getInitialScreen()Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsBootstrapScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsBootstrapScreen;
    .locals 2

    .line 12
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsBootstrapScreen;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsSection;->device:Lcom/squareup/util/Device;

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/deposits/TransferReportsBootstrapScreen;-><init>(Lcom/squareup/util/Device;)V

    return-object v0
.end method
