.class final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1$1;
.super Ljava/lang/Object;
.source "BalanceTransactionsCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1;->invoke(ILjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b8\u0001\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick",
        "com/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$1$4$1$1$1",
        "com/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$bind$3$lambda$1",
        "com/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$create$2$lambda$1$1",
        "com/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$extraItem$lambda$1$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactionsFailed;

.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactionsFailed;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1$1;->$data:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactionsFailed;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 187
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->access$getRunner$p(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$3$1$1;->$data:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactionsFailed;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactionsFailed;->getBatchToken()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;->loadMoreTransactions(Ljava/lang/String;)V

    return-void
.end method
