.class public final Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;
.super Ljava/lang/Object;
.source "BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final balanceActivityMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
            ">;)V"
        }
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->flowProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p2, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->appletProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p3, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p4, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p5, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->runnerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p6, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p7, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->shortDateFormatterProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p8, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->timeFormatterProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p9, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p10, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->localeProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p11, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p12, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p13, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->balanceActivityMapperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
            ">;)",
            "Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;"
        }
    .end annotation

    .line 92
    new-instance v14, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static provideBalanceAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;)Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/ui/balance/BalanceApplet;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/util/Locale;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;",
            ")",
            "Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;"
        }
    .end annotation

    .line 101
    invoke-static/range {p0 .. p12}, Lcom/squareup/ui/balance/BalanceAppletScopeModule;->provideBalanceAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;)Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;
    .locals 14

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->appletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/balance/BalanceApplet;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->shortDateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->timeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->balanceActivityMapperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->provideBalanceAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/balance/BalanceMasterScreen$Runner;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Landroid/content/res/Resources;Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;Lcom/squareup/balance/activity/ui/list/BalanceActivityMapper;)Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_ProvideBalanceAppletSectionsListPresenterFactory;->get()Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method
