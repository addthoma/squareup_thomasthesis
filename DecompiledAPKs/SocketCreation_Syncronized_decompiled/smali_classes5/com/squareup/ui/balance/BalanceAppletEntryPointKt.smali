.class public final Lcom/squareup/ui/balance/BalanceAppletEntryPointKt;
.super Ljava/lang/Object;
.source "BalanceAppletEntryPoint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "LAST_SECTION_NAME_KEY",
        "",
        "determineDefaultSection",
        "Lcom/squareup/applet/AppletSection;",
        "squareCardActivitySection",
        "Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;",
        "unifiedActivitySection",
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final LAST_SECTION_NAME_KEY:Ljava/lang/String; = "last-balance-applet-section-name"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static final synthetic access$determineDefaultSection(Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;)Lcom/squareup/applet/AppletSection;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/balance/BalanceAppletEntryPointKt;->determineDefaultSection(Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;)Lcom/squareup/applet/AppletSection;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getLAST_SECTION_NAME_KEY$p()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/ui/balance/BalanceAppletEntryPointKt;->LAST_SECTION_NAME_KEY:Ljava/lang/String;

    return-object v0
.end method

.method private static final determineDefaultSection(Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;)Lcom/squareup/applet/AppletSection;
    .locals 1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;->showUnifiedActivitySection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    check-cast p1, Lcom/squareup/applet/AppletSection;

    goto :goto_0

    .line 48
    :cond_0
    move-object p1, p0

    check-cast p1, Lcom/squareup/applet/AppletSection;

    :goto_0
    return-object p1
.end method
