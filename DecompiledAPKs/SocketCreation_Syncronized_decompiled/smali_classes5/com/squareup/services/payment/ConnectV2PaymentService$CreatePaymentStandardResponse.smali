.class public final Lcom/squareup/services/payment/ConnectV2PaymentService$CreatePaymentStandardResponse;
.super Lcom/squareup/server/StandardResponse;
.source "ConnectV2PaymentService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/services/payment/ConnectV2PaymentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreatePaymentStandardResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/StandardResponse<",
        "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0013\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0002H\u0014\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/services/payment/ConnectV2PaymentService$CreatePaymentStandardResponse;",
        "Lcom/squareup/server/StandardResponse;",
        "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
        "factory",
        "Lcom/squareup/server/StandardResponse$Factory;",
        "(Lcom/squareup/server/StandardResponse$Factory;)V",
        "isSuccessful",
        "",
        "response",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/server/StandardResponse$Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/StandardResponse$Factory<",
            "Lcom/squareup/protos/connect/v2/CreatePaymentResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/server/StandardResponse;-><init>(Lcom/squareup/server/StandardResponse$Factory;)V

    return-void
.end method


# virtual methods
.method protected isSuccessful(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)Z
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->errors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    const-string v1, "COMPLETED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->payment:Lcom/squareup/protos/connect/v2/Payment;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/Payment;->status:Ljava/lang/String;

    const-string v0, "APPROVED"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic isSuccessful(Ljava/lang/Object;)Z
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/services/payment/ConnectV2PaymentService$CreatePaymentStandardResponse;->isSuccessful(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)Z

    move-result p1

    return p1
.end method
