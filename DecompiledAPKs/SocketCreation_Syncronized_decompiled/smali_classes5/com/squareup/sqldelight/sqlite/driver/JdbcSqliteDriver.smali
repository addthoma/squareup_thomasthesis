.class public final Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;
.super Ljava/lang/Object;
.source "JdbcSqliteDriver.kt"

# interfaces
.implements Lcom/squareup/sqldelight/db/SqlDriver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;,
        Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJdbcSqliteDriver.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JdbcSqliteDriver.kt\ncom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver\n*L\n1#1,149:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001f B\u0011\u0008\u0017\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\n \n*\u0004\u0018\u00010\r0\rH\u0016JB\u0010\u0011\u001a\u00020\u000f2\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u00132\u0019\u0010\u0016\u001a\u0015\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u0017\u00a2\u0006\u0002\u0008\u0019H\u0016\u00a2\u0006\u0002\u0010\u001aJB\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u00132\u0019\u0010\u0016\u001a\u0015\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u0017\u00a2\u0006\u0002\u0008\u0019H\u0016\u00a2\u0006\u0002\u0010\u001dJ\u0008\u0010\u001e\u001a\u00020\rH\u0016R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "properties",
        "Ljava/util/Properties;",
        "(Ljava/util/Properties;)V",
        "url",
        "",
        "(Ljava/lang/String;Ljava/util/Properties;)V",
        "connection",
        "Ljava/sql/Connection;",
        "kotlin.jvm.PlatformType",
        "transactions",
        "Ljava/lang/ThreadLocal;",
        "Lcom/squareup/sqldelight/Transacter$Transaction;",
        "close",
        "",
        "currentTransaction",
        "execute",
        "identifier",
        "",
        "sql",
        "parameters",
        "binders",
        "Lkotlin/Function1;",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "Lkotlin/ExtensionFunctionType;",
        "(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V",
        "executeQuery",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)Lcom/squareup/sqldelight/db/SqlCursor;",
        "newTransaction",
        "Companion",
        "Transaction",
        "sqldelight-sqlite-driver"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Companion;

.field public static final IN_MEMORY:Ljava/lang/String; = "jdbc:sqlite:"


# instance fields
.field private final connection:Ljava/sql/Connection;

.field private final transactions:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/squareup/sqldelight/Transacter$Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->Companion:Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Properties;)V
    .locals 1

    const-string/jumbo v0, "url"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "properties"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1, p2}, Ljava/sql/DriverManager;->getConnection(Ljava/lang/String;Ljava/util/Properties;)Ljava/sql/Connection;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->connection:Ljava/sql/Connection;

    .line 30
    new-instance p1, Ljava/lang/ThreadLocal;

    invoke-direct {p1}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->transactions:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/util/Properties;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 20
    new-instance p2, Ljava/util/Properties;

    invoke-direct {p2}, Ljava/util/Properties;-><init>()V

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;-><init>(Ljava/lang/String;Ljava/util/Properties;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Properties;)V
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->ERROR:Lkotlin/DeprecationLevel;
        message = "Specify connection URL explicitly"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "JdbcSqliteDriver(IN_MEMORY, properties)"
            imports = {}
        .end subannotation
    .end annotation

    const-string v0, "properties"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jdbc:sqlite:"

    .line 27
    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;-><init>(Ljava/lang/String;Ljava/util/Properties;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Properties;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 27
    new-instance p1, Ljava/util/Properties;

    invoke-direct {p1}, Ljava/util/Properties;-><init>()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;-><init>(Ljava/util/Properties;)V

    return-void
.end method

.method public static final synthetic access$getConnection$p(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;)Ljava/sql/Connection;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->connection:Ljava/sql/Connection;

    return-object p0
.end method

.method public static final synthetic access$getTransactions$p(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;)Ljava/lang/ThreadLocal;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->transactions:Ljava/lang/ThreadLocal;

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->connection:Ljava/sql/Connection;

    invoke-interface {v0}, Ljava/sql/Connection;->close()V

    return-void
.end method

.method public currentTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/Transacter$Transaction;

    return-object v0
.end method

.method public execute(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string p1, "sql"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->connection:Ljava/sql/Connection;

    invoke-interface {p1, p2}, Ljava/sql/Connection;->prepareStatement(Ljava/lang/String;)Ljava/sql/PreparedStatement;

    move-result-object p1

    check-cast p1, Ljava/lang/AutoCloseable;

    const/4 p2, 0x0

    check-cast p2, Ljava/lang/Throwable;

    :try_start_0
    move-object p3, p1

    check-cast p3, Ljava/sql/PreparedStatement;

    .line 41
    new-instance v0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;

    const-string v1, "jdbcStatement"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p3}, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;-><init>(Ljava/sql/PreparedStatement;)V

    if-eqz p4, :cond_0

    .line 42
    invoke-interface {p4, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->execute$sqldelight_sqlite_driver()V

    .line 44
    sget-object p3, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    invoke-static {p1, p2}, Lkotlin/jdk7/AutoCloseableKt;->closeFinally(Ljava/lang/AutoCloseable;Ljava/lang/Throwable;)V

    return-void

    :catchall_0
    move-exception p2

    :try_start_1
    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p3

    invoke-static {p1, p2}, Lkotlin/jdk7/AutoCloseableKt;->closeFinally(Ljava/lang/AutoCloseable;Ljava/lang/Throwable;)V

    throw p3
.end method

.method public executeQuery(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)Lcom/squareup/sqldelight/db/SqlCursor;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/sqldelight/db/SqlCursor;"
        }
    .end annotation

    const-string p1, "sql"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance p1, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;

    iget-object p3, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->connection:Ljava/sql/Connection;

    invoke-interface {p3, p2}, Ljava/sql/Connection;->prepareStatement(Ljava/lang/String;)Ljava/sql/PreparedStatement;

    move-result-object p2

    const-string p3, "connection.prepareStatement(sql)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p2}, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;-><init>(Ljava/sql/PreparedStatement;)V

    if-eqz p4, :cond_0

    .line 54
    invoke-interface {p4, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcPreparedStatement;->executeQuery$sqldelight_sqlite_driver()Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;

    move-result-object p1

    check-cast p1, Lcom/squareup/sqldelight/db/SqlCursor;

    return-object p1
.end method

.method public newTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;
    .locals 3

    .line 59
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/Transacter$Transaction;

    .line 60
    new-instance v1, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;

    invoke-direct {v1, p0, v0}, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver$Transaction;-><init>(Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;Lcom/squareup/sqldelight/Transacter$Transaction;)V

    .line 61
    iget-object v2, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/JdbcSqliteDriver;->connection:Ljava/sql/Connection;

    const-string v2, "BEGIN TRANSACTION"

    invoke-interface {v0, v2}, Ljava/sql/Connection;->prepareStatement(Ljava/lang/String;)Ljava/sql/PreparedStatement;

    move-result-object v0

    invoke-interface {v0}, Ljava/sql/PreparedStatement;->execute()Z

    .line 67
    :cond_0
    check-cast v1, Lcom/squareup/sqldelight/Transacter$Transaction;

    return-object v1
.end method
