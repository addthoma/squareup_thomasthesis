.class final Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;
.super Ljava/lang/Object;
.source "JdbcSqliteDriver.kt"

# interfaces
.implements Lcom/squareup/sqldelight/db/SqlCursor;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJdbcSqliteDriver.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JdbcSqliteDriver.kt\ncom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor\n*L\n1#1,149:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0006\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0018\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0017\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u000c\u001a\u00020\rH\u0016\u00a2\u0006\u0002\u0010\u0010J\u0017\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u000c\u001a\u00020\rH\u0016\u00a2\u0006\u0002\u0010\u0013J\u0018\u0010\u0014\u001a\n \u000b*\u0004\u0018\u00010\u00150\u00152\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "preparedStatement",
        "Ljava/sql/PreparedStatement;",
        "resultSet",
        "Ljava/sql/ResultSet;",
        "(Ljava/sql/PreparedStatement;Ljava/sql/ResultSet;)V",
        "close",
        "",
        "getBytes",
        "",
        "kotlin.jvm.PlatformType",
        "index",
        "",
        "getDouble",
        "",
        "(I)Ljava/lang/Double;",
        "getLong",
        "",
        "(I)Ljava/lang/Long;",
        "getString",
        "",
        "next",
        "",
        "sqldelight-sqlite-driver"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final preparedStatement:Ljava/sql/PreparedStatement;

.field private final resultSet:Ljava/sql/ResultSet;


# direct methods
.method public constructor <init>(Ljava/sql/PreparedStatement;Ljava/sql/ResultSet;)V
    .locals 1

    const-string v0, "preparedStatement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultSet"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->preparedStatement:Ljava/sql/PreparedStatement;

    iput-object p2, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    invoke-interface {v0}, Ljava/sql/ResultSet;->close()V

    .line 145
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->preparedStatement:Ljava/sql/PreparedStatement;

    invoke-interface {v0}, Ljava/sql/PreparedStatement;->close()V

    return-void
.end method

.method public getBytes(I)[B
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    add-int/lit8 p1, p1, 0x1

    invoke-interface {v0, p1}, Ljava/sql/ResultSet;->getBytes(I)[B

    move-result-object p1

    return-object p1
.end method

.method public getDouble(I)Ljava/lang/Double;
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    add-int/lit8 p1, p1, 0x1

    invoke-interface {v0, p1}, Ljava/sql/ResultSet;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    invoke-interface {v0}, Ljava/sql/ResultSet;->wasNull()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getLong(I)Ljava/lang/Long;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    add-int/lit8 p1, p1, 0x1

    invoke-interface {v0, p1}, Ljava/sql/ResultSet;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    invoke-interface {v0}, Ljava/sql/ResultSet;->wasNull()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    add-int/lit8 p1, p1, 0x1

    invoke-interface {v0, p1}, Ljava/sql/ResultSet;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public next()Z
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/sqldelight/sqlite/driver/SqliteJdbcCursor;->resultSet:Ljava/sql/ResultSet;

    invoke-interface {v0}, Ljava/sql/ResultSet;->next()Z

    move-result v0

    return v0
.end method
