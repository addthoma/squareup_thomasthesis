.class public final Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory;
.super Ljava/lang/Object;
.source "NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory$InstanceHolder;->access$000()Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSeparatedPrintoutsManager()Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;->provideSeparatedPrintoutsManager()Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory;->provideSeparatedPrintoutsManager()Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsManagerFactory;->get()Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;

    move-result-object v0

    return-object v0
.end method
