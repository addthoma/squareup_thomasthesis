.class public final Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;
.super Ljava/lang/Object;
.source "HasSeparatedPrintouts.kt"

# interfaces
.implements Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NeverHasSeparatedPrintouts"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016J\u0008\u0010\u000c\u001a\u00020\u0004H\u0016J\u0008\u0010\r\u001a\u00020\u0004H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;",
        "Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts;",
        "()V",
        "hasPrintoutsForUnsavedOpenTicket",
        "",
        "order",
        "Lcom/squareup/payment/OrderSnapshot;",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "voidedItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "hasPrintoutsWithNoPaperReceipt",
        "hasPrintoutsWithPaperReceipt",
        "separated-printouts_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;

    invoke-direct {v0}, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;-><init>()V

    sput-object v0, Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;->INSTANCE:Lcom/squareup/separatedprintouts/api/HasSeparatedPrintouts$NeverHasSeparatedPrintouts;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasPrintoutsForUnsavedOpenTicket(Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "openTicket"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "voidedItems"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public hasPrintoutsWithNoPaperReceipt()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasPrintoutsWithPaperReceipt()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
