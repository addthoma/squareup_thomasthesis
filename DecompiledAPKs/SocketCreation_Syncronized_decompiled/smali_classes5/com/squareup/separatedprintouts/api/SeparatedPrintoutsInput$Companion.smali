.class public final Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;
.super Ljava/lang/Object;
.source "SeparatedPrintoutsInput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J6\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u0008H\u0007J\u0018\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u0008H\u0007J \u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u0011\u001a\u00020\u0012H\u0007\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;",
        "",
        "()V",
        "forUnsavedOpenTicket",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "orderSnapshot",
        "Lcom/squareup/payment/OrderSnapshot;",
        "transactionOrderName",
        "",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "voidedItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "uniqueKey",
        "noPaperReceipt",
        "withPaperReceipt",
        "paperReceiptType",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;",
        "separated-printouts_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final forUnsavedOpenTicket(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "orderSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transactionOrderName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicket"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidedItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uniqueKey"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsUnsavedOpenTicketInput;-><init>(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    return-object v0
.end method

.method public final noPaperReceipt(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "transactionOrderName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uniqueKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsReceiptsInput;

    .line 32
    sget-object v1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;->NORMAL:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;

    const/4 v2, 0x0

    .line 27
    invoke-direct {v0, v2, p1, p2, v1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsReceiptsInput;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)V

    check-cast v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    return-object v0
.end method

.method public final withPaperReceipt(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "transactionOrderName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uniqueKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paperReceiptType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsReceiptsInput;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsReceiptsInput;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)V

    check-cast v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    return-object v0
.end method
