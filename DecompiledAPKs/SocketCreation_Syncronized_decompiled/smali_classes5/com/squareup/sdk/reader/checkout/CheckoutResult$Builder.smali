.class public final Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
.super Ljava/lang/Object;
.source "CheckoutResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/CheckoutResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private createdAt:Ljava/util/Date;

.field private locationId:Ljava/lang/String;

.field private final tenders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/Tender;",
            ">;"
        }
    .end annotation
.end field

.field private totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private transactionClientId:Ljava/lang/String;

.field private transactionId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)V
    .locals 3

    .line 210
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->access$900(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->access$1000(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    .line 211
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->access$1100(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/lang/String;

    move-result-object v2

    .line 210
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;-><init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;)V

    .line 212
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->access$1200(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->createdAt:Ljava/util/Date;

    .line 213
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->access$1300(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 214
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->access$1400(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionId:Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->tenders:Ljava/util/Set;

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->access$1500(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/util/Set;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Lcom/squareup/sdk/reader/checkout/CheckoutResult$1;)V
    .locals 0

    .line 190
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;)V
    .locals 2

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->tenders:Ljava/util/Set;

    .line 201
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->locationId:Ljava/lang/String;

    .line 202
    iput-object p3, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionClientId:Ljava/lang/String;

    .line 203
    iput-object p2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    const/4 p1, 0x0

    .line 204
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionId:Ljava/lang/String;

    .line 205
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1}, Ljava/util/Date;-><init>()V

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->createdAt:Ljava/util/Date;

    .line 206
    new-instance p1, Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {p2}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p2

    const-wide/16 v0, 0x0

    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/CheckoutResult$1;)V
    .locals 0

    .line 190
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;-><init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/lang/String;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->locationId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/lang/String;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionClientId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/lang/String;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/util/Date;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/util/Set;
    .locals 0

    .line 190
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->tenders:Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public addTender(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->tenders:Ljava/util/Set;

    const-string v1, "tender"

    invoke-static {p1, v1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/sdk/reader/checkout/CheckoutResult;
    .locals 2

    .line 277
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;Lcom/squareup/sdk/reader/checkout/CheckoutResult$1;)V

    return-object v0
.end method

.method public createdAt(Ljava/util/Date;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    const-string v0, "createdAt"

    .line 256
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Date;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method public locationId(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    const-string v0, "locationId"

    .line 220
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->locationId:Ljava/lang/String;

    return-object p0
.end method

.method public noTenders()Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->tenders:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-object p0
.end method

.method public noTransactionId()Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 244
    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionId:Ljava/lang/String;

    return-object p0
.end method

.method public totalMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    const-string/jumbo v0, "totalMoney"

    .line 226
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public totalTipMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    const-string/jumbo v0, "totalTipMoney"

    .line 250
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public transactionClientId(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    const-string/jumbo v0, "transactionClientId"

    .line 232
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionClientId:Ljava/lang/String;

    return-object p0
.end method

.method public transactionId(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 1

    const-string/jumbo v0, "transactionId"

    .line 238
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionId:Ljava/lang/String;

    return-object p0
.end method
