.class public final Lcom/squareup/sdk/reader/ReaderSdk;
.super Ljava/lang/Object;
.source "ReaderSdk.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static authorizationManager()Lcom/squareup/sdk/reader/authorization/AuthorizationManager;
    .locals 1

    .line 46
    invoke-static {}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/SdkFactory;->authorizationManager()Lcom/squareup/sdk/reader/authorization/AuthorizationManager;

    move-result-object v0

    return-object v0
.end method

.method public static checkoutManager()Lcom/squareup/sdk/reader/checkout/CheckoutManager;
    .locals 1

    .line 61
    invoke-static {}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/SdkFactory;->checkoutManager()Lcom/squareup/sdk/reader/checkout/CheckoutManager;

    move-result-object v0

    return-object v0
.end method

.method public static customerCardManager()Lcom/squareup/sdk/reader/crm/CustomerCardManager;
    .locals 1

    .line 69
    invoke-static {}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/SdkFactory;->customerCardManager()Lcom/squareup/sdk/reader/crm/CustomerCardManager;

    move-result-object v0

    return-object v0
.end method

.method public static initialize(Landroid/app/Application;)V
    .locals 1

    const-string v0, "application"

    .line 37
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 38
    invoke-static {p0}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->onCreate(Landroid/app/Application;)V

    return-void
.end method

.method public static readerManager()Lcom/squareup/sdk/reader/hardware/ReaderManager;
    .locals 1

    .line 53
    invoke-static {}, Lcom/squareup/sdk/reader/internal/AppBootstrapHolder;->getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/sdk/reader/internal/SdkFactory;->readerManager()Lcom/squareup/sdk/reader/hardware/ReaderManager;

    move-result-object v0

    return-object v0
.end method
