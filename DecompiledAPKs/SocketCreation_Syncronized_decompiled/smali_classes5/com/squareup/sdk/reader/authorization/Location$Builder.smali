.class public final Lcom/squareup/sdk/reader/authorization/Location$Builder;
.super Ljava/lang/Object;
.source "Location.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/authorization/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private businessName:Ljava/lang/String;

.field private cardProcessingActivated:Z

.field private currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field private locationId:Ljava/lang/String;

.field private maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/authorization/Location;)V
    .locals 1

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location;->access$900(Lcom/squareup/sdk/reader/authorization/Location;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->locationId:Ljava/lang/String;

    .line 160
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location;->access$1000(Lcom/squareup/sdk/reader/authorization/Location;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 161
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location;->access$1100(Lcom/squareup/sdk/reader/authorization/Location;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->name:Ljava/lang/String;

    .line 162
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location;->access$1200(Lcom/squareup/sdk/reader/authorization/Location;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->businessName:Ljava/lang/String;

    .line 163
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location;->access$1300(Lcom/squareup/sdk/reader/authorization/Location;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->cardProcessingActivated:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/authorization/Location;Lcom/squareup/sdk/reader/authorization/Location$1;)V
    .locals 0

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;-><init>(Lcom/squareup/sdk/reader/authorization/Location;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/CurrencyCode;)V
    .locals 2

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->locationId:Ljava/lang/String;

    .line 150
    iput-object p2, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const-string p1, ""

    .line 151
    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->name:Ljava/lang/String;

    .line 152
    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->businessName:Ljava/lang/String;

    const/4 p1, 0x0

    .line 153
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->cardProcessingActivated:Z

    .line 154
    new-instance p1, Lcom/squareup/sdk/reader/checkout/Money;

    const-wide/16 v0, 0x0

    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 155
    new-instance p1, Lcom/squareup/sdk/reader/checkout/Money;

    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/CurrencyCode;Lcom/squareup/sdk/reader/authorization/Location$1;)V
    .locals 0

    .line 137
    invoke-direct {p0, p1, p2}, Lcom/squareup/sdk/reader/authorization/Location$Builder;-><init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Ljava/lang/String;
    .locals 0

    .line 137
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->locationId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 0

    .line 137
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Ljava/lang/String;
    .locals 0

    .line 137
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Ljava/lang/String;
    .locals 0

    .line 137
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->businessName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Z
    .locals 0

    .line 137
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->cardProcessingActivated:Z

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 137
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 137
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/reader/authorization/Location;
    .locals 2

    .line 218
    new-instance v0, Lcom/squareup/sdk/reader/authorization/Location;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/authorization/Location;-><init>(Lcom/squareup/sdk/reader/authorization/Location$Builder;Lcom/squareup/sdk/reader/authorization/Location$1;)V

    return-object v0
.end method

.method public businessName(Ljava/lang/String;)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 1

    const-string v0, "businessName"

    .line 186
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->businessName:Ljava/lang/String;

    return-object p0
.end method

.method public cardProcessingActivated(Z)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 0

    .line 192
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->cardProcessingActivated:Z

    return-object p0
.end method

.method public currencyCode(Lcom/squareup/sdk/reader/checkout/CurrencyCode;)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 1

    const-string v0, "currencyCode"

    .line 174
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-object p0
.end method

.method public locationId(Ljava/lang/String;)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 1

    const-string v0, "locationId"

    .line 168
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->locationId:Ljava/lang/String;

    return-object p0
.end method

.method public maximumCardPaymentAmountMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 1

    const-string v0, "maximumCardPaymentAmountMoney"

    .line 208
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public minimumCardPaymentAmountMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 1

    const-string v0, "minimumCardPaymentAmountMoney"

    .line 200
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 1

    const-string v0, "name"

    .line 180
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
