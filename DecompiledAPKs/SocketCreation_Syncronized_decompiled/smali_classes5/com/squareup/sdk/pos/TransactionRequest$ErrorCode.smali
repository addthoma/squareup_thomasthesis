.class public final enum Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;
.super Ljava/lang/Enum;
.source "TransactionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/TransactionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum CUSTOMER_MANAGEMENT_NOT_SUPPORTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum DISABLED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum ERROR_INVALID_CUSTOMER_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum GIFT_CARDS_NOT_SUPPORTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum ILLEGAL_LOCATION_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum INSUFFICIENT_CARD_BALANCE:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum INVALID_REQUEST:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum NO_EMPLOYEE_LOGGED_IN:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum NO_NETWORK:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum NO_RESULT:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum TRANSACTION_ALREADY_IN_PROGRESS:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum TRANSACTION_CANCELED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum UNAUTHORIZED_CLIENT_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum UNEXPECTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum UNSUPPORTED_API_VERSION:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum USER_NOT_ACTIVATED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field public static final enum USER_NOT_LOGGED_IN:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

.field private static final errorCodeByApiString:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final apiCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 395
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v1, 0x0

    const-string v2, "DISABLED"

    const-string v3, "com.squareup.pos.ERROR_DISABLED"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->DISABLED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 398
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v2, 0x1

    const-string v3, "CUSTOMER_MANAGEMENT_NOT_SUPPORTED"

    const-string v4, "com.squareup.pos.ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->CUSTOMER_MANAGEMENT_NOT_SUPPORTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 404
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v3, 0x2

    const-string v4, "ERROR_INVALID_CUSTOMER_ID"

    const-string v5, "com.squareup.pos.ERROR_INVALID_CUSTOMER_ID"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->ERROR_INVALID_CUSTOMER_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 407
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v4, 0x3

    const-string v5, "GIFT_CARDS_NOT_SUPPORTED"

    const-string v6, "com.squareup.pos.ERROR_GIFT_CARDS_NOT_SUPPORTED"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->GIFT_CARDS_NOT_SUPPORTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 413
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v5, 0x4

    const-string v6, "ILLEGAL_LOCATION_ID"

    const-string v7, "com.squareup.pos.ERROR_ILLEGAL_LOCATION_ID"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->ILLEGAL_LOCATION_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 420
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v6, 0x5

    const-string v7, "INSUFFICIENT_CARD_BALANCE"

    const-string v8, "com.squareup.pos.ERROR_INSUFFICIENT_CARD_BALANCE"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->INSUFFICIENT_CARD_BALANCE:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 428
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v7, 0x6

    const-string v8, "INVALID_REQUEST"

    const-string v9, "com.squareup.pos.ERROR_INVALID_REQUEST"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->INVALID_REQUEST:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 431
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/4 v8, 0x7

    const-string v9, "NO_EMPLOYEE_LOGGED_IN"

    const-string v10, "com.squareup.pos.ERROR_NO_EMPLOYEE_LOGGED_IN"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->NO_EMPLOYEE_LOGGED_IN:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 437
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v9, 0x8

    const-string v10, "NO_NETWORK"

    const-string v11, "com.squareup.pos.ERROR_NO_NETWORK"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->NO_NETWORK:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 443
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v10, 0x9

    const-string v11, "NO_RESULT"

    const-string v12, "com.squareup.pos.ERROR_NO_RESULT"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->NO_RESULT:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 450
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v11, 0xa

    const-string v12, "TRANSACTION_ALREADY_IN_PROGRESS"

    const-string v13, "com.squareup.pos.ERROR_TRANSACTION_ALREADY_IN_PROGRESS"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->TRANSACTION_ALREADY_IN_PROGRESS:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 453
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v12, 0xb

    const-string v13, "TRANSACTION_CANCELED"

    const-string v14, "com.squareup.pos.ERROR_TRANSACTION_CANCELED"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->TRANSACTION_CANCELED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 459
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v13, 0xc

    const-string v14, "UNAUTHORIZED_CLIENT_ID"

    const-string v15, "com.squareup.pos.ERROR_UNAUTHORIZED_CLIENT_ID"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->UNAUTHORIZED_CLIENT_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 466
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v14, 0xd

    const-string v15, "UNEXPECTED"

    const-string v13, "com.squareup.pos.ERROR_UNEXPECTED"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->UNEXPECTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 473
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v13, 0xe

    const-string v15, "UNSUPPORTED_API_VERSION"

    const-string v14, "com.squareup.pos.UNSUPPORTED_API_VERSION"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->UNSUPPORTED_API_VERSION:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 479
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const-string v14, "USER_NOT_ACTIVATED"

    const/16 v15, 0xf

    const-string v13, "com.squareup.pos.ERROR_USER_NOT_ACTIVATED"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->USER_NOT_ACTIVATED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 482
    new-instance v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const-string v13, "USER_NOT_LOGGED_IN"

    const/16 v14, 0x10

    const-string v15, "com.squareup.pos.ERROR_USER_NOT_LOGGED_IN"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->USER_NOT_LOGGED_IN:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 392
    sget-object v13, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->DISABLED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v13, v0, v1

    sget-object v13, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->CUSTOMER_MANAGEMENT_NOT_SUPPORTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v13, v0, v2

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->ERROR_INVALID_CUSTOMER_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->GIFT_CARDS_NOT_SUPPORTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v4

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->ILLEGAL_LOCATION_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->INSUFFICIENT_CARD_BALANCE:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v6

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->INVALID_REQUEST:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v7

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->NO_EMPLOYEE_LOGGED_IN:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v8

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->NO_NETWORK:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v9

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->NO_RESULT:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v10

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->TRANSACTION_ALREADY_IN_PROGRESS:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v11

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->TRANSACTION_CANCELED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    aput-object v2, v0, v12

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->UNAUTHORIZED_CLIENT_ID:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v3, 0xc

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->UNEXPECTED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v3, 0xd

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->UNSUPPORTED_API_VERSION:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v3, 0xe

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->USER_NOT_ACTIVATED:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v3, 0xf

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->USER_NOT_LOGGED_IN:Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    const/16 v3, 0x10

    aput-object v2, v0, v3

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->$VALUES:[Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    .line 484
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->errorCodeByApiString:Ljava/util/Map;

    .line 493
    invoke-static {}, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->values()[Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 494
    sget-object v4, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->errorCodeByApiString:Ljava/util/Map;

    iget-object v5, v3, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->apiCode:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 488
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 489
    iput-object p3, p0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->apiCode:Ljava/lang/String;

    return-void
.end method

.method static parse(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;
    .locals 1

    .line 499
    sget-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->errorCodeByApiString:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;
    .locals 1

    .line 392
    const-class v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;
    .locals 1

    .line 392
    sget-object v0, Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->$VALUES:[Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    invoke-virtual {v0}, [Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/pos/TransactionRequest$ErrorCode;

    return-object v0
.end method
