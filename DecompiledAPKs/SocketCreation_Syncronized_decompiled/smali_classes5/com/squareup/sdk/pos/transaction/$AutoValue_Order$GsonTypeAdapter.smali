.class public final Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "$AutoValue_Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/$AutoValue_Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter<",
        "Lcom/squareup/sdk/pos/transaction/Order;",
        ">;"
    }
.end annotation


# instance fields
.field private final totalMoneyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final totalTaxMoneyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final totalTipMoneyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 22
    const-class v0, Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalMoneyAdapter:Lcom/google/gson/TypeAdapter;

    .line 23
    const-class v0, Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalTipMoneyAdapter:Lcom/google/gson/TypeAdapter;

    .line 24
    const-class v0, Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalTaxMoneyAdapter:Lcom/google/gson/TypeAdapter;

    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/squareup/sdk/pos/transaction/Order;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 43
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 44
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    return-object v2

    .line 47
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    move-object v0, v2

    move-object v1, v0

    .line 51
    :goto_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 52
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v3

    .line 53
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 54
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_0

    :cond_1
    const/4 v4, -0x1

    .line 57
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    const v6, -0x7665abb7

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-eq v5, v6, :cond_4

    const v6, -0x2b0b4024

    if-eq v5, v6, :cond_3

    const v6, -0xff0c0c7

    if-eq v5, v6, :cond_2

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "totalTaxMoney"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v4, 0x2

    goto :goto_1

    :cond_3
    const-string/jumbo v5, "totalMoney"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v4, 0x0

    goto :goto_1

    :cond_4
    const-string/jumbo v5, "totalTipMoney"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v4, 0x1

    :cond_5
    :goto_1
    if-eqz v4, :cond_8

    if-eq v4, v8, :cond_7

    if-eq v4, v7, :cond_6

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_0

    .line 67
    :cond_6
    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalTaxMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/sdk/pos/transaction/Money;

    goto :goto_0

    .line 63
    :cond_7
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalTipMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/pos/transaction/Money;

    goto :goto_0

    .line 59
    :cond_8
    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v2, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/sdk/pos/transaction/Money;

    goto :goto_0

    .line 75
    :cond_9
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 76
    new-instance p1, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;

    invoke-direct {p1, v2, v0, v1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Order;-><init>(Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;)V

    return-object p1
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/squareup/sdk/pos/transaction/Order;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/squareup/sdk/pos/transaction/Order;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    .line 29
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    return-void

    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    const-string/jumbo v0, "totalMoney"

    .line 33
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 34
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Order;->totalMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string/jumbo v0, "totalTipMoney"

    .line 35
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 36
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalTipMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Order;->totalTipMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string/jumbo v0, "totalTaxMoney"

    .line 37
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 38
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->totalTaxMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Order;->totalTaxMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    return-void
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17
    check-cast p2, Lcom/squareup/sdk/pos/transaction/Order;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Order$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/squareup/sdk/pos/transaction/Order;)V

    return-void
.end method
