.class public abstract Lcom/squareup/sdk/pos/transaction/TenderCashDetails$Builder;
.super Ljava/lang/Object;
.source "TenderCashDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/TenderCashDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;
.end method

.method public abstract buyerTenderedMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/TenderCashDetails$Builder;
.end method

.method public abstract changeBackMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/TenderCashDetails$Builder;
.end method
