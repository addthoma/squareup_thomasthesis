.class public abstract Lcom/squareup/sdk/pos/transaction/TenderCardDetails$Builder;
.super Ljava/lang/Object;
.source "TenderCardDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/TenderCardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/squareup/sdk/pos/transaction/TenderCardDetails;
.end method

.method public abstract card(Lcom/squareup/sdk/pos/transaction/Card;)Lcom/squareup/sdk/pos/transaction/TenderCardDetails$Builder;
.end method

.method public abstract entryMethod(Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;)Lcom/squareup/sdk/pos/transaction/TenderCardDetails$Builder;
.end method
