.class public final Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;
.super Ljava/lang/Object;
.source "StampView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MovingStamp"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStampView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StampView.kt\ncom/squareup/cardcustomizations/stampview/StampView$MovingStamp\n*L\n1#1,505:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0007\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\u000f\u001a\u00020\u0010J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0016\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0003J\u0016\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0003J5\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;",
        "",
        "downLocation",
        "Landroid/graphics/PointF;",
        "stamp",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "secondLocation",
        "pivot",
        "(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V",
        "getDownLocation",
        "()Landroid/graphics/PointF;",
        "getPivot",
        "getSecondLocation",
        "getStamp",
        "()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "bounds",
        "Landroid/graphics/RectF;",
        "component1",
        "component2",
        "component3",
        "component4",
        "computeAngle",
        "",
        "newFirstFinger",
        "newSecondFinger",
        "computeScale",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# instance fields
.field private final downLocation:Landroid/graphics/PointF;

.field private final pivot:Landroid/graphics/PointF;

.field private final secondLocation:Landroid/graphics/PointF;

.field private final stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;


# direct methods
.method public constructor <init>(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 1

    const-string v0, "downLocation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stamp"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    iput-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    iput-object p3, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    iput-object p4, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 439
    move-object p3, v0

    check-cast p3, Landroid/graphics/PointF;

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    .line 440
    move-object p4, v0

    check-cast p4, Landroid/graphics/PointF;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;-><init>(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->copy(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final bounds()Landroid/graphics/RectF;
    .locals 1

    .line 461
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public final component1()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final component2()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    return-object v0
.end method

.method public final component3()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final component4()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final computeAngle(Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 8

    const-string v0, "newFirstFinger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newSecondFinger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    .line 448
    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 449
    iget v2, p2, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    .line 450
    iget v3, p2, Landroid/graphics/PointF;->x:F

    iget v4, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-double v2, v2

    sub-double v4, v0, v2

    const/4 v6, 0x1

    int-to-double v6, v6

    mul-double v0, v0, v2

    add-double/2addr v6, v0

    .line 451
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    neg-double v0, v0

    .line 452
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_1

    const/high16 p1, 0x42b40000    # 90.0f

    return p1

    .line 455
    :cond_1
    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iget p2, p2, Landroid/graphics/PointF;->x:F

    iget p1, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr p2, p1

    mul-float v2, v2, p2

    const/4 p1, 0x0

    int-to-float p1, p1

    cmpg-float p1, v2, p1

    if-gez p1, :cond_2

    double-to-float p1, v0

    const/16 p2, 0xb4

    int-to-float p2, p2

    add-float/2addr p1, p2

    return p1

    :cond_2
    double-to-float p1, v0

    return p1
.end method

.method public final computeScale(Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 1

    const-string v0, "newFirstFinger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newSecondFinger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    invoke-static {p1, p2}, Lcom/squareup/cardcustomizations/geometry/PointKt;->distance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result p1

    iget-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    invoke-static {p2, v0}, Lcom/squareup/cardcustomizations/geometry/PointKt;->distance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result p2

    div-float/2addr p1, p2

    return p1

    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    return p1
.end method

.method public final copy(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;
    .locals 1

    const-string v0, "downLocation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stamp"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;-><init>(Landroid/graphics/PointF;Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    iget-object p1, p1, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDownLocation()Landroid/graphics/PointF;
    .locals 1

    .line 437
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final getPivot()Landroid/graphics/PointF;
    .locals 1

    .line 440
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final getSecondLocation()Landroid/graphics/PointF;
    .locals 1

    .line 439
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final getStamp()Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;
    .locals 1

    .line 438
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MovingStamp(downLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->downLocation:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->stamp:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", secondLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->secondLocation:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pivot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$MovingStamp;->pivot:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
