.class public final Lcom/squareup/blueprint/CreateViewBlock;
.super Lcom/squareup/blueprint/ViewBlock;
.source "CreateViewBlock.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/ViewBlock<",
        "Lcom/squareup/blueprint/ViewsUpdateContext;",
        "TP;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0012\n\u0002\u0010\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B7\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007\u0012\u0014\u0008\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\n\u00a2\u0006\u0002\u0010\rJ \u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\u000f2\u0006\u0010%\u001a\u00020\u000fH\u0016J\u000e\u0010&\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0015J\t\u0010\'\u001a\u00020\u0007H\u00c6\u0003J\t\u0010(\u001a\u00020\u0007H\u00c6\u0003J\u001a\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nH\u00c0\u0003\u00a2\u0006\u0002\u0008*JH\u0010+\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0014\u0008\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nH\u00c6\u0001\u00a2\u0006\u0002\u0010,J\u001a\u0010-\u001a\u00020\"2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nJ\u0013\u0010/\u001a\u00020\u00072\u0008\u00100\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u00101\u001a\u00020\u000fH\u00d6\u0001J\t\u00102\u001a\u000203H\u00d6\u0001R\u001a\u0010\u000e\u001a\u00020\u000fX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0016\u001a\u0004\u0008\u0014\u0010\u0015R&\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u001a\u0010\u0006\u001a\u00020\u0007X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u0008\u001a\u00020\u0007X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001f\u0010\u001c\"\u0004\u0008 \u0010\u001e\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/blueprint/CreateViewBlock;",
        "P",
        "",
        "Lcom/squareup/blueprint/ViewBlock;",
        "Lcom/squareup/blueprint/ViewsUpdateContext;",
        "params",
        "wrapHorizontally",
        "",
        "wrapVertically",
        "viewFactory",
        "Lkotlin/Function1;",
        "Landroid/content/Context;",
        "Landroid/view/View;",
        "(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;)V",
        "id",
        "",
        "getId",
        "()I",
        "setId",
        "(I)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getViewFactory$blueprint_core_release",
        "()Lkotlin/jvm/functions/Function1;",
        "setViewFactory$blueprint_core_release",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getWrapHorizontally",
        "()Z",
        "setWrapHorizontally",
        "(Z)V",
        "getWrapVertically",
        "setWrapVertically",
        "buildView",
        "",
        "updateContext",
        "width",
        "height",
        "component1",
        "component2",
        "component3",
        "component4",
        "component4$blueprint_core_release",
        "copy",
        "(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;)Lcom/squareup/blueprint/CreateViewBlock;",
        "create",
        "block",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private id:I

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private viewFactory:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private wrapHorizontally:Z

.field private wrapVertically:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Lcom/squareup/blueprint/ViewBlock;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/CreateViewBlock;->params:Ljava/lang/Object;

    iput-boolean p2, p0, Lcom/squareup/blueprint/CreateViewBlock;->wrapHorizontally:Z

    iput-boolean p3, p0, Lcom/squareup/blueprint/CreateViewBlock;->wrapVertically:Z

    iput-object p4, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    const/4 p1, -0x1

    .line 20
    iput p1, p0, Lcom/squareup/blueprint/CreateViewBlock;->id:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x1

    if-eqz p6, :cond_0

    const/4 p2, 0x1

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    const/4 p3, 0x1

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    .line 17
    sget-object p4, Lcom/squareup/blueprint/CreateViewBlock$1;->INSTANCE:Lcom/squareup/blueprint/CreateViewBlock$1;

    check-cast p4, Lkotlin/jvm/functions/Function1;

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/blueprint/CreateViewBlock;-><init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/CreateViewBlock;Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/blueprint/CreateViewBlock;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapHorizontally()Z

    move-result p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapVertically()Z

    move-result p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/blueprint/CreateViewBlock;->copy(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;)Lcom/squareup/blueprint/CreateViewBlock;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic buildView(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/blueprint/ViewsUpdateContext;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/CreateViewBlock;->buildView(Lcom/squareup/blueprint/ViewsUpdateContext;II)V

    return-void
.end method

.method public buildView(Lcom/squareup/blueprint/ViewsUpdateContext;II)V
    .locals 1

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, p2, p3}, Lcom/squareup/blueprint/ViewsUpdateContext;->buildView(Lkotlin/jvm/functions/Function1;II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/CreateViewBlock;->setId(I)V

    return-void
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapHorizontally()Z

    move-result v0

    return v0
.end method

.method public final component3()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapVertically()Z

    move-result v0

    return v0
.end method

.method public final component4$blueprint_core_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;)Lcom/squareup/blueprint/CreateViewBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)",
            "Lcom/squareup/blueprint/CreateViewBlock<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/CreateViewBlock;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/blueprint/CreateViewBlock;-><init>(Ljava/lang/Object;ZZLkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public final create(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/CreateViewBlock;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/CreateViewBlock;

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/CreateViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapHorizontally()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapHorizontally()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapVertically()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapVertically()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getId()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->id:I

    return v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final getViewFactory$blueprint_core_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getWrapHorizontally()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->wrapHorizontally:Z

    return v0
.end method

.method public getWrapVertically()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/blueprint/CreateViewBlock;->wrapVertically:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapHorizontally()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapVertically()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public setId(I)V
    .locals 0

    .line 20
    iput p1, p0, Lcom/squareup/blueprint/CreateViewBlock;->id:I

    return-void
.end method

.method public final setViewFactory$blueprint_core_release(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public setWrapHorizontally(Z)V
    .locals 0

    .line 15
    iput-boolean p1, p0, Lcom/squareup/blueprint/CreateViewBlock;->wrapHorizontally:Z

    return-void
.end method

.method public setWrapVertically(Z)V
    .locals 0

    .line 16
    iput-boolean p1, p0, Lcom/squareup/blueprint/CreateViewBlock;->wrapVertically:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateViewBlock(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", wrapHorizontally="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapHorizontally()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", wrapVertically="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/CreateViewBlock;->getWrapVertically()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", viewFactory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/CreateViewBlock;->viewFactory:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
