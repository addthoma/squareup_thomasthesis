.class public final Lcom/squareup/blueprint/LinearBlock$Params;
.super Ljava/lang/Object;
.source "LinearBlock.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blueprint/LinearBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Params"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000e\u0010\u0014\u001a\u00020\u0006H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0015J\'\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00032\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\t\"\u0004\u0008\r\u0010\u000bR\u001a\u0010\u0005\u001a\u00020\u0006X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        "",
        "extend",
        "",
        "definesSize",
        "spacing",
        "Lcom/squareup/resources/DimenModel;",
        "(ZZLcom/squareup/resources/DimenModel;)V",
        "getDefinesSize",
        "()Z",
        "setDefinesSize",
        "(Z)V",
        "getExtend",
        "setExtend",
        "getSpacing$blueprint_core_release",
        "()Lcom/squareup/resources/DimenModel;",
        "setSpacing$blueprint_core_release",
        "(Lcom/squareup/resources/DimenModel;)V",
        "component1",
        "component2",
        "component3",
        "component3$blueprint_core_release",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private definesSize:Z

.field private extend:Z

.field private spacing:Lcom/squareup/resources/DimenModel;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/blueprint/LinearBlock$Params;-><init>(ZZLcom/squareup/resources/DimenModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZLcom/squareup/resources/DimenModel;)V
    .locals 1

    const-string v0, "spacing"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    iput-boolean p2, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    iput-object p3, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public synthetic constructor <init>(ZZLcom/squareup/resources/DimenModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 49
    invoke-static {v0}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object p3

    check-cast p3, Lcom/squareup/resources/DimenModel;

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/blueprint/LinearBlock$Params;-><init>(ZZLcom/squareup/resources/DimenModel;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/LinearBlock$Params;ZZLcom/squareup/resources/DimenModel;ILjava/lang/Object;)Lcom/squareup/blueprint/LinearBlock$Params;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/LinearBlock$Params;->copy(ZZLcom/squareup/resources/DimenModel;)Lcom/squareup/blueprint/LinearBlock$Params;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    return v0
.end method

.method public final component3$blueprint_core_release()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final copy(ZZLcom/squareup/resources/DimenModel;)Lcom/squareup/blueprint/LinearBlock$Params;
    .locals 1

    const-string v0, "spacing"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/blueprint/LinearBlock$Params;-><init>(ZZLcom/squareup/resources/DimenModel;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/LinearBlock$Params;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/LinearBlock$Params;

    iget-boolean v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    iget-boolean v1, p1, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    iget-boolean v1, p1, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    iget-object p1, p1, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDefinesSize()Z
    .locals 1

    .line 48
    iget-boolean v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    return v0
.end method

.method public final getExtend()Z
    .locals 1

    .line 47
    iget-boolean v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    return v0
.end method

.method public final getSpacing$blueprint_core_release()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final setDefinesSize(Z)V
    .locals 0

    .line 48
    iput-boolean p1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    return-void
.end method

.method public final setExtend(Z)V
    .locals 0

    .line 47
    iput-boolean p1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    return-void
.end method

.method public final setSpacing$blueprint_core_release(Lcom/squareup/resources/DimenModel;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Params(extend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->extend:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", definesSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->definesSize:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", spacing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/LinearBlock$Params;->spacing:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
