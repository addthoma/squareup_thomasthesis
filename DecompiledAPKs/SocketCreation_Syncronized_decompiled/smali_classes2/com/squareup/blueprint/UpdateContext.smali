.class public abstract Lcom/squareup/blueprint/UpdateContext;
.super Ljava/lang/Object;
.source "UpdateContext.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUpdateContext.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UpdateContext.kt\ncom/squareup/blueprint/UpdateContext\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,106:1\n1313#2:107\n1382#2,3:108\n1313#2:111\n1382#2,3:112\n1591#2,2:115\n*E\n*S KotlinDebug\n*F\n+ 1 UpdateContext.kt\ncom/squareup/blueprint/UpdateContext\n*L\n61#1:107\n61#1,3:108\n62#1:111\n62#1,3:112\n67#1,2:115\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010!\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\t\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0012J\u0006\u0010\u0018\u001a\u00020\u0012J\u0008\u0010\u0019\u001a\u00020\u0015H\u0016J\u0014\u0010\u001a\u001a\u00020\u00152\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00080\rJ\u000e\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u001e\u0010\t\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u0008@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00080\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/blueprint/UpdateContext;",
        "",
        "constraintLayout",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "(Landroidx/constraintlayout/widget/ConstraintLayout;)V",
        "getConstraintLayout",
        "()Landroidx/constraintlayout/widget/ConstraintLayout;",
        "<set-?>",
        "Landroidx/constraintlayout/widget/ConstraintSet;",
        "constraintSet",
        "getConstraintSet",
        "()Landroidx/constraintlayout/widget/ConstraintSet;",
        "constraintSetFactory",
        "Lkotlin/Function0;",
        "fakeIds",
        "",
        "helperViews",
        "",
        "",
        "nextId",
        "applyConstraintSet",
        "",
        "createView",
        "id",
        "generateViewId",
        "reset",
        "useConstraintSetFactory",
        "factory",
        "useFakeIds",
        "startId",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private constraintSet:Landroidx/constraintlayout/widget/ConstraintSet;

.field private constraintSetFactory:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Landroidx/constraintlayout/widget/ConstraintSet;",
            ">;"
        }
    .end annotation
.end field

.field private fakeIds:Z

.field private final helperViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private nextId:I


# direct methods
.method public constructor <init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 1

    const-string v0, "constraintLayout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/UpdateContext;->constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 18
    sget-object p1, Lcom/squareup/blueprint/UpdateContext$constraintSetFactory$1;->INSTANCE:Lcom/squareup/blueprint/UpdateContext$constraintSetFactory$1;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    iput-object p1, p0, Lcom/squareup/blueprint/UpdateContext;->constraintSetFactory:Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x1

    .line 21
    iput p1, p0, Lcom/squareup/blueprint/UpdateContext;->nextId:I

    .line 36
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/blueprint/UpdateContext;->helperViews:Ljava/util/List;

    .line 37
    new-instance p1, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {p1}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/UpdateContext;->constraintSet:Landroidx/constraintlayout/widget/ConstraintSet;

    return-void
.end method


# virtual methods
.method public final applyConstraintSet()V
    .locals 4

    .line 67
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->helperViews:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 115
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 68
    new-instance v2, Landroid/view/View;

    iget-object v3, p0, Lcom/squareup/blueprint/UpdateContext;->constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v3}, Landroidx/constraintlayout/widget/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 69
    invoke-virtual {v2, v1}, Landroid/view/View;->setId(I)V

    const/16 v1, -0x100

    .line 70
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 72
    iget-object v1, p0, Lcom/squareup/blueprint/UpdateContext;->constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v1, v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->constraintSet:Landroidx/constraintlayout/widget/ConstraintSet;

    iget-object v1, p0, Lcom/squareup/blueprint/UpdateContext;->constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method public final createView(I)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->helperViews:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final generateViewId()I
    .locals 2

    .line 50
    iget-boolean v0, p0, Lcom/squareup/blueprint/UpdateContext;->fakeIds:Z

    if-eqz v0, :cond_0

    .line 51
    iget v0, p0, Lcom/squareup/blueprint/UpdateContext;->nextId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/squareup/blueprint/UpdateContext;->nextId:I

    goto :goto_0

    .line 53
    :cond_0
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    :goto_0
    return v0
.end method

.method public final getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method public final getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->constraintSet:Landroidx/constraintlayout/widget/ConstraintSet;

    return-object v0
.end method

.method public reset()V
    .locals 5

    .line 59
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->constraintSetFactory:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintSet;

    iput-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->constraintSet:Landroidx/constraintlayout/widget/ConstraintSet;

    .line 60
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->helperViews:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 107
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 108
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 109
    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 61
    iget-object v4, p0, Lcom/squareup/blueprint/UpdateContext;->constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v4, v3}, Landroidx/constraintlayout/widget/ConstraintLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 62
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 111
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 112
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 113
    check-cast v2, Landroid/view/View;

    .line 62
    invoke-virtual {v0, v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->removeView(Landroid/view/View;)V

    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 114
    :cond_1
    check-cast v3, Ljava/util/List;

    .line 63
    iget-object v0, p0, Lcom/squareup/blueprint/UpdateContext;->helperViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final useConstraintSetFactory(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Landroidx/constraintlayout/widget/ConstraintSet;",
            ">;)V"
        }
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/squareup/blueprint/UpdateContext;->constraintSetFactory:Lkotlin/jvm/functions/Function0;

    .line 33
    invoke-virtual {p0}, Lcom/squareup/blueprint/UpdateContext;->reset()V

    return-void
.end method

.method public final useFakeIds(I)V
    .locals 0

    .line 25
    iput p1, p0, Lcom/squareup/blueprint/UpdateContext;->nextId:I

    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/squareup/blueprint/UpdateContext;->fakeIds:Z

    return-void
.end method
