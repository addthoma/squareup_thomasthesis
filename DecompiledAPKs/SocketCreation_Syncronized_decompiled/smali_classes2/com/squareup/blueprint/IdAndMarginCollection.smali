.class public final Lcom/squareup/blueprint/IdAndMarginCollection;
.super Ljava/lang/Object;
.source "IdsAndMargins.kt"

# interfaces
.implements Lcom/squareup/blueprint/IdsAndMargins;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIdsAndMargins.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IdsAndMargins.kt\ncom/squareup/blueprint/IdAndMarginCollection\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,286:1\n1084#2,2:287\n1591#3:289\n682#3,10:290\n1600#3,2:300\n692#3,2:302\n1602#3:304\n694#3:305\n1592#3:306\n*E\n*S KotlinDebug\n*F\n+ 1 IdsAndMargins.kt\ncom/squareup/blueprint/IdAndMarginCollection\n*L\n205#1,2:287\n243#1:289\n243#1,10:290\n243#1,2:300\n243#1,2:302\n243#1:304\n243#1:305\n243#1:306\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010#\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u001f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\u0016\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0003J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\"\u0010\u001a\u001a\u00020\u00152\u0018\u0010\u001b\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00150\u001cH\u0016J&\u0010\u001d\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00192\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001e2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002J\u0011\u0010\u001f\u001a\u00020\u00152\u0006\u0010 \u001a\u00020\u0001H\u0086\u0002J\u0008\u0010!\u001a\u00020\"H\u0016J\u0010\u0010#\u001a\u00020\u00002\u0006\u0010$\u001a\u00020\u0003H\u0016R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00030\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u000eR\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0011\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/blueprint/IdAndMarginCollection;",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "side",
        "",
        "id",
        "margin",
        "(III)V",
        "(I)V",
        "distinctMargins",
        "",
        "ids",
        "",
        "isEmpty",
        "",
        "()Z",
        "margins",
        "getSide",
        "()I",
        "size",
        "getSize",
        "add",
        "",
        "asIdAndMargin",
        "Lcom/squareup/blueprint/IdAndMargin;",
        "context",
        "Lcom/squareup/blueprint/UpdateContext;",
        "forEach",
        "block",
        "Lkotlin/Function2;",
        "idOrBarrier",
        "",
        "plusAssign",
        "idsAndMargins",
        "toString",
        "",
        "withExtraMargin",
        "delta",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final distinctMargins:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final margins:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final side:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->side:I

    .line 179
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/blueprint/SideExtensionsKt;->isAValidSide(I)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 182
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->ids:Ljava/util/List;

    .line 183
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->margins:Ljava/util/List;

    .line 184
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->distinctMargins:Ljava/util/Set;

    return-void

    .line 179
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Side: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " is not valid."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 174
    invoke-direct {p0, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 175
    invoke-virtual {p0, p2, p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->add(II)V

    return-void
.end method

.method private final idOrBarrier(Lcom/squareup/blueprint/UpdateContext;Ljava/util/List;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/UpdateContext;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;I)I"
        }
    .end annotation

    .line 270
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->generateViewId()I

    move-result v0

    const/4 v1, 0x6

    const/4 v2, 0x3

    if-eq p3, v2, :cond_4

    const/4 v3, 0x4

    if-eq p3, v3, :cond_3

    if-eq p3, v1, :cond_2

    const/4 v2, 0x7

    if-ne p3, v2, :cond_1

    goto :goto_0

    .line 276
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Side is not recognized: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    const/4 v1, 0x5

    goto :goto_0

    :cond_3
    const/4 v1, 0x3

    goto :goto_0

    :cond_4
    const/4 v1, 0x2

    .line 278
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object p1

    check-cast p2, Ljava/util/Collection;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->toIntArray(Ljava/util/Collection;)[I

    move-result-object p2

    array-length p3, p2

    invoke-static {p2, p3}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object p2

    invoke-virtual {p1, v0, v1, p2}, Landroidx/constraintlayout/widget/ConstraintSet;->createBarrier(II[I)V

    move p1, v0

    :goto_1
    return p1
.end method


# virtual methods
.method public final add(II)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->ids:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object p1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->margins:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object p1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->distinctMargins:Ljava/util/Set;

    check-cast p1, Ljava/util/Collection;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;
    .locals 12

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->distinctMargins:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_5

    .line 242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 243
    iget-object v3, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->distinctMargins:Ljava/util/Set;

    check-cast v3, Ljava/lang/Iterable;

    .line 289
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 244
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->generateViewId()I

    move-result v11

    .line 245
    invoke-virtual {p1, v11}, Lcom/squareup/blueprint/UpdateContext;->createView(I)V

    .line 246
    iget-object v5, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->ids:Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 290
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    check-cast v6, Ljava/util/Collection;

    .line 301
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v7, 0x0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    add-int/lit8 v9, v7, 0x1

    if-gez v7, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    .line 302
    :cond_0
    move-object v10, v8

    check-cast v10, Ljava/lang/Number;

    invoke-virtual {v10}, Ljava/lang/Number;->intValue()I

    .line 246
    iget-object v10, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->margins:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    if-ne v7, v4, :cond_1

    const/4 v7, 0x1

    goto :goto_2

    :cond_1
    const/4 v7, 0x0

    :goto_2
    if-eqz v7, :cond_2

    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_2
    move v7, v9

    goto :goto_1

    .line 305
    :cond_3
    check-cast v6, Ljava/util/List;

    .line 247
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v4

    invoke-direct {p0, p1, v6, v4}, Lcom/squareup/blueprint/IdAndMarginCollection;->idOrBarrier(Lcom/squareup/blueprint/UpdateContext;Ljava/util/List;I)I

    move-result v8

    .line 248
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v5

    .line 250
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v4

    invoke-static {v4}, Lcom/squareup/blueprint/SideExtensionsKt;->getOppositeSide(I)I

    move-result v7

    .line 252
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v9

    const/4 v10, 0x0

    move v6, v11

    .line 248
    invoke-static/range {v5 .. v10}, Lcom/squareup/blueprint/UpdateContextKt;->connectAndLog(Landroidx/constraintlayout/widget/ConstraintSet;IIIII)V

    .line 255
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;->idOrBarrier(Lcom/squareup/blueprint/UpdateContext;Ljava/util/List;I)I

    move-result p1

    .line 259
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v1

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    return-object v0

    .line 236
    :cond_5
    iget-object v0, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->ids:Ljava/util/List;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;->idOrBarrier(Lcom/squareup/blueprint/UpdateContext;Ljava/util/List;I)I

    move-result p1

    .line 237
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v1

    iget-object v3, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->margins:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    return-object v0

    .line 234
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "IdAndMargins with no data."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public connectTo(IIILcom/squareup/blueprint/UpdateContext;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;->connectTo(Lcom/squareup/blueprint/IdsAndMargins;IIILcom/squareup/blueprint/UpdateContext;)V

    return-void
.end method

.method public connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V
    .locals 1

    const-string v0, "hook"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    invoke-static {p0, p1, p2}, Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;->connectTo(Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    return-void
.end method

.method public forEach(Lkotlin/jvm/functions/Function2;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->ids:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->margins:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 287
    invoke-interface {v0}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-virtual {v1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 205
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v2, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getSide()I
    .locals 1

    .line 172
    iget v0, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->side:I

    return v0
.end method

.method public final getSize()I
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->ids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 208
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSize()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNotEmpty()Z
    .locals 1

    .line 172
    invoke-static {p0}, Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;->isNotEmpty(Lcom/squareup/blueprint/IdsAndMargins;)Z

    move-result v0

    return v0
.end method

.method public final plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V
    .locals 2

    const-string v0, "idsAndMargins"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    invoke-interface {p1}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 212
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection$plusAssign$2;

    invoke-direct {v0, p0}, Lcom/squareup/blueprint/IdAndMarginCollection$plusAssign$2;-><init>(Lcom/squareup/blueprint/IdAndMarginCollection;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-interface {p1, v0}, Lcom/squareup/blueprint/IdsAndMargins;->forEach(Lkotlin/jvm/functions/Function2;)V

    return-void

    .line 211
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "You cannot add ids from different sides."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IdAndMarginCollection(ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", margins="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/IdAndMarginCollection;->margins:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withExtraMargin(I)Lcom/squareup/blueprint/IdAndMarginCollection;
    .locals 2

    if-eqz p1, :cond_0

    .line 196
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMarginCollection;->getSide()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 198
    new-instance v1, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;

    invoke-direct {v1, v0, p0, p1}, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;-><init>(Lcom/squareup/blueprint/IdAndMarginCollection;Lcom/squareup/blueprint/IdAndMarginCollection;I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;->forEach(Lkotlin/jvm/functions/Function2;)V

    goto :goto_0

    :cond_0
    move-object v0, p0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 0

    .line 172
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->withExtraMargin(I)Lcom/squareup/blueprint/IdAndMarginCollection;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method
