.class public final Lcom/squareup/blueprint/CenterBlock;
.super Lcom/squareup/blueprint/Block;
.source "CenterBlock.kt"

# interfaces
.implements Lcom/squareup/blueprint/BlueprintContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blueprint/CenterBlock$Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/Block<",
        "TC;TP;>;",
        "Lcom/squareup/blueprint/BlueprintContext<",
        "TC;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCenterBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CenterBlock.kt\ncom/squareup/blueprint/CenterBlock\n+ 2 Block.kt\ncom/squareup/blueprint/ChainInfo\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,296:1\n204#2:297\n205#2:300\n204#2:301\n205#2:304\n1591#3,2:298\n1591#3,2:302\n*E\n*S KotlinDebug\n*F\n+ 1 CenterBlock.kt\ncom/squareup/blueprint/CenterBlock\n*L\n146#1:297\n146#1:300\n203#1:301\n203#1:304\n146#1,2:298\n203#1,2:302\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u00052\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020\u00070\u0006:\u0001@B-\u0012\u0006\u0010\u0008\u001a\u00028\u0001\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0016\u0008\u0002\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u000cJ\u001c\u0010\u001d\u001a\u00020\u00072\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0005H\u0016J%\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00028\u00002\u0006\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\u0017H\u0016\u00a2\u0006\u0002\u0010\"J \u0010#\u001a\u00020\u00172\u0006\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u0017H\u0016J \u0010(\u001a\u00020\u00172\u0006\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u0017H\u0016J\u000e\u0010)\u001a\u00028\u0001H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0019J\t\u0010*\u001a\u00020\nH\u00c6\u0003J\u0017\u0010+\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005H\u00c2\u0003J \u0010,\u001a\u00020-2\u0006\u0010$\u001a\u00020\u00022\u0006\u0010.\u001a\u00020-2\u0006\u0010/\u001a\u000200H\u0016J \u00101\u001a\u00020-2\u0006\u0010$\u001a\u00020\u00022\u0006\u0010.\u001a\u00020-2\u0006\u0010/\u001a\u000202H\u0016JF\u00103\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0008\u0008\u0002\u0010\u0008\u001a\u00028\u00012\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0016\u0008\u0002\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005H\u00c6\u0001\u00a2\u0006\u0002\u00104J\u0010\u00105\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u0002H\u0002J\u0008\u00106\u001a\u00020\u0007H\u0016J\u0013\u00107\u001a\u00020\u000e2\u0008\u00108\u001a\u0004\u0018\u00010\u0004H\u00d6\u0003J\t\u00109\u001a\u00020\u0017H\u00d6\u0001J\u0018\u0010:\u001a\u00020\u000e2\u000e\u0010;\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005H\u0016J\u0010\u0010<\u001a\u00020-2\u0006\u0010$\u001a\u00020\u0002H\u0016J\t\u0010=\u001a\u00020>H\u00d6\u0001J\u0010\u0010?\u001a\u00020-2\u0006\u0010$\u001a\u00020\u0002H\u0016R\u0014\u0010\r\u001a\u00020\u000eX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u000eX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0010R \u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\u00028\u0001X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u001a\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/blueprint/CenterBlock;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "Lcom/squareup/blueprint/Block;",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "",
        "params",
        "type",
        "Lcom/squareup/blueprint/CenterBlock$Type;",
        "initialElement",
        "(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;)V",
        "dependableHorizontally",
        "",
        "getDependableHorizontally",
        "()Z",
        "dependableVertically",
        "getDependableVertically",
        "element",
        "getElement",
        "()Lcom/squareup/blueprint/Block;",
        "helperViewId",
        "",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getType",
        "()Lcom/squareup/blueprint/CenterBlock$Type;",
        "add",
        "buildViews",
        "updateContext",
        "width",
        "height",
        "(Lcom/squareup/blueprint/UpdateContext;II)V",
        "chainHorizontally",
        "context",
        "chainInfo",
        "Lcom/squareup/blueprint/ChainInfo;",
        "previousMargin",
        "chainVertically",
        "component1",
        "component2",
        "component3",
        "connectHorizontally",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "previousIds",
        "align",
        "Lcom/squareup/blueprint/HorizontalAlign;",
        "connectVertically",
        "Lcom/squareup/blueprint/VerticalAlign;",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;)Lcom/squareup/blueprint/CenterBlock;",
        "createHelperViewIfNeeded",
        "createParams",
        "equals",
        "other",
        "hashCode",
        "isSameLayoutAs",
        "block",
        "startIds",
        "toString",
        "",
        "topIds",
        "Type",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dependableHorizontally:Z

.field private final dependableVertically:Z

.field private helperViewId:I

.field private initialElement:Lcom/squareup/blueprint/Block;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private final type:Lcom/squareup/blueprint/CenterBlock$Type;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/blueprint/CenterBlock$Type;",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0}, Lcom/squareup/blueprint/Block;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/CenterBlock;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    iput-object p3, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    const/4 p1, -0x1

    .line 278
    iput p1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 67
    check-cast p3, Lcom/squareup/blueprint/Block;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private final component3()Lcom/squareup/blueprint/Block;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/CenterBlock;Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILjava/lang/Object;)Lcom/squareup/blueprint/CenterBlock;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/CenterBlock;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/CenterBlock;->copy(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;)Lcom/squareup/blueprint/CenterBlock;

    move-result-object p0

    return-object p0
.end method

.method private final createHelperViewIfNeeded(Lcom/squareup/blueprint/UpdateContext;)V
    .locals 3

    .line 287
    iget v0, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 288
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->generateViewId()I

    move-result v0

    iput v0, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    .line 289
    iget v0, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    invoke-virtual {p1, v0}, Lcom/squareup/blueprint/UpdateContext;->createView(I)V

    .line 291
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v0

    iget v1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 292
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object p1

    iget v0, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    invoke-virtual {p1, v0, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    :cond_0
    return-void
.end method

.method private final getElement()Lcom/squareup/blueprint/Block;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public add(Lcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "element"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iput-object p1, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    return-void
.end method

.method public buildViews(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v1}, Lcom/squareup/blueprint/CenterBlock$Type;->getHorizontally()Z

    move-result v1

    const/4 v2, -0x2

    if-eqz v1, :cond_0

    const/4 p2, -0x2

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v1}, Lcom/squareup/blueprint/CenterBlock$Type;->getVertically()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p3, -0x2

    .line 99
    :cond_1
    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->buildViews(Lcom/squareup/blueprint/UpdateContext;II)V

    return-void
.end method

.method public chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v0}, Lcom/squareup/blueprint/CenterBlock$Type;->getHorizontally()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result p1

    return p1

    .line 240
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/blueprint/CenterBlock;->createHelperViewIfNeeded(Lcom/squareup/blueprint/UpdateContext;)V

    .line 242
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v0

    iget v1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 244
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    iget v1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v3, 0x6

    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 245
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v1

    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v3, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    invoke-virtual {v1, p1, v0, v3}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    .line 246
    iget v1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v3, 0x7

    invoke-interface {v0, v1, v3, v2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(IIILcom/squareup/blueprint/UpdateContext;)V

    .line 248
    iget p1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    invoke-virtual {p2, p1, p3}, Lcom/squareup/blueprint/ChainInfo;->add(II)V

    return v2
.end method

.method public chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v0}, Lcom/squareup/blueprint/CenterBlock$Type;->getVertically()Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/blueprint/Block;->chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result p1

    return p1

    .line 266
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/blueprint/CenterBlock;->createHelperViewIfNeeded(Lcom/squareup/blueprint/UpdateContext;)V

    .line 268
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v0

    iget v1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    .line 270
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    iget v1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 271
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v1

    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v3, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v1, p1, v0, v3}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    .line 272
    iget v1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    const/4 v3, 0x4

    invoke-interface {v0, v1, v3, v2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(IIILcom/squareup/blueprint/UpdateContext;)V

    .line 274
    iget p1, p0, Lcom/squareup/blueprint/CenterBlock;->helperViewId:I

    invoke-virtual {p2, p1, p3}, Lcom/squareup/blueprint/ChainInfo;->add(II)V

    return v2
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/CenterBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/blueprint/CenterBlock$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    return-object v0
.end method

.method public connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 17

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "context"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "previousIds"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "align"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v3, p0

    .line 115
    iget-object v4, v3, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v4}, Lcom/squareup/blueprint/CenterBlock$Type;->getHorizontally()Z

    move-result v4

    if-nez v4, :cond_0

    .line 116
    invoke-direct/range {p0 .. p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v2}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    return-object v0

    .line 118
    :cond_0
    sget-object v4, Lcom/squareup/blueprint/CenterBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/blueprint/HorizontalAlign;->ordinal()I

    move-result v5

    aget v4, v4, v5

    const/4 v5, 0x1

    if-eq v4, v5, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v2, 0x3

    if-ne v4, v2, :cond_3

    .line 125
    new-instance v2, Lcom/squareup/blueprint/ChainInfo;

    invoke-direct {v2}, Lcom/squareup/blueprint/ChainInfo;-><init>()V

    .line 126
    invoke-interface {v1, v0}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object v4

    .line 129
    invoke-direct/range {p0 .. p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v6

    invoke-virtual {v4}, Lcom/squareup/blueprint/IdAndMargin;->getMargin()I

    move-result v7

    invoke-virtual {v6, v0, v2, v7}, Lcom/squareup/blueprint/Block;->chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result v6

    .line 130
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v7

    const/4 v8, 0x0

    if-le v7, v5, :cond_1

    const/4 v12, 0x0

    const/4 v13, 0x7

    .line 136
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v9

    .line 137
    invoke-virtual {v4}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v10

    .line 138
    invoke-interface/range {p2 .. p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v11

    .line 141
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->ids()[I

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x2

    .line 136
    invoke-virtual/range {v9 .. v16}, Landroidx/constraintlayout/widget/ConstraintSet;->createHorizontalChainRtl(IIII[I[FI)V

    .line 297
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v1

    invoke-static {v8, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 298
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v4, v1

    check-cast v4, Lkotlin/collections/IntIterator;

    invoke-virtual {v4}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v4

    .line 297
    invoke-virtual {v2, v4}, Lcom/squareup/blueprint/ChainInfo;->idAt(I)I

    move-result v5

    invoke-virtual {v2, v4}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v4

    .line 147
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v7

    const/4 v8, 0x6

    invoke-virtual {v7, v5, v8, v4}, Landroidx/constraintlayout/widget/ConstraintSet;->setMargin(III)V

    goto :goto_0

    .line 151
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v9

    .line 152
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->firstId()I

    move-result v10

    const/4 v11, 0x6

    .line 154
    invoke-virtual {v4}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v12

    .line 155
    invoke-interface/range {p2 .. p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v13

    .line 156
    invoke-virtual {v2, v8}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v14

    .line 151
    invoke-virtual/range {v9 .. v14}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 160
    :cond_2
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 v1, 0x7

    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->lastId()I

    move-result v2

    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(III)V

    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    return-object v0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 122
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v2}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    return-object v0
.end method

.method public connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 18

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "context"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "previousIds"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "align"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v3, p0

    .line 171
    iget-object v4, v3, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v4}, Lcom/squareup/blueprint/CenterBlock$Type;->getVertically()Z

    move-result v4

    if-nez v4, :cond_0

    .line 172
    invoke-direct/range {p0 .. p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v2}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    return-object v0

    .line 174
    :cond_0
    sget-object v4, Lcom/squareup/blueprint/CenterBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result v5

    aget v4, v4, v5

    const/4 v5, 0x1

    if-eq v4, v5, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v2, 0x3

    if-ne v4, v2, :cond_3

    .line 181
    new-instance v4, Lcom/squareup/blueprint/ChainInfo;

    invoke-direct {v4}, Lcom/squareup/blueprint/ChainInfo;-><init>()V

    .line 185
    invoke-interface {v1, v0}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object v6

    .line 186
    invoke-direct/range {p0 .. p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v7

    invoke-virtual {v6}, Lcom/squareup/blueprint/IdAndMargin;->getMargin()I

    move-result v8

    invoke-virtual {v7, v0, v4, v8}, Lcom/squareup/blueprint/Block;->chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result v7

    .line 188
    invoke-virtual {v4}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v8

    const/4 v9, 0x0

    if-le v8, v5, :cond_1

    const/4 v13, 0x0

    const/4 v14, 0x4

    .line 193
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v10

    .line 194
    invoke-virtual {v6}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v11

    .line 195
    invoke-interface/range {p2 .. p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v12

    .line 198
    invoke-virtual {v4}, Lcom/squareup/blueprint/ChainInfo;->ids()[I

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x2

    .line 193
    invoke-virtual/range {v10 .. v17}, Landroidx/constraintlayout/widget/ConstraintSet;->createVerticalChain(IIII[I[FI)V

    .line 301
    invoke-virtual {v4}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v1

    invoke-static {v9, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 302
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v5, v1

    check-cast v5, Lkotlin/collections/IntIterator;

    invoke-virtual {v5}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v5

    .line 301
    invoke-virtual {v4, v5}, Lcom/squareup/blueprint/ChainInfo;->idAt(I)I

    move-result v6

    invoke-virtual {v4, v5}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v5

    .line 204
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v8

    invoke-virtual {v8, v6, v2, v5}, Landroidx/constraintlayout/widget/ConstraintSet;->setMargin(III)V

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v0

    .line 209
    invoke-virtual {v4}, Lcom/squareup/blueprint/ChainInfo;->firstId()I

    move-result v10

    const/4 v11, 0x3

    .line 211
    invoke-virtual {v6}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v12

    .line 212
    invoke-interface/range {p2 .. p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v13

    .line 213
    invoke-virtual {v4, v9}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v14

    move-object v9, v0

    .line 208
    invoke-virtual/range {v9 .. v14}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 217
    :cond_2
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 v1, 0x4

    invoke-virtual {v4}, Lcom/squareup/blueprint/ChainInfo;->lastId()I

    move-result v2

    invoke-direct {v0, v1, v2, v7}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(III)V

    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    return-object v0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 178
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v2}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;)Lcom/squareup/blueprint/CenterBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/blueprint/CenterBlock$Type;",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/blueprint/CenterBlock<",
            "TC;TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/CenterBlock;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;)V

    return-object v0
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/blueprint/CenterBlock;->createParams()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public createParams()V
    .locals 0

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/CenterBlock;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/CenterBlock;

    invoke-virtual {p0}, Lcom/squareup/blueprint/CenterBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/CenterBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    iget-object v1, p1, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    iget-object p1, p1, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDependableHorizontally()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/squareup/blueprint/CenterBlock;->dependableHorizontally:Z

    return v0
.end method

.method public getDependableVertically()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lcom/squareup/blueprint/CenterBlock;->dependableVertically:Z

    return v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final getType()Lcom/squareup/blueprint/CenterBlock$Type;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/blueprint/CenterBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-super {p0, p1}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    check-cast p1, Lcom/squareup/blueprint/CenterBlock;

    .line 85
    iget-object v0, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    iget-object v1, p1, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    .line 86
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-direct {p1}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CenterBlock(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/CenterBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/CenterBlock;->type:Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", initialElement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/blueprint/CenterBlock;->initialElement:Lcom/squareup/blueprint/Block;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-direct {p0}, Lcom/squareup/blueprint/CenterBlock;->getElement()Lcom/squareup/blueprint/Block;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method
