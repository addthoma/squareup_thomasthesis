.class public final Lcom/squareup/blueprint/mosaic/MosaicBlockKt;
.super Ljava/lang/Object;
.source "MosaicBlock.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlockKt\n*L\n1#1,65:1\n19#1,4:66\n20#1,4:70\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001aV\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00020\u00042\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u001d\u0010\t\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\u000b\u0012\u0004\u0012\u00020\u00010\n\u00a2\u0006\u0002\u0008\u000cH\u0086\u0008\u00a8\u0006\r"
    }
    d2 = {
        "model",
        "",
        "P",
        "",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "wrapHorizontally",
        "",
        "wrapVertically",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/blueprint/mosaic/MosaicBlock;",
        "Lkotlin/ExtensionFunctionType;",
        "blueprint-mosaic_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final model(Lcom/squareup/blueprint/BlueprintContext;ZZLkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/mosaic/MosaicBlock<",
            "TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$model"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 20
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    .line 19
    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 22
    invoke-interface {p3, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 19
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method public static synthetic model$default(Lcom/squareup/blueprint/BlueprintContext;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x1

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    const/4 p2, 0x1

    :cond_1
    const-string p4, "$this$model"

    .line 16
    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "block"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance p4, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 70
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object p5

    .line 66
    invoke-direct {p4, p5, p1, p2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 69
    invoke-interface {p3, p4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p4, Lcom/squareup/blueprint/Block;

    .line 66
    invoke-interface {p0, p4}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method
