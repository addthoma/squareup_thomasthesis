.class public final Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;
.super Ljava/lang/Object;
.source "CommonLoggedInModule_ProvideTenderInEditFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/TenderInEdit;",
        ">;"
    }
.end annotation


# instance fields
.field private final alwaysTenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AlwaysTenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/LegacyTenderInEdit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AlwaysTenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/LegacyTenderInEdit;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->alwaysTenderInEditProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->legacyTenderInEditProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AlwaysTenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/LegacyTenderInEdit;",
            ">;)",
            "Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTenderInEdit(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/AlwaysTenderInEdit;Lcom/squareup/payment/LegacyTenderInEdit;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 48
    invoke-static {p0, p1, p2}, Lcom/squareup/CommonLoggedInModule;->provideTenderInEdit(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/AlwaysTenderInEdit;Lcom/squareup/payment/LegacyTenderInEdit;)Lcom/squareup/payment/TenderInEdit;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/TenderInEdit;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->alwaysTenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/AlwaysTenderInEdit;

    iget-object v2, p0, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->legacyTenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/LegacyTenderInEdit;

    invoke-static {v0, v1, v2}, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->provideTenderInEdit(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/AlwaysTenderInEdit;Lcom/squareup/payment/LegacyTenderInEdit;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/CommonLoggedInModule_ProvideTenderInEditFactory;->get()Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    return-object v0
.end method
