.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/activity/SelectRefundTenderScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SRTS2_ComponentImpl"
.end annotation


# instance fields
.field private selectRefundTenderPresenterProvider:Ljavax/inject/Provider;

.field final synthetic this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;)V
    .locals 0

    .line 50934
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50936
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 50930
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 3

    .line 50941
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;->access$181400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/SelectRefundTenderPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->selectRefundTenderPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectSelectRefundTenderView(Lcom/squareup/ui/activity/SelectRefundTenderView;)Lcom/squareup/ui/activity/SelectRefundTenderView;
    .locals 1

    .line 50950
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/SelectRefundTenderView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/activity/SelectRefundTenderView;Lcom/squareup/text/Formatter;)V

    .line 50951
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->selectRefundTenderPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/SelectRefundTenderView_MembersInjector;->injectPresenter(Lcom/squareup/ui/activity/SelectRefundTenderView;Ljava/lang/Object;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/activity/SelectRefundTenderView;)V
    .locals 0

    .line 50946
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$CAS2_ComponentImpl$CDS2_ComponentImpl$SRTS2_ComponentImpl;->injectSelectRefundTenderView(Lcom/squareup/ui/activity/SelectRefundTenderView;)Lcom/squareup/ui/activity/SelectRefundTenderView;

    return-void
.end method
