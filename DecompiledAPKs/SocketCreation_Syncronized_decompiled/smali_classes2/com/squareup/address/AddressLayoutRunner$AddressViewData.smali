.class Lcom/squareup/address/AddressLayoutRunner$AddressViewData;
.super Ljava/lang/Object;
.source "AddressLayoutRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/AddressLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AddressViewData"
.end annotation


# instance fields
.field final apartmentHint:I

.field final countryCode:Lcom/squareup/CountryCode;

.field final postalHint:I

.field final stateHint:I


# direct methods
.method constructor <init>(Lcom/squareup/CountryCode;)V
    .locals 1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->countryCode:Lcom/squareup/CountryCode;

    .line 147
    invoke-static {p1}, Lcom/squareup/address/CountryResources;->stateHint(Lcom/squareup/CountryCode;)I

    move-result v0

    iput v0, p0, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->stateHint:I

    .line 148
    invoke-static {p1}, Lcom/squareup/address/CountryResources;->postalHint(Lcom/squareup/CountryCode;)I

    move-result v0

    iput v0, p0, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->postalHint:I

    .line 149
    invoke-static {p1}, Lcom/squareup/address/CountryResources;->apartmentHint(Lcom/squareup/CountryCode;)I

    move-result p1

    iput p1, p0, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->apartmentHint:I

    return-void
.end method
