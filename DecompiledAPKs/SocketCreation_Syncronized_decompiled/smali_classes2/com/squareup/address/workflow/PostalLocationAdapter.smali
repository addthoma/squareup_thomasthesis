.class public final Lcom/squareup/address/workflow/PostalLocationAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PostalLocationAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;,
        Lcom/squareup/address/workflow/PostalLocationAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPostalLocationAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PostalLocationAdapter.kt\ncom/squareup/address/workflow/PostalLocationAdapter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,42:1\n1360#2:43\n1429#2,3:44\n*E\n*S KotlinDebug\n*F\n+ 1 PostalLocationAdapter.kt\ncom/squareup/address/workflow/PostalLocationAdapter\n*L\n24#1:43\n24#1,3:44\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000 \u000f2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u000e\u000fB\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/address/workflow/PostalLocationAdapter;",
        "Landroid/widget/ArrayAdapter;",
        "Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;",
        "context",
        "Landroid/content/Context;",
        "locations",
        "",
        "Lcom/squareup/server/address/PostalLocation;",
        "(Landroid/content/Context;Ljava/util/List;)V",
        "otherLocation",
        "getItemId",
        "",
        "position",
        "",
        "CityLocation",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/address/workflow/PostalLocationAdapter$Companion;

.field public static final OTHER_ID:J = -0x1L


# instance fields
.field private final otherLocation:Lcom/squareup/server/address/PostalLocation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/address/workflow/PostalLocationAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/address/workflow/PostalLocationAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/address/workflow/PostalLocationAdapter;->Companion:Lcom/squareup/address/workflow/PostalLocationAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget v0, Lcom/squareup/noho/R$layout;->noho_spinner_item:I

    .line 14
    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 18
    new-instance v0, Lcom/squareup/server/address/PostalLocation;

    .line 19
    sget v1, Lcom/squareup/address/R$string;->postal_other:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "context.getString(addressR.string.postal_other)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, ""

    const/4 v2, 0x0

    .line 18
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/server/address/PostalLocation;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/squareup/address/workflow/PostalLocationAdapter;->otherLocation:Lcom/squareup/server/address/PostalLocation;

    .line 23
    sget p1, Lcom/squareup/noho/R$layout;->noho_simple_list_item:I

    invoke-virtual {p0, p1}, Lcom/squareup/address/workflow/PostalLocationAdapter;->setDropDownViewResource(I)V

    .line 24
    check-cast p2, Ljava/util/Collection;

    iget-object p1, p0, Lcom/squareup/address/workflow/PostalLocationAdapter;->otherLocation:Lcom/squareup/server/address/PostalLocation;

    invoke-static {p2, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 43
    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 44
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 45
    check-cast v0, Lcom/squareup/server/address/PostalLocation;

    new-instance v1, Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;

    .line 24
    invoke-direct {v1, v0}, Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;-><init>(Lcom/squareup/server/address/PostalLocation;)V

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_0
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/util/Collection;

    .line 24
    invoke-virtual {p0, p2}, Lcom/squareup/address/workflow/PostalLocationAdapter;->addAll(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    .line 28
    invoke-virtual {p0, p1}, Lcom/squareup/address/workflow/PostalLocationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;->getLocation()Lcom/squareup/server/address/PostalLocation;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/squareup/address/workflow/PostalLocationAdapter;->otherLocation:Lcom/squareup/server/address/PostalLocation;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    goto :goto_1

    :cond_1
    int-to-long v0, p1

    :goto_1
    return-wide v0
.end method
