.class final Lcom/squareup/address/workflow/RealAddressViewFactory$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAddressViewFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/address/workflow/RealAddressViewFactory;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddressViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddressViewFactory.kt\ncom/squareup/address/workflow/RealAddressViewFactory$1\n*L\n1#1,18:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000bH\n\u00a2\u0006\u0002\u0008\u000c"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/address/workflow/AddressScreen;",
        "<anonymous parameter 1>",
        "Landroid/content/Context;",
        "<anonymous parameter 2>",
        "Landroid/view/ViewGroup;",
        "<anonymous parameter 3>",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/address/workflow/RealAddressViewFactory$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/address/workflow/RealAddressViewFactory$1;

    invoke-direct {v0}, Lcom/squareup/address/workflow/RealAddressViewFactory$1;-><init>()V

    sput-object v0, Lcom/squareup/address/workflow/RealAddressViewFactory$1;->INSTANCE:Lcom/squareup/address/workflow/RealAddressViewFactory$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7
    check-cast p1, Lio/reactivex/Observable;

    check-cast p2, Landroid/content/Context;

    check-cast p3, Landroid/view/ViewGroup;

    check-cast p4, Lcom/squareup/workflow/ui/ContainerHints;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/address/workflow/RealAddressViewFactory$1;->invoke(Lio/reactivex/Observable;Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/ui/ContainerHints;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lio/reactivex/Observable;Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/ui/ContainerHints;)Ljava/lang/Void;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string p3, "<anonymous parameter 0>"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "<anonymous parameter 1>"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "<anonymous parameter 3>"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "You should not bind AddressScreen directly, please unpack and use its components"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
