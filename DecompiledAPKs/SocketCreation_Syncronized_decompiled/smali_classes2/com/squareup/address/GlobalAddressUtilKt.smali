.class public final Lcom/squareup/address/GlobalAddressUtilKt;
.super Ljava/lang/Object;
.source "GlobalAddressUtil.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGlobalAddressUtil.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GlobalAddressUtil.kt\ncom/squareup/address/GlobalAddressUtilKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,72:1\n704#2:73\n777#2,2:74\n*E\n*S KotlinDebug\n*F\n+ 1 GlobalAddressUtil.kt\ncom/squareup/address/GlobalAddressUtilKt\n*L\n10#1:73\n10#1,2:74\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u001a\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0004*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\u0012\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0004*\u00020\u0002H\u0002\u001a\u0014\u0010\u0008\u001a\u00020\u0001*\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0004*\u00020\u0002H\u0002\u00a8\u0006\n"
    }
    d2 = {
        "addressLastLine",
        "",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "addressParts",
        "",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "defaultAddressParts",
        "formatMultilineAddress",
        "jPAddressParts",
        "address_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final addressLastLine(Lcom/squareup/protos/common/location/GlobalAddress;)Ljava/lang/String;
    .locals 3

    .line 59
    iget-object v0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    const-string v1, ""

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 60
    :goto_0
    iget-object v2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v1, v2

    .line 61
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    const-string v2, "locality"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 63
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 66
    :cond_2
    iget-object p0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    :goto_1
    move-object v0, p0

    const-string p0, "if (lastPart.isNotBlank(\u2026se {\n      locality\n    }"

    .line 63
    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_3
    return-object v0

    .line 61
    :cond_4
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final addressParts(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/CountryCode;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            "Lcom/squareup/CountryCode;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    sget-object v1, Lcom/squareup/protos/common/countries/Country;->JP:Lcom/squareup/protos/common/countries/Country;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 22
    :cond_0
    invoke-static {p0}, Lcom/squareup/address/GlobalAddressUtilKt;->defaultAddressParts(Lcom/squareup/protos/common/location/GlobalAddress;)Ljava/util/List;

    move-result-object p0

    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/squareup/address/GlobalAddressUtilKt;->jPAddressParts(Lcom/squareup/protos/common/location/GlobalAddress;)Ljava/util/List;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method private static final defaultAddressParts(Lcom/squareup/protos/common/location/GlobalAddress;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    .line 49
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 50
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 51
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 52
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 53
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 54
    invoke-static {p0}, Lcom/squareup/address/GlobalAddressUtilKt;->addressLastLine(Lcom/squareup/protos/common/location/GlobalAddress;)Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x5

    aput-object p0, v0, v1

    .line 48
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final formatMultilineAddress(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/CountryCode;)Ljava/lang/String;
    .locals 9

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_2

    .line 9
    invoke-static {p0, p1}, Lcom/squareup/address/GlobalAddressUtilKt;->addressParts(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/CountryCode;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 73
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 74
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 10
    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :cond_1
    check-cast p1, Ljava/util/List;

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    const-string p0, "\n"

    .line 11
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_2
    const-string p0, ""

    :goto_1
    return-object p0
.end method

.method private static final jPAddressParts(Lcom/squareup/protos/common/location/GlobalAddress;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    .line 28
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 29
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 30
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_2:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 31
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_3:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 32
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 33
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality:Ljava/lang/String;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 34
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_1:Ljava/lang/String;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 35
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_2:Ljava/lang/String;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 36
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_3:Ljava/lang/String;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 37
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_4:Ljava/lang/String;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 38
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->sublocality_5:Ljava/lang/String;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 39
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    .line 40
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    .line 41
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_3:Ljava/lang/String;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    .line 42
    iget-object v1, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_4:Ljava/lang/String;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    .line 43
    iget-object p0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_5:Ljava/lang/String;

    const/16 v1, 0xf

    aput-object p0, v0, v1

    .line 27
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method
