.class final enum Lcom/squareup/address/CountryFlagsAndNames;
.super Ljava/lang/Enum;
.source "CountryFlagsAndNames.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/address/CountryFlagsAndNames;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AI:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AL:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AU:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum AZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BB:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BD:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BF:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BH:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BJ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BS:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum BZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CH:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CI:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CL:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CV:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum CZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum DE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum DK:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum DM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum DO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum DZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum EC:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum EE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum EG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum ES:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum FI:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum FJ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum FM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum FR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GB:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GD:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GH:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GU:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum GY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum HK:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum HN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum HR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum HT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum HU:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum ID:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum IE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum IL:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum IN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum IS:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum IT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum JM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum JO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum JP:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KH:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum KZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LB:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LC:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LI:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LK:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LS:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LU:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LV:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum LY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MD:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MK:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum ML:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MP:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MS:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MU:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MX:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum MZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NI:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NL:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NP:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum NZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum OM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PH:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PK:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PL:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum PY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum QA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum RO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum RS:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum RU:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum RW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SB:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SC:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SI:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SK:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SL:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum ST:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SV:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum SZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TC:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TD:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TH:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TJ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TO:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TR:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TT:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TV:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TW:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum TZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum UA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum UG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum US:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum UY:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum UZ:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum VC:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum VE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum VG:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum VI:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum VN:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum YE:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum ZA:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum ZM:Lcom/squareup/address/CountryFlagsAndNames;

.field public static final enum ZW:Lcom/squareup/address/CountryFlagsAndNames;


# instance fields
.field final countryCode:Lcom/squareup/CountryCode;

.field final countryName:I

.field final flagId:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 15
    new-instance v6, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v3, Lcom/squareup/CountryCode;->AI:Lcom/squareup/CountryCode;

    sget v4, Lcom/squareup/country/resources/R$string;->country_anguilla:I

    sget v5, Lcom/squareup/country/resources/R$drawable;->flag_ai:I

    const-string v1, "AI"

    const/4 v2, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v6, Lcom/squareup/address/CountryFlagsAndNames;->AI:Lcom/squareup/address/CountryFlagsAndNames;

    .line 16
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->AL:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_albania:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_al:I

    const-string v8, "AL"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AL:Lcom/squareup/address/CountryFlagsAndNames;

    .line 17
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->DZ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_algeria:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_dz:I

    const-string v2, "DZ"

    const/4 v3, 0x2

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->DZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 18
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_australia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_au:I

    const-string v8, "AU"

    const/4 v9, 0x3

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AU:Lcom/squareup/address/CountryFlagsAndNames;

    .line 19
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->AO:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_angola:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ao:I

    const-string v2, "AO"

    const/4 v3, 0x4

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 20
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->AG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_antiguaandbarbuda:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ag:I

    const-string v8, "AG"

    const/4 v9, 0x5

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 21
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->AR:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_argentina:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ar:I

    const-string v2, "AR"

    const/4 v3, 0x6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 22
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->AM:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_armenia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_am:I

    const-string v8, "AM"

    const/4 v9, 0x7

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 23
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->AW:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_aruba:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_aw:I

    const-string v2, "AW"

    const/16 v3, 0x8

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 24
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->AT:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_austria:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_at:I

    const-string v8, "AT"

    const/16 v9, 0x9

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 25
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->AZ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_azerbaijan:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_az:I

    const-string v2, "AZ"

    const/16 v3, 0xa

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 26
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BS:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_bahamas:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bs:I

    const-string v8, "BS"

    const/16 v9, 0xb

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BS:Lcom/squareup/address/CountryFlagsAndNames;

    .line 27
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BH:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_bahrain:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_bh:I

    const-string v2, "BH"

    const/16 v3, 0xc

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BH:Lcom/squareup/address/CountryFlagsAndNames;

    .line 28
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BD:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_bangladesh:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bd:I

    const-string v8, "BD"

    const/16 v9, 0xd

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BD:Lcom/squareup/address/CountryFlagsAndNames;

    .line 29
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BB:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_barbados:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_bb:I

    const-string v2, "BB"

    const/16 v3, 0xe

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BB:Lcom/squareup/address/CountryFlagsAndNames;

    .line 30
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BY:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_belarus:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_by:I

    const-string v8, "BY"

    const/16 v9, 0xf

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 31
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BE:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_belgium:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_be:I

    const-string v2, "BE"

    const/16 v3, 0x10

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 32
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BZ:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_belize:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bz:I

    const-string v8, "BZ"

    const/16 v9, 0x11

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 33
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BJ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_benin:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_bj:I

    const-string v2, "BJ"

    const/16 v3, 0x12

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BJ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 34
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BM:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_bermuda:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bm:I

    const-string v8, "BM"

    const/16 v9, 0x13

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 35
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BT:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_bhutan:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_bt:I

    const-string v2, "BT"

    const/16 v3, 0x14

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 36
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BO:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_bolivia_plurinationalstateof:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bo:I

    const-string v8, "BO"

    const/16 v9, 0x15

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 37
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BA:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_bosniaandherzegovina:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ba:I

    const-string v2, "BA"

    const/16 v3, 0x16

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 38
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BW:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_botswana:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bw:I

    const-string v8, "BW"

    const/16 v9, 0x17

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 39
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BR:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_brazil:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_br:I

    const-string v2, "BR"

    const/16 v3, 0x18

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 40
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BN:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_bruneidarussalam:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bn:I

    const-string v8, "BN"

    const/16 v9, 0x19

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 41
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->BG:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_bulgaria:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_bg:I

    const-string v2, "BG"

    const/16 v3, 0x1a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 42
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->BF:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_burkinafaso:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_bf:I

    const-string v8, "BF"

    const/16 v9, 0x1b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->BF:Lcom/squareup/address/CountryFlagsAndNames;

    .line 43
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->CV:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_caboverde:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_cv:I

    const-string v2, "CV"

    const/16 v3, 0x1c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CV:Lcom/squareup/address/CountryFlagsAndNames;

    .line 44
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->KH:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_cambodia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_kh:I

    const-string v8, "KH"

    const/16 v9, 0x1d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KH:Lcom/squareup/address/CountryFlagsAndNames;

    .line 45
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->CM:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_cameroon:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_cm:I

    const-string v2, "CM"

    const/16 v3, 0x1e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 46
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_canada:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ca:I

    const-string v8, "CA"

    const/16 v9, 0x1f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 47
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->KY:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_caymanislands:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ky:I

    const-string v2, "KY"

    const/16 v3, 0x20

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 48
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->TD:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_chad:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_td:I

    const-string v8, "TD"

    const/16 v9, 0x21

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TD:Lcom/squareup/address/CountryFlagsAndNames;

    .line 49
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->CL:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_chile:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_cl:I

    const-string v2, "CL"

    const/16 v3, 0x22

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CL:Lcom/squareup/address/CountryFlagsAndNames;

    .line 50
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->CN:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_china:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_cn:I

    const-string v8, "CN"

    const/16 v9, 0x23

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 51
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->CO:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_colombia:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_co:I

    const-string v2, "CO"

    const/16 v3, 0x24

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 52
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->CG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_congo:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_cg:I

    const-string v8, "CG"

    const/16 v9, 0x25

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 53
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->CR:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_costarica:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_cr:I

    const-string v2, "CR"

    const/16 v3, 0x26

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 54
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->CI:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_coted_ivoire:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ci:I

    const-string v8, "CI"

    const/16 v9, 0x27

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CI:Lcom/squareup/address/CountryFlagsAndNames;

    .line 55
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->HR:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_croatia:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_hr:I

    const-string v2, "HR"

    const/16 v3, 0x28

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->HR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 56
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->CY:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_cyprus:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_cy:I

    const-string v8, "CY"

    const/16 v9, 0x29

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 57
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->CZ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_czechrepublic:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_cz:I

    const-string v2, "CZ"

    const/16 v3, 0x2a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 58
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->DK:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_denmark:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_dk:I

    const-string v8, "DK"

    const/16 v9, 0x2b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->DK:Lcom/squareup/address/CountryFlagsAndNames;

    .line 59
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->DM:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_dominica:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_dm:I

    const-string v2, "DM"

    const/16 v3, 0x2c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->DM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 60
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->DO:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_dominicanrepublic:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_do:I

    const-string v8, "DO"

    const/16 v9, 0x2d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->DO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 61
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->EC:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_ecuador:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ec:I

    const-string v2, "EC"

    const/16 v3, 0x2e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->EC:Lcom/squareup/address/CountryFlagsAndNames;

    .line 62
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->EG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_egypt:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_eg:I

    const-string v8, "EG"

    const/16 v9, 0x2f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->EG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 63
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->SV:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_elsalvador:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_sv:I

    const-string v2, "SV"

    const/16 v3, 0x30

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SV:Lcom/squareup/address/CountryFlagsAndNames;

    .line 64
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->EE:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_estonia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ee:I

    const-string v8, "EE"

    const/16 v9, 0x31

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->EE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 65
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->FJ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_fiji:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_fj:I

    const-string v2, "FJ"

    const/16 v3, 0x32

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->FJ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 66
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->FI:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_finland:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_fi:I

    const-string v8, "FI"

    const/16 v9, 0x33

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->FI:Lcom/squareup/address/CountryFlagsAndNames;

    .line 67
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->FR:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_france:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_fr:I

    const-string v2, "FR"

    const/16 v3, 0x34

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->FR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 68
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->GA:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_gabon:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ga:I

    const-string v8, "GA"

    const/16 v9, 0x35

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 69
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->GM:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_gambia:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_gm:I

    const-string v2, "GM"

    const/16 v3, 0x36

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 70
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->DE:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_germany:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_de:I

    const-string v8, "DE"

    const/16 v9, 0x37

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->DE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 71
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->GH:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_ghana:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_gh:I

    const-string v2, "GH"

    const/16 v3, 0x38

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GH:Lcom/squareup/address/CountryFlagsAndNames;

    .line 72
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->GR:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_greece:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_gr:I

    const-string v8, "GR"

    const/16 v9, 0x39

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 73
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->GD:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_grenada:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_gd:I

    const-string v2, "GD"

    const/16 v3, 0x3a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GD:Lcom/squareup/address/CountryFlagsAndNames;

    .line 74
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->GU:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_guam:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_gu:I

    const-string v8, "GU"

    const/16 v9, 0x3b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GU:Lcom/squareup/address/CountryFlagsAndNames;

    .line 75
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->GT:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_guatemala:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_gt:I

    const-string v2, "GT"

    const/16 v3, 0x3c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 76
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->GW:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_guinea_bissau:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_gw:I

    const-string v8, "GW"

    const/16 v9, 0x3d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 77
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->GY:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_guyana:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_gy:I

    const-string v2, "GY"

    const/16 v3, 0x3e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 78
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->HT:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_haiti:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ht:I

    const-string v8, "HT"

    const/16 v9, 0x3f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->HT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 79
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->HN:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_honduras:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_hn:I

    const-string v2, "HN"

    const/16 v3, 0x40

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->HN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 80
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->HK:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_hongkong:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_hk:I

    const-string v8, "HK"

    const/16 v9, 0x41

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->HK:Lcom/squareup/address/CountryFlagsAndNames;

    .line 81
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->HU:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_hungary:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_hu:I

    const-string v2, "HU"

    const/16 v3, 0x42

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->HU:Lcom/squareup/address/CountryFlagsAndNames;

    .line 82
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->IS:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_iceland:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_is:I

    const-string v8, "IS"

    const/16 v9, 0x43

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->IS:Lcom/squareup/address/CountryFlagsAndNames;

    .line 83
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->IN:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_india:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_in:I

    const-string v2, "IN"

    const/16 v3, 0x44

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->IN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 84
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->ID:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_indonesia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_id:I

    const-string v8, "ID"

    const/16 v9, 0x45

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->ID:Lcom/squareup/address/CountryFlagsAndNames;

    .line 85
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->IE:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_ireland:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ie:I

    const-string v2, "IE"

    const/16 v3, 0x46

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->IE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 86
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->IL:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_israel:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_il:I

    const-string v8, "IL"

    const/16 v9, 0x47

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->IL:Lcom/squareup/address/CountryFlagsAndNames;

    .line 87
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->IT:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_italy:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_it:I

    const-string v2, "IT"

    const/16 v3, 0x48

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->IT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 88
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->JM:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_jamaica:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_jm:I

    const-string v8, "JM"

    const/16 v9, 0x49

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->JM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 89
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_japan:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_jp:I

    const-string v2, "JP"

    const/16 v3, 0x4a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->JP:Lcom/squareup/address/CountryFlagsAndNames;

    .line 90
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->JO:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_jordan:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_jo:I

    const-string v8, "JO"

    const/16 v9, 0x4b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->JO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 91
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->KZ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_kazakhstan:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_kz:I

    const-string v2, "KZ"

    const/16 v3, 0x4c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 92
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->KE:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_kenya:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ke:I

    const-string v8, "KE"

    const/16 v9, 0x4d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 93
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->KW:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_kuwait:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_kw:I

    const-string v2, "KW"

    const/16 v3, 0x4e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 94
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->KG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_kyrgyzstan:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_kg:I

    const-string v8, "KG"

    const/16 v9, 0x4f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 95
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->LA:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_laopeople_sdemocraticrepublic:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_la:I

    const-string v2, "LA"

    const/16 v3, 0x50

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 96
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->LV:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_latvia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_lv:I

    const-string v8, "LV"

    const/16 v9, 0x51

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LV:Lcom/squareup/address/CountryFlagsAndNames;

    .line 97
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->LB:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_lebanon:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_lb:I

    const-string v2, "LB"

    const/16 v3, 0x52

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LB:Lcom/squareup/address/CountryFlagsAndNames;

    .line 98
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->LS:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_lesotho:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ls:I

    const-string v8, "LS"

    const/16 v9, 0x53

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LS:Lcom/squareup/address/CountryFlagsAndNames;

    .line 99
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->LR:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_liberia:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_lr:I

    const-string v2, "LR"

    const/16 v3, 0x54

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 100
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->LY:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_libya:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ly:I

    const-string v8, "LY"

    const/16 v9, 0x55

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 101
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->LI:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_liechtenstein:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_li:I

    const-string v2, "LI"

    const/16 v3, 0x56

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LI:Lcom/squareup/address/CountryFlagsAndNames;

    .line 102
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->LT:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_lithuania:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_lt:I

    const-string v8, "LT"

    const/16 v9, 0x57

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 103
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->LU:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_luxembourg:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_lu:I

    const-string v2, "LU"

    const/16 v3, 0x58

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LU:Lcom/squareup/address/CountryFlagsAndNames;

    .line 104
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MO:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_macao:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_mo:I

    const-string v8, "MO"

    const/16 v9, 0x59

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 105
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->MK:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_northmacedonia_therepublicof:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_mk:I

    const-string v2, "MK"

    const/16 v3, 0x5a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MK:Lcom/squareup/address/CountryFlagsAndNames;

    .line 106
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_madagascar:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_mg:I

    const-string v8, "MG"

    const/16 v9, 0x5b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 107
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->MW:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_malawi:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_mw:I

    const-string v2, "MW"

    const/16 v3, 0x5c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 108
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MY:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_malaysia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_my:I

    const-string v8, "MY"

    const/16 v9, 0x5d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 109
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->ML:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_mali:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ml:I

    const-string v2, "ML"

    const/16 v3, 0x5e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->ML:Lcom/squareup/address/CountryFlagsAndNames;

    .line 110
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MT:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_malta:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_mt:I

    const-string v8, "MT"

    const/16 v9, 0x5f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 111
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->MR:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_mauritania:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_mr:I

    const-string v2, "MR"

    const/16 v3, 0x60

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 112
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MU:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_mauritius:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_mu:I

    const-string v8, "MU"

    const/16 v9, 0x61

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MU:Lcom/squareup/address/CountryFlagsAndNames;

    .line 113
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->MX:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_mexico:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_mx:I

    const-string v2, "MX"

    const/16 v3, 0x62

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MX:Lcom/squareup/address/CountryFlagsAndNames;

    .line 114
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->FM:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_micronesia_federatedstatesof:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_fm:I

    const-string v8, "FM"

    const/16 v9, 0x63

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->FM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 115
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->MD:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_moldova_republicof:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_md:I

    const-string v2, "MD"

    const/16 v3, 0x64

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MD:Lcom/squareup/address/CountryFlagsAndNames;

    .line 116
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MN:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_mongolia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_mn:I

    const-string v8, "MN"

    const/16 v9, 0x65

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 117
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->MS:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_montserrat:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ms:I

    const-string v2, "MS"

    const/16 v3, 0x66

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MS:Lcom/squareup/address/CountryFlagsAndNames;

    .line 118
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MA:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_morocco:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ma:I

    const-string v8, "MA"

    const/16 v9, 0x67

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 119
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->MZ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_mozambique:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_mz:I

    const-string v2, "MZ"

    const/16 v3, 0x68

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 120
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MM:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_myanmar:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_mm:I

    const-string v8, "MM"

    const/16 v9, 0x69

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 121
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->NA:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_namibia:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_na:I

    const-string v2, "NA"

    const/16 v3, 0x6a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 122
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->NP:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_nepal:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_np:I

    const-string v8, "NP"

    const/16 v9, 0x6b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NP:Lcom/squareup/address/CountryFlagsAndNames;

    .line 123
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->NL:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_netherlands:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_nl:I

    const-string v2, "NL"

    const/16 v3, 0x6c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NL:Lcom/squareup/address/CountryFlagsAndNames;

    .line 124
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->NZ:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_newzealand:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_nz:I

    const-string v8, "NZ"

    const/16 v9, 0x6d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 125
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->NI:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_nicaragua:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ni:I

    const-string v2, "NI"

    const/16 v3, 0x6e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NI:Lcom/squareup/address/CountryFlagsAndNames;

    .line 126
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->NE:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_niger:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ne:I

    const-string v8, "NE"

    const/16 v9, 0x6f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 127
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->NG:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_nigeria:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ng:I

    const-string v2, "NG"

    const/16 v3, 0x70

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 128
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->MP:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_northernmarianaislands:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_mp:I

    const-string v8, "MP"

    const/16 v9, 0x71

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->MP:Lcom/squareup/address/CountryFlagsAndNames;

    .line 129
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->NO:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_norway:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_no:I

    const-string v2, "NO"

    const/16 v3, 0x72

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->NO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 130
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->OM:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_oman:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_om:I

    const-string v8, "OM"

    const/16 v9, 0x73

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->OM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 131
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->PK:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_pakistan:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_pk:I

    const-string v2, "PK"

    const/16 v3, 0x74

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PK:Lcom/squareup/address/CountryFlagsAndNames;

    .line 132
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->PW:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_palau:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_pw:I

    const-string v8, "PW"

    const/16 v9, 0x75

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 133
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->PA:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_panama:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_pa:I

    const-string v2, "PA"

    const/16 v3, 0x76

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 134
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->PG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_papuanewguinea:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_pg:I

    const-string v8, "PG"

    const/16 v9, 0x77

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 135
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->PY:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_paraguay:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_py:I

    const-string v2, "PY"

    const/16 v3, 0x78

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 136
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->PE:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_peru:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_pe:I

    const-string v8, "PE"

    const/16 v9, 0x79

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 137
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->PH:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_philippines:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ph:I

    const-string v2, "PH"

    const/16 v3, 0x7a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PH:Lcom/squareup/address/CountryFlagsAndNames;

    .line 138
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->PL:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_poland:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_pl:I

    const-string v8, "PL"

    const/16 v9, 0x7b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PL:Lcom/squareup/address/CountryFlagsAndNames;

    .line 139
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->PT:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_portugal:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_pt:I

    const-string v2, "PT"

    const/16 v3, 0x7c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 140
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->PR:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_puertorico:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_pr:I

    const-string v8, "PR"

    const/16 v9, 0x7d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->PR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 141
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->QA:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_qatar:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_qa:I

    const-string v2, "QA"

    const/16 v3, 0x7e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->QA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 142
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->RO:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_romania:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ro:I

    const-string v8, "RO"

    const/16 v9, 0x7f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->RO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 143
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->RU:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_russianfederation:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ru:I

    const-string v2, "RU"

    const/16 v3, 0x80

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->RU:Lcom/squareup/address/CountryFlagsAndNames;

    .line 144
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->RW:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_rwanda:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_rw:I

    const-string v8, "RW"

    const/16 v9, 0x81

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->RW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 145
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->ST:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_saotomeandprincipe:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_st:I

    const-string v2, "ST"

    const/16 v3, 0x82

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->ST:Lcom/squareup/address/CountryFlagsAndNames;

    .line 146
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->SA:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_saudiarabia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_sa:I

    const-string v8, "SA"

    const/16 v9, 0x83

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 147
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->SN:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_senegal:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_sn:I

    const-string v2, "SN"

    const/16 v3, 0x84

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 148
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->RS:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_serbia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_rs:I

    const-string v8, "RS"

    const/16 v9, 0x85

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->RS:Lcom/squareup/address/CountryFlagsAndNames;

    .line 149
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->SC:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_seychelles:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_sc:I

    const-string v2, "SC"

    const/16 v3, 0x86

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SC:Lcom/squareup/address/CountryFlagsAndNames;

    .line 150
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->SL:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_sierraleone:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_sl:I

    const-string v8, "SL"

    const/16 v9, 0x87

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SL:Lcom/squareup/address/CountryFlagsAndNames;

    .line 151
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->SG:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_singapore:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_sg:I

    const-string v2, "SG"

    const/16 v3, 0x88

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 152
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->SK:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_slovakia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_sk:I

    const-string v8, "SK"

    const/16 v9, 0x89

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SK:Lcom/squareup/address/CountryFlagsAndNames;

    .line 153
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->SI:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_slovenia:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_si:I

    const-string v2, "SI"

    const/16 v3, 0x8a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SI:Lcom/squareup/address/CountryFlagsAndNames;

    .line 154
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->SB:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_solomonislands:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_sb:I

    const-string v8, "SB"

    const/16 v9, 0x8b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SB:Lcom/squareup/address/CountryFlagsAndNames;

    .line 155
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->ZA:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_southafrica:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_za:I

    const-string v2, "ZA"

    const/16 v3, 0x8c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->ZA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 156
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->KR:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_korea_republicof:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_kr:I

    const-string v8, "KR"

    const/16 v9, 0x8d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 157
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->ES:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_spain:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_es:I

    const-string v2, "ES"

    const/16 v3, 0x8e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->ES:Lcom/squareup/address/CountryFlagsAndNames;

    .line 158
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->LK:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_srilanka:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_lk:I

    const-string v8, "LK"

    const/16 v9, 0x8f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LK:Lcom/squareup/address/CountryFlagsAndNames;

    .line 159
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->KN:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_saintkittsandnevis:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_kn:I

    const-string v2, "KN"

    const/16 v3, 0x90

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->KN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 160
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->LC:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_saintlucia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_lc:I

    const-string v8, "LC"

    const/16 v9, 0x91

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->LC:Lcom/squareup/address/CountryFlagsAndNames;

    .line 161
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->VC:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_saintvincentandthegrenadines:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_vc:I

    const-string v2, "VC"

    const/16 v3, 0x92

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->VC:Lcom/squareup/address/CountryFlagsAndNames;

    .line 162
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->SR:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_suriname:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_sr:I

    const-string v8, "SR"

    const/16 v9, 0x93

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 163
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->SZ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_swaziland:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_sz:I

    const-string v2, "SZ"

    const/16 v3, 0x94

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 164
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->SE:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_sweden:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_se:I

    const-string v8, "SE"

    const/16 v9, 0x95

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->SE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 165
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->CH:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_switzerland:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ch:I

    const-string v2, "CH"

    const/16 v3, 0x96

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->CH:Lcom/squareup/address/CountryFlagsAndNames;

    .line 166
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->TW:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_taiwan_provinceofchina:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_tw:I

    const-string v8, "TW"

    const/16 v9, 0x97

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TW:Lcom/squareup/address/CountryFlagsAndNames;

    .line 167
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->TJ:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_tajikistan:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_tj:I

    const-string v2, "TJ"

    const/16 v3, 0x98

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TJ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 168
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->TZ:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_tanzania_unitedrepublicof:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_tz:I

    const-string v8, "TZ"

    const/16 v9, 0x99

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 169
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->TH:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_thailand:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_th:I

    const-string v2, "TH"

    const/16 v3, 0x9a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TH:Lcom/squareup/address/CountryFlagsAndNames;

    .line 170
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->TG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_togo:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_tg:I

    const-string v8, "TG"

    const/16 v9, 0x9b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 171
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->TO:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_tonga:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_to:I

    const-string v2, "TO"

    const/16 v3, 0x9c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TO:Lcom/squareup/address/CountryFlagsAndNames;

    .line 172
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->TT:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_trinidadandtobago:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_tt:I

    const-string v8, "TT"

    const/16 v9, 0x9d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TT:Lcom/squareup/address/CountryFlagsAndNames;

    .line 173
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->TN:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_tunisia:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_tn:I

    const-string v2, "TN"

    const/16 v3, 0x9e

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 174
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->TR:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_turkey:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_tr:I

    const-string v8, "TR"

    const/16 v9, 0x9f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TR:Lcom/squareup/address/CountryFlagsAndNames;

    .line 175
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->TM:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_turkmenistan:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_tm:I

    const-string v2, "TM"

    const/16 v3, 0xa0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 176
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->TC:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_turksandcaicosislands:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_tc:I

    const-string v8, "TC"

    const/16 v9, 0xa1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TC:Lcom/squareup/address/CountryFlagsAndNames;

    .line 177
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->TV:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_tuvalu:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_tv:I

    const-string v2, "TV"

    const/16 v3, 0xa2

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->TV:Lcom/squareup/address/CountryFlagsAndNames;

    .line 178
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->UG:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_uganda:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ug:I

    const-string v8, "UG"

    const/16 v9, 0xa3

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->UG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 179
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->UA:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_ukraine:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ua:I

    const-string v2, "UA"

    const/16 v3, 0xa4

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->UA:Lcom/squareup/address/CountryFlagsAndNames;

    .line 180
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->AE:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_unitedarabemirates:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_ae:I

    const-string v8, "AE"

    const/16 v9, 0xa5

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->AE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 181
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_unitedkingdom:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_gb:I

    const-string v2, "GB"

    const/16 v3, 0xa6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->GB:Lcom/squareup/address/CountryFlagsAndNames;

    .line 182
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_unitedstates:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_us:I

    const-string v8, "US"

    const/16 v9, 0xa7

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->US:Lcom/squareup/address/CountryFlagsAndNames;

    .line 183
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->UY:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_uruguay:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_uy:I

    const-string v2, "UY"

    const/16 v3, 0xa8

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->UY:Lcom/squareup/address/CountryFlagsAndNames;

    .line 184
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->UZ:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_uzbekistan:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_uz:I

    const-string v8, "UZ"

    const/16 v9, 0xa9

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->UZ:Lcom/squareup/address/CountryFlagsAndNames;

    .line 185
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->VE:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_venezuela_bolivarianrepublicof:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ve:I

    const-string v2, "VE"

    const/16 v3, 0xaa

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->VE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 186
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->VN:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_vietnam:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_vn:I

    const-string v8, "VN"

    const/16 v9, 0xab

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->VN:Lcom/squareup/address/CountryFlagsAndNames;

    .line 187
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->VG:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_virginislands_british:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_vg:I

    const-string v2, "VG"

    const/16 v3, 0xac

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->VG:Lcom/squareup/address/CountryFlagsAndNames;

    .line 188
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->VI:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_virginislands_us:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_vi:I

    const-string v8, "VI"

    const/16 v9, 0xad

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->VI:Lcom/squareup/address/CountryFlagsAndNames;

    .line 189
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->YE:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_yemen:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_ye:I

    const-string v2, "YE"

    const/16 v3, 0xae

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->YE:Lcom/squareup/address/CountryFlagsAndNames;

    .line 190
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v10, Lcom/squareup/CountryCode;->ZM:Lcom/squareup/CountryCode;

    sget v11, Lcom/squareup/country/resources/R$string;->country_zambia:I

    sget v12, Lcom/squareup/country/resources/R$drawable;->flag_zm:I

    const-string v8, "ZM"

    const/16 v9, 0xaf

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->ZM:Lcom/squareup/address/CountryFlagsAndNames;

    .line 191
    new-instance v0, Lcom/squareup/address/CountryFlagsAndNames;

    sget-object v4, Lcom/squareup/CountryCode;->ZW:Lcom/squareup/CountryCode;

    sget v5, Lcom/squareup/country/resources/R$string;->country_zimbabwe:I

    sget v6, Lcom/squareup/country/resources/R$drawable;->flag_zw:I

    const-string v2, "ZW"

    const/16 v3, 0xb0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/CountryFlagsAndNames;-><init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->ZW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v0, 0xb1

    new-array v0, v0, [Lcom/squareup/address/CountryFlagsAndNames;

    .line 14
    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AI:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AL:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->DZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AU:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AO:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AG:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AR:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AM:Lcom/squareup/address/CountryFlagsAndNames;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BS:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BH:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BD:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BB:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BJ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->BF:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CV:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KH:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TD:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CL:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CI:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->HR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->DK:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->DM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->DO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->EC:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->EG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SV:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->EE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->FJ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->FI:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->FR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->DE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GH:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GD:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GU:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->HT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->HN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->HK:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->HU:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->IS:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->IN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->ID:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->IE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->IL:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->IT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->JM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->JP:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->JO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LV:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LB:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LS:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LI:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LU:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MK:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->ML:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MU:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MX:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->FM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MD:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MS:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NP:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NL:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NI:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->MP:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->NO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->OM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PK:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PH:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PL:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->PR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->QA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->RO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->RU:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->RW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->ST:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->RS:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SC:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SL:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SK:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SI:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SB:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->ZA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->ES:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LK:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->KN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->LC:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->VC:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->SE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->CH:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TJ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TH:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TO:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TT:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TR:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TC:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->TV:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->UG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->UA:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->AE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->GB:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->US:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->UY:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->UZ:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->VE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->VN:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->VG:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->VI:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->YE:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->ZM:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/address/CountryFlagsAndNames;->ZW:Lcom/squareup/address/CountryFlagsAndNames;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/address/CountryFlagsAndNames;->$VALUES:[Lcom/squareup/address/CountryFlagsAndNames;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/CountryCode;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/CountryCode;",
            "II)V"
        }
    .end annotation

    .line 208
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 209
    iput-object p3, p0, Lcom/squareup/address/CountryFlagsAndNames;->countryCode:Lcom/squareup/CountryCode;

    .line 210
    iput p4, p0, Lcom/squareup/address/CountryFlagsAndNames;->countryName:I

    .line 211
    iput p5, p0, Lcom/squareup/address/CountryFlagsAndNames;->flagId:I

    return-void
.end method

.method static forCountryCode(Lcom/squareup/CountryCode;)Lcom/squareup/address/CountryFlagsAndNames;
    .locals 1

    if-eqz p0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/address/CountryFlagsAndNames;->valueOf(Ljava/lang/String;)Lcom/squareup/address/CountryFlagsAndNames;

    move-result-object p0

    return-object p0

    .line 204
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "null country"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/address/CountryFlagsAndNames;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/address/CountryFlagsAndNames;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/address/CountryFlagsAndNames;

    return-object p0
.end method

.method public static values()[Lcom/squareup/address/CountryFlagsAndNames;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/address/CountryFlagsAndNames;->$VALUES:[Lcom/squareup/address/CountryFlagsAndNames;

    invoke-virtual {v0}, [Lcom/squareup/address/CountryFlagsAndNames;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/address/CountryFlagsAndNames;

    return-object v0
.end method
