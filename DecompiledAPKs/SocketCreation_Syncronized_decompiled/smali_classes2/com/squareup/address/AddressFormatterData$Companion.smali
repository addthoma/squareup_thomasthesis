.class public final Lcom/squareup/address/AddressFormatterData$Companion;
.super Ljava/lang/Object;
.source "AddressFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/AddressFormatterData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/address/AddressFormatterData$Companion;",
        "",
        "()V",
        "fromAddress",
        "Lcom/squareup/address/AddressFormatterData;",
        "address",
        "Lcom/squareup/address/Address;",
        "fromReceiptAddress",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
        "address_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/address/AddressFormatterData$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromAddress(Lcom/squareup/address/Address;)Lcom/squareup/address/AddressFormatterData;
    .locals 9

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/squareup/address/AddressFormatterData;

    .line 67
    iget-object v2, p1, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    .line 68
    iget-object v3, p1, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    .line 70
    iget-object v6, p1, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    .line 71
    iget-object v8, p1, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    .line 72
    iget-object v7, p1, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    .line 73
    iget-object v5, p1, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    const-string v4, ""

    move-object v1, v0

    .line 66
    invoke-direct/range {v1 .. v8}, Lcom/squareup/address/AddressFormatterData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v0
.end method

.method public final fromReceiptAddress(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/address/AddressFormatterData;
    .locals 9

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/squareup/address/AddressFormatterData;

    .line 79
    iget-object v2, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    .line 80
    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    .line 81
    iget-object v4, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    .line 82
    iget-object v6, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    .line 83
    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {v1}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v8

    .line 84
    iget-object v7, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    .line 85
    iget-object v5, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    move-object v1, v0

    .line 78
    invoke-direct/range {v1 .. v8}, Lcom/squareup/address/AddressFormatterData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v0
.end method
