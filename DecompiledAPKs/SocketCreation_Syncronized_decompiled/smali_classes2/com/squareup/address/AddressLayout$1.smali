.class Lcom/squareup/address/AddressLayout$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "AddressLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/address/AddressLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/address/AddressLayout;


# direct methods
.method constructor <init>(Lcom/squareup/address/AddressLayout;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/address/AddressLayout$1;->this$0:Lcom/squareup/address/AddressLayout;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/address/AddressLayout$1;->this$0:Lcom/squareup/address/AddressLayout;

    invoke-static {v0}, Lcom/squareup/address/AddressLayout;->access$000(Lcom/squareup/address/AddressLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/AddressLayout$1;->this$0:Lcom/squareup/address/AddressLayout;

    invoke-static {v0}, Lcom/squareup/address/AddressLayout;->access$100(Lcom/squareup/address/AddressLayout;)Lcom/squareup/text/PostalScrubber;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/AddressLayout$1;->this$0:Lcom/squareup/address/AddressLayout;

    invoke-static {v0}, Lcom/squareup/address/AddressLayout;->access$100(Lcom/squareup/address/AddressLayout;)Lcom/squareup/text/PostalScrubber;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/text/PostalScrubber;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/address/AddressLayout$1;->this$0:Lcom/squareup/address/AddressLayout;

    iget-object v0, v0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    iget-object v1, p0, Lcom/squareup/address/AddressLayout$1;->this$0:Lcom/squareup/address/AddressLayout;

    invoke-static {v1}, Lcom/squareup/address/AddressLayout;->access$100(Lcom/squareup/address/AddressLayout;)Lcom/squareup/text/PostalScrubber;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/text/PostalScrubber;->shorten(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/address/AddressLayoutRunner;->onPostalCodeEntered(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
