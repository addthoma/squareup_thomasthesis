.class public final Lcom/squareup/brandaudio/MediaPlayerFactory;
.super Ljava/lang/Object;
.source "MediaPlayerFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/brandaudio/MediaPlayerFactory;",
        "",
        "()V",
        "create",
        "Lcom/squareup/brandaudio/AudioPlayer;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create()Lcom/squareup/brandaudio/AudioPlayer;
    .locals 1

    .line 6
    new-instance v0, Lcom/squareup/brandaudio/RealAudioPlayer;

    invoke-direct {v0}, Lcom/squareup/brandaudio/RealAudioPlayer;-><init>()V

    check-cast v0, Lcom/squareup/brandaudio/AudioPlayer;

    return-object v0
.end method
