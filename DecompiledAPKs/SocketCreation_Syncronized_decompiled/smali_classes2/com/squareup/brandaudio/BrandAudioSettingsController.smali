.class public final Lcom/squareup/brandaudio/BrandAudioSettingsController;
.super Ljava/lang/Object;
.source "BrandAudioSettingsController.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;,
        Lcom/squareup/brandaudio/BrandAudioSettingsController$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBrandAudioSettingsController.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BrandAudioSettingsController.kt\ncom/squareup/brandaudio/BrandAudioSettingsController\n*L\n1#1,67:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\u0018\u0000 \u000f2\u00020\u0001:\u0002\u000f\u0010B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\t\u001a\u00020\nJ\u0008\u0010\u000b\u001a\u00020\nH\u0002J\u0010\u0010\u000c\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u0008H\u0002J\u0006\u0010\u000e\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/brandaudio/BrandAudioSettingsController;",
        "",
        "audioManager",
        "Landroid/media/AudioManager;",
        "(Landroid/media/AudioManager;)V",
        "defaultAudioState",
        "Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;",
        "getMaxVolume",
        "",
        "restoreDefaultAudioSettings",
        "",
        "saveDefaultAudioSettings",
        "setVolume",
        "volume",
        "turnUpAudioSettings",
        "Companion",
        "DefaultAudioSettings",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/brandaudio/BrandAudioSettingsController$Companion;

.field public static final STREAM_TYPE:I = 0x4

.field private static final volumeAdjustment:F = 0.75f


# instance fields
.field private final audioManager:Landroid/media/AudioManager;

.field private defaultAudioState:Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/brandaudio/BrandAudioSettingsController$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/brandaudio/BrandAudioSettingsController$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->Companion:Lcom/squareup/brandaudio/BrandAudioSettingsController$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/media/AudioManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "audioManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->audioManager:Landroid/media/AudioManager;

    return-void
.end method

.method public static final synthetic access$getDefaultAudioState$p(Lcom/squareup/brandaudio/BrandAudioSettingsController;)Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;
    .locals 1

    .line 18
    iget-object p0, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->defaultAudioState:Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;

    if-nez p0, :cond_0

    const-string v0, "defaultAudioState"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setDefaultAudioState$p(Lcom/squareup/brandaudio/BrandAudioSettingsController;Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->defaultAudioState:Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;

    return-void
.end method

.method private final saveDefaultAudioSettings()V
    .locals 3

    .line 49
    new-instance v0, Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;

    iget-object v1, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->defaultAudioState:Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;

    return-void
.end method

.method private final setVolume(I)V
    .locals 3

    .line 53
    iget-object v0, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isVolumeFixed()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Unable to set volume - device has fixed volume."

    .line 54
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, p1, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method


# virtual methods
.method public final getMaxVolume()I
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float v0, v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public final restoreDefaultAudioSettings()V
    .locals 4

    .line 39
    move-object v0, p0

    check-cast v0, Lcom/squareup/brandaudio/BrandAudioSettingsController;

    iget-object v0, v0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->defaultAudioState:Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;

    if-nez v0, :cond_0

    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->defaultAudioState:Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;

    if-nez v0, :cond_1

    const-string v1, "defaultAudioState"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 43
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restoring audio mode: volume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;->getVolume()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v1, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iget-object v1, p0, Lcom/squareup/brandaudio/BrandAudioSettingsController;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x4

    invoke-virtual {v0}, Lcom/squareup/brandaudio/BrandAudioSettingsController$DefaultAudioSettings;->getVolume()I

    move-result v0

    invoke-virtual {v1, v3, v0, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method

.method public final turnUpAudioSettings()V
    .locals 3

    .line 32
    invoke-virtual {p0}, Lcom/squareup/brandaudio/BrandAudioSettingsController;->getMaxVolume()I

    move-result v0

    .line 33
    invoke-direct {p0}, Lcom/squareup/brandaudio/BrandAudioSettingsController;->saveDefaultAudioSettings()V

    .line 34
    invoke-direct {p0, v0}, Lcom/squareup/brandaudio/BrandAudioSettingsController;->setVolume(I)V

    .line 35
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Turning up audio mode: volume - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " mode - 4"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
