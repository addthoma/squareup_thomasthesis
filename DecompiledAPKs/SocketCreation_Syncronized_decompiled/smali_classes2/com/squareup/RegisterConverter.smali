.class public Lcom/squareup/RegisterConverter;
.super Ljava/lang/Object;
.source "RegisterConverter.java"

# interfaces
.implements Lcom/squareup/tape/FileObjectQueue$Converter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/RegisterConverter$MoneyFixingObjectInputStream;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/FileObjectQueue$Converter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

.field private final emptyBytesObject:Ljava/io/Serializable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final gsonConverter:Lcom/squareup/queue/QueueGsonConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/QueueGsonConverter<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/QueueGsonConverter;Ljava/io/Serializable;Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueGsonConverter<",
            "TT;>;TT;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/RegisterConverter;->gsonConverter:Lcom/squareup/queue/QueueGsonConverter;

    .line 28
    iput-object p2, p0, Lcom/squareup/RegisterConverter;->emptyBytesObject:Ljava/io/Serializable;

    .line 29
    iput-object p3, p0, Lcom/squareup/RegisterConverter;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    return-void
.end method

.method private deserialize(Ljava/io/InputStream;)Ljava/io/Serializable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/RegisterConverter$MoneyFixingObjectInputStream;

    new-instance v1, Ljava/io/BufferedInputStream;

    const/16 v2, 0x400

    invoke-direct {v1, p1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v0, v1}, Lcom/squareup/RegisterConverter$MoneyFixingObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 37
    :try_start_0
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->readUnshared()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/Serializable;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 40
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private static isSerialized([B)Z
    .locals 4

    const/4 v0, 0x0

    .line 60
    aget-byte v1, p0, v0

    const/4 v2, 0x1

    const/16 v3, -0x54

    if-ne v1, v3, :cond_0

    aget-byte p0, p0, v2

    const/16 v1, -0x13

    if-ne p0, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public from([B)Ljava/io/Serializable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    array-length v0, p1

    if-nez v0, :cond_0

    .line 48
    iget-object p1, p0, Lcom/squareup/RegisterConverter;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Should not happen: 0 byte entry in queue."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/queue/CorruptQueueRecorder;->recordCorruptQueue(Ljava/lang/Throwable;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/RegisterConverter;->emptyBytesObject:Ljava/io/Serializable;

    return-object p1

    .line 52
    :cond_0
    invoke-static {p1}, Lcom/squareup/RegisterConverter;->isSerialized([B)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Lcom/squareup/RegisterConverter;->deserialize(Ljava/io/InputStream;)Ljava/io/Serializable;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/RegisterConverter;->gsonConverter:Lcom/squareup/queue/QueueGsonConverter;

    .line 53
    invoke-virtual {v0, p1}, Lcom/squareup/queue/QueueGsonConverter;->from([B)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/Serializable;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic from([B)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/RegisterConverter;->from([B)Ljava/io/Serializable;

    move-result-object p1

    return-object p1
.end method

.method public toStream(Ljava/io/Serializable;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/RegisterConverter;->gsonConverter:Lcom/squareup/queue/QueueGsonConverter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/queue/QueueGsonConverter;->toStream(Ljava/lang/Object;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic toStream(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    check-cast p1, Ljava/io/Serializable;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/RegisterConverter;->toStream(Ljava/io/Serializable;Ljava/io/OutputStream;)V

    return-void
.end method
