.class public Lcom/squareup/MortarLoggedIn;
.super Ljava/lang/Object;
.source "MortarLoggedIn.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 16
    invoke-static {}, Lcom/squareup/AppDelegate$Locator;->getAppDelegate()Lcom/squareup/AppDelegate;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static getLoggedInScope()Lmortar/MortarScope;
    .locals 1

    .line 12
    invoke-static {}, Lcom/squareup/AppDelegate$Locator;->getAppDelegate()Lcom/squareup/AppDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/AppDelegate;->getLoggedInMortarScope()Lmortar/MortarScope;

    move-result-object v0

    return-object v0
.end method
