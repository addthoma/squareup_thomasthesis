.class Lcom/squareup/RegisterAppDelegate$1;
.super Ljava/lang/Object;
.source "RegisterAppDelegate.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/RegisterAppDelegate;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/RegisterAppDelegate;


# direct methods
.method constructor <init>(Lcom/squareup/RegisterAppDelegate;)V
    .locals 0

    .line 472
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate$1;->this$0:Lcom/squareup/RegisterAppDelegate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 474
    iget-object p1, p0, Lcom/squareup/RegisterAppDelegate$1;->this$0:Lcom/squareup/RegisterAppDelegate;

    iget-object p1, p1, Lcom/squareup/RegisterAppDelegate;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate$1;->this$0:Lcom/squareup/RegisterAppDelegate;

    iget-object v0, v0, Lcom/squareup/RegisterAppDelegate;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 478
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate$1;->this$0:Lcom/squareup/RegisterAppDelegate;

    iget-object v0, v0, Lcom/squareup/RegisterAppDelegate;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate$1;->this$0:Lcom/squareup/RegisterAppDelegate;

    iget-object v1, v1, Lcom/squareup/RegisterAppDelegate;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method
