.class public abstract Lcom/squareup/SposReleaseAppComponentModule;
.super Ljava/lang/Object;
.source "SposReleaseAppComponentModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/location/AndroidGeoProdLocationModule;,
        Lcom/squareup/development/drawer/ReleaseDevelopmentDrawerModule;,
        Lcom/squareup/PosReleaseAppComponent$Module;,
        Lcom/squareup/SposAppModule;,
        Lcom/squareup/firebase/fcm/FcmModule;,
        Lcom/squareup/cardreader/ReleaseCardReaderCompleteModule;,
        Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;,
        Lcom/squareup/cardreader/CardReaderNativeLoggingDisabledModule;,
        Lcom/squareup/adanalytics/AdAnalyticsModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideApiMainActivityComponent()Ljava/lang/Class;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoMap;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 43
    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule;->provideMainActivityComponent()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method static provideLoggedOutActivityComponent()Ljava/lang/Class;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoMap;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 49
    const-class v0, Lcom/squareup/ui/root/SposReleaseLoggedOutActivityComponent;

    return-object v0
.end method

.method static provideMainActivityComponent()Ljava/lang/Class;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoMap;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 37
    const-class v0, Lcom/squareup/ui/root/SposReleaseMainActivityComponent;

    return-object v0
.end method

.method static provideOnboardingActivityComponent()Ljava/lang/Class;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoMap;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 55
    const-class v0, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    return-object v0
.end method
