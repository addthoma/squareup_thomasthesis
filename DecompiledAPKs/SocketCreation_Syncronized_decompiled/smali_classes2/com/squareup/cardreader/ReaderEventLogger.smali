.class public interface abstract Lcom/squareup/cardreader/ReaderEventLogger;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;,
        Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;,
        Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;,
        Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;,
        Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;
    }
.end annotation


# virtual methods
.method public abstract initialize()V
.end method

.method public abstract logAudioEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V
.end method

.method public abstract logBatteryInfo(ILcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract logBleConnectionAction(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleAction;)V
.end method

.method public abstract logBleConnectionEnqueued(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)V
.end method

.method public abstract logBleConnectionStateChange(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V
.end method

.method public abstract logBleDisconnectedEvent(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;ZLcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;)V
.end method

.method public abstract logBleReaderForceUnpaired(Lcom/squareup/cardreader/WirelessConnection;)V
.end method

.method public abstract logBluetoothStatusChanged(Ljava/lang/String;ZZ)V
.end method

.method public abstract logCirqueTamperStatus(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/bills/CardData$ReaderType;I)V
.end method

.method public abstract logCommsRateUpdated(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract logCommsVersionAcquired(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
.end method

.method public abstract logCoreDumpResult(Lcom/squareup/cardreader/CardReaderInfo;Z)V
.end method

.method public abstract logEvent(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
.end method

.method public abstract logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V
.end method

.method public abstract logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
.end method

.method public abstract logFirmwareAssetVersionInfo(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V
.end method

.method public abstract logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
.end method

.method public abstract logReaderError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsReaderError;)V
.end method

.method public abstract logSecureSessionResult(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
.end method

.method public abstract logSecureSessionRevoked(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
.end method

.method public abstract logSerialNumberReceived(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)V
.end method

.method public abstract logSystemCapabilities(ZLjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract logTamperData(ILcom/squareup/cardreader/CardReaderInfo;[B)V
.end method

.method public abstract logTamperResult(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
.end method

.method public abstract logTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/CountryCode;Lcom/squareup/CountryCode;)V
.end method
