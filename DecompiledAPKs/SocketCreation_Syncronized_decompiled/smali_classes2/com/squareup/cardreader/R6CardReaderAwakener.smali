.class public Lcom/squareup/cardreader/R6CardReaderAwakener;
.super Ljava/lang/Object;
.source "R6CardReaderAwakener.java"


# instance fields
.field private final cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

.field private final cardReaderDispatch:Lcom/squareup/cardreader/CardReaderDispatch;

.field private final clock:Lcom/squareup/util/Clock;

.field private firstTime:Z

.field private lastStop:J


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/CardReaderConstants;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->cardReaderDispatch:Lcom/squareup/cardreader/CardReaderDispatch;

    .line 20
    iput-object p2, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->clock:Lcom/squareup/util/Clock;

    .line 21
    iput-object p3, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    const-wide/16 p1, 0x0

    .line 22
    iput-wide p1, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->lastStop:J

    const/4 p1, 0x1

    .line 23
    iput-boolean p1, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->firstTime:Z

    return-void
.end method


# virtual methods
.method public resume()V
    .locals 6

    .line 28
    iget-boolean v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->firstTime:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "Powering on reader on first insert or first resume."

    .line 29
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->cardReaderDispatch:Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->powerOn()V

    .line 31
    iput-boolean v1, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->firstTime:Z

    goto :goto_0

    .line 32
    :cond_0
    iget-wide v2, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->lastStop:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->clock:Lcom/squareup/util/Clock;

    .line 33
    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->lastStop:J

    sub-long/2addr v2, v4

    iget-object v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 34
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderConstants;->transportPauseTimeInMilliseconds()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 35
    iget-object v2, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->clock:Lcom/squareup/util/Clock;

    .line 36
    invoke-interface {v2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->lastStop:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Resuming after %ds. Powering on reader."

    .line 35
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->cardReaderDispatch:Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->powerOn()V

    :cond_1
    :goto_0
    return-void
.end method

.method public stop()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener;->lastStop:J

    return-void
.end method
