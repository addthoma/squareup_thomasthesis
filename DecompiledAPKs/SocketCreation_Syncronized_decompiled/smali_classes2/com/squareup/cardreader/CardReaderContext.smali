.class public Lcom/squareup/cardreader/CardReaderContext;
.super Ljava/lang/Object;
.source "CardReaderContext.java"


# instance fields
.field public final cardReader:Lcom/squareup/cardreader/CardReader;

.field public final cardReaderDispatch:Lcom/squareup/cardreader/CardReaderDispatch;

.field public final cardReaderGraphInitializer:Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

.field public final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 16
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderContext;->cardReader:Lcom/squareup/cardreader/CardReader;

    .line 17
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 18
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderDispatch:Lcom/squareup/cardreader/CardReaderDispatch;

    .line 19
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderGraphInitializer:Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    return-void
.end method
