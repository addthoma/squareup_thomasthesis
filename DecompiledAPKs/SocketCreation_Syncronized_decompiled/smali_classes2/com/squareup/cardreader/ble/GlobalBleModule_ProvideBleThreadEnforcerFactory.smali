.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBleThreadEnforcerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Thread;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;->bleThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Thread;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBleThreadEnforcer(Ljavax/inject/Provider;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Thread;",
            ">;)",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;"
        }
    .end annotation

    .line 35
    invoke-static {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBleThreadEnforcer(Ljavax/inject/Provider;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;->bleThreadProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;->provideBleThreadEnforcer(Ljavax/inject/Provider;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadEnforcerFactory;->get()Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    return-object v0
.end method
