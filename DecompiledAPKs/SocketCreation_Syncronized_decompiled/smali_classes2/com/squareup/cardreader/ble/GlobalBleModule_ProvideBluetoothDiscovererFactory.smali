.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBluetoothDiscovererFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    .line 33
    iput-object p2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->bluetoothDiscovererProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBluetoothDiscoverer(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;"
        }
    .end annotation

    .line 52
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBluetoothDiscoverer(Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->bluetoothDiscovererProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->provideBluetoothDiscoverer(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBluetoothDiscovererFactory;->get()Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    move-result-object v0

    return-object v0
.end method
