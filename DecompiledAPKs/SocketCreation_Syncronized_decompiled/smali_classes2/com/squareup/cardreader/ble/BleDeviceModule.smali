.class public Lcom/squareup/cardreader/ble/BleDeviceModule;
.super Ljava/lang/Object;
.source "BleDeviceModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/cardreader/LocalCardReaderModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleDeviceModule$BLE;
    }
.end annotation


# instance fields
.field private final bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

.field private final useV2NewStateMachine:Z

.field private final wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;Z)V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    .line 59
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 60
    iput-boolean p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->useV2NewStateMachine:Z

    return-void
.end method

.method static provideBleBackend(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/BleBackendLegacy;
    .locals 12
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            "Lcom/squareup/tmn/TmnTimings;",
            ")",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;"
        }
    .end annotation

    .line 117
    new-instance v11, Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/cardreader/ble/BleBackendLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;Lcom/squareup/tmn/TmnTimings;)V

    return-object v11
.end method

.method static provideBleConnectionStateMachine(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Landroid/os/Handler;Lcom/squareup/cardreader/ble/BleReceiverFactory;Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/cardreader/ble/BleConnectionStateMachine;
    .locals 15
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 141
    new-instance v14, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;-><init>(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Landroid/os/Handler;Lcom/squareup/cardreader/ble/BleReceiverFactory;Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v14
.end method

.method static provideBleReceiverFactory(Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/BleReceiverFactory;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 130
    new-instance v6, Lcom/squareup/cardreader/ble/BleReceiverFactory;

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v3

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/ble/BleReceiverFactory;-><init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Ljava/lang/String;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)V

    return-object v6
.end method

.method static provideBleSender(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/ble/BleSender;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 77
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/ble/BleSender;-><init>(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;)V

    return-object v0
.end method

.method static provideCardReaderConnector(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/CardReaderConnector;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 166
    new-instance v0, Lcom/squareup/cardreader/CardReaderConnector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/CardReaderConnector;-><init>(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/thread/executor/MainThread;)V

    return-object v0
.end method

.method static provideConnectionType()Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 67
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    return-object v0
.end method

.method static provideLcrBackend(Lcom/squareup/cardreader/ble/BleBackendLegacy;)Lcom/squareup/cardreader/LcrBackend;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p0
.end method


# virtual methods
.method provideBleBackendListener(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 102
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->useV2NewStateMachine:Z

    if-eqz v0, :cond_0

    return-object p2

    :cond_0
    return-object p1
.end method

.method provideBleBackendListenerV2(Lcom/squareup/cardreader/ble/StateMachineV2;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BleBackendListenerV2;
    .locals 11
    .annotation runtime Ldagger/Provides;
    .end annotation

    move-object v0, p0

    .line 94
    new-instance v10, Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/BleDeviceModule;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    iget-object v1, v0, Lcom/squareup/cardreader/ble/BleDeviceModule;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 96
    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/BleConnectType;->isAutoConnect()Z

    move-result v9

    move-object v1, v10

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;-><init>(Lcom/squareup/cardreader/ble/StateMachineV2;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/BluetoothUtils;Z)V

    return-object v10
.end method

.method provideBleCardReaderGraphInitializer(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    iget-boolean v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->useV2NewStateMachine:Z

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleBackendListenerV2;Lcom/squareup/cardreader/ble/BleConnectType;Z)V

    return-object v0
.end method

.method provideBleConnectionStateMachineV2(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lkotlinx/coroutines/CoroutineDispatcher;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/StateMachineV2;
    .locals 8
    .param p3    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 151
    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleConnectType;->isAutoConnect()Z

    move-result v3

    new-instance v7, Lcom/squareup/blecoroutines/RealConnectable;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    .line 152
    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-direct {v7, p3, v0, p5}, Lcom/squareup/blecoroutines/RealConnectable;-><init>(Lkotlinx/coroutines/CoroutineDispatcher;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/tmn/TmnTimings;)V

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 150
    invoke-static/range {v1 .. v7}, Lcom/squareup/cardreader/ble/Factory;->create(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;ZLcom/squareup/thread/executor/MainThread;Lkotlinx/coroutines/CoroutineDispatcher;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;)Lcom/squareup/cardreader/ble/StateMachineV2;

    move-result-object p1

    return-object p1
.end method

.method provideBluetoothDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method provideCardReaderAddress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method provideCardReaderName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule;->wirelessConnection:Lcom/squareup/cardreader/WirelessConnection;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
