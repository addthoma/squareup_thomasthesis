.class public final Lcom/squareup/cardreader/ble/RealConnectionEvents;
.super Ljava/lang/Object;
.source "RealConnectionEvents.kt"

# interfaces
.implements Lcom/squareup/cardreader/ble/ConnectionEvents;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0012\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0002\n\u0002\u0008\u0010\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0015\u0010*\u001a\u00020+2\u0006\u0010\u0005\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\u0008,J\u0015\u0010-\u001a\u00020+2\u0006\u0010\u0011\u001a\u00020\u0012H\u0000\u00a2\u0006\u0002\u0008.J\u0015\u0010/\u001a\u00020+2\u0006\u00100\u001a\u00020\u0016H\u0000\u00a2\u0006\u0002\u00081J\u0015\u00102\u001a\u00020+2\u0006\u00103\u001a\u00020\u001aH\u0000\u00a2\u0006\u0002\u00084J\u0015\u00105\u001a\u00020+2\u0006\u0010$\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\u00086J\u0015\u00107\u001a\u00020+2\u0006\u0010\'\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\u00088J\u0015\u00109\u001a\u00020+2\u0006\u00103\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008:R\u001a\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u001c\u0010\n\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00070\u00070\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\tR\u001c\u0010\u0010\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000e0\u000e0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\tR\u001c\u0010\u0014\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00120\u00120\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\tR\u001c\u0010\u0018\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00160\u00160\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\tR\u001c\u0010!\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u001a0\u001a0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#R\u001a\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\tR\u001c\u0010&\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00070\u00070\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\tR\u001c\u0010)\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00070\u00070\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "Lcom/squareup/cardreader/ble/ConnectionEvents;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "(Lcom/squareup/thread/executor/MainThread;)V",
        "connectionInterval",
        "Lio/reactivex/Observable;",
        "",
        "getConnectionInterval",
        "()Lio/reactivex/Observable;",
        "connectionIntervalRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "kotlin.jvm.PlatformType",
        "connectionState",
        "Lcom/squareup/cardreader/ble/ConnectionState;",
        "getConnectionState",
        "connectionStateRelay",
        "data",
        "",
        "getData",
        "dataRelay",
        "deviceInfo",
        "Lcom/squareup/cardreader/ble/DeviceInfo;",
        "getDeviceInfo",
        "deviceInfoRelay",
        "lastLoggingState",
        "Lcom/squareup/cardreader/ble/R12State;",
        "getLastLoggingState$impl_release",
        "()Lcom/squareup/cardreader/ble/R12State;",
        "setLastLoggingState$impl_release",
        "(Lcom/squareup/cardreader/ble/R12State;)V",
        "loggingState",
        "getLoggingState",
        "loggingStateRelay",
        "getMainThread",
        "()Lcom/squareup/thread/executor/MainThread;",
        "mtu",
        "getMtu",
        "mtuRelay",
        "rssi",
        "getRssi",
        "rssiRelay",
        "publishConnectionInterval",
        "",
        "publishConnectionInterval$impl_release",
        "publishData",
        "publishData$impl_release",
        "publishDeviceInfo",
        "info",
        "publishDeviceInfo$impl_release",
        "publishLoggingState",
        "state",
        "publishLoggingState$impl_release",
        "publishMtu",
        "publishMtu$impl_release",
        "publishRssi",
        "publishRssi$impl_release",
        "publishState",
        "publishState$impl_release",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectionInterval:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionIntervalRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ble/ConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/cardreader/ble/ConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "[B>;"
        }
    .end annotation
.end field

.field private final dataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "[B>;"
        }
    .end annotation
.end field

.field private final deviceInfo:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ble/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceInfoRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/cardreader/ble/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private lastLoggingState:Lcom/squareup/cardreader/ble/R12State;

.field private final loggingState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ble/R12State;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/cardreader/ble/R12State;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final mtu:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mtuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final rssi:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final rssiRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;)V
    .locals 2

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 12
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<ConnectionState>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 13
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<ByteArray>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->dataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 14
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<Int>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mtuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 15
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v1, "BehaviorRelay.create<R12State>()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->loggingStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 16
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v1, "BehaviorRelay.create<DeviceInfo>()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->deviceInfoRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 17
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->rssiRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 18
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionIntervalRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 28
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionState:Lio/reactivex/Observable;

    .line 33
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->dataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->data:Lio/reactivex/Observable;

    .line 38
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mtuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mtu:Lio/reactivex/Observable;

    .line 43
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->loggingStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->loggingState:Lio/reactivex/Observable;

    .line 51
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->deviceInfoRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->deviceInfo:Lio/reactivex/Observable;

    .line 53
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->rssiRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->rssi:Lio/reactivex/Observable;

    .line 55
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionIntervalRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionInterval:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getConnectionIntervalRelay$p(Lcom/squareup/cardreader/ble/RealConnectionEvents;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionIntervalRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getConnectionStateRelay$p(Lcom/squareup/cardreader/ble/RealConnectionEvents;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getDeviceInfoRelay$p(Lcom/squareup/cardreader/ble/RealConnectionEvents;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->deviceInfoRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getLoggingStateRelay$p(Lcom/squareup/cardreader/ble/RealConnectionEvents;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->loggingStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getMtuRelay$p(Lcom/squareup/cardreader/ble/RealConnectionEvents;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mtuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getRssiRelay$p(Lcom/squareup/cardreader/ble/RealConnectionEvents;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->rssiRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method public getConnectionInterval()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionInterval:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getConnectionState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ble/ConnectionState;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->connectionState:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "[B>;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->data:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getDeviceInfo()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ble/DeviceInfo;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->deviceInfo:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final getLastLoggingState$impl_release()Lcom/squareup/cardreader/ble/R12State;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->lastLoggingState:Lcom/squareup/cardreader/ble/R12State;

    return-object v0
.end method

.method public getLoggingState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ble/R12State;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->loggingState:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final getMainThread()Lcom/squareup/thread/executor/MainThread;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object v0
.end method

.method public getMtu()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mtu:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getRssi()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->rssi:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final publishConnectionInterval$impl_release(I)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishConnectionInterval$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishConnectionInterval$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionEvents;I)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final publishData$impl_release([B)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->dataRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final publishDeviceInfo$impl_release(Lcom/squareup/cardreader/ble/DeviceInfo;)V
    .locals 2

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishDeviceInfo$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishDeviceInfo$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/cardreader/ble/DeviceInfo;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->lastLoggingState:Lcom/squareup/cardreader/ble/R12State;

    .line 76
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishLoggingState$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishLoggingState$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/cardreader/ble/R12State;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final publishMtu$impl_release(I)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishMtu$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishMtu$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionEvents;I)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final publishRssi$impl_release(I)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishRssi$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishRssi$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionEvents;I)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final publishState$impl_release(Lcom/squareup/cardreader/ble/ConnectionState;)V
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishState$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents$publishState$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/cardreader/ble/ConnectionState;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final setLastLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionEvents;->lastLoggingState:Lcom/squareup/cardreader/ble/R12State;

    return-void
.end method
