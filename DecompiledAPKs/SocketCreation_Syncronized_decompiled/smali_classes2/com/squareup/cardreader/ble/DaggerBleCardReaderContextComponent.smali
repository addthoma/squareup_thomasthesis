.class public final Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;
.super Ljava/lang/Object;
.source "DaggerBleCardReaderContextComponent.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleCardReaderContextComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleThreadEnforcer;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleExecutor;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideBleBondingBroadcastReceiver;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideMinesweeper;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_magSwipeFailureFilter;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideSquidInterfaceScheduler;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideNativeBinaries;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_touchReporting;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_isReaderSdk;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_minesweeperTicket;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderHub;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bluetoothUtils;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_realCardReaderListeners;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideTmnTimings;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleDispatcher;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainDispatcher;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_application;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderPauseAndResumer;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_crashnado;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_lcrExecutor;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderFactory;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderListeners;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainThread;,
        Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;
    }
.end annotation


# instance fields
.field private applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private bleDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private bleExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private bleThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

.field private isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private magSwipeFailureFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            ">;"
        }
    .end annotation
.end field

.field private mainDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private minesweeperTicketProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;"
        }
    .end annotation
.end field

.field private nativeLoggingEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleBackendListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleBackendListenerV2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendListenerV2;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleBackendNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleCardReaderGraphInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleConnectionStateMachineProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleConnectionStateMachine;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleConnectionStateMachineV2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/StateMachineV2;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleReceiverFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleReceiverFactory;",
            ">;"
        }
    .end annotation
.end field

.field private provideBleSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;"
        }
    .end annotation
.end field

.field private provideBluetoothDeviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderAddressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderConnectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderConstantsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderGraphInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderLogBridge;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderSwigProvider:Ljavax/inject/Provider;

.field private provideCardreaderNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideConnectionTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;",
            ">;"
        }
    .end annotation
.end field

.field private provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

.field private provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventLogFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/EventLogFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private provideLcrBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;"
        }
    .end annotation
.end field

.field private provideLocalCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LocalCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private provideLogNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/LogNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideMinesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private provideNativeBinariesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/NativeBinaries;",
            ">;"
        }
    .end annotation
.end field

.field private provideNfcListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentCompletionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureProvider:Ljavax/inject/Provider;

.field private providePaymentListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private providePowerFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private providePowerFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideResProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

.field private provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private provideSystemFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSystemFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTamperFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideTamperFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimerApiProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TimerApiLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimerNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TimerNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideTmnTimingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserInteractionFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private realCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private touchReportingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/TouchReporting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V
    .locals 0

    .line 308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->initialize(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$1;)V
    .locals 0

    .line 128
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    return-void
.end method

.method public static builder()Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;
    .locals 2

    .line 314
    new-instance v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;-><init>(Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$1;)V

    return-object v0
.end method

.method private initialize(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V
    .locals 33

    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    .line 321
    invoke-static/range {p2 .. p2}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderIdFactory;->create(Lcom/squareup/cardreader/CardReaderModule;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderIdFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    .line 322
    invoke-static {}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideConnectionTypeFactory;->create()Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideConnectionTypeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideConnectionTypeProvider:Ljavax/inject/Provider;

    .line 323
    invoke-static/range {p1 .. p1}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;->create(Lcom/squareup/cardreader/ble/BleDeviceModule;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderAddressProvider:Ljavax/inject/Provider;

    .line 324
    invoke-static/range {p1 .. p1}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;->create(Lcom/squareup/cardreader/ble/BleDeviceModule;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderNameProvider:Ljavax/inject/Provider;

    .line 325
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideConnectionTypeProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderAddressProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderNameProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    .line 326
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainThread;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainThread;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    .line 327
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderListeners;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderListeners;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 328
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderFactory;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderFactory;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 329
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_lcrExecutor;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_lcrExecutor;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 330
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->nativeLoggingEnabledProvider:Ljavax/inject/Provider;

    .line 331
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_crashnado;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_crashnado;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    .line 332
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderPauseAndResumer;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderPauseAndResumer;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    .line 333
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleSenderProvider:Ljavax/inject/Provider;

    .line 334
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_application;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_application;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    .line 335
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainDispatcher;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainDispatcher;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainDispatcherProvider:Ljavax/inject/Provider;

    .line 336
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleDispatcher;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleDispatcher;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bleDispatcherProvider:Ljavax/inject/Provider;

    .line 337
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideTmnTimings;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideTmnTimings;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    .line 338
    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainDispatcherProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bleDispatcherProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->create(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleConnectionStateMachineV2Provider:Ljavax/inject/Provider;

    .line 339
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendProvider:Ljavax/inject/Provider;

    .line 340
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_realCardReaderListeners;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_realCardReaderListeners;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 341
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bluetoothUtils;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bluetoothUtils;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 342
    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleConnectionStateMachineV2Provider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v7}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->create(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendListenerV2Provider:Ljavax/inject/Provider;

    .line 343
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleSenderProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendListenerV2Provider:Ljavax/inject/Provider;

    invoke-static {v8, v1, v2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->create(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendListenerProvider:Ljavax/inject/Provider;

    .line 344
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    .line 345
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderHub;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderHub;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 346
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderHubProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderConnectorProvider:Ljavax/inject/Provider;

    .line 347
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    .line 348
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    .line 349
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    .line 350
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideBleBackendNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideBleBackendNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendNativeProvider:Ljavax/inject/Provider;

    .line 351
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCardreaderNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideCardreaderNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    .line 352
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendListenerProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderConnectorProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendNativeProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    move-object/from16 v18, v4

    move-object/from16 v19, v5

    invoke-static/range {v10 .. v19}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 353
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideLcrBackendFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideLcrBackendFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLcrBackendProvider:Ljavax/inject/Provider;

    .line 354
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideTimerNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideTimerNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTimerNativeProvider:Ljavax/inject/Provider;

    .line 355
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTimerNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTimerApiProvider:Ljavax/inject/Provider;

    .line 356
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideLogNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideLogNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLogNativeProvider:Ljavax/inject/Provider;

    .line 357
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLogNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/LcrModule_Real_ProvideCardReaderLoggerInterfaceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_Real_ProvideCardReaderLoggerInterfaceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;

    .line 358
    invoke-static {}, Lcom/squareup/cardreader/NativeCardReaderConstants_Factory;->create()Lcom/squareup/cardreader/NativeCardReaderConstants_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    .line 359
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->nativeLoggingEnabledProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLcrBackendProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTimerApiProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    invoke-static/range {v10 .. v17}, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 360
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;

    .line 361
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/LcrModule_ProvideCoreDumpFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideCoreDumpFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

    .line 362
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideEventlogFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideEventlogFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;

    .line 363
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/LcrModule_ProvideEventLogFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideEventLogFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideEventLogFeatureProvider:Ljavax/inject/Provider;

    .line 364
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideFirmwareUpdateFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideFirmwareUpdateFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    .line 365
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

    .line 366
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentFeatureNativeProvider:Ljavax/inject/Provider;

    .line 367
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;

    .line 368
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;

    .line 369
    iget-object v10, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentFeatureNativeProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    invoke-static/range {v10 .. v16}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentFeatureProvider:Ljavax/inject/Provider;

    .line 370
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePowerFeatureNativeProvider:Ljavax/inject/Provider;

    .line 371
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePowerFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePowerFeatureProvider:Ljavax/inject/Provider;

    .line 372
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_minesweeperTicket;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_minesweeperTicket;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->minesweeperTicketProvider:Ljavax/inject/Provider;

    .line 373
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    .line 374
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    .line 375
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_isReaderSdk;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_isReaderSdk;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 376
    iget-object v10, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->minesweeperTicketProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->isReaderSdkProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    invoke-static/range {v10 .. v16}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

    .line 377
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchFeatureNativeInterfaceFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchFeatureNativeInterfaceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    .line 378
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_touchReporting;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_touchReporting;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->touchReportingProvider:Ljavax/inject/Provider;

    .line 379
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideNativeBinaries;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideNativeBinaries;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideNativeBinariesProvider:Ljavax/inject/Provider;

    .line 380
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideSquidInterfaceScheduler;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideSquidInterfaceScheduler;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    .line 381
    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->touchReportingProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideNativeBinariesProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v7}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    .line 382
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSystemFeatureNativeProvider:Ljavax/inject/Provider;

    .line 383
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSystemFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSystemFeatureProvider:Ljavax/inject/Provider;

    .line 384
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTamperFeatureNativeProvider:Ljavax/inject/Provider;

    .line 385
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTamperFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTamperFeatureProvider:Ljavax/inject/Provider;

    .line 386
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;

    .line 387
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideUserInteractionFeatureProvider:Ljavax/inject/Provider;

    .line 388
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    .line 389
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderListenerProvider:Ljavax/inject/Provider;

    .line 390
    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    .line 391
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    .line 392
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdateListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdateListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;

    .line 393
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_magSwipeFailureFilter;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_magSwipeFailureFilter;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    .line 394
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    .line 395
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentListenerProvider:Ljavax/inject/Provider;

    .line 396
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideNfcListenerProvider:Ljavax/inject/Provider;

    .line 397
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentCompletionListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentCompletionListenerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentCompletionListenerProvider:Ljavax/inject/Provider;

    .line 398
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideMinesweeper;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideMinesweeper;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideMinesweeperProvider:Ljavax/inject/Provider;

    .line 399
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/LcrModule_ProvideResFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideResFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideResProvider:Ljavax/inject/Provider;

    .line 400
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideMinesweeperProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    .line 401
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideEventLogFeatureProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePowerFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v17, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v18, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v19, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSystemFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v20, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideTamperFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v21, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideUserInteractionFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v22, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v23, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v24, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v25, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideNfcListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v26, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentCompletionListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v27, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v28, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v29, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v30, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    move-object/from16 v31, v2

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 v32, v2

    invoke-static/range {v10 .. v32}, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 402
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v2

    invoke-static/range {v10 .. v16}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 403
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5}, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 404
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideBleBondingBroadcastReceiver;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideBleBondingBroadcastReceiver;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    .line 405
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleExecutor;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleExecutor;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bleExecutorProvider:Ljavax/inject/Provider;

    .line 406
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleSenderProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleReceiverFactoryProvider:Ljavax/inject/Provider;

    .line 407
    invoke-static/range {p1 .. p1}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;->create(Lcom/squareup/cardreader/ble/BleDeviceModule;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBluetoothDeviceProvider:Ljavax/inject/Provider;

    .line 408
    new-instance v1, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleThreadEnforcer;

    invoke-direct {v1, v9}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bleThreadEnforcer;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bleThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 409
    iget-object v9, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bleExecutorProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleReceiverFactoryProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleSenderProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBluetoothDeviceProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->bleThreadEnforcerProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v19, v4

    move-object/from16 v20, v5

    move-object/from16 v21, v6

    invoke-static/range {v9 .. v21}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleConnectionStateMachineProvider:Ljavax/inject/Provider;

    .line 410
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleConnectionStateMachineProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleBackendListenerV2Provider:Ljavax/inject/Provider;

    invoke-static {v8, v1, v2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleCardReaderGraphInitializerFactory;->create(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleCardReaderGraphInitializerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    .line 411
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideBleCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    .line 412
    iget-object v1, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-void
.end method


# virtual methods
.method public cardReaderContext()Lcom/squareup/cardreader/CardReaderContext;
    .locals 1

    .line 417
    iget-object v0, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderContext;

    return-object v0
.end method
