.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBluetoothDeviceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/bluetooth/BluetoothDevice;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/ble/BleDeviceModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/BleDeviceModule;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;)V

    return-object v0
.end method

.method public static provideBluetoothDevice(Lcom/squareup/cardreader/ble/BleDeviceModule;)Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/bluetooth/BluetoothDevice;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;->provideBluetoothDevice(Lcom/squareup/cardreader/ble/BleDeviceModule;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBluetoothDeviceFactory;->get()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method
