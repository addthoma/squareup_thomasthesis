.class public Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;
.super Ljava/lang/Object;
.source "BleAction.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FailedDescriptorWrite"
.end annotation


# instance fields
.field public final descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

.field public final status:I


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    .line 200
    iput p2, p0, Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;->status:I

    return-void
.end method


# virtual methods
.method public describe()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 204
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    .line 205
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;->status:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Failed writing descriptor on characteristic %s, status %d"

    .line 204
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
