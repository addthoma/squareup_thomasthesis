.class public final Lcom/squareup/cardreader/ble/R12GattPublic$Companion;
.super Ljava/lang/Object;
.source "R12GattPublic.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/R12GattPublic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/R12GattPublic$Companion;",
        "",
        "()V",
        "R12_AVAILABLE_TO_BOND_OFFSET",
        "",
        "SQUARE_BLE_KEY",
        "UUID_LCR_SERVICE",
        "Ljava/util/UUID;",
        "getUUID_LCR_SERVICE",
        "()Ljava/util/UUID;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/R12GattPublic$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getUUID_LCR_SERVICE()Ljava/util/UUID;
    .locals 1

    .line 8
    invoke-static {}, Lcom/squareup/cardreader/ble/R12GattPublic;->access$getUUID_LCR_SERVICE$cp()Ljava/util/UUID;

    move-result-object v0

    return-object v0
.end method
