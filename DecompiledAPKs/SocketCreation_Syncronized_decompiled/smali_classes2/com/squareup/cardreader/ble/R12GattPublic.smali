.class public final Lcom/squareup/cardreader/ble/R12GattPublic;
.super Ljava/lang/Object;
.source "R12GattPublic.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/R12GattPublic$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/R12GattPublic;",
        "",
        "()V",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/ble/R12GattPublic$Companion;

.field public static final R12_AVAILABLE_TO_BOND_OFFSET:I = 0x0

.field public static final SQUARE_BLE_KEY:I = 0x827e

.field private static final UUID_LCR_SERVICE:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/ble/R12GattPublic$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ble/R12GattPublic$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12GattPublic;->Companion:Lcom/squareup/cardreader/ble/R12GattPublic$Companion;

    .line 8
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    sput-object v0, Lcom/squareup/cardreader/ble/R12GattPublic;->UUID_LCR_SERVICE:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getUUID_LCR_SERVICE$cp()Ljava/util/UUID;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/cardreader/ble/R12GattPublic;->UUID_LCR_SERVICE:Ljava/util/UUID;

    return-object v0
.end method
