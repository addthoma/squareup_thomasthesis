.class public final synthetic Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/cardreader/ble/R12State;->values()[Lcom/squareup/cardreader/ble/R12State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->CREATED:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_REMOVE_BOND:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_BOND:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_CONNECTION_CONTROL:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_COMMS_VERSION:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->READY:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_DISCONNECTION:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->DISCONNECTED:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1

    return-void
.end method
