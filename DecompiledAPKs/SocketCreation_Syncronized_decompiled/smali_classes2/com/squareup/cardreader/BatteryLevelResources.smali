.class public Lcom/squareup/cardreader/BatteryLevelResources;
.super Ljava/lang/Object;
.source "BatteryLevelResources.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildBatteryDrawable(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 29
    sget-object v0, Lcom/squareup/cardreader/BatteryLevelResources$1;->$SwitchMap$com$squareup$cardreader$BatteryLevel:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/BatteryLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 53
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unrecognized BatteryLevel: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 49
    :pswitch_0
    invoke-static {p3, p5, p1, p1, p2}, Lcom/squareup/cardreader/BatteryLevelResources;->composeWithBatteryOutline(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0

    .line 46
    :pswitch_1
    invoke-static {p3, p6, p1, p1, p2}, Lcom/squareup/cardreader/BatteryLevelResources;->composeWithBatteryOutline(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0

    .line 43
    :pswitch_2
    invoke-static {p3, p7, p1, p1, p2}, Lcom/squareup/cardreader/BatteryLevelResources;->composeWithBatteryOutline(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0

    .line 40
    :pswitch_3
    invoke-static {p3, p8, p1, p1, p2}, Lcom/squareup/cardreader/BatteryLevelResources;->composeWithBatteryOutline(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0

    .line 36
    :pswitch_4
    sget p0, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-static {p3, p5, p0, p1, p2}, Lcom/squareup/cardreader/BatteryLevelResources;->composeWithBatteryOutline(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0

    .line 32
    :pswitch_5
    new-instance p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-direct {p0, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 33
    invoke-virtual {p0, p4}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p0

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static buildBatteryDrawableNormal(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 9

    .line 13
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v6, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v8}, Lcom/squareup/cardreader/BatteryLevelResources;->buildBatteryDrawable(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static buildBatteryDrawableTiny(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 9

    .line 20
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v6, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v8}, Lcom/squareup/cardreader/BatteryLevelResources;->buildBatteryDrawable(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method private static composeWithBatteryOutline(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 59
    invoke-static {p4, p0, p3, p1, p2}, Lcom/squareup/glyph/GlyphTypeface;->buildLayeredGlyphs(Landroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;ILcom/squareup/glyph/GlyphTypeface$Glyph;I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method
