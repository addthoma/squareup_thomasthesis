.class Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer$1;
.super Ljava/lang/Object;
.source "BluetoothDiscoverer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer$1;->this$0:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer$1;->this$0:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    invoke-static {v0}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->access$000(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    return-void
.end method
