.class public final Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory;
.super Ljava/lang/Object;
.source "SpeCardReaderModule_ProvideCardReaderAddressFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideCardReaderAddress()Ljava/lang/String;
    .locals 1

    .line 28
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule;->provideCardReaderAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderAddressFactory;->provideCardReaderAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
