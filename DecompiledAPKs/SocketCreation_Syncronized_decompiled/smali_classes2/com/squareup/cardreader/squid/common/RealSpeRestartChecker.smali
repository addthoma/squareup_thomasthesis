.class public Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;
.super Ljava/lang/Object;
.source "RealSpeRestartChecker.java"

# interfaces
.implements Lcom/squareup/cardreader/squid/common/SpeRestartChecker;


# instance fields
.field private cardReaderReportedReady:Z

.field private ignoreFirmwareUpdateRestart:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;->cardReaderReportedReady:Z

    .line 13
    iput-boolean v0, p0, Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;->ignoreFirmwareUpdateRestart:Z

    return-void
.end method


# virtual methods
.method public onReaderReady()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Checking whether app restart is needed"

    .line 23
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iget-boolean v1, p0, Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;->cardReaderReportedReady:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;->ignoreFirmwareUpdateRestart:Z

    if-eqz v1, :cond_0

    goto :goto_0

    .line 29
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/squid/common/SpeRestartedException;->INSTANCE:Lcom/squareup/cardreader/squid/common/SpeRestartedException;

    throw v0

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 32
    iput-boolean v1, p0, Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;->cardReaderReportedReady:Z

    .line 33
    iput-boolean v0, p0, Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;->ignoreFirmwareUpdateRestart:Z

    return-void
.end method

.method public setIgnoreFirmwareUpdateRestart(Z)V
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/squareup/cardreader/squid/common/RealSpeRestartChecker;->ignoreFirmwareUpdateRestart:Z

    return-void
.end method
