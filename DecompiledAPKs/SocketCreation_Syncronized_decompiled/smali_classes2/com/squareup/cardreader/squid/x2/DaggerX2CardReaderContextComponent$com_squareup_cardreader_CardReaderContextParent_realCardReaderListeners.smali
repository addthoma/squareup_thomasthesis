.class Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners;
.super Ljava/lang/Object;
.source "DaggerX2CardReaderContextComponent.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Lcom/squareup/cardreader/RealCardReaderListeners;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderContextParent;)V
    .locals 0

    .line 510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 511
    iput-object p1, p0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 2

    .line 516
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderContextParent;->realCardReaderListeners()Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable component method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/RealCardReaderListeners;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 506
    invoke-virtual {p0}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_realCardReaderListeners;->get()Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    return-object v0
.end method
