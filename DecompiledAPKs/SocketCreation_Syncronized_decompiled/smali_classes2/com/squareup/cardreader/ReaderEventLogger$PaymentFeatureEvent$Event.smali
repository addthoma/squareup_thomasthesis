.class public final enum Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;
.super Ljava/lang/Enum;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum CANCEL_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum CANCEL_TMN_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum ON_CARD_ACTION_REQUIRED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum ON_CARD_PRESENCE_CHANGED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum ON_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum ON_TMN_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum START_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public static final enum START_TMN_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 124
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v1, 0x0

    const-string v2, "ON_PAYMENT_COMPLETE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 125
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v2, 0x1

    const-string v3, "ON_TMN_PAYMENT_COMPLETE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_TMN_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 126
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v3, 0x2

    const-string v4, "ON_CARD_ACTION_REQUIRED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_CARD_ACTION_REQUIRED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 127
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v4, 0x3

    const-string v5, "ON_CARD_PRESENCE_CHANGED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_CARD_PRESENCE_CHANGED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 128
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v5, 0x4

    const-string v6, "START_PAYMENT"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->START_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 129
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v6, 0x5

    const-string v7, "CANCEL_PAYMENT"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->CANCEL_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 130
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v7, 0x6

    const-string v8, "START_TMN_PAYMENT"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->START_TMN_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 131
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/4 v8, 0x7

    const-string v9, "CANCEL_TMN_PAYMENT"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->CANCEL_TMN_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 123
    sget-object v9, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_TMN_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_CARD_ACTION_REQUIRED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_CARD_PRESENCE_CHANGED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->START_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->CANCEL_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->START_TMN_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->CANCEL_TMN_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->$VALUES:[Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;
    .locals 1

    .line 123
    const-class v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;
    .locals 1

    .line 123
    sget-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->$VALUES:[Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    return-object v0
.end method
