.class public final Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;
.super Ljava/lang/Object;
.source "DaggerAudioCardReaderContextComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

.field private nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$1;)V
    .locals 0

    .line 529
    invoke-direct {p0}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/audio/AudioCardReaderContextComponent;
    .locals 4

    .line 558
    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    const-class v1, Lcom/squareup/cardreader/CardReaderModule;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 559
    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    const-class v1, Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 560
    new-instance v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;

    iget-object v1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    iget-object v2, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;-><init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$1;)V

    return-object v0
.end method

.method public cardReaderModule(Lcom/squareup/cardreader/CardReaderModule;)Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;
    .locals 0

    .line 538
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderModule;

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    return-object p0
.end method

.method public clockModule(Lcom/squareup/android/util/ClockModule;)Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 547
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public nonX2CardReaderContextParent(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;
    .locals 0

    .line 553
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    return-object p0
.end method
